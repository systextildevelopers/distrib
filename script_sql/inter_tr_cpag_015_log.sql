  CREATE OR REPLACE TRIGGER "INTER_TR_CPAG_015_LOG" 
   after insert or
         delete or
         update of dupl_for_nrduppag, dupl_for_no_parc, dupl_for_for_cli9, dupl_for_for_cli4,
    dupl_for_for_cli2, dupl_for_tipo_tit, sequencia_pagto, data_pagamento,
    numero_documento, codigo_historico, valor_juros, valor_descontos,
    valor_pago, valor_abatimento, valor_abatido, cd_porta_cod_port,
    cd_porta_nr_conta, numero_lote, nf_origem_abat, ser_origem_abat,
    codigo_depto, codigo_transacao, data_baixa, pago_adiantam,
    num_contabil, data_pg_real, numero_adiantam, transacao,
    exporta_pagamento, valor_pago_moeda, executa_trigger, vlr_juros_moeda,
    vlr_descontos_moeda, vlr_abatido_moeda, tipo_pagamento, vlr_var_cambial
         on cpag_015

   for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);
   long_aux                  varchar2(2000);

   v_executa_trigger         number;

begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   -- INICIO - Logica implementada para controle de "gatilho" das triggers do banco
   -- para atender a implementacao executada para limpeza da base de dados do cliente
   -- e replicacao dos dados para base historico. (SS.38405)
   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then
      if inserting
      then
         INSERT INTO hist_100
            ( tabela,                         operacao,
              data_ocorr,                     usuario_rede,
              maquina_rede,                   aplicacao,
              num01,                          num02,
              num03,                          num04,
              num05,                          str01,
              long01
            )
         VALUES
            ( 'CPAG_015',                     'I',
              sysdate,                        ws_usuario_rede,
              ws_maquina_rede,                ws_aplicativo,
              :new.dupl_for_nrduppag,         :new.dupl_for_for_cli9,
              :new.dupl_for_for_cli4,         :new.dupl_for_for_cli2,
              :new.dupl_for_tipo_tit,         :new.dupl_for_no_parc,
              '                                               ' ||
              inter_fn_buscar_tag('lb34835#BAIXA DE TITULOS A PAGAR',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              chr(10)                                                             ||
              chr(10)                                                             ||
              inter_fn_buscar_tag('lb31254#CLIENTE:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :new.dupl_for_for_cli9        || '/' ||
                                            :new.dupl_for_for_cli4        || '-' ||
                                            :new.dupl_for_for_cli2        ||
                                             chr(10) ||
                                             chr(10) ||
              inter_fn_buscar_tag('lb06762#SEQUENCIA.:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.sequencia_pagto   ||
                                            chr(10) ||
              inter_fn_buscar_tag('lb02981#DATA PAGAMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.data_pagamento    ||
                                            chr(10) ||
              inter_fn_buscar_tag('lb10014#NR. DOCUMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.numero_documento  ||
                                            chr(10) ||
              inter_fn_buscar_tag('lb34834#HISTORICO TITULO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.codigo_historico  ||
                                            chr(10) ||
              inter_fn_buscar_tag('lb02853#DATA BAIXA:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.data_baixa        ||
                                            chr(10) ||
              inter_fn_buscar_tag('lb05344#NUMERO DO ADIANTAMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.numero_adiantam   ||
                                            chr(10) ||
              inter_fn_buscar_tag('lb01632#ADIANTAMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.pago_adiantam     ||
                                            chr(10) ||
              inter_fn_buscar_tag('lb05343#NUM.CONTABIL:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.num_contabil      ||
                                            chr(10) ||
              inter_fn_buscar_tag('lb05367#ORIGEM DOCUMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.cd_porta_cod_port ||
                                            chr(10) ||
              inter_fn_buscar_tag('lb00097#CONTA:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.cd_porta_nr_conta ||
                                            chr(10) ||
              inter_fn_buscar_tag('lb05241#VALOR PAGO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.valor_pago        ||
                                            chr(10) ||
              inter_fn_buscar_tag('lb01604#ABATIMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.valor_abatimento  ||
                                            chr(10) ||
              inter_fn_buscar_tag('lb31929#VALOR DESCONTOS:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.valor_descontos  ||
                                            chr(10) ||
              inter_fn_buscar_tag('lb31928#VALOR JUROS:',ws_locale_usuario,ws_usuario_systextil) || ' ' || :new.valor_juros  ||
                                            chr(10) ||
              'VALOR VARIACAO CAMBIAL: ' || ' ' || :new.vlr_var_cambial
           );
      end if;

      if updating   and
         (
          :old.dupl_for_for_cli9  <> :new.dupl_for_for_cli9  or
          :old.dupl_for_for_cli4  <> :new.dupl_for_for_cli4  or
          :old.dupl_for_for_cli2  <> :new.dupl_for_for_cli2  or
          :old.sequencia_pagto    <> :new.sequencia_pagto    or
          :old.data_pagamento     <> :new.data_pagamento     or
          :old.numero_documento   <> :new.numero_documento   or
          :old.codigo_historico   <> :new.codigo_historico   or
          :old.data_baixa         <> :new.data_baixa         or
          :old.numero_adiantam    <> :new.numero_adiantam    or
          :old.pago_adiantam      <> :new.pago_adiantam      or
          :old.num_contabil       <> :new.num_contabil       or
          :old.valor_pago         <> :new.valor_pago         or
          :old.valor_abatimento   <> :new.valor_abatimento   or
          :old.valor_descontos    <> :new.valor_descontos    or
          :old.valor_juros        <> :new.valor_juros        or
          :old.cd_porta_cod_port  <> :new.cd_porta_cod_port  or
          :old.cd_porta_nr_conta  <> :new.cd_porta_nr_conta  or
          :old.vlr_var_cambial    <> :new.vlr_var_cambial
         )
      then
         long_aux := long_aux ||
             '                                               ' ||
             inter_fn_buscar_tag('lb34835#BAIXA DE TITULOS A PAGAR',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
             chr(10)                                     ||
             chr(10);

         long_aux := long_aux        ||
          inter_fn_buscar_tag('lb06762#SEQUENCIA.:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                        :old.sequencia_pagto || ' -> ' ||
                                        :new.sequencia_pagto ||
                                         chr(10);


         if :old.data_pagamento <> :new.data_pagamento
         then
             long_aux := long_aux        ||
                inter_fn_buscar_tag('lb02981#DATA PAGAMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :old.data_pagamento || ' -> ' ||
                                            :new.data_pagamento ||
                                            chr(10);
         end if;

         if :old.numero_documento  <> :new.numero_documento
         then
             long_aux := long_aux        ||
                inter_fn_buscar_tag('lb10014#NR. DOCUMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :old.numero_documento || ' -> ' ||
                                            :new.numero_documento ||
                                            chr(10);
         end if;

         if :old.codigo_historico <> :new.codigo_historico
         then
             long_aux := long_aux        ||
                inter_fn_buscar_tag('lb34834#HISTORICO TITULO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :old.codigo_historico || ' -> ' ||
                                            :new.codigo_historico ||
                                            chr(10);
         end if;

         if :old.data_baixa <> :new.data_baixa
         then
             long_aux := long_aux        ||
                inter_fn_buscar_tag('lb02853#DATA BAIXA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :old.data_baixa || ' -> ' ||
                                            :new.data_baixa ||
                                            chr(10);
         end if;

         if :old.numero_adiantam <> :new.numero_adiantam
         then
             long_aux := long_aux        ||
                inter_fn_buscar_tag('lb05344#NUMERO DO ADIANTAMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :old.numero_adiantam || ' -> ' ||
                                            :new.numero_adiantam ||
                                            chr(10);
         end if;

         if  :old.pago_adiantam <> :new.pago_adiantam
         then
             long_aux := long_aux        ||
                inter_fn_buscar_tag('lb01632#ADIANTAMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :old.pago_adiantam || ' -> ' ||
                                            :new.pago_adiantam ||
                                            chr(10);
         end if;

         if :old.num_contabil <> :new.num_contabil
         then
             long_aux := long_aux        ||
                inter_fn_buscar_tag('lb05343#NUM.CONTABIL:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :old.num_contabil || ' -> ' ||
                                            :new.num_contabil ||
                                            chr(10);
         end if;

         if :old.cd_porta_cod_port <> :new.cd_porta_cod_port
         then
             long_aux := long_aux        ||
                inter_fn_buscar_tag('lb05367#ORIGEM DOCUMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
              'ORIGEM DOCUMENTO: '       || :old.cd_porta_cod_port || ' -> ' ||
                                            :new.cd_porta_cod_port ||
                                            chr(10);
         end if;

         if :old.cd_porta_nr_conta <> :new.cd_porta_nr_conta
         then
             long_aux := long_aux        ||
                inter_fn_buscar_tag('lb00097#CONTA:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :old.cd_porta_nr_conta || ' -> ' ||
                                            :new.cd_porta_nr_conta ||
                                            chr(10);
         end if;

         if :old.valor_pago <> :new.valor_pago
         then
             long_aux := long_aux        ||
                inter_fn_buscar_tag('lb05241#VALOR PAGO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :old.valor_pago || ' -> ' ||
                                            :new.valor_pago ||
                                            chr(10);
         end if;

         if :old.valor_abatimento <> :new.valor_abatimento
         then
             long_aux := long_aux        ||
                inter_fn_buscar_tag('lb01604#ABATIMENTO:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :old.valor_abatimento || ' -> ' ||
                                            :new.valor_abatimento ||
                                            chr(10);
         end if;

         if :old.valor_descontos <> :new.valor_descontos
         then
             long_aux := long_aux        ||
                inter_fn_buscar_tag('lb31929#VALOR DESCONTOS:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                          :old.valor_descontos || ' -> ' ||
                                            :new.valor_descontos ||
                                            chr(10);
         end if;

         if :old.valor_juros <> :new.valor_juros
         then
             long_aux := long_aux        ||
                inter_fn_buscar_tag('lb31928#VALOR JUROS:',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                                            :old.valor_juros || ' -> ' ||
                                            :new.valor_juros ||
                                            chr(10);
         end if;
         
         if :old.vlr_var_cambial <> :new.vlr_var_cambial
         then
             long_aux := long_aux        ||
                'VALOR VARIACAO CAMBIAL: ' ||
                                            :old.vlr_var_cambial || ' -> ' ||
                                            :new.vlr_var_cambial;
         end if;

         INSERT INTO hist_100
            ( tabela,            operacao,
              data_ocorr,        usuario_rede,
              maquina_rede,      aplicacao,
              num01,             num02,
              num03,             num04,
              num05,             str01,
              long01
            )
         VALUES
            ( 'CPAG_015',              'A',
              sysdate,                 ws_usuario_rede,
              ws_maquina_rede,         ws_aplicativo,
              :new.dupl_for_nrduppag,  :new.dupl_for_for_cli9,
              :new.dupl_for_for_cli4,  :new.dupl_for_for_cli2,
              :new.dupl_for_tipo_tit,  :new.dupl_for_no_parc,
              long_aux
           );

      end if;

      if deleting
      then
         INSERT INTO hist_100
            ( tabela,                         operacao,
              data_ocorr,                     usuario_rede,
              maquina_rede,                   aplicacao,
              num01,                          num02,
              num03,                          num04,
              num05,                          str01,
              long01
            )
         VALUES
            ( 'CPAG_015',                     'D',
              sysdate,                        ws_usuario_rede,
              ws_maquina_rede,                ws_aplicativo,
              :old.dupl_for_nrduppag,         :old.dupl_for_for_cli9,
              :old.dupl_for_for_cli4,         :old.dupl_for_for_cli2,
              :old.dupl_for_tipo_tit,         :old.dupl_for_no_parc,
              '                                               ' ||
              inter_fn_buscar_tag('lb34835#BAIXA DE TITULOS A PAGAR',ws_locale_usuario,ws_usuario_systextil)
           );
      end if;
   end if;
end inter_tr_cpag_015_log;

-- ALTER TRIGGER "INTER_TR_CPAG_015_LOG" ENABLE
 

 /

exec inter_pr_recompile;

