alter table sped_ctb_j100
add (saldo_per_ant         number(15,2),
     ind_saldo_per_ant     varchar2(1));     
comment on column sped_ctb_j100.saldo_per_ant is 'Saldo do exercicio anterior ao período executado';
comment on column sped_ctb_j100.ind_saldo_per_ant is 'Indica o saldo do exercicio anterior ao período executado';    
exec inter_pr_recompile;
