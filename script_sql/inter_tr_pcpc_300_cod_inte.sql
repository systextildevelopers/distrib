
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPC_300_COD_INTE" 
before insert or
       update
       of cod_integracao
on pcpc_300
for each row

declare

   v_cod_tipo_volume pcpc_300.cod_tipo_volume %type;
   v_cod_integracao  pcpc_300.cod_integracao  %type;

   Pragma autonomous_transaction;

begin

   if :new.cod_integracao > 0
   then

      v_cod_integracao := 0;

      if inserting
      then
         v_cod_tipo_volume := :new.cod_tipo_volume;
      else
         v_cod_tipo_volume := :old.cod_tipo_volume;
      end if;

      begin

         select max(1)
         into   v_cod_integracao
         from  pcpc_300
         where pcpc_300.cod_tipo_volume <> v_cod_tipo_volume
           and pcpc_300.cod_integracao  =  :new.cod_integracao;
         exception
            when others then
               v_cod_integracao := 0;

      end;

      if v_cod_integracao = 1
      then
         raise_application_error(-20000, 'Esse codigo de integracao ja esta associado a outro volume.');
      end if;
   end if;
end inter_tr_pcpc_300_cod_inte;

-- ALTER TRIGGER "INTER_TR_PCPC_300_COD_INTE" ENABLE
 

/

exec inter_pr_recompile;

