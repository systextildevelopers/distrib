create or replace function inter_fn_trata_nulo_str(p_finalidade in integer,
                                                   p_campo1     in varchar) 
                                                   return varchar is
   begin
  /* TRATA FINALIDADE DA NOTA
    SE FOR NOTA NORMAL OU DEVOLU��O RETORNA A MESMA INFORMA,
    CASO FOR COMPLEMNTAR OU AJUSTE RETORNO NULO
   */  
   -- utilizada para strings


       if p_finalidade in (1,4)
       then 
          return p_campo1;
       else
          return null;
       end if;
end inter_fn_trata_nulo_str;
/
