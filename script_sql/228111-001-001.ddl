create table HAUS_001
(
  COD_EMPRESA           NUMBER(3),
  ORIGEM                NUMBER(9),
  CGC9                  NUMBER(9),
  CGC4                  NUMBER(4),
  CGC2                  NUMBER(2),
  COLECAO_TABELA        NUMBER(2),
  MES_TABELA            NUMBER(2),
  SEQUENCIA_TABELA      NUMBER(2),
  COND_PAGAMENTO        NUMBER(3),
  NUM_NATUREZA          NUMBER(3),
  EST_NATUREZA          VARCHAR2(2),
  CLASSIFICACAO_PEDIDO  NUMBER(5),
  CODIGO_DEPOSITO       NUMBER(3),
  CODIGO_HISTORICO      NUMBER(4),
  CONTA_CORRENTE        NUMBER(9)
);

create table HAUS_002
(
  ID                 NUMBER(9) not null,
  NOME_ARQUIVO       VARCHAR2(200),
  DATA_LEITURA       DATE,
  TIPO_REGISTRO      NUMBER(1),
  NUMERO_PEDIDO      NUMBER(9),
  DATA_FATURAMENTO   DATE,
  SITUACAO           NUMBER(1),
  DESCRICAO_PROBLEMA VARCHAR2(4000),
  
  CONSTRAINT PK_HAUS_002 PRIMARY KEY (ID)
);

create table HAUS_003
(
  ID          NUMBER(9) not null,
  ID_HAUS_002 NUMBER(9),
  ITEM_PEDIDO NUMBER(9),
  PRODUTO_EAN VARCHAR2(16),
  NIVEL       VARCHAR2(2),
  GRUPO       VARCHAR2(5),
  SUBGRUPO    VARCHAR2(3),
  ITEM        VARCHAR2(6),
  QUANTIDADE  NUMBER(14,3),
  VALOR_VENDA NUMBER(20,5),
  CONSTRAINT PK_HAUS_003 PRIMARY KEY (ID)
);

alter table HAUS_003
  add constraint FK_HAUS_003_ID_HAUS_002 foreign key (ID_HAUS_002)
  references HAUS_002 (ID);

create table HAUS_004
(
  ID                NUMBER(9) not null,
  ID_HAUS_002       NUMBER(3),
  SEQUENCIA_PARCELA NUMBER(9),
  DATA_VENCIMENTO   DATE,
  VALOR_PARCELA     NUMBER(20,5)
);

alter table HAUS_004
  add constraint FK_HAUS_004_ID_HAUS_002 foreign key (ID_HAUS_002)
  references HAUS_002 (ID);
  
  
create sequence ID_HAUS_002
minvalue 0
maxvalue 99999999999999999999
start with 21
increment by 1
cache 20;

create sequence ID_HAUS_003
minvalue 0
maxvalue 99999999999999999999
start with 1
increment by 1
cache 20;  

/ 
exec inter_pr_recompile;
