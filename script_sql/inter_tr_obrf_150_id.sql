
  CREATE OR REPLACE TRIGGER "INTER_TR_OBRF_150_ID" 
before insert on OBRF_150
    for each row

    declare
     v_nr_registro number;

    begin
     select seq_OBRF_150.nextval into v_nr_registro from dual;

    :new.id := v_nr_registro;

 end inter_tr_OBRF_150_id;
-- ALTER TRIGGER "INTER_TR_OBRF_150_ID" ENABLE
 

/

exec inter_pr_recompile;

