alter table inte_520 add status_integracao   number(1);

comment on column inte_520.status_integracao is 'Flag utilizada por softwares de terceiros para integração, valor livre conforme necessidade';

exec inter_pr_recompile;
