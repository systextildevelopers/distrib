
  CREATE OR REPLACE PROCEDURE "LOJA_PR_CONCATENAR" (pTipo   in varchar2,
                                               pCodFil in varchar2,
                                               pTipPed in varchar2,
                                               pNumPed in varchar2,
                                               pPedEm  in date,
                                               pVlrTot in numeric,
                                               pCodemp in number,
                                               pNumIte in number,
                                               pResult out varchar2) is
begin
  loja_pr_concatenar_inte(pTipo,
                     pCodFil,
                     pTipPed,
                     pNumPed,
                     pPedEm,
                     pVlrTot,
                     pCodemp,
                     pNumIte,
                     pResult);
end loja_pr_concatenar;
 

/

exec inter_pr_recompile;

