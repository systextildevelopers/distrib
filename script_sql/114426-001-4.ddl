UPDATE OBRF_780 
SET OBRF_780.NR_DOCUMENTO = 0,
    OBRF_780.PRE_ROMANEIO = 0;

commit work;

alter table obrf_780 modify (pre_romaneio default 0, nr_documento default 0);

exec inter_pr_recompile;
/

