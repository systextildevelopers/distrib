create or replace package body ST_PCK_PEDIDO is
    
    FUNCTION get_dados_pedido(p_pedido_venda number, p_msg_erro in out varchar2) return t_dados_pedi_100
    AS 
        v_dados_pedido_venda t_dados_pedi_100;
    BEGIN
        begin
            SELECT *  
            BULK COLLECT 
            INTO v_dados_pedido_venda
            FROM pedi_100
            WHERE pedido_venda = p_pedido_venda;
        exception when others then
            p_msg_erro := 'N?o foi possivel buscar o pedido!. p_pedido_venda:' || p_pedido_venda;
            return null;
        end;
        return v_dados_pedido_venda;
    END;

    FUNCTION get_pedido_by_exportacao(  p_pedido_venda number,  p_codigo_empresa number, 
                                        p_cod_proforma varchar2,
                                        p_flag_por_item number,
                                        p_msg_erro in out varchar2) return t_dados_pedi_100
    AS 
        v_dados_pedido_venda t_dados_pedi_100;
    BEGIN
        begin
            SELECT *  
            BULK COLLECT 
            INTO v_dados_pedido_venda
            FROM pedi_100 p
            WHERE pedido_venda = p_pedido_venda
            AND   p.SITUACAO_VENDA <> 10
            AND   p.NATOP_PV_EST_OPER = 'EX'
            AND   (p.CODIGO_EMPRESA = p_codigo_empresa  or p_codigo_empresa = 0)
            AND EXISTS (    SELECT 1 
                            FROM pedi_110 I 
                            WHERE I.PEDIDO_VENDA = P.PEDIDO_VENDA
                              AND (I.COD_PROFORMA = p_cod_proforma or p_flag_por_item = 0));                
        exception when others then
            p_msg_erro := 'N?o foi possivel buscar pedido venda para exportacao!. p_pedido_venda:' || p_pedido_venda ||
                                                                                ' p_codigo_empresa: '|| p_codigo_empresa;
        end;

        return v_dados_pedido_venda;
    END;

    FUNCTION get_pedido_by_processo(  p_chave_processo varchar2,  p_codigo_empresa number, 
                                      p_msg_erro in out varchar2) return t_dados_pedi_100
    AS 
        v_dados_pedidos_processo t_dados_pedi_100;
    BEGIN
        begin
            SELECT * 
            BULK COLLECT 
            INTO v_dados_pedidos_processo
            FROM pedi_100 p
            WHERE   p.COD_CANCELAMENTO = 0
            AND   p.SITUACAO_VENDA <> 10
            AND   p.NATOP_PV_EST_OPER = 'EX'
            AND   (p.CODIGO_EMPRESA = p_codigo_empresa)
            AND EXISTS (    SELECT 1 
                            FROM pedi_110 I 
                            WHERE I.PEDIDO_VENDA = P.PEDIDO_VENDA
                            AND I.COD_PROCESSO = p_chave_processo);
        exception when others then
            p_msg_erro := 'N?o foi possivel buscar pedido venda para exportacao!. p_chave_processo: ' || p_chave_processo ||
                                                                                ' p_codigo_empresa: ' || p_codigo_empresa || ' - ' || SQLERRM;
        end;
        return v_dados_pedidos_processo;
    END;
    
    FUNCTION get_pedido_by_draft(  p_chave_processo varchar2,  p_codigo_empresa number, p_flag_por_item varchar2,  
                                      p_msg_erro in out varchar2) return t_dados_pedi_100
    AS 
        v_dados_pedidos_processo t_dados_pedi_100;
    BEGIN
        begin
            SELECT * 
            BULK COLLECT 
            INTO v_dados_pedidos_processo
            FROM pedi_100 p
            WHERE   p.COD_CANCELAMENTO = 0
            AND   p.SITUACAO_VENDA <> 10
            AND   p.NATOP_PV_EST_OPER = 'EX'
            AND   (p.CODIGO_EMPRESA = p_codigo_empresa)
            AND (
              (p_flag_por_item = 'P' AND p.COD_PROFORMA = p_chave_processo)
              OR (p_flag_por_item = 'I' AND EXISTS (
                  SELECT 1
                  FROM pedi_110 i
                  WHERE i.PEDIDO_VENDA = p.PEDIDO_VENDA
                  AND i.COD_PROFORMA = p_chave_processo
              ))
           );                
        exception when others then
            p_msg_erro := 'N?o foi possivel buscar pedido venda para exportacao!. p_chave_processo: ' || p_chave_processo ||
                                                                                ' p_codigo_empresa: ' || p_codigo_empresa || ' - ' || SQLERRM;
        end;
        return v_dados_pedidos_processo;
    END;


    FUNCTION get_pedidos_por_proforma( p_cod_proforma varchar2, p_msg_erro in out varchar2, p_flag_por_item number)  return t_dados_pedi_100
    AS 
        v_dados_pedidos_proforma t_dados_pedi_100;
    BEGIN
        begin
            SELECT * 
            BULK COLLECT 
            INTO v_dados_pedidos_proforma
            FROM pedi_100 p
            WHERE   p.COD_CANCELAMENTO = 0
            AND   p.SITUACAO_VENDA <> 10
            AND   p.NATOP_PV_EST_OPER = 'EX'
            AND (
              (p_flag_por_item = 0 AND p.COD_PROFORMA = p_cod_proforma)
              OR (p_flag_por_item = 1 AND EXISTS (
                  SELECT 1
                  FROM pedi_110 i
                  WHERE i.PEDIDO_VENDA = p.PEDIDO_VENDA
                  AND i.COD_PROFORMA = p_cod_proforma
              ))
           ); 
        exception when others then
            p_msg_erro := 'N?o foi possivel buscar pedidos venda para exportacao!. p_cod_proforma: ' || p_cod_proforma || SQLERRM;
        end;
        return v_dados_pedidos_proforma;
    END;
end ST_PCK_PEDIDO;
/
