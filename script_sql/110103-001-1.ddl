ALTER TABLE basi_023 ADD (fti NUMBER(9)); 

comment on column basi_023.fti is 'Ficha T�cnica de Insumos.';

alter table basi_023 modify fti default 0;

execute inter_pr_recompile;     
/

declare
cursor basi_023_c is
   select f.rowid from basi_023 f where fti is null;
   
contaRegistro number;

begin
   contaRegistro := 0;
   
   for reg_basi_023_c in basi_023_c
   loop
      update basi_023
      set fti = 0
      where fti is null
        and basi_023.rowid = reg_basi_023_c.rowid;
      
      contaRegistro := contaRegistro + 1;
      
      if contaRegistro = 1000
      then
         contaRegistro := 0;
         commit;
      end if;
   end loop; 

   commit;
end;
/

execute inter_pr_recompile;
/
