
  CREATE OR REPLACE PROCEDURE "INTER_PR_PCPC_040_PMS" 

is
   v_qtde_programada         pcpc_040.qtde_pecas_prog%type;
   v_qtde_produzida          pcpc_040.qtde_pecas_prod%type;
   v_qtde_programada_pms     pcpc_040.qtde_pecas_prod%type;
   v_qtde_programada_pms_alt pcpc_040.qtde_pecas_prod%type;
   v_lote_producao           i_pcpc_040.lote_producao%type;
   v_lote_producao_aux       i_pcpc_040.lote_producao%type;
   v_situacao_ordem          i_pcpc_040.situacao_ordem%type;
   v_descricao_cor           basi_010.descricao_15%type;
   v_tem_registro            boolean;
   v_proconf_subgrupo        pcpc_040.proconf_subgrupo%type;


begin
   for exportar_pms in (
         select sum(pcpc_040.qtde_pecas_prog) v_qtde_programada,
                sum(pcpc_040.qtde_pecas_prod +  pcpc_040.qtde_conserto +
                    pcpc_040.qtde_pecas_2a   +  pcpc_040.qtde_perdas) v_qtde_produzida,
                    pcpc_040.ordem_producao,    pcpc_040.periodo_producao,
                    pcpc_040.proconf_nivel99,   pcpc_040.proconf_grupo,
                    pcpc_040.proconf_item,      pcpc_040.codigo_estagio
         from pcpc_040, basi_030
         where pcpc_040.exportado_pms   = 'N'
           and pcpc_040.proconf_nivel99 = basi_030.nivel_estrutura
           and pcpc_040.proconf_grupo   = basi_030.referencia
           and pcpc_040.codigo_estagio  = basi_030.estagio_altera_programado
         group by pcpc_040.ordem_producao,  pcpc_040.periodo_producao,
                  pcpc_040.proconf_nivel99, pcpc_040.proconf_grupo,
                  pcpc_040.proconf_item,    pcpc_040.codigo_estagio
      )
   loop
      select min(pcpc_040.proconf_subgrupo)
      into v_proconf_subgrupo
      from pcpc_040, basi_030
      where pcpc_040.ordem_producao    = exportar_pms.ordem_producao
        and pcpc_040.periodo_producao  = exportar_pms.periodo_producao
        and pcpc_040.proconf_nivel99   = exportar_pms.proconf_nivel99
        and pcpc_040.proconf_grupo     = exportar_pms.proconf_grupo
        and pcpc_040.proconf_item      = exportar_pms.proconf_item
        and pcpc_040.codigo_estagio    = exportar_pms.codigo_estagio;


      begin
         select basi_010.descricao_15
         into v_descricao_cor
         from basi_010
         where basi_010.nivel_estrutura  = exportar_pms.proconf_nivel99
           and basi_010.grupo_estrutura  = exportar_pms.proconf_grupo
           and basi_010.subgru_estrutura = v_proconf_subgrupo
           and basi_010.item_estrutura   = exportar_pms.proconf_item;
      exception
         when no_data_found then
            v_descricao_cor          := ' ';
      end;

      if exportar_pms.v_qtde_programada        > 0.00 and
         exportar_pms.v_qtde_produzida         > 0.00 and
         exportar_pms.v_qtde_programada        = exportar_pms.v_qtde_produzida
      then
         select nvl(max(i_pcpc_040.lote_producao),0) + 1
         into  v_lote_producao
         from i_pcpc_040
         where i_pcpc_040.ordem_producao = exportar_pms.ordem_producao;

         v_tem_registro            := true;
         v_qtde_programada_pms     := 0;
         v_qtde_programada_pms_alt := 0;

         begin
            select i_pcpc_040.lote_producao, sum(i_pcpc_040.qtde_programada)
            into v_lote_producao_aux,        v_qtde_programada_pms
            from i_pcpc_040
            where i_pcpc_040.ordem_producao = exportar_pms.ordem_producao
              and i_pcpc_040.referencia     = exportar_pms.proconf_grupo
              and i_pcpc_040.codigo_cor     = exportar_pms.proconf_item
              and i_pcpc_040.situacao_ordem = 1
            group by i_pcpc_040.lote_producao;
         exception
            when no_data_found then
            v_lote_producao_aux      := 0;
            v_qtde_programada_pms    := 0;
            v_situacao_ordem         := 0;
         end;

         begin
            select i_pcpc_040.situacao_ordem, i_pcpc_040.lote_producao,
                   i_pcpc_040.qtde_programada
            into   v_situacao_ordem,            v_lote_producao_aux,
                   v_qtde_programada_pms_alt
            from i_pcpc_040
            where i_pcpc_040.ordem_producao = exportar_pms.ordem_producao
              and i_pcpc_040.referencia     = exportar_pms.proconf_grupo
              and i_pcpc_040.codigo_cor     = exportar_pms.proconf_item
             and i_pcpc_040.situacao_ordem = 4;
         exception
            when no_data_found then
            begin
               select i_pcpc_040.situacao_ordem, i_pcpc_040.lote_producao
               into v_situacao_ordem,            v_lote_producao_aux
               from i_pcpc_040
               where i_pcpc_040.ordem_producao = exportar_pms.ordem_producao
                 and i_pcpc_040.referencia     = exportar_pms.proconf_grupo
                 and i_pcpc_040.codigo_cor     = exportar_pms.proconf_item
                 and i_pcpc_040.situacao_ordem = 1
               group by i_pcpc_040.situacao_ordem, i_pcpc_040.lote_producao;
             exception
                when no_data_found then
                begin
                   select i_pcpc_040.situacao_ordem, i_pcpc_040.lote_producao,
                          i_pcpc_040.qtde_programada
                     into v_situacao_ordem,            v_lote_producao_aux,
                          v_qtde_programada_pms
                     from i_pcpc_040
                     where i_pcpc_040.ordem_producao = exportar_pms.ordem_producao
                       and i_pcpc_040.referencia     = exportar_pms.proconf_grupo
                       and i_pcpc_040.codigo_cor     = exportar_pms.proconf_item
                       and i_pcpc_040.situacao_ordem = 0;
                exception
                   when no_data_found then
                      v_situacao_ordem    := 0;
                      v_tem_registro      := false;
                      v_lote_producao_aux := 0;
               end;
            end;
         end;


         if v_qtde_programada_pms <> 0.00 and
            (v_qtde_programada_pms + v_qtde_programada_pms_alt) <> exportar_pms.v_qtde_programada and
            v_situacao_ordem       > 0
         then
            exportar_pms.v_qtde_programada :=  exportar_pms.v_qtde_programada - (v_qtde_programada_pms + v_qtde_programada_pms_alt);
            if v_situacao_ordem = 1
            then
                v_situacao_ordem := 4;
                v_lote_producao  := v_lote_producao_aux;
            else
               update i_pcpc_040
               set qtde_programada = qtde_programada + exportar_pms.v_qtde_programada
               where ordem_producao = exportar_pms.ordem_producao
                 and referencia     = exportar_pms.proconf_grupo
                 and codigo_cor     = exportar_pms.proconf_item
                 and situacao_ordem = 4;
                 v_situacao_ordem := 0;
            end if;
         end if;

         if not v_tem_registro or v_situacao_ordem > 0
         then

            insert into i_pcpc_040
              (ordem_producao, referencia,
               lote_producao,  situacao_ordem,
               codigo_cor,     descricao_cor,
               qtde_programada)
              values
              (exportar_pms.ordem_producao,   exportar_pms.proconf_grupo,
               v_lote_producao,               v_situacao_ordem,
               exportar_pms.proconf_item,     v_descricao_cor,
               exportar_pms.v_qtde_programada);
         else
            update i_pcpc_040
            set qtde_programada = exportar_pms.v_qtde_programada
            where ordem_producao = exportar_pms.ordem_producao
              and referencia     = exportar_pms.proconf_grupo
              and codigo_cor     = exportar_pms.proconf_item
              and situacao_ordem = 0;
         end if;

         update pcpc_040
         set pcpc_040.exportado_pms = 'S'
         where pcpc_040.ordem_producao    = exportar_pms.ordem_producao
           and pcpc_040.periodo_producao  = exportar_pms.periodo_producao
           and pcpc_040.proconf_nivel99   = exportar_pms.proconf_nivel99
           and pcpc_040.proconf_grupo     = exportar_pms.proconf_grupo
           and pcpc_040.proconf_item      = exportar_pms.proconf_item
           and pcpc_040.codigo_estagio    = exportar_pms.codigo_estagio;

         commit;
      end if;
   end loop;
end inter_pr_pcpc_040_pms;
 

/

exec inter_pr_recompile;

