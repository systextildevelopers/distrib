
  CREATE OR REPLACE TRIGGER "INTER_TR_OBRF_658_LOG" 
before insert or
       delete or
       update on obrf_658
for each row

declare

  v_mes_old             obrf_658.mes%type;
  v_ano_old             obrf_658.ano%type;
  v_cod_empresa_old     obrf_658.cod_empresa%type;
  v_nivel_old           obrf_658.nivel%type;
  v_grupo_old           obrf_658.grupo%type;
  v_subgrupo_old        obrf_658.subgrupo%type;
  v_item_old            obrf_658.item%type;
  v_codigo_pais_old     obrf_658.item%type;
  v_estado_old          obrf_658.item%type;
  v_perc_desc_icms_old  obrf_658.item%type;

  v_mes_new             obrf_658.mes%type;
  v_ano_new             obrf_658.ano%type;
  v_cod_empresa_new     obrf_658.cod_empresa%type;
  v_nivel_new           obrf_658.nivel%type;
  v_grupo_new           obrf_658.grupo%type;
  v_subgrupo_new        obrf_658.subgrupo%type;
  v_item_new            obrf_658.item%type;
  v_codigo_pais_new     obrf_658.item%type;
  v_estado_new          obrf_658.item%type;
  v_perc_desc_icms_new  obrf_658.item%type;

  v_sid               sys.gv_$session.sid%type;
  v_empresa           number(3);
  v_usuario_systextil varchar2(250);
  v_locale_usuario    varchar2(5);
  v_nome_programa     varchar2(20);

  v_operacao          varchar(1);
  v_data_operacao     date;
  v_usuario_rede      varchar(20);
  v_maquina_rede      varchar(40);
  v_aplicativo        varchar(20);

begin

    begin
    -- Grava a data/hora da inser��o do registro (log)
    v_data_operacao := sysdate();

   -- Dados do usuario logado
    inter_pr_dados_usuario  (v_usuario_rede,        v_maquina_rede,   v_aplicativo,     v_sid,
                             v_usuario_systextil,   v_empresa,        v_locale_usuario);

    v_nome_programa := inter_fn_nome_programa(v_sid);

    --alimenta as vari�veis new caso seja insert ou update
    if inserting or updating
    then
       if inserting
       then v_operacao := 'I';
       else v_operacao := 'U';
       end if;

         v_mes_new             := :new.mes;
         v_ano_new             := :new.ano;
         v_cod_empresa_new     := :new.cod_empresa;
         v_nivel_new           := :new.nivel;
         v_grupo_new           := :new.grupo;
         v_subgrupo_new        := :new.subgrupo;
         v_item_new            := :new.item;
         v_codigo_pais_new     := :new.codigo_pais;
         v_estado_new          := :new.estado;
         v_perc_desc_icms_new  := :new.perc_desc_icms;


    end if; --fim do if inserting or updating

    --alimenta as vari�veis old caso seja insert ou update
    if deleting or updating
    then
       if deleting
       then v_operacao := 'D';
       else v_operacao := 'U';
       end if;

          v_mes_old             := :old.mes;
          v_ano_old             := :old.ano;
          v_cod_empresa_old     := :old.cod_empresa;
          v_nivel_old           := :old.nivel;
          v_grupo_old           := :old.grupo;
          v_subgrupo_old        := :old.subgrupo;
          v_item_old            := :old.item;
          v_codigo_pais_old     := :old.codigo_pais;
          v_estado_old          := :old.estado;
          v_perc_desc_icms_old  := :old.perc_desc_icms;

    end if; --fim do if inserting or updating

    --insere na obrf_658_log o registro
    insert into obrf_658_log
      (mes_new,            mes_old,
       ano_new,            ano_old,
       cod_empresa_new,    cod_empresa_old,
       nivel_new,           nivel_old,
       grupo_new,          grupo_old,
       subgrupo_new,       subgrupo_old,
       item_new,           item_old,
       codigo_pais_new,    codigo_pais_old,
       estado_new,         estado_old,
       perc_desc_icms_new,  perc_desc_icms_old,
       operacao,           data_operacao,
       usuario_rede,       maquina_rede,
       aplicativo,         nome_programa
       )
    values
      (v_mes_new,            v_mes_old,
       v_ano_new,            v_ano_old,
       v_cod_empresa_new,    v_cod_empresa_old,
       v_nivel_new,           v_nivel_old,
       v_grupo_new,          v_grupo_old,
       v_subgrupo_new,       v_subgrupo_old,
       v_item_new,           v_item_old,
       v_codigo_pais_new,    v_codigo_pais_old,
       v_estado_new,         v_estado_old,
       v_perc_desc_icms_new,  v_perc_desc_icms_old,
       v_operacao,           v_data_operacao,
       v_usuario_rede,       v_maquina_rede,
       v_aplicativo,         v_nome_programa
      );

    exception
       when OTHERS
       then raise_application_error (-20000, 'N�o atualizou a tabela de log da obrf_658.');

    end;
end inter_tr_obrf_658_log;
-- ALTER TRIGGER "INTER_TR_OBRF_658_LOG" ENABLE
 

/

exec inter_pr_recompile;

