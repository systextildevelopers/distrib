
  CREATE OR REPLACE TRIGGER "INTER_TR_OBRF_850_LOG" 
   after insert or delete or update
        of sit_fechamento
   on obrf_850
   for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_usuario_systextil      varchar2(250);
   ws_empresa                number(3);
   ws_locale_usuario         varchar2(5);

begin
	-- Dados do usuario logado
    inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                            ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

    insert into obrf_850_log (
       data_ocorrencia,            /*1*/
       maquina_rede,               /*2*/
       usuario_rede,               /*3*/
       usuario_systextil,          /*4*/
       terc_9,                     /*5*/
       terc_4,                     /*6*/
	     terc_2,                     /*7*/
       sequencia,                /*8*/
       situacao_fechamento_old,    /*9*/
       situacao_fechamento_new,    /*10*/
       data_fechamento_old,        /*11*/
       data_fechamento_new         /*12*/
    )values(
       sysdate,                    /*1*/
	   ws_maquina_rede,            /*2*/
	   ws_usuario_rede,            /*3*/
	   ws_usuario_systextil,       /*4*/
	   :new.terc_9,                /*5*/
	   :new.terc_4,                /*6*/
	   :new.terc_2,                /*7*/
	   :new.sequencia,             /*8*/
	   :old.sit_fechamento,        /*9*/
	   :new.sit_fechamento,        /*10*/
	   :old.data_fechamento,       /*11*/
	   :new.data_fechamento        /*12*/
    );

end inter_tr_obrf_850_log;

-- ALTER TRIGGER "INTER_TR_OBRF_850_LOG" ENABLE
 

/

exec inter_pr_recompile;

