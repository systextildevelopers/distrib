alter table supp_010
add ( requisicao       number(2) default 1,
      data_prorrogacao date,
      data_remessa     date,
      data_ret_supp    date,
      valor_lancto     number(15,2) 
);

exec inter_pr_recompile;
