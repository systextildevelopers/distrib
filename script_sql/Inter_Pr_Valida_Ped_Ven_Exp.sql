BEGIN
  EXECUTE IMMEDIATE 'DROP PROCEDURE Inter_Pr_Valida_Pedido_Venda_Exportacao';
EXCEPTION
  WHEN OTHERS THEN
    IF SQLCODE NOT IN (-4043) THEN -- A procedure não existe
      RAISE;
    END IF;
END;
/

create or replace procedure Inter_Pr_Valida_Ped_Ven_Exp
                           (p_mdi_empresa  in number
                          , p_pedido_venda in number
                          , p_retorno      out varchar2
                          , p_tag_retorno  out varchar2
                          , p_param_valida out varchar2
                           ) is
 v_pedi_100 PEDI_100%rowtype;
 v_qt       number;
begin

 p_param_valida := INTER_FN_GET_PARAM_STRING (p_empresa    => p_mdi_empresa
                                            , p_param_name => 'pedi.valida_ped_origem_filho_exportacao');
 if  p_param_valida = 'N' then
     p_retorno := 'OK';
     return;
 end if;

 select *
   into v_pedi_100
   from PEDI_100
  where pedido_venda = p_pedido_venda;
 if  v_pedi_100.natop_pv_est_oper != 'EX' then
     p_retorno := 'OK';
     return;
 end if;
 if  nvl(v_pedi_100.pedido_original,0) != 0 then
     p_retorno := 'ds60487#Pedido filho não pode ser cancelado/alterado.';--El pedido hijo no puede ser cancelado/modificado.
     p_tag_retorno := 'ds60487';
     return;
 end if;

 select count(*)
   into v_qt
   from PEDI_100
  where pedido_original = p_pedido_venda;
 if  v_qt > 1 then
     p_retorno :=  'ds60488#Pedido com mais de um filho não pode ser cancelado/alterado.';--El pedido con más de un hijo no puede ser cancelado/modificado.
     p_tag_retorno := 'ds60488';
     return;
 end if;

dbms_output.put_line('filhos: '||v_qt);

 begin
  select *
    into v_pedi_100
    from PEDI_100
   where pedido_original = p_pedido_venda;
dbms_output.put_line('v_pedi_100.pedido_venda: '||v_pedi_100.pedido_venda);
  select count(*)
    into v_qt
    from FATU_030
   where pedido_venda = v_pedi_100.pedido_venda;
  if  v_qt > 0 then
      p_retorno :=  'ds60489#Pedido com solicitação de faturamento não pode ser cancelado/alterado.';--El pedido con solicitud de facturación no puede ser cancelado/modificado.
      p_tag_retorno := 'ds60489';
      return;
  end if;
  
  select count(*)
    into v_qt
    from FATU_780
   where pedido = v_pedi_100.pedido_venda;
  if  v_qt > 0 then
      p_retorno := 'ds60490#Pedido sugerido e/ou coletado não pode ser cancelado/alterado. (FATU_780)';--Pedido sugerido y/o recolectado no puede ser cancelado/modificado. (FATU_780)
      p_tag_retorno := 'ds60490';
      return;
  end if;
  
  select count(*)
    into v_qt
    from PCPC_320
   where pedido_venda = v_pedi_100.pedido_venda;
  if  v_qt > 0 then
      p_retorno := 'ds60491#Pedido sugerido e/ou coletado não pode ser cancelado/alterado. (PCPC_320)';--Pedido sugerido y/o recolectado no puede ser cancelado/modificado. (PCPC_320)
      p_tag_retorno := 'ds60491';
      return;
  end if;
  
  select count(*)
    into v_qt
    from PEDI_110
   where pedido_venda  = v_pedi_100.pedido_venda
     and qtde_faturada = qtde_pedida;
  if  v_qt > 0 then
      p_retorno := 'ds60492#Pedido com item faturado não pode ser cancelado/alterado.';--El pedido con ítem facturado no puede ser cancelado/modificado.
      p_tag_retorno := 'ds60492';
      return;
  end if;
 exception
  when no_data_found then
   null;
 end;

 p_retorno := 'OK';

end Inter_Pr_Valida_Ped_Ven_Exp;
/
