
  CREATE OR REPLACE PROCEDURE "INTER_PR_UPDATE_PRODUCAO_PLAN" 
   (p_ordem_planejamento    in number,
    p_pedido_venda          in number,
    p_pedido_reserva        in number,
    p_nivel_produto         in varchar2,
    p_grupo_produto         in varchar2,
    p_subgrupo_produto      in varchar2,
    p_item_produto          in varchar2,
    p_qtde_update           in number,
    p_qtde_atendida_estoque in number,
    p_alternativa_produto   in number,
    p_codigo_risco          in number,
    p_area_producao         in number)
is
   -- Declara as variaveis que serao utilizadas.
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   v_count_reg_tmrp_625      number(6);
   v_flag                    number(1);
   v_tipo_produto            number(1);
   v_unidades_prog           number(9);
   v_qtde_necessidade        number(15,5);
   v_qtde_areceber           number(15,5);
   v_qtde_programar          number(15,5);
   v_saldo_programar         number(15,5);

   v_nivel_origem_lido       varchar2(1);
   v_grupo_origem_lido       varchar2(5);
   v_subgrupo_origem_lido    varchar2(3);
   v_item_origem_lido        varchar2(6);
   v_seq_origem_lido         number(3);
   v_alternativa_origem_lido number(2);
   v_seq_lido                number(3);
   v_qtde_total_superior     number(15,5);

begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   v_tipo_produto    := 0;
   v_saldo_programar := p_qtde_update;

   -- Obtem o numero de itens da ordem de planejamento
   -- que possuem o produto como componente.
   begin
      select nvl(count(1),0)
      into v_count_reg_tmrp_625
      from tmrp_625, tmrp_615
      where   tmrp_625.ordem_planejamento  = p_ordem_planejamento
        and   tmrp_625.pedido_venda        = p_pedido_venda
        and   tmrp_625.pedido_reserva      = p_pedido_reserva
        and   tmrp_625.nivel_produto       = p_nivel_produto
        and   tmrp_625.grupo_produto       = p_grupo_produto
        and   tmrp_625.subgrupo_produto    = p_subgrupo_produto
        and   tmrp_625.item_produto        = p_item_produto
        and  (tmrp_625.alternativa_produto = p_alternativa_produto or p_alternativa_produto = 0)
        and ((tmrp_615.area_producao       = decode(p_area_producao,2,1,
                                                                    4,2,
                                                                    7,4,
                                                                    9,9)
        and   tmrp_615.ordem_planejamento  = tmrp_625.ordem_planejamento
        and   tmrp_615.pedido_venda        = tmrp_625.pedido_venda
        and   tmrp_615.pedido_reserva      = tmrp_625.pedido_reserva
        and   tmrp_615.nivel               = tmrp_625.nivel_produto_origem
        and   tmrp_615.grupo               = tmrp_625.grupo_produto_origem
        and   tmrp_615.subgrupo            = tmrp_625.subgrupo_produto_origem
        and   tmrp_615.item                = tmrp_625.item_produto_origem
        and   tmrp_615.alternativa         = tmrp_625.alternativa_produto_origem
        and   tmrp_615.nome_programa       = 'trigger_planejamento'
        and   tmrp_615.nr_solicitacao      = 888
        and   tmrp_615.tipo_registro       = 888
        and   tmrp_615.cgc_cliente9        = ws_sid) or p_area_producao in (1));
   end;


   -- Verifica se o produto passado  como  parametro e
   -- um tecido retilineo. Em caso positivo, os  teci-
   -- dos crus tambem devem atualizar as unidades.
   if p_nivel_produto = '2'
   then
      begin
         select basi_030.tipo_produto
         into   v_tipo_produto
         from basi_030
         where basi_030.nivel_estrutura = p_nivel_produto
           and basi_030.referencia      = p_grupo_produto;
         exception when no_data_found then
            v_tipo_produto := 0;
      end;
   end if;

   begin
      select decode(sign(sum(tmrp_625.qtde_reserva_planejada) - sum(tmrp_625.qtde_reserva_programada)),
                    1,
                    sum(tmrp_625.qtde_reserva_planejada),
                    sum(tmrp_625.qtde_reserva_programada))
      into v_qtde_total_superior
      from tmrp_625, tmrp_615
      where  tmrp_625.ordem_planejamento  = p_ordem_planejamento
        and  tmrp_625.pedido_venda        = p_pedido_venda
        and  tmrp_625.pedido_reserva      = p_pedido_reserva
        and  tmrp_625.nivel_produto       = p_nivel_produto
        and  tmrp_625.grupo_produto       = p_grupo_produto
        and  tmrp_625.subgrupo_produto    = p_subgrupo_produto
        and  tmrp_625.item_produto        = p_item_produto
        and (tmrp_625.alternativa_produto = p_alternativa_produto or p_alternativa_produto = 0)
        and ((tmrp_615.area_producao      = decode(p_area_producao,2,1,
                                                                  4,2,
                                                                  7,4,
                                                                  9,9)
       and  tmrp_615.ordem_planejamento  = tmrp_625.ordem_planejamento
       and  tmrp_615.pedido_venda        = tmrp_625.pedido_venda
       and  tmrp_615.pedido_reserva      = tmrp_625.pedido_reserva
       and  tmrp_615.nivel               = tmrp_625.nivel_produto_origem
       and  tmrp_615.grupo               = tmrp_625.grupo_produto_origem
       and  tmrp_615.subgrupo            = tmrp_625.subgrupo_produto_origem
       and  tmrp_615.item                = tmrp_625.item_produto_origem
       and  tmrp_615.alternativa         = tmrp_625.alternativa_produto_origem
       and  tmrp_615.nome_programa       = 'trigger_planejamento'
       and  tmrp_615.nr_solicitacao      = 888
       and  tmrp_615.tipo_registro       = 888
       and  tmrp_615.cgc_cliente9        = ws_sid) or p_area_producao in (1));
       exception when others
       then v_qtde_total_superior := 1;
    end;

    if v_qtde_total_superior = 0
    then
       v_qtde_total_superior := 1;
    end if;

   for reg_tmrp625 in (select tmrp_625.nivel_produto_origem,         tmrp_625.grupo_produto_origem,
                              tmrp_625.subgrupo_produto_origem,      tmrp_625.item_produto_origem,
                              tmrp_625.seq_produto_origem,           tmrp_625.alternativa_produto_origem,
                              tmrp_625.consumo,                      tmrp_625.seq_produto,
                              tmrp_625.cons_unid_med_generica
                       from tmrp_625, tmrp_615
                       where  tmrp_625.ordem_planejamento  = p_ordem_planejamento
                         and  tmrp_625.pedido_venda        = p_pedido_venda
                         and  tmrp_625.pedido_reserva      = p_pedido_reserva
                         and  tmrp_625.nivel_produto       = p_nivel_produto
                         and  tmrp_625.grupo_produto       = p_grupo_produto
                         and  tmrp_625.subgrupo_produto    = p_subgrupo_produto
                         and  tmrp_625.item_produto        = p_item_produto
                         and (tmrp_625.alternativa_produto = p_alternativa_produto or p_alternativa_produto = 0)
                         and (tmrp_615.area_producao      = decode(p_area_producao,2,1,
                                                                                    4,2,
                                                                                    7,4,
                                                                                    9,9)
                         and  tmrp_615.ordem_planejamento  = tmrp_625.ordem_planejamento
                         and  tmrp_615.pedido_venda        = tmrp_625.pedido_venda
                         and  tmrp_615.pedido_reserva      = tmrp_625.pedido_reserva
                         and  tmrp_615.nivel               = tmrp_625.nivel_produto_origem
                         and  tmrp_615.grupo               = tmrp_625.grupo_produto_origem
                         and  tmrp_615.subgrupo            = tmrp_625.subgrupo_produto_origem
                         and  tmrp_615.item                = tmrp_625.item_produto_origem
                         and  tmrp_615.alternativa         = tmrp_625.alternativa_produto_origem
                         and  tmrp_615.nome_programa       = 'trigger_planejamento'
                         and  tmrp_615.nr_solicitacao      = 888
                         and  tmrp_615.tipo_registro       = 888
                         and  tmrp_615.cgc_cliente9        = ws_sid)
                       group by tmrp_625.nivel_produto_origem,         tmrp_625.grupo_produto_origem,
                                tmrp_625.subgrupo_produto_origem,      tmrp_625.item_produto_origem,
                                tmrp_625.seq_produto_origem,           tmrp_625.alternativa_produto_origem,
                                tmrp_625.consumo,                      tmrp_625.seq_produto,
                                tmrp_625.cons_unid_med_generica
                       union all
                       select tmrp_625.nivel_produto_origem,         tmrp_625.grupo_produto_origem,
                              tmrp_625.subgrupo_produto_origem,      tmrp_625.item_produto_origem,
                              tmrp_625.seq_produto_origem,           tmrp_625.alternativa_produto_origem,
                              tmrp_625.consumo,                      tmrp_625.seq_produto,
                              tmrp_625.cons_unid_med_generica
                       from tmrp_625
                       where  tmrp_625.ordem_planejamento  = p_ordem_planejamento
                         and  tmrp_625.pedido_venda        = p_pedido_venda
                         and  tmrp_625.nivel_produto       = p_nivel_produto
                         and  tmrp_625.grupo_produto       = p_grupo_produto
                         and  tmrp_625.subgrupo_produto    = p_subgrupo_produto
                         and  tmrp_625.item_produto        = p_item_produto
                         and (tmrp_625.alternativa_produto = p_alternativa_produto or p_alternativa_produto = 0)
                         and p_area_producao in (1)
                       group by tmrp_625.nivel_produto_origem,         tmrp_625.grupo_produto_origem,
                                tmrp_625.subgrupo_produto_origem,      tmrp_625.item_produto_origem,
                                tmrp_625.seq_produto_origem,           tmrp_625.alternativa_produto_origem,
                                tmrp_625.consumo,                      tmrp_625.seq_produto,
                                tmrp_625.cons_unid_med_generica)
   loop
      v_flag := 1;

      v_nivel_origem_lido       := reg_tmrp625.nivel_produto_origem;
      v_grupo_origem_lido       := reg_tmrp625.grupo_produto_origem;
      v_subgrupo_origem_lido    := reg_tmrp625.subgrupo_produto_origem;
      v_item_origem_lido        := reg_tmrp625.item_produto_origem;
      v_seq_origem_lido         := reg_tmrp625.seq_produto_origem;
      v_alternativa_origem_lido := reg_tmrp625.alternativa_produto_origem;
      v_seq_lido                := reg_tmrp625.seq_produto;

      if v_count_reg_tmrp_625 > 1
      then
         begin
            select decode(sign(sum(tmrp_625.qtde_reserva_planejada) - sum(tmrp_625.qtde_reserva_programada)),
                          1,
                          sum(tmrp_625.qtde_reserva_planejada),
                          sum(tmrp_625.qtde_reserva_programada)),sum(tmrp_625.qtde_areceber_programada)
            into v_qtde_necessidade, v_qtde_areceber
            from tmrp_625
            where  tmrp_625.ordem_planejamento         = p_ordem_planejamento
              and  tmrp_625.pedido_venda               = p_pedido_venda
              and  tmrp_625.pedido_reserva             = p_pedido_reserva
              and  tmrp_625.nivel_produto_origem       = reg_tmrp625.nivel_produto_origem
              and  tmrp_625.grupo_produto_origem       = reg_tmrp625.grupo_produto_origem
              and  tmrp_625.subgrupo_produto_origem    = reg_tmrp625.subgrupo_produto_origem
              and  tmrp_625.item_produto_origem        = reg_tmrp625.item_produto_origem
              and  tmrp_625.seq_produto_origem         = reg_tmrp625.seq_produto_origem
              and  tmrp_625.alternativa_produto_origem = reg_tmrp625.alternativa_produto_origem
              and  tmrp_625.nivel_produto              = p_nivel_produto
              and  tmrp_625.grupo_produto              = p_grupo_produto
              and  tmrp_625.subgrupo_produto           = p_subgrupo_produto
              and  tmrp_625.item_produto               = p_item_produto
              and (tmrp_625.alternativa_produto        = p_alternativa_produto or p_alternativa_produto = 0)
              and  tmrp_625.seq_produto                = reg_tmrp625.seq_produto;
            exception when no_data_found
            then v_flag := 0;
         end;

         if v_flag = 1
         then
            v_qtde_programar := (p_qtde_update * (((v_qtde_necessidade) * 100) / v_qtde_total_superior)) / 100;
            v_saldo_programar := v_saldo_programar - v_qtde_programar;

            if v_tipo_produto = 3 or v_tipo_produto = 4 or v_tipo_produto = 6
            then
               v_unidades_prog := round(v_qtde_programar) * reg_tmrp625.cons_unid_med_generica;
            else
               v_unidades_prog := 0;
            end if;
         else
            v_unidades_prog := 0;
            v_qtde_programar := 0;
         end if;
      else
         v_qtde_programar  := v_saldo_programar;
         v_saldo_programar := 0;
      end if;

      begin
         update tmrp_625
         set tmrp_625.qtde_areceber_programada     = tmrp_625.qtde_areceber_programada    + v_qtde_programar,
             tmrp_625.unidades_reserva_programada  = tmrp_625.unidades_reserva_programada + v_unidades_prog,
             tmrp_625.qtde_atendida_estoque        = tmrp_625.qtde_atendida_estoque       + p_qtde_atendida_estoque
         where  tmrp_625.ordem_planejamento         = p_ordem_planejamento
           and  tmrp_625.pedido_venda               = p_pedido_venda
           and  tmrp_625.pedido_reserva             = p_pedido_reserva
           and  tmrp_625.nivel_produto_origem       = reg_tmrp625.nivel_produto_origem
           and  tmrp_625.grupo_produto_origem       = reg_tmrp625.grupo_produto_origem
           and  tmrp_625.subgrupo_produto_origem    = reg_tmrp625.subgrupo_produto_origem
           and  tmrp_625.item_produto_origem        = reg_tmrp625.item_produto_origem
           and  tmrp_625.seq_produto_origem         = reg_tmrp625.seq_produto_origem
           and  tmrp_625.alternativa_produto_origem = reg_tmrp625.alternativa_produto_origem
           and  tmrp_625.nivel_produto              = p_nivel_produto
           and  tmrp_625.grupo_produto              = p_grupo_produto
           and  tmrp_625.subgrupo_produto           = p_subgrupo_produto
           and  tmrp_625.item_produto               = p_item_produto
           and (tmrp_625.alternativa_produto        = p_alternativa_produto or p_alternativa_produto = 0)
           and  tmrp_625.seq_produto                = reg_tmrp625.seq_produto;
      exception when others then
         -- ATENCAO! Nao atualizou a tabela {0} . Status = {1}
         raise_application_error(-20000,inter_fn_buscar_tag_composta('ds24385', 'TMRP_625(inter_pr_update_producao_plan(1))' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;

      begin
         delete tmrp_615
         where tmrp_615.nome_programa      = 'trigger_planejamento'
           and tmrp_615.nr_solicitacao     = 888
           and tmrp_615.tipo_registro      = 888
           and tmrp_615.cgc_cliente9       = ws_sid
           and tmrp_615.nivel_prod         = p_nivel_produto
           and tmrp_615.grupo_prod         = p_grupo_produto
           and tmrp_615.subgrupo_prod      = p_subgrupo_produto
           and tmrp_615.item_prod          = p_item_produto
           and tmrp_615.ordem_planejamento = p_ordem_planejamento
           and tmrp_615.pedido_venda       = p_pedido_venda
           and tmrp_615.pedido_reserva     = p_pedido_reserva
           and tmrp_615.alternativa_prod   = p_alternativa_produto
           and tmrp_615.seq_registro       = reg_tmrp625.seq_produto
           and tmrp_615.nivel              = reg_tmrp625.nivel_produto_origem
           and tmrp_615.grupo              = reg_tmrp625.grupo_produto_origem
           and tmrp_615.subgrupo           = reg_tmrp625.subgrupo_produto_origem
           and tmrp_615.item               = reg_tmrp625.item_produto_origem
           and tmrp_615.alternativa        = reg_tmrp625.alternativa_produto_origem
           and tmrp_615.sequencia          = reg_tmrp625.seq_produto_origem;
      end;

      inter_pr_update_reserva_plan(p_ordem_planejamento,
                                   p_pedido_venda,
                                   p_pedido_reserva,
                                   p_nivel_produto,
                                   p_grupo_produto,
                                   p_subgrupo_produto,
                                   p_item_produto,
                                   p_alternativa_produto,
                                   v_qtde_programar,
                                   reg_tmrp625.seq_produto,
                                   p_codigo_risco);
   end loop;

   if abs(v_saldo_programar) > 0
   then
      begin
         update tmrp_625
         set tmrp_625.qtde_areceber_programada     = tmrp_625.qtde_areceber_programada    + v_saldo_programar,
             tmrp_625.unidades_reserva_programada  = tmrp_625.unidades_reserva_programada + v_unidades_prog,
             tmrp_625.qtde_atendida_estoque        = tmrp_625.qtde_atendida_estoque       + p_qtde_atendida_estoque
         where  tmrp_625.ordem_planejamento         = p_ordem_planejamento
           and  tmrp_625.pedido_venda               = p_pedido_venda
           and  tmrp_625.pedido_reserva             = p_pedido_reserva
           and  tmrp_625.nivel_produto_origem       = v_nivel_origem_lido
           and  tmrp_625.grupo_produto_origem       = v_grupo_origem_lido
           and  tmrp_625.subgrupo_produto_origem    = v_subgrupo_origem_lido
           and  tmrp_625.item_produto_origem        = v_item_origem_lido
           and  tmrp_625.seq_produto_origem         = v_seq_origem_lido
           and  tmrp_625.alternativa_produto_origem = v_alternativa_origem_lido
           and  tmrp_625.nivel_produto              = p_nivel_produto
           and  tmrp_625.grupo_produto              = p_grupo_produto
           and  tmrp_625.subgrupo_produto           = p_subgrupo_produto
           and  tmrp_625.item_produto               = p_item_produto
           and (tmrp_625.alternativa_produto        = p_alternativa_produto or p_alternativa_produto = 0)
           and  tmrp_625.seq_produto                = v_seq_lido;
         exception when others then
            -- ATENCAO! Nao atualizou a tabela {0} . Status = {1}
            raise_application_error(-20000,inter_fn_buscar_tag_composta('ds24385', 'TMRP_625(inter_pr_update_producao_plan(2))' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;

      inter_pr_update_reserva_plan(p_ordem_planejamento,
                                   p_pedido_venda,
                                   p_pedido_reserva,
                                   p_nivel_produto,
                                   p_grupo_produto,
                                   p_subgrupo_produto,
                                   p_item_produto,
                                   p_alternativa_produto,
                                   v_saldo_programar, --v_qtde_programar,
                                   v_seq_lido,
                                   p_codigo_risco);
   end if;
end INTER_PR_UPDATE_PRODUCAO_PLAN;



 

/

exec inter_pr_recompile;

