create table basi_266 (
	cod_empresa 	number(3) default 0 not null,
	tipo_titulo 	number(2) default 0 not null,
	cod_moeda 		number(2) default 0 not null
);

alter table basi_266 add constraint pk_basi_266 primary key (cod_empresa, tipo_titulo, cod_moeda);
