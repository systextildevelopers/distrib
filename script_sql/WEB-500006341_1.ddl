alter table fatu_050
add nr_declaracao_exp_sped_1 varchar2(14);

/

declare
  cont_reg number;
begin
    cont_reg := 0;
  
    for fatu050 in (
      select rowid from fatu_050
      where fatu_050.nr_declaracao_exp_sped_1 is null
    )
    loop
    
        update fatu_050
        set nr_declaracao_exp_sped_1 = nr_declaracao_exp_sped
        where rowid = fatu050.rowid;     
    
        cont_reg := cont_reg + 1;

        if cont_reg = 1000
        then
           commit;
           cont_reg := 0;
        end if;
    end loop;
  commit;
end;

/

alter table fatu_050
drop column nr_declaracao_exp_sped;

/

alter table fatu_050
rename column nr_declaracao_exp_sped_1 to nr_declaracao_exp_sped;

/

exec inter_pr_recompile;

/
