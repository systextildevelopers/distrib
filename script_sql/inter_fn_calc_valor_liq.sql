
  CREATE OR REPLACE FUNCTION "INTER_FN_CALC_VALOR_LIQ" 
(pValor in number, pPercentual in number)
return number is
  Result number;
begin
  Result := pValor - ((pValor * pPercentual) / 100.00);
  return(Result);
end inter_fn_calc_valor_liq;

 

/

exec inter_pr_recompile;

