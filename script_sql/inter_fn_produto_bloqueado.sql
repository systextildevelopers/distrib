create or replace FUNCTION "INTER_FN_PRODUTO_BLOQUEADO" (v_nivel_estrutura in varchar2, v_grupo_estrutura in varchar2, 
                                                             v_sub_estrutura in varchar2,   v_item_estrutura in varchar2,
                                                             v_numero_lote in number)
	return boolean
is 
   produtoBloqueado boolean;
   tmp_sql number;
begin
    produtoBloqueado := false;
    tmp_sql := 0;

    if v_nivel_estrutura = '9' or v_nivel_estrutura = '7' then
        produtoBloqueado := true;

        begin
            select 1 
            into tmp_sql
            from basi_400 
            where  basi_400.nivel           = v_nivel_estrutura
            and  basi_400.grupo             = v_grupo_estrutura
            and  basi_400.subgrupo          = v_sub_estrutura
            and  basi_400.item              = v_item_estrutura
            and  basi_400.tipo_informacao   = 55 
            and  basi_400.valor_15          = 0 --Fio bloqueado
            and  basi_400.valor_05          = v_numero_lote;
        exception when OTHERS then
            produtoBloqueado := false;
        end;
    else
        produtoBloqueado := false;
    end if;

   return(produtoBloqueado); 
end inter_fn_produto_bloqueado;
 

/

exec inter_pr_recompile;

