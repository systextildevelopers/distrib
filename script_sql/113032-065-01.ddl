create table tmp_disponibilidade (
  nr_solicitacao 		number(9) 	not null, 
  data_criacao 			date 		not null, 
  nivel 				varchar2(1) not null, 
  grupo 				varchar2(5) not null, 
  subgru 				varchar2(3) not null, 
  item 					varchar2(6) not null, 
  narrativa 			varchar2(65), 
  marcar_todos 		    number(1), 
  data_disponibilidade 	date
);

alter table tmp_disponibilidade
	add CONSTRAINT tmp_disponibilidade_PK PRIMARY KEY (nr_solicitacao, data_criacao, nivel, grupo, subgru, item);
	

exec inter_pr_recompile;
