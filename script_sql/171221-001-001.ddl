alter table list_critica_estoque add (
difer_vol_kardex    number(15,3),
difer_sld_kardex    number(15,3),
difer_vol_sld       number(15,3));

alter table list_critica_estoque modify difer_vol_kardex    default 0.00;
alter table list_critica_estoque modify difer_sld_kardex    default 0.00;
alter table list_critica_estoque modify difer_vol_sld default 0.00;
