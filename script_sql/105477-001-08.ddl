insert into basi_056 (unid_med_trib, descr_unid_med_trib) values ('UN','UNIDADE');
insert into basi_056 (unid_med_trib, descr_unid_med_trib) values ('1000UN', 'MIL UNIDADES');
insert into basi_056 (unid_med_trib, descr_unid_med_trib) values ('G','GRAMA');
insert into basi_056 (unid_med_trib, descr_unid_med_trib) values ('JOGO','JOGO');
insert into basi_056 (unid_med_trib, descr_unid_med_trib) values ('KG','QUILOGRAMA');
insert into basi_056 (unid_med_trib, descr_unid_med_trib) values ('LT','LITRO');
insert into basi_056 (unid_med_trib, descr_unid_med_trib) values ('M2','METRO QUADRADO');
insert into basi_056 (unid_med_trib, descr_unid_med_trib) values ('M3','METRO CUBICO');
insert into basi_056 (unid_med_trib, descr_unid_med_trib) values ('METRO','METRO');
insert into basi_056 (unid_med_trib, descr_unid_med_trib) values ('MWHORA','MEGAWATT HORA');
insert into basi_056 (unid_med_trib, descr_unid_med_trib) values ('PARES','PARES');
insert into basi_056 (unid_med_trib, descr_unid_med_trib) values ('QUILAT','QUILATE');

commit;
/
