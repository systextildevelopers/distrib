create or replace trigger "INTER_TR_FATU_030_GFRETE"
after update
    of posicao_fatura
on fatu_030
for each row
declare
   Pragma Autonomous_Transaction;

   utiliza_integracao varchar2(1) := 'S';
   tempo_analise_nf   number(3)   := 0;
   transp9_orginal    number(9)   := 0;
   transp4_orginal    number(4)   := 0;
   transp2_orginal    number(2)   := 0;
   transp9_integrada  number(9)   := 0;
   transp4_integrada  number(4)   := 0;
   transp2_integrada  number(2)   := 0;
   sit_nota_integrada number(1)   := 0;
   reg_supr           number(1)   := 0;

   cgc_9              fatu_050.cgc_9%type;
   cgc_4              fatu_050.cgc_4%type;
   cgc_2              fatu_050.cgc_2%type;
   nome_cliente       pedi_010.nome_cliente%type;
   cep_cliente        pedi_010.cep_cliente%type;
   cidade_cliente     basi_160.cidade%type;
   estado_cliente     basi_160.estado%type;
   peso_liquido       fatu_050.peso_liquido%type;
   peso_bruto         fatu_050.peso_bruto%type;
   peso_liquido_rec   fatu_050.peso_liquido%type;
   peso_bruto_rec     fatu_050.peso_bruto%type;
   qtde_itens         fatu_050.qtde_itens%type;
   especie_volume     fatu_050.especie_volume%type;
   transpor_forne9    fatu_050.transpor_forne9%type;
   transpor_forne4    fatu_050.transpor_forne4%type;
   transpor_forne2    fatu_050.transpor_forne2%type;
   data_emissao       fatu_050.data_emissao%type;
   valor_total        number(20,3) := 0.000;
   metros_cubicos_emb fatu_100.metros_cubicos_emb%type;
   v_valor_frete        number(20,3) := 0.000;
   num_nota_fiscal    fatu_050.num_nota_fiscal%type;
   serie_nota_fisc    fatu_050.serie_nota_fisc%type;
   situacao_pedido    pedi_100.situacao_venda%type;
   trans_redespacho9  pedi_100.trans_re_forne9%type;
   trans_redespacho4  pedi_100.trans_re_forne9%type;
   trans_redespacho2  pedi_100.trans_re_forne9%type;
   cep_transp_re      pedi_010.cep_cliente%type;
   cidade_transp_re   basi_160.cidade%type;
   estado_transp_re   basi_160.estado%type;
   cep                pedi_010.cep_cliente%type;
   cidade             basi_160.cidade%type;
   estado             basi_160.estado%type;
   existe_pedido      number(1) := 1;
   existe_nota        number(1) := 1;
   existe_transp_re   number(1) := 1;
   cod_emp            number(3);

   flag_ok            varchar2(1) := 'S';
   flag_existe_070    number(9)   := 0;
   flag_integracao    varchar2(1) := 'S';
   nro_reg            number;
   contador           number(2) := 0;
   achou_transp       varchar2(1) := 'N';
   qtde_emb_nova      number(3) := 0;
   tot_vol            number(3) := null;
   contador_vol       number(3) := 0;
   existe_vol         number(9) := 0;
   qtde_emb_new       number(9) := 0;

   cursor c_NFe is  -- NOTAS
       select fatu_050.cgc_9,              fatu_050.cgc_4,
                    fatu_050.cgc_2,              pedi_010.nome_cliente,
                    pedi_010.cep_cliente,        basi_160.cidade as cidade_cliente,
                    basi_160.estado as estado_cliente,             fatu_050.peso_liquido,
                    fatu_050.peso_bruto,         fatu_050.qtde_embalagens as qtde_itens,
                    fatu_050.especie_volume,     fatu_050.transpor_forne9,
                    fatu_050.transpor_forne4,    fatu_050.transpor_forne2,
                    fatu_050.data_emissao,
                   (fatu_050.valor_itens_nfis + fatu_050.valor_ipi      + fatu_050.valor_encar_nota +
                    fatu_050.valor_frete_nfis + fatu_050.valor_seguro   - fatu_050.valor_desc_nota -
                    fatu_050.valor_suframa    + fatu_050.valor_despesas - fatu_050.vlr_desc_especial +
                    fatu_050.valor_icms_sub) as valor_total,
                    fatu_100.metros_cubicos_emb, fatu_050.num_nota_fiscal,
                    fatu_050.serie_nota_fisc
             from fatu_050, pedi_010, basi_160, fatu_100
             where fatu_050.cgc_9            = pedi_010.cgc_9
               and fatu_050.cgc_4            = pedi_010.cgc_4
               and fatu_050.cgc_2            = pedi_010.cgc_2
               and fatu_050.codigo_embalagem = fatu_100.codigo_embalagem (+)
               and pedi_010.cod_cidade       = basi_160.cod_cidade
               and fatu_050.pedido_venda  = :new.pedido_venda
               and fatu_050.serie_nota_fisc  = :new.serie_nota_fisc
               and fatu_050.tipo_frete       = 1
               and fatu_050.situacao_nfisc   = 0;

begin
   begin
     select pedi_100.codigo_empresa
     into cod_emp
     from pedi_100
     where pedi_100.pedido_venda = :new.pedido_venda;
   end;

   begin
      select fatu_503.analisa_transpor_nf, fatu_503.tempo_analise_transpor_nf
      into   utiliza_integracao,           tempo_analise_nf
      from fatu_503
      where fatu_503.analisa_transpor_nf = 'S'
      and fatu_503.codigo_empresa = cod_emp;
   exception
   when OTHERS then
      utiliza_integracao := 'N';
      tempo_analise_nf := 0;
   end;
   
   if :new.analisa_transportadora is not null and utiliza_integracao = 'S' and utiliza_integracao <> :new.analisa_transportadora 
   then 
       utiliza_integracao := :new.analisa_transportadora;
   end if;

   if utiliza_integracao = 'S' and :new.posicao_fatura = 3
   then
      flag_ok := 'S';
      flag_existe_070 := 0;

      -- Verifica se existe informacao para a nota e serie na
      -- tabela de integracao.

      begin
         select count(1)
         into flag_existe_070
         from inte_070
         where inte_070.numero_nf = :new.num_nota_fiscal
           and inte_070.serie_nf  = :new.serie_nota_fisc;
      exception
      when OTHERS then
         flag_existe_070 := 0;
      end;

      -- Se existe o registro para a nota na tabela de integracao,
      -- atualiza a situacao do registro de integracao para
      -- 0 - A INTEGRAR
      if flag_existe_070 > 0
      then
         begin
            update inte_070
               set inte_070.situacao = 0
            where inte_070.numero_nf = :new.num_nota_fiscal
              and inte_070.serie_nf  = :new.serie_nota_fisc
              and inte_070.situacao <> 9;
         exception
         when OTHERS then
            flag_ok := 'N';
         end;
      else
         begin

            select count(1)
            into existe_pedido
            from pedi_100
            where pedi_100.pedido_venda = :new.pedido_venda;

            exception
            when others then
               existe_pedido := 0;
         end;
         
        BEGIN
              SELECT fatu_100.METROS_CUBICOS_EMB*(fatu_030.QTDE_EMBALAGENS),
                     (fatu_030.PESO_BRUTO + fatu_030.PESO_LIQUIDO),
                     fatu_030.PESO_LIQUIDO,fatu_030.valor_frete
              INTO metros_cubicos_emb, peso_bruto_rec, peso_liquido_rec,
                   v_valor_frete
              FROM fatu_030
                LEFT JOIN fatu_100 ON (fatu_030.CODIGO_EMBALAGEM = fatu_100.CODIGO_EMBALAGEM)
              WHERE fatu_030.PEDIDO_VENDA = :new.pedido_venda;
            EXCEPTION WHEN OTHERS
              THEN 
                metros_cubicos_emb := 0;
                peso_bruto_rec := 0.00;
                peso_liquido_rec := 0.00;
         END;

         if existe_pedido > 0
         then

            trans_redespacho9 := 0;
            trans_redespacho4 := 0;
            trans_redespacho2 := 0;

            begin
               select pedi_100.situacao_venda,  pedi_100.trans_re_forne9,
                      pedi_100.trans_re_forne4, pedi_100.trans_re_forne2
                 into situacao_pedido,          trans_redespacho9,
                      trans_redespacho4,        trans_redespacho2
               from pedi_100
               where pedi_100.pedido_venda = :new.pedido_venda;
            end;
            existe_nota := 1;
               begin
                  for f_nfe in c_NFe
                  loop
                    if existe_nota = 1
                    then
                       begin
                         select  nvl(count(*),0) into nro_reg from fatu_050
                         where fatu_050.pedido_venda = :new.pedido_venda
                         and fatu_050.situacao_nfisc = 0;
                       end;
                       cep    := f_nfe.cep_cliente;
                       cidade := f_nfe.cidade_cliente;
                       estado := f_nfe.estado_cliente;
                       if nro_reg > 1
                         then
                           contador_vol := contador_vol + 1;
                             begin
                                 select sum(pcpc_320.peso_volume),
                                 sum(pcpc_320.peso_embalagem + pcpc_320.peso_volume) as peso_bruto
                                 into peso_liquido_rec,           peso_bruto_rec
                                 from pcpc_320
                                 where pcpc_320.nota_fiscal = f_nfe.num_nota_fiscal
                                   and pcpc_320.pedido_venda = :new.pedido_venda
                                   and pcpc_320.serie_nota  = f_nfe.serie_nota_fisc;
                                 exception
                                 when OTHERS then
                                    peso_liquido_rec := :new.peso_liquido / nro_reg;
                                    peso_bruto_rec := (:new.peso_liquido + :new.peso_bruto) / nro_reg;
                              end;
                              

                              
                              begin
                                 select pcpc_320.nota_fiscal
                                 into existe_vol
                                 from pcpc_320
                                 where pcpc_320.pedido_venda = :new.pedido_venda
                                   and rownum = 1;
                                 exception
                                 when OTHERS then
                                    existe_vol := 0;
                              end;
                              
                              if existe_vol <> 0 and existe_vol is not null
                              then
                              begin
                                   select nvl(count(*),0) into qtde_emb_new 
                                   from pcpc_320
                                   where pcpc_320.nota_fiscal = f_nfe.num_nota_fiscal
                                     and pcpc_320.pedido_venda = :new.pedido_venda
                                     and pcpc_320.serie_nota  = f_nfe.serie_nota_fisc;
                                   exception
                                   when OTHERS then
                                      qtde_emb_new := 0;
                                end;
                              end if;
                                          
                              if peso_liquido_rec is null
                              then
                                peso_liquido_rec := :new.peso_liquido / nro_reg;
                              end if;
                              
                              if peso_bruto_rec is null
                              then
                                peso_bruto_rec := (:new.peso_liquido + :new.peso_bruto) / nro_reg;
                              end if;
                              
                              if existe_vol = 0 and contador_vol = 1
                              then
                                qtde_emb_nova := :new.qtde_embalagens - (nro_reg - 1);
                              end if;
                              
                              if existe_vol = 0 and contador_vol > 1
                              then
                                qtde_emb_nova := 1;
                              end if;
                              
                              if existe_vol = 0
                              then
                                  begin
                                    update fatu_050
                                    set fatu_050.qtde_embalagens  = qtde_emb_nova
                                     where fatu_050.num_nota_fiscal = f_nfe.num_nota_fiscal
                                       and fatu_050.serie_nota_fisc = f_nfe.serie_nota_fisc
                                       and fatu_050.situacao_nfisc   = 0;
                                     exception
                                     when OTHERS then
                                        sit_nota_integrada := 1;
                                        raise_application_error(-20000,'Erro ao atualizar a quantidade de embalagens.');
                                     commit;
                                  end;
                              end if;
                              
                              
                             if existe_vol <> 0
                              then
                                  begin
                                    update fatu_050
                                    set fatu_050.qtde_embalagens  = qtde_emb_new
                                     where fatu_050.num_nota_fiscal = f_nfe.num_nota_fiscal
                                       and fatu_050.serie_nota_fisc = f_nfe.serie_nota_fisc
                                       and fatu_050.situacao_nfisc   = 0;
                                     exception
                                     when OTHERS then
                                        sit_nota_integrada := 1;
                                        raise_application_error(-20000,'Erro ao atualizar a quantidade de embalagens.');
                                     commit;
                                  end;
                              end if;
                                     
                              begin
                                update fatu_050
                                set fatu_050.peso_bruto  = peso_bruto_rec, fatu_050.peso_liquido  = peso_liquido_rec
                                 where fatu_050.num_nota_fiscal = f_nfe.num_nota_fiscal
                                   and fatu_050.serie_nota_fisc = f_nfe.serie_nota_fisc
                                   and fatu_050.situacao_nfisc   = 0;
                                 exception
                                 when OTHERS then
                                    sit_nota_integrada := 1;
                                    raise_application_error(-20000,'Erro ao atualizar os pesos da nota fiscal.');
                                 commit;
                              end;
                       end if;

                       if trans_redespacho9 <> 0 or trans_redespacho4 <> 0 or trans_redespacho2 <> 0
                       then

                          existe_transp_re := 1;

                          begin
                             select supr_010.cep_fornecedor, basi_160.cidade,
                                    basi_160.estado
                               into cep_transp_re,           cidade_transp_re,
                                    estado_transp_re
                             from supr_010, basi_160
                             where supr_010.fornecedor9 = trans_redespacho9
                               and supr_010.fornecedor4 = trans_redespacho4
                               and supr_010.fornecedor2 = trans_redespacho2
                               and supr_010.cod_cidade  = basi_160.cod_cidade;
                             exception
                             when OTHERS then
                                existe_transp_re := 0;
                          end;

                          if existe_transp_re = 1
                          then
                             cep    := cep_transp_re;
                             cidade := cidade_transp_re;
                             estado := estado_transp_re;
                          end if;
                       end if;

                       -- Se nao encontrou o registro de intregracao, insere o registro para
                       -- analise e escolha da transportadora.
                       begin
                          INSERT INTO inte_070
                             (cnpj_cliente9,
                             cnpj_cliente4,
                             cnpj_cliente2,
                             nome_cliente,
                             cep_cliente,
                             cidade_cliente,
                             uf_cliente,
                             peso_total_liq,
                             peso_total_bru,
                             qtd_volumes,
                             tipo_volume,
                             cnpj_trans_pedi9,
                             cnpj_trans_pedi4,
                             cnpj_trans_pedi2,
                             data_remessa,
                             valor_total_nf,
                             volume,
                             numero_nf,
                             serie_nf,
                             pedido_venda,
                             situacao_pedido,
                             situacao)
                          VALUES
                             (f_nfe.cgc_9,
                             f_nfe.cgc_4,
                             f_nfe.cgc_2,
                             f_nfe.nome_cliente,
                             cep,
                             cidade,
                             estado,
                             decode(peso_liquido_rec,null,f_nfe.peso_liquido, peso_liquido_rec),
                             decode(peso_bruto_rec,null, f_nfe.peso_bruto, peso_bruto_rec),
                             f_nfe.qtde_itens,
                             f_nfe.especie_volume,
                             f_nfe.transpor_forne9,
                             f_nfe.transpor_forne4,
                             f_nfe.transpor_forne2,
                             f_nfe.data_emissao,
                             f_nfe.valor_total,
                             metros_cubicos_emb,
                             f_nfe.num_nota_fiscal,
                             f_nfe.serie_nota_fisc,
                             :new.pedido_venda,
                             situacao_pedido,
                             0);
                          exception
                          when OTHERS then
                             flag_ok := 'N';
                       end;
                       end if;
                       if flag_ok = 'S' and  existe_nota = 1
                       then
                          commit;

                          if tempo_analise_nf is null
                          then
                             tempo_analise_nf := 0;
                             raise_application_error(-20000,'Problema na carga do tempo para integracao "Gfrete".');
                          else
                            while contador < 5 and achou_transp = 'N'
                             loop
                               begin
                               inter_pr_sleep(tempo_analise_nf /10);
                               achou_transp := 'S';
                               -- CNPJ TRANSP, transportadora para a nota.
                                 select inte_070.cnpj_transp9,     inte_070.cnpj_transp4,
                                        inte_070.cnpj_transp2,     inte_070.cnpj_trans_pedi9,
                                        inte_070.cnpj_trans_pedi4, inte_070.cnpj_trans_pedi2
                                 into   transp9_integrada,         transp4_integrada,
                                        transp2_integrada,         transp9_orginal,
                                        transp4_orginal,           transp2_orginal
                                 from inte_070
                                 where inte_070.numero_nf = f_nfe.num_nota_fiscal
                                   and inte_070.serie_nf  = f_nfe.serie_nota_fisc
                                   and inte_070.situacao  = 2;
                                 exception
                                 when no_data_found then
                                    flag_integracao := 'N';
                                    transp9_integrada := 0;
                                    transp4_integrada := 0;
                                    transp2_integrada := 0;
                                    transp9_orginal := 0;
                                    transp4_orginal := 0;
                                    transp2_orginal := 0;
                                    achou_transp := 'N';
                                  end;
                               contador := contador + 1;
                             end loop;
                          end if;
                          
                          flag_integracao := 'S';
                          sit_nota_integrada := 1;


                          if flag_integracao = 'S'
                          then
                             -- Se a transportadora "integrada", no Gfrete, retornou 0(zero)
                             -- nao atualiza a capa da nota fiscal.
                             if transp9_integrada = 0 and transp4_integrada = 0 and transp2_integrada = 0
                             then
                                -- 1 - NAO INTEGRADO
                                sit_nota_integrada := 1;
                             else
                                -- 9 - INTEGRADO
                                sit_nota_integrada := 9;

                                select count(supr_010.nome_fornecedor)
                                into reg_supr
                                from supr_010
                                where supr_010.fornecedor9 = transp9_integrada
                                  and supr_010.fornecedor4 = transp4_integrada
                                  and supr_010.fornecedor2 = transp2_integrada;

                                if reg_supr = 0
                                then raise_application_error(-20000,'A transportadora integrada n?o est? cadastrada no Systextil. Favor verificar.');
                                end if;

                                begin
                                  update fatu_050
                                  set fatu_050.transpor_forne9 = transp9_integrada,
                                       fatu_050.transpor_forne4 = transp4_integrada,
                                       fatu_050.transpor_forne2 = transp2_integrada
                                   where fatu_050.num_nota_fiscal = f_nfe.num_nota_fiscal
                                     and fatu_050.serie_nota_fisc = f_nfe.serie_nota_fisc
                                     and fatu_050.situacao_nfisc   = 0;
                                   exception
                                   when OTHERS then
                                      sit_nota_integrada := 1;
                                      raise_application_error(-20000,'Erro ao atualizar a transportadora "GFrete" para a nota fiscal.');
                                   commit;

                                end;
                                /*marlan temp*/
                               -- begin
                                  -- update fatu_050
                                --  set fatu_050.transpor_forne9 = transp9_integrada,
                                 --      fatu_050.transpor_forne4 = transp4_integrada,
                                   --    fatu_050.transpor_forne2 = transp2_integrada
                                 --  where fatu_050.pedido_venda  = :new.pedido_venda
                                   --  and fatu_050.situacao_nfisc   = 0;

                              --  end;/*Marlan temp*/
                            end if;
                          else
                             sit_nota_integrada := 1;
                          end if;

                          begin
                             update inte_070
                             set inte_070.situacao = sit_nota_integrada,
                                 inte_070.data_retorno = sysdate
                             where inte_070.numero_nf = :new.num_nota_fiscal
                               and inte_070.serie_nf  = :new.serie_nota_fisc;
                          end;

                          commit;
                       end if;
                  end loop;
               end;
            end if;
       end if;
    end if;
end;
-- ALTER TRIGGER "INTER_TR_FATU_030_GFRETE" ENABLE


/

exec inter_pr_recompile;

