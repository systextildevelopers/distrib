alter table basi_833 drop constraint pk_basi_833;

drop index pk_basi_833;

alter table basi_833 add (largura number(8,2) default 0 not null);

alter table basi_833 add constraint pk_basi_833 primary key (grupo_produto, variacao_produto, qtde_ini, qtde_fim, largura);
   
exec inter_pr_recompile;

/
