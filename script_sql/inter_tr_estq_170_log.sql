CREATE OR REPLACE TRIGGER "INTER_TR_ESTQ_170_LOG" 
   after delete
   on estq_170
   for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_programa               varchar2(40);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

begin

   if deleting
   then

      -- Dados do usuario logado
      inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                              ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);
      
      ws_programa := inter_fn_nome_programa(ws_sid);
     

      INSERT INTO hist_100
         ( tabela,            operacao,
           data_ocorr,        aplicacao,
           usuario_rede,      maquina_rede,
           num01,             long01,
           str01
         )
      VALUES
         ( 'ESTQ_170',        'D',
           sysdate,           ws_aplicativo,
           ws_usuario_rede,   ws_maquina_rede,
           :new.nr_pedido,    'PEDIDO: ' || to_char(:new.nr_pedido,'000000000'),
           ws_programa
         );

   end if;

end inter_tr_estq_170_log;
-- ALTER TRIGGER "INTER_TR_ESTQ_170_LOG" ENABLE
 

/

exec inter_pr_recompile;

