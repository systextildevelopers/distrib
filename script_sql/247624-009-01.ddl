drop table estq_427;
drop table estq_420_log;
drop table estq_425_log;
drop table estq_426_log;

alter table estq_426
    add preview number(1);

alter table estq_426
    add constraint chk_preview check (preview in (0, 1));

alter table estq_426
    modify preview number(1) default 0;
