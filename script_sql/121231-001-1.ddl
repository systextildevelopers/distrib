CREATE OR REPLACE VIEW VIEW_CONSERTO AS
SELECT efic_100.data_rejeicao,
       efic_100.codigo_estagio as estagio,
       efic_100.codigo_motivo,
       efic_040.descricao,
       basi_180.descricao AS terceiros,
       efic_100.prod_rej_grupo AS referencia,
       basi_030.colecao,
       efic_100.quantidade,
       efic_100.periodo_producao
  FROM efic_100, basi_030, efic_040, basi_180
 WHERE basi_030.nivel_estrutura = 1
   AND efic_100.prod_rej_grupo = basi_030.referencia
   AND efic_100.codigo_estagio = efic_040.codigo_estagio
   AND efic_100.codigo_motivo = efic_040.codigo_motivo
   AND efic_100.codigo_familia = basi_180.divisao_producao
 ORDER BY efic_100.data_rejeicao desc;
