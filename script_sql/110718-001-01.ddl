alter table pedi_090 
add desconto_aplicado number(5,2);

alter table pedi_090 
modify desconto_aplicado default 0;

exec inter_pr_recompile;
