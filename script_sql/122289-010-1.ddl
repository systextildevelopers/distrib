alter table cont_022
add centro_custo number default 0;

comment on column cont_022.centro_custo is 'centro de custo de despesa da conta contabil';

alter table cont_022
  drop constraint PK_CONT_022;

prompt ATENCAO! Ao excluir este index se aparecer a mensagem "ORA-01418: specified index does not exist" favor ignorar.
drop index PK_CONT_022;


alter table CONT_022
  add constraint PK_CONT_022 primary key (COD_PLANO_CTA, CONTA_DRE, CONTA_ANALIT_DRE, CONTA_ANALIT_CTB, CENTRO_CUSTO) novalidate;
  
exec inter_pr_recompile;
