alter table BLOCOK_CFOP_TRANSACOES drop constraint FK_BLOCOK_CFOP_TRANSACOES_BLOCOK_CFOP;

alter table blocok_cfop_transacoes
    add constraint fk_blok_cfp_trnscoes_blok_cfp foreign key (id) references blocok_cfop(id);
