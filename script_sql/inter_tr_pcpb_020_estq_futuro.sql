
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPB_020_ESTQ_FUTURO" 
   before insert or
          delete or
          update of cod_cancelamento, qtde_quilos_prog
   on pcpb_020
   for each row

declare
   v_qtde number;
   v_data_ger date;
   procedure atualiza_saldo_futuro(valor number,nivel varchar2,grupo  varchar2,subgrupo varchar2,item varchar2, data_geracao date)
   is
      v_saldo   number;
      v_qtde    number;
      v_inativo number;
   begin
      v_saldo := valor;

      for reg_estq_560 in (select sequencia_prog sequencia, qtde_prog_futura qtde_futura, cod_chave chave
                           from estq_560
                           where nivel_produto            = nivel
                           and   grupo_produto            = grupo
                           and   subgru_produto           = subgrupo
                           and   item_produto             = item
                           and   qtde_prog_futura          > 0
                           and   inativo                   = 0
                           and   trunc(data_hora_ins,'DD') <= trunc(data_geracao,'DD')
                           order by sequencia_prog)
      loop
         if v_saldo > reg_estq_560.qtde_futura
         then
            v_qtde    := reg_estq_560.qtde_futura;
            v_inativo := 1;
         else
            v_qtde    := v_saldo;
            v_inativo := 0;
         end if;

         begin
            update estq_560
            set qtde_prog_futura = qtde_prog_futura + (v_qtde * (-1)),
                inativo          = v_inativo
            where cod_chave      = reg_estq_560.chave;
         end;

         v_saldo := v_saldo - v_qtde;

         if v_saldo <= 0
         then exit;
         end if;
      end loop;
   end;


begin
   if inserting then

      begin
         select pcpb_010.data_programa
         into   v_data_ger
         from pcpb_010
         where pcpb_010.ordem_producao = :new.ordem_producao;
         exception
         when others then
            v_data_ger := sysdate;
         end;

      atualiza_saldo_futuro(:new.qtde_quilos_prog,:new.pano_sbg_nivel99,:new.pano_sbg_grupo,:new.pano_sbg_subgrupo,:new.pano_sbg_item, v_data_ger);
   end if;

   if updating then
      if :old.cod_cancelamento <> :new.cod_cancelamento
      then v_qtde := :old.qtde_quilos_prog * (-1);
      else v_qtde := :new.qtde_quilos_prog - :old.qtde_quilos_prog;
      end if;

      begin
         select pcpb_010.data_programa
         into   v_data_ger
         from pcpb_010
         where pcpb_010.ordem_producao = :old.ordem_producao;
         exception
         when others then
            v_data_ger := sysdate;
     end;
      atualiza_saldo_futuro(v_qtde,:new.pano_sbg_nivel99,:new.pano_sbg_grupo,:new.pano_sbg_subgrupo,:new.pano_sbg_item, v_data_ger);
   end if;

   if deleting then
      begin
         select pcpb_010.data_programa
         into   v_data_ger
         from pcpb_010
         where pcpb_010.ordem_producao = :old.ordem_producao;
         exception
         when others then
            v_data_ger := sysdate;
     end;
      if :old.cod_cancelamento = 0
      then atualiza_saldo_futuro(:old.qtde_quilos_prog * (-1),:old.pano_sbg_nivel99,:old.pano_sbg_grupo,:old.pano_sbg_subgrupo,:old.pano_sbg_item, v_data_ger);
      end if;
   end if;

end inter_tr_pcpb_020_estq_futuro;
-- ALTER TRIGGER "INTER_TR_PCPB_020_ESTQ_FUTURO" ENABLE
 

/

exec inter_pr_recompile;

