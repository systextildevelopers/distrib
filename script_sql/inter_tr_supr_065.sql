CREATE OR REPLACE TRIGGER "INTER_TR_SUPR_065" 
   before insert or
          update of
      num_requisicao, data_requisicao, centro_custo, nome_requisit,
      observacao_req, cod_cancelamento, codigo_deposito, codigo_empresa,
      ccusto_aplicacao, codigo, solicitacao, executa_trigger,
      codigo_comprador, usuario_digitacao
on supr_065
for each row
declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   v_executa_trigger         number;
BEGIN
   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then
      if :new.usuario_digitacao       is null or
         trim(:new.usuario_digitacao) is null or
         :new.usuario_digitacao = ' '
      then
         -- Busca dados do usuario
         inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                                 ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);
         :new.usuario_digitacao := ws_usuario_systextil;
      end if;
   end if;

EXCEPTION
  WHEN OTHERS THEN
    raise_application_error (-20000, 'Erro na execucao da trigger inter_tr_supr_065'||Chr(10)||SQLERRM);

END inter_tr_supr_065;

-- ALTER TRIGGER "INTER_TR_SUPR_065" ENABLE

 
/

exec inter_pr_recompile;

