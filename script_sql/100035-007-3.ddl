insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('pedi_f103', 'Resitrições de Natureza de Operações', 0, 1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'pedi_f103', 'pedi_m030' , 1, 0, 'S', 'S', 'S', 'S');

update hdoc_036
set hdoc_036.descricao       = 'Resitrições de Natureza de Operações'
where hdoc_036.codigo_programa = 'pedi_f103'
  and hdoc_036.locale          = 'es_ES';
  
commit work;
