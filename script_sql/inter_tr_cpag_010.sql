
  CREATE OR REPLACE TRIGGER "INTER_TR_CPAG_010" 
   before insert or
          update of valor_parcela,    valor_irrf,
                    valor_iss,        valor_inss,
                    cod_cancelamento, situacao,
                    data_vencimento
   on cpag_010
   for each row

declare
   v_contr_irrf         cpag_040.controle_irrf%type;
   v_sinal_irrf         number;
   v_conta_reg          number;
   v_executa_trigger    number;
   v_saldo_titulo       cpag_010.saldo_titulo%type;
   ws_usuario_rede      varchar2(20);
   ws_maquina_rede      varchar2(40);
   ws_aplicativo        varchar2(20);
   ws_sid               number(9);
   ws_empresa           number(3);
   ws_usuario_systextil varchar2(250);
   ws_locale_usuario    varchar2(5);

begin
   -- INICIO - Logica implementada para controle de "gatilho" das triggers do banco
   -- para atender a implementacao executada para limpeza da base de dados do cliente
   -- e replicacao dos dados para base historico. (SS.38405)
   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then
      if inserting
      then
         :new.data_ult_movim_pagto := :new.data_contrato;

         -- Busca dados do usuario
         if :new.usuario_digitacao       is null or
            trim(:new.usuario_digitacao) is null
         then
            inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);
            :new.usuario_digitacao := ws_usuario_systextil;
         end if;
         -- Calculo do saldo inicial considerando o valor liquido a pagar
         -- abatendo os impostos sobre o titulo.
         select cpag_040.controle_irrf
         into v_contr_irrf
         from cpag_040
         where cpag_040.tipo_titulo = :new.tipo_titulo;

         if v_contr_irrf = 0
         then
            v_sinal_irrf :=  1.00;
         else
            v_sinal_irrf := -1.00;
         end if;

         :new.saldo_titulo         := :new.valor_parcela - ((:new.valor_irrf * v_sinal_irrf) + :new.valor_iss + :new.valor_inss);
         if :new.situacao <> 1
         then 
           :new.situacao             := 0;
         end if;
         :new.cod_cancelamento     := 0;
         :new.data_canc_tit        := null;

         select count(*) into v_conta_reg from rcnb_060
         where rcnb_060.tipo_registro = 725
           and rcnb_060.data_1        = :new.data_vencimento;

         if v_conta_reg > 0
         then
            raise_application_error(-20000,chr(10)||chr(10)||'O Setor de Contas a Pagar bloqueou a inclusao de vencimentos na data '||to_char(:new.data_vencimento,'DD/MM/YY')||'.'||chr(10)||chr(10));
         end if;
      end if;

      if updating
      then
         -- se a nova data_ult_movim_pagto for nula, esta sera a data_digitacao da titulo
         if :new.data_ult_movim_pagto is null
         then
            :new.data_ult_movim_pagto := :new.data_contrato;
         end if;

         v_saldo_titulo := :old.saldo_titulo;


         -- ALTERA VALOR PARCELA
         -- se o valor_parcela for alterado, recalcula o saldo da titulo
         if :old.valor_parcela <> :new.valor_parcela
         then
            v_saldo_titulo := v_saldo_titulo - (:old.valor_parcela - :new.valor_parcela);
         end if;

         -- ALTERA O VALOR DOS IMPOSTOS
         -- se o valor_irrf for alterado, recalcula o saldo da titulo
         if :old.valor_irrf <> :new.valor_irrf
         then
            v_saldo_titulo := v_saldo_titulo - (:new.valor_irrf - :old.valor_irrf);
         end if;

         -- se o valor_iss for alterado, recalcula o saldo da titulo
         if :old.valor_iss <> :new.valor_iss
         then
            v_saldo_titulo := v_saldo_titulo - (:new.valor_iss - :old.valor_iss);
         end if;

         -- se o valor_inss for alterado, recalcula o saldo da titulo
         if :old.valor_inss <> :new.valor_inss
         then
            v_saldo_titulo := v_saldo_titulo - (:new.valor_inss - :old.valor_inss);
         end if;

         :new.saldo_titulo := v_saldo_titulo;

         -- ALTERA A SITUACAO
         -- se o titulo for cancelado, a situacao passa para 2 (cancelada)
         if :new.cod_cancelamento > 0
         then
            :new.situacao := 2;
         end if;

         if :new.data_vencimento <> :old.data_vencimento
         then
            select count(*) into v_conta_reg from rcnb_060
            where rcnb_060.tipo_registro = 725
              and rcnb_060.data_1        = :new.data_vencimento;

            if v_conta_reg > 0
            then
               raise_application_error(-20000,chr(10)||chr(10)||'O Setor de Contas a Pagar bloqueou a inclusao de vencimentos na data '||to_char(:new.data_vencimento,'DD/MM/YY')||'.'||chr(10)||chr(10));
            end if;
         end if;
      end if;
   end if;
end inter_tr_cpag_010;

-- ALTER TRIGGER "INTER_TR_CPAG_010" ENABLE
 

/

exec inter_pr_recompile;

