create or replace trigger inter_tr_pcpb_080_3
     before insert
         or delete
     on pcpb_080
     for each row
declare
   v_periodo_producao   number;

begin
   if  (inserting or updating)
   and :new.ordem_producao > 0
   then
      begin
         select pcpb_010.periodo_producao
         into   v_periodo_producao
         from pcpb_010
         where pcpb_010.ORDEM_TINGIMENTO = :new.ordem_producao;
      exception when others then
         v_periodo_producao := 0;
      end;

      if (v_periodo_producao > 0)
      then
         inter_pr_verif_per_fechado(v_periodo_producao,2);
      end if;
   end if;

   if  deleting
   and :old.ordem_producao > 0
   then
      begin
         select pcpb_010.periodo_producao
         into   v_periodo_producao
         from pcpb_010
         where pcpb_010.ORDEM_TINGIMENTO = :old.ordem_producao;
      exception when others then
         v_periodo_producao := 0;
      end;

      inter_pr_verif_per_fechado(v_periodo_producao,2);

   end if;

end inter_tr_pcpb_080_3;

/

exec inter_pr_recompile;

/* versao: 2 */

