CREATE OR REPLACE trigger inter_tr_pedi_841_log 
after insert or delete or update 
on pedi_841  
for each row 
declare 
   ws_usuario_rede           varchar2(20) ; 
   ws_maquina_rede           varchar2(40) ; 
   ws_aplicativo             varchar2(20) ; 
   ws_sid                    number(9) ; 
   ws_empresa                number(3) ; 
   ws_usuario_systextil      varchar2(20) ; 
   ws_locale_usuario         varchar2(5) ; 
   v_nome_programa           varchar2(20) ; 
begin
-- Dados do usuário logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid, 
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);
   v_nome_programa := inter_fn_nome_programa(ws_sid);
 
 
 
 if inserting 
 then 
    begin 
 
        insert into pedi_841_log (
           TIPO_OCORR,   /*0*/ 
           DATA_OCORR,   /*1*/ 
           HORA_OCORR,   /*2*/ 
           USUARIO_REDE,   /*3*/ 
           MAQUINA_REDE,   /*4*/ 
           APLICACAO,   /*5*/ 
           USUARIO_SISTEMA,   /*6*/ 
           NOME_PROGRAMA,   /*7*/ 
           id_NEW,   /*9*/ 
           tipo_cadastro_NEW,   /*11*/ 
           grupo_economico_NEW,   /*13*/ 
           cgc9_NEW,   /*15*/ 
           cgc4_NEW,   /*17*/ 
           cgc2_NEW,   /*19*/ 
           valor_excedente_NEW,   /*21*/ 
           data_validade_NEW,   /*23*/ 
           codigo_bloqueio_NEW,   /*25*/ 
           data_digitacao_NEW,   /*27*/ 
           usuario_logado_NEW,   /*29*/ 
           observacao_bloqueio_NEW    /*31*/
        ) values (    
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/ 
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           :new.id, /*9*/   
           :new.tipo_cadastro, /*11*/   
           :new.grupo_economico, /*13*/   
           :new.cgc9, /*15*/   
           :new.cgc4, /*17*/   
           :new.cgc2, /*19*/   
           :new.valor_excedente, /*21*/   
           :new.data_validade, /*23*/   
           :new.codigo_bloqueio, /*25*/   
           :new.data_digitacao, /*27*/   
           :new.usuario_logado, /*29*/   
           :new.observacao_bloqueio /*31*/   
         );    
    end;    
 end if;    
  
  
 if updating 
 then 
    begin 
        insert into pedi_841_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           id_OLD, /*8*/   
           id_NEW, /*9*/   
           tipo_cadastro_OLD, /*10*/   
           tipo_cadastro_NEW, /*11*/   
           grupo_economico_OLD, /*12*/   
           grupo_economico_NEW, /*13*/   
           cgc9_OLD, /*14*/   
           cgc9_NEW, /*15*/   
           cgc4_OLD, /*16*/   
           cgc4_NEW, /*17*/   
           cgc2_OLD, /*18*/   
           cgc2_NEW, /*19*/   
           valor_excedente_OLD, /*20*/   
           valor_excedente_NEW, /*21*/   
           data_validade_OLD, /*22*/   
           data_validade_NEW, /*23*/   
           codigo_bloqueio_OLD, /*24*/   
           codigo_bloqueio_NEW, /*25*/   
           data_digitacao_OLD, /*26*/   
           data_digitacao_NEW, /*27*/   
           usuario_logado_OLD, /*28*/   
           usuario_logado_NEW, /*29*/   
           observacao_bloqueio_OLD, /*30*/   
           observacao_bloqueio_NEW  /*31*/  
        ) values (    
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/ 
           :old.id,  /*8*/  
           :new.id, /*9*/   
           :old.tipo_cadastro,  /*10*/  
           :new.tipo_cadastro, /*11*/   
           :old.grupo_economico,  /*12*/  
           :new.grupo_economico, /*13*/   
           :old.cgc9,  /*14*/  
           :new.cgc9, /*15*/   
           :old.cgc4,  /*16*/  
           :new.cgc4, /*17*/   
           :old.cgc2,  /*18*/  
           :new.cgc2, /*19*/   
           :old.valor_excedente,  /*20*/  
           :new.valor_excedente, /*21*/   
           :old.data_validade,  /*22*/  
           :new.data_validade, /*23*/   
           :old.codigo_bloqueio,  /*24*/  
           :new.codigo_bloqueio, /*25*/   
           :old.data_digitacao,  /*26*/  
           :new.data_digitacao, /*27*/   
           :old.usuario_logado,  /*28*/  
           :new.usuario_logado, /*29*/   
           :old.observacao_bloqueio,  /*30*/  
           :new.observacao_bloqueio  /*31*/  
         );    
    end;    
 end if;    
  
  
 if deleting 
 then 
    begin 
        insert into pedi_841_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           id_OLD, /*8*/   
           tipo_cadastro_OLD, /*10*/   
           grupo_economico_OLD, /*12*/   
           cgc9_OLD, /*14*/   
           cgc4_OLD, /*16*/   
           cgc2_OLD, /*18*/   
           valor_excedente_OLD, /*20*/   
           data_validade_OLD, /*22*/   
           codigo_bloqueio_OLD, /*24*/   
           data_digitacao_OLD, /*26*/   
           usuario_logado_OLD, /*28*/   
           observacao_bloqueio_OLD /*30*/  
        ) values (    
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede,/*4*/ 
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           :old.id, /*8*/   
           :old.tipo_cadastro, /*10*/   
           :old.grupo_economico, /*12*/   
           :old.cgc9, /*14*/   
           :old.cgc4, /*16*/   
           :old.cgc2, /*18*/   
           :old.valor_excedente, /*20*/   
           :old.data_validade, /*22*/   
           :old.codigo_bloqueio, /*24*/   
           :old.data_digitacao, /*26*/   
           :old.usuario_logado, /*28*/   
           :old.observacao_bloqueio /*30*/   
         );    
    end;    
 end if;    
end inter_tr_pedi_841_log;
-- ALTER TRIGGER "inter_tr_pedi_841_log" ENABLE
/

exec inter_pr_recompile;

