alter table obrf_971
add (num_nf_entr number(9),
    serie_nf_entr varchar2(3),
    cnpj_9_nf_entr number(9),
    cnpj_4_nf_entr number(9),
    cnpj_2_nf_entr number(9));
    
alter table obrf_971
modify (num_nf_entr default 0,
        serie_nf_entr default '',
        cnpj_9_nf_entr default 0,
        cnpj_4_nf_entr default 0,
        cnpj_2_nf_entr default 0);

/
exec inter_pr_recompile;

