
  CREATE OR REPLACE PROCEDURE "LOJA_PR_EXP_TAB_PRECO_INTE" (p_nivel       in varchar2,
                                                          p_grupo       in varchar2,
                                                          p_subgrupo    varchar2,
                                                          p_item        varchar2,
                                                          p_colecao     varchar2,
                                                          p_cod_empr    number,
                                                          p_des_erro OUT varchar2) is

   cursor u_basi_010 (p_nivel    varchar2,
                      p_grupo    varchar2,
                      p_subgrupo varchar2,
                      p_item     varchar2,
                      p_colecao  varchar2) is
      select basi_010.nivel_estrutura, basi_010.grupo_estrutura,
             basi_010.subgru_estrutura, basi_010.item_estrutura
     from basi_010, basi_030
     where (basi_010.nivel_estrutura  = p_nivel or p_nivel is null)
       and (basi_010.grupo_estrutura  = p_grupo or p_grupo is null)
       and (basi_010.subgru_estrutura = p_subgrupo or p_subgrupo is null)
       and (basi_010.item_estrutura   = p_item or p_item is null)
       and (basi_030.nivel_estrutura  = basi_010.nivel_estrutura)
       and (basi_030.referencia       = basi_010.grupo_estrutura)
       and (basi_030.colecao          = p_colecao or p_colecao is null or p_colecao = 0)
       and exists(select 1
                    from pedi_095 t, pedi_090 c, fatu_501
                   where fatu_501.codigo_empresa = p_cod_empr
                     and t.tab_col_tab       = c.col_tabela_preco
                     and t.tab_mes_tab       = c.mes_tabela_preco
                     and t.tab_seq_tab       = c.seq_tabela_preco
                     and t.tab_col_tab       = fatu_501.col_tab_loja
                     and t.tab_mes_tab       = fatu_501.mes_tab_loja
                     and t.tab_seq_tab       = fatu_501.seq_tab_loja
                     and sysdate             between c.data_ini_tabela and c.data_fim_tabela
                     and t.nivel_estrutura   = basi_010.nivel_estrutura
                     and t.grupo_estrutura   = basi_010.grupo_estrutura
                     and t.val_tabela_preco  > 0.00);

BEGIN
p_des_erro := 'Erro na inclus�o da tabela ti_pre - PRE�O ' || Chr(10) || SQLERRM;

END;

 

/

exec inter_pr_recompile;

