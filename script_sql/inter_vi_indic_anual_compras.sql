create or replace view inter_vi_indic_anual_compras as
select YDT1, MDT1, sum(VALOR) VALOR, sum(VALOR_IMPORTADO) VALOR_IMPORTADO, sum(VALOR_NACIONAL) VALOR_NACIONAL, sum(VALOR_NACIONAL_SC) VALOR_NACIONAL_SC, sum(VALOR_NACIONAL_FORA_SC) VALOR_NACIONAL_FORA_SC, sum(VALOR_IMPORTADO_SC) VALOR_IMPORTADO_SC, sum(VALOR_IMPORTADO_FORA_SC) VALOR_IMPORTADO_FORA_SC from
((select  extract (year from obrf_010.data_emissao) as YDT1,  extract (month from obrf_010.data_emissao) as MDT1,
       sum(round(obrf_015.quantidade*obrf_015.valor_unitario,3)) VALOR, 0 VALOR_IMPORTADO, 0 VALOR_NACIONAL, 0 VALOR_NACIONAL_SC, 0 VALOR_NACIONAL_FORA_SC, 0 VALOR_IMPORTADO_SC, 0 VALOR_IMPORTADO_FORA_SC
    from obrf_010, obrf_015, supr_010, basi_010, basi_160, basi_030, basi_150, pedi_080, basi_240
    where obrf_010.documento         = obrf_015.capa_ent_nrdoc
      and obrf_010.serie             = obrf_015.capa_ent_serie
      and obrf_010.cgc_cli_for_9     = obrf_015.capa_ent_forcli9
      and obrf_010.cgc_cli_for_4     = obrf_015.capa_ent_forcli4
      and obrf_010.cgc_cli_for_2     = obrf_015.capa_ent_forcli2
      and obrf_015.natitem_nat_oper  = pedi_080.natur_operacao
      and obrf_015.natitem_est_oper  = pedi_080.estado_natoper
      and pedi_080.nat_ativa         = 0
      and pedi_080.tipo_natureza     not in (2)
      and obrf_015.coditem_nivel99   = basi_010.nivel_estrutura
      and obrf_015.coditem_grupo     = basi_010.grupo_estrutura
      and obrf_015.coditem_subgrupo  = basi_010.subgru_estrutura
      and obrf_015.coditem_item      = basi_010.item_estrutura
      and obrf_015.coditem_nivel99   = basi_030.nivel_estrutura
      and obrf_015.coditem_grupo     = basi_030.referencia
      and basi_030.conta_estoque     = basi_150.conta_estoque
      and basi_150.tipo_produto_sped = 1
      and obrf_010.cgc_cli_for_9     = supr_010.fornecedor9
      and obrf_010.cgc_cli_for_4     = supr_010.fornecedor4
      and obrf_010.cgc_cli_for_2     = supr_010.fornecedor2
      and supr_010.tipo_fornecedor   = 11
      and supr_010.cod_cidade        = basi_160.cod_cidade
      and obrf_015.classific_fiscal  = basi_240.CLASSIFIC_FISCAL
      and extract (year from obrf_010.data_emissao) = extract (year from SYSDATE)
      and obrf_010.cod_canc_nfisc    = 0
      group by extract (month from obrf_010.data_emissao), extract (year from obrf_010.data_emissao))
      union all
    (select extract (year from obrf_010.data_emissao) as YDT1,  extract (month from obrf_010.data_emissao) as MDT1,
           0 VALOR, sum(round(obrf_015.quantidade*obrf_015.valor_unitario,3)) VALOR_IMPORTADO, 0 VALOR_NACIONAL, 0 VALOR_NACIONAL_SC, 0 VALOR_NACIONAL_FORA_SC, 0 VALOR_IMPORTADO_SC, 0 VALOR_IMPORTADO_FORA_SC
      from obrf_010, obrf_015, supr_010, basi_010, basi_160, basi_030, basi_150, pedi_080, basi_240
      where obrf_010.documento         = obrf_015.capa_ent_nrdoc
        and obrf_010.serie             = obrf_015.capa_ent_serie
        and obrf_010.cgc_cli_for_9     = obrf_015.capa_ent_forcli9
        and obrf_010.cgc_cli_for_4     = obrf_015.capa_ent_forcli4
        and obrf_010.cgc_cli_for_2     = obrf_015.capa_ent_forcli2
        and obrf_015.natitem_nat_oper  = pedi_080.natur_operacao
        and obrf_015.natitem_est_oper  = pedi_080.estado_natoper
        and pedi_080.nat_ativa         = 0
        and pedi_080.tipo_natureza     not in (2)
        and obrf_015.coditem_nivel99   = basi_010.nivel_estrutura
        and obrf_015.coditem_grupo     = basi_010.grupo_estrutura
        and obrf_015.coditem_subgrupo  = basi_010.subgru_estrutura
        and obrf_015.coditem_item      = basi_010.item_estrutura
        and obrf_015.coditem_nivel99   = basi_030.nivel_estrutura
        and obrf_015.coditem_grupo     = basi_030.referencia
        and basi_030.conta_estoque     = basi_150.conta_estoque
        and basi_150.tipo_produto_sped = 1
        and obrf_010.cgc_cli_for_9     = supr_010.fornecedor9
        and obrf_010.cgc_cli_for_4     = supr_010.fornecedor4
        and obrf_010.cgc_cli_for_2     = supr_010.fornecedor2
        and supr_010.tipo_fornecedor   = 11
        and supr_010.cod_cidade        = basi_160.cod_cidade
        and obrf_015.classific_fiscal  = basi_240.CLASSIFIC_FISCAL
        and obrf_015.procedencia       = 2
        and extract (year from obrf_010.data_emissao) = extract (year from SYSDATE)
        and obrf_010.cod_canc_nfisc    = 0
      group by extract (month from obrf_010.data_emissao), extract (year from obrf_010.data_emissao))
      union all
      (select extract (year from obrf_010.data_emissao) as YDT1,  extract (month from obrf_010.data_emissao) as MDT1,
           0 VALOR, 0 VALOR_IMPORTADO ,sum(round(obrf_015.quantidade*obrf_015.valor_unitario,3)) VALOR_NACIONAL, 0 VALOR_NACIONAL_SC, 0 VALOR_NACIONAL_FORA_SC, 0 VALOR_IMPORTADO_SC, 0 VALOR_IMPORTADO_FORA_SC
      from obrf_010, obrf_015, supr_010, basi_010, basi_160, basi_030, basi_150, pedi_080, basi_240
      where obrf_010.documento         = obrf_015.capa_ent_nrdoc
        and obrf_010.serie             = obrf_015.capa_ent_serie
        and obrf_010.cgc_cli_for_9     = obrf_015.capa_ent_forcli9
        and obrf_010.cgc_cli_for_4     = obrf_015.capa_ent_forcli4
        and obrf_010.cgc_cli_for_2     = obrf_015.capa_ent_forcli2
        and obrf_015.natitem_nat_oper  = pedi_080.natur_operacao
        and obrf_015.natitem_est_oper  = pedi_080.estado_natoper
        and pedi_080.nat_ativa         = 0
        and pedi_080.tipo_natureza     not in (2)
        and obrf_015.coditem_nivel99   = basi_010.nivel_estrutura
        and obrf_015.coditem_grupo     = basi_010.grupo_estrutura
        and obrf_015.coditem_subgrupo  = basi_010.subgru_estrutura
        and obrf_015.coditem_item      = basi_010.item_estrutura
        and obrf_015.coditem_nivel99   = basi_030.nivel_estrutura
        and obrf_015.coditem_grupo     = basi_030.referencia
        and basi_030.conta_estoque     = basi_150.conta_estoque
        and basi_150.tipo_produto_sped = 1
        and obrf_010.cgc_cli_for_9     = supr_010.fornecedor9
        and obrf_010.cgc_cli_for_4     = supr_010.fornecedor4
        and obrf_010.cgc_cli_for_2     = supr_010.fornecedor2
        and supr_010.tipo_fornecedor   = 11
        and supr_010.cod_cidade        = basi_160.cod_cidade
        and obrf_015.classific_fiscal  = basi_240.CLASSIFIC_FISCAL
        and obrf_015.procedencia       <> 2
        and extract (year from obrf_010.data_emissao) = extract (year from SYSDATE)
        and obrf_010.cod_canc_nfisc    = 0
      group by extract (month from obrf_010.data_emissao), extract (year from obrf_010.data_emissao))
      UNION ALL
      (select extract (year from obrf_010.data_emissao) as YDT1,  extract (month from obrf_010.data_emissao) as MDT1,
           0 VALOR, 0 VALOR_IMPORTADO , 0 VALOR_NACIONAL,sum(round(obrf_015.quantidade*obrf_015.valor_unitario,3)) VALOR_NACIONAL_SC, 0 VALOR_NACIONAL_FORA_SC, 0 VALOR_IMPORTADO_SC, 0 VALOR_IMPORTADO_FORA_SC
      from obrf_010, obrf_015, supr_010, basi_010, basi_160, basi_030, basi_150, pedi_080, basi_240
      where obrf_010.documento         = obrf_015.capa_ent_nrdoc
        and obrf_010.serie             = obrf_015.capa_ent_serie
        and obrf_010.cgc_cli_for_9     = obrf_015.capa_ent_forcli9
        and obrf_010.cgc_cli_for_4     = obrf_015.capa_ent_forcli4
        and obrf_010.cgc_cli_for_2     = obrf_015.capa_ent_forcli2
        and obrf_015.natitem_nat_oper  = pedi_080.natur_operacao
        and obrf_015.natitem_est_oper  = pedi_080.estado_natoper
        and pedi_080.nat_ativa         = 0
        and pedi_080.tipo_natureza     not in (2)
        and obrf_015.coditem_nivel99   = basi_010.nivel_estrutura
        and obrf_015.coditem_grupo     = basi_010.grupo_estrutura
        and obrf_015.coditem_subgrupo  = basi_010.subgru_estrutura
        and obrf_015.coditem_item      = basi_010.item_estrutura
        and obrf_015.coditem_nivel99   = basi_030.nivel_estrutura
        and obrf_015.coditem_grupo     = basi_030.referencia
        and basi_030.conta_estoque     = basi_150.conta_estoque
        and basi_150.tipo_produto_sped = 1
        and obrf_010.cgc_cli_for_9     = supr_010.fornecedor9
        and obrf_010.cgc_cli_for_4     = supr_010.fornecedor4
        and obrf_010.cgc_cli_for_2     = supr_010.fornecedor2
        and supr_010.tipo_fornecedor   = 11
        and supr_010.cod_cidade        = basi_160.cod_cidade
        and obrf_015.classific_fiscal  = basi_240.CLASSIFIC_FISCAL
        and obrf_015.procedencia       <> 2
        and (basi_160.estado = 'SC' or supr_010.considera_ttd = 1 or basi_240.considera_ttd = 1)
        and extract (year from obrf_010.data_emissao) = extract (year from SYSDATE)
        and obrf_010.cod_canc_nfisc    = 0
      group by extract (month from obrf_010.data_emissao), extract (year from obrf_010.data_emissao))
      UNION ALL
      (select extract (year from obrf_010.data_emissao) as YDT1,  extract (month from obrf_010.data_emissao) as MDT1,
           0 VALOR, 0 VALOR_IMPORTADO , 0 VALOR_NACIONAL, 0 VALOR_NACIONAL_SC, sum(round(obrf_015.quantidade*obrf_015.valor_unitario,3)) VALOR_NACIONAL_FORA_SC, 0 VALOR_IMPORTADO_SC, 0 VALOR_IMPORTADO_FORA_SC
      from obrf_010, obrf_015, supr_010, basi_010, basi_160, basi_030, basi_150, pedi_080, basi_240
      where obrf_010.documento         = obrf_015.capa_ent_nrdoc
        and obrf_010.serie             = obrf_015.capa_ent_serie
        and obrf_010.cgc_cli_for_9     = obrf_015.capa_ent_forcli9
        and obrf_010.cgc_cli_for_4     = obrf_015.capa_ent_forcli4
        and obrf_010.cgc_cli_for_2     = obrf_015.capa_ent_forcli2
        and obrf_015.natitem_nat_oper  = pedi_080.natur_operacao
        and obrf_015.natitem_est_oper  = pedi_080.estado_natoper
        and pedi_080.nat_ativa         = 0
        and pedi_080.tipo_natureza     not in (2)
        and obrf_015.coditem_nivel99   = basi_010.nivel_estrutura
        and obrf_015.coditem_grupo     = basi_010.grupo_estrutura
        and obrf_015.coditem_subgrupo  = basi_010.subgru_estrutura
        and obrf_015.coditem_item      = basi_010.item_estrutura
        and obrf_015.coditem_nivel99   = basi_030.nivel_estrutura
        and obrf_015.coditem_grupo     = basi_030.referencia
        and basi_030.conta_estoque     = basi_150.conta_estoque
        and basi_150.tipo_produto_sped = 1
        and obrf_010.cgc_cli_for_9     = supr_010.fornecedor9
        and obrf_010.cgc_cli_for_4     = supr_010.fornecedor4
        and obrf_010.cgc_cli_for_2     = supr_010.fornecedor2
        and supr_010.tipo_fornecedor   = 11
        and supr_010.cod_cidade        = basi_160.cod_cidade
        and obrf_015.classific_fiscal  = basi_240.CLASSIFIC_FISCAL
        and obrf_015.procedencia       <> 2
        and (basi_160.estado <> 'SC' and supr_010.considera_ttd = 0 and basi_240.considera_ttd = 0)
        and extract (year from obrf_010.data_emissao) = extract (year from SYSDATE)
        and obrf_010.cod_canc_nfisc    = 0
      group by extract (month from obrf_010.data_emissao), extract (year from obrf_010.data_emissao))
      UNION ALL
      (select extract (year from obrf_010.data_emissao) as YDT1,  extract (month from obrf_010.data_emissao) as MDT1,
           0 VALOR, 0 VALOR_IMPORTADO , 0 VALOR_NACIONAL, 0 VALOR_NACIONAL_SC, 0 VALOR_NACIONAL_FORA_SC, sum(round(obrf_015.quantidade*obrf_015.valor_unitario,3)) VALOR_IMPORTADO_SC, 0 VALOR_IMPORTADO_FORA_SC
      from obrf_010, obrf_015, supr_010, basi_010, basi_160, basi_030, basi_150, pedi_080, basi_240
      where obrf_010.documento         = obrf_015.capa_ent_nrdoc
        and obrf_010.serie             = obrf_015.capa_ent_serie
        and obrf_010.cgc_cli_for_9     = obrf_015.capa_ent_forcli9
        and obrf_010.cgc_cli_for_4     = obrf_015.capa_ent_forcli4
        and obrf_010.cgc_cli_for_2     = obrf_015.capa_ent_forcli2
        and obrf_015.natitem_nat_oper  = pedi_080.natur_operacao
        and obrf_015.natitem_est_oper  = pedi_080.estado_natoper
        and pedi_080.nat_ativa         = 0
        and pedi_080.tipo_natureza     not in (2)
        and obrf_015.coditem_nivel99   = basi_010.nivel_estrutura
        and obrf_015.coditem_grupo     = basi_010.grupo_estrutura
        and obrf_015.coditem_subgrupo  = basi_010.subgru_estrutura
        and obrf_015.coditem_item      = basi_010.item_estrutura
        and obrf_015.coditem_nivel99   = basi_030.nivel_estrutura
        and obrf_015.coditem_grupo     = basi_030.referencia
        and basi_030.conta_estoque     = basi_150.conta_estoque
        and basi_150.tipo_produto_sped = 1
        and obrf_010.cgc_cli_for_9     = supr_010.fornecedor9
        and obrf_010.cgc_cli_for_4     = supr_010.fornecedor4
        and obrf_010.cgc_cli_for_2     = supr_010.fornecedor2
        and supr_010.tipo_fornecedor   = 11
        and supr_010.cod_cidade        = basi_160.cod_cidade
        and obrf_015.classific_fiscal  = basi_240.CLASSIFIC_FISCAL
        and obrf_015.procedencia       = 2
        and (basi_160.estado = 'SC' or supr_010.considera_ttd = 1 or basi_240.considera_ttd = 1)
        and extract (year from obrf_010.data_emissao) = extract (year from SYSDATE)
        and obrf_010.cod_canc_nfisc    = 0
      group by extract (month from obrf_010.data_emissao), extract (year from obrf_010.data_emissao))
      UNION ALL
      (select extract (year from obrf_010.data_emissao) as YDT1,  extract (month from obrf_010.data_emissao) as MDT1,
           0 VALOR, 0 VALOR_IMPORTADO , 0 VALOR_NACIONAL, 0 VALOR_NACIONAL_SC, 0 VALOR_NACIONAL_FORA_SC, 0 VALOR_IMPORTADO_SC, sum(round(obrf_015.quantidade*obrf_015.valor_unitario,3)) VALOR_IMPORTADO_FORA_SC
          from obrf_010, obrf_015, supr_010, basi_010, basi_160, basi_030, basi_150, pedi_080, basi_240
          where obrf_010.documento         = obrf_015.capa_ent_nrdoc
            and obrf_010.serie             = obrf_015.capa_ent_serie
            and obrf_010.cgc_cli_for_9     = obrf_015.capa_ent_forcli9
            and obrf_010.cgc_cli_for_4     = obrf_015.capa_ent_forcli4
            and obrf_010.cgc_cli_for_2     = obrf_015.capa_ent_forcli2
            and obrf_015.natitem_nat_oper  = pedi_080.natur_operacao
            and obrf_015.natitem_est_oper  = pedi_080.estado_natoper
            and pedi_080.nat_ativa         = 0
            and pedi_080.tipo_natureza     not in (2)
            and obrf_015.coditem_nivel99   = basi_010.nivel_estrutura
            and obrf_015.coditem_grupo     = basi_010.grupo_estrutura
            and obrf_015.coditem_subgrupo  = basi_010.subgru_estrutura
            and obrf_015.coditem_item      = basi_010.item_estrutura
            and obrf_015.coditem_nivel99   = basi_030.nivel_estrutura
            and obrf_015.coditem_grupo     = basi_030.referencia
            and basi_030.conta_estoque     = basi_150.conta_estoque
            and basi_150.tipo_produto_sped = 1
            and obrf_010.cgc_cli_for_9     = supr_010.fornecedor9
            and obrf_010.cgc_cli_for_4     = supr_010.fornecedor4
            and obrf_010.cgc_cli_for_2     = supr_010.fornecedor2
            and supr_010.tipo_fornecedor   = 11
            and supr_010.cod_cidade        = basi_160.cod_cidade
            and obrf_015.classific_fiscal  = basi_240.CLASSIFIC_FISCAL
            and obrf_015.procedencia       = 2
            and (basi_160.estado <> 'SC' and supr_010.considera_ttd = 0 and basi_240.considera_ttd = 0)
            and extract (year from obrf_010.data_emissao) = extract (year from SYSDATE)
            and obrf_010.cod_canc_nfisc    = 0
          group by extract (month from obrf_010.data_emissao), extract (year from obrf_010.data_emissao))
          )
group by YDT1, MDT1
order by YDT1, MDT1;
