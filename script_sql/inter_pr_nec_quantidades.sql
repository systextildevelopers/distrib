
  CREATE OR REPLACE PROCEDURE "INTER_PR_NEC_QUANTIDADES" (
    p_empresa_logada  in number,
    p_inc_exc_dep     in number,
    p_codigo_usuario  in number,
    p_usuario         in varchar2,
    p_nr_solicitacao  in number,
    p_empresa_filtro  in number,
    p_cons_transito   in number,
    p_data_ini_compra in date,
    p_data_fim_compra in date) is

   TYPE nivel IS TABLE OF basi_030.nivel_estrutura%TYPE;
   p_nivel nivel;

   TYPE grupo IS TABLE OF basi_030.referencia%TYPE;
   p_grupo grupo;

   TYPE subgrupo IS TABLE OF basi_020.tamanho_ref%TYPE;
   p_subgrupo subgrupo;

   TYPE item IS TABLE OF basi_010.item_estrutura%TYPE;
   p_item item;

   TYPE qtdeAprogramar IS TABLE OF tmrp_615.qtde_necessaria%TYPE;
   p_qtde_aprogramar qtdeAprogramar;

   p_mensagem          varchar2(200);
   p_usuario_rede      varchar2(20);
   p_maquina_rede      varchar2(40);
   p_aplicativo        varchar2(20);
   p_sid               number(9);
   p_usuario_systextil varchar2(250);
   p_locale_usuario    varchar2(5);
   p_empresa_usu       number(3);

   p_qtde_estoque_atu estq_040.qtde_estoque_atu%TYPE;
   p_qtde_transito    pcpt_020.qtde_quilos_acab%TYPE;
   p_qtde_reservada   tmrp_041.qtde_reservada%TYPE;
   p_qtde_programado  tmrp_041.qtde_areceber%TYPE;
   p_qtde_disponivel  estq_040.qtde_estoque_atu%TYPE;
   p_qtde_sit_liquida estq_040.qtde_estoque_atu%TYPE;
   p_qtde_comprado    tmrp_041.qtde_areceber%TYPE;
   p_qtde_requisitada supr_067.qtde_requisitada%TYPE;
   p_qtde_tot_compras tmrp_041.qtde_areceber%TYPE;
   p_data_ultima_ent  supr_100.data_prev_entr%TYPE;
   p_data_ult_compra  obrf_010.data_transacao%TYPE;

BEGIN

   inter_pr_dados_usuario (p_usuario_rede,
                           p_maquina_rede,
                           p_aplicativo,
                           p_sid,
                           p_usuario_systextil,
                           p_empresa_usu,
                           p_locale_usuario);

   begin
      /*Le os materiais dos projetos cujas referencias nao tenham pedidos.*/
      select /*parallel(tmrp_615, 20)*/
             tmrp_615.nivel,          tmrp_615.grupo,
             tmrp_615.subgrupo,       tmrp_615.item,
             tmrp_615.qtde_necessaria
      BULK COLLECT INTO
             p_nivel,                 p_grupo,
             p_subgrupo,              p_item,
             p_qtde_aprogramar
        from tmrp_615
       where tmrp_615.nome_programa  = 'tmrp_f025'
         and tmrp_615.nr_solicitacao = p_nr_solicitacao
         and tmrp_615.codigo_usuario = p_codigo_usuario
         and tmrp_615.tipo_registro  = 255
         and tmrp_615.usuario        = p_usuario
         and tmrp_615.codigo_empresa = p_empresa_logada;
   end;

   if sql%found
   then

      FOR i IN p_nivel.FIRST .. p_nivel.LAST
      LOOP

         /*Busca quantidade em estoque para o item*/
         begin
            select /*parallel(estq_040, 20)*/
                   nvl(sum(estq_040.qtde_estoque_atu),0)
            into   p_qtde_estoque_atu
            from estq_040
            where  estq_040.cditem_nivel99  = p_nivel(i)
              and  estq_040.cditem_grupo    = p_grupo(i)
              and  estq_040.cditem_subgrupo = p_subgrupo(i)
              and  estq_040.cditem_item     = p_item(i)
              and ((p_inc_exc_dep = 1
              and   estq_040.deposito in (select /*parallel(rcnb_060, 20)*/
                                                 rcnb_060.grupo_estrutura
                                            from rcnb_060
                                           where rcnb_060.tipo_registro    = 37
                                             and rcnb_060.nr_solicitacao   = p_nr_solicitacao
                                             and rcnb_060.subgru_estrutura = p_codigo_usuario
                                             and rcnb_060.usuario_rel      = p_usuario
                                             and rcnb_060.empresa_rel      = p_empresa_logada
                                             and rcnb_060.relatorio_rel    = 'tmrp_f025'
                                             and rcnb_060.tp_reg_relatorio = 0))
               or  (p_inc_exc_dep = 2
              and   estq_040.deposito not in (select /*parallel(rcnb_060, 20)*/
                                                     rcnb_060.grupo_estrutura
                                                from rcnb_060
                                               where rcnb_060.tipo_registro    = 37
                                                 and rcnb_060.nr_solicitacao   = p_nr_solicitacao
                                                 and rcnb_060.subgru_estrutura = p_codigo_usuario
                                                 and rcnb_060.usuario_rel      = p_usuario
                                                 and rcnb_060.empresa_rel      = p_empresa_logada
                                                 and rcnb_060.relatorio_rel    = 'tmrp_f025'
                                                 and rcnb_060.tp_reg_relatorio = 0)))
              and estq_040.deposito in (select basi_205.codigo_deposito
                                          from basi_205
                                         where basi_205.local_deposito = p_empresa_filtro)
            group by estq_040.cditem_nivel99,  estq_040.cditem_grupo,
                     estq_040.cditem_subgrupo, estq_040.cditem_item;
            exception
               when others then
                  p_qtde_estoque_atu := 0.0000;
         end;

         if sql%notfound
         then
            p_qtde_estoque_atu := 0.0000;
         end if;
         /*Fim - Estoque*/

         /*Busca quantidade em transito*/
         begin
            select /*parallel(pcpt_020, 20)*/
                   sum(pcpt_020.qtde_quilos_acab)
              into p_qtde_transito
              from pcpt_020
             where pcpt_020.panoacab_nivel99  = p_nivel(i)
               and pcpt_020.panoacab_grupo    = p_grupo(i)
               and pcpt_020.panoacab_subgrupo = p_subgrupo(i)
               and pcpt_020.panoacab_item     = p_item(i)
               and pcpt_020.rolo_estoque      = 4
               and ((p_inc_exc_dep = 1
               and   pcpt_020.codigo_deposito in (select /*parallel(rcnb_060, 20)*/
                                                         rcnb_060.grupo_estrutura
                                                    from rcnb_060
                                                   where rcnb_060.tipo_registro    = 37
                                                     and rcnb_060.nr_solicitacao   = p_nr_solicitacao
                                                     and rcnb_060.subgru_estrutura = p_codigo_usuario
                                                     and rcnb_060.usuario_rel      = p_usuario
                                                     and rcnb_060.empresa_rel      = p_empresa_logada
                                                     and rcnb_060.relatorio_rel    = 'tmrp_f025'
                                                     and rcnb_060.tp_reg_relatorio = 0))
                or  (p_inc_exc_dep = 2
               and   pcpt_020.codigo_deposito not in (select /*parallel(rcnb_060, 20)*/
                                                             rcnb_060.grupo_estrutura
                                                        from rcnb_060
                                                      where rcnb_060.tipo_registro    = 37
                                                        and rcnb_060.nr_solicitacao   = p_nr_solicitacao
                                                        and rcnb_060.subgru_estrutura = p_codigo_usuario
                                                        and rcnb_060.usuario_rel      = p_usuario
                                                        and rcnb_060.empresa_rel      = p_empresa_logada
                                                        and rcnb_060.relatorio_rel    = 'tmrp_f025'
                                                        and rcnb_060.tp_reg_relatorio = 0)))
               and pcpt_020.codigo_deposito in (select basi_205.codigo_deposito
                                                  from basi_205
                                                 where basi_205.local_deposito = p_empresa_filtro)
            group by pcpt_020.panoacab_nivel99,  pcpt_020.panoacab_grupo,
                     pcpt_020.panoacab_subgrupo, pcpt_020.panoacab_item;
            exception
               when others then
                  p_qtde_transito := 0.0000;
         end;

         if sql%notfound
         then
            p_qtde_transito := 0.0000;
         end if;
         /*Fim - Quantidade em transito*/

         /*Desconsidera quantidade em transito*/
         if p_cons_transito = 0
         then
            p_qtde_estoque_atu := p_qtde_estoque_atu - p_qtde_transito;
         end if;

         /*Busca quantidade reservada para o item*/
         begin
            select /*parallel(tmrp_040, 20)*/
                   nvl(sum(tmrp_040.qtde_reservada),0)
              into p_qtde_reservada
              from tmrp_040
             where tmrp_040.co_reser_nivel99  = p_nivel(i)
               and tmrp_040.co_reser_grupo    = p_grupo(i)
               and tmrp_040.co_reser_subgrupo = p_subgrupo(i)
               and tmrp_040.co_reser_item     = p_item(i)
               and tmrp_040.periodo_producao  <> 0
            group by tmrp_040.co_reser_nivel99,  tmrp_040.co_reser_grupo,
                     tmrp_040.co_reser_subgrupo, tmrp_040.co_reser_item;
            exception
               when others then
                  p_qtde_reservada := 0.0000;
         end;

         if sql%notfound
         then
            p_qtde_reservada := 0.0000;
         end if;
         /*Fim - Reservada*/

         /*Busca quantidade programada para o item*/
         begin
            select /*parallel(tmrp_041, 20)*/
                   nvl(sum(tmrp_041.qtde_areceber),0)
              into p_qtde_programado
              from tmrp_041
             where tmrp_041.area_producao    = 2
               and tmrp_041.nivel_estrutura  = p_nivel(i)
               and tmrp_041.grupo_estrutura  = p_grupo(i)
               and tmrp_041.subgru_estrutura = p_subgrupo(i)
               and tmrp_041.item_estrutura   = p_item(i)
            group by tmrp_041.nivel_estrutura,  tmrp_041.grupo_estrutura,
                     tmrp_041.subgru_estrutura, tmrp_041.item_estrutura;
            exception
               when others then
                  p_qtde_programado := 0.0000;
         end;

         if sql%notfound
         then
            p_qtde_programado := 0.0000;
         end if;
         /*Fim - Programada*/

         /*Busca data da ultima compra*/
         begin
            select /*parallel(obrf_015, obrf_010, 20)*/
                   max(obrf_010.data_transacao)
              into p_data_ult_compra
              from obrf_015, obrf_010
             where obrf_015.coditem_nivel99   = p_nivel(i)
               and obrf_015.coditem_grupo     = p_grupo(i)
               and obrf_015.coditem_subgrupo  = p_subgrupo(i)
               and obrf_015.coditem_item      = p_item(i)
               and obrf_010.data_transacao   <= sysdate
               and obrf_010.tipo_conhecimento = 0
               and obrf_010.situacao_entrada  = 4
               and obrf_010.documento         = obrf_015.capa_ent_nrdoc
               and obrf_010.serie             = obrf_015.capa_ent_serie
               and obrf_010.cgc_cli_for_9     = obrf_015.capa_ent_forcli9
               and obrf_010.cgc_cli_for_4     = obrf_015.capa_ent_forcli4
               and obrf_010.cgc_cli_for_2     = obrf_015.capa_ent_forcli2;
            exception
               when others then
                  p_data_ult_compra := null;
         end;

         if sql%notfound
         then
            p_data_ult_compra := null;
         end if;
         /*Fim - Ultima compra*/

         /*Busca quantidade a receber de pedido de compra no periodo informado*/
         begin
            select /*parallel(supr_090, supr_100, 20)*/
                   nvl(sum(supr_100.qtde_saldo_item),0)
              into p_qtde_comprado
              from supr_090, supr_100
             where supr_090.pedido_compra = supr_100.num_ped_compra
               and supr_090.situacao_pedido not in (4,7) /*4 - BAIXADO TOTAL, 7 - PENDENTE*/
               and supr_090.cod_cancelamento  = 0
               and supr_090.codigo_empresa    = p_empresa_filtro
               and supr_100.cod_cancelamento  = 0
               and supr_100.item_100_nivel99  = p_nivel(i)
               and supr_100.item_100_grupo    = p_grupo(i)
               and supr_100.item_100_subgrupo = p_subgrupo(i)
               and supr_100.item_100_item     = p_item(i)
               and supr_100.situacao_item     <> 3 /*3 - BAIXA TOTAL*/
               and supr_100.data_prev_entr between p_data_ini_compra and p_data_fim_compra
            group by supr_100.item_100_nivel99,  supr_100.item_100_grupo,
                     supr_100.item_100_subgrupo, supr_100.item_100_item;
            exception
               when others then
                  p_qtde_comprado := 0.0000;
         end;

         if sql%notfound
         then
            p_qtde_comprado := 0.0000;
         end if;
         /*Fim - Quantidade comprada*/

         /*Busca quantidade a receber de requisicoes de compra no periodo informado*/
         begin
            select /*parallel(supr_067)*/
                   nvl(sum(supr_067.qtde_requisitada),0)
              into p_qtde_requisitada
              from supr_067
             where supr_067.numero_pedido     = 0
               and supr_067.cod_cancelamento  = 0
               and supr_067.situacao         <> 8
               and supr_067.item_req_nivel99  = p_nivel(i)
               and supr_067.item_req_grupo    = p_grupo(i)
               and supr_067.item_req_subgrupo = p_subgrupo(i)
               and supr_067.item_req_item     = p_item(i)
               and supr_067.data_prev_entr between p_data_ini_compra and p_data_fim_compra;
            exception
               when others then
                  p_qtde_requisitada := 0.0000;
         end;

         if sql%notfound
         then
            p_qtde_requisitada := 0.0000;
         end if;
         /*Fim - Quantidade requisitada*/

         /*Busca data ultima entrega*/
         p_data_ultima_ent := null;

         begin
            select nvl(proxima_entrega.data_entrega, null)
              into p_data_ultima_ent
              from (select supr_100.data_prev_entr data_entrega
                      from supr_090, supr_100
                     where supr_090.pedido_compra = supr_100.num_ped_compra
                       and supr_090.situacao_pedido not in (4,7) /*4 - BAIXADO TOTAL, 7 - PENDENTE*/
                       and supr_090.cod_cancelamento  = 0
                       and supr_090.codigo_empresa    = p_empresa_filtro
                       and supr_100.cod_cancelamento  = 0
                       and supr_100.item_100_nivel99  = p_nivel(i)
                       and supr_100.item_100_grupo    = p_grupo(i)
                       and supr_100.item_100_subgrupo = p_subgrupo(i)
                       and supr_100.item_100_item     = p_item(i)
                       and supr_100.situacao_item     <> 3 /*3 - BAIXA TOTAL*/
                       and supr_100.data_prev_entr between p_data_ini_compra and p_data_fim_compra
                     UNION ALL
                     select supr_067.data_prev_entr data_entrega
                       from supr_067
                      where supr_067.numero_pedido     = 0
                        and supr_067.cod_cancelamento  = 0
                        and supr_067.situacao         <> 8
                        and supr_067.item_req_nivel99  = p_nivel(i)
                        and supr_067.item_req_grupo    = p_grupo(i)
                        and supr_067.item_req_subgrupo = p_subgrupo(i)
                        and supr_067.item_req_item     = p_item(i)
                        and supr_067.data_prev_entr between p_data_ini_compra and p_data_fim_compra) proxima_entrega
             where rownum = 1
            order by proxima_entrega.data_entrega DESC;
            exception
               when no_data_found then
                  p_data_ultima_ent := null;
         end;

         if sql%notfound
         then
            p_data_ultima_ent := null;
         end if;
         /*Fim - Data ultima entrega*/

         /*Calcula quantidade disponivel -> (Estoque - Reservado - A programar) + Programado*/
         p_qtde_disponivel := p_qtde_estoque_atu - p_qtde_reservada - p_qtde_aprogramar(i) + p_qtde_programado;

         /*Calcula quantidade situacao liquida -> (Disponivel + Comprado/Requisicao)*/
         p_qtde_sit_liquida := p_qtde_disponivel + p_qtde_comprado;

         /*Calcula quantidade total pendente de entrega. -> (Pedidos de compras + Requisicoes de compras)*/
         p_qtde_tot_compras := p_qtde_comprado + p_qtde_requisitada;

         /*Atualiza quantidades para cada item.*/
         begin
            update tmrp_615
               set tmrp_615.qtde_estoque      = p_qtde_estoque_atu,
                   tmrp_615.qtde_reserva      = p_qtde_reservada,
                   tmrp_615.qtde_receber      = p_qtde_programado,
                   tmrp_615.qtde_saldo        = p_qtde_disponivel,
                   tmrp_615.qtde_programacao  = p_qtde_sit_liquida,
                   tmrp_615.data_sugestao     = p_data_ult_compra,
                   tmrp_615.qtde_requisitada  = p_qtde_tot_compras,
                   tmrp_615.data_real_inicio  = p_data_ultima_ent
             where tmrp_615.nome_programa  = 'tmrp_f025'
               and tmrp_615.nivel          = p_nivel(i)
               and tmrp_615.grupo          = p_grupo(i)
               and tmrp_615.subgrupo       = p_subgrupo(i)
               and tmrp_615.item           = p_item(i)
               and tmrp_615.nr_solicitacao = p_nr_solicitacao
               and tmrp_615.codigo_usuario = p_codigo_usuario
               and tmrp_615.tipo_registro  = 255
               and tmrp_615.usuario        = p_usuario
               and tmrp_615.codigo_empresa = p_empresa_logada;
            exception
               when others then
                  --ATENCAO! Nao atualizou TMRP_615 {0}. Status = {1}
                  p_mensagem := inter_fn_buscar_tag_composta('ds21501',
                                                             'TMRP_615',
                                                             sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',
                                                             p_locale_usuario,
                                                             p_usuario_systextil);

                  raise_application_error(-20000, p_mensagem);
         end;
      END LOOP;
   end if;

end inter_pr_nec_quantidades;

 

/

exec inter_pr_recompile;

