 insert into pedi_016
   (cnpj9, cnpj4, cnpj2, atributo, conteudo)
 select cgc_9, cgc_4, cgc_2, 'pedido.forma_pagamento', '0' from pedi_010
 where  not exists (select 1 from pedi_016 a
                    where a.cnpj9 = pedi_010.cgc_9
                    and a.cnpj4 = pedi_010.cgc_4
                    and a.cnpj2 = pedi_010.cgc_2
                    and a.atributo = 'pedido.forma_pagamento' );
 
 commit;
 exec inter_pr_recompile;
/
