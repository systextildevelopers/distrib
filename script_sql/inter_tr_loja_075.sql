
  CREATE OR REPLACE TRIGGER "INTER_TR_LOJA_075" 
   after insert or delete or update
   on loja_075
   for each row

declare

   w_inserir_deletar number(1);
   w_cgc9 number(9);
   w_cgc4 number(4);
   w_cgc2 number(2);

begin
  /**
   * 0 - Deletar
   * 1 - Inserir
   * 2 - Nao executar
   */
   w_inserir_deletar := 2;

   if inserting
   then
       if :new.devolucao = 1
       then
          w_inserir_deletar := 1;
       end if;
   end if;

   if updating
   then
       if :old.devolucao = 0 and :new.devolucao = 1
       then
          w_inserir_deletar := 1;
       elsif :old.devolucao = 1 and :new.devolucao = 0
       then
          w_inserir_deletar := 0;
       end if;
   end if;

   if deleting
   then
       if :old.devolucao = 1
       then
          w_inserir_deletar := 0;
       end if;
   end if;

   if w_inserir_deletar = 1
   then

      BEGIN
        select loja_200.cgc9, loja_200.cgc4,
               loja_200.cgc2
        into   w_cgc9       , w_cgc4,
               w_cgc2
        from loja_200
        where loja_200.documento = :new.documento
          and loja_200.cod_empresa = :new.cod_empresa;
      EXCEPTION
         WHEN no_data_found THEN
            raise_application_error(-20000, 'Nao encontrou o orcamento '||to_char(:new.documento)||' para a empresa '||to_char(:new.cod_empresa)||'.');
      END;

      if :new.valor_parc > 0
      then
         inter_pr_gera_debito_devolucao (:new.valor_parc,
                                          w_cgc9,
                                          w_cgc4,
                                          w_cgc2,
                                          :new.cod_empresa,
                                          :new.documento,
                                          'VALOR UTILIZADO NO ORCAMENTO: '||to_char(:new.documento, '000000'));
      end if;
   end if;

   if w_inserir_deletar = 0
   then

      BEGIN
         delete from loja_061
         where loja_061.nr_orcamento_loja = :old.documento
           and loja_061.codigo_empresa    = :old.cod_empresa;
      EXCEPTION
        WHEN others THEN
           raise_application_error(-20000, 'Nao deletou LOJA_061(Devolucoes). Erro: '||SQLERRM);
      END;

   end if;

end inter_tr_loja_075;
-- ALTER TRIGGER "INTER_TR_LOJA_075" ENABLE
 

/

exec inter_pr_recompile;

