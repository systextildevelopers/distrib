
  CREATE OR REPLACE PROCEDURE "INTER_PR_VERIF_REG_EM_PRODUCAO" (
   p_tipo_verificacao  in number,
   p_executa_trigger   in number
) is
   v_tem_reg_615  number;
   v_tem_reg_400  number;

   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);
begin
   -- Dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   v_tem_reg_400 := inter_fn_executou_recalculo();

   -- 0 - VERIFICA (TMRP_615)
   -- 1 - VERIFICA (BASI_400)
   -- 2 - VERIFICA (TMRP_615) e (BASI_400)
   if p_tipo_verificacao in (0,2)
   then
      v_tem_reg_615 := 0;

      begin
         select nvl(count(1),0)
         into v_tem_reg_615
         from tmrp_615
         where tmrp_615.nr_solicitacao = 144
           and tmrp_615.tipo_registro  = 144
           and tmrp_615.nome_programa  = 'recalc_em_prod_a_prod';
      exception when others then
         v_tem_reg_615 := 0;
      end;

      if v_tem_reg_615 <> 0
      then
         raise_application_error (-20000, 'ATEN��O! O processo de calculo do em producao e a produzir esta sendo executado. Nao pode executar esta operacao.');
      end if;
   end if;

   if p_tipo_verificacao in (1,2)
   then
      if v_tem_reg_400 <> 0
      then
         if (p_executa_trigger is null
         or p_executa_trigger = 0
         or p_executa_trigger = 1)
         and 1=3
         then
            raise_application_error (-20000, 'ATEN��O! Foi executado o processo de calculo do em produ��o e a produzir, mas esta sendo executado um programa que esta desatualizado.');
            --inter_fn_buscar_tag_composta('ds30752#ATEN��O! O processo de calculo do em producao e a produzir esta sendo executado. Nao pode executar esta operacao.', 'HIST_100(001-3)' , sqlerrm , '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil )
         end if;
      else
         if not p_executa_trigger is null
         and p_executa_trigger = 3
         then
            raise_application_error (-20000, 'ATEN��O! N�o foi executado o processo de c�lculo do em produ��o e a produzir. Execute o c�lculo do em produ��o e a produzir (pcpc_f144)');
            --inter_fn_buscar_tag_composta('ds30752#ATEN��O! O processo de calculo do em producao e a produzir esta sendo executado. Nao pode executar esta operacao.', 'HIST_100(001-3)' , sqlerrm , '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil )
         end if;
      end if;
   end if;

   if  v_tem_reg_400 = 0
   and (to_date('31/10/2012 ' || to_CHAR(sysdate,'hh24:mi'),'dd/mm/yyyy hh24:mi') - sysdate) <= 0
   then
      raise_application_error (-20000, 'ATEN��O! N�o foi executado o processo de c�lculo do em produ��o e a produzir. Execute o c�lculo do em produ��o e a produzir (pcpc_f144)');
   end if;

end inter_pr_verif_reg_em_producao;

 

/

exec inter_pr_recompile;

