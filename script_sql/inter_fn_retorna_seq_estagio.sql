create or replace function inter_fn_retorna_seq_estagio(pOrdemProducao in number)
return varchar2
is
  pEstagio varchar2(500);
begin

  pEstagio := '0';

  for periodo in (select to_char(pcpc_040.codigo_estagio) codigo_estagio,
                         pcpc_040.seq_operacao
                  from pcpc_040
                  where pcpc_040.ordem_producao = pOrdemProducao
                    and pcpc_040.qtde_disponivel_baixa > 0
                  group by pcpc_040.codigo_estagio,
                           pcpc_040.seq_operacao
                  order by pcpc_040.seq_operacao
  )
  LOOP
    if pEstagio = '0'
    then
       pEstagio := periodo.codigo_estagio;
    else
       pEstagio := pEstagio || ', ' || periodo.codigo_estagio;
    end if;
  END LOOP;

  return(pEstagio);

end inter_fn_retorna_seq_estagio;

/
