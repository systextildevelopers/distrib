
  CREATE OR REPLACE TRIGGER "INTER_TR_BASI_325_LOG" 
   after insert or
         delete or
         update of cod_solicitacao, cod_amostra, ordem_producao, nivel_receita,
		grupo_receita, subgrupo_receita, item_receita, alternativa_receita,
		data_amostra, data_envio, data_retorno, situacao,
		laudo, observacao
   on basi_325
   for each row
declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   ws_desc_receita_n         varchar2(120);
   ws_descr_ref_n            varchar2(120);
   ws_descr_tam_n            varchar2(120);
   ws_descr_item_n           varchar2(120);

   ws_codigo_projeto_n       varchar2(6);
   ws_sequencia_projeto_n    number(4);

   ws_teve                   varchar2(1);

   long_aux                  varchar2(2000);
begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   if inserting
   then
      begin
         select basi_030.descr_referencia
         into ws_descr_ref_n
         from basi_030
         where  basi_030.nivel_estrutura  = :new.nivel_receita
           and  basi_030.referencia       = :new.grupo_receita;
      exception when no_data_found then
         ws_descr_ref_n := '';
      end;

      begin
         select basi_020.descr_tam_refer
         into ws_descr_tam_n
         from basi_020
         where  basi_020.basi030_nivel030  = :new.nivel_receita
           and  basi_020.basi030_referenc  = :new.grupo_receita
           and  basi_020.tamanho_ref       = :new.subgrupo_receita;
      exception when no_data_found then
         ws_descr_tam_n := '';
      end;

      begin
         select basi_010.descricao_15
         into ws_descr_item_n
         from basi_010
         where basi_010.nivel_estrutura  = :new.nivel_receita
           and basi_010.grupo_estrutura  = :new.grupo_receita
           and basi_010.subgru_estrutura = :new.subgrupo_receita
           and basi_010.item_estrutura   = :new.item_receita;
      exception when no_data_found then
         ws_descr_item_n := '';
      end;

      ws_desc_receita_n := ws_descr_ref_n || ' ' || ws_descr_tam_n || ' ' || ws_descr_item_n;

      begin
         select basi_330.codigo_projeto, basi_330.seq_projeto
         into   ws_codigo_projeto_n,     ws_sequencia_projeto_n
         from basi_330
         where basi_330.cod_solicitacao = :new.cod_solicitacao
           and rownum                   = 1;
      exception when no_data_found then
         ws_codigo_projeto_n := '00000';
         ws_sequencia_projeto_n := 0;
      end;

      begin
         INSERT INTO hist_100
            ( tabela,             operacao,
              data_ocorr,         aplicacao,
              usuario_rede,       maquina_rede,
              num08,              num09,
              long01
            )
         VALUES
            ( 'BASI_325',             'I',
              sysdate,                 ws_aplicativo,
              ws_usuario_rede ,        ws_maquina_rede,
              :new.cod_solicitacao,    :new.cod_amostra,
              inter_fn_buscar_tag('lb34898#SOLICITACAO DE DESENVOLVIMENTO DE COR',ws_locale_usuario,ws_usuario_systextil) ||
              chr(10)                                               ||
              chr(10)                                               ||
              inter_fn_buscar_tag('lb31314#SOLICITACAO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                          || to_char(:new.cod_solicitacao,'00000') ||
              chr(10)                                               ||
              inter_fn_buscar_tag('lb32293#AMOSTRA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                          || to_char(:new.cod_amostra, '000') ||
              chr(10)                                               ||
              inter_fn_buscar_tag('lb12988#RECEITA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                          || :new.nivel_receita     ||
              '.'                         || :new.grupo_receita     ||
              '.'                         || :new.subgrupo_receita  ||
              '.'                         || :new.item_receita      ||
              ' - '                       || ws_desc_receita_n      ||
              chr(10)                                               ||
              inter_fn_buscar_tag('lb04571#ALTERNATIVA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                          || to_char(:new.alternativa_receita,'00')   ||
              chr(10)                                               ||
              inter_fn_buscar_tag('lb34899#DATA AMOSTRA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                          || to_char(:new.data_amostra,'DD/MM/YYYY') ||
              chr(10)                                               ||
              inter_fn_buscar_tag('lb04627#DATA ENVIO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                          || to_char(:new.data_envio,'DD/MM/YYYY') ||
              chr(10)                                               ||
              inter_fn_buscar_tag('lb03017#DATA RETORNO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                          || to_char(:new.data_retorno,'DD/MM/YYYY') ||
              chr(10)                                               ||
              inter_fn_buscar_tag('lb00303#SITUACAO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                          || to_char(:new.situacao,'0') ||
              chr(10)                                               ||
              inter_fn_buscar_tag('lb05423#PROJETO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                          || ws_codigo_projeto_n    ||
              chr(10)                                               ||
              inter_fn_buscar_tag('lb30061#SEQUENCIA PROJETO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                          || ws_sequencia_projeto_n ||
              chr(10)
          );
      exception when OTHERS then
         raise_application_error (-20000, inter_fn_buscar_tag_composta('ds22559#ATENCAO! Nao inseriu {0}. Status: {1}.', 'HIST_100(325-1)' , sqlerrm , '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;
   end if;

   if updating
   then
      begin
         select basi_030.descr_referencia
         into ws_descr_ref_n
         from basi_030
         where  basi_030.nivel_estrutura  = :new.nivel_receita
           and  basi_030.referencia       = :new.grupo_receita;
      exception when no_data_found then
         ws_descr_ref_n := '';
      end;

      begin
         select basi_020.descr_tam_refer
         into ws_descr_tam_n
         from basi_020
         where  basi_020.basi030_nivel030  = :new.nivel_receita
           and  basi_020.basi030_referenc  = :new.grupo_receita
           and  basi_020.tamanho_ref       = :new.subgrupo_receita;
      exception when no_data_found then
         ws_descr_tam_n := '';
      end;

      begin
         select basi_010.descricao_15
         into ws_descr_item_n
         from basi_010
         where basi_010.nivel_estrutura  = :new.nivel_receita
           and basi_010.grupo_estrutura  = :new.grupo_receita
           and basi_010.subgru_estrutura = :new.subgrupo_receita
           and basi_010.item_estrutura   = :new.item_receita;
      exception when no_data_found then
         ws_descr_item_n := '';
      end;

      ws_desc_receita_n := ws_descr_ref_n || ' ' || ws_descr_tam_n || ' ' || ws_descr_item_n;

      begin
         select basi_330.codigo_projeto, basi_330.seq_projeto
         into   ws_codigo_projeto_n,     ws_sequencia_projeto_n
         from basi_330
         where basi_330.cod_solicitacao =:new.cod_solicitacao
           and rownum                   = 1;
      exception when no_data_found then
         ws_codigo_projeto_n := '00000';
         ws_sequencia_projeto_n := 0;
      end;

      ws_teve := 's';
      long_aux := long_aux ||
           inter_fn_buscar_tag('lb34898#SOLICITACAO DE DESENVOLVIMENTO DE COR',ws_locale_usuario,ws_usuario_systextil) || '(BASI_325)' ||
           chr(10)                                               ||
           chr(10)                                               ||
           inter_fn_buscar_tag('lb31314#SOLICITACAO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || to_char(:new.cod_solicitacao,'00000') ||
           chr(10)                                               ||
           inter_fn_buscar_tag('lb32293#AMOSTRA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || to_char(:new.cod_amostra, '000') ||
           chr(10)                                               ||
           inter_fn_buscar_tag('lb12988#RECEITA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || :new.nivel_receita     ||
           '.'                         || :new.grupo_receita     ||
           '.'                         || :new.subgrupo_receita  ||
           '.'                         || :new.item_receita      ||
           ' - '                       || ws_desc_receita_n      ||
           chr(10)                                               ||
           inter_fn_buscar_tag('lb04571#ALTERNATIVA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || to_char(:new.alternativa_receita,'00')   ||
           chr(10)                                               ||
           inter_fn_buscar_tag('lb34899#DATA AMOSTRA:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || to_char(:new.data_amostra,'DD/MM/YYYY') ||
           chr(10)                                               ||
           inter_fn_buscar_tag('lb04627#DATA ENVIO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || to_char(:old.data_envio,'DD/MM/YYYY') ||
           '-> '                       || to_char(:new.data_envio,'DD/MM/YYYY') ||
           chr(10)                                               ||
           inter_fn_buscar_tag('lb03017#DATA RETORNO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || to_char(:old.data_retorno,'DD/MM/YYYY') ||
           ' -> '                      || to_char(:new.data_retorno,'DD/MM/YYYY') ||
           chr(10)                                               ||
           inter_fn_buscar_tag('lb00303#SITUACAO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || to_char(:old.situacao,'0') ||
           ' -> '                      || to_char(:new.situacao,'0') ||
           chr(10)                                              ||
           inter_fn_buscar_tag('lb05423#PROJETO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || ws_codigo_projeto_n    ||
           chr(10)                                              ||
           inter_fn_buscar_tag('lb30061#SEQUENCIA PROJETO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                       || to_char(ws_sequencia_projeto_n,'0000') ||
           chr(10);

      if ws_teve = 's'
      then
         begin
            INSERT INTO hist_100
               ( tabela,                operacao,
                 data_ocorr,            aplicacao,
                 usuario_rede,          maquina_rede,
                 num08,                 num09,
                 long01
               )
            VALUES
               ( 'BASI_325',            'A',
                 sysdate,               ws_aplicativo,
                 ws_usuario_rede,       ws_maquina_rede,
                 :new.cod_solicitacao,  :new.cod_amostra,
                 long_aux
            );
         exception when OTHERS then
            raise_application_error (-20000, inter_fn_buscar_tag_composta('ds22559#ATENCAO! Nao inseriu {0}. Status: {1}.', 'HIST_100(325-2)' , sqlerrm , '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
         end;
      end if;
   end if;

   if deleting
   then
      begin
         INSERT INTO hist_100
            ( tabela,                operacao,
              data_ocorr,            aplicacao,
              usuario_rede,          maquina_rede,
              num08,                 num09
            )
         VALUES
            ( 'BASI_325',            'D',
              sysdate,               ws_aplicativo,
              ws_usuario_rede,       ws_maquina_rede,
              :old.cod_solicitacao,  :old.cod_amostra
         );
      exception when OTHERS then
         raise_application_error (-20000, inter_fn_buscar_tag_composta('ds22559#ATENCAO! Nao inseriu {0}. Status: {1}.', 'HIST_100(325-3)' , sqlerrm , '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;
   end if;
end inter_tr_basi_325_log;

-- ALTER TRIGGER "INTER_TR_BASI_325_LOG" ENABLE
 

/

exec inter_pr_recompile;

