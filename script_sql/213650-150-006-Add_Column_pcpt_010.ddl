--Coluna pcpt_010.fam_carretel   
ALTER TABLE pcpt_010 ADD fam_carretel NUMBER(3);

COMMENT ON COLUMN pcpt_010.fam_carretel IS 'Familia Carretel (endr_011)';

--Coluna pcpt_010.num_carretel  
ALTER TABLE pcpt_010 ADD num_carretel VARCHAR2(15);

COMMENT ON COLUMN pcpt_010.num_carretel IS 'Numero do Carretel (endr_012)';
