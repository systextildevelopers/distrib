
  CREATE OR REPLACE PROCEDURE "INTER_PR_GRAVA_TMRP_615_ITEM" 
   (p_empresa_logada   in number,
    p_nivel_item       in varchar2,
    p_grupo_item       in varchar2,
    p_item_item        in varchar2,
    p_alternativa_item in number,
    p_qtde_programada  in number,
    p_nivel            in varchar2,
    p_grupo            in varchar2,
    p_subgrupo         in varchar2,
    p_item             in varchar2,
    p_codigo_usuario   in number,
    p_usuario          in varchar2,
    p_nr_solicitacao   in number,
    p_nome_programa    in varchar2,
    p_tipo_registro    in number,
    p_origem           in varchar2,
    p_consumo          in number,
    p_pedido_venda     in number,
    p_codigo_projeto   in varchar2,
    p_nro_insert       in number) is
begin

declare

   p_count             number(10);
   p_mensagem          varchar2(200);
   p_usuario_rede      varchar2(20);
   p_maquina_rede      varchar2(40);
   p_aplicativo        varchar2(20);
   p_sid               number(9);
   p_usuario_systextil varchar2(250);
   p_locale_usuario    varchar2(5);
   p_narrativa         varchar2(100);
   p_empresa_usu       number(3);
   p_narrativa_item    varchar2(100);
   p_origem_desc       varchar2(100);
   v_codigo_projeto    varchar2(6);
BEGIN

   inter_pr_dados_usuario (p_usuario_rede,
                           p_maquina_rede,
                           p_aplicativo,
                           p_sid,
                           p_usuario_systextil,
                           p_empresa_usu,
                           p_locale_usuario);
   begin
      select count(*)
      into p_count
      from tmrp_615
      where tmrp_615.nr_solicitacao    = p_nr_solicitacao
        and tmrp_615.codigo_usuario    = p_codigo_usuario
        and tmrp_615.tipo_registro     = p_tipo_registro
        and tmrp_615.usuario           = p_usuario
        and tmrp_615.codigo_empresa    = p_empresa_logada
        and tmrp_615.nome_programa     = p_nome_programa
        and tmrp_615.pedido_venda      = p_pedido_venda
        and tmrp_615.nivel_prod        = p_nivel_item
        and tmrp_615.grupo_prod        = p_grupo_item
        and tmrp_615.item_prod         = p_item_item
        and tmrp_615.alternativa_prod  = p_alternativa_item
        and tmrp_615.nivel             = p_nivel
        and tmrp_615.grupo             = p_grupo
        and tmrp_615.subgrupo          = p_subgrupo
        and tmrp_615.item              = p_item
        and round(tmrp_615.consumo,5)  = round(p_consumo,5);
   exception when no_data_found then
      p_count := 0;
   end;

   if p_count = 0
   then

      begin
         select basi_010.narrativa
           into p_narrativa
           from basi_010
          where basi_010.nivel_estrutura  = p_nivel
            and basi_010.grupo_estrutura  = p_grupo
            and basi_010.subgru_estrutura = p_subgrupo
            and basi_010.item_estrutura   = p_item;
         exception
            when no_data_found then
               p_narrativa := null;
      end;

      begin
         select basi_010.narrativa
           into p_narrativa_item
           from basi_010
          where basi_010.nivel_estrutura  = p_nivel_item
            and basi_010.grupo_estrutura  = p_grupo_item
            and basi_010.item_estrutura   = p_item_item
            and rownum                    = 1;
         exception
            when no_data_found then
               p_narrativa_item := null;
      end;

      if p_origem = 'basi_101'
      then
         p_origem_desc := 'Desenvolvimento de Produto(basi_f101)' || to_char(p_nro_insert);
      end if;

      if p_origem = 'basi_850'
      then
         p_origem_desc := 'Cadastro de componentes da combinacao(basi_f863)' || to_char(p_nro_insert);
      end if;

      if p_origem = 'basi_050'
      then
         p_origem_desc := 'Estrutura de produto(basi_f390)'  || to_char(p_nro_insert);
      end if;


      if p_codigo_projeto is null
      then
         v_codigo_projeto := '000000';
      else
         v_codigo_projeto := p_codigo_projeto;

      end if;

      begin
         insert into  /*+APPEND*/ tmrp_615 (
            nome_programa,     nr_solicitacao,
            codigo_usuario,    tipo_registro,
            usuario,           codigo_empresa,
            nivel_prod,        grupo_prod,
            item_prod,         alternativa_prod,
            observacao,
            nivel,             grupo,
            subgrupo,          item,
            qtde_pedido,       narrativa,
            observacao2,       consumo,
            pedido_venda,      codigo_projeto,
            roteiro_prod
         ) values (
            p_nome_programa,   p_nr_solicitacao,
            p_codigo_usuario,  p_tipo_registro,
            p_usuario,         p_empresa_logada,
            p_nivel_item,      p_grupo_item,
            p_item_item,       p_alternativa_item,
            p_narrativa_item,
            p_nivel,           p_grupo,
            p_subgrupo,        p_item,
            p_qtde_programada, p_narrativa,
            p_origem_desc,     p_consumo,
            p_pedido_venda,    v_codigo_projeto,
            p_nro_insert
         );

      exception
            when others then
               --ATENCAO! Nao inseriu TMRP_615 {0}. Status: {1}
               p_mensagem := inter_fn_buscar_tag_composta('ds21459',
                                                          'TMRP_615',
                                                          sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',
                                                          p_locale_usuario,
                                                          p_usuario_systextil);

               raise_application_error(-20000, p_mensagem);

      end;


   else

      begin
         update tmrp_615
            set tmrp_615.qtde_pedido    = tmrp_615.qtde_pedido + p_qtde_programada
         where tmrp_615.nr_solicitacao = p_nr_solicitacao
           and tmrp_615.codigo_usuario = p_codigo_usuario
           and tmrp_615.tipo_registro  = p_tipo_registro
           and tmrp_615.usuario        = p_usuario
           and tmrp_615.codigo_empresa = p_empresa_logada
           and tmrp_615.nome_programa  = p_nome_programa
           and tmrp_615.pedido_venda   = p_pedido_venda
           and tmrp_615.nivel_prod     = p_nivel_item
           and tmrp_615.grupo_prod     = p_grupo_item
           and tmrp_615.item_prod      = p_item_item
           and tmrp_615.alternativa_prod  = p_alternativa_item
           and tmrp_615.nivel          = p_nivel
           and tmrp_615.grupo          = p_grupo
           and tmrp_615.subgrupo       = p_subgrupo
           and tmrp_615.item           = p_item
           and round(tmrp_615.consumo,5) = round(p_consumo,5);

      exception when others then
         --ATENCAO! Nao atualizou TMRP_615 {0}. Status = {1}
         p_mensagem := inter_fn_buscar_tag_composta('ds21501',
                                                          'TMRP_615',
                                                          sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',
                                                          p_locale_usuario,
                                                          p_usuario_systextil);

         raise_application_error(-20000, p_mensagem);

      end;

   end if;

END;


end inter_pr_grava_tmrp_615_item;

 

/

exec inter_pr_recompile;

