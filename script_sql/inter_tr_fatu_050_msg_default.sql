create or replace trigger inter_tr_fatu_050_msg_default
   before insert or update of valor_itens_nfis, nota_entrega, nota_fatura, valor_encar_nota, encargos, transp_redespacho9,
                              valor_icms, cod_status, situacao_nfisc, status_impressao_danfe
                              on fatu_050
   for each row

   
declare

     cursor v_box (p_nr_pedido number) is
       select estq_170.corredor, estq_170.box, estq_170.qtde_rolos
       from estq_170
       where estq_170.nr_pedido = p_nr_pedido
         and :new.pedido_venda > 0
       ORDER BY estq_170.corredor, estq_170.box;
     
     cursor fatu060_msg is
       select distinct fatu_060.pedido_venda
       from fatu_060
       where fatu_060.ch_it_nf_cd_empr = :new.codigo_empresa
       and   fatu_060.ch_it_nf_num_nfis = :new.num_nota_fiscal
       and   fatu_060.ch_it_nf_ser_nfis = :new.serie_nota_fisc
       and   fatu_060.pedido_venda > 0
       order by fatu_060.pedido_venda;

     w_valor_carga_trib_municipal number;
     w_valor_carga_trib_estadual  number;

     w_aliquota_municipal         number;
     w_aliquota_estadual          number;
     w_metros_cubicos             number(11,5);

     v_ind_local                   varchar2(1);
     v_des_mensagem1               obrf_874.des_mensagem1%type;
     v_des_mensagem2               obrf_874.des_mensagem1%type;
     v_des_mensagem3               obrf_874.des_mensagem1%type;
     v_des_mensagem4               obrf_874.des_mensagem1%type;
     v_des_mensagem5               obrf_874.des_mensagem1%type;
     v_des_mensagem6               obrf_874.des_mensagem1%type;
     v_des_mensagem7               obrf_874.des_mensagem1%type;
     v_des_mensagem8               obrf_874.des_mensagem1%type;
     v_des_mensagem9               obrf_874.des_mensagem1%type;
     v_des_mensagem10              obrf_874.des_mensagem1%type;
     v_desc_cond_pgto              pedi_070.descr_pg_cliente%type;
     v_numero_celular              pedi_010.celular_cliente%type;
     v_ddd_cidade                  basi_160.ddd%type;
     v_classificacao_pedido        pedi_100.classificacao_pedido%type;
     v_desc_class_pedido           pedi_060.descricao%type;

     w_cod_msg_tabela           number(6);
     w_vl_msg_tabela            varchar2(60);
     w_cod_msg_box              number(6);
     w_vl_msg_box               varchar2(2000);
     w_vl_msg_tot_pecas         number(9);
     w_vl_msg_tot_rolos         number(9);
     w_vl_msg_represen          varchar2(60);
   w_nota_fatura              fatu_050.nota_fatura%type;
   w_nota_entrega             fatu_050.nota_entrega%type;
     w_cod_msg_nrpedido         pedi_100.pedido_venda%type;
     w_cod_msg_tot_pecas        number(6);
     w_cod_msg_tot_rolo         number(6);
     w_cod_msg_represen         number(6);
     w_cod_msg_perc_comis       number(6);
     w_cod_msg_gm2              number(6);
     w_cod_msg_largura          number(6);
     w_cod_msg_mt_cubicos       number(6);
     w_cod_msg_encargos         number(6);
     w_cod_msg_suframa          number(6);
     w_cod_sequencia_tabela     number(2);
     w_cod_sequencia_box        number(2);
     w_cod_sequencia_nrpedido   number(2);
     w_cod_sequencia_tot_pecas  number(2);
     w_cod_sequencia_tot_rolo   number(2);
     w_cod_sequencia_represen   number(2);
     w_cod_sequencia_perc_comis number(2);
     w_cod_sequencia_gm2        number(2);
     w_cod_sequencia_mt_cubicos number(2);
     w_cod_sequencia_largura    number(2);
     w_cod_sequencia_descontos  number(2);
     w_cod_local_tabela         varchar2(1);
     w_cod_local_box            varchar2(1);
     w_cod_local_nrpedido       varchar2(1);
     w_cod_local_tot_pecas      varchar2(1);
     w_cod_local_tot_rolo       varchar2(1);
     w_cod_local_represen       varchar2(1);
     w_cod_local_perc_comis     varchar2(1);
     w_cod_local_gm2            varchar2(1);
     w_cod_local_largura        varchar2(1);
     w_cod_local_mt_cubicos     varchar2(1);
     w_cod_local_encargos       varchar2(1);
     w_cod_local_suframa        varchar2(1);
     w_ind_descontos            number(1);
     w_flag_possui_rolo         varchar2(200);
     w_cod_sequencia_natureza   number(2);
     w_cod_sequencia_encargos   number(2);
     w_cod_sequencia_suframa    number(2);
     w_nr_suframa_cli           pedi_010.nr_suframa_cli%type;
     w_frete_suframa            number(15,2);
     w_base_suframa             number(15,2);
     w_perc_pis_suframa         fatu_502.perc_pis_suframa%type;
     w_perc_cofins_suframa      fatu_502.perc_cofins_suframa%type;
     w_perc_suframa             fatu_500.perc_suframa%type;

     w_cod_msg_simples_rem      obrf_180.cod_mensagem_simples_rem%type;
     w_cod_sequencia_simples_rem obrf_180.cod_sequencia_simples_rem%type;
     w_cod_local_simples_rem    obrf_180.cod_local_simples_rem%type;
     w_cod_msg_entrega          obrf_180.cod_mensagem_entrega%type;
     w_cod_sequencia_entrega    obrf_180.cod_sequencia_entrega%type;
     w_cod_local_entrega        obrf_180.cod_local_entrega%type;
     w_cod_msg_redespacho       obrf_180.cod_mensagem_redes%type;
     w_cod_sequencia_redespacho obrf_180.cod_sequencia_redes%type;
     w_cod_local_redespacho     obrf_180.cod_local_redes%type;
     w_imp_pedido_forca_venda   obrf_180.imp_pedido_forca_venda%type;

     w_nome_cliente             pedi_010.nome_cliente%type;
     w_insc_est_cliente         pedi_010.insc_est_cliente%type;

     w_cd_cli_cgc_cli9          pedi_150.cd_cli_cgc_cli9%type;
     w_cd_cli_cgc_cli4          pedi_150.cd_cli_cgc_cli4%type;
     w_cd_cli_cgc_cli2          pedi_150.cd_cli_cgc_cli2%type;

     w_cid_entr_cobr            varchar2(200);
     w_bairro_entr_cobr         pedi_150.bairro_entr_cobr%type;
     w_end_entr_cobr            pedi_150.end_entr_cobr%type;
     w_numero_imovel            pedi_150.numero_imovel%type;
     w_complemento_endereco     pedi_150.complemento_endereco%type;

     w_cidade_rede              basi_160.cidade%type;
     w_estado_rede              basi_160.estado%type;

     w_pedido_cliente           varchar2(1000);
     w_id_pedido_forca_vendas   pedi_100.id_pedido_forca_vendas%type;

     w_transportadora           supr_010.nome_fornecedor%type;
     w_endereco_rede            supr_010.endereco_forne%type;
     w_bairro_rede              supr_010.bairro%type;
     w_num_imovel_rede          supr_010.numero_imovel%type;
     w_ins_est_rede             supr_010.inscr_est_forne%type;
     w_cep_entr_cobr            pedi_150.cep_entr_cobr%type;

     w_cod_mensagem_nf_devol    obrf_180.cod_mensagem_nf_devol%type;
     w_cod_sequencia_nf_devol   obrf_180.cod_sequencia_nf_devol%type;
     w_ind_nf_devol             obrf_180.ind_nf_devol%type;
     w_cod_local_nf_devol       obrf_180.cod_local_nf_devol%type;
     w_notas_devolvidas         varchar2(4000);
     w_notas_devolvidas_rol     varchar2(4000);
     w_notas_devolvidas_rol_nota varchar2(4000);
     w_notas_entrada            varchar2(4000);


     w_cod_mensagem_nf_bcm    obrf_180.cod_mensagem_nf_bcm%type;
     w_cod_sequencia_nf_bcm   obrf_180.cod_sequencia_nf_bcm%type;
     w_cod_local_nf_bcm       obrf_180.cod_local_nf_bcm%type;
     w_notas_devolvidas_bcm   varchar2(4000);

     w_cod_mensagem_nf_dev    obrf_180.cod_mensagem_nf_dev%type;
     w_cod_sequencia_nf_dev   obrf_180.cod_sequencia_nf_dev%type;
     w_cod_local_nf_dev       obrf_180.cod_local_nf_dev%type;

     w_cod_mensagem_nf_devol_perdas  obrf_180.cod_mensagem_nf_devol%type;
     w_cod_seq_nf_devol_perdas       obrf_180.cod_sequencia_nf_devol%type;
     w_ind_nf_devol_perdas           obrf_180.ind_nf_devol%type;
     w_cod_local_nf_devol_perdas     obrf_180.cod_local_nf_devol%type;
     w_opcao_impres_nf_devoluc       fatu_502.opcao_impres_nf_devoluc%type;
     w_gera_nota_entreg              number(1);
     w_verifica_empresa              number(1);
     w_tipo_natureza                 number(1);
     w_perda_total                   number(15,3);
     w_qtde_perda                    number(15,3);

     w_1 number(15,2);
     w_2 number(15,2);
     w_3 number(15,2);
     w_4 number(15,2);
     w_5 number(15,2);
     w_6 number(15,2);

     w_cod_natureza                number(6);
     w_mensagem_natureza           varchar2(1000);
     w_mensagem_natureza_aux       varchar2(1000);
     w_cod_natureza_comercial         number(6);
     w_mensagem_natureza_co           varchar2(1000);
     w_mensagem_natureza_aux_co       varchar2(1000);

     w_ind_tabela_ex           number(1);
     w_ind_box_ex              number(1);
     w_ind_agenda_ex           number(1);
     w_ind_nr_pedido_ex        number(1);
     w_ind_mt_lineares_ex      number(1);
     w_ind_tot_pecas_ex        number(1);
     w_ind_tot_rolo_ex         number(1);
     w_ind_representante_ex    number(1);
     w_ind_comissao_ex         number(1);
     w_ind_perc_comissao_ex    number(1);
     w_ind_mt_cubicos_ex       number(1);
     w_ind_suframa_ex          number(1);
     w_ind_largura_ex          number(1);
     w_ind_gramatura_ex        number(1);
     w_ind_natureza_ex         number(1);
     w_ind_dev_fios_ex         number(1);
     w_ind_dev_tecidos_ex      number(1);
     w_ind_dev_quebra_ex       number(1);
     w_ind_nf_fatura_ex        number(1);
     w_ind_nf_entrega_ex       number(1);
     w_ind_msg_exportacao_ex   number(1);
     w_ind_msg_encargos_ex     number(1);
     w_ind_transp_redes_ex     number(1);
     w_ind_retorno_transf_ex   number(1);

     w_cod_mensagem_simples    obrf_180.cod_mensagem_simples%type;
     w_cod_sequencia_simples   obrf_180.cod_sequencia_simples%type;
     w_ind_simples             obrf_180.ind_simples%type;
     w_cod_local_simples       obrf_180.cod_local_simples%type;
     w_ind_empresa_simples     fatu_503.ind_empresa_simples%type;
     w_perc_icms               pedi_080.perc_icms%type;
     w_ind_desc_pis_cofins     pedi_010.ind_desc_pis_cofins_suframa%type;
     w_ind_desc_icms_ipi       pedi_010.ind_desc_icms_ipi%type;
     w_cod_cidade_cli          pedi_010.cod_cidade%type;
     w_suframa_cidade          basi_160.suframa%type;

     w_cod_mensagem_desc_piscofins  obrf_180.cod_mensagem_desc_piscofins%type;
     w_cod_sequencia_desc_piscofins obrf_180.cod_sequencia_desc_piscofins%type;
     w_ind_desc_piscofins_ex        obrf_180.ind_desc_piscofins%type;
     w_cod_local_desc_piscofins     obrf_180.cod_local_desc_piscofins%type;

     w_cod_mensagem_rem_simb        obrf_180.cod_mensagem_rem_simb%type;
     w_cod_sequencia_rem_simb       obrf_180.cod_sequencia_rem_simb%type;
     w_cod_local_rem_simb           obrf_180.cod_local_rem_simb%type;

     w_cod_mensagem_desc            obrf_180.cod_mensagem_desc%type;
     w_cod_sequencia_desc           obrf_180.cod_sequencia_desc%type;
     w_cod_local_desc               obrf_180.cod_local_desc%type;

     w_cod_mensagem_desc_esp        obrf_180.cod_mensagem_desc_esp%type;
     w_cod_sequencia_desc_esp       obrf_180.cod_sequencia_desc_esp%type;
     w_cod_local_desc_esp           obrf_180.cod_local_desc_esp%type;

     w_cod_mensagem_cont            obrf_180.cod_mensagem_cont%type;
     w_cod_sequencia_cont           obrf_180.cod_sequencia_cont%type;
     w_cod_local_cont               obrf_180.cod_local_cont%type;

     w_cod_mensagem_dev_rol         obrf_180.cod_mensagem_dev_rol%type;
     w_cod_sequencia_dev_rol        obrf_180.cod_sequencia_dev_rol%type;
     w_cod_local_dev_rol            obrf_180.cod_local_dev_rol%type;

     w_cod_mensagem_ped_cli         obrf_180.cod_mensagem_ped_cli%type;
     w_cod_sequencia_ped_cli        obrf_180.cod_sequencia_ped_cli%type;
     w_cod_local_ped_cli            obrf_180.cod_local_ped_cli%type;

     w_cod_mensagem_fardos          obrf_180.cod_mensagem_fardos%type;
     w_cod_sequencia_fardos         obrf_180.cod_sequencia_fardos%type;
     w_cod_local_fardos             obrf_180.cod_local_fardos%type;

     w_mensagem_ped_cli             varchar2(275);
     w_mensagem_ped_cli_aux         varchar2(275);
     w_mensagem_ped_cli_final       varchar2(1800);
     w_len_fim                      number(4);
     w_len_aux                      number(4);

     w_inscricao_cliente           pedi_010.insc_est_cliente%type;

     w_cod_mensagem_icms_pr        obrf_180.cod_mensagem_icms_pr%type;
     w_cod_sequencia_icms_pr       obrf_180.cod_sequencia_icms_pr%type;
     w_cod_local_icms_pr           obrf_180.cod_local_icms_pr%type;

     w_cod_mensagem_ipi_dev        obrf_180.cod_mensagem_ipi_dev%type;
     w_cod_sequencia_ipi_dev       obrf_180.cod_sequencia_ipi_dev%type;
     w_cod_local_ipi_dev           obrf_180.cod_local_ipi_dev%type;

     w_cod_mensagem_ipi_cont       obrf_180.cod_mensagem_ipi_cont%type;
     w_cod_sequencia_ipi_cont      obrf_180.cod_sequencia_ipi_cont%type;
     w_cod_local_ipi_cont          obrf_180.cod_local_ipi_cont%type;
     w_cd_msg_reemissao            obrf_180.cod_mensagem_reemissao%type;
     w_cd_seq_reemissao            obrf_180.cod_seq_reemissao%type;
     w_cd_loc_reemissao            obrf_180.cod_local_reemissao%type;

     w_cod_msg_nf_comp             obrf_180.cod_msg_nf_comp%type;
     w_seq_msg_nf_comp             obrf_180.seq_msg_nf_comp%type;
     w_loc_msg_nf_comp             obrf_180.loc_msg_nf_comp%type;

     w_cod_msg_nf_ajuste           obrf_180.cod_msg_nf_ajuste%type;
     w_seq_msg_nf_ajuste           obrf_180.seq_msg_nf_ajuste%type;
     w_loc_msg_nf_ajuste           obrf_180.loc_msg_nf_ajuste%type;

     w_cod_msg_inf_trib            obrf_180.cod_msg_inf_trib%type;
     w_seq_msg_inf_trib            obrf_180.seq_msg_inf_trib%type;
     w_loc_msg_inf_trib            obrf_180.loc_msg_inf_trib%type;

     w_cod_msg_consumo_final       obrf_180.cod_msg_consumo_final%type;
     w_seq_msg_consumo_final       obrf_180.seq_msg_consumo_final%type;
     w_loc_msg_consumo_final       obrf_180.loc_msg_consumo_final%type;

     w_mensagem_ipi                varchar2(300);
     w_zerou_ipi                   varchar2(1);

     w_mensagem_icms_pr            varchar2(300);

     w_perc_reducao_icm            pedi_080.perc_reducao_icm%type;
     w_tipo_reducao                pedi_080.tipo_reducao%type;
	 w_red_icms_base_pis_cof	   pedi_080.red_icms_base_pis_cof%type := 0;
	 
     w_valor_pis                   fatu_060.valor_pis%type;
     w_valor_cofins                fatu_060.valor_cofins%type;

     w_mensagem_pedido             varchar2(550);
     w_mensagem_fardo              varchar2(276);
   w_descr_redespacho         varchar2(100);
     ws_usuario_rede               varchar2(20);
     ws_maquina_rede               varchar2(40);
     ws_aplicativo                 varchar2(20);
     ws_sid                        number(9);
     ws_empresa                    number(3);
     ws_usuario_systextil          varchar2(20);
     ws_locale_usuario             varchar2(5);

     w_estado_empresa              basi_160.estado%type;

     w_len_msg                     number(4);
     w_teve_dev                    boolean;
     w_valor_merc_com_suframa      fatu_050.valor_itens_nfis%type;

     w_flag_fat_nota_serie         obrf_180.flag_fat_nota_serie%type;
     w_flag_fat_data_emis          obrf_180.flag_fat_data_emis%type;
     w_flag_fat_nome_cliente       obrf_180.flag_fat_nome_cliente%type;
     w_flag_fat_cnpj_cliente       obrf_180.flag_fat_cnpj_cliente%type;
     w_flag_fat_insc_estad         obrf_180.flag_fat_insc_estad%type;
     w_flag_fat_nome_cidade        obrf_180.flag_fat_nome_cidade%type;
     w_flag_fat_bairro             obrf_180.flag_fat_bairro%type;
     w_flag_fat_end_nr_compl       obrf_180.flag_fat_end_nr_compl%type;
     w_flag_fat_cep                obrf_180.flag_fat_cep%type;

     w_flag_ent_nota_serie         obrf_180.flag_ent_nota_serie%type;
     w_flag_ent_data_emis          obrf_180.flag_ent_data_emis%type;
     w_flag_ent_nome_cliente       obrf_180.flag_ent_nome_cliente%type;
     w_flag_ent_cnpj_cliente       obrf_180.flag_ent_cnpj_cliente%type;
     w_flag_ent_insc_estad         obrf_180.flag_ent_insc_estad%type;
     w_flag_ent_nome_cidade        obrf_180.flag_ent_nome_cidade%type;
     w_flag_ent_bairro             obrf_180.flag_ent_bairro%type;
     w_flag_ent_end_nr_compl       obrf_180.flag_ent_end_nr_compl%type;
     w_flag_ent_cep                obrf_180.flag_ent_cep%type;
     w_cod_msg_aj_ipi              obrf_180.cod_msg_aj_ipi%type;
     w_seq_msg_aj_ipi              obrf_180.seq_msg_aj_ipi%type;
     w_loc_msg_aj_ipi              obrf_180.loc_msg_aj_ipi%type;
     w_nf_rel_aj_ipi               varchar2(4000);
     w_mensagem_aj_ipi             varchar2(4000);

     w_valor_base_calc             number;
     w_valor_carga_trib            number;
     w_valor_base_calc_tot         number;
     w_valor_carga_trib_tot        number;
     w_aliquota_nacional           number;
     w_mensagem_inf_trib           varchar2(4000);
     w_retorno                     varchar2(4000);
     w_sequencia                   number;
	 
	 w_exclui_icms_pc			   varchar2(1);

     TYPE notasRef IS TABLE OF varchar2(15);
     notas_ref notasRef;

     notas_concat                  varchar2(1000);
     gravou_notas                  varchar2(1);
     w_cod_ped_cli_item            pedi_110.cod_ped_cliente%type;

     w_cod_msg_suppliercard        obrf_180.cod_mensagem_suppliercard%type;
     w_cod_seq_suppliercard        obrf_180.cod_sequencia_suppliercard%type;
     w_cod_local_suppliercard      obrf_180.cod_local_suppliercard%type;
     w_pedido_suppliercard         pedi_100.pedido_suppliercard%type;
     w_condicao_pagamento          pedi_100.cond_pgto_venda%type;
     w_mensagem_suppliercard       varchar2(1000);
     w_nr_parcelas                 number(3);

     w_cod_msg_cest                obrf_180.cod_mensagem_cest%type;
     w_cod_seq_cest                obrf_180.cod_sequencia_cest%type;
     w_cod_local_cest              obrf_180.cod_local_cest%type;
     w_mensagem_cest               varchar2(1000);
     w_msg_nr_cest                 varchar2(1000);
     w_msg_conta_cest              number;

     w_cod_msg_subtrib             obrf_180.cod_mensagem_subtrib%type;
     w_cod_seq_subtrib             obrf_180.cod_sequencia_subtrib%type;
     w_cod_local_subtrib           obrf_180.cod_local_subtrib%type;
     w_msg_subtrib                 varchar2(1000);
     w_ipiDevolucao                fatu_060.valor_ipi%type;
     w_cod_msg_fcp                 obrf_180.cod_msg_fcp%type;
     w_cod_seq_fcp                 obrf_180.cod_seq_fcp%type;
     w_cod_local_fcp               obrf_180.cod_local_fcp%type;

     w_cod_msg_orcamento                 obrf_180.cod_msg_orcamento%type;
     w_cod_seq_orcamento                 obrf_180.cod_seq_orcamento%type;
     w_cod_local_orcamento               obrf_180.cod_local_orcamento%type;

     w_cod_msg_cond_pgto                 obrf_180.cod_msg_cond_pgto%type;
     w_cod_seq_cond_pgto                 obrf_180.cod_seq_cond_pgto%type;
     w_cod_local_cond_pgto               obrf_180.cod_local_cond_pgto%type;

     w_cod_msg_celular                 obrf_180.cod_msg_celular%type;
     w_cod_seq_celular                 obrf_180.cod_seq_celular%type;
     w_cod_local_celular               obrf_180.cod_local_celular%type;

     w_cod_msg_class_pedido            obrf_180.cod_msg_class_pedido%type;
     w_cod_seq_class_pedido            obrf_180.cod_seq_class_pedido%type;
     w_cod_local_class_pedido          obrf_180.cod_local_class_pedido%type;
	 
	 w_cod_msg_exclui_icms_pc          obrf_180.cod_msg_exclui_icms_pc%type;
     w_seq_msg_exclui_icms_pc          obrf_180.seq_msg_exclui_icms_pc%type;
     w_loc_msg_exclui_icms_pc          obrf_180.loc_msg_exclui_icms_pc%type;
     w_mensagem_icms                   varchar2(1000);
     
     w_cod_mensagem_tab_compra         obrf_180.cod_mensagem_tab_compra%type;
     w_cod_sequencia_tab_compra        obrf_180.cod_sequencia_tab_compra%type;
     w_cod_local_tab_compra            obrf_180.cod_local_tab_compra%type;
     w_num_ordem_serv                  fatu_060.num_ordem_serv%type;

     w_pedido_compra                   supr_090.pedido_compra%type;
     
   begin

      if :new.situacao_nfisc <> 2
      then
         begin
           select fatu_500.perc_suframa
             into w_perc_suframa
           from fatu_500
           where fatu_500.codigo_empresa = :new.codigo_empresa;

           exception
           when no_data_found then
             w_perc_suframa := 0;
         end;

         begin
         select fatu_502.perc_pis_suframa,       fatu_502.perc_cofins_suframa,
               fatu_502.opcao_impres_nf_devoluc
          into w_perc_pis_suframa,              w_perc_cofins_suframa,
               w_opcao_impres_nf_devoluc
         from fatu_502
         where fatu_502.codigo_empresa = :new.codigo_empresa;

         exception
         when no_data_found then
          w_perc_pis_suframa        := 0;
          w_perc_cofins_suframa     := 0;
          w_opcao_impres_nf_devoluc := 0;
         end;

         -- Verifica se eh nota da Florisa
         w_verifica_empresa := 0;

         begin
           select 1
             into w_verifica_empresa
           from estq_360
           where estq_360.empresa = :new.codigo_empresa
             and rownum < 2;

         exception
           when no_data_found then
             w_verifica_empresa := 0;
         end;

         begin
           select fatu_503.ind_empresa_simples
             into w_ind_empresa_simples
           from fatu_503
           where fatu_503.codigo_empresa = :new.codigo_empresa;

         exception
           when no_data_found then
             w_ind_empresa_simples    := '3';
         end;

         begin
           select trim(nr_suframa_cli),         pedi_010.ind_desc_pis_cofins_suframa,
                  pedi_010.ind_desc_icms_ipi,   pedi_010.cod_cidade
             into w_nr_suframa_cli,             w_ind_desc_pis_cofins,
                  w_ind_desc_icms_ipi,          w_cod_cidade_cli
           from pedi_010
           where pedi_010.cgc_9 = :new.cgc_9
             and pedi_010.cgc_4 = :new.cgc_4
             and pedi_010.cgc_2 = :new.cgc_2;
         exception
           when no_data_found then
             w_nr_suframa_cli      := ' ';
             w_ind_desc_pis_cofins := 'S';
             w_ind_desc_icms_ipi   := 'S';
             w_cod_cidade_cli      := 0;
         end;

         begin
            select basi_160.suframa into w_suframa_cidade
            from basi_160
            where basi_160.cod_cidade = w_cod_cidade_cli;
         exception
           when no_data_found then
             w_suframa_cidade := 1;
         end;

         begin
            select pedi_080.perc_icms,   pedi_080.tipo_natureza, 	pedi_080.red_icms_base_pis_cof
            into w_perc_icms,            w_tipo_natureza,			w_red_icms_base_pis_cof
            from pedi_080
            where pedi_080.natur_operacao = :new.natop_nf_nat_oper
              and pedi_080.estado_natoper = :new.natop_nf_est_oper;
         exception
           when no_data_found then
             w_perc_icms     := 0.00;
             w_tipo_natureza := 0;
			 w_red_icms_base_pis_cof := 0;
         end;

         select fatu_500.e_mail
           into w_flag_possui_rolo
         from fatu_500
         where fatu_500.codigo_empresa = :new.codigo_empresa;
         
         for reg_fatu060_msg in fatu060_msg
         loop
           for f_v_box in v_box (reg_fatu060_msg.pedido_venda)
           loop

             w_vl_msg_box := w_vl_msg_box || to_char(f_v_box.corredor,'000') || '/' ||
                                             to_char(f_v_box.box,'0000') || '-' ||
                                             to_char(f_v_box.qtde_rolos,'0000') || ' ';
           end loop;
         end loop;

         begin
           select cod_mensagem_tabela,
                  cod_mensagem_box,
                  cod_mensagem_nrpedido,
                  cod_mensagem_tot_pecas,
                  cod_mensagem_tot_rolo,
                  cod_mensagem_represen,
                  cod_mensagem_perc_comis,
                  cod_mensagem_gm2,
                  cod_mensagem_largura,
                  cod_mensagem_mt_cubicos,
                  ind_descontos,
                  cod_sequencia_tabela,
                  cod_sequencia_box,
                  cod_sequencia_nrpedido,
                  cod_sequencia_tot_pecas,
                  cod_sequencia_tot_rolo,
                  cod_sequencia_represen,
                  cod_sequencia_perc_comis,
                  cod_sequencia_gm2,
                  cod_sequencia_mt_cubicos,
                  cod_mensagem_largura,
                  cod_LOCAL_tabela ,
                  cod_LOCAL_box,
                  cod_LOCAL_nrpedido,
                  cod_LOCAL_tot_pecas,
                  cod_LOCAL_tot_rolo,
                  cod_local_represen,
                  cod_local_perc_comis,
                  cod_local_gm2,
                  cod_local_largura,
                  cod_local_mt_cubicos,
                  cod_sequencia_descontos,
                  cod_sequencia_natureza,
                  cod_sequencia_encargos,
                  cod_local_encargos,
                  cod_mensagem_encargos,
                  cod_sequencia_suframa,
                  cod_local_suframa,
                  cod_mensagem_suframa,
                  cod_mensagem_simples_rem,
                  cod_sequencia_simples_rem,
                  cod_local_simples_rem,
                  cod_mensagem_entrega,
                  cod_sequencia_entrega,
                  cod_local_entrega,
                  cod_mensagem_redes,
                  cod_sequencia_redes,
                  cod_local_redes,
                  ind_tabela_ex,
                  ind_box_ex,
                  ind_agenda_ex,
                  ind_nr_pedido_ex,
                  ind_mt_lineares_ex,
                  ind_tot_pecas_ex,
                  ind_tot_rolo_ex,
                  ind_representante_ex,
                  ind_comissao_ex,
                  ind_perc_comissao_ex,
                  ind_mt_cubicos_ex,
                  ind_suframa_ex,
                  ind_largura_ex,
                  ind_gramatura_ex,
                  ind_natureza_ex,
                  ind_dev_fios_ex,
                  ind_dev_tecidos_ex,
                  ind_dev_quebra_ex,
                  ind_nf_fatura_ex,
                  ind_nf_entrega_ex,
                  ind_msg_exportacao_ex,
                  ind_msg_encargos_ex,
                  ind_transp_redes_ex,
                  ind_retorno_transf_ex,
                  cod_mensagem_simples,
                  cod_sequencia_simples,
                  ind_simples,
                  cod_local_simples,
                  cod_mensagem_desc_piscofins,
                  cod_sequencia_desc_piscofins,
                  ind_desc_piscofins,
                  cod_local_desc_piscofins,
                  cod_mensagem_nf_devol,
                  cod_sequencia_nf_devol,
                  ind_nf_devol,
                  cod_local_nf_devol,
                  cod_mensagem_nf_devol_perdas,
                  cod_sequencia_nf_devol_perdas,
                  ind_nf_devol_perdas,
                  cod_local_nf_devol_perdas,
                  cod_mensagem_rem_simb,
                  cod_sequencia_rem_simb,
                  cod_local_rem_simb,
                  cod_mensagem_nf_bcm,
                  cod_sequencia_nf_bcm,
                  cod_local_nf_bcm,
                  cod_mensagem_desc,
                  cod_sequencia_desc,
                  cod_local_desc,
                  cod_mensagem_desc_esp,
                  cod_sequencia_desc_esp,
                  cod_local_desc_esp,
                  cod_mensagem_cont,
                  cod_sequencia_cont,
                  cod_local_cont,
                  cod_mensagem_dev_rol,
                  cod_sequencia_dev_rol,
                  cod_local_dev_rol,
                  cod_mensagem_ped_cli,
                  cod_sequencia_ped_cli,
                  cod_local_ped_cli,
                  cod_mensagem_icms_pr,
                  cod_sequencia_icms_pr,
                  cod_local_icms_pr,
                  cod_mensagem_fardos,
                  cod_sequencia_fardos,
                  cod_local_fardos,
                  cod_mensagem_ipi_dev,
                  cod_sequencia_ipi_dev,
                  cod_local_ipi_dev,
                  cod_mensagem_ipi_cont,
                  cod_sequencia_ipi_cont,
                  cod_local_ipi_cont,
                  cod_mensagem_nf_dev,
                  cod_sequencia_nf_dev,
                  cod_local_nf_dev,
                  cod_mensagem_reemissao,
                  cod_seq_reemissao,
                  cod_local_reemissao,
                  flag_fat_nota_serie,
                  flag_fat_data_emis,
                  flag_fat_nome_cliente,
                  flag_fat_cnpj_cliente,
                  flag_fat_insc_estad,
                  flag_fat_nome_cidade,
                  flag_fat_bairro,
                  flag_fat_end_nr_compl,
                  flag_fat_cep,
                  flag_ent_nota_serie,
                  flag_ent_data_emis,
                  flag_ent_nome_cliente,
                  flag_ent_cnpj_cliente,
                  flag_ent_insc_estad,
                  flag_ent_nome_cidade,
                  flag_ent_bairro,
                  flag_ent_end_nr_compl,
                  flag_ent_cep,
                  cod_msg_aj_ipi,
                  seq_msg_aj_ipi,
                  loc_msg_aj_ipi,
                  cod_msg_nf_comp,
                  seq_msg_nf_comp,
                  loc_msg_nf_comp,
                  cod_msg_nf_ajuste,
                  seq_msg_nf_ajuste,
                  loc_msg_nf_ajuste,
                  cod_msg_inf_trib,
                  seq_msg_inf_trib,
                  loc_msg_inf_trib,
                  cod_msg_consumo_final,
                  seq_msg_consumo_final,
                  loc_msg_consumo_final,
                  cod_mensagem_suppliercard,
                  cod_sequencia_suppliercard,
                  cod_local_suppliercard,
                  cod_mensagem_cest,
                  cod_sequencia_cest,
                  cod_local_cest,
                  cod_mensagem_subtrib,
                  cod_sequencia_subtrib,
                  cod_local_subtrib,
                  cod_msg_fcp,
                  cod_seq_fcp,
                  cod_local_fcp,
                  cod_msg_orcamento,
                  cod_seq_orcamento,
                  cod_local_orcamento,
                  cod_msg_cond_pgto,
                  cod_seq_cond_pgto,
                  cod_local_cond_pgto,
                  cod_msg_celular,
                  cod_seq_celular,
                  cod_local_celular,
                  cod_msg_class_pedido,
                  cod_seq_class_pedido,
                  cod_local_class_pedido,
				  cod_msg_exclui_icms_pc,
				  seq_msg_exclui_icms_pc,
				  loc_msg_exclui_icms_pc,
                                  imp_pedido_forca_venda,
                  cod_mensagem_tab_compra,
                  cod_sequencia_tab_compra,
                  cod_local_tab_compra
            into  w_cod_msg_tabela,
                  w_cod_msg_box,
                  w_cod_msg_nrpedido,
                  w_cod_msg_tot_pecas,
                  w_cod_msg_tot_rolo,
                  w_cod_msg_represen,
                  w_cod_msg_perc_comis,
                  w_cod_msg_gm2,
                  w_cod_msg_largura,
                  w_cod_msg_mt_cubicos,
                  w_ind_descontos,
                  w_cod_sequencia_tabela,
                  w_cod_sequencia_box,
                  w_cod_sequencia_nrpedido,
                  w_cod_sequencia_tot_pecas,
                  w_cod_sequencia_tot_rolo,
                  w_cod_sequencia_represen,
                  w_cod_sequencia_perc_comis,
                  w_cod_sequencia_gm2,
                  w_cod_sequencia_mt_cubicos,
                  w_cod_msg_largura,
                  w_cod_local_tabela,
                  w_cod_local_box,
                  w_cod_local_nrpedido,
                  w_cod_local_tot_pecas,
                  w_cod_local_tot_rolo,
                  w_cod_local_represen,
                  w_cod_local_perc_comis,
                  w_cod_local_gm2,
                  w_cod_local_largura,
                  w_cod_local_mt_cubicos,
                  w_cod_sequencia_descontos,
                  w_cod_sequencia_natureza,
                  w_cod_sequencia_encargos,
                  w_cod_local_encargos,
                  w_cod_msg_encargos,
                  w_cod_sequencia_suframa,
                  w_cod_local_suframa,
                  w_cod_msg_suframa,
                  w_cod_msg_simples_rem,
                  w_cod_sequencia_simples_rem,
                  w_cod_local_simples_rem,
                  w_cod_msg_entrega,
                  w_cod_sequencia_entrega,
                  w_cod_local_entrega,
                  w_cod_msg_redespacho,
                  w_cod_sequencia_redespacho,
                  w_cod_local_redespacho,
                  w_ind_tabela_ex,
                  w_ind_box_ex,
                  w_ind_agenda_ex,
                  w_ind_nr_pedido_ex,
                  w_ind_mt_lineares_ex,
                  w_ind_tot_pecas_ex,
                  w_ind_tot_rolo_ex,
                  w_ind_representante_ex,
                  w_ind_comissao_ex,
                  w_ind_perc_comissao_ex,
                  w_ind_mt_cubicos_ex,
                  w_ind_suframa_ex,
                  w_ind_largura_ex,
                  w_ind_gramatura_ex,
                  w_ind_natureza_ex,
                  w_ind_dev_fios_ex,
                  w_ind_dev_tecidos_ex,
                  w_ind_dev_quebra_ex,
                  w_ind_nf_fatura_ex,
                  w_ind_nf_entrega_ex,
                  w_ind_msg_exportacao_ex,
                  w_ind_msg_encargos_ex,
                  w_ind_transp_redes_ex,
                  w_ind_retorno_transf_ex,
                  w_cod_mensagem_simples,
                  w_cod_sequencia_simples,
                  w_ind_simples,
                  w_cod_local_simples,
                  w_cod_mensagem_desc_piscofins,
                  w_cod_sequencia_desc_piscofins,
                  w_ind_desc_piscofins_ex,
                  w_cod_local_desc_piscofins,
                  w_cod_mensagem_nf_devol,
                  w_cod_sequencia_nf_devol,
                  w_ind_nf_devol,
                  w_cod_local_nf_devol,
                  w_cod_mensagem_nf_devol_perdas,
                  w_cod_seq_nf_devol_perdas,
                  w_ind_nf_devol_perdas,
                  w_cod_local_nf_devol_perdas,
                  w_cod_mensagem_rem_simb,
                  w_cod_sequencia_rem_simb,
                  w_cod_local_rem_simb,
                  w_cod_mensagem_nf_bcm,
                  w_cod_sequencia_nf_bcm,
                  w_cod_local_nf_bcm,
                  w_cod_mensagem_desc,
                  w_cod_sequencia_desc,
                  w_cod_local_desc,
                  w_cod_mensagem_desc_esp,
                  w_cod_sequencia_desc_esp,
                  w_cod_local_desc_esp,
                  w_cod_mensagem_cont,
                  w_cod_sequencia_cont,
                  w_cod_local_cont,
                  w_cod_mensagem_dev_rol,
                  w_cod_sequencia_dev_rol,
                  w_cod_local_dev_rol,
                  w_cod_mensagem_ped_cli,
                  w_cod_sequencia_ped_cli,
                  w_cod_local_ped_cli,
                  w_cod_mensagem_icms_pr,
                  w_cod_sequencia_icms_pr,
                  w_cod_local_icms_pr,
                  w_cod_mensagem_fardos,
                  w_cod_sequencia_fardos,
                  w_cod_local_fardos,
                  w_cod_mensagem_ipi_dev,
                  w_cod_sequencia_ipi_dev,
                  w_cod_local_ipi_dev,
                  w_cod_mensagem_ipi_cont,
                  w_cod_sequencia_ipi_cont,
                  w_cod_local_ipi_cont,
                  w_cod_mensagem_nf_dev,
                  w_cod_sequencia_nf_dev,
                  w_cod_local_nf_dev,
                  w_cd_msg_reemissao,
                  w_cd_seq_reemissao,
                  w_cd_loc_reemissao,
                  w_flag_fat_nota_serie,
                  w_flag_fat_data_emis,
                  w_flag_fat_nome_cliente,
                  w_flag_fat_cnpj_cliente,
                  w_flag_fat_insc_estad,
                  w_flag_fat_nome_cidade,
                  w_flag_fat_bairro,
                  w_flag_fat_end_nr_compl,
                  w_flag_fat_cep,
                  w_flag_ent_nota_serie,
                  w_flag_ent_data_emis,
                  w_flag_ent_nome_cliente,
                  w_flag_ent_cnpj_cliente,
                  w_flag_ent_insc_estad,
                  w_flag_ent_nome_cidade,
                  w_flag_ent_bairro,
                  w_flag_ent_end_nr_compl,
                  w_flag_ent_cep,
                  w_cod_msg_aj_ipi,
                  w_seq_msg_aj_ipi,
                  w_loc_msg_aj_ipi,
                  w_cod_msg_nf_comp,
                  w_seq_msg_nf_comp,
                  w_loc_msg_nf_comp,
                  w_cod_msg_nf_ajuste,
                  w_seq_msg_nf_ajuste,
                  w_loc_msg_nf_ajuste,
                  w_cod_msg_inf_trib,
                  w_seq_msg_inf_trib,
                  w_loc_msg_inf_trib,
                  w_cod_msg_consumo_final,
                  w_seq_msg_consumo_final,
                  w_loc_msg_consumo_final,
                  w_cod_msg_suppliercard,
                  w_cod_seq_suppliercard,
                  w_cod_local_suppliercard,
                  w_cod_msg_cest,
                  w_cod_seq_cest,
                  w_cod_local_cest,
                  w_cod_msg_subtrib,
                  w_cod_seq_subtrib,
                  w_cod_local_subtrib,
                  w_cod_msg_fcp,
                  w_cod_seq_fcp,
                  w_cod_local_fcp,
                  w_cod_msg_orcamento,
                  w_cod_seq_orcamento,
                  w_cod_local_orcamento,
                  w_cod_msg_cond_pgto,
                  w_cod_seq_cond_pgto,
                  w_cod_local_cond_pgto,
                  w_cod_msg_celular,
                  w_cod_seq_celular,
                  w_cod_local_celular,
                  w_cod_msg_class_pedido,
                  w_cod_seq_class_pedido,
                  w_cod_local_class_pedido,
				  w_cod_msg_exclui_icms_pc,
				  w_seq_msg_exclui_icms_pc,
				  w_loc_msg_exclui_icms_pc,
                                  w_imp_pedido_forca_venda,
                  w_cod_mensagem_tab_compra,
                  w_cod_sequencia_tab_compra,
                  w_cod_local_tab_compra
           from obrf_180
           where obrf_180.cod_empresa = :new.codigo_empresa;
         exception
           when no_data_found then
            w_cod_msg_mt_cubicos       := 0;
            w_cod_sequencia_mt_cubicos := 0;
            w_cod_local_mt_cubicos     := 0;
            w_ind_mt_cubicos_ex        := 0;

            w_cod_msg_tabela         := 0;
            w_cod_msg_box            := 0;
            w_cod_msg_nrpedido       := 0;
            w_cod_sequencia_tabela   := 0;
            w_cod_sequencia_box      := 0;
            w_cod_sequencia_nrpedido := 0;

            w_cod_mensagem_icms_pr   := 0;
            w_cod_sequencia_icms_pr  := 0;
            w_cod_local_icms_pr      := 'C';

            w_cod_mensagem_ipi_dev   := 0;
            w_cod_sequencia_ipi_dev  := 0;
            w_cod_local_ipi_dev      := 'D';

            w_cod_mensagem_ipi_cont  := 0;
            w_cod_sequencia_ipi_cont := 0;
            w_cod_local_ipi_cont     := 'D';

            w_cod_msg_nf_comp        := 0;
            w_seq_msg_nf_comp        := 0;
            w_loc_msg_nf_comp        := 'D';

            w_cod_msg_nf_ajuste      := 0;
            w_seq_msg_nf_ajuste      := 0;
            w_loc_msg_nf_ajuste      := 'D';

            w_cod_msg_inf_trib       := 0;
            w_seq_msg_inf_trib       := 0;
            w_loc_msg_inf_trib       := 'D';
			
			w_cod_msg_exclui_icms_pc := 0;
			w_seq_msg_exclui_icms_pc := 0;
			w_loc_msg_exclui_icms_pc := 'D';
      
            w_cod_mensagem_tab_compra := 0;
            w_cod_sequencia_tab_compra := 0;
            w_cod_local_tab_compra := 'D';

            w_flag_fat_nota_serie    := 1;
            w_flag_fat_data_emis     := 1;
            w_flag_fat_nome_cliente  := 1;
            w_flag_fat_cnpj_cliente  := 1;
            w_flag_fat_insc_estad    := 1;
            w_flag_fat_nome_cidade   := 1;
            w_flag_fat_bairro        := 1;
            w_flag_fat_end_nr_compl  := 1;
            w_flag_fat_cep           := 1;
            w_flag_ent_nota_serie    := 1;
            w_flag_ent_data_emis     := 1;
            w_flag_ent_nome_cliente  := 1;
            w_flag_ent_cnpj_cliente  := 1;
            w_flag_ent_insc_estad    := 1;
            w_flag_ent_nome_cidade   := 1;
            w_flag_ent_bairro        := 1;
            w_flag_ent_end_nr_compl  := 1;
            w_flag_ent_cep           := 1;

            w_cod_msg_suppliercard   := 0;
            w_cod_seq_suppliercard   := 0;
            w_cod_local_suppliercard := 0;

            w_cod_msg_cest           := 0;
            w_cod_seq_cest           := 0;
            w_cod_local_cest         := 0;

            -- vai ter mais mensagens
         end;
         
         
         --Mensagem referente ao pedido de compra
         
         if w_cod_mensagem_tab_compra > 0
         then
           begin
             for itemNota in (select distinct fatu_060.num_ordem_serv
               from fatu_060
               where fatu_060.ch_it_nf_cd_empr  = :new.codigo_empresa
                 and fatu_060.ch_it_nf_num_nfis = :new.num_nota_fiscal
                 and fatu_060.ch_it_nf_ser_nfis = :new.serie_nota_fisc
                 and fatu_060.num_ordem_serv    > 0
                 and rownum = 1)
             loop
               
            --   raise_application_error(-20000,'w_num_ordem_serv: ' || itemNota.num_ordem_serv);
             
               begin
                 select supr_090.pedido_compra
                 into w_pedido_compra
                 from supr_090
                 where supr_090.ordem_servico = itemNota.num_ordem_serv
                   and rownum = 1;
               exception
                 when no_data_found then w_pedido_compra := 0;
               end;
               
               if w_pedido_compra > 0
               then
                 begin
                   insert into fatu_052
                   (cod_empresa,
                   num_nota,
                   cod_serie_nota,
                   cod_mensagem,
                   cnpj9,
                   cnpj4,
                   cnpj2,
                   ind_entr_saida,
                   seq_mensagem,
                   ind_local,
                   des_mensag_1,
                   des_mensag_2,
                   des_mensag_3,
                   des_mensag_4,
                   des_mensag_5,
                   des_mensag_6,
                   des_mensag_7,
                   des_mensag_8,
                   des_mensag_9,
                   des_mensag_10)
                   (select :new.codigo_empresa,
                         :new.num_nota_fiscal,
                         :new.serie_nota_fisc,
                         o.cod_mensagem,
                         :new.cgc_9,
                         :new.cgc_4,
                         :new.cgc_2,
                         'S',
                         w_cod_sequencia_tab_compra,
                         w_cod_local_tab_compra,
                         o.des_mensagem1 || ': ' || w_pedido_compra,
                         o.des_mensagem2,
                         o.des_mensagem3,
                         o.des_mensagem4,
                         o.des_mensagem5,
                         o.des_mensagem6,
                         o.des_mensagem7,
                         o.des_mensagem8,
                         o.des_mensagem9,
                         o.des_mensagem10
                   from obrf_874 o
                   where o.cod_mensagem = w_cod_mensagem_tab_compra);
                 exception
                  WHEN dup_val_on_index THEN
                     NULL;
                  when others then
                     raise_application_error(-20000,'Erro ao inserir mensagem do pedido de compra' || sqlerrm);
                 end;
               end if;
             end loop;
           end;
         end if;
		 
		 --Mensagem referente Ã  exclusÃ£o do ICMS do PC
		 
		 w_exclui_icms_pc := INTER_FN_GET_PARAM_STRING(:new.codigo_empresa, 'fiscal.redusIcmsBasePisCofins');
		 
		 if w_cod_msg_exclui_icms_pc > 0 and w_exclui_icms_pc = 'S' and w_red_icms_base_pis_cof > 0
		 then
			
			begin
                        select distinct nvl(sum(fatu_060.valor_pis),0), nvl(sum(fatu_060.valor_cofins),0)
                        into w_valor_pis, w_valor_cofins
                        from fatu_060
                        where fatu_060.ch_it_nf_cd_empr  = :new.codigo_empresa
                        and   fatu_060.ch_it_nf_num_nfis = :new.num_nota_fiscal
                        and   fatu_060.ch_it_nf_ser_nfis = :new.serie_nota_fisc
                        order by nvl(sum(fatu_060.valor_pis),0), nvl(sum(fatu_060.valor_cofins),0);
                      exception
                  when no_data_found then
                  w_valor_pis := 0;
                  w_valor_cofins := 0;
            end;

            select des_mensagem1,
                   des_mensagem2,
                   des_mensagem3,
                   des_mensagem4,
                   des_mensagem5
            into v_des_mensagem1,
                   v_des_mensagem2,
                   v_des_mensagem3,
                   v_des_mensagem4,
                   v_des_mensagem5
            from obrf_874 o
            where o.cod_mensagem = w_cod_msg_exclui_icms_pc;
            
            if (length(trim(v_des_mensagem1))) > 0
            then
                w_mensagem_icms := v_des_mensagem1;
            end if;

            if (length(trim(v_des_mensagem2))) > 0
            then
                w_mensagem_icms := w_mensagem_icms || v_des_mensagem2;
            end if;

            if (length(trim(v_des_mensagem3))) > 0
            then
                w_mensagem_icms := w_mensagem_icms || v_des_mensagem3;
            end if;

            if (length(trim(v_des_mensagem4))) > 0
            then
                w_mensagem_icms := w_mensagem_icms || v_des_mensagem4;
            end if;

            if (length(trim(v_des_mensagem5))) > 0
            then
                w_mensagem_icms := w_mensagem_icms || v_des_mensagem5;
            end if;

            if w_valor_pis > 0 or w_valor_cofins > 0
            then
                    w_mensagem_icms := w_mensagem_icms ||chr(10)|| 'Valor do PIS: ' || w_valor_pis;
                
                if w_valor_cofins > 0
                then 
                    w_mensagem_icms := w_mensagem_icms || ' Valor do COFINS: ' || w_valor_cofins;
                end if;
            else
                if w_valor_cofins > 0
                then 
                    w_mensagem_icms := w_mensagem_icms ||chr(10)|| 'Valor do COFINS: ' || w_valor_cofins;
                end if;
            end if;
			
      	    if w_valor_pis > 0 or w_valor_cofins > 0
            then
               begin
                  insert into fatu_052
                  (cod_empresa,
                   num_nota,
                   cod_serie_nota,
                   cod_mensagem,
                   cnpj9,
                   cnpj4,
                   cnpj2,
                   ind_entr_saida,
                   seq_mensagem,
                   ind_local,
                   des_mensag_1,
                   des_mensag_2,
                   des_mensag_3,
                   des_mensag_4,
                   des_mensag_5)
                  (select :new.codigo_empresa,
                          :new.num_nota_fiscal,
                          :new.serie_nota_fisc,
                          o.cod_mensagem,
                          :new.cgc_9,
                          :new.cgc_4,
                          :new.cgc_2,
                          'S',
                          w_seq_msg_exclui_icms_pc,
                          w_loc_msg_exclui_icms_pc,
                          substr(w_mensagem_icms,1,55),
                          substr(w_mensagem_icms,56,55),
                          substr(w_mensagem_icms,111,55),
                          substr(w_mensagem_icms,166,55),
                          substr(w_mensagem_icms,221,55)
                   from obrf_874 o
                   where o.cod_mensagem = w_cod_msg_exclui_icms_pc);
               exception
                  WHEN dup_val_on_index THEN
                     update fatu_052
                        set des_mensag_1 = substr(w_mensagem_icms,1,55),
                        des_mensag_2 = substr(w_mensagem_icms,56,55),
                        des_mensag_3 = substr(w_mensagem_icms,111,55),
                        des_mensag_4 = substr(w_mensagem_icms,166,55),
                        des_mensag_5 = substr(w_mensagem_icms,221,55)
                     where cod_empresa = :new.codigo_empresa
                       and num_nota = :new.num_nota_fiscal
                       and cod_serie_nota = :new.serie_nota_fisc
                       and cod_mensagem =  w_cod_msg_exclui_icms_pc
                       and cnpj9 = :new.cgc_9
                       and cnpj4 = :new.cgc_4
                       and cnpj2 = :new.cgc_2;
                  when others then
                     raise_application_error(-20000,'Erro ao inserir mensagem da exclusÃ£o do ICMS do PIS/COFINS' || sqlerrm);
               end;
            end if;
	end if;

         --Mensagem referente aos Fardos (Processo de Chekout Loja)

         if inserting and :new.nr_solicitacao > 0 and w_cod_mensagem_fardos > 0
         then
            w_mensagem_fardo := ' ';
            for loja008 in (select loja_008.nr_fardo, rownum
                            from loja_008
                            where loja_008.pedido_loja = :new.nr_solicitacao)
            LOOP
              if loja008.rownum = 1
              then
                 w_mensagem_fardo := loja008.nr_fardo;
              else
                 w_mensagem_fardo := w_mensagem_fardo||', '||loja008.nr_fardo;
              end if;
            END LOOP;

            if trim(w_mensagem_fardo) is not null
            then
               begin
                  insert into fatu_052
                  (cod_empresa,
                   num_nota,
                   cod_serie_nota,
                   cod_mensagem,
                   cnpj9,
                   cnpj4,
                   cnpj2,
                   ind_entr_saida,
                   seq_mensagem,
                   ind_local,
                   des_mensag_1,
                   des_mensag_2,
                   des_mensag_3,
                   des_mensag_4,
                   des_mensag_5)
                  (select :new.codigo_empresa,
                          :new.num_nota_fiscal,
                          :new.serie_nota_fisc,
                          o.cod_mensagem,
                          :new.cgc_9,
                          :new.cgc_4,
                          :new.cgc_2,
                          'S',
                          w_cod_sequencia_fardos,
                          w_cod_local_fardos,
                          substr(o.des_mensagem1 ||' ' || w_mensagem_fardo,1,55),
                          substr(o.des_mensagem1 ||' ' || w_mensagem_fardo,56,55),
                          substr(o.des_mensagem1 ||' ' || w_mensagem_fardo,111,55),
                          substr(o.des_mensagem1 ||' ' || w_mensagem_fardo,166,55),
                          substr(o.des_mensagem1 ||' ' || w_mensagem_fardo,221,55)
                   from obrf_874 o
                   where o.cod_mensagem = w_cod_mensagem_fardos);
               exception
                  WHEN dup_val_on_index THEN
                     NULL;
                  when others then
                     raise_application_error(-20000,'Erro ao inserir mensagem dos fardos' || sqlerrm);
               end;
            end if;
         end if;

         --Mensagem referente ao NR.Pedido

         if ( (:new.natop_nf_est_oper <> 'EX' or w_ind_nr_pedido_ex <> 1) and w_cod_msg_nrpedido > 0 )
         then
            w_mensagem_pedido := ' ';

            for fatu060 in (select distinct fatu_060.pedido_venda
                            from fatu_060
                            where fatu_060.ch_it_nf_cd_empr  = :new.codigo_empresa
                              and fatu_060.ch_it_nf_num_nfis = :new.num_nota_fiscal
                              and fatu_060.ch_it_nf_ser_nfis = :new.serie_nota_fisc
                            order by fatu_060.pedido_venda)
            loop
                begin
                  select decode(count(*),0,null,1)
                  into w_cod_ped_cli_item
                  from pedi_110
                  where pedi_110.pedido_venda  = fatu060.pedido_venda
                  and trim(pedi_110.cod_ped_cliente) is not null;
               exception
                  when no_data_found then
                  w_cod_ped_cli_item := null;
               end;

               if trim(w_cod_ped_cli_item) is not null
               then
                  for pedi110 in  (select distinct (trim(substr(pedi_110.cod_ped_cliente, 1, 15))) as cod_ped_cliente
                                   from pedi_110, fatu_060
                                   where pedi_110.pedido_venda      = fatu060.pedido_venda
                                     and pedi_110.pedido_venda      = fatu_060.pedido_venda
                                     and pedi_110.seq_item_pedido   = fatu_060.seq_item_pedido
                                     and fatu_060.ch_it_nf_cd_empr  = :new.codigo_empresa
                                     and fatu_060.ch_it_nf_num_nfis = :new.num_nota_fiscal
                                     and fatu_060.ch_it_nf_ser_nfis = :new.serie_nota_fisc
                                    order by (trim(substr(pedi_110.cod_ped_cliente, 1, 15))))
                  loop
                     if trim(pedi110.cod_ped_cliente) is null
                     then
                        w_pedido_cliente := to_char(fatu060.pedido_venda);
                     else
                        w_pedido_cliente := trim(to_char(fatu060.pedido_venda)||' CLIENTE: '||pedi110.cod_ped_cliente);
                     end if;

                     if trim(w_mensagem_pedido) is null
                     then
                        w_mensagem_pedido := w_pedido_cliente;
                     else
                        w_mensagem_pedido := substr(w_mensagem_pedido || ' / ' || w_pedido_cliente,1,550);
                     end if;
                  end loop;
               else
                  begin
                     select pedi_100.cod_ped_cliente, pedi_100.id_pedido_forca_vendas 
                     into w_pedido_cliente, w_id_pedido_forca_vendas 
                     from pedi_100
                     where pedi_100.pedido_venda = fatu060.pedido_venda;
                  exception
                   when no_data_found then
                   w_pedido_cliente := ' ';
                   w_pedido_cliente := ' ';
                  end;

                  if trim(w_id_pedido_forca_vendas) is null
                  then w_id_pedido_forca_vendas := 0;
                  end if;

                  if trim(w_pedido_cliente) is null
                   then 
                    if (length(trim(w_id_pedido_forca_vendas)) > 0 and w_imp_pedido_forca_venda = 1)
                        then w_pedido_cliente := to_char(fatu060.pedido_venda)|| ' PEDIDO FORCA DE VENDAS: '||w_id_pedido_forca_vendas; 
                    else w_pedido_cliente := to_char(fatu060.pedido_venda);
                    end if;
                  else 
                    if (length(trim(w_id_pedido_forca_vendas)) > 0 and w_imp_pedido_forca_venda = 1)
                        then w_pedido_cliente := trim(to_char(fatu060.pedido_venda)||' CLIENTE: '||w_pedido_cliente|| ' PEDIDO  FORCA DE VENDAS: '||w_id_pedido_forca_vendas); 
                    else w_pedido_cliente := trim(to_char(fatu060.pedido_venda)||' CLIENTE: '||w_pedido_cliente);
                    end if;
                  end if;

                  if trim(w_mensagem_pedido) is null
                  then w_mensagem_pedido := w_pedido_cliente;
                  else w_mensagem_pedido := substr(w_mensagem_pedido || ' / ' || w_pedido_cliente,1,550);
                  end if;
               end if;
            end loop;

            if trim(w_mensagem_pedido) is not null
            then
               begin
                  insert into fatu_052
                    (cod_empresa,
                   num_nota,
                   cod_serie_nota,
                   cod_mensagem,
                   cnpj9,
                   cnpj4,
                   cnpj2,
                   ind_entr_saida,
                   seq_mensagem,
                   ind_local,
                   des_mensag_1,
                   des_mensag_2,
                   des_mensag_3,
                   des_mensag_4,
                   des_mensag_5,
                   des_mensag_6,
                   des_mensag_7,
                   des_mensag_8,
                   des_mensag_9,
                   des_mensag_10)
                  (select :new.codigo_empresa,
                          :new.num_nota_fiscal,
                    :new.serie_nota_fisc,
                    o.cod_mensagem,
                    :new.cgc_9,
                    :new.cgc_4,
                    :new.cgc_2,
                    'S',
                    w_cod_sequencia_nrpedido,
                    w_cod_local_nrpedido,
                    substr(o.des_mensagem1 ||': ' || w_mensagem_pedido,1,55),
                    substr(o.des_mensagem1 ||': ' || w_mensagem_pedido,56,55),
                    substr(o.des_mensagem1 ||': ' || w_mensagem_pedido,111,55),
                    substr(o.des_mensagem1 ||': ' || w_mensagem_pedido,166,55),
                    substr(o.des_mensagem1 ||': ' || w_mensagem_pedido,221,55),
                    substr(o.des_mensagem1 ||': ' || w_mensagem_pedido,276,55),
                    substr(o.des_mensagem1 ||': ' || w_mensagem_pedido,331,55),
                    substr(o.des_mensagem1 ||': ' || w_mensagem_pedido,386,55),
                    substr(o.des_mensagem1 ||': ' || w_mensagem_pedido,441,55),
                    substr(o.des_mensagem1 ||': ' || w_mensagem_pedido,496,55)
                   from obrf_874 o
                   where o.cod_mensagem = w_cod_msg_nrpedido
                     and :new.pedido_venda > 0
                     and w_cod_msg_nrpedido > 0);
                      exception
                  WHEN dup_val_on_index THEN
                     NULL;
                  when others then
                     raise_application_error(-20000,'Erro ao inserir Mensagem do Pedido da nota' || sqlerrm);
               end;
            end if;
         end if;


         begin
            select to_char(p.colecao_tabela) ||'.'|| to_char(p.mes_tabela) ||'.'||to_char(p.sequencia_tabela)
            into  w_vl_msg_tabela
            from pedi_100 p
            where p.pedido_venda = :new.pedido_venda
             and w_cod_msg_tabela > 0;
         exception
          when no_data_found then
            w_vl_msg_tabela := ' ';
         end;

         -- MENSAGENS DEFAULT
         if :new.cod_status <> '100'
         then
            begin
              insert into fatu_052
              (cod_empresa,
               num_nota,
               cod_serie_nota,
               cod_mensagem,
               cnpj9,
               cnpj4,
               cnpj2,
               ind_entr_saida,
         ind_local,
               seq_mensagem,
               des_mensag_1,
               des_mensag_2,
               des_mensag_3,
               des_mensag_4,
               des_mensag_5,
               des_mensag_6,
               des_mensag_7,
               des_mensag_8,
               des_mensag_9,
               des_mensag_10)
              (select :new.codigo_empresa,
                      :new.num_nota_fiscal,
                      :new.serie_nota_fisc,
                      o.cod_mensagem,
                      :new.cgc_9,
                      :new.cgc_4,
                      :new.cgc_2,
                      'S',
                      o.ind_local,
                      o.cod_mensagem,
                      o.des_mensagem1,
                      o.des_mensagem2,
                      o.des_mensagem3,
                      o.des_mensagem4,
                      o.des_mensagem5,
                      o.des_mensagem6,
                      o.des_mensagem7,
                      o.des_mensagem8,
                      o.des_mensagem9,
                      o.des_mensagem10
               from obrf_874 o
               where o.msg_default = 'S');
            exception
               WHEN dup_val_on_index THEN
               NULL;
               when others then
                  raise_application_error(-20000,'Erro ao inserir Mensagem default da nota' || Chr(10) || SQLERRM);
            end;
         end if;

         --Mensagens da natureza de operacao
         if :new.cod_status <> '100'
         then
             w_cod_natureza := 0;
             w_mensagem_natureza := '';
             w_mensagem_natureza_aux := '';
             w_cod_natureza_comercial := 0;
             w_mensagem_natureza_co := '';
             w_mensagem_natureza_aux_co := '';

             for mensagem_natureza in (select distinct(pedi_080.cod_mensagem) from fatu_060, pedi_080
                                       where fatu_060.ch_it_nf_cd_empr  = :new.codigo_empresa
                                         and fatu_060.ch_it_nf_num_nfis = :new.num_nota_fiscal
                                         and fatu_060.ch_it_nf_ser_nfis = :new.serie_nota_fisc
                                         and pedi_080.natur_operacao = fatu_060.natopeno_nat_oper
                                         and pedi_080.estado_natoper = fatu_060.natopeno_est_oper
                                         and exists(select 1  from obrf_874
                                                    where cod_mensagem = pedi_080.cod_mensagem )
                                    order by pedi_080.cod_mensagem
                                         )
             loop
                w_cod_natureza := mensagem_natureza.cod_mensagem;

                if  w_cod_natureza > 0
                then

                  select ind_local,       des_mensagem1,   des_mensagem2,
                          des_mensagem3,   des_mensagem4,   des_mensagem5,
                          des_mensagem6,   des_mensagem7,   des_mensagem8,
                          des_mensagem9,   des_mensagem10
                  into   v_ind_local,     v_des_mensagem1, v_des_mensagem2,
                          v_des_mensagem3, v_des_mensagem4, v_des_mensagem5,
                          v_des_mensagem6, v_des_mensagem7, v_des_mensagem8,
                          v_des_mensagem9, v_des_mensagem10
                  from obrf_874
                  where cod_mensagem = w_cod_natureza;

                  w_mensagem_natureza_aux :=  v_des_mensagem1 || v_des_mensagem2 ||
                                              v_des_mensagem3 || v_des_mensagem4 ||
                                              v_des_mensagem5 || v_des_mensagem6 ||
                                              v_des_mensagem7 || v_des_mensagem8 ||
                                              v_des_mensagem9 || v_des_mensagem10;
                end if;

                w_mensagem_natureza := w_mensagem_natureza || ' ' || inter_fn_retira_acentuacao(w_mensagem_natureza_aux);

             end loop;

             if  w_cod_natureza > 0
             then
               begin
                  insert into fatu_052
                  (cod_empresa,
                  num_nota,
                  cod_serie_nota,
                  cod_mensagem,
                  cnpj9,
                  cnpj4,
                  cnpj2,
                  ind_entr_saida,
                  ind_local,
                  des_mensag_1,
                  des_mensag_2,
                  des_mensag_3,
                  des_mensag_4,
                  des_mensag_5,
                  des_mensag_6,
                  des_mensag_7,
                  des_mensag_8,
                  des_mensag_9,
                  des_mensag_10)
                  (select :new.codigo_empresa,
                        :new.num_nota_fiscal,
                        :new.serie_nota_fisc,
                        o.cod_mensagem,
                        :new.cgc_9,
                        :new.cgc_4,
                        :new.cgc_2,
                        'S',
                        o.ind_local,
                        substr(w_mensagem_natureza,1,100),
                        substr(w_mensagem_natureza,101,100),
                        substr(w_mensagem_natureza,201,100),
                        substr(w_mensagem_natureza,301,100),
                        substr(w_mensagem_natureza,401,100),
                        substr(w_mensagem_natureza,501,100),
                        substr(w_mensagem_natureza,601,100),
                        substr(w_mensagem_natureza,701,100),
                        substr(w_mensagem_natureza,801,100),
                        substr(w_mensagem_natureza,901,100)
                  from obrf_874 o
                  where o.cod_mensagem = w_cod_natureza);
               exception
               WHEN dup_val_on_index THEN
               NULL;
               when others then
                 raise_application_error(-20000,'Erro ao inserir Mensagem default da nota' || Chr(10) || SQLERRM);
               end;
             end if;

             -- mensagem comercial
             for mensagem_natureza_comercial in (select distinct(pedi_080.cod_mensagem_comercial) from fatu_060, pedi_080
                                       where fatu_060.ch_it_nf_cd_empr  = :new.codigo_empresa
                                         and fatu_060.ch_it_nf_num_nfis = :new.num_nota_fiscal
                                         and fatu_060.ch_it_nf_ser_nfis = :new.serie_nota_fisc
                                         and pedi_080.natur_operacao = fatu_060.natopeno_nat_oper
                                         and pedi_080.estado_natoper = fatu_060.natopeno_est_oper
                                         and exists(select 1  from obrf_874
                                                    where cod_mensagem = pedi_080.cod_mensagem_comercial )
                                    order by pedi_080.cod_mensagem_comercial
                                         )
             loop
                w_cod_natureza_comercial := mensagem_natureza_comercial.cod_mensagem_comercial;

                if  w_cod_natureza_comercial > 0
                then

                  select ind_local,       des_mensagem1,   des_mensagem2,
                          des_mensagem3,   des_mensagem4,   des_mensagem5,
                          des_mensagem6,   des_mensagem7,   des_mensagem8,
                          des_mensagem9,   des_mensagem10
                  into   v_ind_local,     v_des_mensagem1, v_des_mensagem2,
                          v_des_mensagem3, v_des_mensagem4, v_des_mensagem5,
                          v_des_mensagem6, v_des_mensagem7, v_des_mensagem8,
                          v_des_mensagem9, v_des_mensagem10
                  from obrf_874
                  where cod_mensagem = w_cod_natureza_comercial;

                  w_mensagem_natureza_aux_co :=  v_des_mensagem1 || v_des_mensagem2 ||
                                              v_des_mensagem3 || v_des_mensagem4 ||
                                              v_des_mensagem5 || v_des_mensagem6 ||
                                              v_des_mensagem7 || v_des_mensagem8 ||
                                              v_des_mensagem9 || v_des_mensagem10;
                end if;

                w_mensagem_natureza_co := w_mensagem_natureza_co || ' ' || inter_fn_retira_acentuacao(w_mensagem_natureza_aux_co);

             end loop;

             if  w_cod_natureza_comercial > 0
             then
               begin
                  insert into fatu_052
                  (cod_empresa,
                  num_nota,
                  cod_serie_nota,
                  cod_mensagem,
                  cnpj9,
                  cnpj4,
                  cnpj2,
                  ind_entr_saida,
                  ind_local,
                  des_mensag_1,
                  des_mensag_2,
                  des_mensag_3,
                  des_mensag_4,
                  des_mensag_5,
                  des_mensag_6,
                  des_mensag_7,
                  des_mensag_8,
                  des_mensag_9,
                  des_mensag_10)
                  (select :new.codigo_empresa,
                        :new.num_nota_fiscal,
                        :new.serie_nota_fisc,
                        o.cod_mensagem,
                        :new.cgc_9,
                        :new.cgc_4,
                        :new.cgc_2,
                        'S',
                        o.ind_local,
                        substr(w_mensagem_natureza_co,1,100),
                        substr(w_mensagem_natureza_co,101,100),
                        substr(w_mensagem_natureza_co,201,100),
                        substr(w_mensagem_natureza_co,301,100),
                        substr(w_mensagem_natureza_co,401,100),
                        substr(w_mensagem_natureza_co,501,100),
                        substr(w_mensagem_natureza_co,601,100),
                        substr(w_mensagem_natureza_co,701,100),
                        substr(w_mensagem_natureza_co,801,100),
                        substr(w_mensagem_natureza_co,901,100)
                  from obrf_874 o
                  where o.cod_mensagem = w_cod_natureza_comercial);
               exception
               WHEN dup_val_on_index THEN
               NULL;
               when others then
                 raise_application_error(-20000,'Erro ao inserir Mensagem default da nota' || Chr(10) || SQLERRM);
               end;
             end if;
         end if;

         -- Mensagem Tabela de precos


         if :new.natop_nf_est_oper <> 'EX' or w_ind_tabela_ex <> 1
         then
            begin
               insert into fatu_052
               (cod_empresa,
               num_nota,
               cod_serie_nota,
               cod_mensagem,
               cnpj9,
               cnpj4,
               cnpj2,
               ind_entr_saida,
               seq_mensagem,
               ind_local,
               des_mensag_1,
               des_mensag_2,
               des_mensag_3,
               des_mensag_4,
               des_mensag_5,
               des_mensag_6,
               des_mensag_7,
               des_mensag_8,
               des_mensag_9,
               des_mensag_10)
               (select :new.codigo_empresa,
                     :new.num_nota_fiscal,
                     :new.serie_nota_fisc,
                     o.cod_mensagem,
                     :new.cgc_9,
                     :new.cgc_4,
                     :new.cgc_2,
                     'S',
                     w_cod_sequencia_tabela,
                     w_cod_local_tabela,
                     o.des_mensagem1 || ': ' || w_vl_msg_tabela,
                     o.des_mensagem2,
                     o.des_mensagem3,
                     o.des_mensagem4,
                     o.des_mensagem5,
                     o.des_mensagem6,
                     o.des_mensagem7,
                     o.des_mensagem8,
                     o.des_mensagem9,
                     o.des_mensagem10
               from obrf_874 o
               where o.cod_mensagem = w_cod_msg_tabela
                and w_vl_msg_tabela <> ' ');
            exception
             WHEN dup_val_on_index THEN
                NULL;

             when others then
                raise_application_error(-20000,'Erro ao inserir Mensagem da tabela de precos' || Chr(10) || SQLERRM);
            end;

         end if;

          -- Mensagem metros cubicos

         if ((:new.natop_nf_est_oper <> 'EX' or w_ind_mt_cubicos_ex <> 1) or w_cod_msg_mt_cubicos > 0)
         then
           begin
               update rcnb_910
               set nota_fiscal = :new.num_nota_fiscal
               where nr_solicitacao = :new.nr_solicitacao
               and   pedido_venda   = :new.pedido_venda
               and   nota_fiscal    = 0;

               select sum(metros_cubicos)
               into
               w_metros_cubicos
               from rcnb_910
               where nr_solicitacao = :new.nr_solicitacao
               and   pedido_venda   = :new.pedido_venda
               and   nota_fiscal    = :new.num_nota_fiscal;

               insert into fatu_052
               (cod_empresa,
               num_nota,
               cod_serie_nota,
               cod_mensagem,
               cnpj9,
               cnpj4,
               cnpj2,
               ind_entr_saida,
               seq_mensagem,
               ind_local,
               des_mensag_1,
               des_mensag_2,
               des_mensag_3,
               des_mensag_4,
               des_mensag_5,
               des_mensag_6,
               des_mensag_7,
               des_mensag_8,
               des_mensag_9,
               des_mensag_10)
               (select :new.codigo_empresa,
                     :new.num_nota_fiscal,
                     :new.serie_nota_fisc,
                     o.cod_mensagem,
                     :new.cgc_9,
                     :new.cgc_4,
                     :new.cgc_2,
                     'S',
                     w_cod_sequencia_mt_cubicos,
                     w_cod_local_mt_cubicos,
                     o.des_mensagem1 || ': ' || w_metros_cubicos,
                     o.des_mensagem2,
                     o.des_mensagem3,
                     o.des_mensagem4,
                     o.des_mensagem5,
                     o.des_mensagem6,
                     o.des_mensagem7,
                     o.des_mensagem8,
                     o.des_mensagem9,
                     o.des_mensagem10
               from obrf_874 o
               where o.cod_mensagem = w_cod_msg_mt_cubicos
                and w_metros_cubicos > 0);
            exception
             WHEN dup_val_on_index THEN
                NULL;

             when others then
                raise_application_error(-20000,'Erro ao inserir Mensagem de metros cubicos' || Chr(10) || SQLERRM);
            end;

         end if;

         -- Mensagem referente a Re-emissao

         if :old.status_impressao_danfe = 1 and :new.status_impressao_danfe = 1
         then
            begin
               insert into fatu_052
               (cod_empresa,
               num_nota,
               cod_serie_nota,
               cod_mensagem,
               cnpj9,
               cnpj4,
               cnpj2,
               ind_entr_saida,
               seq_mensagem,
               ind_local,
               des_mensag_1,
               des_mensag_2,
               des_mensag_3,
               des_mensag_4,
               des_mensag_5,
               des_mensag_6,
               des_mensag_7,
               des_mensag_8,
               des_mensag_9,
               des_mensag_10)
               (select :new.codigo_empresa,
                     :new.num_nota_fiscal,
                     :new.serie_nota_fisc,
                     o_re.cod_mensagem,
                     :new.cgc_9,
                     :new.cgc_4,
                     :new.cgc_2,
                     'S',
                     w_cd_seq_reemissao,
                     w_cd_loc_reemissao,
                     o_re.des_mensagem1,
                     o_re.des_mensagem2,
                     o_re.des_mensagem3,
                     o_re.des_mensagem4,
                     o_re.des_mensagem5,
                     o_re.des_mensagem6,
                     o_re.des_mensagem7,
                     o_re.des_mensagem8,
                     o_re.des_mensagem9,
                     o_re.des_mensagem10
               from obrf_874 o_re
               where o_re.cod_mensagem  = w_cd_msg_reemissao
                and w_cd_msg_reemissao <> 0);
            exception
            WHEN dup_val_on_index THEN
               NULL;
            when others then
               raise_application_error(-20000,'Erro ao inserir Mensagem de Re-emissao' || Chr(10) || SQLERRM);
            end;

         end if;

          -- Mensagem referente notas de ajuste ou complementar

         if :new.tipo_nf_referenciada = 2 /*Complementar*/
         or :new.tipo_nf_referenciada = 3 /*Ajuste*/
         then

            begin
             insert into fatu_052
             (cod_empresa,
              num_nota,
              cod_serie_nota,
              cod_mensagem,
              cnpj9,
              cnpj4,
              cnpj2,
              ind_entr_saida,
              seq_mensagem,
              ind_local,
              des_mensag_1,
              des_mensag_2,
              des_mensag_3,
              des_mensag_4,
              des_mensag_5,
              des_mensag_6,
              des_mensag_7,
              des_mensag_8,
              des_mensag_9,
              des_mensag_10)
             (select :new.codigo_empresa,
                     :new.num_nota_fiscal,
                     :new.serie_nota_fisc,
                     o_re.cod_mensagem,
                     :new.cgc_9,
                     :new.cgc_4,
                     :new.cgc_2,
                     'S',
                     decode(:new.tipo_nf_referenciada, 2, w_seq_msg_nf_comp, w_seq_msg_nf_ajuste),
                     decode(:new.tipo_nf_referenciada, 2, w_loc_msg_nf_comp, w_loc_msg_nf_ajuste),
                     o_re.des_mensagem1,
                     o_re.des_mensagem2,
                     o_re.des_mensagem3,
                     o_re.des_mensagem4,
                     o_re.des_mensagem5,
                     o_re.des_mensagem6,
                     o_re.des_mensagem7,
                     o_re.des_mensagem8,
                     o_re.des_mensagem9,
                     o_re.des_mensagem10
              from obrf_874 o_re
              where o_re.cod_mensagem  = decode(:new.tipo_nf_referenciada, 2, w_cod_msg_nf_comp, w_cod_msg_nf_ajuste)
                and decode(:new.tipo_nf_referenciada, 2, w_cod_msg_nf_comp, w_cod_msg_nf_ajuste) <> 0);
            exception
            WHEN dup_val_on_index THEN
               NULL;
            when others then
               raise_application_error(-20000,'Erro ao inserir Mensagem de Ajuste/Complementar' || Chr(10) || SQLERRM);
            end;

            if updating
            and ((:new.tipo_nf_referenciada = 2 and w_cod_msg_nf_comp <> 0)
            or  (:new.tipo_nf_referenciada = 3 and w_cod_msg_nf_ajuste <> 0))
            then

               gravou_notas := 'n';

               begin
                   select 's'
                     into gravou_notas
                     from fatu_052
                    where fatu_052.cod_empresa    = :new.codigo_empresa
                      and fatu_052.num_nota       = :new.num_nota_fiscal
                      and fatu_052.cod_serie_nota = :new.serie_nota_fisc
                      and fatu_052.cod_mensagem   = decode(:new.tipo_nf_referenciada, 2, w_cod_msg_nf_comp, w_cod_msg_nf_ajuste)
                      and fatu_052.seq_mensagem   = decode(:new.tipo_nf_referenciada, 2, w_seq_msg_nf_comp, w_seq_msg_nf_ajuste)
                      and fatu_052.cnpj9          = :new.cgc_9
                      and fatu_052.cnpj4          = :new.cgc_4
                      and fatu_052.cnpj2          = :new.cgc_2
                      and fatu_052.ind_entr_saida = 'S'
                      and fatu_052.des_mensag_5 like '%NF Ref.:%';
               exception when others then
                   gravou_notas := 'n';
               end;

               if gravou_notas = 'n'
               then

                   begin
                      select distinct(to_char(fatu_060.nota_ajuste) || '/' || fatu_060.serie_ajuste)
                      BULK COLLECT INTO notas_ref
                        from fatu_060
                       where fatu_060.ch_it_nf_cd_empr  = :new.codigo_empresa
                         and fatu_060.ch_it_nf_num_nfis = :new.num_nota_fiscal
                         and fatu_060.ch_it_nf_ser_nfis = :new.serie_nota_fisc
                      order by to_char(fatu_060.nota_ajuste) || '/' || fatu_060.serie_ajuste;
                   end;

                  if sql%found
                  then

                     FOR i IN notas_ref.FIRST .. notas_ref.LAST
                     LOOP

                      notas_concat := notas_concat || notas_ref(i) || ' ';

                     END LOOP;
                  end if;

                  begin
                   update fatu_052
                      set fatu_052.des_mensag_5 = fatu_052.des_mensag_5 || ' NF Ref.: ' || notas_concat
                    where fatu_052.cod_empresa    = :new.codigo_empresa
                      and fatu_052.num_nota       = :new.num_nota_fiscal
                      and fatu_052.cod_serie_nota = :new.serie_nota_fisc
                      and fatu_052.cod_mensagem   = decode(:new.tipo_nf_referenciada, 2, w_cod_msg_nf_comp, w_cod_msg_nf_ajuste)
                      and fatu_052.seq_mensagem   = decode(:new.tipo_nf_referenciada, 2, w_seq_msg_nf_comp, w_seq_msg_nf_ajuste)
                      and fatu_052.cnpj9          = :new.cgc_9
                      and fatu_052.cnpj4          = :new.cgc_4
                      and fatu_052.cnpj2          = :new.cgc_2
                      and fatu_052.ind_entr_saida = 'S';
                  end;
               end if;
            end if;
         end if;

          -- Mensagem referente ao box

          if :new.natop_nf_est_oper <> 'EX' or w_ind_box_ex <> 1
          then

             begin
                insert into fatu_052
                (cod_empresa,
                 num_nota,
                 cod_serie_nota,
                 cod_mensagem,
                 cnpj9,
                 cnpj4,
                 cnpj2,
                 ind_entr_saida,
                 seq_mensagem,
                 ind_local,
                 des_mensag_1,
                 des_mensag_2,
                 des_mensag_3,
                 des_mensag_4,
                 des_mensag_5)
                (select :new.codigo_empresa,
                        :new.num_nota_fiscal,
                        :new.serie_nota_fisc,
                        o.cod_mensagem,
                        :new.cgc_9,
                        :new.cgc_4,
                        :new.cgc_2,
                        'S',
                        w_cod_sequencia_box,
                        w_cod_local_box,
                        substr(o.des_mensagem1 || ': ' || w_vl_msg_box,001,55),
                      substr(o.des_mensagem1 || ': ' || w_vl_msg_box,056,55),
                      substr(o.des_mensagem1 || ': ' || w_vl_msg_box,101,55),
                      substr(o.des_mensagem1 || ': ' || w_vl_msg_box,156,55),
                      substr(o.des_mensagem1 || ': ' || w_vl_msg_box,201,55)
                 from obrf_874 o
                 where o.cod_mensagem = w_cod_msg_box
                   and w_vl_msg_box <> ' '
                   and w_cod_msg_box <> 0);
             exception
               WHEN dup_val_on_index THEN
                  NULL;
               when others then
                  raise_application_error(-20000,'Erro ao inserir Mensagem das Mensagens de BOX' || Chr(10) || SQLERRM);
             end;

          end if;

       --Mensagem referente ao Total de Pecas

       --SS 55129 - Luiz
         if :new.natop_nf_est_oper <> 'EX' or w_ind_tot_pecas_ex <> 1
         then

             select nvl(sum(fatu_060.qtde_item_fatur),0)
               into w_vl_msg_tot_pecas
             from fatu_060
             where fatu_060.ch_it_nf_cd_empr  = :new.codigo_empresa
               and fatu_060.ch_it_nf_num_nfis = :new.num_nota_fiscal
               and fatu_060.ch_it_nf_ser_nfis = :new.serie_nota_fisc
               and fatu_060.nivel_estrutura   = '1';

             begin
                insert into fatu_052
                (cod_empresa,
                 num_nota,
                 cod_serie_nota,
                 cod_mensagem,
                 cnpj9,
                 cnpj4,
                 cnpj2,
                 ind_entr_saida,
                 seq_mensagem,
                 ind_local,
                 des_mensag_1,
                 des_mensag_2,
                 des_mensag_3,
                 des_mensag_4,
                 des_mensag_5)
                (select :new.codigo_empresa,
                        :new.num_nota_fiscal,
                        :new.serie_nota_fisc,
                        o.cod_mensagem,
                        :new.cgc_9,
                        :new.cgc_4,
                        :new.cgc_2,
                        'S',
                        w_cod_sequencia_tot_pecas,
                        w_cod_local_tot_pecas,
                        o.des_mensagem1 || ': ' || to_char(w_vl_msg_tot_pecas),
                        o.des_mensagem2,
                        o.des_mensagem3,
                        o.des_mensagem4,
                        o.des_mensagem5
                 from obrf_874 o
                 where o.cod_mensagem = w_cod_msg_tot_pecas
                   and w_vl_msg_tot_pecas > 0
                   and w_cod_msg_tot_pecas <> 0);
                exception
                WHEN dup_val_on_index THEN
                   NULL;

                when others then
                 raise_application_error(-20000,'Erro ao inserir Mensagem da Quantidade de Pecas' || Chr(10) || SQLERRM);
             end;

          end if;

       --Mensagem referente ao Total Rolo

         if :new.natop_nf_est_oper <> 'EX' or w_ind_tot_rolo_ex <> 1
         then

             if :new.nota_fatura = 0
             then w_nota_fatura := :new.num_nota_fiscal;
             else w_nota_fatura := :new.nota_fatura;
             end if;

             if :new.nota_entrega = 0
             then w_nota_entrega := :new.num_nota_fiscal;
             else w_nota_entrega := :new.nota_entrega;
             end if;

             select nvl(count(*),0)
               into w_vl_msg_tot_rolos
             from pcpt_020
             where pcpt_020.cod_empresa_nota  = :new.codigo_empresa
               and pcpt_020.nota_fiscal       in (:new.num_nota_fiscal, w_nota_fatura, w_nota_entrega)
               and pcpt_020.serie_fiscal_sai  = :new.serie_nota_fisc;

             begin
                insert into fatu_052
                (cod_empresa,
                 num_nota,
                 cod_serie_nota,
                 cod_mensagem,
                 cnpj9,
                 cnpj4,
                 cnpj2,
                 ind_entr_saida,
                 seq_mensagem,
                 ind_local,
                 des_mensag_1,
                 des_mensag_2,
                 des_mensag_3,
                 des_mensag_4,
                 des_mensag_5,
                 des_mensag_6,
                 des_mensag_7,
                 des_mensag_8,
                 des_mensag_9,
                 des_mensag_10)
                (select :new.codigo_empresa,
                        :new.num_nota_fiscal,
                        :new.serie_nota_fisc,
                        o.cod_mensagem,
                        :new.cgc_9,
                        :new.cgc_4,
                        :new.cgc_2,
                        'S',
                        w_cod_sequencia_tot_rolo,
                        w_cod_local_tot_rolo,
                        o.des_mensagem1 || ': ' || to_char(w_vl_msg_tot_rolos),
                        o.des_mensagem2,
                        o.des_mensagem3,
                        o.des_mensagem4,
                        o.des_mensagem5,
                        o.des_mensagem6,
                        o.des_mensagem7,
                        o.des_mensagem8,
                        o.des_mensagem9,
                        o.des_mensagem10
                 from obrf_874 o
                 where o.cod_mensagem = w_cod_msg_tot_rolo
                   and w_vl_msg_tot_rolos > 0
                   and w_cod_msg_tot_rolo <> 0);
                exception
                WHEN dup_val_on_index THEN
                   NULL;

                when others then
                 raise_application_error(-20000,'Erro ao inserir Mensagem da Quantidade de rolos' || Chr(10) || SQLERRM);
             end;

          end if;

      -- Mensagem referente ao Representante

          if :new.natop_nf_est_oper <> 'EX' or w_ind_representante_ex <> 1
          then

             begin
               select pedi_020.nome_rep_cliente
                 INTO w_vl_msg_represen
               from pedi_020
               where pedi_020.cod_rep_cliente = :new.cod_rep_cliente;
             exception
              when no_data_found then
                w_vl_msg_represen := ' ';
             end;

             begin
                insert into fatu_052
                (cod_empresa,
                 num_nota,
                 cod_serie_nota,
                 cod_mensagem,
                 cnpj9,
                 cnpj4,
                 cnpj2,
                 ind_entr_saida,
                 seq_mensagem,
                 ind_local,
                 des_mensag_1,
                 des_mensag_2,
                 des_mensag_3,
                 des_mensag_4,
                 des_mensag_5,
                 des_mensag_6,
                 des_mensag_7,
                 des_mensag_8,
                 des_mensag_9,
                 des_mensag_10)
                (select :new.codigo_empresa,
                        :new.num_nota_fiscal,
                        :new.serie_nota_fisc,
                        o.cod_mensagem,
                        :new.cgc_9,
                        :new.cgc_4,
                        :new.cgc_2,
                        'S',
                        w_cod_sequencia_represen,
                        w_cod_local_represen,
                        substr(o.des_mensagem1 || ': ' || w_vl_msg_represen,1,50),
                        substr(o.des_mensagem1 || ': ' || w_vl_msg_represen,51,length(o.des_mensagem1 || ': ' || w_vl_msg_represen)),
                        o.des_mensagem3,
                        o.des_mensagem4,
                        o.des_mensagem5,
                        o.des_mensagem6,
                        o.des_mensagem7,
                        o.des_mensagem8,
                        o.des_mensagem9,
                        o.des_mensagem10
                 from obrf_874 o
                 where o.cod_mensagem = w_cod_msg_represen
                   and w_vl_msg_represen <> ' '
                   and w_cod_msg_represen <> 0);
             exception
                WHEN dup_val_on_index THEN
                   NULL;
              when others then
                raise_application_error(-20000,'Erro ao inserir Mensagem do Representante ' || SQLERRM);
             end;

          end if;

      --Mensagem referente ao Perc. de Comissao

          if :new.natop_nf_est_oper <> 'EX' or w_ind_perc_comissao_ex <> 1
          then

             begin
                insert into fatu_052
                (cod_empresa,
                 num_nota,
                 cod_serie_nota,
                 cod_mensagem,
                 cnpj9,
                 cnpj4,
                 cnpj2,
                 ind_entr_saida,
                 seq_mensagem,
                 ind_local,
                 des_mensag_1,
                 des_mensag_2,
                 des_mensag_3,
                 des_mensag_4,
                 des_mensag_5,
                 des_mensag_6,
                 des_mensag_7,
                 des_mensag_8,
                 des_mensag_9,
                 des_mensag_10)
                (select :new.codigo_empresa,
                        :new.num_nota_fiscal,
                        :new.serie_nota_fisc,
                        o.cod_mensagem,
                        :new.cgc_9,
                        :new.cgc_4,
                        :new.cgc_2,
                        'S',
                        w_cod_sequencia_perc_comis,
                        w_cod_local_perc_comis,
                        o.des_mensagem1 || ': ' || to_char((:new.perc_repres),'9999990.00'),
                        o.des_mensagem2,
                        o.des_mensagem3,
                        o.des_mensagem4,
                        o.des_mensagem5,
                        o.des_mensagem6,
                        o.des_mensagem7,
                        o.des_mensagem8,
                        o.des_mensagem9,
                        o.des_mensagem10
                 from obrf_874 o
                 where o.cod_mensagem = w_cod_msg_perc_comis
                   and :new.perc_repres > 0
                   and w_cod_msg_perc_comis <> 0);
             exception
                WHEN dup_val_on_index THEN
                   NULL;
                when others then
                   raise_application_error(-20000,'Erro ao inserir Mensagem do Percentual de comissao' || Chr(10) || SQLERRM);
             end;

          end if;

      --Mensage referente a GRamatura

          if :new.natop_nf_est_oper <> 'EX' or w_ind_gramatura_ex <> 1
          then

             begin
                insert into fatu_052
                (cod_empresa,
                 num_nota,
                 cod_serie_nota,
                 cod_mensagem,
                 cnpj9,
                 cnpj4,
                 cnpj2,
                 ind_entr_saida,
                 seq_mensagem,
                 ind_local,
                 des_mensag_1,
                 des_mensag_2,
                 des_mensag_3,
                 des_mensag_4,
                 des_mensag_5,
                 des_mensag_6,
                 des_mensag_7,
                 des_mensag_8,
                 des_mensag_9,
                 des_mensag_10)
                (select :new.codigo_empresa,
                        :new.num_nota_fiscal,
                        :new.serie_nota_fisc,
                        o.cod_mensagem,
                        :new.cgc_9,
                        :new.cgc_4,
                        :new.cgc_2,
                        'S',
                        w_cod_sequencia_gm2,
                        w_cod_local_gm2,
                        o.des_mensagem1,
                        o.des_mensagem2,
                        o.des_mensagem3,
                        o.des_mensagem4,
                        o.des_mensagem5,
                        o.des_mensagem6,
                        o.des_mensagem7,
                        o.des_mensagem8,
                        o.des_mensagem9,
                        o.des_mensagem10
                 from obrf_874 o
                 where o.cod_mensagem = w_cod_msg_gm2
                   and w_cod_msg_gm2 > 0
                   and Substr(w_flag_possui_rolo, 1, 1) = 'S');
             exception
                WHEN dup_val_on_index THEN
                   NULL;
                when others then
                   raise_application_error(-20000,'Erro ao inserir Mensagem do GM2' || Chr(10) || SQLERRM);
             end;

          end if;

      --mensagem referente a Largura

          if :new.natop_nf_est_oper <> 'EX' or w_ind_largura_ex <> 1
          then

             begin
                insert into fatu_052
                (cod_empresa,
                 num_nota,
                 cod_serie_nota,
                 cod_mensagem,
                 cnpj9,
                 cnpj4,
                 cnpj2,
                 ind_entr_saida,
                 seq_mensagem,
                 ind_local,
                 des_mensag_1,
                 des_mensag_2,
                 des_mensag_3,
                 des_mensag_4,
                 des_mensag_5,
                 des_mensag_6,
                 des_mensag_7,
                 des_mensag_8,
                 des_mensag_9,
                 des_mensag_10)
                (select :new.codigo_empresa,
                        :new.num_nota_fiscal,
                        :new.serie_nota_fisc,
                        o.cod_mensagem,
                        :new.cgc_9,
                        :new.cgc_4,
                        :new.cgc_2,
                        'S',
                        w_cod_sequencia_largura,
                        w_cod_local_largura,
                        o.des_mensagem1,
                        o.des_mensagem2,
                        o.des_mensagem3,
                        o.des_mensagem4,
                        o.des_mensagem5,
                        o.des_mensagem6,
                        o.des_mensagem7,
                        o.des_mensagem8,
                        o.des_mensagem9,
                        o.des_mensagem10
                 from obrf_874 o
                 where o.cod_mensagem = w_cod_msg_largura
                   and w_cod_msg_largura > 0
                   and Substr(w_flag_possui_rolo, 1, 1) = 'S');
             exception
                WHEN dup_val_on_index THEN
                NULL;

               when others then
                raise_application_error(-20000,'Erro ao inserir Mensagem de largura'  || Chr(10) || SQLERRM);
             end;

          end if;

       --Mensagem referente aos Encargos

          if :new.natop_nf_est_oper <> 'EX' or w_ind_msg_encargos_ex <> 1
          then

             begin
                insert into fatu_052
                (cod_empresa,
                 num_nota,
                 cod_serie_nota,
                 cod_mensagem,
                 cnpj9,
                 cnpj4,
                 cnpj2,
                 ind_entr_saida,
                 seq_mensagem,
                 ind_local,
                 des_mensag_1,
                 des_mensag_2,
                 des_mensag_3,
                 des_mensag_4,
                 des_mensag_5,
                 des_mensag_6,
                 des_mensag_7,
                 des_mensag_8,
                 des_mensag_9,
                 des_mensag_10)
                (select :new.codigo_empresa,
                        :new.num_nota_fiscal,
                        :new.serie_nota_fisc,
                        o.cod_mensagem,
                        :new.cgc_9,
                        :new.cgc_4,
                        :new.cgc_2,
                        'S',
                        w_cod_sequencia_encargos,
                        w_cod_local_encargos,
                        substr(o.des_mensagem1 || ' : ' || to_char(:new.encargos,'90.00') || '% ' ||to_char(:new.valor_encar_nota,'999990.00'),1,55),
                        o.des_mensagem2,
                        o.des_mensagem3,
                        o.des_mensagem4,
                        o.des_mensagem5,
                        o.des_mensagem6,
                        o.des_mensagem7,
                        o.des_mensagem8,
                        o.des_mensagem9,
                        o.des_mensagem10
                 from obrf_874 o
                 where o.cod_mensagem = w_cod_msg_encargos
                   and w_cod_msg_encargos > 0
                   and :new.valor_encar_nota > 0
                   and :new.encargos > 0);
             exception
                WHEN dup_val_on_index THEN
                NULL;

               when others then
                raise_application_error(-20000,'Erro ao inserir Mensagem de encargos' || Chr(10) || SQLERRM);
             end;

          end if;


          if :new.natop_nf_est_oper <> 'EX' or w_ind_nf_fatura_ex <> 1
          then
             
             -- deleta mensagem de nota ao inserir uma nota de entrega

             if :new.nota_entrega > 0 and w_cod_msg_simples_rem > 0 and :old.nota_entrega = 0
             then
                begin
                    delete from fatu_052 
                    where cod_empresa = :new.codigo_empresa
                    and num_nota = :new.num_nota_fiscal
                    and cod_serie_nota = :new.serie_nota_fisc
                    and cod_mensagem = w_cod_msg_simples_rem
                    and cnpj9 = :new.cgc_9
                    and cnpj4 = :new.cgc_4
                    and cnpj2 = :new.cgc_2
                    and ind_entr_saida = 'S'
                    and ind_local = w_cod_local_simples_rem;
                exception
                    when others then
                    raise_application_error(-20000,'Erro ao deletar mensagem de nota.' || Chr(10) || SQLERRM);
                end;
             end if;

             -- gera mensagem de nota fiscal referenciada (por conta e ordem)
             -- esta mensagem se refere a NF de fatura


             if :new.nota_entrega > 0 and w_cod_msg_simples_rem > 0
             then
                  begin
                    select pedi_010.nome_cliente,
                           pedi_010.insc_est_cliente,
                           pedi_150.cgc_entr_cobr9,
                           pedi_150.cgc_entr_cobr4,
                           pedi_150.cgc_entr_cobr2,
                           basi_160.cidade || ' - ' || basi_160.estado,
                           pedi_150.bairro_entr_cobr,
                           pedi_150.end_entr_cobr,
                           pedi_150.numero_imovel,
                           pedi_150.complemento_endereco,
                           pedi_150.cep_entr_cobr
                      into w_nome_cliente,
                           w_insc_est_cliente,
                           w_cd_cli_cgc_cli9,
                           w_cd_cli_cgc_cli4,
                           w_cd_cli_cgc_cli2,
                           w_cid_entr_cobr,
                           w_bairro_entr_cobr,
                           w_end_entr_cobr,
                           w_numero_imovel,
                           w_complemento_endereco,
                           w_cep_entr_cobr
                    from pedi_010, pedi_150, basi_160
                    where pedi_150.cd_cli_cgc_cli9 = :new.cgc_9
                      and pedi_150.cd_cli_cgc_cli4 = :new.cgc_4
                      and pedi_150.cd_cli_cgc_cli2 = :new.cgc_2
                      and pedi_150.seq_endereco    = :new.seq_end_entr
                      and pedi_150.cid_entr_cobr   = basi_160.cod_cidade
                      and pedi_010.cgc_9 =  pedi_150.cgc_entr_cobr9
                      and pedi_010.cgc_4 =  pedi_150.cgc_entr_cobr4
                      and pedi_010.cgc_2 =  pedi_150.cgc_entr_cobr2;
                  exception
                  when no_data_found then
                     w_nome_cliente         := ' ';
                     w_insc_est_cliente     := ' ';
                     w_cd_cli_cgc_cli9      := 0;
                     w_cd_cli_cgc_cli4      := 0;
                     w_cd_cli_cgc_cli2      := 0;
                     w_cid_entr_cobr        := ' ';
                     w_bairro_entr_cobr     := ' ';
                     w_end_entr_cobr        := ' ';
                     w_numero_imovel        := ' ';
                     w_complemento_endereco := ' ';
                     w_cep_entr_cobr        := 0;
                  end;

               begin
                  insert into fatu_052
                  (cod_empresa,
                   num_nota,
                   cod_serie_nota,
                   cod_mensagem,
                   cnpj9,
                   cnpj4,
                   cnpj2,
                   ind_entr_saida,
                   seq_mensagem,
                   ind_local,
                   des_mensag_1,
                   des_mensag_2,
                   des_mensag_3,
                   des_mensag_4,
                   des_mensag_5)
                  (select :new.codigo_empresa,
                          :new.num_nota_fiscal,
                          :new.serie_nota_fisc,
                          o.cod_mensagem,
                          :new.cgc_9,
                          :new.cgc_4,
                          :new.cgc_2,
                          'S',
                          w_cod_sequencia_simples_rem,
                          w_cod_local_simples_rem,
                          substr(o.des_mensagem1 || decode(w_flag_ent_nota_serie,   1, to_char(:new.nota_entrega,'000000000') ||'/'||:new.serie_nota_fisc, 0, '')
                                                 || decode(w_flag_ent_data_emis,    1, ' DT.EMISSAO: ' || to_char(:new.data_emissao,'DD/MM/YY'), 0, '')
                                                 || decode(w_flag_ent_nome_cliente, 1, ' CLIENTE: ' || w_nome_cliente, 0, '')
                                                 || decode(w_flag_ent_cnpj_cliente, 1, ' CNPJ ' || to_char(w_cd_cli_cgc_cli9,'000000000') || '/' ||to_char(w_cd_cli_cgc_cli4,'0000')|| '-' || to_char(w_cd_cli_cgc_cli2,'00'), 0, '')
                                                 || decode(w_flag_ent_insc_estad,   1, ' - INS.EST.: ' || w_insc_est_cliente, 0, '')
                                                 || decode(w_flag_ent_nome_cidade,  1, ' - CIDADE: ' || w_cid_entr_cobr, 0, '')
                                                 || decode(w_flag_ent_bairro,       1, ' - BAIRRO: ' || w_bairro_entr_cobr, 0, '')
                                                 || decode(w_flag_ent_end_nr_compl, 1, ' - END.: ' || w_end_entr_cobr || ', NR.: ' || w_numero_imovel || ', COMPL.: ' || w_complemento_endereco, 0, '')
                                                 || decode(w_flag_ent_cep,          1, ' - CEP: ' || to_char(w_cep_entr_cobr,'00000000'), 0, '') ,1,55),
                                      substr(o.des_mensagem1 || decode(w_flag_ent_nota_serie,   1, to_char(:new.nota_entrega,'000000000') ||'/'||:new.serie_nota_fisc, 0, '')
                                                       || decode(w_flag_ent_data_emis,    1, ' DT.EMISSAO: ' || to_char(:new.data_emissao,'DD/MM/YY'), 0, '')
                                                 || decode(w_flag_ent_nome_cliente, 1, ' CLIENTE: ' || w_nome_cliente, 0, '')
                                                 || decode(w_flag_ent_cnpj_cliente, 1, ' CNPJ ' || to_char(w_cd_cli_cgc_cli9,'000000000') || '/' ||to_char(w_cd_cli_cgc_cli4,'0000')|| '-' || to_char(w_cd_cli_cgc_cli2,'00'), 0, '')
                                                 || decode(w_flag_ent_insc_estad,   1, ' - INS.EST.: ' || w_insc_est_cliente, 0, '')
                                                 || decode(w_flag_ent_nome_cidade,  1, ' - CIDADE: ' || w_cid_entr_cobr, 0, '')
                                                 || decode(w_flag_ent_bairro,       1, ' - BAIRRO: ' || w_bairro_entr_cobr, 0, '')
                                                 || decode(w_flag_ent_end_nr_compl, 1, ' - END.: ' || w_end_entr_cobr || ', NR.: ' || w_numero_imovel || ', COMPL.: ' || w_complemento_endereco, 0, '')
                                                 || decode(w_flag_ent_cep,          1, ' - CEP: ' || to_char(w_cep_entr_cobr,'00000000'), 0, '') ,56,55),
                                      substr(o.des_mensagem1 || decode(w_flag_ent_nota_serie,   1, to_char(:new.nota_entrega,'000000000') ||'/'||:new.serie_nota_fisc, 0, '')
                                                        || decode(w_flag_ent_data_emis,    1, ' DT.EMISSAO: ' || to_char(:new.data_emissao,'DD/MM/YY'), 0, '')
                                                 || decode(w_flag_ent_nome_cliente, 1, ' CLIENTE: ' || w_nome_cliente, 0, '')
                                                 || decode(w_flag_ent_cnpj_cliente, 1, ' CNPJ ' || to_char(w_cd_cli_cgc_cli9,'000000000') || '/' ||to_char(w_cd_cli_cgc_cli4,'0000')|| '-' || to_char(w_cd_cli_cgc_cli2,'00'), 0, '')
                                                 || decode(w_flag_ent_insc_estad,   1, ' - INS.EST.: ' || w_insc_est_cliente, 0, '')
                                                 || decode(w_flag_ent_nome_cidade,  1, ' - CIDADE: ' || w_cid_entr_cobr, 0, '')
                                                 || decode(w_flag_ent_bairro,       1, ' - BAIRRO: ' || w_bairro_entr_cobr, 0, '')
                                                 || decode(w_flag_ent_end_nr_compl, 1, ' - END.: ' || w_end_entr_cobr || ', NR.: ' || w_numero_imovel || ', COMPL.: ' || w_complemento_endereco, 0, '')
                                                 || decode(w_flag_ent_cep,          1, ' - CEP: ' || to_char(w_cep_entr_cobr,'00000000'), 0, '') ,111,55),
                                      substr(o.des_mensagem1 || decode(w_flag_ent_nota_serie,   1, to_char(:new.nota_entrega,'000000000') ||'/'||:new.serie_nota_fisc, 0, '')
                                                       || decode(w_flag_ent_data_emis,    1, ' DT.EMISSAO: ' || to_char(:new.data_emissao,'DD/MM/YY'), 0, '')
                                                 || decode(w_flag_ent_nome_cliente, 1, ' CLIENTE: ' || w_nome_cliente, 0, '')
                                                 || decode(w_flag_ent_cnpj_cliente, 1, ' CNPJ ' || to_char(w_cd_cli_cgc_cli9,'000000000') || '/' ||to_char(w_cd_cli_cgc_cli4,'0000')|| '-' || to_char(w_cd_cli_cgc_cli2,'00'), 0, '')
                                                 || decode(w_flag_ent_insc_estad,   1, ' - INS.EST.: ' || w_insc_est_cliente, 0, '')
                                                 || decode(w_flag_ent_nome_cidade,  1, ' - CIDADE: ' || w_cid_entr_cobr, 0, '')
                                                 || decode(w_flag_ent_bairro,       1, ' - BAIRRO: ' || w_bairro_entr_cobr, 0, '')
                                                 || decode(w_flag_ent_end_nr_compl, 1, ' - END.: ' || w_end_entr_cobr || ', NR.: ' || w_numero_imovel || ', COMPL.: ' || w_complemento_endereco, 0, '')
                                                  || decode(w_flag_ent_cep,          1, ' - CEP: ' || to_char(w_cep_entr_cobr,'00000000'), 0, '') ,166,55),
                                      substr(o.des_mensagem1 || decode(w_flag_ent_nota_serie,   1, to_char(:new.nota_entrega,'000000000') ||'/'||:new.serie_nota_fisc, 0, '')
                                                        || decode(w_flag_ent_data_emis,    1, ' DT.EMISSAO: ' || to_char(:new.data_emissao,'DD/MM/YY'), 0, '')
                                                 || decode(w_flag_ent_nome_cliente, 1, ' CLIENTE: ' || w_nome_cliente, 0, '')
                                                 || decode(w_flag_ent_cnpj_cliente, 1, ' CNPJ ' || to_char(w_cd_cli_cgc_cli9,'000000000') || '/' ||to_char(w_cd_cli_cgc_cli4,'0000')|| '-' || to_char(w_cd_cli_cgc_cli2,'00'), 0, '')
                                                 || decode(w_flag_ent_insc_estad,   1, ' - INS.EST.: ' || w_insc_est_cliente, 0, '')
                                                 || decode(w_flag_ent_nome_cidade,  1, ' - CIDADE: ' || w_cid_entr_cobr, 0, '')
                                                 || decode(w_flag_ent_bairro,       1, ' - BAIRRO: ' || w_bairro_entr_cobr, 0, '')
                                                 || decode(w_flag_ent_end_nr_compl, 1, ' - END.: ' || w_end_entr_cobr || ', NR.: ' || w_numero_imovel || ', COMPL.: ' || w_complemento_endereco, 0, '')
                                                 || decode(w_flag_ent_cep,          1, ' - CEP: ' || to_char(w_cep_entr_cobr,'00000000'), 0, '') ,221,55)
                   from obrf_874 o
                   where o.cod_mensagem = w_cod_msg_simples_rem
                     and w_cod_msg_simples_rem > 0);
               exception
                  WHEN dup_val_on_index THEN
                    NULL;
                  when others then
                    raise_application_error(-20000,'Erro ao inserir Mensagem de entrega' || Chr(10) || SQLERRM);
               end;
             end if;

          end if;


          if :new.natop_nf_est_oper <> 'EX' or w_ind_nf_entrega_ex <> 1
          then

             -- gera mensagem de nota fiscal referenciada (por conta e ordem)
             -- esta mensagem se refere a NF de entrega

             if :new.nota_fatura > 0 and w_cod_msg_entrega > 0
             then

               if :new.cnpj9_fatura > 0
               then

                  begin
                     select pedi_010.nome_cliente,
                            pedi_010.insc_est_cliente,
                            basi_160.cidade || ' - ' || basi_160.estado,
                            pedi_010.bairro,
                            pedi_010.endereco_cliente,
                            pedi_010.numero_imovel,
                            pedi_010.complemento,
                            pedi_010.cep_cliente
                       into w_nome_cliente,
                            w_insc_est_cliente,
                            w_cid_entr_cobr,
                            w_bairro_entr_cobr,
                            w_end_entr_cobr,
                            w_numero_imovel,
                            w_complemento_endereco,
                            w_cep_entr_cobr
                     from pedi_010, basi_160
                     where pedi_010.cod_cidade   = basi_160.cod_cidade
                       and pedi_010.cgc_9        =  :new.cnpj9_fatura
                       and pedi_010.cgc_4        =  :new.cnpj4_fatura
                       and pedi_010.cgc_2        =  :new.cnpj2_fatura;
                  exception
                  when no_data_found then
                     w_nome_cliente         := ' ';
                     w_insc_est_cliente     := ' ';
                     w_cid_entr_cobr        := ' ';
                     w_bairro_entr_cobr     := ' ';
                     w_end_entr_cobr        := ' ';
                     w_numero_imovel        := ' ';
                     w_complemento_endereco := ' ';
                  end;

                  if :new.cnpj9_fatura > 0
                  then

                     begin
                        insert into fatu_052
                        (cod_empresa,
                         num_nota,
                         cod_serie_nota,
                         cod_mensagem,
                         cnpj9,
                         cnpj4,
                         cnpj2,
                         ind_entr_saida,
                         seq_mensagem,
                         ind_local,
                         des_mensag_1,
                         des_mensag_2,
                         des_mensag_3,
                         des_mensag_4,
                         des_mensag_5)
                        (select :new.codigo_empresa,
                                :new.num_nota_fiscal,
                                :new.serie_nota_fisc,
                                o.cod_mensagem,
                                :new.cgc_9,
                                :new.cgc_4,
                                :new.cgc_2,
                                'S',
                                w_cod_sequencia_entrega,
                                w_cod_local_entrega,
                                substr(o.des_mensagem1 || decode(w_flag_fat_nota_serie,   1, to_char(:new.nota_fatura,'000000000') ||'/'||:new.serie_nota_fisc , 0, '')
                                                       || decode(w_flag_fat_data_emis,    1, ' DT.EMISSAO: ' || to_char(:new.data_emissao,'DD/MM/YY'), 0, '')
                                                       || decode(w_flag_fat_nome_cliente, 1, ' CLIENTE: ' || w_nome_cliente, 0, '')
                                                       || decode(w_flag_fat_cnpj_cliente, 1, ' CNPJ ' || to_char(:new.cnpj9_fatura,'000000000') || '/' ||to_char(:new.cnpj4_fatura,'0000')|| '-' || to_char(:new.cnpj2_fatura,'00'), 0, '')
                                                       || decode(w_flag_fat_insc_estad,   1, ' - INS.EST.: ' || w_insc_est_cliente, 0, '')
                                                       || decode(w_flag_fat_nome_cidade,  1, ' - CIDADE: ' || w_cid_entr_cobr, 0, '')
                                                       || decode(w_flag_fat_bairro,       1, ' - BAIRRO: ' || w_bairro_entr_cobr, 0, '')
                                                       || decode(w_flag_fat_end_nr_compl, 1, ' - END.: ' || w_end_entr_cobr || ', NR.: ' || w_numero_imovel || ', COMPL.: ' || w_complemento_endereco, 0, '')
                                                       || decode(w_flag_fat_cep,          1, ' - CEP: ' || to_char(w_cep_entr_cobr,'00000000'), 0, '') ,1,55),
                                substr(o.des_mensagem1 || decode(w_flag_fat_nota_serie,   1, to_char(:new.nota_fatura,'000000000') ||'/'||:new.serie_nota_fisc , 0, '')
                                                       || decode(w_flag_fat_data_emis,    1, ' DT.EMISSAO: ' || to_char(:new.data_emissao,'DD/MM/YY'), 0, '')
                                                       || decode(w_flag_fat_nome_cliente, 1, ' CLIENTE: ' || w_nome_cliente, 0, '')
                                                       || decode(w_flag_fat_cnpj_cliente, 1, ' CNPJ ' || to_char(:new.cnpj9_fatura,'000000000') || '/' ||to_char(:new.cnpj4_fatura,'0000')|| '-' || to_char(:new.cnpj2_fatura,'00'), 0, '')
                                                       || decode(w_flag_fat_insc_estad,   1, ' - INS.EST.: ' || w_insc_est_cliente, 0, '')
                                                       || decode(w_flag_fat_nome_cidade,  1, ' - CIDADE: ' || w_cid_entr_cobr, 0, '')
                                                       || decode(w_flag_fat_bairro,       1, ' - BAIRRO: ' || w_bairro_entr_cobr, 0, '')
                                                       || decode(w_flag_fat_end_nr_compl, 1, ' - END.: ' || w_end_entr_cobr || ', NR.: ' || w_numero_imovel || ', COMPL.: ' || w_complemento_endereco, 0, '')
                                                       || decode(w_flag_fat_cep,          1, ' - CEP: ' || to_char(w_cep_entr_cobr,'00000000'), 0, '') ,56,55),
                                substr(o.des_mensagem1 || decode(w_flag_fat_nota_serie,   1, to_char(:new.nota_fatura,'000000000') ||'/'||:new.serie_nota_fisc , 0, '')
                                                       || decode(w_flag_fat_data_emis,    1, ' DT.EMISSAO: ' || to_char(:new.data_emissao,'DD/MM/YY'), 0, '')
                                                       || decode(w_flag_fat_nome_cliente, 1, ' CLIENTE: ' || w_nome_cliente, 0, '')
                                                       || decode(w_flag_fat_cnpj_cliente, 1, ' CNPJ ' || to_char(:new.cnpj9_fatura,'000000000') || '/' ||to_char(:new.cnpj4_fatura,'0000')|| '-' || to_char(:new.cnpj2_fatura,'00'), 0, '')
                                                       || decode(w_flag_fat_insc_estad,   1, ' - INS.EST.: ' || w_insc_est_cliente, 0, '')
                                                       || decode(w_flag_fat_nome_cidade,  1, ' - CIDADE: ' || w_cid_entr_cobr, 0, '')
                                                       || decode(w_flag_fat_bairro,       1, ' - BAIRRO: ' || w_bairro_entr_cobr, 0, '')
                                                       || decode(w_flag_fat_end_nr_compl, 1, ' - END.: ' || w_end_entr_cobr || ', NR.: ' || w_numero_imovel || ', COMPL.: ' || w_complemento_endereco, 0, '')
                                                       || decode(w_flag_fat_cep,          1, ' - CEP: ' || to_char(w_cep_entr_cobr,'00000000'), 0, '') ,111,55),
                                substr(o.des_mensagem1 || decode(w_flag_fat_nota_serie,   1, to_char(:new.nota_fatura,'000000000') ||'/'||:new.serie_nota_fisc , 0, '')
                                                       || decode(w_flag_fat_data_emis,    1, ' DT.EMISSAO: ' || to_char(:new.data_emissao,'DD/MM/YY'), 0, '')
                                                       || decode(w_flag_fat_nome_cliente, 1, ' CLIENTE: ' || w_nome_cliente, 0, '')
                                                       || decode(w_flag_fat_cnpj_cliente, 1, ' CNPJ ' || to_char(:new.cnpj9_fatura,'000000000') || '/' ||to_char(:new.cnpj4_fatura,'0000')|| '-' || to_char(:new.cnpj2_fatura,'00'), 0, '')
                                                       || decode(w_flag_fat_insc_estad,   1, ' - INS.EST.: ' || w_insc_est_cliente, 0, '')
                                                       || decode(w_flag_fat_nome_cidade,  1, ' - CIDADE: ' || w_cid_entr_cobr, 0, '')
                                                       || decode(w_flag_fat_bairro,       1, ' - BAIRRO: ' || w_bairro_entr_cobr, 0, '')
                                                       || decode(w_flag_fat_end_nr_compl, 1, ' - END.: ' || w_end_entr_cobr || ', NR.: ' || w_numero_imovel || ', COMPL.: ' || w_complemento_endereco, 0, '')
                                                       || decode(w_flag_fat_cep,          1, ' - CEP: ' || to_char(w_cep_entr_cobr,'00000000'), 0, '') ,166,55),
                                substr(o.des_mensagem1 || decode(w_flag_fat_nota_serie,   1, to_char(:new.nota_fatura,'000000000') ||'/'||:new.serie_nota_fisc , 0, '')
                                                       || decode(w_flag_fat_data_emis,    1, ' DT.EMISSAO: ' || to_char(:new.data_emissao,'DD/MM/YY'), 0, '')
                                                       || decode(w_flag_fat_nome_cliente, 1, ' CLIENTE: ' || w_nome_cliente, 0, '')
                                                       || decode(w_flag_fat_cnpj_cliente, 1, ' CNPJ ' || to_char(:new.cnpj9_fatura,'000000000') || '/' ||to_char(:new.cnpj4_fatura,'0000')|| '-' || to_char(:new.cnpj2_fatura,'00'), 0, '')
                                                       || decode(w_flag_fat_insc_estad,   1, ' - INS.EST.: ' || w_insc_est_cliente, 0, '')
                                                       || decode(w_flag_fat_nome_cidade,  1, ' - CIDADE: ' || w_cid_entr_cobr, 0, '')
                                                       || decode(w_flag_fat_bairro,       1, ' - BAIRRO: ' || w_bairro_entr_cobr, 0, '')
                                                       || decode(w_flag_fat_end_nr_compl, 1, ' - END.: ' || w_end_entr_cobr || ', NR.: ' || w_numero_imovel || ', COMPL.: ' || w_complemento_endereco, 0, '')
                                                       || decode(w_flag_fat_cep,          1, ' - CEP: ' || to_char(w_cep_entr_cobr,'00000000'), 0, '') ,221,55)
                         from obrf_874 o
                         where o.cod_mensagem = w_cod_msg_entrega
                           and w_cod_msg_entrega > 0);
                     exception
                        WHEN dup_val_on_index THEN
                          NULL;
                        when others then
                          raise_application_error(-20000,'Erro ao inserir Mensagem de Fatura' || Chr(10) || SQLERRM);
                     end;
                  end if;
               end if;
             end if;

          end if;

      --Mensagem referente ao Transpor. Redes.

          if :new.natop_nf_est_oper <> 'EX' or w_ind_transp_redes_ex <> 1
          then

             if :new.transp_redespacho9 > 0
             then
               begin

                  begin
                    select basi_160.cidade,           supr_010.nome_fornecedor,
                           basi_160.estado,           supr_010.endereco_forne,
                           supr_010.bairro,           supr_010.numero_imovel,
                           supr_010.inscr_est_forne
                      into w_cidade_rede,             w_transportadora,
                           w_estado_rede,             w_endereco_rede,
                           w_bairro_rede,             w_num_imovel_rede,
                           w_ins_est_rede
                    from supr_010, basi_160
                    where supr_010.cod_cidade  = basi_160.cod_cidade
                      and supr_010.fornecedor9 = :new.transp_redespacho9
                      and supr_010.fornecedor4 = :new.transp_redespacho4
                      and supr_010.fornecedor2 = :new.transp_redespacho2;
                  exception
                    when no_data_found then
                      w_cidade_rede     := ' ';
                      w_transportadora  := ' ';
                      w_estado_rede     := ' ';
                      w_endereco_rede   := ' ';
                      w_bairro_rede     := ' ';
                      w_num_imovel_rede := ' ';
                      w_ins_est_rede    := ' ';
                  end;

                  if(:new.tipo_frete_redes = 1)
                  then
                    w_descr_redespacho := ' Tipo frete redes.: 0 Por conta do Rem. (CIF)';
                  else
                    w_descr_redespacho := ' Tipo frete redes.: 1 Por conta do Dest. (FOB)';
                  end if;

                  insert into fatu_052
                  (cod_empresa,
                   num_nota,
                   cod_serie_nota,
                   cod_mensagem,
                   cnpj9,
                   cnpj4,
                   cnpj2,
                   ind_entr_saida,
                   seq_mensagem,
                   ind_local,
                   des_mensag_1,
                   des_mensag_2,
                   des_mensag_3,
                   des_mensag_4)
                  (select :new.codigo_empresa,
                          :new.num_nota_fiscal,
                          :new.serie_nota_fisc,
                          o.cod_mensagem,
                          :new.cgc_9,
                          :new.cgc_4,
                          :new.cgc_2,
                          'S',
                          w_cod_sequencia_redespacho,
                          w_cod_local_redespacho,
                          substr(o.des_mensagem1 || ' ' ||to_char(:new.transp_redespacho9,'000000000') ||'/'||to_char(:new.transp_redespacho4,'0000')||'-'||to_char(:new.transp_redespacho2,'00')||' ' || w_transportadora || o.des_mensagem2 ||': '|| w_cidade_rede ||' - '|| w_estado_rede ||' - BAIRRO: ' || w_bairro_rede ||' - END.: '|| w_endereco_rede ||' - NR.: '|| w_num_imovel_rede ||' - INS.EST.: '|| w_ins_est_rede || w_descr_redespacho,1,55),
                          substr(o.des_mensagem1 || ' ' ||to_char(:new.transp_redespacho9,'000000000') ||'/'||to_char(:new.transp_redespacho4,'0000')||'-'||to_char(:new.transp_redespacho2,'00')||' ' || w_transportadora || o.des_mensagem2 ||': '|| w_cidade_rede ||' - '|| w_estado_rede ||' - BAIRRO: ' || w_bairro_rede ||' - END.: '|| w_endereco_rede ||' - NR.: '|| w_num_imovel_rede ||' - INS.EST.: '|| w_ins_est_rede || w_descr_redespacho,56,55),
                          substr(o.des_mensagem1 || ' ' ||to_char(:new.transp_redespacho9,'000000000') ||'/'||to_char(:new.transp_redespacho4,'0000')||'-'||to_char(:new.transp_redespacho2,'00')||' ' || w_transportadora || o.des_mensagem2 ||': '|| w_cidade_rede ||' - '|| w_estado_rede ||' - BAIRRO: ' || w_bairro_rede ||' - END.: '|| w_endereco_rede ||' - NR.: '|| w_num_imovel_rede ||' - INS.EST.: '|| w_ins_est_rede || w_descr_redespacho,111,55),
                          substr(o.des_mensagem1 || ' ' ||to_char(:new.transp_redespacho9,'000000000') ||'/'||to_char(:new.transp_redespacho4,'0000')||'-'||to_char(:new.transp_redespacho2,'00')||' ' || w_transportadora || o.des_mensagem2 ||': '|| w_cidade_rede ||' - '|| w_estado_rede ||' - BAIRRO: ' || w_bairro_rede ||' - END.: '|| w_endereco_rede ||' - NR.: '|| w_num_imovel_rede ||' - INS.EST.: '|| w_ins_est_rede || w_descr_redespacho,166,55)
                   from obrf_874 o
                   where o.cod_mensagem = w_cod_msg_redespacho
                     and w_cod_msg_redespacho > 0
                     and w_cidade_rede <> ' '
                     and w_transportadora <> ' ');
               exception
                  WHEN dup_val_on_index THEN
                    NULL;
                  when others then
                    raise_application_error(-20000,'Erro ao inserir Mensagem de Redespacho' || Chr(10) || SQLERRM);
               end;
             end if;

          end if;

          /*Busca dados para verificar se o endereco de entrega eh diferente do endereco de cobranï¿¿ca.

          Esta alteracao adiciona mensagem na nota informando o qual eh o endereco de entrega do destinatario.
          Este procedimento poderah ser realizado somente se a NF estiver sendo enviada para uma da empresa filial,
          caso contrario deve-se utilizar nota triangular

          SS 55795/001 - JOHN*/

          begin
             select pedi_010.nome_cliente,
                    pedi_010.insc_est_cliente,
                    pedi_150.cgc_entr_cobr9,
                    pedi_150.cgc_entr_cobr4,
                    pedi_150.cgc_entr_cobr2,
                    basi_160.cidade || ' - ' || basi_160.estado,
                    pedi_150.bairro_entr_cobr,
                    pedi_150.end_entr_cobr,
                    pedi_150.numero_imovel,
                    pedi_150.complemento_endereco,
                    pedi_150.cep_entr_cobr
               into w_nome_cliente,
                    w_insc_est_cliente,
                    w_cd_cli_cgc_cli9,
                    w_cd_cli_cgc_cli4,
                    w_cd_cli_cgc_cli2,
                    w_cid_entr_cobr,
                    w_bairro_entr_cobr,
                    w_end_entr_cobr,
                    w_numero_imovel,
                    w_complemento_endereco,
                    w_cep_entr_cobr
             from pedi_010, pedi_150, basi_160
             where pedi_150.cd_cli_cgc_cli9 = :new.cgc_9
               and pedi_150.cd_cli_cgc_cli4 = :new.cgc_4
               and pedi_150.cd_cli_cgc_cli2 = :new.cgc_2
               and pedi_150.seq_endereco    = :new.seq_end_entr
               and pedi_150.cid_entr_cobr   = basi_160.cod_cidade
               and pedi_010.cgc_9 =  pedi_150.cgc_entr_cobr9
               and pedi_010.cgc_4 =  pedi_150.cgc_entr_cobr4
               and pedi_010.cgc_2 =  pedi_150.cgc_entr_cobr2;
          exception
          when no_data_found then
               w_nome_cliente         := ' ';
               w_insc_est_cliente     := ' ';
               w_cd_cli_cgc_cli9      := 0;
               w_cd_cli_cgc_cli4      := 0;
               w_cd_cli_cgc_cli2      := 0;
               w_cid_entr_cobr        := ' ';
               w_bairro_entr_cobr     := ' ';
               w_end_entr_cobr        := ' ';
               w_numero_imovel        := ' ';
               w_complemento_endereco := ' ';
               w_cep_entr_cobr        := 0;
          end;

          if :new.cgc_9 = w_cd_cli_cgc_cli9 and :new.cgc_4 <> w_cd_cli_cgc_cli4 and :new.nota_entrega = 0
          then
             begin
               insert into fatu_052
               (cod_empresa,
                num_nota,
                cod_serie_nota,
                cod_mensagem,
                cnpj9,
                cnpj4,
                cnpj2,
                ind_entr_saida,
                seq_mensagem,
                ind_local,
                des_mensag_1,
                des_mensag_2,
                des_mensag_3,
                des_mensag_4,
                des_mensag_5)
               (select :new.codigo_empresa,
                       :new.num_nota_fiscal,
                       :new.serie_nota_fisc,
                       o.cod_mensagem,
                       :new.cgc_9,
                       :new.cgc_4,
                       :new.cgc_2,
                       'S',
                       w_cod_sequencia_simples_rem,
                       w_cod_local_entrega,
                       substr(o.des_mensagem1 || decode(w_flag_ent_nome_cliente, 1, ' LOCAL DE ENTREGA: ' || w_nome_cliente, 0, '')
                                              || decode(w_flag_ent_cnpj_cliente, 1, ' CNPJ ' || to_char(w_cd_cli_cgc_cli9,'000000000') || '/' ||to_char(w_cd_cli_cgc_cli4,'0000')|| '-' || to_char(w_cd_cli_cgc_cli2,'00'), 0, '')
                                              || decode(w_flag_ent_insc_estad,   1, ' - INS.EST.: ' || w_insc_est_cliente, 0, '')
                                              || decode(w_flag_ent_nome_cidade,  1, ' - CIDADE: ' || w_cid_entr_cobr, 0, '')
                                              || decode(w_flag_ent_bairro,       1, ' - BAIRRO: ' || w_bairro_entr_cobr, 0, '')
                                              || decode(w_flag_ent_end_nr_compl, 1, ' - END.: ' || w_end_entr_cobr || ', NR.: ' || w_numero_imovel || ', COMPL.: ' || w_complemento_endereco, 0, '')
                                              || decode(w_flag_ent_cep,          1, ' - CEP: ' || to_char(w_cep_entr_cobr,'00000000'), 0, '') ,1,55),
                       substr(o.des_mensagem1 || decode(w_flag_ent_nome_cliente, 1, ' LOCAL DE ENTREGA: ' || w_nome_cliente, 0, '')                                            || decode(w_flag_ent_cnpj_cliente, 1, ' CNPJ ' || to_char(w_cd_cli_cgc_cli9,'000000000') || '/' ||to_char(w_cd_cli_cgc_cli4,'0000')|| '-' || to_char(w_cd_cli_cgc_cli2,'00'), 0, '')
                                              || decode(w_flag_ent_insc_estad,   1, ' - INS.EST.: ' || w_insc_est_cliente, 0, '')
                                              || decode(w_flag_ent_nome_cidade,  1, ' - CIDADE: ' || w_cid_entr_cobr, 0, '')
                                              || decode(w_flag_ent_bairro,       1, ' - BAIRRO: ' || w_bairro_entr_cobr, 0, '')
                                              || decode(w_flag_ent_end_nr_compl, 1, ' - END.: ' || w_end_entr_cobr || ', NR.: ' || w_numero_imovel || ', COMPL.: ' || w_complemento_endereco, 0, '')
                                              || decode(w_flag_ent_cep,          1, ' - CEP: ' || to_char(w_cep_entr_cobr,'00000000'), 0, '') ,56,55),
                       substr(o.des_mensagem1 || decode(w_flag_ent_nome_cliente, 1, ' LOCAL DE ENTREGA: ' || w_nome_cliente, 0, '')                                            || decode(w_flag_ent_cnpj_cliente, 1, ' CNPJ ' || to_char(w_cd_cli_cgc_cli9,'000000000') || '/' ||to_char(w_cd_cli_cgc_cli4,'0000')|| '-' || to_char(w_cd_cli_cgc_cli2,'00'), 0, '')
                                              || decode(w_flag_ent_insc_estad,   1, ' - INS.EST.: ' || w_insc_est_cliente, 0, '')
                                              || decode(w_flag_ent_nome_cidade,  1, ' - CIDADE: ' || w_cid_entr_cobr, 0, '')
                                              || decode(w_flag_ent_bairro,       1, ' - BAIRRO: ' || w_bairro_entr_cobr, 0, '')
                                              || decode(w_flag_ent_end_nr_compl, 1, ' - END.: ' || w_end_entr_cobr || ', NR.: ' || w_numero_imovel || ', COMPL.: ' || w_complemento_endereco, 0, '')
                                              || decode(w_flag_ent_cep,          1, ' - CEP: ' || to_char(w_cep_entr_cobr,'00000000'), 0, '') ,111,55),
                       substr(o.des_mensagem1 || decode(w_flag_ent_nome_cliente, 1, ' LOCAL DE ENTREGA: ' || w_nome_cliente, 0, '')                                            || decode(w_flag_ent_cnpj_cliente, 1, ' CNPJ ' || to_char(w_cd_cli_cgc_cli9,'000000000') || '/' ||to_char(w_cd_cli_cgc_cli4,'0000')|| '-' || to_char(w_cd_cli_cgc_cli2,'00'), 0, '')
                                              || decode(w_flag_ent_insc_estad,   1, ' - INS.EST.: ' || w_insc_est_cliente, 0, '')
                                              || decode(w_flag_ent_nome_cidade,  1, ' - CIDADE: ' || w_cid_entr_cobr, 0, '')
                                              || decode(w_flag_ent_bairro,       1, ' - BAIRRO: ' || w_bairro_entr_cobr, 0, '')
                                              || decode(w_flag_ent_end_nr_compl, 1, ' - END.: ' || w_end_entr_cobr || ', NR.: ' || w_numero_imovel || ', COMPL.: ' || w_complemento_endereco, 0, '')
                                              || decode(w_flag_ent_cep,          1, ' - CEP: ' || to_char(w_cep_entr_cobr,'00000000'), 0, '') ,166,55),
                       substr(o.des_mensagem1 || decode(w_flag_ent_nome_cliente, 1, ' LOCAL DE ENTREGA: ' || w_nome_cliente, 0, '')                                            || decode(w_flag_ent_cnpj_cliente, 1, ' CNPJ ' || to_char(w_cd_cli_cgc_cli9,'000000000') || '/' ||to_char(w_cd_cli_cgc_cli4,'0000')|| '-' || to_char(w_cd_cli_cgc_cli2,'00'), 0, '')
                                              || decode(w_flag_ent_insc_estad,   1, ' - INS.EST.: ' || w_insc_est_cliente, 0, '')
                                              || decode(w_flag_ent_nome_cidade,  1, ' - CIDADE: ' || w_cid_entr_cobr, 0, '')
                                              || decode(w_flag_ent_bairro,       1, ' - BAIRRO: ' || w_bairro_entr_cobr, 0, '')
                                              || decode(w_flag_ent_end_nr_compl, 1, ' - END.: ' || w_end_entr_cobr || ', NR.: ' || w_numero_imovel || ', COMPL.: ' || w_complemento_endereco, 0, '')
                                              || decode(w_flag_ent_cep,          1, ' - CEP: ' || to_char(w_cep_entr_cobr,'00000000'), 0, '') ,221,55)
                from obrf_874 o
                where o.cod_mensagem = w_cod_msg_simples_rem
                  and w_cod_msg_simples_rem > 0);
             exception
               WHEN dup_val_on_index THEN
                 NULL;
               when others then
                 raise_application_error(-20000,'Erro ao inserir Mensagem de entrega' || Chr(10) || SQLERRM);
             end;
          end if;

      --Mensagem referente ao Suframa

          if :new.valor_itens_nfis > 0.00 and :new.valor_suframa > 0.00
          then
             begin
                select nvl(sum(decode(pedi_080.considera_suframa ,'N',0.00, fatu_060.valor_faturado)),0.00)
                into w_valor_merc_com_suframa
                from fatu_060, pedi_080
                where fatu_060.ch_it_nf_cd_empr  = :new.codigo_empresa
                  and fatu_060.ch_it_nf_num_nfis = :new.num_nota_fiscal
                  and fatu_060.ch_it_nf_ser_nfis = :new.serie_nota_fisc
                  and fatu_060.natopeno_nat_oper = pedi_080.natur_operacao
                  and fatu_060.natopeno_est_oper = pedi_080.estado_natoper;
             end;
             if w_valor_merc_com_suframa > 0
             then
                if :new.tipo_frete = 1
                then
                  w_frete_suframa := nvl(:new.valor_frete_nfis,0);
                else
                  w_frete_suframa := 0.00;
                end if;

                if w_valor_merc_com_suframa is not null
                then
                   w_1 := w_valor_merc_com_suframa;
                else
                   w_1 := 0;
                end if;
                if :new.valor_desconto is not null
                then
                   w_2 := :new.valor_desc_nota;
                else
                   w_2 := 0;
                end if;
                if :new.valor_despesas is not null
                then
                  w_3 := :new.valor_despesas;
                else
                  w_3 := 0;
                end if;
                if :new.valor_seguro is not null
                then
                   w_4 := :new.valor_seguro;
                else
                   w_4 := 0;
                end if;
                if :new.vlr_desc_especial is not null
                then
                   w_5 := :new.vlr_desc_especial;
                else
                   w_5 := 0;
                end if;
                if :new.valor_encar_nota is not null
                then
                   w_6 := :new.valor_encar_nota;
                else
                   w_6 := 0;
                end if;
                w_base_suframa := ( w_1 - w_2 +
                                      w_3    + w_4   +
                                      w_frete_suframa    - w_5 +
                                      w_6);

               /*rafaelsalvador  SS73094.001*/

                if (:new.natop_nf_est_oper <> 'EX' or w_ind_suframa_ex <> 1) and
                   w_cod_msg_suframa > 0 and w_nr_suframa_cli <> ' '
                then

                   if w_ind_desc_pis_cofins = 'N'
                   then
                      w_perc_pis_suframa    := 0.00;
                      w_perc_cofins_suframa := 0.00;
                   end if;

                   if w_ind_desc_icms_ipi = 'N'
                   then
                      w_perc_suframa := 0.00;
                   end if;

                   /*nivel de suframa por cidade.*/

                   if w_suframa_cidade = 0 /*nao ha incentivos para o suframa por cidade.*/
                   then
                      w_perc_pis_suframa    := 0.00;
                      w_perc_cofins_suframa := 0.00;
                      w_perc_suframa        := 0.00;
                   end if;

                   if w_suframa_cidade = 2 /*hah incentivos do SUFRAMA para o ICMS e IPI.*/
                   then
                      w_perc_pis_suframa    := 0.00;
                      w_perc_cofins_suframa := 0.00;
                   end if;

                   if w_suframa_cidade = 3 /* Hah incentivos do SUFRAMA para o PIS e COFINS */
                   then
                      w_perc_suframa  := 0.00;
                   end if;

                   begin
                      insert into fatu_052
                      (cod_empresa,
                       num_nota,
                       cod_serie_nota,
                       cod_mensagem,
                       cnpj9,
                       cnpj4,
                       cnpj2,
                       ind_entr_saida,
                       seq_mensagem,
                       ind_local,
                       des_mensag_1,
                       des_mensag_2,
                       des_mensag_3,
                       des_mensag_4,
                       des_mensag_5)
                      (select :new.codigo_empresa,
                              :new.num_nota_fiscal,
                              :new.serie_nota_fisc,
                              o.cod_mensagem,
                              :new.cgc_9,
                              :new.cgc_4,
                              :new.cgc_2,
                              'S',
                              w_cod_sequencia_suframa,
                              w_cod_local_suframa,
                              substr(o.des_mensagem1 || '  ' || 'BASE ICMS: R$' || ' ' ||to_char(w_base_suframa,'999990.00') || ' DESC: ' || to_char(w_perc_suframa,'90.00') ||' '|| ' % ICMS DESONERADO: R$ ' || to_char(((w_base_suframa) * w_perc_suframa)  / 100.00,'9999990.00') || ' - ' || w_perc_pis_suframa || '% PIS: R$ ' || to_char(((w_base_suframa) *  w_perc_pis_suframa)  / 100.00,'9999990.00')|| ' - ' ||    w_perc_cofins_suframa ||'% COFINS: R$ ' || to_char(((w_base_suframa) * w_perc_cofins_suframa)  / 100.00,'9999990.00') || ' TOT.MERC.: R$ ' || to_char(w_valor_merc_com_suframa,'9999999990.00') || ' DESC.SUFRAMA: R$ ' || to_char(:new.valor_suframa,'9999990.00') || ' SUFRAMA: '|| w_nr_suframa_cli,1,49),
                              substr(o.des_mensagem1 || '  ' || 'BASE ICMS: R$' || ' ' ||to_char(w_base_suframa,'999990.00') || ' DESC: ' || to_char(w_perc_suframa,'90.00') ||' '|| ' % ICMS DESONERADO: R$ ' || to_char(((w_base_suframa) * w_perc_suframa)  / 100.00,'9999990.00') || ' - ' || w_perc_pis_suframa || '% PIS: R$ ' || to_char(((w_base_suframa) *  w_perc_pis_suframa)  / 100.00,'9999990.00')|| ' - ' ||    w_perc_cofins_suframa ||'% COFINS: R$ ' || to_char(((w_base_suframa) * w_perc_cofins_suframa)  / 100.00,'9999990.00') || ' TOT.MERC.: R$ ' || to_char(w_valor_merc_com_suframa,'9999999990.00') || ' DESC.SUFRAMA: R$ ' || to_char(:new.valor_suframa,'9999990.00') || ' SUFRAMA: '|| w_nr_suframa_cli,50,50),
                              substr(o.des_mensagem1 || '  ' || 'BASE ICMS: R$' || ' ' ||to_char(w_base_suframa,'999990.00') || ' DESC: ' || to_char(w_perc_suframa,'90.00') ||' '|| ' % ICMS DESONERADO: R$ ' || to_char(((w_base_suframa) * w_perc_suframa)  / 100.00,'9999990.00') || ' - ' || w_perc_pis_suframa || '% PIS: R$ ' || to_char(((w_base_suframa) *  w_perc_pis_suframa)  / 100.00,'9999990.00')|| ' - ' ||    w_perc_cofins_suframa ||'% COFINS: R$ ' || to_char(((w_base_suframa) * w_perc_cofins_suframa)  / 100.00,'9999990.00') || ' TOT.MERC.: R$ ' || to_char(w_valor_merc_com_suframa,'9999999990.00') || ' DESC.SUFRAMA: R$ ' || to_char(:new.valor_suframa,'9999990.00') || ' SUFRAMA: '|| w_nr_suframa_cli,100,50),
                              substr(o.des_mensagem1 || '  ' || 'BASE ICMS: R$' || ' ' ||to_char(w_base_suframa,'999990.00') || ' DESC: ' || to_char(w_perc_suframa,'90.00') ||' '|| ' % ICMS DESONERADO: R$ ' || to_char(((w_base_suframa) * w_perc_suframa)  / 100.00,'9999990.00') || ' - ' || w_perc_pis_suframa || '% PIS: R$ ' || to_char(((w_base_suframa) *  w_perc_pis_suframa)  / 100.00,'9999990.00')|| ' - ' ||    w_perc_cofins_suframa ||'% COFINS: R$ ' || to_char(((w_base_suframa) * w_perc_cofins_suframa)  / 100.00,'9999990.00') || ' TOT.MERC.: R$ ' || to_char(w_valor_merc_com_suframa,'9999999990.00') || ' DESC.SUFRAMA: R$ ' || to_char(:new.valor_suframa,'9999990.00') || ' SUFRAMA: '|| w_nr_suframa_cli,150,50),
                              substr(o.des_mensagem1 || '  ' || 'BASE ICMS: R$' || ' ' ||to_char(w_base_suframa,'999990.00') || ' DESC: ' || to_char(w_perc_suframa,'90.00') ||' '|| ' % ICMS DESONERADO: R$ ' || to_char(((w_base_suframa) * w_perc_suframa)  / 100.00,'9999990.00') || ' - ' || w_perc_pis_suframa || '% PIS: R$ ' || to_char(((w_base_suframa) *  w_perc_pis_suframa)  / 100.00,'9999990.00')|| ' - ' ||    w_perc_cofins_suframa ||'% COFINS: R$ ' || to_char(((w_base_suframa) * w_perc_cofins_suframa)  / 100.00,'9999990.00') || ' TOT.MERC.: R$ ' || to_char(w_valor_merc_com_suframa,'9999999990.00') || ' DESC.SUFRAMA: R$ ' || to_char(:new.valor_suframa,'9999990.00') || ' SUFRAMA: '|| w_nr_suframa_cli,200,50)
                       from obrf_874 o
                       where o.cod_mensagem = w_cod_msg_suframa
                         and w_cod_msg_suframa > 0
                         and w_nr_suframa_cli <> ' ');
                   exception
                      WHEN dup_val_on_index THEN
                      NULL;

                     when others then
                      raise_application_error(-20000,'Erro ao inserir Mensagem do Suframa' || Chr(10) || SQLERRM);
                   end;

                end if;
             end if;

          end if;

      --  mensagem do simples nacional

          if :new.natop_nf_est_oper <> 'EX' or w_ind_simples <> 1 and w_cod_mensagem_simples > 0 and
             :new.valor_icms > 0.00 and w_ind_empresa_simples = '1'
          then

             begin
                insert into fatu_052
                (cod_empresa,
                 num_nota,
                 cod_serie_nota,
                 cod_mensagem,
                 cnpj9,
                 cnpj4,
                 cnpj2,
                 ind_entr_saida,
                 seq_mensagem,
                 ind_local,
                 des_mensag_1,
                 des_mensag_2,
                 des_mensag_3)
                (select :new.codigo_empresa,
                        :new.num_nota_fiscal,
                        :new.serie_nota_fisc,
                        o.cod_mensagem,
                        :new.cgc_9,
                        :new.cgc_4,
                        :new.cgc_2,
                        'S',
                        w_cod_sequencia_simples,
                        w_cod_local_simples,
                        substr('PERMITE O APROVEITAMENTO DO CREDITO DE ICMS NO VALOR DE R$ ' || ' : ' || to_char(:new.valor_icms,'999999999990.00') ||
                               '; CORRESPONDENTE A ALIQUOTA DE ' ||to_char(w_perc_icms,'90.00') || '%, NOS TERMOS DO ARTIGO 23 DA LC 123' ,1,55),
                        substr('PERMITE O APROVEITAMENTO DO CREDITO DE ICMS NO VALOR DE R$ ' || ' : ' || to_char(:new.valor_icms,'999999999990.00') ||
                               ': CORRESPONDENTE A ALIQUOTA DE ' ||to_char(w_perc_icms,'90.00') || '%, NOS TERMOS DO ARTIGO 23 DA LC 123' ,56,55),
                        substr('PERMITE O APROVEITAMENTO DO CREDITO DE ICMS NO VALOR DE R$ ' || ' : ' || to_char(:new.valor_icms,'999999999990.00') ||
                               ': CORRESPONDENTE A ALIQUOTA DE ' ||to_char(w_perc_icms,'90.00') || '%, NOS TERMOS DO ARTIGO 23 DA LC 123' ,111,55)
                 from obrf_874 o
                 where o.cod_mensagem = w_cod_mensagem_simples
                   and w_cod_mensagem_simples > 0
                   and :new.valor_icms > 0.00
                   and w_ind_empresa_simples = '1');
             exception
                WHEN dup_val_on_index THEN
                NULL;

               when others then
                raise_application_error(-20000,'Erro ao inserir Mensagem do simples nacional' || Chr(10) || SQLERRM);
             end;

          end if;

         --Mensagem referente Notas de devolucao (Retorno de marcadorias)

          if (:new.natop_nf_est_oper <> 'EX' or w_ind_nf_devol <> 1) and  :new.valor_itens_nfis > 0.00 and
             (w_cod_mensagem_nf_devol > 0 or w_cod_mensagem_dev_rol > 0 or w_cod_mensagem_nf_dev > 0)
          then

              w_len_msg := 300;

              if w_cod_local_nf_devol = 'C'
              then
                 w_len_msg := 2000;
              end if;

              select pedi_010.nome_cliente, pedi_010.insc_est_cliente
                into w_nome_cliente,        w_inscricao_cliente
              from pedi_010
              where pedi_010.cgc_9 = :new.cgc_9
                and pedi_010.cgc_4 = :new.cgc_4
                and pedi_010.cgc_2 = :new.cgc_2;

              w_teve_dev := false;

              w_notas_devolvidas := ' ';
              w_notas_devolvidas_rol := ' ';

             FOR nf_devolucao
             IN (
             select fatu_060.num_nota_orig nf_devol, fatu_060.serie_nota_orig serie_devol,
                    obrf_010.data_emissao data_devol,
                    sum(fatu_060.valor_contabil) valor_nf_devol
             from fatu_060,
                  obrf_010,
                  estq_005,
                  fatu_505
             where fatu_060.ch_it_nf_cd_empr   = :new.codigo_empresa
               and fatu_060.ch_it_nf_num_nfis  = :new.num_nota_fiscal
               and fatu_060.ch_it_nf_ser_nfis  = :new.serie_nota_fisc
               and fatu_060.num_nota_orig      > 0
               and fatu_060.transacao          = estq_005.codigo_transacao
               and (estq_005.tipo_transacao    in ('I','R') or fatu_060.nf_invet_fiscal = 'S')
               and obrf_010.documento          = fatu_060.num_nota_orig
               and obrf_010.serie              = fatu_060.serie_nota_orig
               and obrf_010.cgc_cli_for_9      = fatu_060.cgc9_origem
               and obrf_010.cgc_cli_for_4      = fatu_060.cgc4_origem
               and obrf_010.cgc_cli_for_2      = fatu_060.cgc2_origem
               and fatu_060.ch_it_nf_cd_empr   = fatu_505.codigo_empresa
               and fatu_060.ch_it_nf_ser_nfis  = fatu_505.serie_nota_fisc
               and fatu_060.valor_contabil     > 0.00
               and fatu_505.serie_bcm          = 'N'
             group by obrf_010.data_emissao,fatu_060.num_nota_orig, fatu_060.serie_nota_orig
             order by obrf_010.data_emissao,fatu_060.num_nota_orig, fatu_060.serie_nota_orig)
             LOOP
                if (w_teve_dev = false) /*somente pra primeira nota  aqui*/
                then

                   if w_cod_mensagem_nf_dev > 0
                   then
                      w_notas_devolvidas := w_nome_cliente || to_char(:new.cgc_9,'000000000') || '/' ||
                      to_char(:new.cgc_4,'0000')|| '-' || to_char(:new.cgc_2,'00') || ' REF NF(s) ';
                   end if;


                   w_notas_devolvidas_rol := w_nome_cliente ||
                                             to_char(:new.cgc_9,'000000000') || '/' ||to_char(:new.cgc_4,'0000')|| '-' || to_char(:new.cgc_2,'00') ||
                                             ' IE nr ' || w_inscricao_cliente||
                                             ' Referente as NF nr ';
                end if;




                w_teve_dev := true;

                if (trim(w_notas_devolvidas) is null or length(trim(w_notas_devolvidas)) < w_len_msg)
                then

                   w_notas_devolvidas_rol_nota := w_notas_devolvidas_rol_nota ||
                                               to_char(nf_devolucao.nf_devol, '000000000') || ' / ';

                   if w_cod_mensagem_nf_dev > 0
                   then
                      w_notas_devolvidas := w_notas_devolvidas ||
                                            to_char(nf_devolucao.nf_devol, '000000000') || '-' ||
                                            nf_devolucao.serie_devol || ' ' ||
                                            to_char(nf_devolucao.data_devol,'DD/MM/YY');
                   else
                      w_notas_devolvidas := w_notas_devolvidas ||
                                            to_char(nf_devolucao.nf_devol, '000000000') || '-' ||
                                            nf_devolucao.serie_devol || ' ' ||
                                            to_char(nf_devolucao.data_devol,'DD/MM/YY') || ' R$ ' ||
                                            ltrim(to_char(nf_devolucao.valor_nf_devol,'9999999990.00')) || ' / ';
                   end if;
                end if;
             END LOOP;

             w_notas_devolvidas_rol := w_notas_devolvidas_rol || ' ' || w_notas_devolvidas_rol_nota;

             if trim(w_notas_devolvidas) is not null and w_cod_mensagem_nf_devol > 0
             then

                if w_cod_local_nf_devol = 'C'
                then
                   begin
                      insert into fatu_052
                      (cod_empresa,
                       num_nota,
                       cod_serie_nota,
                       cod_mensagem,
                       cnpj9,
                       cnpj4,
                       cnpj2,
                       ind_entr_saida,
                       seq_mensagem,
                       ind_local,
                       des_mensag_1,
                       des_mensag_2,
                       des_mensag_3,
                       des_mensag_4,
                       des_mensag_5,
                       des_mensag_6,
                       des_mensag_7,
                       des_mensag_8,
                       des_mensag_9,
                       des_mensag_10,
                       des_mensag_11,
                       des_mensag_12)
                      (select :new.codigo_empresa,
                              :new.num_nota_fiscal,
                              :new.serie_nota_fisc,
                              o.cod_mensagem,
                              :new.cgc_9,
                              :new.cgc_4,
                              :new.cgc_2,
                              'S',
                              w_cod_sequencia_nf_devol,
                              w_cod_local_nf_devol,
                              substr(o.des_mensagem1 || ' ' || w_notas_devolvidas ,1,55),
                              substr(o.des_mensagem1 || ' ' || w_notas_devolvidas ,56,55),
                              substr(o.des_mensagem1 || ' ' || w_notas_devolvidas ,111,55),
                              substr(o.des_mensagem1 || ' ' || w_notas_devolvidas ,166,55),
                              substr(o.des_mensagem1 || ' ' || w_notas_devolvidas ,221,55),
                              substr(o.des_mensagem1 || ' ' || w_notas_devolvidas ,276,55),
                              substr(o.des_mensagem1 || ' ' || w_notas_devolvidas ,331,55),
                              substr(o.des_mensagem1 || ' ' || w_notas_devolvidas ,386,55),
                              substr(o.des_mensagem1 || ' ' || w_notas_devolvidas ,441,55),
                              substr(o.des_mensagem1 || ' ' || w_notas_devolvidas ,496,55),
                              substr(o.des_mensagem1 || ' ' || w_notas_devolvidas ,551,725),
                              substr(o.des_mensagem1 || ' ' || w_notas_devolvidas ,1276,725)
                       from obrf_874 o
                       where o.cod_mensagem = w_cod_mensagem_nf_devol);
                   exception
                      WHEN dup_val_on_index THEN
                      NULL;

                     when others then
                      raise_application_error(-20000,'Erro ao inserir Mensagem de notas de devolucao' || Chr(10) || SQLERRM);
                   end;
                else

                   begin
                      insert into fatu_052
                      (cod_empresa,
                       num_nota,
                       cod_serie_nota,
                       cod_mensagem,
                       cnpj9,
                       cnpj4,
                       cnpj2,
                       ind_entr_saida,
                       seq_mensagem,
                       ind_local,
                       des_mensag_1,
                       des_mensag_2,
                       des_mensag_3,
                       des_mensag_4,
                       des_mensag_5)
                      (select :new.codigo_empresa,
                              :new.num_nota_fiscal,
                              :new.serie_nota_fisc,
                              o.cod_mensagem,
                              :new.cgc_9,
                              :new.cgc_4,
                              :new.cgc_2,
                              'S',
                              w_cod_sequencia_nf_devol,
                              w_cod_local_nf_devol,
                              substr(o.des_mensagem1 || ' ' || w_notas_devolvidas ,1,55),
                              substr(o.des_mensagem1 || ' ' || w_notas_devolvidas ,56,55),
                              substr(o.des_mensagem1 || ' ' || w_notas_devolvidas ,111,55),
                              substr(o.des_mensagem1 || ' ' || w_notas_devolvidas ,166,55),
                              substr(o.des_mensagem1 || ' ' || w_notas_devolvidas ,221,55)
                       from obrf_874 o
                       where o.cod_mensagem = w_cod_mensagem_nf_devol);
                   exception
                      WHEN dup_val_on_index THEN
                      NULL;

                     when others then
                      raise_application_error(-20000,'Erro ao inserir Mensagem de notas de devolucao' || Chr(10) || SQLERRM);
                   end;
                end if;

             end if;

             if trim(w_notas_devolvidas) is not null and w_cod_mensagem_nf_dev > 0 and w_teve_dev = true
             then

                if w_cod_local_nf_dev = 'C'
                then
                   begin
                      insert into fatu_052
                      (cod_empresa,
                       num_nota,
                       cod_serie_nota,
                       cod_mensagem,
                       cnpj9,
                       cnpj4,
                       cnpj2,
                       ind_entr_saida,
                       seq_mensagem,
                       ind_local,
                       des_mensag_1,
                       des_mensag_2,
                       des_mensag_3,
                       des_mensag_4,
                       des_mensag_5,
                       des_mensag_6,
                       des_mensag_7,
                       des_mensag_8,
                       des_mensag_9,
                       des_mensag_10,
                       des_mensag_11,
                       des_mensag_12)
                      (select :new.codigo_empresa,
                              :new.num_nota_fiscal,
                              :new.serie_nota_fisc,
                              o.cod_mensagem,
                              :new.cgc_9,
                              :new.cgc_4,
                              :new.cgc_2,
                              'S',
                              w_cod_sequencia_nf_dev,
                              w_cod_local_nf_dev,
                              substr(o.des_mensagem1 || ' ' || w_notas_devolvidas ,1,55),
                              substr(o.des_mensagem1 || ' ' || w_notas_devolvidas ,56,55),
                              substr(o.des_mensagem1 || ' ' || w_notas_devolvidas ,111,55),
                              substr(o.des_mensagem1 || ' ' || w_notas_devolvidas ,166,55),
                              substr(o.des_mensagem1 || ' ' || w_notas_devolvidas ,221,55),
                              substr(o.des_mensagem1 || ' ' || w_notas_devolvidas ,276,55),
                              substr(o.des_mensagem1 || ' ' || w_notas_devolvidas ,331,55),
                              substr(o.des_mensagem1 || ' ' || w_notas_devolvidas ,386,55),
                              substr(o.des_mensagem1 || ' ' || w_notas_devolvidas ,441,55),
                              substr(o.des_mensagem1 || ' ' || w_notas_devolvidas ,496,55),
                              substr(o.des_mensagem1 || ' ' || w_notas_devolvidas ,551,725),
                              substr(o.des_mensagem1 || ' ' || w_notas_devolvidas ,1276,725)
                       from obrf_874 o
                       where o.cod_mensagem = w_cod_mensagem_nf_dev);
                   exception
                      WHEN dup_val_on_index THEN
                      NULL;

                     when others then
                      raise_application_error(-20000,'Erro ao inserir Mensagem de notas de devolucao' || Chr(10) || SQLERRM);
                   end;
                else

                   begin
                      insert into fatu_052
                      (cod_empresa,
                       num_nota,
                       cod_serie_nota,
                       cod_mensagem,
                       cnpj9,
                       cnpj4,
                       cnpj2,
                       ind_entr_saida,
                       seq_mensagem,
                       ind_local,
                       des_mensag_1,
                       des_mensag_2,
                       des_mensag_3,
                       des_mensag_4,
                       des_mensag_5)
                      (select :new.codigo_empresa,
                              :new.num_nota_fiscal,
                              :new.serie_nota_fisc,
                              o.cod_mensagem,
                              :new.cgc_9,
                              :new.cgc_4,
                              :new.cgc_2,
                              'S',
                              w_cod_sequencia_nf_dev,
                              w_cod_local_nf_dev,
                              substr(o.des_mensagem1 || ' ' || w_notas_devolvidas ,1,55),
                              substr(o.des_mensagem1 || ' ' || w_notas_devolvidas ,56,55),
                              substr(o.des_mensagem1 || ' ' || w_notas_devolvidas ,111,55),
                              substr(o.des_mensagem1 || ' ' || w_notas_devolvidas ,166,55),
                              substr(o.des_mensagem1 || ' ' || w_notas_devolvidas ,221,55)
                       from obrf_874 o
                       where o.cod_mensagem = w_cod_mensagem_nf_dev);
                   exception
                      WHEN dup_val_on_index THEN
                      NULL;

                     when others then
                      raise_application_error(-20000,'Erro ao inserir Mensagem de notas de devolucao' || Chr(10) || SQLERRM);
                   end;
                end if;

             end if;

             if w_notas_devolvidas_rol_nota is not null and w_cod_mensagem_dev_rol > 0
             then
                if w_cod_local_dev_rol = 'C'
                then
                   begin
                      insert into fatu_052
                      (cod_empresa,
                       num_nota,
                       cod_serie_nota,
                       cod_mensagem,
                       cnpj9,
                       cnpj4,
                       cnpj2,
                       ind_entr_saida,
                       seq_mensagem,
                       ind_local,
                       des_mensag_1,
                       des_mensag_2,
                       des_mensag_3,
                       des_mensag_4,
                       des_mensag_5,
                       des_mensag_6,
                       des_mensag_7,
                       des_mensag_8,
                       des_mensag_9,
                       des_mensag_10,
                       des_mensag_11,
                       des_mensag_12)
                      (select :new.codigo_empresa,
                              :new.num_nota_fiscal,
                              :new.serie_nota_fisc,
                              o.cod_mensagem,
                              :new.cgc_9,
                              :new.cgc_4,
                              :new.cgc_2,
                              'S',
                              w_cod_sequencia_dev_rol,
                              w_cod_local_dev_rol,
                              substr(o.des_mensagem1 || ' ' || w_notas_devolvidas_rol ,1,55),
                              substr(o.des_mensagem1 || ' ' || w_notas_devolvidas_rol ,56,55),
                              substr(o.des_mensagem1 || ' ' || w_notas_devolvidas_rol ,111,55),
                              substr(o.des_mensagem1 || ' ' || w_notas_devolvidas_rol ,166,55),
                              substr(o.des_mensagem1 || ' ' || w_notas_devolvidas_rol ,221,55),
                              substr(o.des_mensagem1 || ' ' || w_notas_devolvidas_rol ,276,55),
                              substr(o.des_mensagem1 || ' ' || w_notas_devolvidas_rol ,331,55),
                              substr(o.des_mensagem1 || ' ' || w_notas_devolvidas_rol ,386,55),
                              substr(o.des_mensagem1 || ' ' || w_notas_devolvidas_rol ,441,55),
                              substr(o.des_mensagem1 || ' ' || w_notas_devolvidas_rol ,496,55),
                              substr(o.des_mensagem1 || ' ' || w_notas_devolvidas_rol ,551,725),
                              substr(o.des_mensagem1 || ' ' || w_notas_devolvidas_rol ,1276,725)
                       from obrf_874 o
                       where o.cod_mensagem = w_cod_mensagem_dev_rol);
                   exception
                      WHEN dup_val_on_index THEN
                      NULL;

                     when others then
                      raise_application_error(-20000,'Erro ao inserir Mensagem de notas de devolucao' || Chr(10) || SQLERRM);
                   end;
                else
                   begin
                      insert into fatu_052
                      (cod_empresa,
                       num_nota,
                       cod_serie_nota,
                       cod_mensagem,
                       cnpj9,
                       cnpj4,
                       cnpj2,
                       ind_entr_saida,
                       seq_mensagem,
                       ind_local,
                       des_mensag_1,
                       des_mensag_2,
                       des_mensag_3,
                       des_mensag_4,
                       des_mensag_5)
                      (select :new.codigo_empresa,
                              :new.num_nota_fiscal,
                              :new.serie_nota_fisc,
                              o.cod_mensagem,
                              :new.cgc_9,
                              :new.cgc_4,
                              :new.cgc_2,
                              'S',
                              w_cod_sequencia_dev_rol,
                              w_cod_local_dev_rol,
                              substr(o.des_mensagem1 || ' ' || w_notas_devolvidas_rol ,1,55),
                              substr(o.des_mensagem1 || ' ' || w_notas_devolvidas_rol ,56,55),
                              substr(o.des_mensagem1 || ' ' || w_notas_devolvidas_rol ,111,55),
                              substr(o.des_mensagem1 || ' ' || w_notas_devolvidas_rol ,166,55),
                              substr(o.des_mensagem1 || ' ' || w_notas_devolvidas_rol ,221,55)
                       from obrf_874 o
                       where o.cod_mensagem = w_cod_mensagem_dev_rol);
                   exception
                      WHEN dup_val_on_index THEN
                      NULL;

                     when others then
                      raise_application_error(-20000,'Erro ao inserir Mensagem de notas de devolucao' || Chr(10) || SQLERRM);
                   end;
                end if;
             end if;
          end if;

      --Mensagem referente Notas de devolucao com perdas no processo (Retorno de marcadorias)
      --Somente notas de devolucao da Florisa

          if w_verifica_empresa = 1 and w_tipo_natureza > 0
          then
             begin
                select pedi_150.gera_nota_entreg
                into w_gera_nota_entreg
                from pedi_150
                where pedi_150.cd_cli_cgc_cli9 = :new.cgc_9
                  and pedi_150.cd_cli_cgc_cli4 = :new.cgc_4
                  and pedi_150.cd_cli_cgc_cli2 = :new.cgc_2
                  and pedi_150.seq_endereco    = :new.seq_end_entr
                  and pedi_150.tipo_endereco   = 1;

                exception
                when no_data_found then
                   w_gera_nota_entreg := 0;
             end;

             if (:new.natop_nf_est_oper <> 'EX' or w_ind_nf_devol_perdas <> 1)
             and :new.valor_itens_nfis > 0.00
             and w_cod_mensagem_nf_devol_perdas > 0
             and w_opcao_impres_nf_devoluc = 1
             and w_gera_nota_entreg  = 3
             then

                w_perda_total := 0;

                FOR itens_pedido
                IN (
                select pedi_112.ped_facc_pe_ve_it, pedi_112.ped_facc_sq_it_pe
                from fatu_060,
                     obrf_010,
                     estq_005,
                     estq_360,
                     pedi_112
                where fatu_060.num_nota_orig      = obrf_010.documento
                  and fatu_060.serie_nota_orig    = obrf_010.serie
                  and fatu_060.cgc9_origem        = obrf_010.responsavel9
                  and fatu_060.cgc4_origem        = obrf_010.responsavel4
                  and fatu_060.cgc2_origem        = obrf_010.responsavel2
                  and fatu_060.transacao          = estq_005.codigo_transacao
                  and estq_005.tipo_transacao     in ('I','R')
                  and estq_360.documento          = obrf_010.doc_conta_corrente
                  and estq_360.nota_entrada       = obrf_010.documento
                  and estq_360.serie_nota_entrada = obrf_010.serie
                  and estq_360.seq_nota_entrada   = fatu_060.seq_nota_orig
                  and pedi_112.nota_entrada       = estq_360.documento
                  and pedi_112.seq_nf_entrada     = estq_360.seq_lancamento
                  and fatu_060.ch_it_nf_cd_empr   = :new.codigo_empresa
                  and fatu_060.ch_it_nf_num_nfis  = :new.num_nota_fiscal
                  and fatu_060.ch_it_nf_ser_nfis  = :new.serie_nota_fisc
                  and fatu_060.num_nota_orig      > 0)
                LOOP
                   begin
                      select sum(pcpb_020.qtde_quilos_real) - sum(pcpb_020.qtde_quilos_prod)
                      into   w_qtde_perda
                      from pcpb_030, pcpb_020
                      where pcpb_030.pano_nivel99     = pcpb_020.pano_sbg_nivel99
                        and pcpb_030.pano_grupo       = pcpb_020.pano_sbg_grupo
                        and pcpb_030.pano_subgrupo    = pcpb_020.pano_sbg_subgrupo
                        and pcpb_030.pano_item        = pcpb_020.pano_sbg_item
                        and pcpb_030.sequencia        = pcpb_020.sequencia
                        and pcpb_030.ordem_producao   = pcpb_020.ordem_producao
                        and pcpb_030.pedido_corte     = 3
                        and pcpb_030.nr_pedido_ordem  = itens_pedido.ped_facc_pe_ve_it
                        and pcpb_030.sequenci_periodo = itens_pedido.ped_facc_sq_it_pe;

                      exception
                      when others then
                         w_qtde_perda := 0;

                   end;

                   w_perda_total := w_perda_total + w_qtde_perda;

                END LOOP;

                if w_perda_total > 0
                then
                   begin
                      insert into fatu_052
                      (cod_empresa,
                       num_nota,
                       cod_serie_nota,
                       cod_mensagem,
                       cnpj9,
                       cnpj4,
                       cnpj2,
                       ind_entr_saida,
                       seq_mensagem,
                       ind_local,
                       des_mensag_1,
                       des_mensag_2,
                       des_mensag_3,
                       des_mensag_4,
                       des_mensag_5)
                      (select :new.codigo_empresa,
                              :new.num_nota_fiscal,
                              :new.serie_nota_fisc,
                              o.cod_mensagem,
                              :new.cgc_9,
                              :new.cgc_4,
                              :new.cgc_2,
                              'S',
                              w_cod_seq_nf_devol_perdas,
                              w_cod_local_nf_devol_perdas,
                              substr(o.des_mensagem1 || ' ' || to_char(w_perda_total,'9999990.000') ,1,55),
                              substr(o.des_mensagem1 || ' ' || to_char(w_perda_total,'9999990.000') ,56,55),
                              substr(o.des_mensagem1 || ' ' || to_char(w_perda_total,'9999990.000') ,111,55),
                              substr(o.des_mensagem1 || ' ' || to_char(w_perda_total,'9999990.000') ,166,55),
                              substr(o.des_mensagem1 || ' ' || to_char(w_perda_total,'9999990.000') ,221,55)
                       from obrf_874 o
                       where o.cod_mensagem = w_cod_mensagem_nf_devol_perdas);
                      exception

                      WHEN dup_val_on_index THEN
                      NULL;

                     when others then
                      raise_application_error(-20000,'Erro ao inserir Mensagem de notas de devolucao com perdas' || Chr(10) || SQLERRM);
                   end;

                end if;
             end if;
          end if;

      -- Mensagm as notas de Remessa Simbolica, utilizada pela empresa ABL, para emitir notas para a Lunender e Lunelli.
      --

          if :new.valor_itens_nfis > 0.00 and w_cod_mensagem_rem_simb > 0
          then
             w_notas_entrada := '';
             FOR nf_entrada
             IN (
             select fatu_060.num_nota_orig nf_ent, fatu_060.serie_nota_orig serie_ent
             from fatu_060,
                  estq_005,
                  fatu_505
             where fatu_060.ch_it_nf_cd_empr   = :new.codigo_empresa
               and fatu_060.ch_it_nf_num_nfis  = :new.num_nota_fiscal
               and fatu_060.ch_it_nf_ser_nfis  = :new.serie_nota_fisc
               and fatu_060.num_nota_orig      > 0
               and fatu_060.transacao          = estq_005.codigo_transacao
               and estq_005.tipo_transacao     = 'R'
               and fatu_060.ch_it_nf_cd_empr   = fatu_505.codigo_empresa
               and fatu_060.ch_it_nf_ser_nfis  = fatu_505.serie_nota_fisc
               and fatu_505.serie_bcm          = 'N'
             group by fatu_060.num_nota_orig, fatu_060.serie_nota_orig
             order by fatu_060.num_nota_orig, fatu_060.serie_nota_orig)
             LOOP
                w_notas_entrada := w_notas_entrada ||
                                   to_char(nf_entrada.nf_ent, '000000') || '-' ||
                                           nf_entrada.serie_ent || ' / ';
             END LOOP;

             if w_notas_entrada is not null
             then
                begin
                   insert into fatu_052
                   (cod_empresa,
                    num_nota,
                    cod_serie_nota,
                    cod_mensagem,
                    cnpj9,
                    cnpj4,
                    cnpj2,
                    ind_entr_saida,
                    seq_mensagem,
                    ind_local,
                    des_mensag_1,
                    des_mensag_2,
                    des_mensag_3,
                    des_mensag_4,
                    des_mensag_5)
                   (select :new.codigo_empresa,
                           :new.num_nota_fiscal,
                           :new.serie_nota_fisc,
                           o.cod_mensagem,
                           :new.cgc_9,
                           :new.cgc_4,
                           :new.cgc_2,
                           'S',
                           w_cod_sequencia_rem_simb,
                           w_cod_local_rem_simb,
                           substr(o.des_mensagem1 || ' ' || w_notas_entrada ,1,55),
                           substr(o.des_mensagem1 || ' ' || w_notas_entrada ,56,55),
                           substr(o.des_mensagem1 || ' ' || w_notas_entrada ,111,55),
                           substr(o.des_mensagem1 || ' ' || w_notas_entrada ,166,55),
                           substr(o.des_mensagem1 || ' ' || w_notas_entrada ,221,55)
                    from obrf_874 o
                    where o.cod_mensagem = w_cod_mensagem_rem_simb);
                exception
                   WHEN dup_val_on_index THEN
                   NULL;

                  when others then
                   raise_application_error(-20000,'Erro ao inserir Mensagem de notas de remessa simbolica' || Chr(10) || SQLERRM);
                end;
             end if;
          end if;


       --Mensagem referente aos descontos

          if (w_cod_mensagem_desc > 0) and (:new.desconto1 > 0 or :new.desconto2 > 0 or :new.desconto3 > 0)
          then

             begin
                insert into fatu_052
                (cod_empresa,
                 num_nota,
                 cod_serie_nota,
                 cod_mensagem,
                 cnpj9,
                 cnpj4,
                 cnpj2,
                 ind_entr_saida,
                 seq_mensagem,
                 ind_local,
                 des_mensag_1,
                 des_mensag_2,
                 des_mensag_3,
                 des_mensag_4,
                 des_mensag_5)
                (select :new.codigo_empresa,
                        :new.num_nota_fiscal,
                        :new.serie_nota_fisc,
                        o.cod_mensagem,
                        :new.cgc_9,
                        :new.cgc_4,
                        :new.cgc_2,
                        'S',
                        w_cod_sequencia_desc,
                        w_cod_local_desc,
                        substr(o.des_mensagem1 || ' : ' || to_char(:new.desconto1,'90.00') || '% ' ||decode(to_char(:new.desconto2),0.00,'',' + '||to_char(:new.desconto2,'90.00')||'%') ||decode(to_char(:new.desconto3),0.00,'',' + '||to_char(:new.desconto3,'90.00')||'%'),1,55),
                        substr(o.des_mensagem1 || ' : ' || to_char(:new.desconto1,'90.00') || '% ' ||decode(to_char(:new.desconto2),0.00,'',' + '||to_char(:new.desconto2,'90.00')||'%') ||decode(to_char(:new.desconto3),0.00,'',' + '||to_char(:new.desconto3,'90.00')||'%'),56,55),
                        o.des_mensagem3,
                        o.des_mensagem4,
                        o.des_mensagem5
                 from obrf_874 o
                 where o.cod_mensagem = w_cod_mensagem_desc
                   and w_cod_mensagem_desc > 0);
             exception
                WHEN dup_val_on_index THEN
                NULL;

               when others then
                raise_application_error(-20000,'Erro ao inserir Mensagem de descontos' || Chr(10) || SQLERRM);
             end;

          end if;

       --Mensagem referente aos descontos

          if (w_cod_mensagem_desc > 0) and (:new.vlr_desc_especial > 0)
          then

             begin
                insert into fatu_052
                (cod_empresa,
                 num_nota,
                 cod_serie_nota,
                 cod_mensagem,
                 cnpj9,
                 cnpj4,
                 cnpj2,
                 ind_entr_saida,
                 seq_mensagem,
                 ind_local,
                 des_mensag_1,
                 des_mensag_2,
                 des_mensag_3,
                 des_mensag_4,
                 des_mensag_5)
                (select :new.codigo_empresa,
                        :new.num_nota_fiscal,
                        :new.serie_nota_fisc,
                        o.cod_mensagem,
                        :new.cgc_9,
                        :new.cgc_4,
                        :new.cgc_2,
                        'S',
                        w_cod_sequencia_desc_esp,
                        w_cod_local_desc_esp,
                        substr(o.des_mensagem1 || ' : ' || to_char(:new.vlr_desc_especial,'999990.00'),1,55),
                        substr(o.des_mensagem1 || ' : ' || to_char(:new.vlr_desc_especial,'999990.00'),56,55),
                        o.des_mensagem3,
                        o.des_mensagem4,
                        o.des_mensagem5
                 from obrf_874 o
                 where o.cod_mensagem = w_cod_mensagem_desc_esp
                   and w_cod_mensagem_desc_esp > 0);
             exception
                WHEN dup_val_on_index THEN
                NULL;

               when others then
                raise_application_error(-20000,'Erro ao inserir Mensagem de descontos especiais' || Chr(10) || SQLERRM);
             end;

          end if;

      -- MENSAGEM REFERENTE A VENDA DE OPERACAO FINAL (CONSUMO FINAL)



          if (w_cod_msg_consumo_final > 0) and (:new.VAL_ICMS_UF_DEST_NF > 0)-- and (:old.situacao_nfisc <> 0) and (:new.situacao_nfisc = 0)
          then

             begin
                insert into fatu_052
                (cod_empresa,
                 num_nota,
                 cod_serie_nota,
                 cod_mensagem,
                 cnpj9,
                 cnpj4,
                 cnpj2,
                 ind_entr_saida,
                 seq_mensagem,
                 ind_local,
                 des_mensag_1,
                 des_mensag_2,
                 des_mensag_3,
                 des_mensag_4,
                 des_mensag_5)
                (select :new.codigo_empresa,
                        :new.num_nota_fiscal,
                        :new.serie_nota_fisc,
                        o.cod_mensagem,
                        :new.cgc_9,
                        :new.cgc_4,
                        :new.cgc_2,
                        'S',
                        w_seq_msg_consumo_final,
                        w_loc_msg_consumo_final,
                        decode(:new.VAL_ICMS_UF_DEST_NF, 0.00, ' ',
                           substr('ICMS CONFORME DECRETO 93/2015. VALOR UF DESTINATARIO' || ' : ' || TRIM(to_char(:new.VAL_ICMS_UF_DEST_NF,'9999999990.00')) || ' VALOR FCP: ' || TRIM(to_char(:new.VAL_FCP_UF_DEST_NF,'9999999990.00')) || ' VALOR UF REMETENTE' || ' : ' || TRIM(to_char(:new.VAL_ICMS_UF_REMET_NF,'9999999990.00')),1,55)
                        ),
                        decode(:new.VAL_ICMS_UF_DEST_NF, 0.00, ' ',
                           substr('ICMS CONFORME DECRETO 93/2015. VALOR UF DESTINATARIO' || ' : ' || TRIM(to_char(:new.VAL_ICMS_UF_DEST_NF,'9999999990.00')) || ' VALOR FCP: ' || TRIM(to_char(:new.VAL_FCP_UF_DEST_NF,'9999999990.00')) || ' VALOR UF REMETENTE' || ' : ' || TRIM(to_char(:new.VAL_ICMS_UF_REMET_NF,'9999999990.00')),56,55)
                        ),
                        decode(:new.VAL_ICMS_UF_DEST_NF, 0.00, ' ',
                           substr('ICMS CONFORME DECRETO 93/2015. VALOR UF DESTINATARIO' || ' : ' || TRIM(to_char(:new.VAL_ICMS_UF_DEST_NF,'9999999990.00')) || ' VALOR FCP: ' || TRIM(to_char(:new.VAL_FCP_UF_DEST_NF,'9999999990.00')) || ' VALOR UF REMETENTE' || ' : ' || TRIM(to_char(:new.VAL_ICMS_UF_REMET_NF,'9999999990.00')),111,55)
                        ),
                        '',
                        ''
                 from obrf_874 o
                 where o.cod_mensagem = w_cod_msg_consumo_final
                   and w_cod_msg_consumo_final > 0);
             exception
                WHEN dup_val_on_index THEN
                NULL;

               when others then
                raise_application_error(-20000,'Erro ao inserir Mensagem de consumo final' || Chr(10) || SQLERRM);
             end;

          end if;


          /* MENSAGEM SOMENTE PARA FCP */

          if w_cod_msg_fcp > 0 and :new.VAL_ICMS_UF_DEST_NF = 0 and :new.VAL_FCP_UF_DEST_NF > 0
          then

             begin
                insert into fatu_052
                (cod_empresa,
                 num_nota,
                 cod_serie_nota,
                 cod_mensagem,
                 cnpj9,
                 cnpj4,
                 cnpj2,
                 ind_entr_saida,
                 seq_mensagem,
                 ind_local,
                 des_mensag_1,
                 des_mensag_2,
                 des_mensag_3,
                 des_mensag_4,
                 des_mensag_5)
                (select :new.codigo_empresa,
                        :new.num_nota_fiscal,
                        :new.serie_nota_fisc,
                        o.cod_mensagem,
                        :new.cgc_9,
                        :new.cgc_4,
                        :new.cgc_2,
                        'S',
                        w_cod_seq_fcp,
                        w_cod_local_fcp,
                        substr('Adicional de aliquota - Fundo Estadual de Combate a Pobreza e as Desigualdades Sociais (FECP) R$ ' || TRIM(to_char(:new.VAL_FCP_UF_DEST_NF,'9999999990.00')) || '.',1,55),
                        substr('Adicional de aliquota - Fundo Estadual de Combate a Pobreza e as Desigualdades Sociais (FECP) R$ ' || TRIM(to_char(:new.VAL_FCP_UF_DEST_NF,'9999999990.00')) || '.',56,55),
                        substr('Adicional de aliquota - Fundo Estadual de Combate a Pobreza e as Desigualdades Sociais (FECP) R$ ' || TRIM(to_char(:new.VAL_FCP_UF_DEST_NF,'9999999990.00')) || '.',111,55),
                        '',
                        ''
                 from obrf_874 o
                 where o.cod_mensagem = w_cod_msg_fcp
                   and w_cod_msg_fcp > 0);
             exception
                WHEN dup_val_on_index THEN
                NULL;

               when others then
                raise_application_error(-20000,'Erro ao inserir Mensagem de FCP' || Chr(10) || SQLERRM);
             end;

          end if;

          /* MENSAGEM SOMENTE PARA ORCAMENTO */

          if w_cod_msg_orcamento > 0 and :new.nr_solicitacao > 0
          then

             begin
                insert into fatu_052
                (cod_empresa,
                 num_nota,
                 cod_serie_nota,
                 cod_mensagem,
                 cnpj9,
                 cnpj4,
                 cnpj2,
                 ind_entr_saida,
                 seq_mensagem,
                 ind_local,
                 des_mensag_1,
                 des_mensag_2,
                 des_mensag_3,
                 des_mensag_4,
                 des_mensag_5)
                (select :new.codigo_empresa,
                        :new.num_nota_fiscal,
                        :new.serie_nota_fisc,
                        o.cod_mensagem,
                        :new.cgc_9,
                        :new.cgc_4,
                        :new.cgc_2,
                        'S',
                        w_cod_seq_orcamento,
                        w_cod_local_orcamento,
                        substr(o.des_mensagem1 || ' : ' || to_char(:new.nr_solicitacao,'000000'),1,55),
                        '',
                        '',
                        '',
                        ''
                 from obrf_874 o
                 where o.cod_mensagem = w_cod_msg_orcamento
                   and w_cod_msg_orcamento > 0);
             exception
                WHEN dup_val_on_index THEN
                NULL;

               when others then
                raise_application_error(-20000,'Erro ao inserir Mensagem do Orcamento' || Chr(10) || SQLERRM);
             end;

          end if;

          /* FINALIZA MENSAGEM SOMENTE PARA ORCAMENTO */


          /* Mensagem somente para Condicao de Pagamento */

          if w_cod_msg_cond_pgto > 0
          then
            BEGIN
              select pedi_070.descr_pg_cliente
              into v_desc_cond_pgto
              from pedi_070
              where pedi_070.cond_pgt_cliente = :new.cond_pgto_venda
              and   pedi_070.gera_cond_danfe = 'S';
            EXCEPTION
            WHEN OTHERS THEN
              v_desc_cond_pgto := '';
            END;

            if v_desc_cond_pgto is not null
            then
             begin
                insert into fatu_052
                (cod_empresa,
                 num_nota,
                 cod_serie_nota,
                 cod_mensagem,
                 cnpj9,
                 cnpj4,
                 cnpj2,
                 ind_entr_saida,
                 seq_mensagem,
                 ind_local,
                 des_mensag_1,
                 des_mensag_2,
                 des_mensag_3,
                 des_mensag_4,
                 des_mensag_5)
                (select :new.codigo_empresa,
                        :new.num_nota_fiscal,
                        :new.serie_nota_fisc,
                        o.cod_mensagem,
                        :new.cgc_9,
                        :new.cgc_4,
                        :new.cgc_2,
                        'S',
                        w_cod_seq_cond_pgto,
                        w_cod_local_cond_pgto,
                        substr(o.des_mensagem1 || ' : ' || to_char(:new.cond_pgto_venda) || ' - ' || v_desc_cond_pgto,1,55),
                        '',
                        '',
                        '',
                        ''
                 from obrf_874 o
                 where o.cod_mensagem = w_cod_msg_cond_pgto
                   and w_cod_msg_cond_pgto > 0);
             exception
                WHEN dup_val_on_index THEN
                NULL;

               when others then
                raise_application_error(-20000,'Erro ao inserir Mensagem da Condiï¿¿ao de pagamento' || Chr(10) || SQLERRM);
             end;
            end if;
          end if;

          /* Finaliza mensagem somente para Condicao de Pagamento */


          /* MENSAGEM SOMENTE PARA CELULAR */

          if w_cod_msg_celular > 0
          then
            BEGIN
              select basi_160.ddd, pedi_010.celular_cliente
              into v_ddd_cidade, v_numero_celular
              from pedi_010
              inner join basi_160 on pedi_010.cod_cidade = basi_160.cod_cidade
              where pedi_010.cgc_9 = :new.cgc_9
              and pedi_010.cgc_4 = :new.cgc_4
              and pedi_010.cgc_2 = :new.cgc_2;
            EXCEPTION
            WHEN OTHERS THEN
              v_numero_celular := 0;
              v_ddd_cidade := 0;
            END;

            if v_numero_celular <> 0 and v_ddd_cidade <> 0
            then
             begin
                insert into fatu_052
                (cod_empresa,
                 num_nota,
                 cod_serie_nota,
                 cod_mensagem,
                 cnpj9,
                 cnpj4,
                 cnpj2,
                 ind_entr_saida,
                 seq_mensagem,
                 ind_local,
                 des_mensag_1,
                 des_mensag_2,
                 des_mensag_3,
                 des_mensag_4,
                 des_mensag_5)
                (select :new.codigo_empresa,
                        :new.num_nota_fiscal,
                        :new.serie_nota_fisc,
                        o.cod_mensagem,
                        :new.cgc_9,
                        :new.cgc_4,
                        :new.cgc_2,
                        'S',
                        w_cod_seq_celular,
                        w_cod_local_celular,
                        substr(o.des_mensagem1 || '(' || to_char(v_ddd_cidade) || ')' || to_char(v_numero_celular),1,55),
                        '',
                        '',
                        '',
                        ''
                 from obrf_874 o
                 where o.cod_mensagem = w_cod_msg_celular
                   and w_cod_msg_celular > 0);
             exception
                WHEN dup_val_on_index THEN
                NULL;

               when others then
                raise_application_error(-20000,'Erro ao inserir o nÃºmero do celular do cliente.' || Chr(10) || SQLERRM);
             end;
            end if;
          end if;

          /* FINALIZA MENSAGEM SOMENTE PARA CELULAR */

          /* MENSAGEM PARA INFORMAR CLASSIFICAÃ¿Ã¿O DO PEDIDO */

          if w_cod_msg_class_pedido > 0
          then
            BEGIN
              select pedi_100.classificacao_pedido, pedi_060.descricao
              into v_classificacao_pedido, v_desc_class_pedido
              from pedi_100
              inner join pedi_060 on pedi_100.classificacao_pedido = pedi_060.codigo
              where pedi_100.classificacao_pedido <> 0
              and pedi_100.pedido_venda = :new.pedido_venda;
            EXCEPTION
            WHEN OTHERS THEN
              v_classificacao_pedido := 0;
              v_desc_class_pedido := '';
            END;

            if v_classificacao_pedido > 0
            then
             begin
                insert into fatu_052
                (cod_empresa,
                 num_nota,
                 cod_serie_nota,
                 cod_mensagem,
                 cnpj9,
                 cnpj4,
                 cnpj2,
                 ind_entr_saida,
                 seq_mensagem,
                 ind_local,
                 des_mensag_1,
                 des_mensag_2,
                 des_mensag_3,
                 des_mensag_4,
                 des_mensag_5)
                (select :new.codigo_empresa,
                        :new.num_nota_fiscal,
                        :new.serie_nota_fisc,
                        o.cod_mensagem,
                        :new.cgc_9,
                        :new.cgc_4,
                        :new.cgc_2,
                        'S',
                        w_cod_seq_class_pedido,
                        w_cod_local_class_pedido,
                        substr(o.des_mensagem1 || to_char(v_classificacao_pedido) || ' - ' || v_desc_class_pedido,1,55),
                        '',
                        '',
                        '',
                        ''
                 from obrf_874 o
                 where o.cod_mensagem = w_cod_msg_class_pedido
                   and w_cod_msg_class_pedido > 0);
             exception
                WHEN dup_val_on_index THEN
                NULL;

                when others then
                raise_application_error(-20000,'Erro ao inserir a classificaÃ§Ã£o do cliente.' || Chr(10) || SQLERRM);
             end;
            end if;
          end if;

          /* FINALIZA MENSAGEM PARA INFORMAR CLASSIFICAÃ¿Ã¿O DO PEDIDO */

          --Mensagem referente Notas de NF-e (BCM)

          if updating and w_cod_mensagem_nf_bcm > 0 and :new.valor_itens_nfis > 0.00
          then
             w_notas_devolvidas_bcm := '';

             FOR nf_devolucao_bcm
             IN (
             select fatu_060.num_nota_orig nf_bcm, fatu_060.serie_nota_orig serie_bcm
             from fatu_060,
                  fatu_505
             where fatu_060.ch_it_nf_cd_empr   = :new.codigo_empresa
               and fatu_060.ch_it_nf_num_nfis  = :new.num_nota_fiscal
               and fatu_060.ch_it_nf_ser_nfis  = :new.serie_nota_fisc
               and fatu_060.num_nota_orig      > 0
               and fatu_060.ch_it_nf_cd_empr   = fatu_505.codigo_empresa
               and fatu_060.ch_it_nf_ser_nfis  = fatu_505.serie_nota_fisc
               and fatu_505.serie_bcm          = 'S'
             group by fatu_060.num_nota_orig, fatu_060.serie_nota_orig
             order by fatu_060.num_nota_orig, fatu_060.serie_nota_orig)
             LOOP
                w_notas_devolvidas_bcm := w_notas_devolvidas_bcm ||
                                     to_char(nf_devolucao_bcm.nf_bcm, '000000') || '-' ||
                                     nf_devolucao_bcm.serie_bcm || ' / ';
             END LOOP;

             if w_notas_devolvidas_bcm is not null
             then
                begin
                   insert into fatu_052
                   (cod_empresa,
                    num_nota,
                    cod_serie_nota,
                    cod_mensagem,
                    cnpj9,
                    cnpj4,
                    cnpj2,
                    ind_entr_saida,
                    seq_mensagem,
                    ind_local,
                    des_mensag_1,
                    des_mensag_2,
                    des_mensag_3,
                    des_mensag_4,
                    des_mensag_5)
                   (select :new.codigo_empresa,
                           :new.num_nota_fiscal,
                           :new.serie_nota_fisc,
                           o.cod_mensagem,
                           :new.cgc_9,
                           :new.cgc_4,
                           :new.cgc_2,
                           'S',
                           w_cod_sequencia_nf_bcm,
                           w_cod_local_nf_bcm,
                           substr(o.des_mensagem1 || ' ' || w_notas_devolvidas_bcm ,1,55),
                           substr(o.des_mensagem1 || ' ' || w_notas_devolvidas_bcm ,56,55),
                           substr(o.des_mensagem1 || ' ' || w_notas_devolvidas_bcm ,111,55),
                           substr(o.des_mensagem1 || ' ' || w_notas_devolvidas_bcm ,166,55),
                           substr(o.des_mensagem1 || ' ' || w_notas_devolvidas_bcm ,221,55)
                    from obrf_874 o
                    where o.cod_mensagem = w_cod_mensagem_nf_bcm);
                exception
                   WHEN dup_val_on_index THEN
                   NULL;

                   when others then
                   raise_application_error(-20000,'Erro ao inserir Mensagem de notas de BCM' || Chr(10) || SQLERRM);
                end;
             end if;
          end if;

          --Mensagem referente as contigencias

          if (w_cod_mensagem_cont > 0) and (:new.cod_status = '-94') and updating
          then

             begin
                insert into fatu_052
                (cod_empresa,
                 num_nota,
                 cod_serie_nota,
                 cod_mensagem,
                 cnpj9,
                 cnpj4,
                 cnpj2,
                 ind_entr_saida,
                 seq_mensagem,
                 ind_local,
                 des_mensag_1,
                 des_mensag_2,
                 des_mensag_3,
                 des_mensag_4,
                 des_mensag_5)
                (select :new.codigo_empresa,
                        :new.num_nota_fiscal,
                        :new.serie_nota_fisc,
                        o.cod_mensagem,
                        :new.cgc_9,
                        :new.cgc_4,
                        :new.cgc_2,
                        'S',
                        w_cod_sequencia_cont,
                        w_cod_local_cont,
                        o.des_mensagem1,
                        o.des_mensagem2,
                        o.des_mensagem3,
                        o.des_mensagem4,
                        o.des_mensagem5
                 from obrf_874 o
                 where o.cod_mensagem = w_cod_mensagem_cont
                   and w_cod_mensagem_cont > 0);
             exception
                WHEN dup_val_on_index THEN
                NULL;

               when others then
                raise_application_error(-20000,'Erro ao inserir Mensagem de Contigencia NF-e' || Chr(10) || SQLERRM);
             end;

          end if;

          --Mensagem pedido do cliente
          if w_cod_mensagem_ped_cli > 0 and :new.pedido_venda > 0
          then

             w_mensagem_ped_cli := ' ';
             w_mensagem_ped_cli_final := ' ';

             begin
                select obrf_874.des_mensagem1
                into w_mensagem_ped_cli
                from obrf_874
                where obrf_874.cod_mensagem = w_cod_mensagem_ped_cli;
             exception
                 when no_data_found then
                    w_mensagem_ped_cli := ' ';
             end;

             if w_mensagem_ped_cli <> ' '
             then

                FOR nf_ped_cli
                IN (
                select fatu_060.pedido_venda
                from fatu_060
                where fatu_060.ch_it_nf_cd_empr   = :new.codigo_empresa
                  and fatu_060.ch_it_nf_num_nfis  = :new.num_nota_fiscal
                  and fatu_060.ch_it_nf_ser_nfis  = :new.serie_nota_fisc
                  and fatu_060.pedido_venda       > 0
                group by fatu_060.pedido_venda
                order by fatu_060.pedido_venda)
                LOOP

                   w_mensagem_ped_cli_aux := ' ';

                   begin
                      select pedi_100.cod_ped_cliente
                      into w_mensagem_ped_cli_aux
                      from pedi_100
                      where pedi_100.pedido_venda = nf_ped_cli.pedido_venda;
                   exception
                       when no_data_found then
                          w_mensagem_ped_cli_aux := ' ';
                   end;

                   if length(w_mensagem_ped_cli_aux) > 1 and instr(w_mensagem_ped_cli_aux, '-',  1, 1) > 0
                      and instr(w_mensagem_ped_cli_aux, '-',  1, 2) > 0
                   then

                      if w_mensagem_ped_cli_final = ' '
                      then
                         w_mensagem_ped_cli_final := w_mensagem_ped_cli;
                      else
                         w_mensagem_ped_cli_final := w_mensagem_ped_cli_final || ' / ' || w_mensagem_ped_cli;
                      end if;

                      w_len_fim := instr(w_mensagem_ped_cli_aux, '-', 1, 1);
                      w_len_aux := w_len_fim + 1;

                      w_mensagem_ped_cli_final := replace(trim(w_mensagem_ped_cli_final), '^1^', trim(substr(w_mensagem_ped_cli_aux, 1, (w_len_fim - 1))));
                      w_len_fim := instr(w_mensagem_ped_cli_aux, '-', 1, 2);

                      w_mensagem_ped_cli_final := replace(trim(w_mensagem_ped_cli_final), '^2^', trim(substr(w_mensagem_ped_cli_aux, (w_len_aux + 1), (w_len_fim - 1 - w_len_aux))));
                      w_len_aux := w_len_fim + 1;

                      w_mensagem_ped_cli_final := replace(trim(w_mensagem_ped_cli_final), '^3^', trim(substr(w_mensagem_ped_cli_aux, w_len_aux, length(w_mensagem_ped_cli_aux))));

                      if length(w_mensagem_ped_cli_final) > 275
                      then EXIT WHEN length(w_mensagem_ped_cli_final) > 275;
                      end if;

                   end if;


                END LOOP;

                if w_mensagem_ped_cli_final <> ' '
                then
                   begin
                      insert into fatu_052
                      (cod_empresa,
                       num_nota,
                       cod_serie_nota,
                       cod_mensagem,
                       cnpj9,
                       cnpj4,
                       cnpj2,
                       ind_entr_saida,
                       seq_mensagem,
                       ind_local,
                       des_mensag_1,
                       des_mensag_2,
                       des_mensag_3,
                       des_mensag_4,
                       des_mensag_5)
                      (select :new.codigo_empresa,
                              :new.num_nota_fiscal,
                              :new.serie_nota_fisc,
                              o.cod_mensagem,
                              :new.cgc_9,
                              :new.cgc_4,
                              :new.cgc_2,
                              'S',
                              w_cod_sequencia_ped_cli,
                              w_cod_local_ped_cli,
                              substr(w_mensagem_ped_cli_final, 1, 55),
                              substr(w_mensagem_ped_cli_final, 56, 55),
                              substr(w_mensagem_ped_cli_final, 111, 55),
                              substr(w_mensagem_ped_cli_final, 166, 55),
                              substr(w_mensagem_ped_cli_final, 221, 55)
                       from obrf_874 o
                       where o.cod_mensagem = w_cod_mensagem_ped_cli
                         and w_cod_mensagem_ped_cli > 0);
                   exception
                      WHEN dup_val_on_index THEN
                      NULL;

                     when others then
                      raise_application_error(-20000,'Erro ao inserir Mensagem do Pedido do Cliente.' || Chr(10) || SQLERRM);
                   end;
                end if;
             end if;

          end if;

          --Mensagem ICMS PR
          if w_cod_mensagem_icms_pr > 0 and updating and :new.natop_nf_est_oper = 'PR'
          then
             begin
                select pedi_010.insc_est_cliente
                into w_insc_est_cliente
                from pedi_010
                where pedi_010.cgc_9 = :new.cgc_9
                  and pedi_010.cgc_4 = :new.cgc_4
                  and pedi_010.cgc_2 = :new.cgc_2;
             exception
                 when no_data_found then
                    w_insc_est_cliente := ' ';
             end;

             begin
                select basi_160.estado
                into w_estado_empresa
                from fatu_500, basi_160
                where basi_160.cod_cidade     = fatu_500.codigo_cidade
                  and fatu_500.codigo_empresa = :new.codigo_empresa;
             exception
                 when no_data_found then
                    w_estado_empresa := ' ';
             end;

             if w_insc_est_cliente <> ' ' and w_insc_est_cliente <> 'ISENTO' and w_estado_empresa = 'PR'
             then

                FOR nf_icms_pr
                IN (
                select fatu_060.natopeno_nat_oper, fatu_060.natopeno_est_oper,
                       fatu_060.perc_icms
                from fatu_060
                where fatu_060.ch_it_nf_cd_empr   = :new.codigo_empresa
                  and fatu_060.ch_it_nf_num_nfis  = :new.num_nota_fiscal
                  and fatu_060.ch_it_nf_ser_nfis  = :new.serie_nota_fisc)
                LOOP
                   begin
                      select pedi_080.tipo_reducao,  pedi_080.perc_reducao_icm
                      into   w_tipo_reducao,         w_perc_reducao_icm
                      from pedi_080
                      where pedi_080.natur_operacao = nf_icms_pr.natopeno_nat_oper
                        and pedi_080.estado_natoper = nf_icms_pr.natopeno_est_oper;
                   exception
                       when no_data_found then
                          w_tipo_reducao := 0;
                          w_perc_reducao_icm := 0.00;
                   end;

                   if w_tipo_reducao = 2 and w_perc_reducao_icm > 0.00
                   then

                      begin
                         select obrf_874.des_mensagem1
                         into w_mensagem_icms_pr
                         from obrf_874
                         where obrf_874.cod_mensagem = w_cod_mensagem_icms_pr;
                      exception
                          when no_data_found then
                             w_mensagem_icms_pr := ' ';
                      end;

                      -- Dados do usuario logado
                      inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                                              ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

                      w_mensagem_icms_pr := inter_fn_buscar_tag('lb18462#1ICMS dif.em',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                         trim(to_char(w_perc_reducao_icm, 999999990.99)) || ' ' ||
                         inter_fn_buscar_tag('lb19770#1% (',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                         trim(to_char(:new.base_icms, 999999999999990.99)) || ' ' ||
                         inter_fn_buscar_tag('lb19772#1 x ',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                         trim(to_char(nf_icms_pr.perc_icms, 9999990.99)) || ' ' ||
                         inter_fn_buscar_tag('lb19774#1% = ',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                         trim(to_char((:new.base_icms * (nf_icms_pr.perc_icms / 100)), 999999999999990.99)) || ' ' ||
                         inter_fn_buscar_tag('lb19776#1 * ',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                         trim(to_char(w_perc_reducao_icm, 999999990.99)) || ' ' ||
                         inter_fn_buscar_tag('lb19774#1% = ',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                         trim(to_char(((:new.base_icms * (nf_icms_pr.perc_icms / 100)) - :new.valor_icms), 999999999999990.99)) || ' ' ||
                         inter_fn_buscar_tag('lb38694#1 ',ws_locale_usuario,ws_usuario_systextil) || ' ' ||
                         w_mensagem_icms_pr;

                         begin
                            insert into fatu_052
                            (cod_empresa,
                             num_nota,
                             cod_serie_nota,
                             cod_mensagem,
                             cnpj9,
                             cnpj4,
                             cnpj2,
                             ind_entr_saida,
                             seq_mensagem,
                             ind_local,
                             des_mensag_1,
                             des_mensag_2,
                             des_mensag_3,
                             des_mensag_4,
                             des_mensag_5)
                             (select :new.codigo_empresa,
                                     :new.num_nota_fiscal,
                                     :new.serie_nota_fisc,
                                     o.cod_mensagem,
                                     :new.cgc_9,
                                     :new.cgc_4,
                                     :new.cgc_2,
                                     'S',
                                     w_cod_sequencia_icms_pr,
                                     w_cod_local_icms_pr,
                                     substr(w_mensagem_icms_pr, 1, 55),
                                     substr(w_mensagem_icms_pr, 56, 55),
                                     substr(w_mensagem_icms_pr, 111, 55),
                                     substr(w_mensagem_icms_pr, 166, 55),
                                     substr(w_mensagem_icms_pr, 221, 55)
                              from obrf_874 o
                              where o.cod_mensagem = w_cod_mensagem_icms_pr
                                and w_cod_mensagem_icms_pr > 0);
                         exception
                             WHEN dup_val_on_index THEN
                                NULL;
                             when others then
                                raise_application_error(-20000,'Erro ao inserir Mensagem do Pedido do Cliente.' || Chr(10) || SQLERRM);
                         end;
                   end if;
                END LOOP;
             end if;

          end if;

          if w_cod_mensagem_ipi_dev > 0 and :new.valor_ipi > 0.00
          then

             w_zerou_ipi := 'N';

             begin
                select 'S' into w_zerou_ipi
                from pedi_080, estq_005
                where pedi_080.natur_operacao   = :new.natop_nf_nat_oper
                  and pedi_080.estado_natoper   = :new.natop_nf_est_oper
                  and estq_005.codigo_transacao = pedi_080.codigo_transacao
                  and estq_005.tipo_transacao = 'D';
             exception
                 when no_data_found then
                    w_zerou_ipi := 'N';
             end;

             if w_zerou_ipi = 'S'
             then

                begin
                 select nvl(sum(decode(fatu_060.cvf_ipi_saida,99,fatu_060.valor_ipi,0)),0.00)
                  into   w_ipiDevolucao
                from fatu_060
                where fatu_060.ch_it_nf_cd_empr   = :new.codigo_empresa
                  and fatu_060.ch_it_nf_num_nfis  = :new.num_nota_fiscal
                  and fatu_060.ch_it_nf_ser_nfis  = :new.serie_nota_fisc;
                exception
                when access_into_null then
                  w_ipiDevolucao := 0.00;
                end;

                w_mensagem_ipi := '';

                if w_ipiDevolucao > 0.00
                then

                  begin
                     select obrf_874.des_mensagem1 || obrf_874.des_mensagem2 || obrf_874.des_mensagem3 ||
                            obrf_874.des_mensagem4 || obrf_874.des_mensagem5
                     into w_mensagem_ipi
                     from obrf_874
                     where obrf_874.cod_mensagem = w_cod_mensagem_ipi_dev;
                  exception
                      when no_data_found then
                         w_mensagem_ipi := '';
                  end;

                  w_mensagem_ipi := trim(w_mensagem_ipi || ' ' || TRIM(to_char(w_ipiDevolucao,'9999999990.00')));

                  begin
                     insert into fatu_052
                     (cod_empresa,
                      num_nota,
                      cod_serie_nota,
                      cod_mensagem,
                      cnpj9,
                      cnpj4,
                      cnpj2,
                      ind_entr_saida,
                      seq_mensagem,
                      ind_local,
                      des_mensag_1,
                      des_mensag_2,
                      des_mensag_3,
                      des_mensag_4,
                      des_mensag_5)
                     (select :new.codigo_empresa,
                             :new.num_nota_fiscal,
                             :new.serie_nota_fisc,
                             o.cod_mensagem,
                             :new.cgc_9,
                             :new.cgc_4,
                             :new.cgc_2,
                             'S',
                             w_cod_sequencia_ipi_dev,
                             w_cod_local_ipi_dev,
                             substr(w_mensagem_ipi, 1, 55),
                             substr(w_mensagem_ipi, 56, 55),
                             substr(w_mensagem_ipi, 111, 55),
                             substr(w_mensagem_ipi, 166, 55),
                             substr(w_mensagem_ipi, 221, 55)
                      from obrf_874 o
                      where o.cod_mensagem = w_cod_mensagem_ipi_dev);
                  exception
                     WHEN dup_val_on_index THEN
                        NULL;
                     when others then
                        raise_application_error(-20000,'Erro ao inserir Mensagem de IPI' || Chr(10) || SQLERRM);
                  end;
              end if;
             end if;
          end if;

          if w_cod_mensagem_ipi_cont > 0 and :new.valor_ipi > 0.00 and :new.nota_fatura > 0
          then

             w_mensagem_ipi := ' ';

             begin
                select obrf_874.des_mensagem1 || obrf_874.des_mensagem2 || obrf_874.des_mensagem3 ||
                       obrf_874.des_mensagem4 || obrf_874.des_mensagem5
                into w_mensagem_ipi
                from obrf_874
                where obrf_874.cod_mensagem = w_cod_mensagem_ipi_cont;
             exception
                 when no_data_found then
                    w_mensagem_ipi := ' ';
             end;

             w_mensagem_ipi := trim(w_mensagem_ipi || ' ' || trim(to_char(:new.valor_ipi, 999999999999990.99)));

             begin
                insert into fatu_052
                (cod_empresa,
                 num_nota,
                 cod_serie_nota,
                 cod_mensagem,
                 cnpj9,
                 cnpj4,
                 cnpj2,
                 ind_entr_saida,
                 seq_mensagem,
                 ind_local,
                 des_mensag_1,
                 des_mensag_2,
                 des_mensag_3,
                 des_mensag_4,
                 des_mensag_5)
                (select :new.codigo_empresa,
                        :new.num_nota_fiscal,
                        :new.serie_nota_fisc,
                        o.cod_mensagem,
                        :new.cgc_9,
                        :new.cgc_4,
                        :new.cgc_2,
                        'S',
                        w_cod_sequencia_ipi_cont,
                        w_cod_local_ipi_cont,
                        substr(w_mensagem_ipi, 1, 55),
                        substr(w_mensagem_ipi, 56, 55),
                        substr(w_mensagem_ipi, 111, 55),
                        substr(w_mensagem_ipi, 166, 55),
                        substr(w_mensagem_ipi, 221, 55)
                 from obrf_874 o
                 where o.cod_mensagem = w_cod_mensagem_ipi_cont);
             exception
                WHEN dup_val_on_index THEN
                   NULL;
                when others then
                  raise_application_error(-20000,'Erro ao inserir Mensagem de IPI' || Chr(10) || SQLERRM);
             end;
          end if;

          /* MONTA MSG PARA NOTAS DE AJUSTE DE IPI PARA CUPOM FISCAL */
          if w_cod_msg_aj_ipi > 0 and :new.valor_ipi > 0.00
          then
             w_nf_rel_aj_ipi := null;

             for nf_rel_aj_ipi in (
                select distinct fatu_060.ch_it_nf_num_nfis
                from fatu_060
                where fatu_060.ch_it_nf_cd_empr  = :new.codigo_empresa
                  and fatu_060.num_nota_ecf_ipi  = :new.num_nota_fiscal
                  and fatu_060.ch_it_nf_ser_nfis = 'CF')
             loop
                w_nf_rel_aj_ipi := w_nf_rel_aj_ipi || nf_rel_aj_ipi.ch_it_nf_num_nfis || ' ';
             end loop;

             if w_nf_rel_aj_ipi is not null
             then
                w_mensagem_aj_ipi := ' ';

                begin
                   select obrf_874.des_mensagem1 || obrf_874.des_mensagem2 || obrf_874.des_mensagem3 ||
                          obrf_874.des_mensagem4 || obrf_874.des_mensagem5
                   into w_mensagem_aj_ipi
                   from obrf_874
                   where obrf_874.cod_mensagem = w_cod_msg_aj_ipi;
                exception
                    when no_data_found then
                       w_mensagem_aj_ipi := ' ';
                end;

                w_mensagem_aj_ipi := w_mensagem_aj_ipi || ' ' || w_nf_rel_aj_ipi || ' ' || ' SERIE: CF';

                begin
                   insert into fatu_052
                   (cod_empresa,
                    num_nota,
                    cod_serie_nota,
                    cod_mensagem,
                    cnpj9,
                    cnpj4,
                    cnpj2,
                    ind_entr_saida,
                    seq_mensagem,
                    ind_local,
                    des_mensag_1,
                    des_mensag_2,
                    des_mensag_3,
                    des_mensag_4,
                    des_mensag_5)
                   (select :new.codigo_empresa,
                           :new.num_nota_fiscal,
                           :new.serie_nota_fisc,
                           o.cod_mensagem,
                           :new.cgc_9,
                           :new.cgc_4,
                           :new.cgc_2,
                           'S',
                           w_seq_msg_aj_ipi,
                           w_loc_msg_aj_ipi,
                           substr(w_mensagem_aj_ipi, 1, 55),
                           substr(w_mensagem_aj_ipi, 56, 55),
                           substr(w_mensagem_aj_ipi, 111, 55),
                           substr(w_mensagem_aj_ipi, 166, 55),
                           substr(w_mensagem_aj_ipi, 221, 55)
                    from obrf_874 o
                    where o.cod_mensagem = w_cod_msg_aj_ipi);
                exception
                   WHEN dup_val_on_index THEN
                      NULL;
                   when others then
                     raise_application_error(-20000,'Erro ao inserir Mensagem de ajuste de IPI' || Chr(10) || SQLERRM);
                end;
             end if;
          end if;

          if w_cod_msg_inf_trib  > 0 and
           ((:old.situacao_nfisc = 0 and :new.situacao_nfisc = 3) or
            (:old.situacao_nfisc = 5 and :new.situacao_nfisc = 0))
            and :new.natop_nf_est_oper <> 'EX'
          then
             w_valor_base_calc      := 0;
               w_valor_carga_trib     := 0;
               w_valor_base_calc_tot  := 0;
               w_valor_carga_trib_tot := 0;

               w_valor_carga_trib_municipal := 0;
               w_valor_carga_trib_estadual  := 0;

            w_aliquota_municipal         := 0;
            w_aliquota_estadual          := 0;
            w_aliquota_nacional          := 0;

             begin
             select pedi_010.insc_est_cliente
             into w_insc_est_cliente
             from pedi_010
             where pedi_010.cgc_9 = :new.cgc_9
               and pedi_010.cgc_4 = :new.cgc_4
               and pedi_010.cgc_2 = :new.cgc_2;

             exception
                when no_data_found then
                 w_insc_est_cliente := ' ';
             end;

             for fatu060_trib in (select fatu_060.classific_fiscal, fatu_060.qtde_item_fatur,
                                         fatu_060.valor_unitario,   fatu_060.rateio_despesa
                                  from fatu_060
                                  where fatu_060.ch_it_nf_cd_empr  = :new.codigo_empresa
                                    and fatu_060.ch_it_nf_num_nfis = :new.num_nota_fiscal
                                    and fatu_060.ch_it_nf_ser_nfis = :new.serie_nota_fisc
                                    and (exists (select 1 from pedi_080
                                                where pedi_080.natur_operacao = fatu_060.natopeno_nat_oper
                                                  and pedi_080.estado_natoper = fatu_060.natopeno_est_oper
                                                  and pedi_080.consumidor_final = 'S')
                                        or w_insc_est_cliente = 'ISENTO'))
             loop

                w_valor_base_calc  := 0;
                w_valor_carga_trib := 0;

                begin
                  select min(obrf_995.aliquota_nacional),min(obrf_995.aliquota_estadual), min(obrf_995.aliquota_municipal)
                  into   w_aliquota_nacional, w_aliquota_estadual, w_aliquota_municipal
                  from obrf_995
                  where To_number(trim(replace(obrf_995.classif_fiscal,'.',''))) =
                        To_number(trim(replace(fatu060_trib.classific_fiscal,'.','')))
                    and obrf_995.inicio_vigencia in (select max(obrf995.inicio_vigencia) from obrf_995 obrf995
                                                     where obrf995.classif_fiscal   = obrf_995.classif_fiscal
                                                       and obrf995.inicio_vigencia <= :new.data_emissao);
                exception when others
                  then
                  w_aliquota_nacional := 0;
                  w_aliquota_estadual := 0;
                  w_aliquota_municipal := 0;
                end;

              w_valor_base_calc  := (fatu060_trib.qtde_item_fatur * fatu060_trib.valor_unitario) + fatu060_trib.rateio_despesa;
              w_valor_base_calc_tot  := w_valor_base_calc_tot  + w_valor_base_calc;

                if w_aliquota_nacional <> 0
                then
                   w_valor_carga_trib := w_valor_base_calc * w_aliquota_nacional / 100;
                   w_valor_carga_trib_tot := w_valor_carga_trib_tot + w_valor_carga_trib;
                end if;

              if w_aliquota_estadual <> 0
                then
                  w_valor_carga_trib_estadual := w_valor_carga_trib_estadual + (w_valor_base_calc * w_aliquota_estadual / 100);
              end if;

              if w_aliquota_municipal <> 0
                then
                  w_valor_carga_trib_municipal := w_valor_carga_trib_municipal + (w_valor_base_calc * w_aliquota_municipal / 100);
              end if;

             end loop;

             if w_valor_carga_trib_tot > 0 or w_valor_carga_trib_estadual > 0 or w_valor_carga_trib_municipal > 0
             then

                w_mensagem_inf_trib := 'Val. Aprox. Tributos: Carga Nacional R$: ' ||
                                       trim(replace(replace(replace(to_char(w_valor_carga_trib_tot,'999,999,990.00'),'.','x'),',','.'),'x',',') || ' (' ||
                                       trim(replace(to_char((w_valor_carga_trib_tot/w_valor_base_calc_tot)*100,'90.00'),'.',','))) || '%). ' ||

                               'Carga estadual R$: ' ||
                               trim(replace(replace(replace(to_char(w_valor_carga_trib_estadual,'999,999,990.00'),'.','x'),',','.'),'x',',') || ' (' ||
                                       trim(replace(to_char((w_valor_carga_trib_estadual/w_valor_base_calc_tot)*100,'90.00'),'.',','))) || '%). ' ||

                               'Carga municipal R$: ' ||
                               trim(replace(replace(replace(to_char(w_aliquota_municipal,'999,999,990.00'),'.','x'),',','.'),'x',',') || ' (' ||
                                       trim(replace(to_char((w_aliquota_municipal/w_valor_base_calc_tot)*100,'90.00'),'.',','))) || '%).' ||

                               ' Fonte: IBPT.';


                begin
                   insert into fatu_052
                   (cod_empresa,
                    num_nota,
                    cod_serie_nota,
                    cod_mensagem,
                    cnpj9,
                    cnpj4,
                    cnpj2,
                    ind_entr_saida,
                    seq_mensagem,
                    ind_local,
                    des_mensag_1,
                    des_mensag_2,
                    des_mensag_3,
                    des_mensag_4,
                    des_mensag_5)
                   (select :new.codigo_empresa,
                           :new.num_nota_fiscal,
                           :new.serie_nota_fisc,
                           o.cod_mensagem,
                           :new.cgc_9,
                           :new.cgc_4,
                           :new.cgc_2,
                           'S',
                           w_seq_msg_inf_trib,
                           w_loc_msg_inf_trib,
                           substr(w_mensagem_inf_trib, 1, 55),
                           substr(w_mensagem_inf_trib, 56, 55),
                           substr(w_mensagem_inf_trib, 111, 55),
                           substr(w_mensagem_inf_trib, 166, 55),
                           substr(w_mensagem_inf_trib, 221, 55)
                    from obrf_874 o
                    where o.cod_mensagem = w_cod_msg_inf_trib);
                exception
                   WHEN dup_val_on_index THEN
                      NULL;
                   when others then
                     raise_application_error(-20000,'Erro ao inserir Mensagem de carga tributaria' || Chr(10) || SQLERRM);
                end;

             end if;

          end if;

          :new.valor_tributos := w_valor_carga_trib;

         if w_cod_msg_suppliercard > 0 and :new.pedido_venda > 0
          then
             select pedi_100.pedido_suppliercard, cond_pgto_venda
             into w_pedido_suppliercard, w_condicao_pagamento
             from pedi_100
             where pedi_100.pedido_venda = :new.pedido_venda;

             if w_pedido_suppliercard = 'S'
             then

                select count(1)
                into w_nr_parcelas
                from pedi_075
                where pedi_075.condicao_pagto = w_condicao_pagamento;

                w_mensagem_suppliercard := 'Compra efetuada atraves do cartao em '
                                    || w_nr_parcelas || ' parcelas. Central de atendimento Live!Card: 03007704417';

                begin
                   insert into fatu_052
                   (cod_empresa,
                    num_nota,
                    cod_serie_nota,
                    cod_mensagem,
                    cnpj9,
                    cnpj4,
                    cnpj2,
                    ind_entr_saida,
                    seq_mensagem,
                    ind_local,
                    des_mensag_1,
                    des_mensag_2,
                    des_mensag_3)
                   (select :new.codigo_empresa,
                    :new.num_nota_fiscal,
                    :new.serie_nota_fisc,
                    w_cod_msg_suppliercard,
                    :new.cgc_9,
                    :new.cgc_4,
                    :new.cgc_2,
                    'S',
                    w_cod_seq_suppliercard,
                    w_cod_local_suppliercard,
                    substr(w_mensagem_suppliercard, 1, 55),
                    substr(w_mensagem_suppliercard, 56, 55),
                    substr(w_mensagem_suppliercard, 111, 55) from dual);
                exception
                   WHEN dup_val_on_index THEN
                      NULL;
                   when others then
                     raise_application_error(-20000,'Erro ao inserir Mensagem de carga tributaria' || Chr(10) || SQLERRM);
                end;
             end if;
          end if;

          if  updating and w_cod_msg_cest > 0 and :new.valor_itens_nfis > 0.00
          then
             w_msg_nr_cest := '';

             for fatu060_cest in (select distinct trim(fatu_060.cest) cest
                                  from fatu_060
                                  where fatu_060.ch_it_nf_cd_empr  = :new.codigo_empresa
                                    and fatu_060.ch_it_nf_num_nfis = :new.num_nota_fiscal
                                    and fatu_060.ch_it_nf_ser_nfis = :new.serie_nota_fisc)
             loop
                w_msg_nr_cest := w_msg_nr_cest || fatu060_cest.cest || ' ';
             end loop;

             select length(w_msg_nr_cest) into  w_msg_conta_cest from dual;

           if w_msg_conta_cest > 3
           then

                 begin
                   select obrf_874.des_mensagem1 || obrf_874.des_mensagem2 || obrf_874.des_mensagem3 ||
                          obrf_874.des_mensagem4 || obrf_874.des_mensagem5
                   into w_mensagem_cest
                   from obrf_874
                   where obrf_874.cod_mensagem = w_cod_msg_cest;
                 exception
                   when no_data_found then
                   w_mensagem_cest := ' ';
                 end;

                 w_mensagem_cest := w_mensagem_cest || ' ' ||  w_msg_nr_cest;

                 begin
                   insert into fatu_052
                     (cod_empresa,
                      num_nota,
                      cod_serie_nota,
                      cod_mensagem,
                      cnpj9,
                      cnpj4,
                      cnpj2,
                      ind_entr_saida,
                      seq_mensagem,
                      ind_local,
                      des_mensag_1,
                      des_mensag_2,
                      des_mensag_3,
                      des_mensag_4,
                      des_mensag_5)
                   Values
               (:new.codigo_empresa,
                      :new.num_nota_fiscal,
                      :new.serie_nota_fisc,
                      w_cod_msg_cest,
                      :new.cgc_9,
                      :new.cgc_4,
                      :new.cgc_2,
                      'S',
                      w_cod_seq_cest,
                      w_cod_local_cest,
                      substr(w_mensagem_cest, 1, 55),
                      substr(w_mensagem_cest, 56, 55),
                      substr(w_mensagem_cest, 111, 55),
                      substr(w_mensagem_cest, 166, 55),
                      substr(w_mensagem_cest, 221, 55));
              exception
                   WHEN dup_val_on_index THEN
                      NULL;
                   when others then
                     raise_application_error(-20000,'Erro ao inserir Mensagem de CEST' || Chr(10) || SQLERRM);
              end;
             end if;
          end if;

        if  updating and w_cod_msg_subtrib > 0 and :new.base_icms_sub > 0.00 and :new.valor_icms_sub > 0.00
          then


            begin
               select obrf_874.des_mensagem1 || obrf_874.des_mensagem2 || obrf_874.des_mensagem3 ||
                      obrf_874.des_mensagem4 || obrf_874.des_mensagem5 || obrf_874.des_mensagem6 ||
                      obrf_874.des_mensagem7 || obrf_874.des_mensagem8 || obrf_874.des_mensagem9
                into w_msg_subtrib
                   from obrf_874
                   where obrf_874.cod_mensagem = w_cod_msg_subtrib;
                 exception
                   when no_data_found then
                   w_msg_subtrib := ' ';
            end;

                 w_msg_subtrib := w_msg_subtrib || ' ' ||  :new.base_icms_sub || ' E VALOR: ' || :new.valor_icms_sub;

                 begin
                   insert into fatu_052
                     (cod_empresa,
                      num_nota,
                      cod_serie_nota,
                      cod_mensagem,
                      cnpj9,
                      cnpj4,
                      cnpj2,
                      ind_entr_saida,
                      seq_mensagem,
                      ind_local,
                      des_mensag_1,
                      des_mensag_2,
                      des_mensag_3,
                      des_mensag_4,
                      des_mensag_5,
                      des_mensag_6,
                      des_mensag_7)
                   Values
                     (:new.codigo_empresa,
                      :new.num_nota_fiscal,
                      :new.serie_nota_fisc,
                      w_cod_msg_subtrib,
                      :new.cgc_9,
                      :new.cgc_4,
                      :new.cgc_2,
                      'S',
                      w_cod_seq_subtrib,
                      w_cod_local_subtrib,
                      substr(w_msg_subtrib, 1, 55),
                      substr(w_msg_subtrib, 56, 55),
                      substr(w_msg_subtrib, 111, 55),
                      substr(w_msg_subtrib, 166, 55),
                      substr(w_msg_subtrib, 221, 55),
                      substr(w_msg_subtrib, 276, 55),
                      substr(w_msg_subtrib, 331, 55));
              exception
                   WHEN dup_val_on_index THEN
                      NULL;
                   when others then
                     raise_application_error(-20000,'Erro ao inserir Mensagem de SUBST.TRIB.' || Chr(10) || SQLERRM);
              end;

          end if;
          -- MENSAGENS DE CLIENTES
          if inserting
          then
             begin
               insert into fatu_052
               (cod_empresa,
               num_nota,
               cod_serie_nota,
               cod_mensagem,
               cnpj9,
               cnpj4,
               cnpj2,
               ind_entr_saida,
               seq_mensagem,
               ind_local,
               des_mensag_1,
               des_mensag_2,
               des_mensag_3,
               des_mensag_4,
               des_mensag_5,
               des_mensag_6,
               des_mensag_7,
               des_mensag_8,
               des_mensag_9,
               des_mensag_10)
               (select :new.codigo_empresa,
                     :new.num_nota_fiscal,
                     :new.serie_nota_fisc,
                     o.cod_mensagem,
                     :new.cgc_9,
                     :new.cgc_4,
                     :new.cgc_2,
                     'S',
                     o.cod_mensagem,
                     o.ind_local,
                     o.des_mensagem1,
                     o.des_mensagem2,
                     o.des_mensagem3,
                     o.des_mensagem4,
                     o.des_mensagem5,
                     o.des_mensagem6,
                     o.des_mensagem7,
                     o.des_mensagem8,
                     o.des_mensagem9,
                     o.des_mensagem10
                 from obrf_874 o, pedi_265 p

                 where p.cnpj9        = :new.cgc_9
                  and  p.cnpj4        = :new.cgc_4
                  and  p.cnpj2        = :new.cgc_2
                  and  o.cod_mensagem = p.cod_msg_nota );
            exception
               WHEN dup_val_on_index THEN
               NULL;
               when others then
                  raise_application_error(-20000,'Verifique mensagem de clientes (tela pedi_f267) ' || Chr(10) || SQLERRM);
            end;
         end if  /* fim msg cliente*/;

         begin
           select nvl(max(seq_mensagem),0) +1  
           into w_sequencia
           from fatu_052
           where cod_empresa = :new.codigo_empresa
             and num_nota = :new.num_nota_fiscal
             and cod_serie_nota = :new.serie_nota_fisc
             and cnpj9 = :new.cgc_9
             and cnpj4 = :new.cgc_4
             and cnpj2 = :new.cgc_2;

           exception
           when no_data_found then
             w_sequencia := 1;
         end;
        
         --Aqui entra a regra nova de mensagem da nota, inserindo uma sequencia nova com a maior sequencia +1 
         w_retorno := inter_fn_gera_msg_nfe_adic(:new.codigo_empresa, :new.num_nota_fiscal, :new.serie_nota_fisc, :new.cgc_9, :new.cgc_4, :new.cgc_2, w_sequencia, 'S', 0);

      end if /* fim if situacao_nfisc*/;

   end inter_tr_fatu_050_msg_default;


/
exec inter_pr_recompile;
