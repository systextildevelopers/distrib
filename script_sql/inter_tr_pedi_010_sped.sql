
  CREATE OR REPLACE TRIGGER "INTER_TR_PEDI_010_SPED" 
BEFORE  UPDATE

of nome_cliente,
   cod_cidade,
   insc_est_cliente,
   nr_suframa_cli,
   endereco_cliente,
   numero_imovel,
   complemento,
   bairro

on pedi_010
for each row

begin

  if :new.nome_cliente <> :old.nome_cliente
  then
    insert into hist_100(
      tabela,
      str01,
      operacao,
      data_ocorr,
      str03,
      str02,
      str04,
      str05,
      str06)
    values (
      'pedi_010', -- INDICE
      'SPED',     -- INDICE
      'U',
      sysdate,    -- INDICE
      '3',
      :old.nome_cliente,
      :new.cgc_9,
      :new.cgc_4,
      :new.cgc_2);
  end if;

  if :new.cod_cidade <> :old.cod_cidade
  then
    insert into hist_100(
      tabela,
      str01,
      operacao,
      data_ocorr,
      str03,
      str02,
      str04,
      str05,
      str06)
    values (
      'pedi_010', -- INDICE
      'SPED',     -- INDICE
      'U',
      sysdate,    -- INDICE
      '4',
      :old.cod_cidade,
      :new.cgc_9,
      :new.cgc_4,
      :new.cgc_2);
  end if;

  if :new.insc_est_cliente <> :old.insc_est_cliente
  then
    insert into hist_100(
      tabela,
      str01,
      operacao,
      data_ocorr,
      str03,
      str02,
      str04,
      str05,
      str06)
    values (
      'pedi_010', -- INDICE
      'SPED',     -- INDICE
      'U',
      sysdate,    -- INDICE
      '7',
      :old.insc_est_cliente,
      :new.cgc_9,
      :new.cgc_4,
      :new.cgc_2);
  end if;

  if :new.nr_suframa_cli <> :old.nr_suframa_cli
  then
    insert into hist_100(
      tabela,
      str01,
      operacao,
      data_ocorr,
      str03,
      str02,
      str04,
      str05,
      str06)
    values (
      'pedi_010', -- INDICE
      'SPED',     -- INDICE
      'U',
      sysdate,    -- INDICE
      '9',
      :old.nr_suframa_cli,
      :new.cgc_9,
      :new.cgc_4,
      :new.cgc_2);
  end if;

  if :new.endereco_cliente <> :old.endereco_cliente
  then
    insert into hist_100(
      tabela,
      str01,
      operacao,
      data_ocorr,
      str03,
      str02,
      str04,
      str05,
      str06)
    values (
      'pedi_010', -- INDICE
      'SPED',     -- INDICE
      'U',
      sysdate,    -- INDICE
      '10',
      :old.endereco_cliente,
      :new.cgc_9,
      :new.cgc_4,
      :new.cgc_2);
  end if;

  if :new.numero_imovel <> :old.numero_imovel
  then
    insert into hist_100(
      tabela,
      str01,
      operacao,
      data_ocorr,
      str03,
      str02,
      str04,
      str05,
      str06)
    values (
      'pedi_010', -- INDICE
      'SPED',     -- INDICE
      'U',
      sysdate,    -- INDICE
      '11',
      :old.numero_imovel,
      :new.cgc_9,
      :new.cgc_4,
      :new.cgc_2);
  end if;
  if :new.complemento <> :old.complemento
  then
    insert into hist_100(
      tabela,
      str01,
      operacao,
      data_ocorr,
      str03,
      str02,
      str04,
      str05,
      str06)
    values (
      'pedi_010', -- INDICE
      'SPED',     -- INDICE
      'U',
      sysdate,    -- INDICE
      '12',
      :old.complemento,
      :new.cgc_9,
      :new.cgc_4,
      :new.cgc_2);
  end if;
  if :new.bairro <> :old.bairro
  then
    insert into hist_100(
      tabela,
      str01,
      operacao,
      data_ocorr,
      str03,
      str02,
      str04,
      str05,
      str06)
    values (
      'pedi_010', -- INDICE
      'SPED',     -- INDICE
      'U',
      sysdate,    -- INDICE
      '13',
      :old.bairro,
      :new.cgc_9,
      :new.cgc_4,
      :new.cgc_2);
  end if;

end;
-- ALTER TRIGGER "INTER_TR_PEDI_010_SPED" ENABLE
 

/

exec inter_pr_recompile;

