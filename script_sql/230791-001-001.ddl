alter table fatu_070
add ren_num_renegocia number (9);

alter table fatu_070
add ren_sit_renegocia number(1);

alter table fatu_070
modify ren_num_renegocia default 0;

alter table fatu_070
modify ren_sit_renegocia default 0;

exec inter_pr_recompile;
