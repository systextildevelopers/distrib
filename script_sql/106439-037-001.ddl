alter table basi_189
drop constraint pk_basi_189;

drop index PK_BASI_189;

alter table basi_189
  add constraint pk_basi_189_1 primary key (centro_custo,dia_semana,data_excecao,divisao_producao);
  
exec inter_pr_recompile;
