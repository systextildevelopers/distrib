alter table fatu_110 add (
  empresa number(3),
  processo_enderecamento number(3),
  tipo_endrecamento number(3),
  ativo_inativo number(1)
);

insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('fatu_f831', 'Enderecos da Expedicao',1,1);

INSERT INTO HDOC_033 (USU_PRG_CDUSU, USU_PRG_EMPR_USU, PROGRAMA, NOME_MENU, ITEM_MENU, ORDEM_MENU, INCLUIR, MODIFICAR, EXCLUIR, PROCURAR)
VALUES ('INTERSYS', 1, 'fatu_f831', 'fatu_m013', 1, 1, 'S', 'S', 'S', 'S');

commit;

alter table fatu_502
add(transfere_volume number(1) default 0);

/
