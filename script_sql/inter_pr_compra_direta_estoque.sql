
  CREATE OR REPLACE PROCEDURE "INTER_PR_COMPRA_DIRETA_ESTOQUE"    (p_nivel in varchar2,      p_grupo in varchar2,
                                 p_subgrupo in varchar2,   p_item in varchar2,
                                 p_deposito in number)
  is
  -- local variables here
   v_local_deposito_func       number;
   v_controle_cd               number;
   v_qtde_estoque_func         number;
   v_estoque_minimo_func       number;
   v_tipo_produto              number;
   v_tipo_produto_old          number;
   v_tipo_produto_alterado     basi_015.tipo_produto_alterado%type;
   v_tipo_produto_alterado_old basi_015.tipo_produto_alterado%type;
begin
   --encontra o local do deposito
   select local_deposito
   into   v_local_deposito_func
   from  basi_205
   where codigo_deposito = p_deposito;

   --verifica o tipo de controle se e compra direta ou e de estoque
   select altera_cd
   into   v_controle_cd
   from fatu_501
   where codigo_empresa = v_local_deposito_func;

   if v_controle_cd = 1
   then  
      select nvl(sum(estq_040.qtde_estoque_atu - estq_040.qtde_empenhada),0)
      into   v_qtde_estoque_func
      from  estq_040, basi_205
      where estq_040.deposito        = basi_205.codigo_deposito
        and estq_040.cditem_nivel99  = p_nivel
        and estq_040.cditem_grupo    = p_grupo
        and estq_040.cditem_subgrupo = p_subgrupo
        and estq_040.cditem_item     = p_item
        and basi_205.local_deposito  = v_local_deposito_func;

      select estoque_minimo, tipo_produto, tipo_produto_alterado
      into   v_estoque_minimo_func, v_tipo_produto_old, v_tipo_produto_alterado_old
      from  basi_015
      where codigo_empresa   = v_local_deposito_func
        and nivel_estrutura  = p_nivel
        and grupo_estrutura  = p_grupo
        and subgru_estrutura = p_subgrupo
        and item_estrutura   = p_item;

      if v_qtde_estoque_func <= 0.00 and v_estoque_minimo_func = 0.00
      then v_tipo_produto := 2;
      else v_tipo_produto := 1;
      end if;
      
      if v_tipo_produto_old <> v_tipo_produto
      then v_tipo_produto_alterado := '*';
      else v_tipo_produto_alterado := v_tipo_produto_alterado_old;
      end if;
      
      begin
         update basi_015
            set tipo_produto             = v_tipo_produto,
			          tipo_produto_alterado    = v_tipo_produto_alterado
            where codigo_empresa   = v_local_deposito_func
              and nivel_estrutura  = p_nivel
              and grupo_estrutura  = p_grupo
              and subgru_estrutura = p_subgrupo
              and item_estrutura   = p_item;

         if sql%notfound
         then
            begin
               insert into basi_015
                 (codigo_empresa,        nivel_estrutura,
                  grupo_estrutura,       subgru_estrutura,
                  item_estrutura,        estoque_minimo,
                  tempo_reposicao,       lote_multiplo,
                  estoque_maximo,        tipo_produto,
                  tipo_produto_alterado)
               values
                 (v_local_deposito_func, p_nivel,
                  p_grupo,               p_subgrupo,
                  p_item,                0.00,
                  0,                     0.00,
                  0.00,                  v_tipo_produto,
                  '*');
               exception
                  when others then
                     raise_application_error(-20000,'Nao inseriu na BASI_015.');
            end;
         end if;
      end; --fim do bloco do update basi_015
   end if; --fim do else do if v_controle_cd = 1

   exception
   when others then
     v_controle_cd := 0;

end inter_pr_compra_direta_estoque;



 

/

exec inter_pr_recompile;

