alter table fatu_504 add (nr_dias_max_antecipa number(3) default 0);

comment on column fatu_504.nr_dias_max_antecipa is 'indica o n�mero m�ximo de dias anteriores a data de entrega do pedido que ir� considerar como antecipa��o';

exec inter_pr_recompile;
/
