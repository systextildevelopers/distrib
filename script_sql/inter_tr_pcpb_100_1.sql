CREATE OR REPLACE TRIGGER INTER_TR_PCPB_100_1
   before insert
   on pcpb_100
   for each row

declare
    v_nome_usuario            hdoc_030.usuario%type;
    v_osuser                  varchar2(20);
    v_sid                     number;
    ws_maquina_rede           varchar2(60);   
    ws_aplicativo             varchar2(60);
    ws_empresa                varchar2(60);
    ws_locale_usuario         varchar2(60);
begin

     -- Dados do usuario logado
     inter_pr_dados_usuario (v_osuser,         ws_maquina_rede,   ws_aplicativo,     v_sid,
                             v_nome_usuario,   ws_empresa,        ws_locale_usuario);

     :new.usuario_inclusao := v_nome_usuario;
     :new.data_inclusao    := to_date(sysdate, 'DD/MM/yy');
     :new.hora_inclusao    := to_date('16111989'|| to_char(sysdate, 'HH24miss'), 'DDMMyyHH24miss');

end inter_tr_pcpb_100_1;

-- ALTER TRIGGER INTER_TR_PCPB_100_1 ENABLE
/
