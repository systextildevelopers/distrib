create table mqop_015 (
  COD_PARTE number(4) not null,
  DESCRICAO varchar2(30) not null,
  primary key(COD_PARTE)
);

create table mqop_016 (
  COD_COMP number(4) not null,
  DESCRICAO varchar2(30)not null,
  primary key(COD_COMP)
);

comment on table mqop_015 is 'Tabela de parametrização das partes de maquinas';
comment on table mqop_016 is 'Tabela de parametrização dos componentes das partes de maquinas';

comment on column mqop_015.cod_parte            is 'Codigo da parte de maquina gerado via TR';
comment on column mqop_015.descricao            is 'Descricao da parte gravado via mqop_f020';
comment on column mqop_016.cod_comp             is 'Codigo do componente da parte da maquina gerado via TR';
comment on column mqop_016.descricao            is 'Descricao do componente gravado via mqop_f021';
