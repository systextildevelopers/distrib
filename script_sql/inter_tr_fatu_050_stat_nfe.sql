
  CREATE OR REPLACE TRIGGER "INTER_TR_FATU_050_STAT_NFE" 
  before insert or update of cod_status, NFE_CONTIGENCIA
  on fatu_050
  for each row
declare

  tipo_nf   varchar(2);
  serie_nfe varchar2(1);

begin
  if :new.cod_status = '999'
  then
    :new.msg_status := 'Preparado para o envio';
  end if;
  if :new.cod_status = '998'
  then
    :new.msg_status := 'Preparado para o cancelamento';
  end if;
  if :new.cod_status = '997'
  then
    :new.msg_status := 'Preparado para a inutilizac�o da numerac�o';
  end if;
  if :new.cod_status = '100'
  then
    :new.msg_status := 'Autorizado o uso da NF-e';
  end if;
  if :new.cod_status = '101'
  then
    :new.msg_status := 'Cancelamento de NF-e homologado';
  end if;
  if :new.cod_status = '102'
  then
    :new.msg_status := 'Inutilizacao de numero homologado';
  end if;
  if :new.cod_status = '-94'
  then
    :new.msg_status := 'Envio em modo de contingencia';
  end if;
  if :new.cod_status = '996' and :new.msg_status = ' '
  then
    :new.msg_status := 'Problemas no cadastro';
  end if;
  if :new.cod_status = ' '
  then
    :new.msg_status := ' ';
  end if;

  select fatu_505.tipo_nf , fatu_505.serie_nfe
    into tipo_nf,           serie_nfe
  from fatu_505
  where fatu_505.codigo_empresa  = :new.codigo_empresa
    and fatu_505.serie_nota_fisc = :new.serie_nota_fisc;

  if serie_nfe = 'N'
  then
    :new.tipo_nf := '03';
  end if;

  if :new.nfe_contigencia = 1 and serie_nfe = 'S'
  then
    :new.tipo_nf := tipo_nf;
  end if;

end INTER_TR_FATU_050_STAT_NFE;
-- ALTER TRIGGER "INTER_TR_FATU_050_STAT_NFE" ENABLE
 

/

exec inter_pr_recompile;

