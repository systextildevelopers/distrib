CREATE OR REPLACE TRIGGER "TRIGGER_PEDI_027_LOG" 
BEFORE UPDATE OR DELETE OR INSERT on pedi_027
for each row

declare
   ws_representante              pedi_027.representante%type;
   ws_cod_cidade                 pedi_027.cod_cidade%type;

   ws_tipo_ocorr                 varchar2(1);
   ws_usuario_rede               varchar2(20);
   ws_maquina_rede               varchar2(40);
   ws_aplicacao                  varchar2(20);
   ws_processo_systextil         hdoc_090.programa%type;
   ws_sid                        number(9);

begin


   if INSERTING then

      ws_representante   := :new.representante;
      ws_cod_cidade      := :new.cod_cidade;
      ws_tipo_ocorr      := 'I';

   end if;

   if UPDATING then

      ws_representante   := :new.representante;
      ws_cod_cidade      := :new.cod_cidade;
      ws_tipo_ocorr      := 'A';

   end if;

   if DELETING then

      ws_representante   := :old.representante;
      ws_cod_cidade      := :old.cod_cidade;
      ws_tipo_ocorr      := 'D';

   end if;

   select substr(osuser,1,20), substr(machine,1,40), substr(program,1,20), sid
   into   ws_usuario_rede,     ws_maquina_rede,      ws_aplicacao,         ws_sid
   from sys.v_$session
   where audsid = userenv('SESSIONID');

   ws_processo_systextil := inter_fn_nome_programa(ws_sid);  

   INSERT INTO pedi_027_log
          (representante,                 cod_cidade,

           data_ocorr,                    tipo_ocorr,
           usuario_rede,                  maquina_rede,
           aplicacao,                     processo_systextil)
   VALUES (ws_representante,              ws_cod_cidade,

           sysdate,                       ws_tipo_ocorr,
           ws_usuario_rede,               ws_maquina_rede,
           ws_aplicacao,                  ws_processo_systextil);

end trigger_pedi_027_log;

-- ALTER TRIGGER "TRIGGER_PEDI_027_LOG" ENABLE
 

/

exec inter_pr_recompile;

