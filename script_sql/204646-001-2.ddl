declare
    id_grid    number;
    id_profile number;
    id_column  number;

    procedure insert_column(p_id number,
                            p_id_grid number,
                            p_key varchar2,
                            p_name varchar2,
                            p_type varchar2,
                            p_formatter_index number,
                            p_scale number) is
    begin
        insert into conf_grid_column (id, id_grid, key, name, type, formatter_index, scale)
        values (p_id, p_id_grid, p_key, p_name, p_type, p_formatter_index, p_scale);
    end;

    procedure insert_column_profile(p_id_grid_perfil number,
                                    p_id_grid_column number,
                                    p_position number) is
    begin
        insert into conf_grid_perfil_column (id_grid_perfil, id_grid_column, position)
        values (p_id_grid_perfil, p_id_grid_column, p_position);
    end;
begin
    id_grid := id_conf_grid.nextval;

    insert into conf_grid (id, nome) values (id_grid, 'PedidosDeDestino');

    id_profile := id_conf_grid_perfil.nextval;

    insert into conf_grid_perfil (id, id_grid, usuario_gerenciador,
                                  empresa_usuario_gerenciador, nome,
                                  publico, padrao, padrao_usuario)
    values (id_profile, id_grid, 'INTERSYS', 1, 'Padrão', 1, 1, 1);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'pedido_venda', 'Pedido Venda',
                  'NUMBER', 1, 0);
    insert_column_profile(id_profile, id_column, 1);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'cliente', 'Cliente',
                  'VARCHAR2', 0, 0);
    insert_column_profile(id_profile, id_column, 2);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'data_embarque', 'Data Embarque',
                  'DATE', 2, 0);
    insert_column_profile(id_profile, id_column, 3);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'condicao_pgto', 'Condicao Pgto',
                  'VARCHAR2', 0, 0);
    insert_column_profile(id_profile, id_column, 4);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'qtde_saldo', 'Qtde Saldo',
                  'NUMBER', 1, 3);
    insert_column_profile(id_profile, id_column, 5);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'valor_saldo', 'Valor Saldo',
                  'NUMBER', 1, 2);
    insert_column_profile(id_profile, id_column, 6);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'qtde_sugerida', 'Qtde Sugerida',
                  'NUMBER', 1, 0);
    insert_column_profile(id_profile, id_column, 7);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'valor_sugerido', 'Valor Sugerido',
                  'NUMBER', 1, 0);
    insert_column_profile(id_profile, id_column, 8);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'faturamento', 'Faturamento',
                  'VARCHAR2', 0, 0);
    insert_column_profile(id_profile, id_column, 9);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'valor_frete_pedido', 'Valor Frete Pedido',
                  'NUMBER', 1, 2);
    insert_column_profile(id_profile, id_column, 10);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'valor_despesas_pedido', 'Valor Despesas Pedido',
                  'NUMBER', 1, 2);
    insert_column_profile(id_profile, id_column, 11);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'tabela_preco', 'Tabela Preco',
                  'VARCHAR2', 0, 0);
    insert_column_profile(id_profile, id_column, 12);

end;

/
