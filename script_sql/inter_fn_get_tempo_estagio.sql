
  CREATE OR REPLACE FUNCTION "INTER_FN_GET_TEMPO_ESTAGIO" (p_nivel_produto       in varchar2,
                                                       p_grupo_produto       in varchar2,
                                                       p_subgrupo_produto    in varchar2,
                                                       p_item_produto        in varchar2,
                                                       p_alternativa_produto in varchar2,
                                                       p_roteiro_produto     in varchar2,
                                                       p_estagio             in varchar2,
                                                       p_depende_quantidade  in number)
return number is
   v_subgrupo_leitura      varchar(3);
   v_item_leitura          varchar(6);
   v_count                 number;
   v_tempo_roteiro         number;
begin

   -- p_depende_quantidade:
   -- Segundo Cadastro de Operacoes, coluna tipo_operacao,
   -- (1) Depende da quantidade,
   -- (2) Independe da quantidade.

   v_subgrupo_leitura   := p_subgrupo_produto;
   v_item_leitura       := p_item_produto;

   begin
      select count(1)
        into v_count
        from mqop_050
       where mqop_050.nivel_estrutura  = p_nivel_produto
         and mqop_050.grupo_estrutura  = p_grupo_produto
         and mqop_050.subgru_estrutura = v_subgrupo_leitura
         and mqop_050.item_estrutura   = v_item_leitura
         and mqop_050.numero_alternati = p_alternativa_produto
         and mqop_050.numero_roteiro   = p_roteiro_produto;
      if v_count = 0
      then
         v_item_leitura := '000000';

         begin
            select count(1)
              into v_count
              from mqop_050
             where mqop_050.nivel_estrutura  = p_nivel_produto
               and mqop_050.grupo_estrutura  = p_grupo_produto
               and mqop_050.subgru_estrutura = v_subgrupo_leitura
               and mqop_050.item_estrutura   = v_item_leitura
               and mqop_050.numero_alternati = p_alternativa_produto
               and mqop_050.numero_roteiro   = p_roteiro_produto;
            if v_count = 0
            then
               v_subgrupo_leitura := '000';
               v_item_leitura     := p_item_produto;

               begin
                  select count(1)
                    into v_count
                    from mqop_050
                   where mqop_050.nivel_estrutura  = p_nivel_produto
                     and mqop_050.grupo_estrutura  = p_grupo_produto
                     and mqop_050.subgru_estrutura = v_subgrupo_leitura
                     and mqop_050.item_estrutura   = v_item_leitura
                     and mqop_050.numero_alternati = p_alternativa_produto
                     and mqop_050.numero_roteiro   = p_roteiro_produto;
                  if v_count = 0
                  then
                     v_item_leitura := '000000';
                  end if;
               end;
            end if;
         end;
      end if;
   end;

   begin
      select  nvl(sum(mqop_050.minutos),0)
        into  v_tempo_roteiro
        from  mqop_050, mqop_040
       where  mqop_050.nivel_estrutura  = p_nivel_produto
         and  mqop_050.grupo_estrutura  = p_grupo_produto
         and  mqop_050.subgru_estrutura = v_subgrupo_leitura
         and  mqop_050.item_estrutura   = v_item_leitura
         and  mqop_050.numero_alternati = p_alternativa_produto
         and  mqop_050.numero_roteiro   = p_roteiro_produto
         and  mqop_050.codigo_estagio   = p_estagio
         and  mqop_050.codigo_operacao  = mqop_040.codigo_operacao
         and (mqop_040.tipo_operacao    = p_depende_quantidade or p_depende_quantidade = 0);
   end;

   return v_tempo_roteiro;
end inter_fn_get_tempo_estagio;
 

/

exec inter_pr_recompile;

