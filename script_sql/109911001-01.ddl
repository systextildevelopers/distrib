create table pedi_058
( codigo_empresa           number(3),
  cnpj9                    number(9),
  cnpj4                    number(4),
  cnpj2                    number(2), 
  cnpj_seq_rem             number(5) default 0,
  codigo_clie              varchar2(17) default '',  
  banco_clie               number(3) default 0,
  agencia_clie             number(9) default 0,
  agencia_dig_clie         number(2) default 0,
  conta_clie               number(9) default 0,
  conta_dig_clie           number(2) default 0,
  banco_remessa            number(3) default 0,
  identif_remessa          number(9) default 0,    
  flag_remessa             number(1) default 0,
  numero_remessa           number(9) default 0,  
  status_di                number(1) default 0,
  data_inclusao            date,
  data_exclusao            date,
  data_retorno             date,
  motivo_rejeicao          varchar2(1200) default '',
  arquivo_retorno          varchar2(100) default '',
  cod_clie_banco           varchar2(20)  default '',
  email_cliente            varchar2(150) default '');

alter table pedi_058
add constraint pk_pedi_058 primary key (codigo_empresa,cnpj9,cnpj4,cnpj2);

comment on table pedi_058 is 'Depósito Identificado';
comment on column pedi_058.codigo_empresa     is 'Código da empresa do Deposito Identificado';        
comment on column pedi_058.cnpj9              is 'CNPJ do D.I.';        
comment on column pedi_058.cnpj4              is 'CNPJ do D.I.';            
comment on column pedi_058.cnpj2              is 'CNPJ do D.I.';            
comment on column pedi_058.cnpj_seq_rem       is 'Sequencia de envio do cliente para o banco';            
comment on column pedi_058.codigo_clie        is 'Codigo do cliente na empresa';                
comment on column pedi_058.banco_clie         is 'Banco do cliente';                
comment on column pedi_058.agencia_clie       is 'Agencia do cliente';                    
comment on column pedi_058.agencia_dig_clie   is 'digito Agencia do cliente';                        
comment on column pedi_058.conta_clie         is 'conta do cliente';      
comment on column pedi_058.conta_dig_clie     is 'digito conta do cliente';          
comment on column pedi_058.banco_remessa      is 'Banco da empresa que recebera o cliente';              
comment on column pedi_058.identif_remessa    is 'Identificar da conta do Banco da empresa que recebera o cliente';                  
comment on column pedi_058.flag_remessa       is '0 - não selecionado, 1 - seleciona inclusao, 2 - seleciona exclusão';          
comment on column pedi_058.numero_remessa     is 'número da remessa';              
comment on column pedi_058.status_di          is '0 - não possui identificacao, 1 - identificado';
comment on column pedi_058.data_inclusao      is 'data da inclusao do cliente no D.I.';         
comment on column pedi_058.data_exclusao      is 'data da exclusao do cliente no D.I.';         
comment on column pedi_058.data_retorno       is 'data retorno';
comment on column pedi_058.motivo_rejeicao    is 'descricao da rejeicao';
comment on column pedi_058.arquivo_retorno    is 'Nome do arquivo de retorno';
comment on column pedi_058.cod_clie_banco     is 'Código do cliente no banco';
comment on column pedi_058.email_cliente      is  'email que receberá o Código do cliente no banco';

/
exec inter_pr_recompile;
