INSERT INTO EMPR_007 (PARAM, TIPO, LABEL, FYI_MESSAGE, DEFAULT_STR, DEFAULT_INT, DEFAULT_DBL, DEFAULT_DAT)
VALUES ('fiscal.codMsgIcmsBeneficio', 1, 'null', 'null', null, 0, null, null);

declare 

cursor parametro_c is
  select codigo_empresa
  from fatu_500;

begin
  for reg_parametro in parametro_c
  loop
      begin
        insert into empr_008 (codigo_empresa, param, val_int) values (reg_parametro.codigo_empresa, 'fiscal.codMsgIcmsBeneficio', 0);
      end;

  end loop;
  commit;
end;
/
