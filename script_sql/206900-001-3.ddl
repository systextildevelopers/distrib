alter table basi_839 add 
(cor_trama varchar2(6) default '000000' not null enable, 
 cor_atador varchar2(6) default '000000' not null enable, 
 coeficiente_bx_relevo number(7,5) default 0.00000);

comment on column basi_839.cor_trama is 'Fio da trama quando o cadarco é tubular e tem poliamida';
comment on column basi_839.cor_atador is 'Fio do atador quando o cadarco é tubular e tem poliamida';
comment on column basi_839.coeficiente_bx_relevo is 'Coeficiente para ETIPRINT com baixo relevo';

exec inter_pr_recompile;

/
