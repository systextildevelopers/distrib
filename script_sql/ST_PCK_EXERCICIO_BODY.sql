create or replace package body ST_PCK_EXERCICIO is

    function get_exercicio(p_codigo_empresa number, p_codigo_exercicio number, p_msg_erro in out varchar2) return t_dados_exercicio 
    as
        v_dados_exercicio t_dados_exercicio;
    begin
        begin
            select *
            bulk collect into v_dados_exercicio
            from cont_500
            where cod_empresa = p_codigo_empresa 
            and exercicio = p_codigo_exercicio;
        exception when others then
            p_msg_erro := 'Não foi possível buscar o exercicio!. codigo_exercicio:' || p_codigo_exercicio;
        end;

        return v_dados_exercicio;
    end;
   
    function get_exercicio_by_date(p_codigo_empresa number, p_data Date, p_msg_erro in out varchar2) return t_dados_exercicio 
    as
        v_dados_exercio t_dados_exercicio;
        v_empresa ST_PCK_EMPRESAS.t_dados_fatu_500;
    begin
        begin
            select *
            bulk collect into v_dados_exercio
            from cont_500
            where cod_empresa = p_codigo_empresa 
            and p_data between per_inicial and per_final;
        exception when others then
            begin

                v_empresa := ST_PCK_EMPRESAS.get_fatu_500(p_codigo_empresa, p_msg_erro);
                    
                select *
                bulk collect into v_dados_exercio
                from cont_500
                where cod_empresa = v_empresa(1).codigo_empresa 
                and p_data between per_inicial and per_final;
            exception when others then
                p_msg_erro := 'Não foi possível buscar o exercicio por data!. data:' || p_data 
                                || ' empresa: '|| v_empresa(1).codigo_empresa || SQLERRM;
            end;
        end;
                
        return v_dados_exercio;
   end;

    FUNCTION valida_exercicio(p_empresa_matriz NUMBER, p_data_lac DATE, p_regras ST_PCK_CHECA_DATA.t_regras, p_msg_error in out varchar2) 
        RETURN ST_PCK_EXERCICIO.t_dados_exercicio 
    AS
        v_array_exercicio ST_PCK_EXERCICIO.t_dados_exercicio;
        v_array_periodo ST_PCK_PERIODO_CONTABIL.t_dados_cont_510;
        v_msg_error varchar2 (4000) := '';

    BEGIN
        -- Acha o exercício e o período do lançamento
        v_array_exercicio := ST_PCK_EXERCICIO.get_exercicio_by_date(p_empresa_matriz, p_data_lac, v_msg_error);
        IF not v_array_exercicio.exists(1) OR (p_regras.validarExercicio = 1 AND v_array_exercicio(1).situacao != 0) THEN
            p_msg_error :=  'Não conseguiu buscar exercicio por data ' || p_data_lac || '  e Empresa ' || p_empresa_matriz || '! ';
            return null;
        END IF;

        v_array_periodo := ST_PCK_PERIODO_CONTABIL.get_by_date(p_empresa_matriz, v_array_exercicio(1).exercicio, p_data_lac, v_msg_error);
        IF not v_array_periodo.exists(1) OR (p_regras.validarPeriodo = 1 AND v_array_periodo(1).situacao != 0) THEN
            p_msg_error :=  'Não conseguiu encontrar exercicio no  período contabil '|| p_data_lac || '   Empresa ' || p_empresa_matriz || ' e exercicio ' || v_array_exercicio(1).exercicio || '! ';
            return null;
        END IF;
        RETURN v_array_exercicio;
    END valida_exercicio;
            

end ST_PCK_EXERCICIO;
