
  CREATE OR REPLACE TRIGGER "INTER_TR_TMRP_621_RESERVA" 
after insert
or update of data_requerida_prev, data_requerida_real, data_acertada_compra, data_acertada_desenv,
	     observacao, setor, relacao_banho, qtde_atendida_estoque,
	     cons_unid_med_generica, unidades_reserva_planejada, unidades_reserva_programada, unidades_areceber_programada,
	     tecido_principal, liquida_saldo, ordem_prod_compra, area_producao,
	     ordem_planejamento, pedido_venda, pedido_reserva, codigo_projeto,
	     seq_projeto_origem, tipo_produto_origem, seq_produto_origem, nivel_produto_origem,
	     grupo_produto_origem, subgrupo_produto_origem, item_produto_origem, alternativa_produto_origem,
	     tipo_produto, seq_projeto, seq_produto, nivel_produto,
	     grupo_produto, subgrupo_produto, item_produto, alternativa_produto,
	     alternativa_produto_old, estagio_producao, tipo_calculo, consumo,
	     qtde_reserva_planejada, qtde_reserva_programada, situacao, qtde_areceber_programada,
	     qtde_vendida, qtde_adicional, qtde_reposicao, data_requerida,
	     qtde_produzida_comprada, qtde_conserto, qtde_2qualidade, qtde_perdas,
	     qtde_atendida_adicional, qtde_atendida_devolvida
on tmrp_621
for each row
   /**
    * Trigger para fazer a atualizacao do tmrp_625
    * com o produto real das ordens de producao.
    * Tambem fara o rateio da quantidade do ficticio
    * com o real.
    *
    * author: Fabio Kiatkowski
    * date: 09/06/2010
    *
    */
declare
   --Cursor para gerar componente real na 625
   cursor tmrp625real is select * from tmrp_625
                         where tmrp_625.ordem_planejamento  = :new.ordem_planejamento
                           and tmrp_625.nivel_produto       = :new.nivel_produto_origem
                           and tmrp_625.grupo_produto       = :new.grupo_produto_origem
                           and tmrp_625.subgrupo_produto    = :new.subgrupo_produto_origem
                           and tmrp_625.item_produto        = :new.item_produto_origem
                           and tmrp_625.alternativa_produto = :new.alternativa_produto_old;

   --Variaveis do processo
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

--   v_existe    number(9,3);
   v_qtde      number(9,3);

begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   if inserting
   then
      --Iterando cursor real
      for tmrp625 in tmrp625real
      loop
         v_qtde := 0.000;

         if :new.qtde_reserva_planejada > 0
         then
            v_qtde := (tmrp625.qtde_reserva_planejada / :new.qtde_reserva_planejada) * :new.qtde_areceber_programada;
         end if;

         inter_pr_atualiza_estrutura(tmrp625.ordem_planejamento,
                                     tmrp625.pedido_venda,
                                     tmrp625.pedido_reserva,
                                     :new.nivel_produto_origem,
                                     :new.grupo_produto_origem,
                                     :new.subgrupo_produto_origem,
                                     :new.item_produto_origem,
                                     :new.alternativa_produto_origem,
                                     v_qtde);

         begin
            insert into tmrp_625
               (ordem_planejamento,                   pedido_venda,
                pedido_reserva,                       codigo_projeto,
                seq_projeto_origem,                   tipo_produto_origem,
                seq_produto_origem,                   nivel_produto_origem,
                grupo_produto_origem,                 subgrupo_produto_origem,
                item_produto_origem,                  alternativa_produto_origem,
                tipo_produto,                         seq_projeto,
                seq_produto,                          nivel_produto,
                grupo_produto,                        subgrupo_produto,
                item_produto,                         alternativa_produto,
                estagio_producao,                     tipo_calculo,
                consumo,                              qtde_reserva_planejada,
                qtde_reserva_programada,
                situacao,
                qtde_areceber_programada,             qtde_vendida,
                qtde_adicional,                       qtde_reposicao,
                data_requerida,                       qtde_produzida_comprada,
                qtde_conserto,                        qtde_2qualidade,
                qtde_perdas,                          qtde_atendida_adicional,
                qtde_atendida_devolvida,              data_requerida_prev,
                data_requerida_real,                  data_acertada_compra,
                data_acertada_desenv,                 observacao,
                setor,                                relacao_banho,
                qtde_atendida_estoque,                cons_unid_med_generica,
                unidades_reserva_planejada,           unidades_reserva_programada,
                unidades_areceber_programada,         tecido_principal,
                liquida_saldo
            )values(
                tmrp625.ordem_planejamento,          tmrp625.pedido_venda,
                tmrp625.pedido_reserva,               tmrp625.codigo_projeto,
                tmrp625.seq_projeto_origem,           tmrp625.tipo_produto_origem,
                tmrp625.seq_produto_origem,           tmrp625.nivel_produto_origem,
                tmrp625.grupo_produto_origem,         tmrp625.subgrupo_produto_origem,
                tmrp625.item_produto_origem,          tmrp625.alternativa_produto_origem,
                tmrp625.tipo_produto,                 tmrp625.seq_projeto,
                :new.seq_produto,                     :new.nivel_produto,
                :new.grupo_produto,                   :new.subgrupo_produto,
                :new.item_produto,                    :new.alternativa_produto,
                tmrp625.estagio_producao,             tmrp625.tipo_calculo,
                tmrp625.consumo,                      v_qtde,
                0.000,
                tmrp625.situacao,
                0.000,                                tmrp625.qtde_vendida,
                tmrp625.qtde_adicional,               tmrp625.qtde_reposicao,
                tmrp625.data_requerida,               tmrp625.qtde_produzida_comprada,
                tmrp625.qtde_conserto,                tmrp625.qtde_2qualidade,
                tmrp625.qtde_perdas,                  tmrp625.qtde_atendida_adicional,
                tmrp625.qtde_atendida_devolvida,      tmrp625.data_requerida_prev,
                tmrp625.data_requerida_real,          tmrp625.data_acertada_compra,
                tmrp625.data_acertada_desenv,         tmrp625.observacao,
                tmrp625.setor,                        tmrp625.relacao_banho,
                tmrp625.qtde_atendida_estoque,        tmrp625.cons_unid_med_generica,
                tmrp625.unidades_reserva_planejada,   tmrp625.unidades_reserva_programada,
                tmrp625.unidades_areceber_programada, tmrp625.tecido_principal,
                tmrp625.liquida_saldo
            );
         exception when
         OTHERS then
            raise_application_error(-20000,'Nao inseriu tmrp_625 ' || sqlerrm);
         end;

         inter_pr_update_tmrp_625_fic(tmrp625.ordem_planejamento,          tmrp625.pedido_venda,
                                      tmrp625.pedido_reserva,              tmrp625.nivel_produto_origem,
                                      tmrp625.grupo_produto_origem,        tmrp625.subgrupo_produto_origem,
                                      tmrp625.item_produto_origem,         tmrp625.nivel_produto,
                                      tmrp625.grupo_produto,               tmrp625.subgrupo_produto,
                                      tmrp625.item_produto,                v_qtde,
                                      tmrp625.seq_produto_origem);
      end loop;
   end if;

   if updating
   then
      --Iterando cursor real
      for tmrp625 in tmrp625real
      loop
         begin
            update tmrp_625
               set tmrp_625.qtde_reserva_planejada = tmrp_625.qtde_reserva_planejada + (:new.qtde_areceber_programada - :old.qtde_areceber_programada)
               where tmrp_625.ordem_planejamento         = tmrp625.ordem_planejamento
                 and tmrp_625.pedido_venda               = tmrp625.pedido_venda
                 and tmrp_625.pedido_reserva             = tmrp625.pedido_reserva
                 and tmrp_625.seq_produto_origem         = tmrp625.seq_produto_origem
                 and tmrp_625.nivel_produto_origem       = tmrp625.nivel_produto_origem
                 and tmrp_625.grupo_produto_origem       = tmrp625.grupo_produto_origem
                 and tmrp_625.subgrupo_produto_origem    = tmrp625.subgrupo_produto_origem
                 and tmrp_625.item_produto_origem        = tmrp625.item_produto_origem
                 and tmrp_625.alternativa_produto_origem = tmrp625.alternativa_produto_origem
                 and tmrp_625.nivel_produto              = :new.nivel_produto
                 and tmrp_625.grupo_produto              = :new.grupo_produto
                 and tmrp_625.subgrupo_produto           = :new.subgrupo_produto
                 and tmrp_625.item_produto               = :new.item_produto
                 and tmrp_625.alternativa_produto        = :new.alternativa_produto
                 and tmrp_625.alternativa_produto_origem = tmrp625.alternativa_produto_origem;
         exception when
         OTHERS then
            raise_application_error(-20000,'Nao atualizou tmrp_625 ');
         end;

         inter_pr_update_tmrp_625_fic(tmrp625.ordem_planejamento,          tmrp625.pedido_venda,
                                      tmrp625.pedido_reserva,              tmrp625.nivel_produto_origem,
                                      tmrp625.grupo_produto_origem,        tmrp625.subgrupo_produto_origem,
                                      tmrp625.item_produto_origem,         tmrp625.nivel_produto,
                                      tmrp625.grupo_produto,               tmrp625.subgrupo_produto,
                                      tmrp625.item_produto,                (:new.qtde_areceber_programada - :old.qtde_areceber_programada),
                                      tmrp625.seq_produto_origem);

      end loop;
   end if;
end inter_tr_tmrp_621_reserva;

-- ALTER TRIGGER "INTER_TR_TMRP_621_RESERVA" ENABLE
 

/

exec inter_pr_recompile;

