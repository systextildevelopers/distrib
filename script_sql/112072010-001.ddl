create table PEDI_100_LIB (
  PEDIDO_VENDA                  NUMBER(9)     default 0 not null,
  TECIDO_PECA                   VARCHAR2(1)   default '',
  COND_PGTO_VENDA               NUMBER(3)     default 0,
  CLI_PED_CGC_CLI9              NUMBER(9)     default 0,
  CLI_PED_CGC_CLI4              NUMBER(4)     default 0,
  CLI_PED_CGC_CLI2              NUMBER(2)     default 0,
  COD_BANCO                     NUMBER(3)     default 0,
  COD_REP_CLIENTE               NUMBER(5)     default 0,
  DATA_DIGIT_VENDA              DATE,
  DATA_ENTR_VENDA               DATE,
  VALOR_TOTAL_PEDI              NUMBER(17,2)  default 0.0,
  VALOR_SALDO_PEDI              NUMBER(17,2)  default 0.0,
  SOLIC_LIBERA                  NUMBER(9)     default 0 not null,
  DATA_SOLIC_LIBERA             DATE,
  MARCA_LIBERA                  NUMBER(1)     default 0
);

exec inter_pr_recompile;
