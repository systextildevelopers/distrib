
  CREATE OR REPLACE PROCEDURE "P_GERA_NFEC" (p_cod_empresa    in  number,
                                        p_cod_id         in  number,
                                        p_cod_canc       in  number,
                                        p_str_usuario    in  varchar2,
                                        p_msg_erro       out varchar2
                                        ) is
-- Created on 6/2/2009 by ANDRE

  cursor c_NFeC is
    select justificativa
           ,numero_danf_nfe
           ,nr_protocolo
           ,e_mail
           ,nfe_e_mail
           ,user_id_e_mail
           ,f_user_e_mail
           ,senha_e_mail
           ,numero
           ,serie
           ,data_emissao
           ,cod_status
           ,cgc_9
           ,cgc_4
           ,cgc_2
           ,dest_nome
           ,cgc_9_emp
           ,cgc_4_emp
           ,cgc_2_emp
           ,cod_rep_cliente
           ,tipo_emissao
           ,ano_inutilizacao
           ,nr_final_inut
           ,dest_sigla_uf
           ,entrada_saida
           ,cnpj9_transp
           ,cnpj4_transp
           ,cnpj2_transp
    from (select m.des_canc_nfiscal as justificativa
           ,o.numero_danf_nfe
           ,o.nr_protocolo
           ,s010.e_mail
           ,s010.nfe_e_mail
           ,h_030.user_id_e_mail
           ,h_030.e_mail as f_user_e_mail
           ,h_030.senha_e_mail
           ,o.documento as numero
           ,o.serie as serie
           ,o.data_emissao
           ,o.cod_status
           ,o.cgc_cli_for_9 as cgc_9
           ,o.cgc_cli_for_4 as cgc_4
           ,o.cgc_cli_for_2 as cgc_2
           ,s010.nome_fornecedor as dest_nome
           ,f500.cgc_9 as cgc_9_emp
           ,f500.cgc_4 as cgc_4_emp
           ,f500.cgc_2 as cgc_2_emp
           ,0 as cod_rep_cliente
           ,0 as tipo_emissao
           ,substr(to_char(o.data_transacao,'DD/MM/YY'),7,2) as ano_inutilizacao
           ,o.documento as nr_final_inut
           ,o.natoper_est_oper as dest_sigla_uf
           ,'E' as  entrada_saida
           ,0 as cnpj9_transp
           ,0 as cnpj4_transp
           ,0 as cnpj2_transp
          from obrf_010 o
              ,fatu_010 m
              ,hdoc_030 h_030
              ,supr_010 s010
              ,fatu_500 f500
          where o.cod_solicitacao_nfe = p_cod_id
            and (o.cod_status = '998' or o.cod_status = '990')
            and m.cod_canc_nfiscal = p_cod_canc
            and s010.fornecedor9   = o.cgc_cli_for_9
            and s010.fornecedor4   = o.cgc_cli_for_4
            and s010.fornecedor2   = o.cgc_cli_for_2
            and h_030.empresa      = p_cod_empresa
            and h_030.usuario      = p_str_usuario
            and f500.codigo_empresa = h_030.empresa)
    union all
    (select m.des_canc_nfiscal as justificativa
           ,f.numero_danf_nfe
           ,f.nr_protocolo
           ,p010.e_mail
           ,p010.nfe_e_mail
           ,h_030.user_id_e_mail
           ,h_030.e_mail as f_user_e_mail
           ,h_030.senha_e_mail
           ,f.num_nota_fiscal as numero
           ,f.serie_nota_fisc as serie
           ,f.data_emissao
           ,f.cod_status
           ,f.cgc_9 as cgc_9
           ,f.cgc_4 as cgc_4
           ,f.cgc_2 as cgc_2
           ,p010.nome_cliente as dest_nome
           ,f500.cgc_9 as cgc_9_emp
           ,f500.cgc_4 as cgc_4_emp
           ,f500.cgc_2 as cgc_2_emp
           ,f.cod_rep_cliente as cod_rep_cliente
           ,1 as tipo_emissao
           ,substr(to_char(f.data_emissao,'DD/MM/YY'),7,2) as ano_inutilizacao
           ,f.num_nota_fiscal as nr_final_inut
           ,f.natop_nf_est_oper as dest_sigla_uf
           ,'S' as  entrada_saida
           ,f.transpor_forne9 as cnpj9_transp
           ,f.transpor_forne4 as cnpj4_transp
           ,f.transpor_forne2 as cnpj2_transp
    from fatu_050 f
        ,fatu_010 m
        ,hdoc_030 h_030
        ,pedi_010 p010
        ,fatu_500 f500
    where f.cod_solicitacao_nfe = p_cod_id
      and (f.cod_status = '998' or f.cod_status = '990')
      and m.cod_canc_nfiscal = p_cod_canc
      and p010.cgc_9         = f.cgc_9
      and p010.cgc_4         = f.cgc_4
      and p010.cgc_2         = f.cgc_2
      and h_030.empresa      = p_cod_empresa
      and h_030.usuario      = p_str_usuario
      and f500.codigo_empresa = h_030.empresa);

 v_erro   EXCEPTION;
 v_ambiente number;
 v_cod_cidade_ibge number;
 v_nr_protocolo fatu_050.nr_protocolo%type;
 v_imprime_boleto_danfe    fatu_503.imprime_boleto_danfe%type;
 v_nfe_e_mail supr_010.nfe_e_mail%type;
 v_e_mail pedi_020.e_mail%type;

begin

  select obrf_158.nfe_ambiente
  into   v_ambiente
  from obrf_158
  where obrf_158.cod_empresa = p_cod_empresa;

  select fatu_503.imprime_boleto_danfe
  into   v_imprime_boleto_danfe
  from fatu_503
  where fatu_503.codigo_empresa = p_cod_empresa;

/*  select substr(basi_160.cod_cidade_ibge,1,2) */
  select to_char(basi_167.codigo_fiscal_uf,'00')
  into   v_cod_cidade_ibge
  from fatu_500,
       basi_160,
       basi_167
   where fatu_500.codigo_empresa = p_cod_empresa
     and fatu_500.codigo_cidade  = basi_160.cod_cidade
     and basi_160.estado         = basi_167.estado
     and basi_160.codigo_pais    = basi_167.codigo_pais;


  BEGIN
    insert into obrf_150
    (cod_empresa
    ,cod_usuario
    ,tipo_ambiente
    ,emit_codigo_uf
    ,tipo_documento)
    values
    (p_cod_empresa
     ,p_cod_id
     ,v_ambiente
     ,v_cod_cidade_ibge
     ,5);
  EXCEPTION
     WHEN OTHERS THEN
        p_msg_erro := 'Nao inseriu dados na tabela obrf_150' || Chr(10) || SQLERRM;
     RAISE v_ERRO;
  END;

  for f_NFeC in c_NFeC
  LOOP
    BEGIN
      if f_NFeC.cod_status <> '990'
      then
         f_NFeC.cod_status := '';
      end if;

      v_nfe_e_mail := '';
      v_e_mail := '';
      
      if f_NFeC.entrada_saida = 'S'
      then
         begin
           select nfe_e_mail 
           into v_nfe_e_mail
           from supr_010
           where supr_010.FORNECEDOR9 = f_NFeC.cnpj9_transp
           and   supr_010.FORNECEDOR4 = f_NFeC.cnpj4_transp
           and   supr_010.FORNECEDOR2 = f_NFeC.cnpj2_transp;
         exception when no_data_found then
              v_nfe_e_mail := '';
         end;  
      
         begin
           select e_mail 
           into v_e_mail
           from pedi_020
           where pedi_020.cod_rep_cliente = f_NFeC.cod_rep_cliente
           and   pedi_020.envia_email in (1,3);
         exception when no_data_found then
              v_e_mail := '';
         end;
      end if;
      
      begin
         select max(nr_protocolo)
         into v_nr_protocolo
         from obrf_400
         where obrf_400.codigo_empresa = p_cod_empresa
           and obrf_400.num_nota_fiscal = f_NFeC.numero
           and obrf_400.serie_nota_fisc = f_NFeC.Serie
           and (( (obrf_400.cgc_9 = f_NFeC.Cgc_9) and
                 (obrf_400.cgc_4 = f_NFeC.Cgc_4) and
                 (obrf_400.cgc_2 = f_NFeC.Cgc_2)) or (obrf_400.cgc_9 is null) )
           and obrf_400.cod_status = 100;
       exception when no_data_found then
            v_nr_protocolo := f_NFeC.Nr_Protocolo;
      end;

      if trim(v_nr_protocolo) is null
      then v_nr_protocolo := f_NFeC.Nr_Protocolo;
      end if;



      insert into obrf_150
      (obrf_150.cod_empresa       /*1*/
      ,obrf_150.cod_usuario       /*2*/
      ,obrf_150.tipo_ambiente     /*3*/
      ,obrf_150.nr_protocolo      /*4*/
      ,obrf_150.justificativa     /*5*/
      ,obrf_150.tipo_documento    /*6*/
      ,obrf_150.emit_codigo_uf    /*7*/
      ,obrf_150.chave_acesso      /*8*/
      ,obrf_150.dest_e_mail       /*9*/
      ,obrf_150.dest_nfe_e_mail   /*10*/
      ,obrf_150.user_id_e_mail    /*11*/
      ,obrf_150.user_e_mail       /*12*/
      ,obrf_150.user_senha_e_mail /*13*/
      ,obrf_150.numero            /*14*/
      ,obrf_150.serie             /*15*/
      ,obrf_150.data_emissao      /*16*/
      ,obrf_150.cod_status        /*17*/
      ,obrf_150.dest_cnpj9        /*18*/
      ,obrf_150.dest_cnpj4        /*19*/
      ,obrf_150.dest_cnpj2        /*20*/
      ,obrf_150.dest_nome         /*21*/
      ,obrf_150.emit_cnpj9        /*22*/
      ,obrf_150.emit_cnpj4        /*23*/
      ,obrf_150.emit_cnpj2        /*24*/
      ,obrf_150.tipo_emissao      /*25*/
      ,obrf_150.ano_inutilizacao  /*26*/
      ,obrf_150.nr_final_inut    /*27*/
      ,obrf_150.dest_sigla_uf    /*28*/
      ,ind_envia_boleto_email    /*29*/
      ,obrf_150.transp_email     /*30*/
      ,obrf_150.repre_email )    /*31*/
      values
      (p_cod_empresa              /*1*/
      ,p_cod_id                   /*2*/
      ,v_ambiente                 /*3*/
      ,v_nr_protocolo             /*4*/
      ,trim(f_NFeC.justificativa) /*5*/
      ,2                          /*6*/
      ,v_cod_cidade_ibge          /*7*/
      ,f_NFeC.numero_danf_nfe     /*8*/
      ,f_NFeC.e_mail              /*9*/
      ,f_NFeC.nfe_e_mail          /*10*/
      ,f_NFeC.user_id_e_mail      /*11*/
      ,f_NFeC.f_user_e_mail       /*12*/
      ,f_NFeC.senha_e_mail        /*13*/
      ,f_NFeC.numero              /*14*/
      ,f_NFeC.serie               /*15*/
      ,f_NFeC.data_emissao        /*16*/
      ,f_NFeC.cod_status          /*17*/
      ,f_NFeC.cgc_9               /*18*/
      ,f_NFeC.cgc_4               /*19*/
      ,f_NFeC.cgc_2               /*20*/
      ,f_NFeC.Dest_Nome           /*21*/
      ,f_NFeC.Cgc_9_Emp           /*22*/
      ,f_NFeC.Cgc_4_Emp           /*23*/
      ,f_NFeC.Cgc_2_Emp           /*24*/
      ,f_NFeC.Tipo_Emissao        /*25*/
      ,f_NFeC.Ano_Inutilizacao    /*26*/
      ,f_NFeC.Nr_Final_Inut       /*27*/
      ,f_NFeC.Dest_Sigla_Uf       /*28*/
      ,v_imprime_boleto_danfe     /*29*/
      ,v_nfe_e_mail               /*30*/
      ,v_e_mail);                 /*31*/
    EXCEPTION
       WHEN OTHERS THEN
          p_msg_erro := 'Nao inseriu dados na tabela obrf_150' || Chr(10) || SQLERRM;
        RAISE v_ERRO;
    END;

  END LOOP;

end;

 

/

exec inter_pr_recompile;

