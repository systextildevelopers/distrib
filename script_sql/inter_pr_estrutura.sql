
  CREATE OR REPLACE PROCEDURE "INTER_PR_ESTRUTURA" 
   (p_empresa_logada  in number,
    p_nivel           in varchar2,
    p_grupo           in varchar2,
    p_subgrupo        in varchar2,
    p_item            in varchar2,
    p_alternativa     in number,
    p_qtde_pedida     in number,
    p_nivel_material  in varchar2,
    p_tipo_material   in number,
    p_codigo_usuario  in number,
    p_usuario         in varchar2,
    p_nr_solicitacao  in number,
    flag_encon_estru  in out varchar2,
    p_nome_programa   in varchar2,
    p_pedido_venda    in number,
    p_codigo_projeto  in varchar2,
    p_inc_exc_aplic   in number,
    p_inc_exc_produto in number,
    p_visualiza_mp    in number
) is

   TYPE nivelComp IS TABLE OF basi_030.nivel_estrutura%TYPE;
   p_nivel_comp nivelComp;

   TYPE grupoComp IS TABLE OF basi_030.referencia%TYPE;
   p_grupo_comp grupoComp;

   TYPE subgrupoComp IS TABLE OF basi_020.tamanho_ref%TYPE;
   p_sub_comp subgrupoComp;

   TYPE itemComp IS TABLE OF basi_010.item_estrutura%TYPE;
   p_item_comp itemComp;

   TYPE consumo IS TABLE OF basi_050.consumo%TYPE;
   p_consumo consumo;

   TYPE alternativaComp IS TABLE OF basi_050.alternativa_item%TYPE;
   p_alternativa_comp alternativaComp;

   TYPE sequenciaComp IS TABLE OF basi_050.sequencia%TYPE;
   p_sequencia sequenciaComp;

   TYPE subgrupoItem IS TABLE OF basi_020.tamanho_ref%TYPE;
   p_sub_item subgrupoItem;

   TYPE itemItem IS TABLE OF basi_010.item_estrutura%TYPE;
   p_item_item itemItem;

   p_alternativa_aux basi_050.alternativa_item%TYPE;
   p_aprogramar      number;
   flag_continue     varchar2(1);
   p_comprado_fabric integer;
   selecionou_dados  integer;

BEGIN

   /*Se alternativa vier zero no parametro, a procedure encontra-a com o item enviado*/
   if p_alternativa = 0
   then
      begin
         select basi_010.numero_alternati
         into   p_alternativa_aux
         from basi_010
         where basi_010.nivel_estrutura  = p_nivel
           and basi_010.grupo_estrutura  = p_grupo
           and basi_010.subgru_estrutura = p_subgrupo
           and basi_010.item_estrutura   = p_item;
         exception
            when others then
               p_alternativa_aux := 0;
      end;

   else

      p_alternativa_aux := p_alternativa;

   end if;

   begin

      flag_encon_estru := 'n';

      begin
         select/*parallel(basi_050, 20)*/
                basi_050.nivel_comp,       basi_050.grupo_comp,
                basi_050.sub_comp,         basi_050.item_comp,
                basi_050.alternativa_comp, basi_050.sequencia,
                basi_050.sub_item,         basi_050.item_item,
                basi_050.consumo
         BULK COLLECT INTO
                p_nivel_comp,              p_grupo_comp,
                p_sub_comp,                p_item_comp,
                p_alternativa_comp,        p_sequencia,
                p_sub_item,                p_item_item,
                p_consumo
         from basi_050
         where  basi_050.nivel_item       = p_nivel
           and  basi_050.grupo_item       = p_grupo
           and (basi_050.sub_item         = p_subgrupo
            or  basi_050.sub_item         = '000')
           and (basi_050.item_item        = p_item
            or  basi_050.item_item        = '000000')
           and  basi_050.alternativa_item = p_alternativa_aux;
      end;

      if sql%found
      then

         flag_encon_estru := 's';

         FOR i IN p_sequencia.FIRST .. p_sequencia.LAST
         LOOP

            if p_sub_comp(i) = '000' or p_consumo(i) = 0.0000
            then

               begin
                  select basi_040.sub_comp, basi_040.consumo
                  into   p_sub_comp(i),     p_consumo(i)
                  from basi_040
                  where basi_040.nivel_item       = p_nivel
                    and basi_040.grupo_item       = p_grupo
                    and basi_040.sub_item         = p_subgrupo
                    and basi_040.item_item        = p_item_item(i)
                    and basi_040.sequencia        = p_sequencia(i)
                    and basi_040.alternativa_item = p_alternativa_aux;
                  exception
                     when others then
                        p_sub_comp(i) := '000';
                        p_consumo(i)  := 0.0000;
               end;
            end if;

            if p_item_comp(i) = '000000'
            then
               begin
                  select basi_040.item_comp
                  into   p_item_comp(i)
                  from basi_040
                  where basi_040.nivel_item       = p_nivel
                    and basi_040.grupo_item       = p_grupo
                    and basi_040.sub_item         = p_sub_item(i)
                    and basi_040.item_item        = p_item
                    and basi_040.sequencia        = p_sequencia(i)
                    and basi_040.alternativa_item = p_alternativa_aux;
               exception when others then
                  p_item_comp(i) := '000000';
               end;
            end if;

            flag_continue := 's';

            selecionou_dados := 0;

            begin
               select 1
               into selecionou_dados
               from basi_030
               where basi_030.nivel_estrutura = p_nivel_comp(i)
                 and basi_030.referencia      = p_grupo_comp(i)

                 /* PRODUTO */
                 and ((p_inc_exc_produto = 1
                 and   exists (select /*parallel(rcnb_060, 20)*/ 1 from rcnb_060       /* INCLUSAO */
                               where rcnb_060.tipo_registro        = 599
                                 and rcnb_060.nr_solicitacao       = 25
                                 and rcnb_060.subgru_estrutura     = p_codigo_usuario
                                 and rcnb_060.usuario_rel          = p_usuario
                                 and rcnb_060.empresa_rel          = p_empresa_logada
                                 and rcnb_060.relatorio_rel        = 'tmrp_f025'
                                 and rcnb_060.tipo_registro_rel    = p_nr_solicitacao
                                 and rcnb_060.nivel_estrutura_str  in (basi_030.nivel_estrutura, 'X')
                                 and rcnb_060.grupo_estrutura_str  in (basi_030.referencia, 'XXXXXX')))
                  or  (p_inc_exc_produto = 2
                  and  not exists (select /*parallel(rcnb_060, 20)*/ 1 from rcnb_060   /* EXCLUSAO */
                                   where rcnb_060.tipo_registro        = 599
                                     and rcnb_060.nr_solicitacao       = 25
                                     and rcnb_060.subgru_estrutura     = p_codigo_usuario
                                     and rcnb_060.usuario_rel          = p_usuario
                                     and rcnb_060.empresa_rel          = p_empresa_logada
                                     and rcnb_060.relatorio_rel        = 'tmrp_f025'
                                     and rcnb_060.tipo_registro_rel    = p_nr_solicitacao
                                     and rcnb_060.nivel_estrutura_str  in (basi_030.nivel_estrutura,'X')
                                     and (rcnb_060.grupo_estrutura_str = basi_030.referencia
                                     or   rcnb_060.grupo_estrutura_str = 'XXXXXX'))
                  or exists (select /*parallel(rcnb_060, 20)*/ 1 from rcnb_060
                             where rcnb_060.tipo_registro        = 599
                               and rcnb_060.nr_solicitacao       = 25
                               and rcnb_060.subgru_estrutura     = p_codigo_usuario
                               and rcnb_060.usuario_rel          = p_usuario
                               and rcnb_060.empresa_rel          = p_empresa_logada
                               and rcnb_060.relatorio_rel        = 'tmrp_f025'
                               and rcnb_060.tipo_registro_rel    = p_nr_solicitacao
                               and rcnb_060.nivel_estrutura_str  in (basi_030.nivel_estrutura,'X')
                               and rcnb_060.grupo_estrutura_str  = 'XXXXXX')));
            exception when others then
               selecionou_dados := 0;
            end;

            if selecionou_dados = 0
            then
               flag_continue := 'n';
            end if;

/*            if  p_nivel_comp(i) = '2'
            and p_grupo_comp(i) <> '00018'
            and flag_continue = 's'
            then
raise_application_error(-20000, 'Produto f' || p_grupo_comp(i));
            end if;
*/
            if  p_nivel_comp(i) = '2'
            and flag_continue   = 's'
            then
               selecionou_dados := 0;

               /* APLICACAO */
               begin
                  select 1
                  into selecionou_dados
                  from tmrp_615
                  where tmrp_615.nr_solicitacao = p_nr_solicitacao
                    and tmrp_615.codigo_usuario = p_codigo_usuario
                    and tmrp_615.tipo_registro  = 345
                    and tmrp_615.usuario        = p_usuario
                    and tmrp_615.codigo_empresa = p_empresa_logada
                    and tmrp_615.nome_programa  = p_nome_programa
                    and tmrp_615.nivel          = p_nivel_comp(i)
                    and tmrp_615.grupo          = p_grupo_comp(i)
                    and tmrp_615.subgrupo       = p_sub_comp(i)
                    and tmrp_615.item           = '000000'

                    and rownum                  = 1

                    and ((p_inc_exc_aplic = 1
                    and   tmrp_615.sequencia_projeto in (select rcnb_060.grupo_estrutura
                                                         from rcnb_060
                                                         where rcnb_060.tipo_registro     = 598
                                                           and rcnb_060.nr_solicitacao    = 25
                                                           and rcnb_060.subgru_estrutura  = p_codigo_usuario
                                                           and rcnb_060.usuario_rel       = p_usuario
                                                           and rcnb_060.empresa_rel       = p_empresa_logada
                                                           and rcnb_060.relatorio_rel     = 'tmrp_f025'
                                                           and rcnb_060.tipo_registro_rel = p_nr_solicitacao))
                    or  (p_inc_exc_aplic = 2
                    and  tmrp_615.sequencia_projeto not in (select rcnb_060.grupo_estrutura
                                                            from rcnb_060
                                                            where rcnb_060.tipo_registro     = 598
                                                              and rcnb_060.nr_solicitacao    = 25
                                                              and rcnb_060.subgru_estrutura  = p_codigo_usuario
                                                              and rcnb_060.usuario_rel       = p_usuario
                                                              and rcnb_060.empresa_rel       = p_empresa_logada
                                                              and rcnb_060.relatorio_rel     = 'tmrp_f025'
                                                              and rcnb_060.tipo_registro_rel = p_nr_solicitacao)));
               exception when others then
                  selecionou_dados := 0;
               end;

               if selecionou_dados = 0
               then
                  flag_continue := 'n';
               end if;
            end if;

            /*Se for conjunto chama novamente a procedure para explodir os tecidos/aviamentos.*/
            if p_nivel_comp(i) = '1'
            then
               inter_pr_estrutura(p_empresa_logada,
                                  p_nivel_comp(i),
                                  p_grupo_comp(i),
                                  p_sub_comp(i),
                                  p_item_comp(i),
                                  p_alternativa_comp(i),
                                  p_qtde_pedida,
                                  p_nivel_material,
                                  p_tipo_material,
                                  p_codigo_usuario,
                                  p_usuario,
                                  p_nr_solicitacao,
                                  flag_encon_estru,
                                  p_nome_programa,
                                  p_pedido_venda,
                                  p_codigo_projeto,
                                  p_inc_exc_aplic,
                                  p_inc_exc_produto,
                                  p_visualiza_mp);

               flag_continue := 'n';

            end if;

            /*TMRP_F025 - Processo de necessidades liquidas de projeto*/
            if p_nome_programa = 'tmrp_f025'
            then

               /*Grava dados na tmrp_615*/
               if   flag_continue    = 's'
               and ((p_nivel_material = p_nivel_comp(i))
               or   ((p_nivel_comp(i) = '2' or p_nivel_comp(i) = '9') and p_nivel_material = '0'))
               then

                  begin
                     select basi_030.comprado_fabric
                     into   p_comprado_fabric
                     from basi_030
                     where basi_030.nivel_estrutura  = p_nivel_comp(i)
                       and basi_030.referencia       = p_grupo_comp(i);
                     exception
                        when others then
                           p_comprado_fabric := 0;
                  end;

                  if p_tipo_material = p_comprado_fabric
                  or p_tipo_material = 3
                  then

                     p_aprogramar := p_qtde_pedida * p_consumo(i);

                     inter_pr_grava_tmrp_615_item(p_empresa_logada,
                                                  p_nivel,
                                                  p_grupo,
                                                  p_item_item(i),
                                                  0,
                                                  p_qtde_pedida,
                                                  p_nivel_comp(i),
                                                  p_grupo_comp(i),
                                                  p_sub_comp(i),
                                                  p_item_comp(i),
                                                  p_codigo_usuario,
                                                  p_usuario,
                                                  p_nr_solicitacao,
                                                  'tmrp_f026',
                                                  256,
                                                  'basi_050',
                                                  p_consumo(i),
                                                  p_pedido_venda,
                                                  p_codigo_projeto,
                                                  8);


                     inter_pr_grava_tmrp_615(p_empresa_logada,
                                             p_nivel_comp(i),
                                             p_grupo_comp(i),
                                             p_sub_comp(i),
                                             p_item_comp(i),
                                             p_aprogramar,
                                             p_codigo_usuario,
                                             p_usuario,
                                             p_nr_solicitacao,
                                             p_nome_programa,
                                             255,
                                             8,
                                             p_visualiza_mp);

                  end if;
               end if;
            end if; /*Fim - Processo TMRP_F025*/
         END LOOP;
      end if;
   end;
end inter_pr_estrutura;

 

/

exec inter_pr_recompile;

