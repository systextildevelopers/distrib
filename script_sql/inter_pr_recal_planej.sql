
  CREATE OR REPLACE PROCEDURE "INTER_PR_RECAL_PLANEJ" (
p_ordem_planejamento              number
) is
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   v_count_reg               integer;

function f_tem_area (p_ordem_planejamento in number,
                      p_area               in number)
RETURN integer is
   tem_area integer;
BEGIN
   tem_area := 0;

   begin
     select nvl(count(1),0)
     into tem_area
     from tmrp_637
     where tmrp_637.ordem_planejamento = p_ordem_planejamento
       and tmrp_637.area_producao      = p_area;
   exception when others then
      tem_area := 0;
   end;

   return(tem_area);

END;

procedure f_erro_recalculo (p1_ordem_planejamento in number,
                            p1_area               in number)
IS

BEGIN
    /*
       Marca os produtos da tmrp_630 cujo produto nao esta mais na estrutura do produto da tmrp_625
    */
    for reg_tmrp_630 in (select tmrp_630.ordem_planejamento,
                                tmrp_630.pedido_venda,
                                tmrp_630.pedido_reserva,
                                tmrp_630.nivel_produto,
                                tmrp_630.grupo_produto,
                                tmrp_630.subgrupo_produto,
                                tmrp_630.item_produto,
                                tmrp_630.alternativa_produto
                         from tmrp_630
                         where tmrp_630.ordem_planejamento = p1_ordem_planejamento
                           and tmrp_630.area_producao      = p1_area)
    loop
       begin
          select nvl(count(*),0)
          into v_count_reg
          from tmrp_625
          where tmrp_625.ordem_planejamento   = reg_tmrp_630.ordem_planejamento
            and tmrp_625.pedido_venda         = reg_tmrp_630.pedido_venda
            and tmrp_625.pedido_reserva       = reg_tmrp_630.pedido_reserva
            and tmrp_625.nivel_produto        = reg_tmrp_630.nivel_produto
            and tmrp_625.grupo_produto        = reg_tmrp_630.grupo_produto
            and tmrp_625.subgrupo_produto     = reg_tmrp_630.subgrupo_produto
            and tmrp_625.item_produto         = reg_tmrp_630.item_produto
            and tmrp_625.alternativa_produto  = reg_tmrp_630.alternativa_produto;
       exception when others then
          v_count_reg := 0;
       end;

       /*
          Se nao encontrar o produto na tmrp_625, deve marcar o registro da tmrp_630 com ERRO
       */
       if v_count_reg = 0
       then
          begin
             update tmrp_630
             set   tmrp_630.erro_recalculo       = 1         -- tem erro neste produto
             where tmrp_630.ordem_planejamento   = reg_tmrp_630.ordem_planejamento
               and tmrp_630.pedido_venda         = reg_tmrp_630.pedido_venda
               and tmrp_630.pedido_reserva       = reg_tmrp_630.pedido_reserva
               and tmrp_630.area_producao        = p1_area
               and tmrp_630.nivel_produto        = reg_tmrp_630.nivel_produto
               and tmrp_630.grupo_produto        = reg_tmrp_630.grupo_produto
               and tmrp_630.subgrupo_produto     = reg_tmrp_630.subgrupo_produto
               and tmrp_630.item_produto         = reg_tmrp_630.item_produto
               and tmrp_630.alternativa_produto  = reg_tmrp_630.alternativa_produto;
          exception when others then
             -- ATENCAO! Nao eliminou {0}. Mensagem do banco de dados: {1}
             raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26375', 'TMRP_630(E630)' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
          end;
       end if;
    end loop;  --reg_tmrp_630
END;

procedure f_erro_recalculo_031 (p1_ordem_planejamento in number)
IS

BEGIN
   /*
      Marca os produtos da tmrp_630 cujo produto nao esta mais na estrutura do produto da tmrp_625
   */
   for reg_pcpb_031 in (select pcpb_031.ordem_planejamento,
                               pcpb_031.pedido_venda,
                               pcpb_031.nivel_prod,
                               pcpb_031.grupo_prod,
                               pcpb_031.subgrupo_prod,
                               pcpb_031.item_prod,
                               pcpb_031.pano_nivel99,
                               pcpb_031.pano_grupo,
                               pcpb_031.pano_subgrupo,
                               pcpb_031.pano_item
                        from pcpb_031
                        where pcpb_031.ordem_planejamento = p1_ordem_planejamento)
   loop
      begin
         select nvl(count(*),0)
         into v_count_reg
         from tmrp_625
         where tmrp_625.ordem_planejamento      = reg_pcpb_031.ordem_planejamento
           and tmrp_625.pedido_venda            = reg_pcpb_031.pedido_venda
           and tmrp_625.nivel_produto_origem    = reg_pcpb_031.nivel_prod
           and tmrp_625.grupo_produto_origem    = reg_pcpb_031.grupo_prod
           and tmrp_625.subgrupo_produto_origem = reg_pcpb_031.subgrupo_prod
           and tmrp_625.item_produto_origem     = reg_pcpb_031.item_prod
           and tmrp_625.nivel_produto           = reg_pcpb_031.pano_nivel99
           and tmrp_625.grupo_produto           = reg_pcpb_031.pano_grupo
           and tmrp_625.subgrupo_produto        = reg_pcpb_031.pano_subgrupo
           and tmrp_625.item_produto            = reg_pcpb_031.pano_item;
      exception when others then
         v_count_reg := 0;
      end;

      /*
         Se nao encontrar o produto na tmrp_625, deve marcar o registro da tmrp_630 com ERRO
      */
      if v_count_reg = 0
      then
         begin
            update tmrp_630
            set   tmrp_630.erro_recalculo       = 1         -- tem erro neste produto
            where tmrp_630.ordem_planejamento   = reg_pcpb_031.ordem_planejamento
              and tmrp_630.pedido_venda         = reg_pcpb_031.pedido_venda
              and tmrp_630.area_producao        = 2
              and tmrp_630.nivel_produto        = reg_pcpb_031.pano_nivel99
              and tmrp_630.grupo_produto        = reg_pcpb_031.pano_grupo
              and tmrp_630.subgrupo_produto     = reg_pcpb_031.pano_subgrupo
              and tmrp_630.item_produto         = reg_pcpb_031.pano_item;
         exception when others then
            -- ATENCAO! Nao eliminou {0}. Mensagem do banco de dados: {1}
            raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26375', 'TMRP_630(E31)' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
         end;
      end if;
   end loop;  --reg_tmrp_630
END;

procedure f_erro_recalculo_030 (p1_ordem_planejamento in number)
IS

BEGIN
   /*
      Marca os produtos da tmrp_630 cujo produto nao esta mais na estrutura do produto da tmrp_625
   */
   for reg_tmrp_630 in (select tmrp_630.ordem_planejamento,
                               tmrp_630.pedido_venda,
                               tmrp_630.pedido_reserva,
                               tmrp_630.nivel_produto,
                               tmrp_630.grupo_produto,
                               tmrp_630.subgrupo_produto,
                               tmrp_630.item_produto,
                               tmrp_630.alternativa_produto
                        from tmrp_630
                        where tmrp_630.ordem_planejamento = p1_ordem_planejamento
                          and tmrp_630.area_producao      = 2
                          and not exists (select 1
                                          from pcpb_031
                                          where pcpb_031.ordem_planejamento = tmrp_630.ordem_planejamento
                                            and pcpb_031.ordem_producao     = tmrp_630.ordem_prod_compra))
   loop
      begin
         select nvl(count(*),0)
         into v_count_reg
         from tmrp_625
         where tmrp_625.ordem_planejamento   = reg_tmrp_630.ordem_planejamento
           and tmrp_625.pedido_venda         = reg_tmrp_630.pedido_venda
           and tmrp_625.pedido_reserva         = reg_tmrp_630.pedido_reserva
           and tmrp_625.nivel_produto        = reg_tmrp_630.nivel_produto
           and tmrp_625.grupo_produto        = reg_tmrp_630.grupo_produto
           and tmrp_625.subgrupo_produto     = reg_tmrp_630.subgrupo_produto
           and tmrp_625.item_produto         = reg_tmrp_630.item_produto
           and tmrp_625.alternativa_produto  = reg_tmrp_630.alternativa_produto;
      exception when others then
         v_count_reg := 0;
      end;

      /*
         Se nao encontrar o produto na tmrp_625, deve marcar o registro da tmrp_630 com ERRO
      */
      if v_count_reg = 0
      then
         begin
            update tmrp_630
            set   tmrp_630.erro_recalculo       = 1         -- tem erro neste produto
            where tmrp_630.ordem_planejamento   = reg_tmrp_630.ordem_planejamento
              and tmrp_630.pedido_venda         = reg_tmrp_630.pedido_venda
              and tmrp_630.pedido_reserva       = reg_tmrp_630.pedido_reserva
              and tmrp_630.area_producao        = 2
              and tmrp_630.nivel_produto        = reg_tmrp_630.nivel_produto
              and tmrp_630.grupo_produto        = reg_tmrp_630.grupo_produto
              and tmrp_630.subgrupo_produto     = reg_tmrp_630.subgrupo_produto
              and tmrp_630.item_produto         = reg_tmrp_630.item_produto
              and tmrp_630.alternativa_produto  = reg_tmrp_630.alternativa_produto
              and not exists (select 1
                              from pcpb_031
                              where pcpb_031.ordem_planejamento = tmrp_630.ordem_planejamento
                                and pcpb_031.ordem_producao     = tmrp_630.ordem_prod_compra);
         exception when others then
            -- ATENCAO! Nao eliminou {0}. Mensagem do banco de dados: {1}
            raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26375', 'TMRP_630(E30)' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
         end;
      end if;
   end loop;  --reg_tmrp_630
END;

/*
   Marca os produtos da tmrp_630 cujo produto nao esta mais na estrutura do produto da tmrp_625
*/
procedure f_erro_recalculo_016 (p1_ordem_planejamento in number)
IS

BEGIN
   for reg_tmrp_630 in (select tmrp_630.ordem_prod_compra,
                               tmrp_630.nivel_produto,
                               tmrp_630.grupo_produto,
                               tmrp_630.subgrupo_produto,
                               tmrp_630.item_produto,
                               tmrp_630.alternativa_produto
                        from tmrp_630
                        where tmrp_630.ordem_planejamento = p1_ordem_planejamento
                          and tmrp_630.area_producao      = 4
                        group by tmrp_630.ordem_prod_compra,
                                 tmrp_630.nivel_produto,
                                 tmrp_630.grupo_produto,
                                 tmrp_630.subgrupo_produto,
                                 tmrp_630.item_produto,
                                 tmrp_630.alternativa_produto)
   loop
      for reg_pcpt_016   in (select pcpt_016.ordem_pedido
                             from pcpt_016
                             where pcpt_016.ordem_tecelagem = reg_tmrp_630.ordem_prod_compra)
      loop
         for reg_origem in   (select tmrp_630.ordem_planejamento,         tmrp_630.pedido_venda,
                                     tmrp_630.pedido_reserva,
                                     tmrp_625.nivel_produto_origem,       tmrp_625.grupo_produto_origem,
                                     tmrp_625.subgrupo_produto_origem,    tmrp_625.item_produto_origem,
                                     tmrp_625.alternativa_produto_origem, tmrp_630.area_producao,
                                     tmrp_630.ordem_prod_compra,          tmrp_625.seq_produto,
                                     tmrp_625.seq_produto_origem
                              from tmrp_630, tmrp_625
                              where tmrp_630.ordem_planejamento         = tmrp_625.ordem_planejamento
                                and tmrp_630.pedido_venda               = tmrp_625.pedido_venda
                                and tmrp_630.pedido_reserva             = tmrp_625.pedido_reserva
                                and tmrp_630.nivel_produto              = tmrp_625.nivel_produto_origem
                                and tmrp_630.grupo_produto              = tmrp_625.grupo_produto_origem
                                and tmrp_630.subgrupo_produto           = tmrp_625.subgrupo_produto_origem
                                and tmrp_630.item_produto               = tmrp_625.item_produto_origem
                                and tmrp_630.alternativa_produto        = tmrp_625.alternativa_produto_origem
                                and tmrp_630.area_producao              = 2                         -- Beneficiamento.
                                and tmrp_630.ordem_prod_compra          = reg_pcpt_016.ordem_pedido -- OB que esta sendo destinada.
                                and tmrp_625.nivel_produto              = reg_tmrp_630.nivel_produto    -- Produto da ordem de tecelagem.
                                and tmrp_625.grupo_produto              = reg_tmrp_630.grupo_produto
                                and tmrp_625.subgrupo_produto           = reg_tmrp_630.subgrupo_produto
                                and tmrp_625.item_produto               = reg_tmrp_630.item_produto
                                and tmrp_625.alternativa_produto        = reg_tmrp_630.alternativa_produto    -- Alternativa da ordem de tecelagem.
                                and tmrp_625.ordem_planejamento         = p1_ordem_planejamento
                              group by tmrp_630.ordem_planejamento,         tmrp_630.pedido_venda,
                                       tmrp_630.pedido_venda,
                                       tmrp_625.nivel_produto_origem,       tmrp_625.grupo_produto_origem,
                                       tmrp_625.subgrupo_produto_origem,    tmrp_625.item_produto_origem,
                                       tmrp_625.alternativa_produto_origem, tmrp_630.area_producao,
                                       tmrp_630.ordem_prod_compra,          tmrp_625.seq_produto,
                                       tmrp_625.seq_produto_origem)
         loop
            begin
               select nvl(count(*),0)
               into v_count_reg
               from tmrp_625
               where tmrp_625.ordem_planejamento         = p1_ordem_planejamento
                 and tmrp_625.pedido_venda               = reg_origem.pedido_venda
                 and tmrp_625.pedido_reserva             = reg_origem.pedido_reserva
                 and tmrp_625.nivel_produto_origem       = reg_origem.nivel_produto_origem
                 and tmrp_625.grupo_produto_origem       = reg_origem.grupo_produto_origem
                 and tmrp_625.subgrupo_produto_origem    = reg_origem.subgrupo_produto_origem
                 and tmrp_625.item_produto_origem        = reg_origem.item_produto_origem
                 and tmrp_625.alternativa_produto_origem = reg_origem.alternativa_produto_origem
                 and tmrp_625.nivel_produto              = reg_tmrp_630.nivel_produto
                 and tmrp_625.grupo_produto              = reg_tmrp_630.grupo_produto
                 and tmrp_625.subgrupo_produto           = reg_tmrp_630.subgrupo_produto
                 and tmrp_625.item_produto               = reg_tmrp_630.item_produto
                 and tmrp_625.alternativa_produto        = reg_tmrp_630.alternativa_produto;
            exception when others then
               v_count_reg := 0;
            end;

            /*
               Se nao encontrar o produto na tmrp_625, deve marcar o registro da tmrp_630 com ERRO
            */
            if v_count_reg = 0
            then
               begin
                  update tmrp_630
                  set   tmrp_630.erro_recalculo       = 1         -- tem erro neste produto
                  where tmrp_630.ordem_planejamento   = p1_ordem_planejamento
                    and tmrp_630.pedido_venda         = reg_origem.pedido_venda
                    and tmrp_630.pedido_reserva       = reg_origem.pedido_reserva
                    and tmrp_630.area_producao        = 4
                    and tmrp_630.nivel_produto        = reg_tmrp_630.nivel_produto
                    and tmrp_630.grupo_produto        = reg_tmrp_630.grupo_produto
                    and tmrp_630.subgrupo_produto     = reg_tmrp_630.subgrupo_produto
                    and tmrp_630.item_produto         = reg_tmrp_630.item_produto
                    and tmrp_630.alternativa_produto  = reg_tmrp_630.alternativa_produto;
               exception when others then
                  -- ATENCAO! Nao eliminou {0}. Mensagem do banco de dados: {1}
                  raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26375', 'TMRP_630(E31)' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
               end;
            end if;
         end loop; -- reg_origem
      end loop; -- reg_pcpt_010
   end loop; -- reg_tmrp_630
END;

-- ?????????????????????????????????????????????????????????????              UNIDADES
begin
   -- Dados do usuario logad
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   -- Deleta a tmrp_637 da ordem de planejamento
   begin
      delete from tmrp_637
      where tmrp_637.ordem_planejamento = p_ordem_planejamento;
   exception when others then
      -- ATENCAO! Nao eliminou {0}. Mensagem do banco de dados: {1}
      raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26373', 'TMRP_637' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
   end;

   -- Cria umm backup da tmrp_630 desta ordem de planejamento
   -- tmrp_637 e uma copia da tmrp_630, todos os campso que tiver na tmrp_630 deve ter na tmrp_637
   begin
      insert into tmrp_637 (
             ordem_planejamento,                      area_producao,
             ordem_prod_compra,
             nivel_produto,                           grupo_produto,
             subgrupo_produto,                        item_produto,
             qtde_produzida_comprada,                 qtde_conserto,
             qtde_2qualidade,                         qtde_perdas,
             pedido_venda,                            programado_comprado, 
             alternativa_produto,                     qtde_atendida_estoque)
      select tmrp_630.ordem_planejamento,             tmrp_630.area_producao,
             tmrp_630.ordem_prod_compra,
             tmrp_630.nivel_produto,                  tmrp_630.grupo_produto,
             tmrp_630.subgrupo_produto,               tmrp_630.item_produto,
             tmrp_630.qtde_produzida_comprada,        tmrp_630.qtde_conserto,
             tmrp_630.qtde_2qualidade,                tmrp_630.qtde_perdas,
             tmrp_630.pedido_venda,                   tmrp_630.programado_comprado,
             tmrp_630.alternativa_produto,            tmrp_630.qtde_atendida_estoque
      from tmrp_630
      where tmrp_630.ordem_planejamento = p_ordem_planejamento;
   exception when others then
      -- ATENCAO! Nao inseriu {0}. Mensagem do banco de dados: {1}
      raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'TMRP_637' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
   end;

   -- Deleta a tmrp_630 desta ordem de planejamento
   begin
      delete from tmrp_630
      where tmrp_630.ordem_planejamento = p_ordem_planejamento;
   exception when others then
      -- ATENCAO! Nao eliminou {0}. Mensagem do banco de dados: {1}
      raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26373', 'TMRP_630' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
   end;

   -- Verifica se tem alguma ordem para a Area 2
   if f_tem_area (p_ordem_planejamento, 2) > 0
   then
      -- Deleta a pcpb_033 da ordem de planejamento
      begin
         delete from pcpb_033
         where pcpb_033.ordem_planejamento = p_ordem_planejamento;
      exception when others then
         -- ATENCAO! Nao eliminou {0}. Mensagem do banco de dados: {1}
         raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26373', 'PCPB_033' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;

      -- Cria um backup da pcpb_031 desta ordem de planejamento
      -- pcpb_033 e uma copia da pcpb_031, todos os campos que tiver na pcpb_031 deve ter na pcpb_033
      begin
         insert into pcpb_033 (
                ordem_planejamento,                pedido_venda,
                ordem_producao,                    pano_nivel99,
                pano_grupo,                        pano_subgrupo,
                pano_item,                         sequenci_periodo,
                nr_pedido_ordem,                   nivel_prod,
                grupo_prod,                        subgrupo_prod,
                item_prod,                         qtde_quilos_prog,
                qtde_unidades_prog,                tipo_programacao)
         select pcpb_031.ordem_planejamento,       pcpb_031.pedido_venda,
                pcpb_031.ordem_producao,           pcpb_031.pano_nivel99,
                pcpb_031.pano_grupo,               pcpb_031.pano_subgrupo,
                pcpb_031.pano_item,                pcpb_031.sequenci_periodo,
                pcpb_031.nr_pedido_ordem,          pcpb_031.nivel_prod,
                pcpb_031.grupo_prod,               pcpb_031.subgrupo_prod,
                pcpb_031.item_prod,                pcpb_031.qtde_quilos_prog,
                pcpb_031.qtde_unidades_prog,       pcpb_031.tipo_programacao
         from pcpb_031
         where pcpb_031.ordem_planejamento = p_ordem_planejamento;
      exception when others then
         -- ATENCAO! Nao inseriu {0}. Mensagem do banco de dados: {1}
         raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'PCPB_033' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;

      -- Deleta a pcpb_033 da ordem de planejamento
      begin
         delete from pcpb_031
         where pcpb_031.ordem_planejamento = p_ordem_planejamento;
      exception when others then
         -- ATENCAO! Nao eliminou {0}. Mensagem do banco de dados: {1}
         raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26373', 'PCPB_031' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;
   end if;

   begin
      -- Zerar as qtde do planejamento para depois recalcular abaixo
      update tmrp_625
      set   tmrp_625.qtde_reserva_programada  = 0,
            tmrp_625.qtde_areceber_programada = 0
      where tmrp_625.ordem_planejamento       = p_ordem_planejamento;
   exception when others then
      -- ATENCAO! Nao atualizou {0}. Mensagem do banco de dados: {1}
      raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26375', 'PCPB_033' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
   end;

   -- Deletar tabela temporaria que guarda o produto origem
   begin
      delete from tmrp_615
      where tmrp_615.nome_programa      = 'trigger_planejamento'
        and tmrp_615.nr_solicitacao     = 888
        and tmrp_615.tipo_registro      = 888
        and tmrp_615.ordem_planejamento = p_ordem_planejamento;
   exception when others then
      -- ATENCAO! Nao eliminou {0}. Mensagem do banco de dados: {1}
      raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26373', 'TMRP_615(12)' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
   end;

   /*
   #################################################################################
   #################################################################################
                         rotina de calculo para o NIVEL 1
   #################################################################################
   #################################################################################
   */
   -- Verifica se tem alguma ordem para a Area 1
   if f_tem_area (p_ordem_planejamento, 1) > 0
   then
      -- Inserir na tabela temporaria os produto origem do nivel 1
      begin
         insert into tmrp_615 (
                nome_programa,                            area_producao,
                nr_solicitacao,                           tipo_registro,
                cgc_cliente9,                             alternativa,
                nivel,                                    grupo,
                subgrupo,                                 item,
                ordem_planejamento,                       pedido_venda)
         select 'trigger_planejamento',                   1,
                888,                                      888,
                ws_sid,                                   0,
                '0',                                      '00000',
                '000',                                    '000000',
                p_ordem_planejamento,                     tmrp_637.pedido_venda
         from tmrp_637
         where tmrp_637.ordem_planejamento = p_ordem_planejamento;
      exception when others then
         -- ATENCAO! Nao inseriu {0}. Mensagem do banco de dados: {1}
         raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'TMRP_615(10)' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;

      -- Volta o backup da tmrp_630 para a area 1, para recalcular a qtde areceber e a qtde programada
      begin
         insert into tmrp_630 (
                ordem_planejamento,                      area_producao,
                ordem_prod_compra,
                nivel_produto,                           grupo_produto,
                subgrupo_produto,                        item_produto,
                qtde_produzida_comprada,                 qtde_conserto,
                qtde_2qualidade,                         qtde_perdas,
                pedido_venda,                            programado_comprado,
                alternativa_produto,                     qtde_atendida_estoque)
         select tmrp_637.ordem_planejamento,             tmrp_637.area_producao,
                tmrp_637.ordem_prod_compra,
                tmrp_637.nivel_produto,                  tmrp_637.grupo_produto,
                tmrp_637.subgrupo_produto,               tmrp_637.item_produto,
                tmrp_637.qtde_produzida_comprada,        tmrp_637.qtde_conserto,
                tmrp_637.qtde_2qualidade,                tmrp_637.qtde_perdas,
                tmrp_637.pedido_venda,                   tmrp_637.programado_comprado,
                tmrp_637.alternativa_produto,            tmrp_637.qtde_atendida_estoque
         from tmrp_637
         where tmrp_637.ordem_planejamento = p_ordem_planejamento
           and tmrp_637.area_producao      = 1;
      exception when others then
         -- ATENCAO! Nao inseriu {0}. Mensagem do banco de dados: {1}
         raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'TMRP_630(01)' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;

      /*
         Marca os produtos da tmrp_630 cujo produto nao esta mais na estrutura do produto da tmrp_625
      */
      f_erro_recalculo (p_ordem_planejamento, 1);
   end if;

   /*
   #################################################################################
   #################################################################################
                         rotina de calculo para o NIVEL 2
   #################################################################################
   #################################################################################
   */

   -- Verifica se tem alguma ordem para a Area 2
   if f_tem_area (p_ordem_planejamento, 2) > 0
   then

      -- ###################################  PCPB_031  ##############################################
      begin
         select nvl(count(*),0)
         into v_count_reg
         from pcpb_033
         where pcpb_033.ordem_planejamento = p_ordem_planejamento;
      exception when others then
         v_count_reg := 0;
      end;

      if v_count_reg > 0
      then
         -- Volta o backup da pcpb_031, assim vai criar a tmrp_630 para estas ordens de producao
         -- Os dados do produto origem (tmrp_615) sera criada na trigger da pcpb_031
         begin
            insert into pcpb_031 (
                   ordem_planejamento,                pedido_venda,
                   ordem_producao,                    pano_nivel99,
                   pano_grupo,                        pano_subgrupo,
                   pano_item,                         sequenci_periodo,
                   nr_pedido_ordem,                   nivel_prod,
                   grupo_prod,                        subgrupo_prod,
                   item_prod,                         qtde_quilos_prog,
                   qtde_unidades_prog,                tipo_programacao)
            select pcpb_033.ordem_planejamento,       pcpb_033.pedido_venda,
                   pcpb_033.ordem_producao,           pcpb_033.pano_nivel99,
                   pcpb_033.pano_grupo,               pcpb_033.pano_subgrupo,
                   pcpb_033.pano_item,                pcpb_033.sequenci_periodo,
                   pcpb_033.nr_pedido_ordem,          pcpb_033.nivel_prod,
                   pcpb_033.grupo_prod,               pcpb_033.subgrupo_prod,
                   pcpb_033.item_prod,                pcpb_033.qtde_quilos_prog,
                   pcpb_033.qtde_unidades_prog,       pcpb_033.tipo_programacao
            from pcpb_033
            where pcpb_033.ordem_planejamento = p_ordem_planejamento;
         exception when others then
            -- ATENCAO! Nao inseriu {0}. Mensagem do banco de dados: {1}
            raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'PCPB_031(01)' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
         end;

         /*
            Marca os produtos da tmrp_630 cujo produto nao esta mais na estrutura do produto da tmrp_625
         */
         f_erro_recalculo_031(p_ordem_planejamento);

      end if;  -- Verificacao se tem pcpb_031

      -- ###################################  PCPB_030  ##############################################
      /*
         Verificar as Ordens de Beneficiamento que nao tem pcpb_031
      */
      begin
         select nvl(count(*),0)
         into v_count_reg
         from tmrp_637
         where tmrp_637.ordem_planejamento = p_ordem_planejamento
           and tmrp_637.area_producao      = 2
           and not exists (select 1
                           from pcpb_031
                           where pcpb_031.ordem_planejamento = tmrp_637.ordem_planejamento
                             and pcpb_031.ordem_producao     = tmrp_637.ordem_prod_compra);
      exception when others then
         v_count_reg := 0;
      end;

      if v_count_reg > 0
      then
         for reg_tmrp_637   in (select tmrp_637.ordem_prod_compra
                                from tmrp_637
                                where tmrp_637.ordem_planejamento = p_ordem_planejamento
                                  and tmrp_637.area_producao      = 2
                                  and not exists (select 1
                                                  from pcpb_031
                                                  where pcpb_031.ordem_planejamento = tmrp_637.ordem_planejamento
                                                    and pcpb_031.ordem_producao     = tmrp_637.ordem_prod_compra)
                                group by tmrp_637.ordem_prod_compra
                                order by tmrp_637.ordem_prod_compra)
         loop
            /*
               Ler os dados da Ordem de Beneficiamento, para depois inserir a tmpr_630 deste produto
            */
            for reg_pcpb_030   in (select pcpb_030.pano_nivel99,
                                          pcpb_030.pano_grupo,
                                          pcpb_030.pano_subgrupo,
                                          pcpb_030.pano_item,
                                          pcpb_020.alternativa_item,
                                          pcpb_030.pedido_corte,
                                          pcpb_030.nr_pedido_ordem,
                                          pcpb_030.qtde_quilos_prog
                                   from pcpb_020, pcpb_030
                                   where pcpb_020.ordem_producao     = reg_tmrp_637.ordem_prod_compra
                                     and pcpb_030.ordem_producao     = pcpb_020.ordem_producao
                                     and pcpb_030.pano_nivel99       = pcpb_020.pano_sbg_nivel99
                                     and pcpb_030.pano_grupo         = pcpb_020.pano_sbg_grupo
                                     and pcpb_030.pano_subgrupo      = pcpb_020.pano_sbg_subgrupo
                                     and pcpb_030.pano_item          = pcpb_020.pano_sbg_item)
            loop
               inter_pr_pcpb_030_plan(reg_tmrp_637.ordem_prod_compra,
                                      reg_pcpb_030.pano_nivel99,
                                      reg_pcpb_030.pano_grupo,
                                      reg_pcpb_030.pano_subgrupo,
                                      reg_pcpb_030.pano_item,
                                      reg_pcpb_030.alternativa_item,
                                      reg_pcpb_030.nr_pedido_ordem,
                                      reg_pcpb_030.qtde_quilos_prog);
            end loop; -- pcpb_030
         end loop; -- tmrp_637

         /*
            Marca os produtos da tmrp_630 cujo produto nao esta mais na estrutura do produto da tmrp_625
         */
         f_erro_recalculo_030 (p_ordem_planejamento);
      end if;
   end if; -- tem_area2 > 0

   /*
   #################################################################################
   #################################################################################
                         rotina de calculo para o NIVEL 7 - BENEFICIAMENTO
   #################################################################################
   #################################################################################
   */
   -- Verifica se tem alguma ordem para a Area 7
   if f_tem_area (p_ordem_planejamento, 7) > 0
   then
      -- Inserir na tabela temporaria os produto origem do nivel 7 - BENEFICIAMENTO
      for reg_tmrp_637   in (select tmrp_637.ordem_prod_compra
                             from tmrp_637
                             where tmrp_637.ordem_planejamento = p_ordem_planejamento
                               and tmrp_637.area_producao      = 7
                             group by tmrp_637.ordem_prod_compra
                             order by tmrp_637.ordem_prod_compra)
      loop
         for reg_pcpb_030   in (select pcpb_030.pano_nivel99,
                                       pcpb_030.pano_grupo,
                                       pcpb_030.pano_subgrupo,
                                       pcpb_030.pano_item,
                                       pcpb_020.alternativa_item,
                                       pcpb_030.pedido_corte,
                                       pcpb_030.nr_pedido_ordem,
                                       pcpb_030.qtde_quilos_prog
                                from pcpb_020, pcpb_030
                                where pcpb_020.ordem_producao     = reg_tmrp_637.ordem_prod_compra
                                  and pcpb_030.ordem_producao     = pcpb_020.ordem_producao
                                  and pcpb_030.pano_nivel99       = pcpb_020.pano_sbg_nivel99
                                  and pcpb_030.pano_grupo         = pcpb_020.pano_sbg_grupo
                                  and pcpb_030.pano_subgrupo      = pcpb_020.pano_sbg_subgrupo
                                  and pcpb_030.pano_item          = pcpb_020.pano_sbg_item)
         loop
            inter_pr_pcpb_030_plan_fio(reg_tmrp_637.ordem_prod_compra,
                                       reg_pcpb_030.pano_nivel99,
                                       reg_pcpb_030.pano_grupo,
                                       reg_pcpb_030.pano_subgrupo,
                                       reg_pcpb_030.pano_item,
                                       reg_pcpb_030.alternativa_item,
                                       reg_pcpb_030.nr_pedido_ordem,
                                       reg_pcpb_030.qtde_quilos_prog);
         end loop;   -- reg_pcpb_030
      end loop;   -- reg_tmrp_637

      /*
         Marca os produtos da tmrp_630 cujo produto nao esta mais na estrutura do produto da tmrp_625
      */
      f_erro_recalculo (p_ordem_planejamento, 7);
   end if;

   /*
   #################################################################################
   #################################################################################
                         rotina de calculo para o NIVEL 4 - TECELAGEM
   #################################################################################
   #################################################################################
   */
   -- Verifica se tem alguma ordem para a Area 1
   if f_tem_area (p_ordem_planejamento, 4) > 0
   then
      -- Inserir na tabela temporaria os produto origem do nivel 4 - tecelagem
      for reg_tmrp_637   in (select tmrp_637.ordem_prod_compra
                             from tmrp_637
                             where tmrp_637.ordem_planejamento = p_ordem_planejamento
                               and tmrp_637.area_producao      = 4
                             group by tmrp_637.ordem_prod_compra
                             order by tmrp_637.ordem_prod_compra)
      loop
         for reg_pcpt_010   in (select pcpt_016.ordem_pedido,
                                       pcpt_010.cd_pano_nivel99,
                                       pcpt_010.cd_pano_grupo,
                                       pcpt_010.cd_pano_subgrupo,
                                       pcpt_010.cd_pano_item,
                                       pcpt_010.alternativa_item,
                                       pcpt_016.quantidade
                                from pcpt_010, pcpt_016
                                where pcpt_010.ordem_tecelagem = reg_tmrp_637.ordem_prod_compra
                                  and pcpt_016.ordem_tecelagem = pcpt_010.ordem_tecelagem)
         loop
            inter_pr_pcpt_016_plan (reg_tmrp_637.ordem_prod_compra,
                                    reg_pcpt_010.cd_pano_nivel99,
                                    reg_pcpt_010.cd_pano_grupo,
                                    reg_pcpt_010.cd_pano_subgrupo,
                                    reg_pcpt_010.cd_pano_item,
                                    reg_pcpt_010.alternativa_item,
                                    reg_pcpt_010.ordem_pedido,
                                    reg_pcpt_010.quantidade);
         end loop; -- reg_pcpt_010
      end loop; -- reg_tmrp_637

      /*
         Marca os produtos da tmrp_630 cujo produto nao esta mais na estrutura do produto da tmrp_625
      */
      f_erro_recalculo_016(p_ordem_planejamento);
   end if;

   /*
   #################################################################################
   #################################################################################
                         rotina de calculo para o NIVEL 9 - COMPRAS
   #################################################################################
   #################################################################################
   */
   -- Verifica se tem alguma ordem para a Area 1
   if f_tem_area (p_ordem_planejamento, 9) > 0
   then
      begin
         insert into tmrp_615
           (nome_programa,
            nr_solicitacao,                           tipo_registro,
            cgc_cliente9)
         values
           ('trigger_planejamento',
            888,                                      888,
            ws_sid);
      exception when others then
         -- ATENCAO! Nao inseriu {0}. Mensagem do banco de dados: {1}
         raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'TMRP_615(76)' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;

      -- Volta o backup da tmrp_630 para a area 9 - COMPRAS, para recalcular a qtde areceber e a qtde programada
      -- dos produtos da area 9
      begin
         insert into tmrp_630 (
                ordem_planejamento,                      area_producao,
                ordem_prod_compra,
                nivel_produto,                           grupo_produto,
                subgrupo_produto,                        item_produto,
                qtde_produzida_comprada,                 qtde_conserto,
                qtde_2qualidade,                         qtde_perdas,
                pedido_venda,                            programado_comprado,
                alternativa_produto,                     qtde_atendida_estoque)
         select tmrp_637.ordem_planejamento,             tmrp_637.area_producao,
                tmrp_637.ordem_prod_compra,
                tmrp_637.nivel_produto,                  tmrp_637.grupo_produto,
                tmrp_637.subgrupo_produto,               tmrp_637.item_produto,
                tmrp_637.qtde_produzida_comprada,        tmrp_637.qtde_conserto,
                tmrp_637.qtde_2qualidade,                tmrp_637.qtde_perdas,
                tmrp_637.pedido_venda,                   tmrp_637.programado_comprado,
                tmrp_637.alternativa_produto,            tmrp_637.qtde_atendida_estoque
         from tmrp_637
         where tmrp_637.ordem_planejamento = p_ordem_planejamento
           and tmrp_637.area_producao      = 9;
      exception when others then
         -- ATENCAO! Nao inseriu {0}. Mensagem do banco de dados: {1}
         raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'TMRP_630(99)' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;

      /*
         Marca os produtos da tmrp_630 cujo produto nao esta mais na estrutura do produto da tmrp_625
      */
      f_erro_recalculo (p_ordem_planejamento, 9);
   end if;

   -- Deletar tabela temporaria que guarda o produto origem
   begin
      delete from tmrp_615
      where tmrp_615.nome_programa      = 'trigger_planejamento'
        and tmrp_615.nr_solicitacao     = 888
        and tmrp_615.tipo_registro      = 888
        and tmrp_615.ordem_planejamento = p_ordem_planejamento;
   exception when others then
      -- ATENCAO! Nao eliminou {0}. Mensagem do banco de dados: {1}
      raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26373', 'TMRP_615(345)' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
   end;
end inter_pr_recal_planej;

 

/

exec inter_pr_recompile;

