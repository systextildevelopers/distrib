alter table obrf_015 add (perc_substituicao      NUMBER(5,2));

alter table obrf_015 add (perc_redu_sub          NUMBER(5,2));

alter table obrf_015 add (perc_redu_icm          NUMBER(5,2));

alter table i_obrf_015 add (perc_substituicao      NUMBER(5,2));

alter table i_obrf_015 add (perc_redu_sub          NUMBER(5,2));

alter table i_obrf_015 add (perc_redu_icm          NUMBER(5,2));

comment on column i_obrf_015.perc_substituicao
  is 'Percentual de substitui��o do item da nota fiscal';

comment on column i_obrf_015.perc_redu_sub
  is 'Percentual de redu��o de substitui��o.';

comment on column i_obrf_015.perc_redu_icm
  is 'Percentual de redu��o de ICMD';

comment on column obrf_015.perc_substituicao
  is 'Percentual de substitui��o do item da nota fiscal';

comment on column obrf_015.perc_redu_sub
  is 'Percentual de redu��o de substitui��o.';

comment on column obrf_015.perc_redu_icm
  is 'Percentual de redu��o de ICMD';

/
exec inter_pr_recompile;
/
