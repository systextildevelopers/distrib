alter table pedi_110 add (
val_liq_unt    number(17,5),
val_liq_unt_um number(17,5)
);


alter table pedi_110 modify (
val_liq_unt    default 0,
val_liq_unt_um default 0
);
