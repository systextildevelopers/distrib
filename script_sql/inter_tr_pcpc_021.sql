create or replace trigger inter_tr_pcpc_021
   before delete
   on pcpc_021
   for each row
declare
   v_executa_trigger  number;

   v_referencia_op     pcpc_020.referencia_peca%type;
begin
   -- L�gica de controle para execu��o ou n�o da trigger, referente ao
   -- processo de limpeza de dados.
   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then
      if deleting
      then
         -- BUSCA DADOS DA ORDEM DE CORTE
         begin
            select pcpc_020.referencia_peca
            into   v_referencia_op
            from pcpc_020
            where pcpc_020.ordem_producao = :old.ordem_producao;
         exception when others then
            v_referencia_op := '';
         end;

         for reg_tmrp in (select tmrp_630.ordem_planejamento,            tmrp_630.pedido_venda,
                                 tmrp_630.nivel_produto,                 tmrp_630.grupo_produto,
                                 tmrp_630.subgrupo_produto,              tmrp_630.item_produto,
                                 tmrp_625.seq_produto_origem,            tmrp_625.nivel_produto_origem,
                                 tmrp_625.grupo_produto_origem,          tmrp_625.subgrupo_produto_origem,
                                 tmrp_625.item_produto_origem,           tmrp_625.alternativa_produto_origem,
                                 tmrp_625.seq_produto,                   tmrp_625.qtde_areceber_programada,
                                 tmrp_625.qtde_reserva_programada,       tmrp_630.programado_comprado
                          from tmrp_630, tmrp_625
                          where tmrp_630.ordem_planejamento      = tmrp_625.ordem_planejamento
                            and tmrp_630.pedido_venda            = tmrp_625.pedido_venda
                            and tmrp_630.nivel_produto           = tmrp_625.nivel_produto
                            and tmrp_630.grupo_produto           = tmrp_625.grupo_produto
                            and tmrp_630.subgrupo_produto        = tmrp_625.subgrupo_produto
                            and tmrp_630.item_produto            = tmrp_625.item_produto
                            and tmrp_625.nivel_produto_origem    = '0'
                            and tmrp_625.grupo_produto_origem    = '00000'
                            and tmrp_625.subgrupo_produto_origem = '000'
                            and tmrp_625.item_produto_origem     = '000000'
                            and tmrp_630.nivel_produto           = '1'
                            and tmrp_630.grupo_produto           = v_referencia_op
                            and tmrp_630.subgrupo_produto        = :old.tamanho
                            and tmrp_630.item_produto            = :old.sortimento
                            and tmrp_630.area_producao           = 1 --Confec�ao
                            and tmrp_630.ordem_prod_compra       = :old.ordem_producao)
         loop

            inter_pr_atu_planej_reves( reg_tmrp.ordem_planejamento,
                                       reg_tmrp.pedido_venda,            reg_tmrp.nivel_produto,
                                       reg_tmrp.grupo_produto,           reg_tmrp.subgrupo_produto,
                                       reg_tmrp.item_produto,            reg_tmrp.programado_comprado,
                                       0.00,
                                       reg_tmrp.nivel_produto_origem,    reg_tmrp.grupo_produto_origem,
                                       reg_tmrp.subgrupo_produto_origem, reg_tmrp.item_produto_origem,
                                       reg_tmrp.seq_produto_origem,      reg_tmrp.alternativa_produto_origem,
                                       reg_tmrp.seq_produto,             'pcpc_020',
                                       :old.ordem_producao);

            -- Ap�s atualiza��o de todos os componentes do produto ir� deletar o link entre
            -- o Systextil e as ordens de planejamento (tmrp_630)
            begin
               delete from tmrp_630
               where tmrp_630.ordem_planejamento = reg_tmrp.ordem_planejamento
                 and tmrp_630.area_producao      = 1
                 and tmrp_630.ordem_prod_compra  = :old.ordem_producao
                 and tmrp_630.pedido_venda       = reg_tmrp.pedido_venda
                 and tmrp_630.nivel_produto      = reg_tmrp.nivel_produto
                 and tmrp_630.grupo_produto      = reg_tmrp.grupo_produto
                 and tmrp_630.subgrupo_produto   = reg_tmrp.subgrupo_produto
                 and tmrp_630.item_produto       = reg_tmrp.item_produto;
            exception
            when OTHERS then
               raise_application_error(-20000,'N�o excluiu tmpr_630');
            end;
         end loop;
      end if;
   end if;
end;

/

exec inter_pr_recompile;

/* versao: 3 */
