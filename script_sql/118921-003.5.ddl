create table PROG_059
(
    NIVEL_ROTEIRO    VARCHAR2(1)   default '0'      not null,
    GRUPO_ROTEIRO    VARCHAR2(5)   default '00000'  not null,
    SUBGRU_ROTEIRO   VARCHAR2(3)   default '000'    not null,
    ITEM_ROTEIRO     VARCHAR2(6)   default '000000' not null,
    ALTERNATIVA      NUMBER(2)     default 0        not null,
    ROTEIRO          NUMBER(2)     default 0        not null,
    OPERACAO_ROTEIRO NUMBER(5)     default 0        not null,
    SEQ_OPERACAO     NUMBER(4)     default 0        not null,
    NIVEL_LINHA      VARCHAR2(1)   default '0'      not null,
    GRUPO_LINHA      VARCHAR2(5)   default '00000'  not null,
    SUBGRU_LINHA     VARCHAR2(3)   default '000'    not null,
    ITEM_LINHA       VARCHAR2(6)   default '000000' not null,
    CONSUMO_LINHA    NUMBER(6, 2)  default 0.00,
    PESO_METRO       NUMBER(10, 6) default 0.000000,
    COL_TABELA_PRECO NUMBER(2)     default 0,
    MES_TABELA_PRECO NUMBER(2)     default 0,
    SEQ_TABELA_PRECO NUMBER(2)     default 0,
    CUSTO            NUMBER(10, 6) default 0.000000,
    ULTIMO_CUSTO     NUMBER(10, 6) default 0.000000
);
/
exec inter_pr_recompile;
