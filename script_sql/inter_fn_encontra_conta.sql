create or replace FUNCTION "INTER_FN_ENCONTRA_CONTA" (p_cod_empresa          in number,
                                                   p_tipo_contab          in number,
                                                   p_codigo_contab        in number,
                                                   p_transacao            in number,
                                                   p_c_custo              in number,
                                                   p_codigo_exercicio     in number,
                                                   p_codigo_exercicio_doc in number)
                                                   return number   is v_conta_contab_ret  number;

v_empresa_matriz             fatu_500.codigo_matriz%type;
v_plano_conta_doc            cont_500.cod_plano_cta%type;
v_plano_conta                cont_500.cod_plano_cta%type;
v_conta_contab               cont_550.conta_contabil%type;
v_cta_destino                cont_538.conta_destino%type;
v_patr_result                cont_535.patr_result%type;
v_exige_subconta             cont_535.exige_subconta%type;
v_anal_sint                  cont_535.tipo_conta%type;
v_c_custo_lanc               basi_185.centro_custo%type;
v_cod_reduzido               cont_535.cod_reduzido%type;
v_conta_mae                  cont_535.conta_mae%type;
v_registro_ok                varchar2(1);
v_conta_tem_relac            varchar2(1);
v_cont                       number(10);

begin
   v_conta_contab_ret := 0;
   

	begin
	 select fatu_500.codigo_matriz
	 into v_empresa_matriz
	 from fatu_500
	 where fatu_500.codigo_empresa = p_cod_empresa;
	exception
	  when no_data_found then
		 v_empresa_matriz := 0;
	end;

	if p_codigo_exercicio > 0 and p_codigo_exercicio_doc > 0
	then
		 v_plano_conta_doc := 0;
		 begin
			select cont_500.cod_plano_cta
			into   v_plano_conta_doc
			from cont_500
			where cont_500.cod_empresa  = v_empresa_matriz
			  and cont_500.exercicio    = p_codigo_exercicio_doc;
		 exception
			 when no_data_found then
				v_plano_conta_doc := -99;
		 end;

		 if v_plano_conta_doc <> -99
		 then

			v_conta_contab := 0;
			begin
			   select cont_550.conta_contabil
			   into   v_conta_contab
			   from cont_550
			   where cont_550.cod_plano_cta   = v_plano_conta_doc
				 and cont_550.tipo_contabil   = p_tipo_contab
				 and cont_550.codigo_contabil = p_codigo_contab
				 and cont_550.transacao       = p_transacao;
			exception
				when no_data_found then
				   v_conta_contab := -99;
			end;

			if v_conta_contab = -99
			then
			   v_cont := 0;
			   begin
				  select 1
				  into   v_cont
				  from cont_550
				  where cont_550.cod_plano_cta   = v_plano_conta_doc
					and cont_550.tipo_contabil   = p_tipo_contab
					and cont_550.codigo_contabil = 999999
					and cont_550.transacao       = p_transacao
					and cont_550.conta_contabil  = 99999;
			   exception
				   when no_data_found then
					  v_cont := 0;
			   end;

               if v_cont = 0 then 
                v_cont := -99;
                begin
                    select 1
                    into   v_cont
                    from cont_550
                    where cont_550.cod_plano_cta   = v_plano_conta_doc
                        and cont_550.tipo_contabil   = p_tipo_contab
                        and cont_550.codigo_contabil = p_codigo_contab
                        and cont_550.transacao       = 0;
                exception
                    when no_data_found then
                        v_cont := -99;
                end;
               end if;

			   if v_cont = 1
			   then
				  begin
					 select cont_535.cod_reduzido
					 into   v_conta_contab
					 from cont_535
					 where cont_535.cod_plano_cta = v_plano_conta_doc
					   and cont_535.cod_reduzido  = p_codigo_contab;
				  exception
					  when no_data_found then
						 v_conta_contab := -99;
				  end;
			   end if; --if v_cont = 1
			end if; --if v_conta_contab = -99
		 else
			v_conta_contab := -99;
		 end if; --if v_plano_conta_doc <> -99
	end if; --if v_cod_exercicio > 0 and p_codigo_exercicio_doc > 0

	if v_conta_contab > 0
	then
	 if p_codigo_exercicio <> p_codigo_exercicio_doc
	 then
		begin
		   select cont_538.conta_destino
		   into   v_cta_destino
		   from cont_538
		   where cont_538.cod_empresa         = v_empresa_matriz
			 and   cont_538.exercicio_origem  = p_codigo_exercicio_doc
			 and   cont_538.exercicio_destino = p_codigo_exercicio
			 and   cont_538.conta_origem      = v_conta_contab;
		exception
			when no_data_found then
			   v_cta_destino := -99;
		end;
		if v_cta_destino <> -99
		then
		   v_conta_contab := v_cta_destino;
		end if; --if v_cta_destino <> -99
	 end if; --if p_codigo_exercicio <> p_codigo_exercicio_doc
	end if; --if v_conta_contab > 0

	if v_conta_contab > 0
	then
	 begin
		select cont_500.cod_plano_cta
		into   v_plano_conta
		from cont_500
		where cont_500.cod_empresa  = v_empresa_matriz
		  and cont_500.exercicio    = p_codigo_exercicio;
	 exception
		 when no_data_found then
			v_conta_contab := -99;
	 end;

	 v_cont := 1;

	 begin
		select cont_535.patr_result, cont_535.exige_subconta,
			   cont_535.tipo_conta
		into   v_patr_result,        v_exige_subconta,
			   v_anal_sint
		from cont_535
		where cont_535.cod_plano_cta = v_plano_conta
		  and cont_535.cod_reduzido  = v_conta_contab;
	 exception
		 when no_data_found then
			v_cont := 0;
	 end;

	 if v_cont <> 1
	 then
		v_conta_contab := -99;
	 else
		if v_anal_sint = 2
		then
		   v_conta_contab := -99;
		else

		   if v_patr_result = 1 or (v_patr_result = 2 and v_exige_subconta = 2)
		   then
			  v_c_custo_lanc := 0;
		   else
			  v_c_custo_lanc := p_c_custo;
		   end if; --if v_patr_result = 1 or (v_patr_result = 2 and v_exige_subconta = 2)

		   if v_patr_result = 2
		   then
			  v_cod_reduzido := v_conta_contab;
			  v_conta_tem_relac := 'n';
			  v_registro_ok := 'n';

			  LOOP
				 if v_cod_reduzido > 0
				 then

					v_cont := 1;
					begin
					   select cont_535.cod_reduzido, cont_535.conta_mae
					   into   v_cod_reduzido,        v_conta_mae
						 from cont_535
						 where cont_535.cod_plano_cta = v_plano_conta
						   and cont_535.cod_reduzido  = v_cod_reduzido;
					exception
						when no_data_found then
						   v_cont := 0;
					end;

					if v_cont = 0
					then
					   v_cod_reduzido := 0;
					end if;

					v_cont := 0;

					if v_cod_reduzido > 0
					then
					   for f_conta_red in (
						  select cont_555.centro_custo from cont_555
						  where cont_555.cod_plano_conta   = v_plano_conta
							 and cont_555.cod_reduzido     = v_cod_reduzido)
					   LOOP
						  v_cont := 1;

						  if f_conta_red.centro_custo = v_c_custo_lanc
						  then
							 v_registro_ok := 's';
						  else
							 v_conta_tem_relac := 's';
						  end if;
					   END LOOP;
					end if; --if v_cod_reduzido > 0

					if v_cont <> 1 or v_conta_tem_relac = 's'
					then
					   v_cod_reduzido := v_conta_mae;
					end if;
				 end if; --if v_cod_reduzido > 0
				 EXIT WHEN v_cod_reduzido = 0 or v_registro_ok = 's';
			  END LOOP;
		   end if; --if v_patr_result = 2
		end if; --if v_anal_sint = 2
	 end if; --if v_cont <> 1
	end if; --if v_conta_contab > 0

   v_conta_contab_ret := v_conta_contab;

   return(v_conta_contab_ret);
end inter_fn_encontra_conta;
/
