CREATE TABLE basi_710 (
        fam_pente       VARCHAR2(10) NOT NULL ENABLE,
        descricao_fam   VARCHAR2(60) NOT NULL ENABLE,
        qtde_pente      NUMBER(5) NOT NULL ENABLE,
        pente_ocupado   NUMBER(5) NOT NULL ENABLE,
        saldo_pente     NUMBER(5) NOT NULL ENABLE,
    CONSTRAINT "PK_BASI_710" PRIMARY KEY (fam_pente) ENABLE
);

COMMENT ON COLUMN basi_710.pente_ocupado IS 'Pentes alocados a maquina';

COMMENT ON COLUMN basi_710.saldo_pente IS 'Pentes livres';
