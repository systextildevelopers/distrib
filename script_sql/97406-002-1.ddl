alter table basi_030
add(ind_envia_infotint varchar2(1));

alter table basi_030
modify(ind_envia_infotint default 'S');

prompt 'Atualizando valores default para InfoTint...'
begin
   for reg in(select rowid from basi_030 where ind_envia_infotint is null)
   loop
       update basi_030
       set ind_envia_infotint = 'S'
       where basi_030.rowid = reg.rowid;
       commit;
   end loop;
end;
/
exec inter_pr_recompile;
