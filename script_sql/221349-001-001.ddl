create table inte_263 (
    END_WS_INTEGRACAO	varchar2(100)   not null,
    USER_INTEGRACAO		varchar2(60) not null,
    PASS_INTEGRACAO     varchar2(60)	not null,
	
    CONSTRAINT inte_263_origem_pk PRIMARY KEY (END_WS_INTEGRACAO) USING INDEX  ENABLE
);

/
exec inter_pr_recompile;
