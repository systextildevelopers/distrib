alter table oper_275 add 
 cod_natur_operacao_saida   number(9)  default 0;
 
alter table oper_275 add  
 cod_natur_operacao_entrada   number(9)   default 0;
 
alter table oper_275 add 
 cod_natur_operacao_devolucao   number(9)   default 0;
 
alter table oper_275 add 
 codigo_deposito              number(9) default 0;
 
alter table oper_275 add
 codigo_transacao             number(9) default 0;
 
exec inter_pr_recompile;
