create or replace procedure inter_pr_gera_ctb_sped_i012(p_cod_empresa   IN NUMBER,
                                                        p_exercicio     IN NUMBER,
                                                        p_cod_plano_cta IN NUMBER,
                                                        p_des_erro      OUT varchar2) is

   -- Finalidade: Gerar a tabela sped_cta_i012 - Termos de Abertura de livros
   --
   -- Hist�ricos
   --
   -- Data    Autor    Observa��es
   --

   cursor cont535(p_cod_plano_cta NUMBER, p_tipo_livro VARCHAR2) IS
      SELECT *
      from   cont_535
      where  cod_plano_cta     = p_cod_plano_cta
      and    tp_livro_aux_sped = p_tipo_livro
      order by conta_contabil;

   cursor cont535Ras(p_cod_plano_cta NUMBER) IS
      SELECT cont_535.*
      from   cont_535, cont_605
      where  cod_plano_cta     = p_cod_plano_cta
      and    cont_535.grupo_subconta_sped > 0
      and    cont_535.cod_reduzido = cont_605.conta_reduzida
      and    cont_605.cod_empresa  = p_cod_empresa
      and    cont_605.exercicio    = p_exercicio
      order by cont_535.conta_contabil;

   v_erro                         EXCEPTION;

   v_nr_livro_diario_geral_sped   fatu_503.nr_livro_diario_geral_sped%type;
   v_livro_diario_geral_sped_exe  cont_500.nr_livro_diario_geral_sped%type;
   v_nr_livro_razao_clientes_sped fatu_503.nr_livro_razao_clientes_sped%type;
   v_nr_livro_razao_fornec_sped   fatu_503.nr_livro_razao_fornec_sped%type;
   v_data_arq_atos_constitutivos  fatu_503.data_arq_atos_constitutivos%type;
   v_data_arq_conversao_sociedade fatu_503.data_arq_conversao_sociedade%type;
   v_ident_escr_contab_sped       cont_530.ident_escr_contab_sped%type;
   v_tipo_escrituracao            varchar2(100);
   v_cod_hash                     varchar2(250);
   v_conta_contabil               varchar2(100);

BEGIN

   p_des_erro := NULL;

   v_cod_hash  := '';

   -- le fatu_503 para pegar os n�meros dos livros
   begin
      select nr_livro_diario_geral_sped,      nr_livro_razao_clientes_sped,
             nr_livro_razao_fornec_sped,      data_arq_atos_constitutivos,
             data_arq_conversao_sociedade
      into   v_nr_livro_diario_geral_sped,    v_nr_livro_razao_clientes_sped,
             v_nr_livro_razao_fornec_sped,    v_data_arq_atos_constitutivos,
             v_data_arq_conversao_sociedade
      from fatu_503
      where codigo_empresa = p_cod_empresa;

   exception
      when NO_DATA_FOUND then
         v_nr_livro_diario_geral_sped   := 0;
         v_nr_livro_razao_clientes_sped := 0;
         v_nr_livro_razao_fornec_sped   := 0;
         v_data_arq_atos_constitutivos  := NULL;
         v_data_arq_conversao_sociedade := NULL;

      when others then
         v_nr_livro_diario_geral_sped   := 0;
         v_nr_livro_razao_clientes_sped := 0;
         v_nr_livro_razao_fornec_sped   := 0;
         v_data_arq_atos_constitutivos  := NULL;
         v_data_arq_conversao_sociedade := NULL;

         inter_pr_insere_erro_sped('C', p_cod_empresa, 'Erro na leitura do numero dos livros');
         commit;

         p_des_erro := 'Erro na leitura do numero dos livros';

         RAISE v_erro;
   end;

   -- le fatu_503 para pegar os numeros dos livros
   begin
      select nr_livro_diario_geral_sped
      into v_livro_diario_geral_sped_exe
      from cont_500
      where cod_empresa = p_cod_empresa
        and exercicio = p_exercicio;
      
      if v_livro_diario_geral_sped_exe > 0 
      then 
         v_nr_livro_diario_geral_sped := v_livro_diario_geral_sped_exe;
      end if;
      
   exception
      when others then
         inter_pr_insere_erro_sped('C', p_cod_empresa, 'Erro na leitura do exercicio');
         commit;
         p_des_erro := 'Erro na leitura do exercicio';
         RAISE v_erro;
   end;

   -- le a capa do plano de contas para identificar o tipo da escrituracao contabil para o SPED
   -- sendo que pode ser "G" para exportar apenas o Di�rio Geral
   -- ou "R" para exportar o Raz�o auxiliar dos cliente ou fornecedores (se houverem)
   begin
      select ident_escr_contab_sped
      into   v_ident_escr_contab_sped
      from cont_530
      where cod_plano_cta = p_cod_plano_cta;

   exception
      when others then
         v_ident_escr_contab_sped := NULL;

         inter_pr_insere_erro_sped('C', p_cod_empresa, 'Erro na leitura do plano de contas ' || p_cod_plano_cta);
         commit;

         p_des_erro := 'Erro na leitura do plano de contas ' || p_cod_plano_cta;

         RAISE v_erro;
   end;

   -- grava o termo de abertura do Diario geral

   -- identifica o nome do livro, de acordo com o tipo de escritura��o que a empresa tem
   if v_ident_escr_contab_sped = 'G'  -- Escrituracao SEM livros auxiliares
   then
      v_tipo_escrituracao := 'DI�RIO GERAL';
   else                               -- Escrituracao COM livros auxiliares
      v_tipo_escrituracao := 'DIARIO GERAL COM ESCRITURA��O RESUMIDA';
   end if;

   -- dados para serem preenchidos no quadro i012, do arquivo dos livros auxiliares
   -- dados para serem preenchidos no quadro i030, do arquivo principal (escrituracao "G" ou "R")
   begin
      insert into sped_ctb_i012
        (cod_empresa,                       exercicio,
         tipo_livro_aux,                    num_ordem,
         natureza_livro,                    tipo_escrituracao,
         cod_hash_aux,                      cod_cta_res,
         data_arq_atos_constitutivos,       data_arq_conversao_sociedade)
      values
         (p_cod_empresa,                     p_exercicio,
          'D',                               v_nr_livro_diario_geral_sped,
          v_tipo_escrituracao,               0,
          '',                                '.',
          v_data_arq_atos_constitutivos,     v_data_arq_conversao_sociedade);

   exception
      when others then
         p_des_erro := 'Erro na inclus�o da tabela sped_ctb_i012 (1) ' || Chr(10)  || ' - ' ||
         P_COD_EMPRESA || ' - ' || P_EXERCICIO || ' - ' ||
         v_nr_livro_diario_geral_sped || Chr(10)  ||
         SQLERRM;
         RAISE v_erro;
   end;
   commit;

   if v_ident_escr_contab_sped = 'R'
   then

      -- le o plano de contas para listar todas as conta que fazem parte do Razao de clientes
      for reg_cont535 in cont535(p_cod_plano_cta, 'C')
      loop

          if reg_cont535.patr_result = 2 or reg_cont535.exige_subconta = 2
          or reg_cont535.tipo_conta  = 2
          then
             v_conta_contabil := reg_cont535.conta_contabil;
          else
             v_conta_contabil := rtrim(reg_cont535.conta_contabil) ||
                                 ltrim(to_char(reg_cont535.subconta, '0000'));
          end if;


         -- dados para serem preenchidos no quadro i012, do arquivo do livro principal
         begin
            insert into sped_ctb_i012
              (cod_empresa,                       exercicio,
               tipo_livro_aux,                    num_ordem,
               natureza_livro,                    tipo_escrituracao,
               cod_hash_aux,                      cod_cta_res,
               data_arq_atos_constitutivos,       data_arq_conversao_sociedade)
            values
              (p_cod_empresa,                     p_exercicio,
               'C',                               v_nr_livro_razao_clientes_sped,
               'DIARIO AUXILIAR DE CLIENTES',     0,
               v_cod_hash,                        v_conta_contabil,
               v_data_arq_atos_constitutivos,     v_data_arq_conversao_sociedade);

         exception
            when others then
               p_des_erro := 'Erro na inclus�o da tabela sped_ctb_i012 (2) ' || Chr(10) ||
               P_COD_EMPRESA || ' - ' || P_EXERCICIO || ' - ' ||
                v_nr_livro_diario_geral_sped  || ' - ' || v_conta_contabil || Chr(10) ||
               SQLERRM;

               RAISE v_erro;
         end;
      end loop;
      commit;

      -- le o plano de contas para listar todas as conta que fazem parte do Razao de fornecedores
      for reg_cont535 in cont535(p_cod_plano_cta, 'F')
      loop

          if reg_cont535.patr_result = 2 or reg_cont535.exige_subconta = 2
          or reg_cont535.tipo_conta  = 2
          then
             v_conta_contabil := reg_cont535.conta_contabil;
          else
             v_conta_contabil := rtrim(reg_cont535.conta_contabil) ||
                                 ltrim(to_char(reg_cont535.subconta, '0000'));
          end if;


         -- dados para serem preenchidos no quadro i012, do arquivo do livro principal
         begin
            insert into sped_ctb_i012
              (cod_empresa,                       exercicio,
               tipo_livro_aux,                    num_ordem,
               natureza_livro,                    tipo_escrituracao,
               cod_hash_aux,                      cod_cta_res,
               data_arq_atos_constitutivos,       data_arq_conversao_sociedade)
            values
              (p_cod_empresa,                     p_exercicio,
               'F',                               v_nr_livro_razao_fornec_sped,
               'DIARIO AUXILIAR DE FORNECEDORES', 0,
               v_cod_hash,                        v_conta_contabil,
               v_data_arq_atos_constitutivos,     v_data_arq_conversao_sociedade);

         exception
            when others then
               p_des_erro := 'Erro na inclus�o da tabela sped_ctb_i012 (3) ' || Chr(10) ||
               P_COD_EMPRESA || ' - ' || P_EXERCICIO || ' - ' ||
                v_nr_livro_diario_geral_sped  || ' - ' || v_conta_contabil || Chr(10) ||
               SQLERRM;
               RAISE v_erro;
         end;
      end loop;
      commit;

      -- le o plano de contas para listar todas as conta que fazem parte do LIVRO RAS
      for reg_cont535 in cont535Ras(p_cod_plano_cta)
      loop

         -- dados para serem preenchidos no quadro i012, do arquivo do livro principal
         begin
            insert into sped_ctb_i012
              (cod_empresa,                       exercicio,
               tipo_livro_aux,                    num_ordem,
               natureza_livro,                    tipo_escrituracao,
               cod_hash_aux,                      cod_cta_res,
               data_arq_atos_constitutivos,       data_arq_conversao_sociedade)
            values
              (p_cod_empresa,                     p_exercicio,
               'Z',                               v_nr_livro_razao_fornec_sped,
               'DIARIO AUXILIAR DAS SUBCONTAS',   0,
               v_cod_hash,                        reg_cont535.conta_contabil,
               v_data_arq_atos_constitutivos,     v_data_arq_conversao_sociedade);

         exception
            when others then
               p_des_erro := 'Erro na inclus�o da tabela sped_ctb_i012 (3) ' || Chr(10) ||
               P_COD_EMPRESA || ' - ' || P_EXERCICIO || ' - ' ||
                v_nr_livro_diario_geral_sped  || ' - ' || reg_cont535.conta_contabil || Chr(10) ||
               SQLERRM;
               RAISE v_erro;
         end;
      end loop;
      commit;

   end if;

exception
   when V_ERRO then
      p_des_erro := 'Erro na procedure inter_pr_gera_ctb_sped_i012 ' || Chr(10) || p_des_erro;

   when others then
      p_des_erro := 'Outros erros na procedure inter_pr_gera_ctb_sped_i012 ' || Chr(10) || SQLERRM;

end inter_pr_gera_ctb_sped_i012;
/
exec inter_pr_recompile;
