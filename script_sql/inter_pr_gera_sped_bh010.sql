create or replace procedure inter_pr_gera_sped_bh010(p_cod_empresa    in number,
                                                     p_dat_final      IN DATE,
                                                     p_des_erro       out varchar2) is
  w_erro EXCEPTION;
  
  temTpSpedH010 number;
    
  CURSOR u_sped_k200_h010(p_cod_empresa NUMBER, p_dat_final DATE) IS
    select sped_k200_h010.empresa,
           sped_k200_h010.nivel,
           sped_k200_h010.grupo,
           sped_k200_h010.subgrupo,
           sped_k200_h010.item,
           sped_k200_h010.tipo_propriedade,
           sum(sped_k200_h010.quantidade) quantidade,
           sum(sped_k200_h010.valor_total) valor_total,
           sped_k200_h010.cnpj9_participante,
           sped_k200_h010.cnpj4_participante,
           sped_k200_h010.cnpj2_participante,
           sped_k200_h010.estagio_agrupador,
           sped_k200_h010.estagio_agrupador_simultaneo,
           sped_k200_h010.ncm,
           avg(sped_k200_h010.valor_unitario) valor_unitario,
           sped_k200_h010.codigo_deposito,
           sped_k200_h010.unidade_medida,
           sped_k200_h010.codigo_contabil,
           avg(sped_k200_h010.valor_ir) valor_ir,
           sped_k200_h010.sequencia_operacao_agrupador as seq_agrupador,
           sped_k200_h010.tipo_produto_sped
      from sped_k200_h010
     where sped_k200_h010.empresa = p_cod_empresa
     and   sped_k200_h010.ano = extract(year from p_dat_final) 
     and   sped_k200_h010.mes = extract(month from p_dat_final)
     group by sped_k200_h010.empresa,
           sped_k200_h010.nivel,
           sped_k200_h010.grupo,
           sped_k200_h010.subgrupo,
           sped_k200_h010.item,
           sped_k200_h010.tipo_propriedade,
           sped_k200_h010.cnpj9_participante,
           sped_k200_h010.cnpj4_participante,
           sped_k200_h010.cnpj2_participante,
           sped_k200_h010.estagio_agrupador,
           sped_k200_h010.estagio_agrupador_simultaneo,
           sped_k200_h010.ncm,
           sped_k200_h010.codigo_deposito,
           sped_k200_h010.unidade_medida,
           sped_k200_h010.codigo_contabil,
           sped_k200_h010.sequencia_operacao_agrupador,
           sped_k200_h010.tipo_produto_sped
      having sum(sped_k200_h010.quantidade) <> 0;

BEGIN

  FOR sped_k200_h010 IN u_sped_k200_h010(p_cod_empresa, p_dat_final) LOOP
    
    -- POR PADR?O S?O OS TIPO PRODUTO SPED 0,1,2,3,4,5,6
    
    SELECT count(*) into temTpSpedH010  from empr_008 
    where param          = 'obrf.tipoSpedFiscalH010'
    and   codigo_empresa = p_cod_empresa
    and sped_k200_h010.tipo_produto_sped IN (
          SELECT 
            TO_NUMBER(
              REGEXP_SUBSTR(
                val_str, '[^,]+', 1, LEVEL
              )
            ) 
          FROM DUAL CONNECT BY REGEXP_SUBSTR(
             val_str, '[^,]+', 1, LEVEL
            ) IS NOT NULL
        );
   
     if temTpSpedH010 > 0
     then        
        if sped_k200_h010.tipo_propriedade = 0
        then
           sped_k200_h010.tipo_propriedade := 1;
        else 
           if sped_k200_h010.tipo_propriedade = 1
           then
              sped_k200_h010.tipo_propriedade := 2;
           
           else 
              if sped_k200_h010.tipo_propriedade = 3
              then
                 sped_k200_h010.tipo_propriedade := 3;
              end if;
           end if;
        end if;
         

       BEGIN
         INSERT INTO sped_h010
           (cod_empresa,
            nivel,
            grupo,
            subgrupo,
            item,
            tip_propriedade_deposito,
            dat_inventario,
            qtde_inventario,
            vlr_total,
            cnpj9,
            cnpj4,
            cnpj2,
            inscr_estadual,
            sig_estado,
            classif_ncm,
            valor_unit,
            observacao,
            natureza_estoque,
            local_estoque,
            categoria,
            cod_deposito,
            cod_unid_medida,
            codigo_contabil,
            conta_contabil,
            val_item_ir,
            estagio_agrupador,
            estagio_agrupador_simultaneo,
            sequencia_operacao_agrupador)
         values
           (sped_k200_h010.empresa,
            sped_k200_h010.nivel,
            sped_k200_h010.grupo,
            sped_k200_h010.subgrupo,
            sped_k200_h010.item,
            sped_k200_h010.tipo_propriedade,
            p_dat_final,
            sped_k200_h010.quantidade,
            sped_k200_h010.valor_total,
            sped_k200_h010.cnpj9_participante,
            sped_k200_h010.cnpj4_participante,
            sped_k200_h010.cnpj2_participante,
            null,
            null,
            sped_k200_h010.ncm,
            sped_k200_h010.valor_unitario,
            null,
            null,
            null,
            null,
            sped_k200_h010.codigo_deposito,
            sped_k200_h010.unidade_medida,
            sped_k200_h010.codigo_contabil,
            null,
            sped_k200_h010.valor_ir,
            sped_k200_h010.estagio_agrupador,
            sped_k200_h010.estagio_agrupador_simultaneo,
            sped_k200_h010.seq_agrupador);
       EXCEPTION
         WHEN OTHERS THEN
           p_des_erro := 'Erro na inclusao da tabela sped_h010 (1)' || Chr(10) ||
                         ' PRODUTO: ' || sped_k200_h010.nivel ||
                         sped_k200_h010.grupo || sped_k200_h010.subgrupo ||
                         sped_k200_h010.item || Chr(10) || SQLERRM;
           RAISE W_ERRO;
       END;
    end if;
  END LOOP;

EXCEPTION
  WHEN W_ERRO then
    p_des_erro := 'Erro na procedure inter_pr_gera_sped_bh010 ' || Chr(10) ||
                  p_des_erro;
  WHEN OTHERS THEN
    p_des_erro := 'Outros erros na procedure inter_pr_gera_sped_bh010 ' ||
                  Chr(10) || SQLERRM;
END inter_pr_gera_sped_bh010;
/
