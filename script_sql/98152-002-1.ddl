alter table pcpc_045 add (
  usuario_systextil varchar2(30)
);

alter table pcpc_045
modify usuario_systextil default ' ';

comment on column pcpc_045.usuario_systextil
  is 'Indica qual usuário do systextil que fez a movimentação';

