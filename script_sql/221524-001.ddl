create table obrf_061
(
CODIGO_EMPRESA               NUMBER(3)     default 0,                                                                                                                                                
MES_REFERENCIA               NUMBER(2)     default 0,     
ANO_REFERENCIA               NUMBER(4)     default 0,                                                              
II_TIPO_DECLARACAO           NUMBER(1)     default 1,                                                                                                                                                               
COD_TIPO_DECLARACAO          NUMBER(1)     default 0,
TIPO_REGISTRO                NUMBER(2)     default 0,
QUADRO                       NUMBER(2)     default 0, 
SEQ                          NUMBER(3)     default 0,
COD_BENEFICIO                NUMBER(4)     default 0,
NUMERO_CONCESSAO             NUMBER(15)    default 0,
SUBTIPO_DCIP                 NUMBER(4)     default 0,
BASE_CALC_ICMS_OPER          NUMBER(17,2)  default 0,
VALOR_ICMS_OPER              NUMBER(17,2)  default 0,
COD_CALCULO_FUMDES           VARCHAR2(1)   default '',
VALOR_FUMDES                 NUMBER(17,2)  default 0,
COD_FUNDO_SOCIAL             VARCHAR2(1)   default '',
VLR_FUNDO_SOCIAL             NUMBER(17,2)  default 0,
BASE_CALC_ICMS_DEVOL         NUMBER(17,2)  default 0,
VLR_ICMS_DEVOL               NUMBER(17,2)  default 0,
VLR_FUMDES_DEVOL             NUMBER(17,2)  default 0,
VLR_FUNDO_SOCIAL_DEVOL       NUMBER(17,2)  default 0
);

ALTER TABLE obrf_061
ADD CONSTRAINT OBRF_061_PK PRIMARY KEY (CODIGO_EMPRESA,MES_REFERENCIA,ANO_REFERENCIA,II_TIPO_DECLARACAO,COD_TIPO_DECLARACAO,TIPO_REGISTRO,QUADRO,SEQ);

COMMENT ON COLUMN obrf_061.CODIGO_EMPRESA IS 'c�digo da empresa';  
COMMENT ON COLUMN obrf_061.MES_REFERENCIA IS 'mes de refer�ncia';                                                                                                                                            
COMMENT ON COLUMN obrf_061.ANO_REFERENCIA IS 'ano de refer�ncia';                                                                                   
