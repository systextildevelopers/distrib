insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('basi_e540', 'Relatório de lancamento das Referencias da Colecao', 1,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'basi_e540', 'inte_menu', 1, 20, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Relatório de lancamento das Referencias da Colecao'
 where hdoc_036.codigo_programa = 'basi_e540'
   and hdoc_036.locale          = 'es_ES';
commit;
