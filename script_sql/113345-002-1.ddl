/*-------------------------------
pedi
systextil-vendas
systextil-clientes
systextil-lunender
inte
--------------------------------*/

ALTER TABLE pedi_010
MODIFY (valor_ult_compra NUMERIC(15,2));
ALTER TABLE pedi_010
MODIFY (val_maior_fatur NUMERIC(15,2));
ALTER TABLE pedi_010
MODIFY (valor_ult_fatur NUMERIC(15,2)) ;
--Valores que n�o estao na tela
ALTER TABLE pedi_010
MODIFY (ACUMULADO_VENDAS NUMERIC(15,2));
ALTER TABLE pedi_010
MODIFY (VAL_MAIOR_FATUR_ANT NUMERIC(15,2));
ALTER TABLE pedi_010
MODIFY (VALOR_ULT_COMPRA_ANT NUMERIC(15,2));
ALTER TABLE pedi_010
MODIFY (VALOR_ULT_FATUR_ANT NUMERIC(15,2));

ALTER TABLE pedi_067
MODIFY (limite_credito NUMERIC(15,2));
ALTER TABLE pedi_067
MODIFY (maior_fatura NUMERIC(15,2));
ALTER TABLE pedi_067
MODIFY (maior_acumulo NUMERIC(15,2));
ALTER TABLE pedi_067
MODIFY (ultima_compra NUMERIC(15,2)); 

ALTER TABLE pedi_095
MODIFY (val_tabela_preco NUMERIC(19,6));
--Valores que n�o estao na tela
ALTER TABLE pedi_095
MODIFY (MAIOR_VALOR_TING NUMERIC(15,2));

ALTER TABLE pedi_095_log
MODIFY (VAL_TABELA_PRECO_OLD NUMERIC(19,6));
--Valores que n�o estao na tela
ALTER TABLE pedi_095_log
MODIFY (VAL_TABELA_PRECO_NEW NUMERIC(19,6));

ALTER TABLE pedi_110
MODIFY (valor_unitario NUMERIC(17,4));
ALTER TABLE pedi_110
MODIFY (um_faturamento_valor NUMERIC(17,4));


ALTER TABLE pedi_100
MODIFY (valor_total_pedi NUMERIC(15,2));
ALTER TABLE pedi_100
MODIFY (valor_saldo_pedi NUMERIC(15,2));
ALTER TABLE pedi_100
MODIFY (valor_liq_itens NUMERIC(15,2));
--Valores que n�o estao na tela
ALTER TABLE pedi_100
MODIFY (DESCONTO_ESPECIAL NUMERIC(15,2));


ALTER TABLE pedi_100_hist
MODIFY (VALOR_SALDO_PEDI_OLD NUMERIC(15,2));
ALTER TABLE pedi_100_hist
MODIFY (VALOR_SALDO_PEDI_NEW NUMERIC(15,2));

ALTER TABLE pedi_400
MODIFY (valor_devolvido NUMERIC(15,2));

ALTER TABLE pedi_182
MODIFY (limite_alcada NUMERIC(15,2));

ALTER TABLE pedi_160
MODIFY (valor_vendido NUMERIC(15,2));
--Valores que n�o estao na tela
ALTER TABLE pedi_160
MODIFY (VALOR_FATURADO NUMERIC(15,2));
ALTER TABLE pedi_160
MODIFY (VALOR_CANC_VENDA NUMERIC(15,2));
ALTER TABLE pedi_160
MODIFY (VALOR_CANC_FATU NUMERIC(15,2));

ALTER TABLE pedi_165
MODIFY (valor_vendido NUMERIC(15,2));
--Valores que n�o estao na tela
ALTER TABLE pedi_165
MODIFY (VALOR_FATURADO NUMERIC(15,2));
ALTER TABLE pedi_165
MODIFY (VALOR_CANC_VENDA NUMERIC(15,2));
ALTER TABLE pedi_165
MODIFY (VALOR_CANC_FATU NUMERIC(15,2));

ALTER TABLE pedi_113
MODIFY (valor_unitario NUMERIC(18,5));
--Valores que n�o estao na tela
ALTER TABLE pedi_113
MODIFY (VALOR_UNIT_NEGOCIACAO NUMERIC(18,5));
ALTER TABLE pedi_113
MODIFY (UM_FATURAMENTO_VALOR_UNIT NUMERIC(18,5));

/*----------------------------------------
PEDI_F001 - Valor
PEDI_F002 - Valor
PEDI_F008 - Valor
PEDI_F029 - Valor
PEDI_F012 - Sem Altera��o
PEDI_F017 - Valor
PEDI_F234 - Valor
PEDI_F351 - Valor
PEDI_F352 - Valor
PEDI_F353 - Valor
PEDI_F354 - Valor
PEDI_F355 - Valor
PEDI_F359 - Valor
PEDI_F363 - Valor
PEDI_F375 - Valor
PEDI_F420 - Valor
PEDI_F425 - Sem Altera��o
PEDI_F460 - Sem Altera��o
PEDI_F465 - Valor
PEDI_F570 - Valor
PEDI_F700 - Valor
PEDI_F036 - Valor
PEDI_F130 - Valor
PEDI_F140 - Valor
PEDI_F142 - Valor
PEDI_F182 - Valor
PEDI_F220 - Sem Altera��o
PEDI_F225 - Valor
PEDI_F705 - Valor
PEDI_L425 - Valor
INTE_F220 - Valor

#,###,###,###,##&.&&&&;-#,###,###,###,##&.&&&&
-----------------------------------------*/

/
exec inter_pr_recompile;
