create or replace view inter_vi_basi_033_1 as
select basi_050.nivel_item,
       basi_050.grupo_item,
       basi_050.sub_item,
       basi_050.item_item,
       basi_050.alternativa_item,
       basi_050.sequencia,
       basi_060.descricao_parte
       from basi_050, basi_060
where basi_050.nivel_item           = '1'
  and basi_050.grupo_item           = basi_060.grupo_estrutura
  and basi_050.sub_item          = basi_060.subgru_estrutura
  and basi_050.item_item            = basi_060.item_estrutura
  and basi_050.alternativa_item  = basi_060.alternativa_item
  and basi_050.sequencia  = basi_060.sequencia
  and basi_050.nivel_comp           = '2';
