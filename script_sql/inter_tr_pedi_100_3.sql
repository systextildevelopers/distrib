
  CREATE OR REPLACE TRIGGER "INTER_TR_PEDI_100_3" 
   before insert
       or update of situacao_venda, num_periodo_prod, data_entr_venda
   on pedi_100
   for each row

declare

   v_ordem_tingimento  number;
   v_data_embarque     date;
   v_data_embarque_min date;
   v_registro_ok       number;
   v_executa_trigger   number(1);
begin

   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then
      if updating
      then

         if :new.situacao_venda  = 10 and :old.situacao_venda <> 10
         then
            :new.status_pedido    := 0;
            :new.status_expedicao := 9;
         end if;
         if :new.situacao_venda  = 9 and
            :old.situacao_venda <> 5 and
            :old.situacao_venda <> 9
         then
            :new.status_pedido    := 2;
            :new.status_expedicao := 9;
         end if;

         -- Se alterou o periodo de producao atualiza o periodo de
         -- producao para o planejamento.
         if :new.num_periodo_prod <> :old.num_periodo_prod
         then
            update tmrp_041
            set periodo_producao = :new.num_periodo_prod
            where tmrp_041.area_producao   = 6
              and tmrp_041.nr_pedido_ordem = :new.pedido_venda;
         end if;

         -- Quando altera a data de entrega do pedido de venda a prioridade da
         -- producao quando o pedido estiver ligado a OB deve ser recalculado.
         if :new.data_entr_venda <> :old.data_entr_venda
         then
            -- Busca a data de embarque/entrega do pedido de venda destinado a OB
            -- para calcular com o centro de custo os dias para producao.
            v_registro_ok       := 0;
            v_data_embarque     := null;
            v_data_embarque_min := null;

            select count(*)
            into v_registro_ok
            from pcpb_030
            where pcpb_030.nr_pedido_ordem = :old.pedido_venda;

            if v_registro_ok > 0
            then
               -- Busca as OB's destinadas para o pedido.
               for reg_pcpb_030 in (select pcpb_030.ordem_producao
                                    from pcpb_030
                                    where pcpb_030.nr_pedido_ordem = :old.pedido_venda)
               loop

                  begin
                     -- Busca a Ordem de Tingimento para atualizar com a prioridade da producao nova.
                     select pcpb_010.ordem_tingimento
                     into v_ordem_tingimento
                     from pcpb_010
                     where pcpb_010.ordem_producao = reg_pcpb_030.ordem_producao;
                  end;

                  begin
                     -- Dos pedidos destinados para a OB busca a menor data.
                     select min(pedi_100.data_entr_venda)
                     into   v_data_embarque
                     from pedi_100, pcpb_030
                     where pedi_100.pedido_venda   = pcpb_030.nr_pedido_ordem
                       and pedi_100.pedido_venda  <> :old.pedido_venda
                       and pcpb_030.ordem_producao = reg_pcpb_030.ordem_producao
                       and pcpb_030.pedido_corte   = 3;
                     exception
                     when others then
                        v_data_embarque := null;
                  end;

                  if v_data_embarque is not null
                  then
                     if v_data_embarque > :new.data_entr_venda
                     then
                        v_data_embarque_min := :new.data_entr_venda;
                     else
                        v_data_embarque_min := v_data_embarque;
                     end if;
                  else
                     v_data_embarque_min := :new.data_entr_venda;
                  end if;

                  -- Busca a sequencia dos itens de cada OB destinada para o pedido.
                  for reg_pcpb_030_1 in (select pcpb_030.sequencia,  pcpb_030.pano_nivel99,
                                                pcpb_030.pano_grupo, pcpb_030.pano_subgrupo,
                                                pcpb_030.pano_item,  pcpb_030.alternativa,
                                                pcpb_030.roteiro
                                         from pcpb_030
                                         where pcpb_030.ordem_producao  = reg_pcpb_030.ordem_producao
                                           and pcpb_030.nr_pedido_ordem = :old.pedido_venda)
                  loop
                     -- Chama procedure para calcular a prioridade das operacoes.
                     inter_pr_calcula_seq_ob(reg_pcpb_030.ordem_producao,
                                             v_ordem_tingimento,
                                             v_data_embarque_min,
                                             reg_pcpb_030_1.sequencia,
                                             'PEDI_100_3',
                                             reg_pcpb_030_1.pano_nivel99,
                                             reg_pcpb_030_1.pano_grupo,
                                             reg_pcpb_030_1.pano_subgrupo,
                                             reg_pcpb_030_1.pano_item,
                                             reg_pcpb_030_1.alternativa,
                                             reg_pcpb_030_1.roteiro);
                  end loop;
               end loop;
            end if;
         end if;
      end if;
   end if;
end inter_tr_pedi_100_3;
-- ALTER TRIGGER "INTER_TR_PEDI_100_3" ENABLE
 

/

exec inter_pr_recompile;

