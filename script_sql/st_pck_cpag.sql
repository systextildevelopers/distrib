create or replace PACKAGE ST_PCK_CPAG AS 

    TYPE t_projeto IS RECORD
    (
        data_ini_validade   cpag_350.data_ini_validade%type,
        data_fim_validade   cpag_350.data_fim_validade%type
    );

    function exists_tipo_titulo(p_tipo_titulo number) return boolean;
    
    function exists_cancelamento(v_codigo_cancelamento number) return boolean;

    function exists_projeto(p_projeto number, p_subprojeto number, p_servico number) return boolean;
    
    function get_data_validade_projeto(p_projeto number, p_subprojeto number, p_servico number) return t_projeto;

    function exists_adiantamento_imp(p_num_importacao varchar2, p_cgc_9 number, p_cgc_4 number, p_cgc_2 number) return boolean;

    function cria_adiantamento(p_cgc9 			number, 	p_cgc4 		number, p_cgc2 				number, p_empresa 			number, p_centro_custo 	number, 
                               p_nome 			varchar2, 	p_valor 	number, p_data_contrato 	date, 	p_data_vencimento 	date,
                               p_cod_portador   number,     p_moeda 	varchar2,
                               p_num_importacao varchar2,   v_num_adiantam in out number) return varchar2;

END ST_PCK_CPAG;
