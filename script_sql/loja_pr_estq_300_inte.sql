
  CREATE OR REPLACE PROCEDURE "LOJA_PR_ESTQ_300_INTE" (new_processo_systextil  estq_300.processo_systextil%type,
                                            new_numero_documento    estq_300.numero_documento%type,
                                            new_data_movimento      estq_300.data_movimento%type,
                                            new_quantidade          estq_300.quantidade%type,
                                            new_entrada_saida       estq_300.entrada_saida%type,
                                            new_valor_total         estq_300.valor_total%type,
                                            new_nivel_estrutura     estq_300.nivel_estrutura%type,
                                            new_grupo_estrutura     estq_300.grupo_estrutura%type,
                                            new_subgrupo_estrutura  estq_300.subgrupo_estrutura%type,
                                            new_item_estrutura      estq_300.item_estrutura%type,
                                            new_codigo_deposito     estq_300.codigo_deposito%type,
                                            new_codigo_transacao    estq_300.codigo_transacao%type,
                                            new_usuario_systextil   estq_300.usuario_systextil%type,
                                            new_tabela_origem       estq_300.tabela_origem%type,
                                            old_processo_systextil  estq_300.processo_systextil%type,
                                            old_numero_documento    estq_300.numero_documento%type,
                                            old_data_movimento      estq_300.data_movimento%type,
                                            old_quantidade          estq_300.quantidade%type,
                                            old_entrada_saida       estq_300.entrada_saida%type,
                                            old_valor_total         estq_300.valor_total%type,
                                            old_nivel_estrutura     estq_300.nivel_estrutura%type,
                                            old_grupo_estrutura     estq_300.grupo_estrutura%type,
                                            old_subgrupo_estrutura  estq_300.subgrupo_estrutura%type,
                                            old_item_estrutura      estq_300.item_estrutura%type,
                                            old_codigo_deposito     estq_300.codigo_deposito%type,
                                            old_codigo_transacao    estq_300.codigo_transacao%type,
                                            old_usuario_systextil   estq_300.usuario_systextil%type) is

 pvar number;

begin

  pvar := pvar;

end loja_pr_estq_300_inte;
 

/

exec inter_pr_recompile;

