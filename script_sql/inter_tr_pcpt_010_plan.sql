
CREATE OR REPLACE TRIGGER "INTER_TR_PCPT_010_PLAN" 
   before delete or
          update of cod_cancelamento
   on pcpt_010
   for each row
declare
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);

   v_exite_615 number(1);
begin

   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   v_exite_615 := 0;

   for reg_tmrp_630 in (select tmrp_630.ordem_planejamento,          tmrp_630.pedido_venda,
                               tmrp_630.pedido_reserva,
                               tmrp_625.nivel_produto_origem,        tmrp_625.grupo_produto_origem,
                               tmrp_625.subgrupo_produto_origem,     tmrp_625.item_produto_origem,
                               tmrp_625.alternativa_produto_origem,
                               tmrp_630.area_producao,               tmrp_630.ordem_prod_compra,
                               tmrp_625.seq_produto,                 tmrp_625.seq_produto_origem
                        from tmrp_630, tmrp_625
                        where tmrp_625.ordem_planejamento         = tmrp_630.ordem_planejamento
                          and tmrp_625.pedido_venda               = tmrp_630.pedido_venda
                          and tmrp_625.pedido_reserva             = tmrp_630.pedido_reserva
                          and tmrp_625.nivel_produto_origem       = tmrp_630.nivel_produto
                          and tmrp_625.grupo_produto_origem       = tmrp_630.grupo_produto
                          and tmrp_625.subgrupo_produto_origem    = tmrp_630.subgrupo_produto
                          and tmrp_625.item_produto_origem        = tmrp_630.item_produto
                          and tmrp_625.alternativa_produto_origem = tmrp_630.alternativa_produto
                          and tmrp_625.nivel_produto              = :old.cd_pano_nivel99
                          and tmrp_625.grupo_produto              = :old.cd_pano_grupo
                          and tmrp_625.subgrupo_produto           = :old.cd_pano_subgrupo
                          and tmrp_625.item_produto               = :old.cd_pano_item
                          and tmrp_625.alternativa_produto        = :old.alternativa_item
                          and tmrp_630.area_producao              = 2
                        group by  tmrp_630.ordem_planejamento,          tmrp_630.pedido_venda,
                                  tmrp_630.pedido_reserva,
                                  tmrp_625.nivel_produto_origem,        tmrp_625.grupo_produto_origem,
                                  tmrp_625.subgrupo_produto_origem,     tmrp_625.item_produto_origem,
                                  tmrp_625.alternativa_produto_origem,
                                  tmrp_630.area_producao,               tmrp_630.ordem_prod_compra,
                                  tmrp_625.seq_produto,                 tmrp_625.seq_produto_origem)
   loop
      v_exite_615 := 1;

      begin
         insert into tmrp_615
           (ordem_prod,                               area_producao,
            nivel,                                    grupo,
            subgrupo,                                 item,
            alternativa,                              ordem_planejamento,
            pedido_venda,                             pedido_reserva,
            nome_programa,
            nr_solicitacao,                           tipo_registro,
            cgc_cliente9,

            seq_registro,                             sequencia,
            nivel_prod,                               grupo_prod,
            subgrupo_prod,                            item_prod,
            alternativa_prod)
         values
           (reg_tmrp_630.ordem_prod_compra,           reg_tmrp_630.area_producao,
            reg_tmrp_630.nivel_produto_origem,        reg_tmrp_630.grupo_produto_origem,
            reg_tmrp_630.subgrupo_produto_origem,     reg_tmrp_630.item_produto_origem,
            reg_tmrp_630.alternativa_produto_origem,  reg_tmrp_630.ordem_planejamento,
            reg_tmrp_630.pedido_venda,                reg_tmrp_630.pedido_reserva,
            'trigger_planejamento',
            888,                                      888,
            ws_sid,

            reg_tmrp_630.seq_produto,                 reg_tmrp_630.seq_produto_origem,
            :old.cd_pano_nivel99,                     :old.cd_pano_grupo,
            :old.cd_pano_subgrupo,                    :old.cd_pano_item,
            :old.alternativa_item);
         exception when others
         then raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'TMRP_615' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;
   end loop;


   if ((updating and :old.cod_cancelamento <> :new.cod_cancelamento and :old.cod_cancelamento = 0) or deleting) and v_exite_615 = 1
   then
      begin
         delete tmrp_630
         where tmrp_630.ordem_prod_compra    = :old.ordem_tecelagem
           and tmrp_630.area_producao        = 4
           and tmrp_630.nivel_produto        = :old.cd_pano_nivel99
           and tmrp_630.grupo_produto        = :old.cd_pano_grupo
           and tmrp_630.subgrupo_produto     = :old.cd_pano_subgrupo
           and tmrp_630.item_produto         = :old.cd_pano_item
           and tmrp_630.alternativa_produto  = :old.alternativa_item;
         exception when others
         then raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26373', 'TMRP_630' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;
   elsif ((updating and :old.cod_cancelamento <> :new.cod_cancelamento and :old.cod_cancelamento = 0) or deleting) and v_exite_615 = 0
   then
      for reg_16 in (select pcpt_016.ordem_pedido, pcpt_016.quantidade
                     from pcpt_016
                     where pcpt_016.ordem_tecelagem = :old.ordem_tecelagem
                       and pcpt_016.tipo_destino    = 5)
      loop
         inter_pr_destinos_planejamento(reg_16.ordem_pedido,
                                        :old.ordem_tecelagem,
                                        reg_16.quantidade,
                                        :old.cd_pano_nivel99,
                                        :old.cd_pano_grupo,
                                        :old.cd_pano_subgrupo,
                                        :old.cd_pano_item,
                                        :old.alternativa_item,
                                        'D');
      end loop;
   end if;
end inter_tr_pcpt_010_plan;

-- ALTER TRIGGER "INTER_TR_PCPT_010_PLAN" ENABLE
 

/

exec inter_pr_recompile;

