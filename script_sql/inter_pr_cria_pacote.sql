
  CREATE OR REPLACE PROCEDURE "INTER_PR_CRIA_PACOTE" (
   p_empresa          in number,
   p_codigo_usuario   in number,
   p_ordem_producao   in number,
   p_codigo_estagio   in number,
   p_subgrupo         in varchar2,
   p_item             in varchar2,
   p_qtde_pecas       in number
)
is
   ws_usuario_rede                 varchar2(20);
   ws_maquina_rede                 varchar2(40);
   ws_aplicativo                   varchar2(20);
   ws_sid                          number(9);
   ws_empresa                      number(3);
   ws_usuario_systextil            varchar2(250);
   ws_locale_usuario               varchar2(5);

   v_ordem_conf_aux                number;
   v_periodo_producao              number;
   v_referencia_peca               varchar2(5);
   v_alternativa_peca              number;
   v_roteiro_peca                  number;
   v_subgru_estrutura              varchar2(3);
   v_item_estrutura                varchar2(6);
   v_estagio_corte                 number;
   v_salva_estagio                 number;
   v_salva_sequencia               number;
   v_estagio_ant                   number;
   v_ordem_confeccao               number;
   v_tem_registro                  number;
   v_inseriu_ordem_confeccao       number;
   v_passou_estagio_corte          number;
   v_grupo_maq                     varchar2(4);
   v_sub_maq                       varchar2(3);
   v_div_aproduzir                 number;
   v_seq_operacao                  number;

/*
   #################################################################################################
   Busca o �ltimo nro da ordem de confe��o no per�odo
   #################################################################################################
*/
PROCEDURE f_get_pcpc_010_ult_nro (
    p_periodo_producao_proc  in number,
    r_ultimo_numero          out number)
IS
BEGIN
   r_ultimo_numero := 0;

   begin
      update pcpc_010
      set   pcpc_010.ultimo_numero    = pcpc_010.ultimo_numero
      where pcpc_010.area_periodo     = 1
        and pcpc_010.periodo_producao = p_periodo_producao_proc;
   end;

   begin
      select nvl(pcpc_010.ultimo_numero,0)
      into   r_ultimo_numero
      from pcpc_010
      where pcpc_010.area_periodo     = 1
        and pcpc_010.periodo_producao = p_periodo_producao_proc;
   end;
end f_get_pcpc_010_ult_nro;


/*
   #################################################################################################
   Encontra um novo per�odo do produ��o, pois no per�odo atual de produ��o n�o tem mais ordem de
   confec��o vagas.
   #################################################################################################
*/
PROCEDURE f_get_pcpc_010_novo_periodo (
    p_periodo_producao_proc   in  number,
    v_periodo_ordem           out number)
IS
   vv_codigo_empresa    number;
BEGIN
   vv_codigo_empresa := 0;

   begin
      select nvl(pcpc_010.codigo_empresa,0)
      into   vv_codigo_empresa
      from pcpc_010
      where pcpc_010.area_periodo     = 1
        and pcpc_010.periodo_producao = p_periodo_producao_proc;
   exception when others then
      vv_codigo_empresa := 1;
   end;

   begin
      select nvl(min(pcpc_010.periodo_producao),0)
      into   v_periodo_ordem
      from pcpc_010
      where pcpc_010.area_periodo     = 1
        and pcpc_010.periodo_producao > p_periodo_producao_proc
        and pcpc_010.codigo_empresa   = vv_codigo_empresa
        and pcpc_010.situacao_periodo < 3;
   exception when others then
      v_periodo_ordem := 0;
   end;

   if v_periodo_ordem = 0
   then
      raise_application_error(-20000,inter_fn_buscar_tag('ds31966#ATEN��O! N�o existe per�odo de produ��o vagos para a �rea de confec��o.',ws_locale_usuario,ws_usuario_systextil));
      v_periodo_ordem := 0;
   end if;
END f_get_pcpc_010_novo_periodo;


/*
   #################################################################################################
   Econtra a pr�xima ordem de confec��o vaga
   #################################################################################################
*/
PROCEDURE f_get_pcpc_040_ordem_confecao(
    p_periodo_producao_prin  in out number,
    vv_ordem_confeccao              out number)
IS
   vv_nro_tentativas     number;
BEGIN
   vv_ordem_confeccao := 0;

   f_get_pcpc_010_ult_nro(p_periodo_producao_prin,vv_ordem_confeccao);

   if vv_ordem_confeccao < 0
   then
      raise_application_error(-20000,inter_fn_buscar_tag('ds32093#ATEN��O! N�o encontrou a pr�xima ordem de confec��o, verifique o cadastro de per�odos de produ��o.',ws_locale_usuario,ws_usuario_systextil));
   end if;

   vv_nro_tentativas := 1;

   loop
      vv_ordem_confeccao := vv_ordem_confeccao + 1;

      if vv_ordem_confeccao > 99999
      then
         f_get_pcpc_010_novo_periodo(p_periodo_producao_prin,p_periodo_producao_prin);

         vv_ordem_confeccao := 0;
         f_get_pcpc_010_ult_nro(p_periodo_producao_prin,vv_ordem_confeccao);
         vv_ordem_confeccao := vv_ordem_confeccao + 1;

         vv_nro_tentativas  := vv_nro_tentativas + 1;
      end if;

      begin
         select nvl(count(1),0)
         into   v_tem_registro
         from pcpc_040
         where pcpc_040.periodo_producao = p_periodo_producao_prin
           and pcpc_040.ordem_confeccao  = vv_ordem_confeccao;
      end;

      exit when v_tem_registro = 0;

      if vv_nro_tentativas  =  5
      then
         raise_application_error(-20000,inter_fn_buscar_tag('ds32093#ATEN��O! N�o encontrou a pr�xima ordem de confec��o, verifique o cadastro de per�odos de produ��o.',ws_locale_usuario,ws_usuario_systextil));
      end  if;

      exit when vv_nro_tentativas = 5;

   end loop;

   begin
      update pcpc_010
      set   pcpc_010.ultimo_numero    = vv_ordem_confeccao
      where pcpc_010.area_periodo     = 1
        and pcpc_010.periodo_producao = p_periodo_producao_prin;
   end;

END f_get_pcpc_040_ordem_confecao;


/*
   #################################################################################################
   Econtra o SUBGRUPU e o ITEM que deve utilizado para ler o roteiro
   #################################################################################################
*/
PROCEDURE f_get_produto_roteiro (
    p_nivel_rot            in varchar2,
    p_grupo_rot            in varchar2,
    p_subgrupo_rot         in out varchar2,
    p_item_rot             in out varchar2,
    P_alternativa_peca_rot in number,
    P_roteiro_peca_rot     in number)
IS
   v_count                 number(9);
   v_subgrupo_rot          varchar(3);
   v_item_rot              varchar(6);
BEGIN
   v_subgrupo_rot := p_subgrupo_rot;
   v_item_rot     := p_item_rot;

   begin
      select count(1)
      into   v_count
      from mqop_050
      where mqop_050.nivel_estrutura  = p_nivel_rot
        and mqop_050.grupo_estrutura  = p_grupo_rot
        and mqop_050.subgru_estrutura = p_subgrupo_rot
        and mqop_050.item_estrutura   = p_item_rot
        and mqop_050.numero_alternati = p_alternativa_peca_rot
        and mqop_050.numero_roteiro   = p_roteiro_peca_rot;
      if v_count = 0
      then
         v_item_rot := '000000';

         begin
            select count(*)
            into   v_count
            from mqop_050
            where mqop_050.nivel_estrutura  = p_nivel_rot
              and mqop_050.grupo_estrutura  = p_grupo_rot
              and mqop_050.subgru_estrutura = p_subgrupo_rot
              and mqop_050.item_estrutura   = v_item_rot
              and mqop_050.numero_alternati = p_alternativa_peca_rot
              and mqop_050.numero_roteiro   = p_roteiro_peca_rot;
            if v_count = 0
            then
               v_subgrupo_rot := '000';
               v_item_rot     := p_item_rot;

               begin
                  select count(*)
                  into   v_count
                  from mqop_050
                  where mqop_050.nivel_estrutura  = p_nivel_rot
                    and mqop_050.grupo_estrutura  = p_grupo_rot
                    and mqop_050.subgru_estrutura = v_subgrupo_rot
                    and mqop_050.item_estrutura   = v_item_rot
                    and mqop_050.numero_alternati = p_alternativa_peca_rot
                    and mqop_050.numero_roteiro   = p_roteiro_peca_rot;
                  if  v_count = 0
                  then
                     v_item_rot := '000000';
                  end if;
               end;
            end if;
         end;
      end if;
   end;

   p_subgrupo_rot := v_subgrupo_rot;
   p_item_rot     := v_item_rot;

END f_get_produto_roteiro;

/*
   #################################################################################################
   Atualiza historico da OP
   #################################################################################################
*/
PROCEDURE f_atz_historico (
    p_ordem_producao_hist    in number,
    p_codigo_usuario_hist    in number,
    p_ordem_confeccao        in number)
IS
   v_historico_ordem_aux     varchar2(4000);
   v_mensagem_historico      varchar2(4000);
   v_nome_form               varchar2(30);
BEGIN
   begin
      select pcpc_020.historico_ordem
      into   v_historico_ordem_aux
      from pcpc_020
      where pcpc_020.ordem_producao = p_ordem_producao_hist;
   exception when others then
      v_historico_ordem_aux  := '';
   end;

   v_nome_form := inter_fn_nome_programa(ws_sid);

   if v_historico_ordem_aux is null
   or v_historico_ordem_aux  = ' '
   then
      v_mensagem_historico := inter_fn_buscar_tag('lb00348#USU�RIO: ',ws_locale_usuario,ws_usuario_systextil)            || to_char(p_codigo_usuario_hist)     ||
                              inter_fn_buscar_tag('lb04093# - ',ws_locale_usuario,ws_usuario_systextil)                 || ws_usuario_systextil               ||
                              '          '          ||
                              inter_fn_buscar_tag('lb06759#PROGRAMA: ',ws_locale_usuario,ws_usuario_systextil)           || v_nome_form                        ||
                              chr(10)               ||
                              inter_fn_buscar_tag('lb00004#DATA:',ws_locale_usuario,ws_usuario_systextil)                || to_char(sysdate,'dd-mm-yyyy')      ||
                              inter_fn_buscar_tag('lb05276#HORA:',ws_locale_usuario,ws_usuario_systextil)                || to_char(sysdate,'hh24:mi')         ||
                              '          '          ||
                              inter_fn_buscar_tag('lb06122#ORDEM CONFEC��O: ',ws_locale_usuario,ws_usuario_systextil)   || to_char(p_ordem_confeccao,'00000') ||
                              chr(010)              ||
                              inter_fn_buscar_tag('lb24903#A��O: INCLUS�O DA ORDEM DE CONFEC��O',ws_locale_usuario,ws_usuario_systextil);
   else
      v_mensagem_historico := v_historico_ordem_aux    ||
                              chr(010) ||
                              chr(010) ||
                              inter_fn_buscar_tag('lb00348#USU�RIO: ',ws_locale_usuario,ws_usuario_systextil)            ||  to_char(p_codigo_usuario_hist)     ||
                              inter_fn_buscar_tag('lb04093#1 - ',ws_locale_usuario,ws_usuario_systextil)                 || ws_usuario_systextil                ||
                              '          '          ||
                              inter_fn_buscar_tag('lb06759#PROGRAMA: ',ws_locale_usuario,ws_usuario_systextil)           || v_nome_form ||
                              chr(10)               ||
                              inter_fn_buscar_tag('lb00004#DATA:',ws_locale_usuario,ws_usuario_systextil)                || to_char(sysdate,'dd-mm-yyyy')       ||
                              inter_fn_buscar_tag('lb05276#HORA:',ws_locale_usuario,ws_usuario_systextil)                || to_char(sysdate,'hh24:mi')          ||
                              '          '          ||
                              inter_fn_buscar_tag('lb06122#1ORDEM CONFEC��O: ',ws_locale_usuario,ws_usuario_systextil)   || to_char(p_ordem_confeccao,'00000') ||
                              chr(10)               ||
                              inter_fn_buscar_tag('lb24903#1A��O: INCLUS�O DA ORDEM DE CONFEC��O',ws_locale_usuario,ws_usuario_systextil);
   end if;

   begin
      UPDATE pcpc_020
      set   pcpc_020.historico_ordem = substr(v_mensagem_historico,4000)
      where pcpc_020.ordem_producao = p_ordem_producao;
   end;

end f_atz_historico;

/*
   #######################
   PROCESSAMENTO PRINCIPAL
   #######################
*/
begin
   -- Dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   begin
      select   pcpc_020.periodo_producao,            pcpc_020.referencia_peca,
               pcpc_020.alternativa_peca,            pcpc_020.roteiro_peca
      into     v_periodo_producao,                   v_referencia_peca,
               v_alternativa_peca,                   v_roteiro_peca
      from pcpc_020
      where pcpc_020.ordem_producao = p_ordem_producao;
   exception when others then
      raise_application_error(-20000,'ATEN��O! Ordem de produ��o n�o cadastrada.');
   end;

   begin
      select nvl(max(pcpc_040.ordem_confeccao),0)
      into v_ordem_conf_aux
      from pcpc_040
      where pcpc_040.ordem_producao   = p_ordem_producao
        and pcpc_040.proconf_nivel99  = '1'
        and pcpc_040.proconf_grupo    = v_referencia_peca
        and pcpc_040.proconf_subgrupo = p_subgrupo
        and pcpc_040.proconf_item     = p_item;
   exception when others then
      v_ordem_conf_aux := 0;
   end;

   v_ordem_confeccao := 0;

   f_get_pcpc_040_ordem_confecao(v_periodo_producao,v_ordem_confeccao);


   f_atz_historico (p_ordem_producao,    p_codigo_usuario,   v_ordem_confeccao);

   if v_ordem_conf_aux = 0
   then
      v_subgru_estrutura := p_subgrupo;
      v_item_estrutura   := p_item;

      f_get_produto_roteiro('1',v_referencia_peca,v_subgru_estrutura,v_item_estrutura,v_alternativa_peca,v_roteiro_peca);

      begin
         select basi_030.estagio_altera_programado,    basi_030.div_prod
         into   v_estagio_corte,                       v_div_aproduzir
         from basi_030
         where basi_030.nivel_estrutura = '1'
           and basi_030.referencia      = v_referencia_peca;
      exception when others then
         v_estagio_corte := 0;
      end;

      v_salva_estagio           := 0;
      v_salva_sequencia         := 0;
      v_inseriu_ordem_confeccao := 0;
      v_passou_estagio_corte    := 0;

      for reg_roteiro in (select mqop_050.codigo_estagio,     mqop_050.codigo_operacao,
                                 mqop_050.centro_custo,       mqop_050.minutos,
                                 mqop_050.sequencia_estagio,  mqop_050.estagio_depende,
                                 mqop_040.pede_produto,       mqop_050.codigo_familia,
                                 mqop_050.seq_operacao
                          from mqop_040, mqop_050
                          where mqop_050.nivel_estrutura  = '1'
                            and mqop_050.grupo_estrutura  = v_referencia_peca
                            and mqop_050.subgru_estrutura = v_subgru_estrutura
                            and mqop_050.item_estrutura   = v_item_estrutura
                            and mqop_050.numero_alternati = v_alternativa_peca
                            and mqop_050.numero_roteiro   = v_roteiro_peca
                            and mqop_050.codigo_operacao  = mqop_040.codigo_operacao
                          order by mqop_050.seq_operacao      ASC,
                                   mqop_050.sequencia_estagio ASC,
                                   mqop_050.estagio_depende   ASC)
      loop
         v_inseriu_ordem_confeccao := 1;

         v_tem_registro := 0;

         begin
            select nvl(count(*),0)
            into v_tem_registro
            from mqop_045
            where mqop_045.codigo_operacao = reg_roteiro.codigo_operacao;
         exception when others then
            v_tem_registro := 0;
         end;

         if  v_salva_estagio           <> reg_roteiro.codigo_estagio
         and reg_roteiro.pede_produto  <> 1
         and v_tem_registro            = 0
         then
            if reg_roteiro.sequencia_estagio <> v_salva_sequencia
            then
               v_estagio_ant := v_salva_estagio;
            end if;

            if reg_roteiro.sequencia_estagio = 0
            then
               v_estagio_ant := v_salva_estagio;
            end if;

            begin
               insert into pcpc_040 (
                  periodo_producao,              ordem_confeccao,
                  codigo_estagio,                sequencia_estagio,
                  ordem_producao,                proconf_nivel99,

                  proconf_grupo,                 proconf_subgrupo,
                  proconf_item,                  qtde_pecas_prog,
                  estagio_anterior,              qtde_pecas_prod,

                  codigo_familia,                qtde_programada,
                  seq_operacao,                  estagio_depende,
                  executa_trigger
               ) VALUES (
                  v_periodo_producao,            v_ordem_confeccao,
                  reg_roteiro.codigo_estagio,    reg_roteiro.sequencia_estagio,
                  p_ordem_producao,              '1',

                  v_referencia_peca,             p_subgrupo,
                  p_item,                        p_qtde_pecas,
                  v_estagio_ant,                 0,

                  reg_roteiro.codigo_familia,    p_qtde_pecas,
                  reg_roteiro.seq_operacao,      reg_roteiro.estagio_depende,
                  3
               );
            exception when others then
               -- ATEN��O! N�o inseriu {0}. Mensagem do banco de dados: {1}
               raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'PCPC_040' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
            end;

            v_salva_sequencia := reg_roteiro.sequencia_estagio;
            v_salva_estagio   := reg_roteiro.codigo_estagio;

            if reg_roteiro.codigo_estagio = v_estagio_corte
            then
               v_passou_estagio_corte := 1;
            end if;

            if v_passou_estagio_corte = 1
            then
               begin
                  select mqop_040.grupo_maquinas, mqop_040.sub_maquina
                  into   v_grupo_maq,             v_sub_maq
                  from mqop_040
                  where mqop_040.codigo_operacao = reg_roteiro.codigo_operacao;
               end;

               if reg_roteiro.minutos > 0.000
               then
                  v_tem_registro := 0;

                  begin
                     select nvl(count(*),0)
                     into v_tem_registro
                     from pcpc_060
                     where pcpc_060.periodo_producao = v_periodo_producao
                       and pcpc_060.divisao_producao = v_div_aproduzir
                       and pcpc_060.centro_custo     = reg_roteiro.centro_custo
                       and pcpc_060.grupo_maquina    = v_grupo_maq
                       and pcpc_060.sub_maquina      = v_sub_maq;
                  end;

                  if v_tem_registro = 0
                  then
                     begin
                        insert into pcpc_060 (
                           periodo_producao, 	          divisao_producao,
                           centro_custo,     	          grupo_maquina,
                           sub_maquina,      	          horas_alocadas
                        ) VALUES (
                           v_periodo_producao,	        v_div_aproduzir,
                           reg_roteiro.centro_custo,	  v_grupo_maq,
                           v_sub_maq,		                reg_roteiro.minutos * p_qtde_pecas
                        );
                     end;
                  else
                     begin
                        update pcpc_060
                        set   pcpc_060.horas_alocadas   = pcpc_060.horas_alocadas + (reg_roteiro.minutos* p_qtde_pecas)
                        where pcpc_060.periodo_producao = v_periodo_producao
                          and pcpc_060.divisao_producao = v_div_aproduzir
                          and pcpc_060.centro_custo     = reg_roteiro.centro_custo
                          and pcpc_060.grupo_maquina    = v_grupo_maq
                          and pcpc_060.sub_maquina      = v_sub_maq;
                     end;
                  end if;
               end if;
            end if;
         end if;
      end loop;

      if v_inseriu_ordem_confeccao = 0
      then
         begin
            insert into pcpc_040 (
               periodo_producao,       ordem_confeccao,
               codigo_estagio,         ordem_producao,
               proconf_nivel99,        proconf_grupo,

               proconf_subgrupo,       proconf_item,
               qtde_pecas_prog,        qtde_pecas_prod,
               codigo_familia,         qtde_programada,

               seq_operacao,           executa_trigger
            ) VALUES (
               v_periodo_producao,     v_ordem_confeccao,
               0,                      p_ordem_producao,
               '1',                    v_referencia_peca,

               v_subgru_estrutura,     v_item_estrutura,
               p_qtde_pecas,           0,
               0,                      p_qtde_pecas,

               0,                      3
            );
         exception when others then
            -- ATEN��O! N�o inseriu {0}. Mensagem do banco de dados: {1}
            raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'PCPC_040(2)' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
         end;
      end if;
   else
      for reg_copia in (select pcpc_040.codigo_estagio,        pcpc_040.sequencia_estagio,
                               pcpc_040.estagio_anterior,      pcpc_040.codigo_familia,
                               pcpc_040.seq_operacao,          pcpc_040.estagio_depende
                        from pcpc_040
                        where pcpc_040.ordem_producao  = p_ordem_producao
                          and pcpc_040.ordem_confeccao = v_ordem_conf_aux
                        order by pcpc_040.seq_operacao      ASC,
                                 pcpc_040.sequencia_estagio ASC,
                                 pcpc_040.estagio_depende   ASC)
      loop
         begin
            insert into pcpc_040 (
               periodo_producao,                ordem_confeccao,
               codigo_estagio,                  sequencia_estagio,
               ordem_producao,                  proconf_nivel99,

               proconf_grupo,                   proconf_subgrupo,
               proconf_item,                    qtde_pecas_prog,
               estagio_anterior,                qtde_pecas_prod,

               codigo_familia,                  qtde_programada,
               seq_operacao,                    estagio_depende,
               executa_trigger
            ) values (
               v_periodo_producao,              v_ordem_confeccao,
               reg_copia.codigo_estagio,        reg_copia.sequencia_estagio,
               p_ordem_producao,                 '1',

               v_referencia_peca,               p_subgrupo,
               p_item,                          p_qtde_pecas,
               reg_copia.estagio_anterior,      0,

               reg_copia.codigo_familia,        p_qtde_pecas,
               reg_copia.seq_operacao,          reg_copia.estagio_depende,
               3
            );
         exception when others then
            -- ATEN��O! N�o inseriu {0}. Mensagem do banco de dados: {1}
            raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'PCPC_040(3)' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
         end;
      end loop;
   end if;

   v_seq_operacao := 0;

   begin
      select nvl(min(pcpc_040.seq_operacao),0)
      into v_seq_operacao
      from pcpc_040
      where pcpc_040.ordem_producao  = p_ordem_producao
        and pcpc_040.ordem_confeccao = v_ordem_confeccao
        and pcpc_040.codigo_estagio  = p_codigo_estagio;
   exception when others then
      v_seq_operacao := 0;
   end;

   -- Gera apontamento dos pacotes
   for reg_apont in (select pcpc_040.codigo_estagio,        pcpc_040.qtde_pecas_prog
                     from pcpc_040
                     where pcpc_040.ordem_producao  = p_ordem_producao
                       and pcpc_040.ordem_confeccao = v_ordem_confeccao
                       and pcpc_040.seq_operacao    < v_seq_operacao
                     order by pcpc_040.seq_operacao      ASC,
                              pcpc_040.sequencia_estagio ASC,
                              pcpc_040.estagio_depende   ASC)
   loop
      inter_pr_aponta_pacote (p_empresa,                    p_codigo_usuario,
                              p_ordem_producao,             v_ordem_confeccao,
                              reg_apont.codigo_estagio,     1,
                              reg_apont.qtde_pecas_prog);

   end loop;
end inter_pr_cria_pacote;

 

/

exec inter_pr_recompile;

