
  CREATE OR REPLACE TRIGGER "INTER_TR_HDOC_001_PED_RESERVA" 
BEFORE INSERT OR DELETE
OR UPDATE of tipo, codigo, descricao, data1,
	data2, classificacao1, classificacao2, quantidade,
	observacao, descricao2, campo_numerico01, campo_numerico02,
	campo_numerico03, campo_numerico04, descricao3, campo_numerico05,
	campo_numerico06, valor01, descricao4, obs_ingles,
	valor02, valor03, valor04, valor05,
	campo_numerico07, campo_numerico08, campo_numerico09, campo_numerico10,
	campo_numerico11, campo_numerico12, campo_numerico13, campo_numerico14,
	campo_numerico15, campo_numerico16, campo_numerico17, campo_numerico18,
	campo_numerico19, descricao5, descricao6, descricao7,
	descricao8, descricao9, descricao10, descricao11,
	campo_numerico20, campo_numerico21, campo_numerico22, campo_numerico23,
	campo_numerico24, campo_numerico25, descricao12, descricao13,
	descricao14, descricao15, campo_numerico26, campo_numerico27,
	campo_numerico28, campo_numerico29, campo_numerico30, codigo2,
	descricao16, campo_numerico31, campo_numerico32, campo_numerico33,
	campo_numerico34, campo_numerico35, descricao17, descricao18,
	descricao19, descricao20, descricao21
ON HDOC_001 for each row
declare
  w_tipo_pedido number;
BEGIN

   if inserting and :new.tipo = 341 and :new.codigo > 0
   then
      w_tipo_pedido := 6;

      INSERT INTO  TMRP_800 (
        TIPO,
        CODIGO,
        DESCRICAO,
        COR_INICIAL,
        COR_FINAL,
        COR_PADRAO
      ) VALUES (
        w_tipo_pedido,
        :new.codigo,
        :new.descricao,
        :new.descricao2,
        :new.descricao2,
        0
      );
   end if;

   if updating and :new.tipo = 341 and :new.codigo > 0
   then
      w_tipo_pedido := 6;

      update tmrp_800 set codigo      = :new.codigo,
                          descricao   = :new.descricao,
                          cor_inicial = :new.descricao2,
                          cor_final   = :new.descricao2
      where tipo   = w_tipo_pedido
        and codigo = :old.codigo;
   end if;

   if deleting and :old.codigo > 0
   then
      delete tmrp_800
      where tipo   = :old.tipo
        and codigo = :old.codigo;
   end if;

end INTER_TR_HDOC_001_PED_RESERVA;

/* versao: 2 */
-- ALTER TRIGGER "INTER_TR_HDOC_001_PED_RESERVA" ENABLE
 

/

exec inter_pr_recompile;

