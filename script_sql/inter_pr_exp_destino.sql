
  CREATE OR REPLACE PROCEDURE "INTER_PR_EXP_DESTINO" 
   (p_codigo_projeto        in varchar2,         p_sequencia_subprojeto  in number,
    p_nivel_item            in varchar2,         p_grupo_item            in varchar2,
    p_subgru_item           in varchar2,         p_item_item             in varchar2,
    p_alternativa_produto   in number,
    p_nivel_comp            in varchar2,         p_grupo_comp            in varchar2,
    p_subgru_comp           in varchar2,         p_item_comp             in varchar2,
    p_cliente9              in number,           p_cliente4              in number,
    p_cliente2              in number,
    p_sequencia_estrutura   in number,           p_seq_cor               in varchar2,
    p_codigo_desenho        in varchar2,         p_tipo_destino          in number,
    p_tabela_origem         in varchar2)
is
   v_total_027          number;
   v_total_028          number;

PROCEDURE inserir_basi_027_028 (vp_nivel_comp         in     varchar2,
                                vp_grupo_comp         in     varchar2,
                                vp_subgrupo_comp      in     varchar2,
                                vp_item_comp          in     varchar2,
                                vp_codigo_projeto     in     varchar2,
                                vp_sequencia_projeto  in     number,
                                vp_nivel_item         in     varchar2,
                                vp_grupo_item         in     varchar2,
                                vp_subgrupo_item      in     varchar2,
                                vp_item_item          in     varchar2,
                                vp_alternativa_item   in     number,
                                vp_cliente9           in     number,
                                vp_cliente4           in     number,
                                vp_cliente2           in     number)
IS

BEGIN
   if vp_nivel_comp    is not null and vp_nivel_comp    <> '0'      and
      vp_grupo_comp    is not null and vp_grupo_comp    <> '00000'  and
      vp_subgrupo_comp is not null and vp_subgrupo_comp <> '000'    and
      vp_item_comp     is not null and vp_item_comp     <> '000000'
   then
      --Count para verficar se ja existe o componente
      begin
         select count(1)
         into v_total_027
         from basi_027
         where basi_027.nivel_componente    = vp_nivel_comp
           and basi_027.grupo_componente    = vp_grupo_comp
           and basi_027.subgrupo_componente = vp_subgrupo_comp
           and basi_027.item_componente     = vp_item_comp
           and basi_027.codigo_projeto      = vp_codigo_projeto
           and basi_027.sequencia_projeto   = vp_sequencia_projeto
           and basi_027.nivel_item          = vp_nivel_item
           and basi_027.grupo_item          = vp_grupo_item
           and basi_027.subgrupo_item       = vp_subgrupo_item
           and basi_027.item_item           = vp_item_item
           and basi_027.alt_item            = vp_alternativa_item;
      end;

      if v_total_027 = 0
      then
         begin
            INSERT INTO basi_027 (
                  nivel_componente,           grupo_componente,
                  subgrupo_componente,        item_componente,
                  codigo_projeto,             sequencia_projeto,
                  cnpj_cliente9,
                  cnpj_cliente4,              cnpj_cliente2,
                  nivel_item,                 grupo_item,
                  subgrupo_item,              item_item,
                  alt_item
            ) VALUES (
                  vp_nivel_comp,              vp_grupo_comp,
                  vp_subgrupo_comp,           vp_item_comp,
                  vp_codigo_projeto,          vp_sequencia_projeto,
                  vp_cliente9,
                  vp_cliente4,                vp_cliente2,
                  vp_nivel_item,              vp_grupo_item,
                  vp_subgrupo_item,           vp_item_item,
                  vp_alternativa_item
             );
         exception when OTHERS then
           raise_application_error (-20000, 'Nao atualizou a tabela de Relacionamento da Situacao(basi_027)');
         end;
      end if;

       if p_nivel_comp = '2'
      then
         -- Consiste se ha registro do componente na tabela de componente aprovado.
         select count(1)
         into v_total_028
         from basi_028
         where basi_028.nivel_componente    = vp_nivel_comp
           and basi_028.grupo_componente    = vp_grupo_comp
           and basi_028.subgrupo_componente = vp_subgrupo_comp
           and basi_028.item_componente     = vp_item_comp
           and basi_028.cnpj_cliente9       = vp_cliente9
           and basi_028.cnpj_cliente4       = vp_cliente4
           and basi_028.cnpj_cliente2       = vp_cliente2
           and basi_028.tipo_aprovacao      = 1;   --APROVACAO POR COMPONENTE
         if v_total_028 = 0
         then
            begin
               INSERT INTO basi_028 (
                  nivel_componente,                grupo_componente,
                  subgrupo_componente,             item_componente,
                  cnpj_cliente9,                   cnpj_cliente4,
                  cnpj_cliente2,                   codigo_projeto,
                  sequencia_projeto,               situacao_componente,
                  tipo_aprovacao
               ) VALUES (
                  vp_nivel_comp,                    vp_grupo_comp,
                  vp_subgrupo_comp,                 vp_item_comp,
                  vp_cliente9,                      vp_cliente4,
                  vp_cliente2,                      vp_codigo_projeto,
                  vp_sequencia_projeto,             0,
                  1
               );
            exception when OTHERS then
               raise_application_error (-20000, 'Nao atualizou a tabela de Situacao do Item(basi_028)');
            end;
         end if;
      else
         -- Consiste se ha registro do componente na tabela de componente aprovado.
         select count(1)
         into v_total_028
         from basi_028
         where basi_028.nivel_componente    = vp_nivel_comp
           and basi_028.grupo_componente    = vp_grupo_comp
           and basi_028.subgrupo_componente = vp_subgrupo_comp
           and basi_028.item_componente     = vp_item_comp
           and basi_028.codigo_projeto      = vp_codigo_projeto
           and basi_028.sequencia_projeto   = vp_sequencia_projeto
           and basi_028.tipo_aprovacao      = 1;   --APROVACAO POR COMPONENTE
         if v_total_028 = 0
         then
            begin
               INSERT INTO basi_028 (
                  nivel_componente,                grupo_componente,
                  subgrupo_componente,             item_componente,
                  cnpj_cliente9,                   cnpj_cliente4,
                  cnpj_cliente2,                   codigo_projeto,
                  sequencia_projeto,               situacao_componente,
                  tipo_aprovacao
               ) VALUES (
                  vp_nivel_comp,                   vp_grupo_comp,
                  vp_subgrupo_comp,                vp_item_comp,
                  vp_cliente9,                     vp_cliente4,
                  vp_cliente2,                     vp_codigo_projeto,
                  vp_sequencia_projeto,            0,
                  1
               );
            exception when OTHERS then
               raise_application_error (-20000, 'Nao atualizou a tabela de Situacao do Item(basi_028)');
            end;
         end if;
      end if;
   end if;
END;

begin
   --
   --
   -- 1 = DESTINO COMPLETO -> TODOS O COMPONENTE E FORMADO NO DESTINO
   --
   --
   if p_tipo_destino = 1
   then
      if p_tabela_origem = 'basi_853'
      then
         inserir_basi_027_028 (p_nivel_comp,
                               p_grupo_comp,
                               P_subgru_comp,
                               p_item_comp,
                               p_codigo_projeto,
                               p_sequencia_subprojeto,
                               p_nivel_item,
                               p_grupo_item,
                               p_subgru_item,
                               p_item_item,
                               p_alternativa_produto,
                               p_cliente9,
                               p_cliente4,
                               p_cliente2);
      else
         for reg_basi_853 in (select basi_853.nivel_comp,         basi_853.grupo_comp,
                                     basi_853.subgrupo_comp,      basi_853.item_comp
                              from basi_853
                              where basi_853.codigo_projeto       = p_codigo_projeto
                                and basi_853.nivel_item           = p_nivel_item
                                and basi_853.grupo_item           = p_grupo_item
                                and basi_853.subgru_item          = P_subgru_item
                                and basi_853.item_item            = p_item_item
                                and basi_853.alternativa_produto  = p_alternativa_produto
                                and basi_853.sequencia_estrutura  = p_sequencia_estrutura
                                and basi_853.sequencia_subprojeto = p_sequencia_subprojeto
                              group by basi_853.nivel_comp,       basi_853.grupo_comp,
                                       basi_853.subgrupo_comp,    basi_853.item_comp)
         loop
            inserir_basi_027_028 (reg_basi_853.nivel_comp,
                                  reg_basi_853.grupo_comp,
                                  reg_basi_853.subgrupo_comp,
                                  reg_basi_853.item_comp,
                                  p_codigo_projeto,
                                  p_sequencia_subprojeto,
                                  p_nivel_item,
                                  p_grupo_item,
                                  p_subgru_item,
                                  p_item_item,
                                  p_alternativa_produto,
                                  p_cliente9,
                                  p_cliente4,
                                  p_cliente2);
         end loop;
      end if;
   end if;
   --
   --
   -- 2 = DESTINO COM SEQ. DE COR -> COMPONENTE E FORMADO NO DESTINO + A SEQ COR DO CADASTRO DE COMBINACAO
   --
   --
   if p_tipo_destino = 2
   then
      if p_tabela_origem = 'basi_853'
      then
         for reg_basi_021 in (select basi_021.subgru_comp subgrupo_comp
                              from basi_021
                              where basi_021.codigo_projeto       = p_codigo_projeto
                                and basi_021.nivel_item           = p_nivel_item
                                and basi_021.grupo_item           = p_grupo_item
                                and basi_021.item_item            = p_item_item
                                and basi_021.alternativa_produto  = p_alternativa_produto
                                and basi_021.sequencia_estrutura  = p_sequencia_estrutura
                                and basi_021.subgru_comp          <> ' '
                                and basi_021.subgru_comp          <> '000'
                                and basi_021.utiliza_componente   = 1
                              group by basi_021.subgru_comp)
         loop
            inserir_basi_027_028 (p_nivel_comp,
                                  p_grupo_comp,
                                  reg_basi_021.subgrupo_comp,
                                  p_item_comp,
                                  p_codigo_projeto,
                                  p_sequencia_subprojeto,
                                  p_nivel_item,
                                  p_grupo_item,
                                  p_subgru_item,
                                  p_item_item,
                                  p_alternativa_produto,
                                  p_cliente9,
                                  p_cliente4,
                                  p_cliente2);
         end loop;
      else
         for reg_basi_853 in (select basi_853.nivel_comp,         basi_853.grupo_comp,
                                     basi_853.item_comp
                              from basi_853
                              where basi_853.codigo_projeto       = p_codigo_projeto
                                and basi_853.nivel_item           = p_nivel_item
                                and basi_853.grupo_item           = p_grupo_item
                                and basi_853.item_item            = p_item_item
                                and basi_853.alternativa_produto  = p_alternativa_produto
                                and basi_853.sequencia_estrutura  = p_sequencia_estrutura
                                and basi_853.sequencia_subprojeto = p_sequencia_subprojeto
                              group by basi_853.nivel_comp,       basi_853.grupo_comp,
                                       basi_853.item_comp)
         loop
            if p_tabela_origem = 'basi_021'
            then
               inserir_basi_027_028 (reg_basi_853.nivel_comp,
                                     reg_basi_853.grupo_comp,
                                     p_subgru_comp,
                                     reg_basi_853.item_comp,
                                     p_codigo_projeto,
                                     p_sequencia_subprojeto,
                                     p_nivel_item,
                                     p_grupo_item,
                                     p_subgru_item,
                                     p_item_item,
                                     p_alternativa_produto,
                                     p_cliente9,
                                     p_cliente4,
                                     p_cliente2);
            else
               for reg_basi_021 in (select basi_021.subgru_comp subgrupo_comp
                                    from basi_021
                                    where basi_021.codigo_projeto       = p_codigo_projeto
                                      and basi_021.nivel_item           = p_nivel_item
                                      and basi_021.grupo_item           = p_grupo_item
                                      and basi_021.item_item            = p_item_item
                                      and basi_021.alternativa_produto  = p_alternativa_produto
                                      and basi_021.sequencia_estrutura  = p_sequencia_estrutura
                                      and basi_021.subgru_comp          <> ' '
                                      and basi_021.subgru_comp          <> '000'
                                      and basi_021.utiliza_componente   = 1
                                    group by basi_021.subgru_comp)
               loop
                  inserir_basi_027_028 (reg_basi_853.nivel_comp,
                                        reg_basi_853.grupo_comp,
                                        reg_basi_021.subgrupo_comp,
                                        reg_basi_853.item_comp,
                                        p_codigo_projeto,
                                        p_sequencia_subprojeto,
                                        p_nivel_item,
                                        p_grupo_item,
                                        p_subgru_item,
                                        p_item_item,
                                        p_alternativa_produto,
                                        p_cliente9,
                                        p_cliente4,
                                        p_cliente2);

               end loop;
            end if;
         end loop;
      end if;
   end if;
   --
   --
   -- 3 = DESTINO COM VARIACAO DE TAMANHO -> COMPONENTE E FORMADO NO DESTINO + A VARIACAO DE TAMANHO
   --
   --
   if p_tipo_destino = 3
   then
      if p_tabela_origem = 'basi_853'
      then
         for reg_basi_021 in (select basi_021.subgru_comp subgrupo_comp
                              from basi_021
                              where basi_021.codigo_projeto       = p_codigo_projeto
                                and basi_021.nivel_item           = p_nivel_item
                                and basi_021.grupo_item           = p_grupo_item
                                and basi_021.item_item            = p_item_item
                                and basi_021.alternativa_produto  = p_alternativa_produto
                                and basi_021.sequencia_estrutura  = p_sequencia_estrutura
                                and basi_021.subgru_comp          <> ' '
                                and basi_021.subgru_comp          <> '000'
                                and basi_021.utiliza_componente   = 1
                              group by basi_021.subgru_comp)
         loop
            for reg_basi_847 in (select basi_847.cor item_comp
                                 from basi_847
                                 where basi_847.codigo_projeto  = p_codigo_projeto
                                   and basi_847.cor             is not null
                                   and basi_847.seq_cor         = p_seq_cor
                                 group by basi_847.cor)
            loop
               inserir_basi_027_028 (p_nivel_comp,
                                     p_grupo_comp,
                                     reg_basi_021.subgrupo_comp,
                                     reg_basi_847.item_comp,
                                     p_codigo_projeto,
                                     p_sequencia_subprojeto,
                                     p_nivel_item,
                                     p_grupo_item,
                                     p_subgru_item,
                                     p_item_item,
                                     p_alternativa_produto,
                                     p_cliente9,
                                     p_cliente4,
                                     p_cliente2);
            end loop;
         end loop;
      else
         for reg_basi_853 in (select basi_853.nivel_comp,         basi_853.grupo_comp
                              from basi_853
                              where basi_853.codigo_projeto       = p_codigo_projeto
                                and basi_853.nivel_item           = p_nivel_item
                                and basi_853.grupo_item           = p_grupo_item
                                and basi_853.subgru_item          = P_subgru_item
                                and basi_853.item_item            = p_item_item
                                and basi_853.alternativa_produto  = p_alternativa_produto
                                and basi_853.sequencia_estrutura  = p_sequencia_estrutura
                                and basi_853.sequencia_subprojeto = p_sequencia_subprojeto
                              group by basi_853.nivel_comp,         basi_853.grupo_comp,
                                       basi_853.item_comp)
         loop
            for reg_basi_847 in (select basi_847.cor item_comp
                                 from basi_847
                                 where basi_847.codigo_projeto  = p_codigo_projeto
                                   and basi_847.cor             is not null
                                   and basi_847.seq_cor         = p_seq_cor
                                 group by basi_847.cor)
            loop
               if p_tabela_origem = 'basi_021'
               then
                  inserir_basi_027_028 (reg_basi_853.nivel_comp,
                                        reg_basi_853.grupo_comp,
                                        p_subgru_comp,
                                        reg_basi_847.item_comp,
                                        p_codigo_projeto,
                                        p_sequencia_subprojeto,
                                        p_nivel_item,
                                        p_grupo_item,
                                        p_subgru_item,
                                        p_item_item,
                                        p_alternativa_produto,
                                        p_cliente9,
                                        p_cliente4,
                                        p_cliente2);
               else
                  for reg_basi_021 in (select basi_021.subgru_comp subgrupo_comp
                                       from basi_021
                                       where basi_021.codigo_projeto       = p_codigo_projeto
                                         and basi_021.nivel_item           = p_nivel_item
                                         and basi_021.grupo_item           = p_grupo_item
                                         and basi_021.item_item            = p_item_item
                                         and basi_021.alternativa_produto  = p_alternativa_produto
                                         and basi_021.sequencia_estrutura  = p_sequencia_estrutura
                                         and basi_021.subgru_comp          <> ' '
                                         and basi_021.subgru_comp          <> '000'
                                         and basi_021.utiliza_componente   = 1
                                       group by basi_021.subgru_comp)
                  loop
                     inserir_basi_027_028 (reg_basi_853.nivel_comp,
                                           reg_basi_853.grupo_comp,
                                           reg_basi_021.subgrupo_comp,
                                           reg_basi_847.item_comp,
                                           p_codigo_projeto,
                                           p_sequencia_subprojeto,
                                           p_nivel_item,
                                           p_grupo_item,
                                           p_subgru_item,
                                           p_item_item,
                                           p_alternativa_produto,
                                           p_cliente9,
                                           p_cliente4,
                                           p_cliente2);
                  end loop;
               end if;
            end loop;
         end loop;
      end if;
   end if;

   --
   --
   -- 4 = DESTINO COM VARIACAO DE TAMANHO E SEQ COR -> COMPONENTE E FORMADO NO DESTINO + A VARIACAO DE TAMANHO + seq COR
   --
   --

   if p_tipo_destino = 4
   then
      if p_tabela_origem = 'basi_853'
      then
         for reg_basi_847 in (select basi_847.cor item_comp
                              from basi_847
                              where basi_847.codigo_projeto  = p_codigo_projeto
                                and basi_847.cor             is not null
                                and basi_847.seq_cor         = p_seq_cor
                              group by basi_847.cor)
         loop
            inserir_basi_027_028 (p_nivel_comp,
                                  p_grupo_comp,
                                  p_subgru_comp,
                                  reg_basi_847.item_comp,
                                  p_codigo_projeto,
                                  p_sequencia_subprojeto,
                                  p_nivel_item,
                                  p_grupo_item,
                                  p_subgru_item,
                                  p_item_item,
                                  p_alternativa_produto,
                                  p_cliente9,
                                  p_cliente4,
                                  p_cliente2);
         end loop;
      else
         for reg_basi_853 in (select basi_853.nivel_comp,         basi_853.grupo_comp,
                                     basi_853.subgrupo_comp
                              from basi_853
                              where basi_853.codigo_projeto       = p_codigo_projeto
                                and basi_853.nivel_item           = p_nivel_item
                                and basi_853.grupo_item           = p_grupo_item
                                and basi_853.subgru_item          = P_subgru_item
                                and basi_853.item_item            = p_item_item
                                and basi_853.alternativa_produto  = p_alternativa_produto
                                and basi_853.sequencia_estrutura  = p_sequencia_estrutura
                                and basi_853.sequencia_subprojeto = p_sequencia_subprojeto
                              group by basi_853.nivel_comp,       basi_853.grupo_comp,
                                       basi_853.subgrupo_comp)
         loop
            for reg_basi_847 in (select basi_847.cor item_comp
                                 from basi_847
                                 where basi_847.codigo_projeto  = p_codigo_projeto
                                   and basi_847.cor             is not null
                                   and basi_847.seq_cor         = p_seq_cor
                                 group by basi_847.cor)
            loop
               inserir_basi_027_028 (reg_basi_853.nivel_comp,
                                     reg_basi_853.grupo_comp,
                                     reg_basi_853.subgrupo_comp,
                                     reg_basi_847.item_comp,
                                     p_codigo_projeto,
                                     p_sequencia_subprojeto,
                                     p_nivel_item,
                                     p_grupo_item,
                                     p_subgru_item,
                                     p_item_item,
                                     p_alternativa_produto,
                                     p_cliente9,
                                     p_cliente4,
                                     p_cliente2);
            end loop;
         end loop;
      end if;
   end if;

end inter_pr_exp_destino;

 

/

exec inter_pr_recompile;

