create table basi_590
(grupo_embarque number(6),
 data_entrega date,
 nivel varchar2(1),
 grupo varchar2(5),
 subgrupo varchar2(3),
 item varchar2(6),
 seq_tamanho number(3),
 constraint basi_590_pk primary key (grupo_embarque, nivel, grupo, subgrupo, item),
 constraint fk_basi_590_basi_010 foreign key (nivel, grupo, subgrupo, item) references basi_010 (nivel_estrutura, grupo_estrutura, subgru_estrutura, item_estrutura));

comment on column basi_590.grupo_embarque is 'Numero do grupo de embarque'; 
comment on column basi_590.data_entrega is 'Data de entrega do embarque';
comment on column basi_590.nivel is 'Nivel do produto do embarque';
comment on column basi_590.grupo is 'Grupo do produto do embarque';
comment on column basi_590.subgrupo is 'Subgrupo do produto do embarque';
comment on column basi_590.item is 'Item do produto do embarque';
  
comment on table basi_590 is 'Grupos de embarque';

create INDEX BASI_590_ITEM on BASI_590(nivel, grupo, subgrupo, item);

exec inter_pr_recompile;

/
