ALTER TABLE SUPR_050 ADD (
    STATUS_EXPORTACAO NUMBER(1) default 0
);

COMMENT ON COLUMN SUPR_050.STATUS_EXPORTACAO 	IS 'Informa se os dados da supr_055 já foram exportados ao OBC para essa condicao de pagamento ou não. 0 para "a exportar", 1 para "exportado".';

EXEC INTER_PR_RECOMPILE;
