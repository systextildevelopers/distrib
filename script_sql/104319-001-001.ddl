alter table obrf_056 add PERC_COFINS_CREDITAR NUMBER(5,2);
alter table obrf_056 modify PERC_COFINS_CREDITAR default 0;

alter table obrf_056 add PERC_PIS_CREDITAR    NUMBER(5,2);
alter table obrf_056 modify PERC_PIS_CREDITAR default 0;

exec inter_pr_recompile;
