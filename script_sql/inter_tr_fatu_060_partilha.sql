create or replace trigger INTER_TR_FATU_060_PARTILHA
   before insert
   on fatu_060
   for each row
declare
   v_executa_trigger       number(1);
   v_perc_icms_uf_dest     pedi_080.perc_icms_uf_dest%type;
   v_perc_partilha         pedi_080.perc_icms_uf_dest%type;
   v_perc_fcp_uf_dest      pedi_080.perc_fcp_uf_dest%type;
   v_consumidor_final      pedi_080.consumidor_final%type;
   v_insc_est_cliente      pedi_010.insc_est_cliente%type;
   v_cfop_digito_ini       varchar2(1);
   v_cgc4                  fatu_050.cgc_4%type;
   v_presumido_reduzido    fatu_504.presumido_reduzido%type;
   v_perc_fcp              fatu_060.val_fcp_uf_dest%type;
   v_tipo_calculo_difal    basi_167.tipo_calculo_difal%type;
   v_base_calculo          number(18,5);

begin

   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if; 

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;


   if v_executa_trigger = 0 and inserting and :new.nf_integracao = 'N'
   then

       begin
         select pedi_010.insc_est_cliente, fatu_050.cgc_4 
         into v_insc_est_cliente,          v_cgc4 
         from pedi_010,fatu_050
         where pedi_010.cgc_9 = fatu_050.cgc_9
           and pedi_010.cgc_4 = fatu_050.cgc_4
           and pedi_010.cgc_2 = fatu_050.cgc_2
           and fatu_050.codigo_empresa  = :new.ch_it_nf_cd_empr
           and fatu_050.num_nota_fiscal = :new.ch_it_nf_num_nfis
           and fatu_050.serie_nota_fisc = :new.ch_it_nf_ser_nfis;
       exception
       when no_data_found then
          v_insc_est_cliente := '';
       end;
      -- le parametros da natureza.
      begin
         select pedi_080.perc_icms_uf_dest, pedi_080.perc_fcp_uf_dest,
                pedi_080.consumidor_final,  substr(pedi_080.cod_natureza,1,1)
         into   v_perc_icms_uf_dest,        v_perc_fcp_uf_dest,
                v_consumidor_final,         v_cfop_digito_ini
         from pedi_080
         where pedi_080.natur_operacao   = :new.natopeno_nat_oper
           and pedi_080.estado_natoper   = :new.natopeno_est_oper ;
         exception
         when no_data_found then
            v_perc_icms_uf_dest  := 0;
            v_perc_fcp_uf_dest   := 0;
            v_consumidor_final   := 'N';
            v_cfop_digito_ini    := '';
      end;
      
      --LE O PARAMETRO DE EMPRESA QUE DIZ SE O CALCULO DIFAL VAI SER COM BASE SIMPLES OU BASE DUPLA--
      begin
         select basi_167.tipo_calculo_difal 
         into v_tipo_calculo_difal
         from basi_167
         where basi_167.estado = :new.natopeno_est_oper
           and basi_167.codigo_pais = 1;
      exception
         when no_data_found then
            v_tipo_calculo_difal := 0;
      end;


      --LE O PARAMETRO DE EMPRESA QUE DIZ SE A EMPRESA USA O BENEFICIO DO PRESUMIDO REDUZIDO--
        begin
           select fatu_504.presumido_reduzido 
             into v_presumido_reduzido
             from fatu_504
             where fatu_504.codigo_empresa = :new.ch_it_nf_cd_empr;
             exception
             when no_data_found then
                v_presumido_reduzido := 0;
        end;


      -- SE FOR OPERA￿￿O FINAL OU CONSUMIDOR FINAL FAZ PARTILHA E FORA DO ESTADO
      if (v_consumidor_final = 'S' or (v_insc_est_cliente = 'ISENTO' and v_cgc4 = 0)) and v_cfop_digito_ini = '6'
      then
         /*
         perc_fcp_uf_dest is 'Percentual do ICMS relativo ao Fundo de Combate ￿ Pobreza (FCP) na UF de destino';
         perc_icms_uf_dest is 'Al￿quota interna da UF do destinat￿rio';
         perc_icms_partilha is 'Percentual de partilha para a UF do destinat￿rio: 40% em 2016;60% em 2017;80% em 2018;100% a partir de 2019';
         val_fcp_uf_dest is 'Valor do ICMS relativo ao Fundo de Combate ￿ Pobreza (FCP) da UF de destino.';
         val_icms_uf_dest is 'Valor do ICMS de partilha para a UF do destinat￿rio';
         val_icms_uf_remet is 'Valor do ICMS de partilha para a UF do remetente.';

         */
         select decode(to_number(to_char(:new.data_emissao,'YYYY')),2015,0,2016,40,2017,60,2018,80,100) into v_perc_partilha from dual;

         :new.perc_fcp_uf_dest          := v_perc_fcp_uf_dest;
         :new.perc_icms_uf_dest         := v_perc_icms_uf_dest;
         :new.perc_icms_partilha        := v_perc_partilha;
         :new.val_fcp_uf_dest           := round(((:new.base_icms + :new.rateio_despesa) * v_perc_fcp_uf_dest) / 100.00000,2);

         if v_perc_icms_uf_dest-:new.perc_icms <= 0
         then
            :new.val_icms_uf_dest       := 0;
         else
            :new.val_icms_uf_dest :=  round(round((round(:new.base_icms + :new.rateio_despesa,2) *
                 (v_perc_icms_uf_dest - :new.perc_icms)) /100.00000,2)*(v_perc_partilha/100.00000),2);
         end if;
         
         if v_tipo_calculo_difal = 0
         then
            if (v_perc_icms_uf_dest-:new.perc_icms <= 0) or v_perc_partilha = 100.00 or v_presumido_reduzido = 1
            then
               :new.val_icms_uf_remet      := 0;
            else
               :new.val_icms_uf_remet      := round((round(:new.base_icms + :new.rateio_despesa,2) * (v_perc_icms_uf_dest - :new.perc_icms)) /100,2)  -
                                              round(round((round(:new.base_icms + :new.rateio_despesa,2) *
                                              (v_perc_icms_uf_dest - :new.perc_icms)) /100,2)*(v_perc_partilha/100.00000),2);
            end if;
         else
            v_base_calculo := round((round(:new.valor_faturado - :new.valor_icms,2)) / ((v_perc_icms_uf_dest / 100) - 1 ),2);
            
            :new.val_icms_uf_dest := round(round(v_base_calculo * (v_perc_icms_uf_dest * -1) / 100 ,2) - :new.valor_icms,2);
         end if;
      else
         
         if :new.val_icms_uf_dest = 0.00
         then 
            :new.perc_fcp_uf_dest       := 0;
            :new.perc_icms_uf_dest      := 0;
            :new.perc_icms_partilha     := 0;
            :new.val_fcp_uf_dest        := 0;
            :new.val_icms_uf_dest       := 0;
            :new.val_icms_uf_remet      := 0;
         end if;
         
         begin
            select nvl(basi_242.perc_fcp,0) into v_perc_fcp  from basi_242 
            where basi_242.ncm = :new.classific_fiscal 
            and   basi_242.uf  = :new.natopeno_est_oper;
            exception
            when no_data_found then
                v_perc_fcp := 0;
         end;
         
         /* CALCULA FCP POR NCM e UF*/
         if :new.val_fcp_uf_dest = 0 and v_perc_fcp > 0
         then
            :new.val_fcp_uf_dest := :new.base_icms * (v_perc_fcp / 100);
            :new.perc_fcp_uf_dest := v_perc_fcp;
         end if;
         
      end if;
   end if;


end INTER_TR_FATU_060_PARTILHA;

/
exec inter_pr_recompile;
