create table RCNB_859
(
  CODIGO_EMPRESA          NUMBER(3) default 0 not null,
  MES                     NUMBER(2) default 0 not null,
  ANO                     NUMBER(4) default 0 not null,
  NIVEL_ESTRUTURA         VARCHAR2(1) default ' ' not null,
  GRUPO_ESTRUTURA         VARCHAR2(5) default ' ' not null,
  SUBGRU_ESTRUTURA        VARCHAR2(3) default ' ' not null,
  ITEM_ESTRUTURA          VARCHAR2(6) default ' ' not null,
  CODIGO_ESTAGIO          NUMBER(2) default 0 not null,
  ESTAGIO_DEPENDE         NUMBER(2) default 0,
  ESTAGIO_AGRUPADOR       NUMBER(2) default 0,
  ESTAGIO_AGRUPADOR_SIMUL NUMBER(2) default 0,
  SEQUENCIA_ESTAGIO       NUMBER(9) default 0 not null,
  SEQ_OPERACAO            NUMBER(9) default 0,
  CUSTO_ESTAGIO           NUMBER(19,6) default 0.00,
  CUSTO_ACUMULADO         NUMBER(19,6) default 0.00,
  CUSTO_ESTAGIO_ANTERIOR  NUMBER(19,6) default 0.00,
  CUSTO_FINAL_ESTAGIO     NUMBER(19,6) default 0.00,
  ALTERNATIVA_ESTRUTURA   NUMBER(2) default 1 not null,
  ROTEIRO_ESTRUTURA       NUMBER(9) default 0 not null,
  DATA_EXECUCAO           DATE,
  IND_ESTAGIO_SIMUL       VARCHAR2(1) default ' ' not null
);

alter table RCNB_859
  add constraint CONSTRAINT_NAME primary key (CODIGO_EMPRESA, MES, ANO, NIVEL_ESTRUTURA, GRUPO_ESTRUTURA, SUBGRU_ESTRUTURA, ITEM_ESTRUTURA, CODIGO_ESTAGIO);
