create table pedi_010_fci (
   percentual_fci number(6,2) default 0.00 not null, 
   percentual_natureza number(6,2) default 0.00  not null,
   percentual_aplicado number(6,2) default 0.00  not null, 
   pedido number(9) default 0  not null, 
   nivel varchar2(1) default '',
   grupo varchar2(5) default '',
   subgrupo varchar2(3) default '',
   item varchar2(6) default '',
   codigo_empresa number(3) default 0 not null,
   nota_fiscal number(9) default 0 not null, 
   serie_nota varchar2(3) default '',
   data_ocorr date not null
);
