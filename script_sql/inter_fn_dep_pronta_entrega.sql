
  CREATE OR REPLACE FUNCTION "INTER_FN_DEP_PRONTA_ENTREGA" 
   (p_codigo_deposito in number)
   return number is

   v_pronta_entrega number;
  
begin
   begin
      select pronta_entrega into v_pronta_entrega from basi_205
      where codigo_deposito = p_codigo_deposito;
         
      exception
          when others then
             v_pronta_entrega := 0;
   end;
   return(v_pronta_entrega);
end inter_fn_dep_pronta_entrega;

 

/

exec inter_pr_recompile;

