create or replace package body ST_PCK_COND_PGTO is

FUNCTION get_dados_cond_pgt_cliente(p_cond_pgt_cliente number, p_msg_erro in out varchar2) return t_dados_pedi_070
AS 
    v_dados_pedi_070 t_dados_pedi_070;
BEGIN
    begin
        SELECT *  
        BULK COLLECT 
        INTO v_dados_pedi_070
        FROM pedi_070
        WHERE cond_pgt_cliente = p_cond_pgt_cliente;
    exception when others then
        p_msg_erro := 'Não foi possível buscar dados para a condição de pagamento!. p_cond_pgt_cliente:' || p_cond_pgt_cliente;
        return null;
    end;

    return v_dados_pedi_070;

END;

end ST_PCK_COND_PGTO;
