alter table pedi_100 add (pedido_origem      number(9),
                          data_hora_copia    date);

comment on column pedi_100.pedido_origem    IS 'Número do pedido origem da cópia (pedido que originou este via cópia de pedidos pelo pedi_f610)';
comment on column pedi_100.data_hora_copia  IS 'Data/hora que este pedido foi criado (pela cópia do pedido pedi_f610)';

exec inter_pr_recompile;
