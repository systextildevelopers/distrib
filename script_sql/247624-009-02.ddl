declare

    v_count_035 number;
    v_count_033 number;
    v_count_550 number;

begin

    for x in(
        select
            usuario
            ,empresa
            ,ativo_inativo
        from hdoc_030
        where ativo_inativo = 1
    )loop

        select
            count(*)
        into v_count_035
        from hdoc_035
        where codigo_programa = 'estq_fa25';

        if v_count_035 = 0 then

            insert into hdoc_035 (codigo_programa, descricao, programa_menu, item_menu_def, item_apex)

            values ('estq_fa25', 'Integração de Rolos Industrializados', 0, 1, 1);

        end if;

        select
            count(*)
        into v_count_033
        from hdoc_033
        where usu_prg_cdusu      = x.usuario
            and usu_prg_empr_usu = x.empresa
            and programa         = 'estq_fa25';

        if v_count_033 = 0 then 
            insert into hdoc_033(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
            values(x.usuario, x.empresa, 'estq_fa25', 'menu_ap01', 1, 0, 'S', 'S', 'S', 'S');
        end if;

        select
            count(*)
        into v_count_550
        from oper_550
        where usuario            = x.usuario
            and empresa          = x.empresa
            and nome_programa    = 'estq_fa25'
            and nome_subprograma = 'deposito_entrada'
            and nome_field       = 'depositoEntrada';

        if v_count_550 = 0 then 
            insert into oper_550(usuario, empresa, nome_programa ,nome_field ,nome_subprograma, requerido, acessivel)
            values (x.usuario, x.empresa,'estq_fa25','depositoEntrada','deposito_entrada',0,0);
        end if;

        select
            count(*)
        into v_count_550
        from oper_550
        where usuario            = x.usuario
            and empresa          = x.empresa
            and nome_programa    = 'estq_fa25'
            and nome_subprograma = 'transacao_entrada'
            and nome_field       = 'transacaoEntrada';

        if v_count_550 = 0 then 
            insert into oper_550(usuario, empresa, nome_programa ,nome_field ,nome_subprograma, requerido, acessivel)
            values (x.usuario, x.empresa,'estq_fa25','transacaoEntrada','transacao_entrada',0,0);
        end if;

        select
            count(*)
        into v_count_550
        from oper_550
        where usuario            = x.usuario
            and empresa          = x.empresa
            and nome_programa    = 'estq_fa25'
            and nome_subprograma = 'realiza_conferencia'
            and nome_field       = 'realizaConferencia';

        if v_count_550 = 0 then 
            insert into oper_550(usuario, empresa, nome_programa ,nome_field ,nome_subprograma, requerido, acessivel)
            values (x.usuario, x.empresa,'estq_fa25','realizaConferencia','realiza_conferencia',0,0);
        end if;

        select
            count(*)
        into v_count_550
        from oper_550
        where usuario            = x.usuario
            and empresa          = x.empresa
            and nome_programa    = 'estq_fa25'
            and nome_subprograma = 'preview'
            and nome_field       = 'preview';

        if v_count_550 = 0 then 
            insert into oper_550(usuario, empresa, nome_programa ,nome_field ,nome_subprograma, requerido, acessivel)
            values (x.usuario, x.empresa,'estq_fa25','preview','preview',0,0);
        end if;

        select
            count(*)
        into v_count_550
        from oper_550
        where usuario            = x.usuario
            and empresa          = x.empresa
            and nome_programa    = 'estq_fa25'
            and nome_subprograma = 'salvar_parametros'
            and nome_field       = 'salvarParametros' ;

        if v_count_550 = 0 then 
            insert into oper_550(usuario, empresa, nome_programa ,nome_field ,nome_subprograma, requerido, acessivel)
            values (x.usuario, x.empresa,'estq_fa25','salvarParametros','salvar_parametros',0,0);
        end if;

        select
            count(*)
        into v_count_550
        from oper_550
        where usuario            = x.usuario
            and empresa          = x.empresa
            and nome_programa    = 'estq_fa25'
            and nome_subprograma = 'zerar_parametros'
            and nome_field       = 'zerarParametros'  ;

        if v_count_550 = 0 then 
            insert into oper_550(usuario, empresa, nome_programa ,nome_field ,nome_subprograma, requerido, acessivel)
            values (x.usuario, x.empresa,'estq_fa25','zerarParametros','zerar_parametros',0,0);
        end if;

    end loop;

exception
    when others then
        null;
    
end;

-- execute
