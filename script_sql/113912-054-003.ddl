alter table rcnb_750
  drop constraint pk_rcnb_750;

drop index pk_rcnb_750;

alter table rcnb_750 drop column id_execucao;

alter table rcnb_750 add id_execucao varchar2(20) default 0;

alter table rcnb_750
  add constraint pk_rcnb_750 primary key (codigo_empresa, nivel_produto, grupo_produto, subgru_produto, item_produto, nome_usuario, colecao, alternativa, tipo_analise, faixa_etaria_custos,id_execucao);

exec inter_pr_recompile;
