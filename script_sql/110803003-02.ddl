INSERT INTO hdoc_035
( codigo_programa, programa_menu, 
 item_menu_def,   descricao)
VALUES
( 'inte_l087',     1,
 1,        'Parâmetros de Importação de Pedido do Paraguai');
 
INSERT INTO hdoc_033
( usu_prg_cdusu, usu_prg_empr_usu, 
 programa,      nome_menu, 
 item_menu,     ordem_menu, 
 incluir,       modificar, 
 excluir,       procurar)
VALUES
( 'INTERSYS',    1, 
 'inte_l087',   'inte_menu', 
 1,             1, 
 'S',           'S', 
 'S',           'S');
 
INSERT INTO hdoc_033
( usu_prg_cdusu,  usu_prg_empr_usu, 
 programa,       nome_menu, 
 item_menu,      ordem_menu, 
 incluir,        modificar, 
 excluir,        procurar)
VALUES
( 'TREINAMENTO',  1, 
 'inte_l087',    'inte_menu', 
 1,              1, 
 'S',            'S', 
 'S',            'S');

UPDATE hdoc_036
   SET hdoc_036.descricao       = 'Parâmetros de Importação de Pedido do Paraguai'
 WHERE hdoc_036.codigo_programa = 'inte_l087'
   AND hdoc_036.locale          = 'es_ES';
   
commit;

exec inter_pr_recompile;
