  CREATE OR REPLACE FUNCTION "INTER_FN_VALIDA_ROTEIRO_OB" (p_ob in number)
return varchar2
is
   v_nivel_aux varchar2(1);
   v_grupo_aux varchar2(5);
   v_subgrupo_aux varchar2(3);
   v_item_aux varchar2(6);


   v_nr_solicitacao number(9);
   v_sequencia_rot_valid number(9);
   v_count number(9);

   v_existe_oper number(9);
   v_existe_sequencia number(9);
   v_existe_estagio number(9);

   v_mensagem varchar2(4000);

   v_existe_nivel varchar2(1);
   v_existe_grupo varchar2(5);
   v_existe_subgrupo varchar2(3);
   v_existe_item varchar2(6);
   v_existe_alternativa number(2);
   v_existe_roteiro number(2);
   v_existe_codigo_estagio number(5);

   v_descricao_estagio0 varchar2(120);
   v_descricao_estagio1 varchar2(120);
begin
   v_mensagem := '';

   v_nr_solicitacao := 0;

   begin
      select dbms_utility.get_hash_value(to_char(dbms_utility.get_time),100,99999)
      into v_nr_solicitacao
      from dual;
   exception
   when OTHERS then
      v_nr_solicitacao := 0;
   end;

   v_sequencia_rot_valid := 0;

   FOR pb20 IN (select pcpb_020.pano_sbg_nivel99 nivel,       pcpb_020.pano_sbg_grupo grupo,
                       pcpb_020.pano_sbg_subgrupo subgrupo,   pcpb_020.pano_sbg_item item,
                       pcpb_020.alternativa_item alternativa, pcpb_020.roteiro_opcional roteiro
                from pcpb_010,
                     pcpb_020
                where pcpb_010.ordem_producao = pcpb_020.ordem_producao
                and   pcpb_010.ordem_producao = p_ob
                order by pcpb_020.sequencia)
   LOOP
      v_nivel_aux := pb20.nivel;
      v_grupo_aux := pb20.grupo;
      v_subgrupo_aux := pb20.subgrupo;
      v_item_aux := pb20.item;

      v_count := 0;

      begin
         select count(1)
         into   v_count
         from mqop_050
         where mqop_050.nivel_estrutura  = v_nivel_aux
           and mqop_050.grupo_estrutura  = v_grupo_aux
           and mqop_050.subgru_estrutura = v_subgrupo_aux
           and mqop_050.item_estrutura   = v_item_aux
           and mqop_050.numero_alternati = pb20.alternativa
           and mqop_050.numero_roteiro   = pb20.roteiro;
         if v_count = 0
         then
            v_item_aux := '000000';

            begin
               select count(1)
               into   v_count
               from mqop_050
               where mqop_050.nivel_estrutura  = v_nivel_aux
                 and mqop_050.grupo_estrutura  = v_grupo_aux
                 and mqop_050.subgru_estrutura = v_subgrupo_aux
                 and mqop_050.item_estrutura   = v_item_aux
                 and mqop_050.numero_alternati = pb20.alternativa
                 and mqop_050.numero_roteiro   = pb20.roteiro;
               if v_count = 0
               then
                  v_subgrupo_aux := '000';
                  v_item_aux     := pb20.item;

                  begin
                     select count(1)
                     into   v_count
                     from mqop_050
                     where mqop_050.nivel_estrutura  = v_nivel_aux
                       and mqop_050.grupo_estrutura  = v_grupo_aux
                       and mqop_050.subgru_estrutura = v_subgrupo_aux
                       and mqop_050.item_estrutura   = v_item_aux
                       and mqop_050.numero_alternati = pb20.alternativa
                       and mqop_050.numero_roteiro   = pb20.roteiro;
                     if  v_count = 0
                     then
                        v_item_aux := '000000';
                     end if;
                  end;
               end if;
            end;
         end if;
      exception
      when OTHERS then
         v_existe_roteiro := 0;
      end;

      begin
         select count(1)
         into v_existe_oper
         from oper_tmp
         where oper_tmp.nr_solicitacao = v_nr_solicitacao
           and oper_tmp.nome_relatorio = 'rot_valid';
      exception
      when OTHERS then
         v_existe_oper := 0;
      end;

      FOR mp_50 IN (select mqop_050.seq_operacao seq_operacao, mqop_050.codigo_estagio codigo_estagio
                    from mqop_050
                    where mqop_050.nivel_estrutura  = v_nivel_aux
                      and mqop_050.grupo_estrutura  = v_grupo_aux
                      and mqop_050.subgru_estrutura = v_subgrupo_aux
                      and mqop_050.item_estrutura   = v_item_aux
                      and mqop_050.numero_alternati = pb20.alternativa
                      and mqop_050.numero_roteiro   = pb20.roteiro
                    order by mqop_050.seq_operacao)
      LOOP
         if v_existe_oper = 0
         then
            v_sequencia_rot_valid := v_sequencia_rot_valid + 1;

            begin
               INSERT INTO oper_tmp (
                  nr_solicitacao,            nome_relatorio,
                  sequencia,                 data_criacao,

                  int_01,                    int_02,

                  str_01,                    str_02,
                  str_03,                    str_04,
                  int_03,                    int_04
               ) VALUES (
                  v_nr_solicitacao,          'rot_valid',
                  v_sequencia_rot_valid,     sysdate,

                  mp_50.seq_operacao,        mp_50.codigo_estagio,

                  pb20.nivel,                pb20.grupo,
                  pb20.subgrupo,             pb20.item,
                  pb20.alternativa,          pb20.roteiro
               );
            exception
            when OTHERS then
               return ('Erro ao inserir oper_tmp');
            end;
         else
            begin
               select count(1) into v_existe_sequencia
               from oper_tmp
               where oper_tmp.nr_solicitacao = v_nr_solicitacao
                 and oper_tmp.nome_relatorio = 'rot_valid'
                 and oper_tmp.int_01         = mp_50.seq_operacao;
            exception
            when OTHERS then
               v_existe_sequencia := 0;
            end;


            if v_existe_sequencia <> 0
            then
               begin
                  select count(1)
                  into   v_existe_estagio
                  from oper_tmp
                  where oper_tmp.nr_solicitacao = v_nr_solicitacao
                    and oper_tmp.nome_relatorio = 'rot_valid'
                    and oper_tmp.int_01         = mp_50.seq_operacao
                    and oper_tmp.int_02         = mp_50.codigo_estagio;
               exception
               when OTHERS then
                  v_existe_estagio := 0;
               end;

               if v_existe_estagio = 0
               then
                  begin
                     select str_01,
                            str_02,
                            str_03,
                            str_04,
                            int_03,
                            int_04,
                            int_02
                     into   v_existe_nivel,
                            v_existe_grupo,
                            v_existe_subgrupo,
                            v_existe_item,
                            v_existe_alternativa,
                            v_existe_roteiro,
                            v_existe_codigo_estagio
                     from oper_tmp
                     where oper_tmp.nr_solicitacao = v_nr_solicitacao
                       and oper_tmp.nome_relatorio = 'rot_valid'
                       and oper_tmp.int_01         = mp_50.seq_operacao;
                  end;

                  begin
                     select mqop_005.descricao
                     into v_descricao_estagio0
                     from mqop_005
                     where mqop_005.codigo_estagio = v_existe_codigo_estagio;
                  exception
                  when OTHERS then
                     v_descricao_estagio0 := '';
                  end;

                  begin
                     select mqop_005.descricao
                     into v_descricao_estagio1
                     from mqop_005
                     where mqop_005.codigo_estagio = mp_50.codigo_estagio;
                  exception
                  when OTHERS then
                     v_descricao_estagio1 := '';
                  end;

                  v_mensagem := '';/* 'ATENCAO! Os tecidos da ordem devem conter o mesmo codigo de estagio na mesma sequencia de operacao. Revise o cadastro dos roteiros de fabricacao dos tecidos a seguir para poder continuar.'
                                || chr(10) || chr(10) ||
                                'ATENCAO! Revisar roteiro do tecido ' ||
                                v_existe_nivel || '.' || v_existe_grupo || '.' || v_existe_subgrupo || '.' || v_existe_item ||
                                ', Alternativa: ' || to_char(v_existe_alternativa,'00') ||
                                ', Roteiro: ' || to_char(v_existe_roteiro,'00') ||
                                ', Seq. Operacao: ' || to_char(mp_50.seq_operacao,'00') ||
                                ' e Estagio: ' || to_char(v_existe_codigo_estagio,'99900') || ' - ' || v_descricao_estagio0 ||
                                chr(10) ||
                                'ATENCAO! Revisar roteiro do tecido ' ||
                                pb20.nivel || '.' || pb20.grupo || '.' || pb20.subgrupo || '.' || pb20.item ||
                                ', Alternativa: ' || to_char(pb20.alternativa,'00') ||
                                ', Roteiro: ' || to_char(pb20.roteiro,'00') ||
                                ', Seq. Operacao: ' || to_char(mp_50.seq_operacao,'00') ||
                                ' e Estagio: ' || to_char(mp_50.codigo_estagio,'99900') || ' - ' || v_descricao_estagio1 ||
                                chr(10) || chr(10) ||
                                'ATENCAO! Apos revisar e corrigir os roteiros sera possivel programar os tecidos informados nas mensagens anteriores.';*/

                  return (v_mensagem);
               end if;
            end if;
         end if;
      END LOOP;
   END LOOP;

   return (v_mensagem);
end;

 

/

exec inter_pr_recompile;

