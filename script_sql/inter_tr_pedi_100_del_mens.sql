
  CREATE OR REPLACE TRIGGER "INTER_TR_PEDI_100_DEL_MENS" 
AFTER  DELETE

on pedi_100

for each row

begin

   delete from pedi_101
   where pedi_101.num_pedido    = :old.pedido_venda;

end INTER_TR_PEDI_100_DEL_MENS;

-- ALTER TRIGGER "INTER_TR_PEDI_100_DEL_MENS" ENABLE
 

/

exec inter_pr_recompile;

