alter table pedi_115 
add (
	tipo_intermediador number(1) default 0,
	cgc9_intermediador number(9) default 0,
	cgc4_intermediador number(4) default 0,
	cgc2_intermediador number(2) default 0
);
