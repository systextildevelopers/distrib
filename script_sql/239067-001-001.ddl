CREATE TABLE BLOCOK_270 (
    COD_EMPRESA            NUMBER(3,0),
	ANO_PERIODO_APUR       NUMBER(4,0),
	MES_PERIODO_APUR       NUMBER(2,0),
	COD_ITEM               VARCHAR2(60),
	REGISTRO               VARCHAR2(4BYTE) DEFAULT 'K270',
    DT_INI_AP              DATE,
    DT_FIN_AP              DATE,
    COD_OP_OS              VARCHAR2(30),
    COD_ITEM_NIVEL         VARCHAR2(1)   DEFAULT ' ' NOT NULL,
	COD_ITEM_GRUPO         VARCHAR2(5)   DEFAULT ' ' NOT NULL,
	COD_ITEM_SUBGRUPO      VARCHAR2(3)   DEFAULT ' ' NOT NULL,
	COD_ITEM_ITEM          VARCHAR2(6)   DEFAULT ' ' NOT NULL,
    QTDE_COR_POS           NUMBER(13,5),
    QTDE_COR_NEG           NUMBER(13,5),
    ORIGEM                 NUMBER(1)
);

COMMENT ON COLUMN BLOCOK_270.COD_OP_OS             IS 'Código documento - Número ordem';
COMMENT ON COLUMN BLOCOK_270.DT_INI_AP             IS 'Data do inicio da Ordem';
COMMENT ON COLUMN BLOCOK_270.DT_FIN_AP             IS 'Data do final da Ordem';
COMMENT ON COLUMN BLOCOK_270.QTDE_COR_POS          IS 'Quantidade Corrigida positiva';
COMMENT ON COLUMN BLOCOK_270.QTDE_COR_NEG          IS 'Quantidade Corrigida negativa';
COMMENT ON COLUMN BLOCOK_270.ORIGEM                IS 'Origem: 1- K230/K235, 2- K250/K255, 3- K210/K215, 4- K260/K265, 5- K220';

/
exec inter_pr_recompile;
