
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPC_040_4" 
     before insert
         or delete
         or update of qtde_pecas_prog
     on pcpc_040
     for each row
declare
   v_executa_trigger number;
   v_nro_reg         number;
begin
   -- INICIO - Logica implementada para controle de "gatilho" das triggers do banco
   -- para atender a implementacao executada para limpeza da base de dados do cliente
   -- e replicacao dos dados para base historico. (SS.38405)
   if inserting
   then
      if :new.executa_trigger = 1
      then
         v_executa_trigger := 1;
      else
         if :new.executa_trigger = 3
         then
             v_executa_trigger := 3;
         else
            v_executa_trigger := 0;
         end if;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1
      or :new.executa_trigger = 1
      then
         v_executa_trigger := 1;
      else
         if :new.executa_trigger = 3
         then
            v_executa_trigger := 3;
         else
             v_executa_trigger := 0;
         end if;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then
         v_executa_trigger := 1;
      else
         if :old.executa_trigger = 3
         then
            v_executa_trigger := 3;
         else
            v_executa_trigger := 0;
         end if;
      end if;
   end if;
   -- FINAL - Logica implementada para controle de "gatilho" das triggers do banco. (SS.38405)

   if v_executa_trigger <> 1
   then
      if inserting
      or (updating
      and :new.qtde_pecas_prog <> :old.qtde_pecas_prog)
      then
         begin
            select nvl(count(1),0)
            into   v_nro_reg
            from pcpc_049
            where pcpc_049.ordem_producao   = :new.ordem_producao
              and pcpc_049.subgrupo_produto = :new.proconf_subgrupo
              and pcpc_049.item_produto     = :new.proconf_item;
         exception when others then
            v_nro_reg := 0;
         end;

         if v_nro_reg <> 0
         then
            begin
               update pcpc_049
               set    pcpc_049.dta_alteracao = sysdate
               where pcpc_049.ordem_producao   = :new.ordem_producao
                 and pcpc_049.subgrupo_produto = :new.proconf_subgrupo
                 and pcpc_049.item_produto     = :new.proconf_item;
            end;
         end if;
      end if;

      if deleting
      then
         begin
            select nvl(count(1),0)
            into   v_nro_reg
            from pcpc_049
            where pcpc_049.ordem_producao   = :old.ordem_producao
              and pcpc_049.subgrupo_produto = :old.proconf_subgrupo
              and pcpc_049.item_produto     = :old.proconf_item;
         exception when others then
            v_nro_reg := 0;
         end;

         if v_nro_reg <> 0
         then
            begin
               update pcpc_049
               set    pcpc_049.dta_alteracao   = sysdate
               where pcpc_049.ordem_producao   = :old.ordem_producao
                 and pcpc_049.subgrupo_produto = :old.proconf_subgrupo
                 and pcpc_049.item_produto     = :old.proconf_item;
            end;
         end if;
      end if;
   end if;

end inter_tr_pcpc_040_4;

-- ALTER TRIGGER "INTER_TR_PCPC_040_4" ENABLE
 

/

exec inter_pr_recompile;

