create table estq_711 (
	documento			number(9)			not null,
	seq_prod_consumido	number(9)			not null,
	nivel_consumido		varchar2(1),
	grupo_consumido		varchar2(5),
	subgrupo_consumido	varchar2(3),
	item_consumido		varchar2(6),
	quantidade			number(12,3),
	deposito_saida		number(3),
	transacao_saida		number(3)	
);

comment on table estq_711 						is 'Tabela dos Produtos Consumidos na Montagem de Produtos (Saida)';

comment on column estq_711.documento 			is 'Numero do documento da montagem';
comment on column estq_711.seq_prod_consumido	is 'Sequencia do produto consumido (saida)';

comment on column estq_711.nivel_consumido 		is 'Nivel do produto consumido (saida)';
comment on column estq_711.grupo_consumido 		is 'Grupo do produto consumido (saida)';
comment on column estq_711.subgrupo_consumido 	is 'Subgrupo do produto consumido (saida)';
comment on column estq_711.item_consumido 		is 'Item do produto consumido (saida)';
comment on column estq_711.quantidade 			is 'Quantidade consumida (saida) do produto consumido';

comment on column estq_711.deposito_saida		is 'Deposito de saida do produto consumido';
comment on column estq_711.transacao_saida 		is 'Transacao de saida do produto consumido';

alter table estq_711 add constraint estq_711_pk primary key (documento, seq_prod_consumido);

