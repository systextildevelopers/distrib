alter table proj_015 modify QTDE_MAQ_NECESSIDADE NUMBER(22,4);

alter table proj_015 modify COMPOSICAO_CALC VARCHAR2(4000);

alter table proj_015 add MINUTOS_DIA_PORMAQ_DISPONIVEL NUMBER(20,2);
