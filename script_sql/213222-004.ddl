CREATE SEQUENCE SEQ_COD_COMP_MQOP_016 INCREMENT BY 1 START WITH 1 MAXVALUE 9999 MINVALUE 1 CACHE 2;


CREATE OR REPLACE TRIGGER inter_tr_mqop_016_cod_comp
BEFORE INSERT ON mqop_016 FOR EACH ROW
DECLARE
    next_value number;
BEGIN
    if :new.cod_comp is null then
        select SEQ_COD_COMP_MQOP_016.nextval
        into next_value from dual;
        :new.cod_comp := next_value;
    end if;
END inter_tr_mqop_016_cod_comp;
