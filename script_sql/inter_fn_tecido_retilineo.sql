
  CREATE OR REPLACE FUNCTION "INTER_FN_TECIDO_RETILINEO" (
f_nivel_tecido_acabado in varchar2,
f_grupo_tecido_acabado in varchar2)

return number
is
   tipo_tecido      number;
   tipo_produto_030 number;
begin
   /*
      BUSCAR O TIPO DE PRODUTO DA BASI_030, PARA SABER SE E RETILINEO OU NAO
   */
   tipo_tecido := 0;

   begin
      select basi_030.tipo_produto
      into   tipo_produto_030
      from basi_030
      where basi_030.nivel_estrutura = f_nivel_tecido_acabado
       and basi_030.referencia       = f_grupo_tecido_acabado;
   exception when others then
      tipo_produto_030 := 0;
   end;

   if tipo_produto_030 <> 0
   then
      tipo_tecido := 0;

      if tipo_produto_030 = 1
      or tipo_produto_030 = 2
      or tipo_produto_030 = 5
      then
         tipo_tecido := 1;  /* NORMAL */
      else
         tipo_tecido := 2;  /* RETILINEO */
      end if;
   end if;

   return(tipo_tecido);

end inter_fn_tecido_retilineo;

 

/

exec inter_pr_recompile;

