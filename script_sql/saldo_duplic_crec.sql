
  CREATE OR REPLACE FUNCTION "SALDO_DUPLIC_CREC" 
        (emp_duplic in fatu_070.codigo_empresa%type,
         num_duplic in fatu_070.num_duplicata%type,
         seq_duplic in fatu_070.seq_duplicatas%type,
         tip_duplic in fatu_070.tipo_titulo%type,
         cg9_duplic in fatu_070.cli_dup_cgc_cli9%type,
         cg4_duplic in fatu_070.cli_dup_cgc_cli4%type,
         cg2_duplic in fatu_070.cli_dup_cgc_cli2%type)
RETURN number
IS
   v_saldo_valdup     number(15,7);
   v_saldo_encontrado number(15,7);
   v_saldo_pagto      number(15,7);
   v_saldo_estorno    number(15,7);
   v_saldo_situacao   number(2);
begin
   select nvl(saldo_duplic_crec_valdup(emp_duplic,num_duplic,seq_duplic,tip_duplic,cg9_duplic,cg4_duplic,cg2_duplic),0.0000)  into v_saldo_valdup  from dual;
   select nvl(saldo_duplic_crec_pagto(emp_duplic,num_duplic,seq_duplic,tip_duplic,cg9_duplic,cg4_duplic,cg2_duplic),0.0000)   into v_saldo_pagto   from dual;
   select nvl(saldo_duplic_crec_estorno(emp_duplic,num_duplic,seq_duplic,tip_duplic,cg9_duplic,cg4_duplic,cg2_duplic),0.0000) into v_saldo_estorno from dual;

   select nvl(v_saldo_valdup - v_saldo_pagto + v_saldo_estorno,0.0000) into v_saldo_encontrado from dual;

   select nvl(situacao_duplic,0) into v_saldo_situacao from fatu_070
   where fatu_070.codigo_empresa            = emp_duplic
     and fatu_070.num_duplicata             = num_duplic
     and fatu_070.seq_duplicatas            = seq_duplic
     and fatu_070.tipo_titulo               = tip_duplic
     and fatu_070.cli_dup_cgc_cli9          = cg9_duplic
     and fatu_070.cli_dup_cgc_cli4          = cg4_duplic
     and fatu_070.cli_dup_cgc_cli2          = cg2_duplic;

   if v_saldo_situacao=2
   then
      v_saldo_encontrado:=0.00;
   end if;

   return(v_saldo_encontrado);

end saldo_duplic_crec;

 

/

exec inter_pr_recompile;

