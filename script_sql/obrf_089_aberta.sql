  CREATE OR REPLACE VIEW "OBRF_089_ABERTA" ("ORDEM_SERVICO", "SEQ_ORDEM_SERVICO", "SITUACAO_ITEM", "QUALIDADE_RETORNO", "NUMERO_SOLICITACAO", "TIPO_SOLICITACAO_CONSERTO", "TAG_ATUALIZA_CONSERTO", "NIVEL_ESTRUTURA", "GRUPO_ESTRUTURA", "SUBGRUPO_ESTRUTURA", "ITEM_ESTRUTURA", "PERIODO_PRODUCAO", "ORDEM_PRODUCAO", "ORDEM_CONFECCAO", "SEQ_ORDEM_CONFECCAO", "CODIGO_BARRAS","ESTAGIO") AS 
  select obrf_089.ordem_servico                         as ordem_servico,
       obrf_089.sequencia                             as seq_ordem_servico,
       obrf_089.situacao_item                         as situacao_item,
       obrf_089.qualidade_retorno                     as qualidade_retorno,
       obrf_089.numero_solicitacao                    as numero_solicitacao,
       obrf_089.tipo_solicitacao_conserto             as tipo_solicitacao_conserto,
       obrf_089.tag_atualiza_conserto                 as tag_atualiza_conserto,
       obrf_089.nivel_estrutura                       as nivel_estrutura,
       obrf_089.grupo_estrutura                       as grupo_estrutura,
       obrf_089.subgrupo_estrutura                    as subgrupo_estrutura,
       obrf_089.item_estrutura                        as item_estrutura,
       obrf_089.periodo_producao                      as periodo_producao,
       obrf_089.ordem_producao                        as ordem_producao,
       obrf_089.ordem_confeccao                       as ordem_confeccao,
       obrf_089.seq_ordem_confeccao                   as seq_ordem_confeccao,
       obrf_089.codigo_barras                         as codigo_barras,
       obrf_089.estagio
 from obrf_089;

/
