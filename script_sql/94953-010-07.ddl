alter table rcnb_750
add (perc_cor number(5,2),
     faixa_etaria_custos number(3),
     qtde_marcacoes number(4,1));

alter table rcnb_750
modify (faixa_etaria_custos default 0,
        perc_cor            default 0.0,
        qtde_marcacoes      default 0.0);

/
declare
cursor rcnb_750_c is
   select f.rowid from rcnb_750 f where faixa_etaria_custos is null;
   
contaRegistro number;

begin
   contaRegistro := 0;
   
   for reg_rcnb_750_c in rcnb_750_c
   loop
      update rcnb_750
      set faixa_etaria_custos   = 0,
          perc_cor              = 0,
          qtde_marcacoes        = 0
      where faixa_etaria_custos is null
        and rcnb_750.rowid = reg_rcnb_750_c.rowid;
      
      contaRegistro := contaRegistro + 1;
      
      if contaRegistro = 1000
      then
         contaRegistro := 0;
         commit;
      end if;
   end loop; 

   commit;
end;
/
alter table rcnb_750
  drop constraint PK_RCNB_750;

drop index PK_RCNB_750;

alter table rcnb_750
add  constraint PK_RCNB_750
primary key (CODIGO_EMPRESA, NIVEL_PRODUTO, GRUPO_PRODUTO, SUBGRU_PRODUTO, ITEM_PRODUTO, NOME_USUARIO, COLECAO, ALTERNATIVA, TIPO_ANALISE,faixa_etaria_custos);

execute inter_pr_recompile;     
