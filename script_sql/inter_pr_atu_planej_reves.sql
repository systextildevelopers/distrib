create or replace procedure inter_pr_atu_planej_reves(p_ordem_planejamento    in number,
                                                      p_pedido_venda          in number,
                                                      p_nivel_produto         in varchar2,
                                                      p_grupo_produto         in varchar2,
                                                      p_subgrupo_produto      in varchar2,
                                                      p_item_produto          in varchar2,
                                                      p_qtde_prod_pedido      in number,
                                                      p_qtde_prod_comp        in number,
                                                      p_nivel_produto_orig    in varchar2,
                                                      p_grupo_produto_orig    in varchar2,
                                                      p_subgrupo_produto_orig in varchar2,
                                                      p_item_produto_orig     in varchar2,
                                                      p_seq_prod_orig         in number,
                                                      p_alter_prod_orig       in number,
                                                      p_seq_prod              in number,
                                                      p_tabela                in varchar2,
                                                      p_ordem_producao        in number)
is

   v_area_prod number;

begin
   -- A receber do compras...
   if p_tabela = 'supr_090' or p_tabela = 'supr_100'
   then
      
      begin
         update tmrp_625
            set tmrp_625.qtde_areceber_programada = tmrp_625.qtde_areceber_programada - p_qtde_prod_pedido
            where tmrp_625.ordem_planejamento          = p_ordem_planejamento
              and tmrp_625.pedido_venda                = p_pedido_venda
              and tmrp_625.seq_produto_origem          = p_seq_prod_orig
              and tmrp_625.nivel_produto_origem        = p_nivel_produto_orig
              and tmrp_625.grupo_produto_origem        = p_grupo_produto_orig
              and tmrp_625.subgrupo_produto_origem     = p_subgrupo_produto_orig
              and tmrp_625.item_produto_origem         = p_item_produto_orig
              and tmrp_625.alternativa_produto_origem  = p_alter_prod_orig
              and tmrp_625.seq_produto                 = p_seq_prod
              and tmrp_625.nivel_produto               = p_nivel_produto
              and tmrp_625.grupo_produto               = p_grupo_produto
              and tmrp_625.subgrupo_produto            = p_subgrupo_produto
              and tmrp_625.item_produto                = p_item_produto;
      exception
      when OTHERS then
         raise_application_error(-20000,'ATEN��O! N�o atualizou TMRP_625 (QTDE A RECEBER - QTDE RESERVADA)');
      end;
   -- A receber das ordens de produ��o...
   elsif p_tabela = 'pcpc_020' or p_tabela = 'pcpb_020' or p_tabela = 'pcpt_010'
   then
      begin
         update tmrp_625
            set qtde_areceber_programada = tmrp_625.qtde_areceber_programada - p_qtde_prod_pedido,
                qtde_reserva_programada  = tmrp_625.qtde_reserva_programada  - p_qtde_prod_comp
            where ordem_planejamento          = p_ordem_planejamento
              and pedido_venda                = p_pedido_venda
              and nivel_produto_origem        = p_nivel_produto_orig
              and grupo_produto_origem        = p_grupo_produto_orig
              and subgrupo_produto_origem     = p_subgrupo_produto_orig
              and item_produto_origem         = p_item_produto_orig
              and seq_produto_origem          = p_seq_prod_orig
              and nivel_produto               = p_nivel_produto
              and grupo_produto               = p_grupo_produto
              and subgrupo_produto            = p_subgrupo_produto
              and item_produto                = p_item_produto
              and seq_produto                 = p_seq_prod;
      exception
      when OTHERS then
         raise_application_error(-20000,'ATEN��O! N�o atualizou TMRP_625 (QTDE A RECEBER - QTDE RESERVADA)');
      end;
      
      if p_tabela = 'pcpc_020'
      then
         v_area_prod := 1;
      elsif p_tabela = 'pcpb_020'
         then
            v_area_prod := 2;
         elsif p_tabela = 'pcpt_010'
            then
               v_area_prod := 4;
            end if;
      
      -- Procedure para atualizar os componentes da ordem de produ��o com base
      -- no produto origem da ordem de planejamento.
      inter_pr_atu_reves_comp(p_ordem_planejamento,     p_pedido_venda,
                              p_nivel_produto,          p_grupo_produto,
                              p_subgrupo_produto,       p_item_produto,
                              p_seq_prod,               v_area_prod,
                              p_ordem_producao,         p_qtde_prod_pedido);
   end if;
end inter_pr_atu_planej_reves;


/

execute inter_pr_recompile;
/* versao: 3 */
