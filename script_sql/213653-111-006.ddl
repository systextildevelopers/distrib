-- Create table
create table FTEC_030
(
  ID              NUMBER not null,
  COD_PROD_GLOBAL NUMBER(9) not null,
  COD_TIPO_ORDEM  NUMBER(5) not null
);
-- Add comments to the columns 
comment on column FTEC_030.COD_PROD_GLOBAL
  is 'HDOC_001 WHERE TIPO = 85';
comment on column FTEC_030.COD_TIPO_ORDEM
  is 'FTEC_020';
-- Create/Recreate primary, unique and foreign key constraints 
alter table FTEC_030
  add constraint ID primary key (ID)
  using index;
alter table FTEC_030
  add constraint REF_FTEC030_FTEC020 foreign key (COD_TIPO_ORDEM)
  references FTEC_020 (ID);
  
-- Create/Recreate indexes 
create index IDX1_FTEC_030 on FTEC_030 (COD_PROD_GLOBAL, COD_TIPO_ORDEM);

