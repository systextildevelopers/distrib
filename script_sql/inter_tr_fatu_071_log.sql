
  CREATE OR REPLACE TRIGGER "INTER_TR_FATU_071_LOG" 
after insert or delete or update
on fatu_071
for each row
declare
   ws_usuario_rede           varchar2(20) ;
   ws_maquina_rede           varchar2(40) ;
   ws_aplicativo             varchar2(20) ;
   ws_sid                    number(9) ;
   ws_empresa                number(3) ;
   ws_usuario_systextil      varchar2(250) ;
   ws_locale_usuario         varchar2(5) ;
   v_nome_programa           varchar2(20) ;


begin
-- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   v_nome_programa := inter_fn_nome_programa(ws_sid);  
       
 if inserting
 then
    begin

        insert into fatu_071_log (
           TIPO_OCORR,   /*0*/
           DATA_OCORR,   /*1*/
           HORA_OCORR,   /*2*/
           USUARIO_REDE,   /*3*/
           MAQUINA_REDE,   /*4*/
           APLICACAO,   /*5*/
           USUARIO_SISTEMA,   /*6*/
           NOME_PROGRAMA,   /*7*/
           PEDIDO_NOTA_OLD,   /*8*/
           PEDIDO_NOTA_NEW,   /*9*/
           SERIE_OLD,   /*10*/
           SERIE_NEW,   /*11*/
           SEQUENCIA_OLD,   /*12*/
           SEQUENCIA_NEW,   /*13*/
           VENCIMENTO_OLD,   /*14*/
           VENCIMENTO_NEW,   /*15*/
           VALOR_OLD,   /*16*/
           VALOR_NEW,   /*17*/
           PARCELA_ENTRADA_OLD,   /*18*/
           PARCELA_ENTRADA_NEW,   /*19*/
           GEROU_DUPLICATAS_OLD,   /*20*/
           GEROU_DUPLICATAS_NEW    /*21*/
        ) values (
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           0,/*8*/
           :new.PEDIDO_NOTA, /*9*/
           '',/*10*/
           :new.SERIE, /*11*/
           0,/*12*/
           :new.SEQUENCIA, /*13*/
           null,/*14*/
           :new.VENCIMENTO, /*15*/
           0,/*16*/
           :new.VALOR, /*17*/
           0,/*18*/
           :new.PARCELA_ENTRADA, /*19*/
           0,/*20*/
           :new.GEROU_DUPLICATAS /*21*/
         );
    end;
 end if;


 if updating
 then
    begin
        insert into fatu_071_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           PEDIDO_NOTA_OLD, /*8*/
           PEDIDO_NOTA_NEW, /*9*/
           SERIE_OLD, /*10*/
           SERIE_NEW, /*11*/
           SEQUENCIA_OLD, /*12*/
           SEQUENCIA_NEW, /*13*/
           VENCIMENTO_OLD, /*14*/
           VENCIMENTO_NEW, /*15*/
           VALOR_OLD, /*16*/
           VALOR_NEW, /*17*/
           PARCELA_ENTRADA_OLD, /*18*/
           PARCELA_ENTRADA_NEW, /*19*/
           GEROU_DUPLICATAS_OLD, /*20*/
           GEROU_DUPLICATAS_NEW  /*21*/
        ) values (
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.PEDIDO_NOTA,  /*8*/
           :new.PEDIDO_NOTA, /*9*/
           :old.SERIE,  /*10*/
           :new.SERIE, /*11*/
           :old.SEQUENCIA,  /*12*/
           :new.SEQUENCIA, /*13*/
           :old.VENCIMENTO,  /*14*/
           :new.VENCIMENTO, /*15*/
           :old.VALOR,  /*16*/
           :new.VALOR, /*17*/
           :old.PARCELA_ENTRADA,  /*18*/
           :new.PARCELA_ENTRADA, /*19*/
           :old.GEROU_DUPLICATAS,  /*20*/
           :new.GEROU_DUPLICATAS  /*21*/
         );
    end;
 end if;


 if deleting
 then
    begin
        insert into fatu_071_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           PEDIDO_NOTA_OLD, /*8*/
           PEDIDO_NOTA_NEW, /*9*/
           SERIE_OLD, /*10*/
           SERIE_NEW, /*11*/
           SEQUENCIA_OLD, /*12*/
           SEQUENCIA_NEW, /*13*/
           VENCIMENTO_OLD, /*14*/
           VENCIMENTO_NEW, /*15*/
           VALOR_OLD, /*16*/
           VALOR_NEW, /*17*/
           PARCELA_ENTRADA_OLD, /*18*/
           PARCELA_ENTRADA_NEW, /*19*/
           GEROU_DUPLICATAS_OLD, /*20*/
           GEROU_DUPLICATAS_NEW /*21*/
        ) values (
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede,/*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.PEDIDO_NOTA, /*8*/
           0, /*9*/
           :old.SERIE, /*10*/
           '', /*11*/
           :old.SEQUENCIA, /*12*/
           0, /*13*/
           :old.VENCIMENTO, /*14*/
           null, /*15*/
           :old.VALOR, /*16*/
           0, /*17*/
           :old.PARCELA_ENTRADA, /*18*/
           0, /*19*/
           :old.GEROU_DUPLICATAS, /*20*/
           0 /*21*/
         );
    end;
 end if;
end inter_tr_fatu_071_log;
-- ALTER TRIGGER "INTER_TR_FATU_071_LOG" ENABLE
 

/

exec inter_pr_recompile;

