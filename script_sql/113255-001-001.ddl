	
insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('manu_f024', 'Solicitação de Compra de Componentes Para Manutenção', 0,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'manu_f024', 'NENHUM', 1, 20, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Solicitação de Compra de Componentes Para Manutenção'
 where hdoc_036.codigo_programa = 'manu_f024'
   and hdoc_036.locale          = 'es_ES';
commit;
