
CREATE OR REPLACE TRIGGER "INTER_TR_SUPR_090_PLAN" 
   after delete or
         update of cod_cancelamento
   on supr_090
   for each row

declare
   ws_sid number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);

begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   if (updating and (:old.cod_cancelamento = 0 and :new.cod_cancelamento <> :old.cod_cancelamento)) or deleting
   then
      begin
         insert into tmrp_615
           (nome_programa,
            nr_solicitacao,                           tipo_registro,
            cgc_cliente9)
         values
           ('trigger_planejamento',
            888,                                      888,
            ws_sid);
         exception when others
	 then raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'TMRP_615' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;

      begin
         delete from tmrp_630
         where tmrp_630.area_producao      = 9
           and tmrp_630.ordem_prod_compra  = :old.pedido_compra;
         exception when others
         then raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26373', 'TMRP_630' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;
   end if;
end;

-- ALTER TRIGGER "INTER_TR_SUPR_090_PLAN" ENABLE
 

/

exec inter_pr_recompile;

