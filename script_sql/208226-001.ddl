-- Cópia de Referencias em massa

insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('rcnb_f571', 'Cadastro temporario de Referencias', 0,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'rcnb_f571', 'basi_menu', 0, 0, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'rcnb_f571', 'basi_menu', 0, 0, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Registro temporal de referencias'
 where hdoc_036.codigo_programa = 'rcnb_f571'
   and hdoc_036.locale          = 'es_ES';
commit;
