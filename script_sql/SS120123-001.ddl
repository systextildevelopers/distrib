ALTER TABLE OBRF_180
ADD (COD_LOCAL_COND_PGTO      VARCHAR2(1) default ' ',
     COD_MSG_COND_PGTO        NUMBER(6) default 0,
     COD_SEQ_COND_PGTO        NUMBER(2) default 0);

/
ALTER TABLE PEDI_070
ADD (GERA_COND_DANFE   VARCHAR(1) default 'N');

exec inter_pr_recompile;
/
