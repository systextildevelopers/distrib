CREATE OR REPLACE TRIGGER "INTER_TR_FATU_501_HIST"
  AFTER INSERT OR DELETE OR UPDATE OF CODIGO_EMPRESA, PERIODO_FATURAM, PERIODO_OBRF, PERIODO_FINANCEIRO, PERIODO_CTA_REC, PERIODO_CTA_PAG
  
ON FATU_501
  FOR EACH ROW

DECLARE
  WS_USUARIO_REDE      VARCHAR2(20);
  WS_MAQUINA_REDE      VARCHAR2(40);
  WS_APLICATIVO        VARCHAR2(20);
  WS_SID               NUMBER(9);
  WS_EMPRESA           NUMBER(3);
  WS_USUARIO_SYSTEXTIL VARCHAR2(250);
  WS_LOCALE_USUARIO    VARCHAR2(5);

  WS_NOME_PROGRAMA HDOC_090.PROGRAMA%TYPE;
  LONG_AUX                  VARCHAR2(4000);

BEGIN

  INTER_PR_DADOS_USUARIO(WS_USUARIO_REDE,
                         WS_MAQUINA_REDE,
                         WS_APLICATIVO,
                         WS_SID,
                         WS_USUARIO_SYSTEXTIL,
                         WS_EMPRESA,
                         WS_LOCALE_USUARIO);

  WS_NOME_PROGRAMA := INTER_FN_NOME_PROGRAMA(WS_SID);

  IF INSERTING THEN
    INSERT INTO HIST_100
      (PROGRAMA,
       TABELA,
       OPERACAO,
       DATA_OCORR,
       USUARIO_REDE,
       MAQUINA_REDE,
       APLICACAO,
       NUM01,
       LONG01
       
       )
    VALUES
      (WS_NOME_PROGRAMA,
       'FATU_501',
       'I',
       SYSDATE,
       WS_USUARIO_REDE,
       WS_MAQUINA_REDE,
       WS_APLICATIVO,
       :NEW.CODIGO_EMPRESA,
       INTER_FN_BUSCAR_TAG('lb07207#EMPRESA:',
                           WS_LOCALE_USUARIO,
                           WS_USUARIO_SYSTEXTIL) || ' '     ||
                           :NEW.CODIGO_EMPRESA   || CHR(10) ||
       
       INTER_FN_BUSCAR_TAG('lb10164#PER�ODO DE FATURAMENTO:',
                           WS_LOCALE_USUARIO,
                           WS_USUARIO_SYSTEXTIL) || ' '                    ||
                           TO_CHAR(:NEW.PERIODO_FATURAM, 'DD/MM/YYYY')     || CHR(10) ||
       
       INTER_FN_BUSCAR_TAG('lb16413#PER�ODO OBRIGA��ES FISCAIS:',
                           WS_LOCALE_USUARIO,
                           WS_USUARIO_SYSTEXTIL) || ' '     ||
                           TO_CHAR(:NEW.PERIODO_OBRF, 'DD/MM/YYYY')        || CHR(10) ||
       
       INTER_FN_BUSCAR_TAG('lb08109#PER�ODO DO FINANCEIRO:',
                           WS_LOCALE_USUARIO,
                           WS_USUARIO_SYSTEXTIL) || ' '     ||
                           TO_CHAR(:NEW.PERIODO_FINANCEIRO, 'DD/MM/YYYY')  || CHR(10) ||
       
       INTER_FN_BUSCAR_TAG('lb08108#PER�ODO DO CONTAS A RECEBER:',
                           WS_LOCALE_USUARIO,
                           WS_USUARIO_SYSTEXTIL) || ' '     ||
                           TO_CHAR(:NEW.PERIODO_CTA_REC, 'DD/MM/YYYY')     || CHR(10) ||  
                           
       INTER_FN_BUSCAR_TAG('lb08107#PER�ODO DO CONTAS A PAGAR:',
                           WS_LOCALE_USUARIO,
                           WS_USUARIO_SYSTEXTIL) || ' '     ||
                           TO_CHAR(:NEW.PERIODO_CTA_PAG, 'DD/MM/YYYY')                                                    
                           
       );
  END IF;
  
  IF UPDATING THEN
      
     IF  :OLD.CODIGO_EMPRESA<> :NEW.CODIGO_EMPRESA
      THEN
          LONG_AUX := LONG_AUX ||
          INTER_FN_BUSCAR_TAG('lb07207#EMPRESA:',WS_LOCALE_USUARIO,WS_USUARIO_SYSTEXTIL) || ' '
                                    || :OLD.CODIGO_EMPRESA  || ' -> '
                                    || :NEW.CODIGO_EMPRESA 
                                    || CHR(10);
      END IF;
      IF  :OLD.PERIODO_FATURAM <> :NEW.PERIODO_FATURAM
      THEN
          LONG_AUX := LONG_AUX ||
          INTER_FN_BUSCAR_TAG('lb10164#PER�ODO DE FATURAMENTO:',WS_LOCALE_USUARIO,WS_USUARIO_SYSTEXTIL) || ' '
                                    || TO_CHAR(:OLD.PERIODO_FATURAM,'DD/MM/YYYY')   || ' -> '
                                    || TO_CHAR(:NEW.PERIODO_FATURAM,'DD/MM/YYYY')
                                    || CHR(10);
      END IF;
      IF  :OLD.PERIODO_OBRF <> :NEW.PERIODO_OBRF
      THEN
          LONG_AUX := LONG_AUX ||
          INTER_FN_BUSCAR_TAG('lb16413#PER�ODO OBRIGA��ES FISCAIS:',WS_LOCALE_USUARIO,WS_USUARIO_SYSTEXTIL) || ' '
                                    || TO_CHAR(:OLD.PERIODO_OBRF,'DD/MM/YYYY')   || ' -> '
                                    || TO_CHAR(:NEW.PERIODO_OBRF,'DD/MM/YYYY')
                                    || CHR(10);
      END IF;
      IF  :OLD.PERIODO_FINANCEIRO <> :NEW.PERIODO_FINANCEIRO
      THEN
          LONG_AUX := LONG_AUX ||
          INTER_FN_BUSCAR_TAG('lb08109#PER�ODO DO FINANCEIRO:',WS_LOCALE_USUARIO,WS_USUARIO_SYSTEXTIL) || ' '
                                    || TO_CHAR(:OLD.PERIODO_FINANCEIRO,'DD/MM/YYYY')   || ' -> '
                                    || TO_CHAR(:NEW.PERIODO_FINANCEIRO,'DD/MM/YYYY')
                                    || CHR(10);
      END IF;
      IF  :OLD.PERIODO_CTA_REC <> :NEW.PERIODO_CTA_REC
      THEN
          LONG_AUX := LONG_AUX ||
          INTER_FN_BUSCAR_TAG('lb08108#PER�ODO DO CONTAS A RECEBER:',WS_LOCALE_USUARIO,WS_USUARIO_SYSTEXTIL) || ' '
                                    || TO_CHAR(:OLD.PERIODO_CTA_REC,'DD/MM/YYYY')   || ' -> '
                                    || TO_CHAR(:NEW.PERIODO_CTA_REC,'DD/MM/YYYY')
                                    || CHR(10);
      END IF;
      IF  :OLD.PERIODO_CTA_PAG <> :NEW.PERIODO_CTA_PAG
      THEN
          LONG_AUX := LONG_AUX ||
          INTER_FN_BUSCAR_TAG('lb08107#PER�ODO DO CONTAS A PAGAR:',WS_LOCALE_USUARIO,WS_USUARIO_SYSTEXTIL) || ' '
                                    || TO_CHAR(:OLD.PERIODO_CTA_PAG,'DD/MM/YYYY')   || ' -> '
                                    || TO_CHAR(:NEW.PERIODO_CTA_PAG,'DD/MM/YYYY')
                                    || CHR(10);
      END IF;

      

      BEGIN
        
         INSERT INTO HIST_100
                    (PROGRAMA,
                     TABELA,
                     OPERACAO,
                     DATA_OCORR,
                     USUARIO_REDE,
                     MAQUINA_REDE,
                     APLICACAO,
                     NUM01,
                     LONG01
          ) VALUES (
            WS_NOME_PROGRAMA,
             'FATU_501',
             'A',
             SYSDATE,
             WS_USUARIO_REDE,
             WS_MAQUINA_REDE,
             WS_APLICATIVO,
             :NEW.CODIGO_EMPRESA,
            LONG_AUX
          );
         
      EXCEPTION WHEN OTHERS THEN
         RAISE_APPLICATION_ERROR (-20000, INTER_FN_BUSCAR_TAG_COMPOSTA('ds22559#ATENCAO! Nao inseriu {0}. Status: {1}.', 'HIST_100(001-2)' , SQLERRM , '' , '' , '' , '' , '' , '' , '' , '',WS_LOCALE_USUARIO,WS_USUARIO_SYSTEXTIL));

      END;
   END IF;   
   
  IF DELETING THEN
    INSERT INTO HIST_100
      (PROGRAMA,
       TABELA,
       OPERACAO,
       DATA_OCORR,
       USUARIO_REDE,
       MAQUINA_REDE,
       APLICACAO,
       NUM01,
       LONG01
       
       )
    VALUES
      (WS_NOME_PROGRAMA,
       'FATU_501',
       'D',
       SYSDATE,
       WS_USUARIO_REDE,
       WS_MAQUINA_REDE,
       WS_APLICATIVO,
       :OLD.CODIGO_EMPRESA,
       INTER_FN_BUSCAR_TAG('lb07207#EMPRESA:',
                           WS_LOCALE_USUARIO,
                           WS_USUARIO_SYSTEXTIL) || ' '     ||
                           :OLD.CODIGO_EMPRESA   || CHR(10) ||
       
       INTER_FN_BUSCAR_TAG('lb10164#PER�ODO DE FATURAMENTO:',
                           WS_LOCALE_USUARIO,
                           WS_USUARIO_SYSTEXTIL) || ' '                    ||
                           TO_CHAR(:OLD.PERIODO_FATURAM, 'DD/MM/YYYY')     || CHR(10) ||
       
       INTER_FN_BUSCAR_TAG('lb16413#PER�ODO OBRIGA��ES FISCAIS:',
                           WS_LOCALE_USUARIO,
                           WS_USUARIO_SYSTEXTIL) || ' '     ||
                           TO_CHAR(:OLD.PERIODO_OBRF, 'DD/MM/YYYY')        || CHR(10) ||
       
       INTER_FN_BUSCAR_TAG('lb08109#PER�ODO DO FINANCEIRO:',
                           WS_LOCALE_USUARIO,
                           WS_USUARIO_SYSTEXTIL) || ' '     ||
                           TO_CHAR(:OLD.PERIODO_FINANCEIRO, 'DD/MM/YYYY')  || CHR(10) ||
       
       INTER_FN_BUSCAR_TAG('lb08108#PER�ODO DO CONTAS A RECEBER:',
                           WS_LOCALE_USUARIO,
                           WS_USUARIO_SYSTEXTIL) || ' '     ||
                           TO_CHAR(:OLD.PERIODO_CTA_REC, 'DD/MM/YYYY')     || CHR(10) ||  
                           
       INTER_FN_BUSCAR_TAG('lb08107#PER�ODO DO CONTAS A PAGAR:',
                           WS_LOCALE_USUARIO,
                           WS_USUARIO_SYSTEXTIL) || ' '     ||
                           TO_CHAR(:OLD.PERIODO_CTA_PAG, 'DD/MM/YYYY') 
  );
  END IF;
END INTER_TR_FATU_501_HIST;  
 
