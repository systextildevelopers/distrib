-- Add/modify columns 
alter table BASI_050 add (situacao_programacao   NUMBER(1));

alter table BASI_050 modify situacao_programacao default 0;

-- Add comments to the columns 
comment on column BASI_050.situacao_programacao
  is 'Situacao de liberacao para produto nivel 4 para engenharia';

exec inter_pr_recompile;
/

declare
nro_registro number;
cursor basi_050_c is
   select f.rowid from basi_050 f
                  where situacao_programacao is null;

begin
   nro_registro := 0;

   for reg_basi_050_c in basi_050_c
   loop
      begin
         update basi_050
         set   basi_050.situacao_programacao    = 0
         where basi_050.situacao_programacao     is null
           and basi_050.rowid          		= reg_basi_050_c.rowid; 
      end;
      
      nro_registro := nro_registro + 1;
      
      if nro_registro > 1000
      then
         nro_registro := 0;
         commit;
      end if;
   end loop; 

   commit;
end;
/

exec inter_pr_recompile;

