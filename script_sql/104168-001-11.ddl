CREATE TABLE EMPR_100 (
COD_EMPRESA NUMBER(3) default 0,
COD_EMPRESA_SUPPLIERCARD VARCHAR2(2) default '',
COD_LOJA VARCHAR2(4) default '',
COD_FILIAL VARCHAR2(4) default '',
NR_DIAS_REANALISE NUMBER(3) default 90
);
ALTER TABLE EMPR_100
ADD CONSTRAINT PK_EMPR_100 PRIMARY KEY (COD_EMPRESA);
comment on column EMPR_100.cod_empresa is 'Identifica o c�digo da empresa no systextil';
comment on column EMPR_100.cod_empresa_suppliercard is 'Identifica o c�digo da empresa na suppliecard';
comment on column EMPR_100.cod_loja is 'Identifica o c�digo da loja na suppliecard';
comment on column EMPR_100.cod_filial is 'Identifica o c�digo da filial na suppliecard';
comment on column EMPR_100.NR_DIAS_REANALISE is 'Indica o n�mero de dias que a suppliecard dever� fazer a rean�lise do cliente';

EXEC inter_pr_recompile;
