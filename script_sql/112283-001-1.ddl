alter table fatu_050
add( NOTA_PENDENTE NUMBER(1) default 0 not null);

alter table obrf_010
add( NOTA_PENDENTE NUMBER(1) default 0 not null);

create table I_OBRF_016
(
  ID_REGISTRO      NUMBER(9) not null,
  FLAG_IMPORTACAO  NUMBER(1) default 0,
  TIPO_ATUALIZACAO VARCHAR2(1),
  DATA_EXPORTACAO  DATE,
  DATA_IMPORTACAO  DATE,
  NUM_CONHECIMENTO NUMBER(9) default 0 not null,
  SER_CONHECIMENTO VARCHAR2(3) default '' not null,
  TRANSPORTADORA9  NUMBER(9) default 0 not null,
  TRANSPORTADORA4  NUMBER(4) default 0 not null,
  TRANSPORTADORA2  NUMBER(2) default 0 not null,
  NUMERO_NOTA      NUMBER(9) default 0 not null,
  SERIE_NOTA       VARCHAR2(3) default '' not null,
  FORNECEDOR9      NUMBER(9) default 0 not null,
  FORNECEDOR4      NUMBER(4) default 0 not null,
  FORNECEDOR2      NUMBER(2) default 0 not null,
  COD_TRANSACAO    NUMBER(3) default 0,
  COD_DEPOSITO     NUMBER(3) default 0
);

/
exec inter_pr_recompile;
