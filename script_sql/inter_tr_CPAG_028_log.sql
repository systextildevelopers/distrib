create or replace trigger inter_tr_CPAG_028_log 
after insert or delete or update 
on CPAG_028 
for each row 
declare 
   ws_usuario_rede           varchar2(20) ; 
   ws_maquina_rede           varchar2(40) ; 
   ws_aplicativo             varchar2(20) ; 
   ws_sid                    number(9) ; 
   ws_empresa                number(3) ; 
   ws_usuario_systextil      varchar2(250) ; 
   ws_locale_usuario         varchar2(5) ; 
   v_nome_programa           varchar2(20) ; 


begin
-- Dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid, 
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario); 



    begin 
       select hdoc_090.programa 
       into v_nome_programa 
       from hdoc_090 
       where hdoc_090.sid = ws_sid 
         and rownum       = 1 
         and hdoc_090.programa not like '%menu%' 
         and hdoc_090.programa not like '%_m%'; 
       exception 
         when no_data_found then            v_nome_programa := 'SQL'; 
    end; 
 
 
 
 if inserting 
 then 
    begin 
 
        insert into CPAG_028_log (
           TIPO_OCORR,   /*0*/ 
           DATA_OCORR,   /*1*/ 
           HORA_OCORR,   /*2*/ 
           USUARIO_REDE,   /*3*/ 
           MAQUINA_REDE,   /*4*/ 
           APLICACAO,   /*5*/ 
           USUARIO_SISTEMA,   /*6*/ 
           NOME_PROGRAMA,   /*7*/ 
           cod_banco_OLD,   /*8*/ 
           cod_banco_NEW,   /*9*/ 
           conta_corrente_OLD,   /*10*/ 
           conta_corrente_NEW,   /*11*/ 
           data_lancamento_OLD,   /*12*/ 
           data_lancamento_NEW,   /*13*/ 
           cod_registro_OLD,   /*14*/ 
           cod_registro_NEW,   /*15*/ 
           conciliado_OLD,   /*16*/ 
           conciliado_NEW,   /*17*/ 
           cod_conciliacao_OLD,   /*18*/ 
           cod_conciliacao_NEW,   /*19*/ 
           descr_lancamento_OLD,   /*20*/ 
           descr_lancamento_NEW,   /*21*/ 
           valor_lancamento_OLD,   /*22*/ 
           valor_lancamento_NEW,   /*23*/ 
           debito_credito_OLD,   /*24*/ 
           debito_credito_NEW,   /*25*/ 
           nr_documento_OLD,   /*26*/ 
           nr_documento_NEW,   /*27*/ 
           marca_concilia_OLD,   /*28*/ 
           marca_concilia_NEW,   /*29*/ 
           tipo_registro_OLD,   /*30*/ 
           tipo_registro_NEW,   /*31*/ 
           flag_conciliacao_OLD,   /*32*/ 
           flag_conciliacao_NEW    /*33*/
        ) values (    
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/ 
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           0,/*8*/
           :new.cod_banco, /*9*/   
           0,/*10*/
           :new.conta_corrente, /*11*/   
           null,/*12*/
           :new.data_lancamento, /*13*/   
           0,/*14*/
           :new.cod_registro, /*15*/   
           '',/*16*/
           :new.conciliado, /*17*/   
           0,/*18*/
           :new.cod_conciliacao, /*19*/   
           '',/*20*/
           :new.descr_lancamento, /*21*/   
           0,/*22*/
           :new.valor_lancamento, /*23*/   
           '',/*24*/
           :new.debito_credito, /*25*/   
           0,/*26*/
           :new.nr_documento, /*27*/   
           0,/*28*/
           :new.marca_concilia, /*29*/   
           0,/*30*/
           :new.tipo_registro, /*31*/   
           0,/*32*/
           :new.flag_conciliacao /*33*/   
         );    
    end;    
 end if;    
  
  
 if updating 
 then 
    begin 
        insert into CPAG_028_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           cod_banco_OLD, /*8*/   
           cod_banco_NEW, /*9*/   
           conta_corrente_OLD, /*10*/   
           conta_corrente_NEW, /*11*/   
           data_lancamento_OLD, /*12*/   
           data_lancamento_NEW, /*13*/   
           cod_registro_OLD, /*14*/   
           cod_registro_NEW, /*15*/   
           conciliado_OLD, /*16*/   
           conciliado_NEW, /*17*/   
           cod_conciliacao_OLD, /*18*/   
           cod_conciliacao_NEW, /*19*/   
           descr_lancamento_OLD, /*20*/   
           descr_lancamento_NEW, /*21*/   
           valor_lancamento_OLD, /*22*/   
           valor_lancamento_NEW, /*23*/   
           debito_credito_OLD, /*24*/   
           debito_credito_NEW, /*25*/   
           nr_documento_OLD, /*26*/   
           nr_documento_NEW, /*27*/   
           marca_concilia_OLD, /*28*/   
           marca_concilia_NEW, /*29*/   
           tipo_registro_OLD, /*30*/   
           tipo_registro_NEW, /*31*/   
           flag_conciliacao_OLD, /*32*/   
           flag_conciliacao_NEW  /*33*/  
        ) values (    
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/ 
           :old.cod_banco,  /*8*/  
           :new.cod_banco, /*9*/   
           :old.conta_corrente,  /*10*/  
           :new.conta_corrente, /*11*/   
           :old.data_lancamento,  /*12*/  
           :new.data_lancamento, /*13*/   
           :old.cod_registro,  /*14*/  
           :new.cod_registro, /*15*/   
           :old.conciliado,  /*16*/  
           :new.conciliado, /*17*/   
           :old.cod_conciliacao,  /*18*/  
           :new.cod_conciliacao, /*19*/   
           :old.descr_lancamento,  /*20*/  
           :new.descr_lancamento, /*21*/   
           :old.valor_lancamento,  /*22*/  
           :new.valor_lancamento, /*23*/   
           :old.debito_credito,  /*24*/  
           :new.debito_credito, /*25*/   
           :old.nr_documento,  /*26*/  
           :new.nr_documento, /*27*/   
           :old.marca_concilia,  /*28*/  
           :new.marca_concilia, /*29*/   
           :old.tipo_registro,  /*30*/  
           :new.tipo_registro, /*31*/   
           :old.flag_conciliacao,  /*32*/  
           :new.flag_conciliacao  /*33*/  
         );    
    end;    
 end if;    
  
  
 if deleting 
 then 
    begin 
        insert into CPAG_028_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           cod_banco_OLD, /*8*/   
           cod_banco_NEW, /*9*/   
           conta_corrente_OLD, /*10*/   
           conta_corrente_NEW, /*11*/   
           data_lancamento_OLD, /*12*/   
           data_lancamento_NEW, /*13*/   
           cod_registro_OLD, /*14*/   
           cod_registro_NEW, /*15*/   
           conciliado_OLD, /*16*/   
           conciliado_NEW, /*17*/   
           cod_conciliacao_OLD, /*18*/   
           cod_conciliacao_NEW, /*19*/   
           descr_lancamento_OLD, /*20*/   
           descr_lancamento_NEW, /*21*/   
           valor_lancamento_OLD, /*22*/   
           valor_lancamento_NEW, /*23*/   
           debito_credito_OLD, /*24*/   
           debito_credito_NEW, /*25*/   
           nr_documento_OLD, /*26*/   
           nr_documento_NEW, /*27*/   
           marca_concilia_OLD, /*28*/   
           marca_concilia_NEW, /*29*/   
           tipo_registro_OLD, /*30*/   
           tipo_registro_NEW, /*31*/   
           flag_conciliacao_OLD, /*32*/   
           flag_conciliacao_NEW /*33*/   
        ) values (    
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede,/*4*/ 
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           :old.cod_banco, /*8*/   
           0, /*9*/
           :old.conta_corrente, /*10*/   
           0, /*11*/
           :old.data_lancamento, /*12*/   
           null, /*13*/
           :old.cod_registro, /*14*/   
           0, /*15*/
           :old.conciliado, /*16*/   
           '', /*17*/
           :old.cod_conciliacao, /*18*/   
           0, /*19*/
           :old.descr_lancamento, /*20*/   
           '', /*21*/
           :old.valor_lancamento, /*22*/   
           0, /*23*/
           :old.debito_credito, /*24*/   
           '', /*25*/
           :old.nr_documento, /*26*/   
           0, /*27*/
           :old.marca_concilia, /*28*/   
           0, /*29*/
           :old.tipo_registro, /*30*/   
           0, /*31*/
           :old.flag_conciliacao, /*32*/   
           0 /*33*/
         );    
    end;    
 end if;    
end inter_tr_CPAG_028_log;
