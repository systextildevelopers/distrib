ALTER TABLE OBRF_180
ADD (COD_LOCAL_CELULAR   VARCHAR2(1) default 'D',
    COD_MSG_CELULAR      NUMBER(6) default 0,
    COD_SEQ_CELULAR      NUMBER(2) default 0);

exec inter_pr_recompile;
