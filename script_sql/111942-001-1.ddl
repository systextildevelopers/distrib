CREATE TABLE "OBRF_971" 
   (  "NIVEL" VARCHAR2(1) DEFAULT ' ' NOT NULL ENABLE, 
  "GRUPO" VARCHAR2(5) DEFAULT ' ' NOT NULL ENABLE, 
  "SUBGRUPO" VARCHAR2(3) DEFAULT ' ' NOT NULL ENABLE, 
  "ITEM" VARCHAR2(6) DEFAULT ' ' NOT NULL ENABLE, 
  "LOTE" NUMBER(6,0) DEFAULT 0 NOT NULL ENABLE, 
  "QTDE" NUMBER(17,3) DEFAULT 0, 
  "VAL_UNITARIO" NUMBER(17,5) DEFAULT 0, 
  "COD_EMPRESA" NUMBER(3,0) DEFAULT 0, 
  "NUM_NOTA" NUMBER(9,0) DEFAULT 0, 
  "SERIE_NOTA" VARCHAR2(3) DEFAULT ' ', 
  "IND_REM_RET" NUMBER(1,0) DEFAULT 0 NOT NULL ENABLE, 
  "SEQ_NOTA" NUMBER(9,0) DEFAULT 0, 
  "NUM_NF_ENTR" NUMBER(9,0) DEFAULT 0, 
  "SERIE_NF_ENTR" VARCHAR2(3) DEFAULT '', 
  "CNPJ_9_NF_ENTR" NUMBER(9,0) DEFAULT 0, 
  "CNPJ_4_NF_ENTR" NUMBER(9,0) DEFAULT 0, 
  "CNPJ_2_NF_ENTR" NUMBER(9,0) DEFAULT 0);
   ALTER TABLE OBRF_971 ADD CONSTRAINT "PK_OBRF_971" PRIMARY KEY ("NIVEL", "GRUPO", "SUBGRUPO", "ITEM", "LOTE", "IND_REM_RET");
   COMMENT ON COLUMN "OBRF_971"."NIVEL" IS 'Nivel do produto';
   COMMENT ON COLUMN "OBRF_971"."GRUPO" IS 'grupo do produto';
   COMMENT ON COLUMN "OBRF_971"."SUBGRUPO" IS 'subgrupo do produto';
   COMMENT ON COLUMN "OBRF_971"."ITEM" IS 'item do produto';
   COMMENT ON COLUMN "OBRF_971"."LOTE" IS 'lote estoque do produto';
   COMMENT ON COLUMN "OBRF_971"."QTDE" IS 'quantidade em estoque/elabora��o';
   COMMENT ON COLUMN "OBRF_971"."COD_EMPRESA" IS 'Codigo da empresa (quando j� foi gerado nota)';
   COMMENT ON COLUMN "OBRF_971"."NUM_NOTA" IS 'Numero da nota fiscal (quando j� foi gerado nota)';
   COMMENT ON COLUMN "OBRF_971"."SERIE_NOTA" IS 'Serie da nota fiscal (quando j� foi gerado nota)';
   COMMENT ON COLUMN "OBRF_971"."IND_REM_RET" IS 'Indica se o produto (1) � de remessa ou (2) de retorno';
   COMMENT ON COLUMN "OBRF_971"."SEQ_NOTA" IS 'sequencia da nota fiscal (quando j� foi gerado nota)';
   COMMENT ON TABLE "OBRF_971"  IS 'Tabela utilizada para gerar notas para invent�rio na sala de malhas nos processos de terceiros';
