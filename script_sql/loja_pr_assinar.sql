
  CREATE OR REPLACE PROCEDURE "LOJA_PR_ASSINAR" (pTipo in varchar2,
                                            pConcatena in varchar2,
                                            pConecta in number,
                                            pHash in varchar2,
                                            pCodFil in varchar2,
                                            pNumPed in varchar2,
                                            pTipPed in varchar2) is

begin
  loja_pr_Assinar_inte(pTipo,
                  pConcatena,
                  pConecta,
                  pHash,
                  pCodFil,
                  pNumPed,
                  pTipPed);
end loja_pr_Assinar;
 

/

exec inter_pr_recompile;

