--------------------------------------------------------------------------------
-- Inclusão novos campos no FINA_202 para atender expiracao do link pagamento --
--------------------------------------------------------------------------------
ALTER TABLE FINA_202
ADD TEMPO_EXPIRACAO_MINUTO NUMBER(9,0);

ALTER TABLE FINA_202
ADD TEMPO_CANCELAMENTO_MINUTO NUMBER(9,0);
 
