alter table empr_002 
add
(consumo_max_comp NUMBER(13,6)         DEFAULT 0.000000);

comment on column empr_002.consumo_max_comp is 'Indica o valor maximo de consumo do componente no cadastro de estrutura de produto por projetos.';
/
exec inter_pr_recompile;
