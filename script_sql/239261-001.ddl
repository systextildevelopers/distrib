alter table pedi_150
modify complemento_endereco varchar2(60);

alter table pedi_010
modify complemento varchar2(60);

alter table inte_010
modify complemento varchar2(60);

alter table PEDI_010_HIST
modify complemento_new varchar2(60);

alter table PEDI_010_HIST
modify complemento_old varchar2(60);
