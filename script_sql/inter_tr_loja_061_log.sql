
  CREATE OR REPLACE TRIGGER "INTER_TR_LOJA_061_LOG" 
after insert or delete or update
on LOJA_061
for each row
declare
   ws_usuario_rede           varchar2(20) ;
   ws_maquina_rede           varchar2(40) ;
   ws_aplicativo             varchar2(20) ;
   ws_sid                    number(9) ;
   ws_empresa                number(3) ;
   ws_usuario_systextil      varchar2(250) ;
   ws_locale_usuario         varchar2(5) ;
   v_nome_programa           varchar2(20) ;


begin
-- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);


 v_nome_programa := inter_fn_nome_programa(ws_sid);


 if inserting
 then
    begin

        insert into LOJA_061_log (
           TIPO_OCORR,   /*0*/
           DATA_OCORR,   /*1*/
           HORA_OCORR,   /*2*/
           USUARIO_REDE,   /*3*/
           MAQUINA_REDE,   /*4*/
           APLICACAO,   /*5*/
           USUARIO_SISTEMA,   /*6*/
           NOME_PROGRAMA,   /*7*/
           DOCUMENTO_OLD,   /*8*/
           DOCUMENTO_NEW,   /*9*/
           CNPJ9_OLD,   /*10*/
           CNPJ9_NEW,   /*11*/
           CNPJ4_OLD,   /*12*/
           CNPJ4_NEW,   /*13*/
           CNPJ2_OLD,   /*14*/
           CNPJ2_NEW,   /*15*/
           SEQUENCIA_OLD,   /*16*/
           SEQUENCIA_NEW,   /*17*/
           TRANSACAO_OLD,   /*18*/
           TRANSACAO_NEW,   /*19*/
           NIVEL_OLD,   /*20*/
           NIVEL_NEW,   /*21*/
           GRUPO_OLD,   /*22*/
           GRUPO_NEW,   /*23*/
           SUBGRUPO_OLD,   /*24*/
           SUBGRUPO_NEW,   /*25*/
           ITEM_OLD,   /*26*/
           ITEM_NEW,   /*27*/
           DEPOSITO_OLD,   /*28*/
           DEPOSITO_NEW,   /*29*/
           LOTE_OLD,   /*30*/
           LOTE_NEW,   /*31*/
           QUANTIDADE_OLD,   /*32*/
           QUANTIDADE_NEW,   /*33*/
           SITUACAO_ITEM_OLD,   /*34*/
           SITUACAO_ITEM_NEW,   /*35*/
           OBSERVACAO_OLD,   /*36*/
           OBSERVACAO_NEW,   /*37*/
           DATA_MOVTO_OLD,   /*38*/
           DATA_MOVTO_NEW,   /*39*/
           VALOR_UNIT_OLD,   /*40*/
           VALOR_UNIT_NEW,   /*41*/
           SEQ_SAIDA_OLD,   /*42*/
           SEQ_SAIDA_NEW,   /*43*/
           CODIGO_EMPRESA_OLD,   /*44*/
           CODIGO_EMPRESA_NEW,   /*45*/
           PERIODO_OLD,   /*46*/
           PERIODO_NEW,   /*47*/
           ORDEM_PRODUCAO_OLD,   /*48*/
           ORDEM_PRODUCAO_NEW,   /*49*/
           ORDEM_CONF_OLD,   /*50*/
           ORDEM_CONF_NEW,   /*51*/
           SEQUENCIA_TAG_OLD,   /*52*/
           SEQUENCIA_TAG_NEW,   /*53*/
           ENTRADA_SAIDA_OLD,   /*54*/
           ENTRADA_SAIDA_NEW,   /*55*/
           NR_ORCAMENTO_LOJA_OLD,   /*56*/
           NR_ORCAMENTO_LOJA_NEW    /*57*/
        ) values (
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           0,/*8*/
           :new.DOCUMENTO, /*9*/
           0,/*10*/
           :new.CNPJ9, /*11*/
           0,/*12*/
           :new.CNPJ4, /*13*/
           0,/*14*/
           :new.CNPJ2, /*15*/
           0,/*16*/
           :new.SEQUENCIA, /*17*/
           0,/*18*/
           :new.TRANSACAO, /*19*/
           '',/*20*/
           :new.NIVEL, /*21*/
           '',/*22*/
           :new.GRUPO, /*23*/
           '',/*24*/
           :new.SUBGRUPO, /*25*/
           '',/*26*/
           :new.ITEM, /*27*/
           0,/*28*/
           :new.DEPOSITO, /*29*/
           0,/*30*/
           :new.LOTE, /*31*/
           0,/*32*/
           :new.QUANTIDADE, /*33*/
           0,/*34*/
           :new.SITUACAO_ITEM, /*35*/
           '',/*36*/
           :new.OBSERVACAO, /*37*/
           null,/*38*/
           :new.DATA_MOVTO, /*39*/
           0,/*40*/
           :new.VALOR_UNIT, /*41*/
           0,/*42*/
           :new.SEQ_SAIDA, /*43*/
           0,/*44*/
           :new.CODIGO_EMPRESA, /*45*/
           0,/*46*/
           :new.PERIODO, /*47*/
           0,/*48*/
           :new.ORDEM_PRODUCAO, /*49*/
           0,/*50*/
           :new.ORDEM_CONF, /*51*/
           0,/*52*/
           :new.SEQUENCIA_TAG, /*53*/
           '',/*54*/
           :new.ENTRADA_SAIDA, /*55*/
           0,/*56*/
           :new.NR_ORCAMENTO_LOJA /*57*/
         );
    end;
 end if;


 if updating
 then
    begin
        insert into LOJA_061_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           DOCUMENTO_OLD, /*8*/
           DOCUMENTO_NEW, /*9*/
           CNPJ9_OLD, /*10*/
           CNPJ9_NEW, /*11*/
           CNPJ4_OLD, /*12*/
           CNPJ4_NEW, /*13*/
           CNPJ2_OLD, /*14*/
           CNPJ2_NEW, /*15*/
           SEQUENCIA_OLD, /*16*/
           SEQUENCIA_NEW, /*17*/
           TRANSACAO_OLD, /*18*/
           TRANSACAO_NEW, /*19*/
           NIVEL_OLD, /*20*/
           NIVEL_NEW, /*21*/
           GRUPO_OLD, /*22*/
           GRUPO_NEW, /*23*/
           SUBGRUPO_OLD, /*24*/
           SUBGRUPO_NEW, /*25*/
           ITEM_OLD, /*26*/
           ITEM_NEW, /*27*/
           DEPOSITO_OLD, /*28*/
           DEPOSITO_NEW, /*29*/
           LOTE_OLD, /*30*/
           LOTE_NEW, /*31*/
           QUANTIDADE_OLD, /*32*/
           QUANTIDADE_NEW, /*33*/
           SITUACAO_ITEM_OLD, /*34*/
           SITUACAO_ITEM_NEW, /*35*/
           OBSERVACAO_OLD, /*36*/
           OBSERVACAO_NEW, /*37*/
           DATA_MOVTO_OLD, /*38*/
           DATA_MOVTO_NEW, /*39*/
           VALOR_UNIT_OLD, /*40*/
           VALOR_UNIT_NEW, /*41*/
           SEQ_SAIDA_OLD, /*42*/
           SEQ_SAIDA_NEW, /*43*/
           CODIGO_EMPRESA_OLD, /*44*/
           CODIGO_EMPRESA_NEW, /*45*/
           PERIODO_OLD, /*46*/
           PERIODO_NEW, /*47*/
           ORDEM_PRODUCAO_OLD, /*48*/
           ORDEM_PRODUCAO_NEW, /*49*/
           ORDEM_CONF_OLD, /*50*/
           ORDEM_CONF_NEW, /*51*/
           SEQUENCIA_TAG_OLD, /*52*/
           SEQUENCIA_TAG_NEW, /*53*/
           ENTRADA_SAIDA_OLD, /*54*/
           ENTRADA_SAIDA_NEW, /*55*/
           NR_ORCAMENTO_LOJA_OLD, /*56*/
           NR_ORCAMENTO_LOJA_NEW  /*57*/
        ) values (
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.DOCUMENTO,  /*8*/
           :new.DOCUMENTO, /*9*/
           :old.CNPJ9,  /*10*/
           :new.CNPJ9, /*11*/
           :old.CNPJ4,  /*12*/
           :new.CNPJ4, /*13*/
           :old.CNPJ2,  /*14*/
           :new.CNPJ2, /*15*/
           :old.SEQUENCIA,  /*16*/
           :new.SEQUENCIA, /*17*/
           :old.TRANSACAO,  /*18*/
           :new.TRANSACAO, /*19*/
           :old.NIVEL,  /*20*/
           :new.NIVEL, /*21*/
           :old.GRUPO,  /*22*/
           :new.GRUPO, /*23*/
           :old.SUBGRUPO,  /*24*/
           :new.SUBGRUPO, /*25*/
           :old.ITEM,  /*26*/
           :new.ITEM, /*27*/
           :old.DEPOSITO,  /*28*/
           :new.DEPOSITO, /*29*/
           :old.LOTE,  /*30*/
           :new.LOTE, /*31*/
           :old.QUANTIDADE,  /*32*/
           :new.QUANTIDADE, /*33*/
           :old.SITUACAO_ITEM,  /*34*/
           :new.SITUACAO_ITEM, /*35*/
           :old.OBSERVACAO,  /*36*/
           :new.OBSERVACAO, /*37*/
           :old.DATA_MOVTO,  /*38*/
           :new.DATA_MOVTO, /*39*/
           :old.VALOR_UNIT,  /*40*/
           :new.VALOR_UNIT, /*41*/
           :old.SEQ_SAIDA,  /*42*/
           :new.SEQ_SAIDA, /*43*/
           :old.CODIGO_EMPRESA,  /*44*/
           :new.CODIGO_EMPRESA, /*45*/
           :old.PERIODO,  /*46*/
           :new.PERIODO, /*47*/
           :old.ORDEM_PRODUCAO,  /*48*/
           :new.ORDEM_PRODUCAO, /*49*/
           :old.ORDEM_CONF,  /*50*/
           :new.ORDEM_CONF, /*51*/
           :old.SEQUENCIA_TAG,  /*52*/
           :new.SEQUENCIA_TAG, /*53*/
           :old.ENTRADA_SAIDA,  /*54*/
           :new.ENTRADA_SAIDA, /*55*/
           :old.NR_ORCAMENTO_LOJA,  /*56*/
           :new.NR_ORCAMENTO_LOJA  /*57*/
         );
    end;
 end if;


 if deleting
 then
    begin
        insert into LOJA_061_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           DOCUMENTO_OLD, /*8*/
           DOCUMENTO_NEW, /*9*/
           CNPJ9_OLD, /*10*/
           CNPJ9_NEW, /*11*/
           CNPJ4_OLD, /*12*/
           CNPJ4_NEW, /*13*/
           CNPJ2_OLD, /*14*/
           CNPJ2_NEW, /*15*/
           SEQUENCIA_OLD, /*16*/
           SEQUENCIA_NEW, /*17*/
           TRANSACAO_OLD, /*18*/
           TRANSACAO_NEW, /*19*/
           NIVEL_OLD, /*20*/
           NIVEL_NEW, /*21*/
           GRUPO_OLD, /*22*/
           GRUPO_NEW, /*23*/
           SUBGRUPO_OLD, /*24*/
           SUBGRUPO_NEW, /*25*/
           ITEM_OLD, /*26*/
           ITEM_NEW, /*27*/
           DEPOSITO_OLD, /*28*/
           DEPOSITO_NEW, /*29*/
           LOTE_OLD, /*30*/
           LOTE_NEW, /*31*/
           QUANTIDADE_OLD, /*32*/
           QUANTIDADE_NEW, /*33*/
           SITUACAO_ITEM_OLD, /*34*/
           SITUACAO_ITEM_NEW, /*35*/
           OBSERVACAO_OLD, /*36*/
           OBSERVACAO_NEW, /*37*/
           DATA_MOVTO_OLD, /*38*/
           DATA_MOVTO_NEW, /*39*/
           VALOR_UNIT_OLD, /*40*/
           VALOR_UNIT_NEW, /*41*/
           SEQ_SAIDA_OLD, /*42*/
           SEQ_SAIDA_NEW, /*43*/
           CODIGO_EMPRESA_OLD, /*44*/
           CODIGO_EMPRESA_NEW, /*45*/
           PERIODO_OLD, /*46*/
           PERIODO_NEW, /*47*/
           ORDEM_PRODUCAO_OLD, /*48*/
           ORDEM_PRODUCAO_NEW, /*49*/
           ORDEM_CONF_OLD, /*50*/
           ORDEM_CONF_NEW, /*51*/
           SEQUENCIA_TAG_OLD, /*52*/
           SEQUENCIA_TAG_NEW, /*53*/
           ENTRADA_SAIDA_OLD, /*54*/
           ENTRADA_SAIDA_NEW, /*55*/
           NR_ORCAMENTO_LOJA_OLD, /*56*/
           NR_ORCAMENTO_LOJA_NEW /*57*/
        ) values (
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede,/*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.DOCUMENTO, /*8*/
           0, /*9*/
           :old.CNPJ9, /*10*/
           0, /*11*/
           :old.CNPJ4, /*12*/
           0, /*13*/
           :old.CNPJ2, /*14*/
           0, /*15*/
           :old.SEQUENCIA, /*16*/
           0, /*17*/
           :old.TRANSACAO, /*18*/
           0, /*19*/
           :old.NIVEL, /*20*/
           '', /*21*/
           :old.GRUPO, /*22*/
           '', /*23*/
           :old.SUBGRUPO, /*24*/
           '', /*25*/
           :old.ITEM, /*26*/
           '', /*27*/
           :old.DEPOSITO, /*28*/
           0, /*29*/
           :old.LOTE, /*30*/
           0, /*31*/
           :old.QUANTIDADE, /*32*/
           0, /*33*/
           :old.SITUACAO_ITEM, /*34*/
           0, /*35*/
           :old.OBSERVACAO, /*36*/
           '', /*37*/
           :old.DATA_MOVTO, /*38*/
           null, /*39*/
           :old.VALOR_UNIT, /*40*/
           0, /*41*/
           :old.SEQ_SAIDA, /*42*/
           0, /*43*/
           :old.CODIGO_EMPRESA, /*44*/
           0, /*45*/
           :old.PERIODO, /*46*/
           0, /*47*/
           :old.ORDEM_PRODUCAO, /*48*/
           0, /*49*/
           :old.ORDEM_CONF, /*50*/
           0, /*51*/
           :old.SEQUENCIA_TAG, /*52*/
           0, /*53*/
           :old.ENTRADA_SAIDA, /*54*/
           '', /*55*/
           :old.NR_ORCAMENTO_LOJA, /*56*/
           0 /*57*/
         );
    end;
 end if;
end inter_tr_LOJA_061_log;

-- ALTER TRIGGER "INTER_TR_LOJA_061_LOG" ENABLE
 

/

exec inter_pr_recompile;

