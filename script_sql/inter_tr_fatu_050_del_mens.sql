
  CREATE OR REPLACE TRIGGER "INTER_TR_FATU_050_DEL_MENS" 
AFTER  DELETE

on fatu_050

for each row

begin

   delete from fatu_052
   where fatu_052.cod_empresa    = :old.codigo_empresa
     and fatu_052.num_nota       = :old.num_nota_fiscal
     and fatu_052.cod_serie_nota = :old.serie_nota_fisc;

end INTER_TR_FATU_050_DEL_MENS;

-- ALTER TRIGGER "INTER_TR_FATU_050_DEL_MENS" ENABLE
 

/

exec inter_pr_recompile;

