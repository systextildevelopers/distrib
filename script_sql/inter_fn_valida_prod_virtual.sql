CREATE OR REPLACE FUNCTION inter_fn_valida_prod_virtual (
    p_cod_estagio_agrupador_insu OBRF_015.cod_estagio_agrupador_insu%TYPE,
    p_cod_estagio_simultaneo_insu OBRF_015.cod_estagio_simultaneo_insu%TYPE
)
RETURN BOOLEAN
IS

BEGIN
    IF (p_cod_estagio_agrupador_insu + p_cod_estagio_simultaneo_insu) > 0 THEN
        RETURN FALSE;
    ELSE
        RETURN TRUE;
    END IF;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN FALSE;
    WHEN OTHERS THEN
        RETURN FALSE;
END inter_fn_valida_prod_virtual;
/
