create or replace view inter_vi_itens_sugeridos_fatu as
select p.pedido_venda, p.seq_item_pedido, p.cd_it_pe_nivel99, p.cd_it_pe_grupo, p.cd_it_pe_subgrupo, p.cd_it_pe_item,
       p.lote_empenhado, p.codigo_deposito, p.qtde_pedida, p.qtde_faturada, 
            p.qtde_sugerida,
            p.qtde_afaturar,
       p.situacao_fatu_it, p.cod_cancelamento,
       nvl(b.grupo_embarque,0) grupo_embarque,
       nvl((select 'S' from inter_vi_grupo_emb_cod_sug_100 i
            where i.pedido_venda = p.pedido_venda 
              and i.grupo_embarque = b.grupo_embarque),'N') sug_grupo_emb_100,
              (select nvl(max(c.seq_conjunto), 0) from fatu_780 c
        where c.pedido = p.pedido_venda
            and c.nivel = p.cd_it_pe_nivel99
            and c.grupo = p.cd_it_pe_grupo
            and c.subgrupo = p.cd_it_pe_subgrupo
            and c.item = p.cd_it_pe_item) seq_conjunto
from pedi_110 p, basi_590 b
where b.nivel (+) = p.cd_it_pe_nivel99
  and b.grupo (+) = p.cd_it_pe_grupo
  and b.subgrupo (+) = p.cd_it_pe_subgrupo
  and b.item (+) = p.cd_it_pe_item;
