create or replace function inter_fn_tp_prod_sped_recur (p_cod_empresa in number,
                                                           p_conta_estoque in number,
                                                           p_nivel in varchar2,
                                                           p_grupo in varchar2,
                                                           p_subgrupo in varchar2,
                                                           p_item in varchar2,
                                                           p_tipo_produto_sped in number,
                                                           p_cod_estagio_agrupador in number) return number is w_tipo_produto_sped number;


begin
   begin
       select oper_315.tipo_produto_sped
       into w_tipo_produto_sped
       from oper_315
       where oper_315.cod_empresa = p_cod_empresa
         and oper_315.conta_estoque = p_conta_estoque
         and oper_315.nivel = p_nivel
         and oper_315.grupo = p_grupo
         and oper_315.subgrupo = p_subgrupo
         and oper_315.item = p_item
         and oper_315.cod_estagio_agrupador_insu = p_cod_estagio_agrupador;
    exception
    when no_data_found then
       w_tipo_produto_sped := null;
    end;

    if w_tipo_produto_sped is null
    then
        begin
           select oper_315.tipo_produto_sped
           into w_tipo_produto_sped
           from oper_315
           where oper_315.cod_empresa = p_cod_empresa
             and oper_315.conta_estoque = p_conta_estoque
             and oper_315.nivel = p_nivel
             and oper_315.grupo = p_grupo
             and oper_315.subgrupo = p_subgrupo
             and oper_315.item     = 'XXXXXX'
             and oper_315.cod_estagio_agrupador_insu = p_cod_estagio_agrupador;
        exception
        when no_data_found then
           w_tipo_produto_sped := null;
        end;
    end if;

    if w_tipo_produto_sped is null
    then
       begin
           select oper_315.tipo_produto_sped
           into w_tipo_produto_sped
           from oper_315
           where oper_315.cod_empresa = p_cod_empresa
             and oper_315.conta_estoque = p_conta_estoque
             and oper_315.nivel = p_nivel
             and oper_315.grupo = p_grupo
             and oper_315.subgrupo = 'XXX'
             and oper_315.item     = 'XXXXXX'
             and oper_315.cod_estagio_agrupador_insu = p_cod_estagio_agrupador;
       exception
       when no_data_found then
           w_tipo_produto_sped := null;
        end;
    end if;

    if w_tipo_produto_sped is null
    then
        begin
           select oper_315.tipo_produto_sped
           into w_tipo_produto_sped
           from oper_315
           where oper_315.cod_empresa = p_cod_empresa
              and oper_315.conta_estoque = p_conta_estoque
              and oper_315.nivel = p_nivel
              and oper_315.grupo = 'XXXXX'
             and oper_315.subgrupo = 'XXX'
             and oper_315.item     = 'XXXXXX'
             and oper_315.cod_estagio_agrupador_insu = p_cod_estagio_agrupador;
        exception
        when no_data_found then
           w_tipo_produto_sped := null;
        end;
    end if;

    if w_tipo_produto_sped is null
    then
        begin
           select oper_315.tipo_produto_sped
           into w_tipo_produto_sped
           from oper_315
           where oper_315.cod_empresa = p_cod_empresa
              and oper_315.conta_estoque = p_conta_estoque
              and oper_315.nivel = 'X'
              and oper_315.grupo = 'XXXXX'
             and oper_315.subgrupo = 'XXX'
             and oper_315.item     = 'XXXXXX'
             and oper_315.cod_estagio_agrupador_insu = p_cod_estagio_agrupador;
        exception
        when no_data_found then
           w_tipo_produto_sped := p_tipo_produto_sped;
        end;
    end if;

    return w_tipo_produto_sped;

end inter_fn_tp_prod_sped_recur;
/
