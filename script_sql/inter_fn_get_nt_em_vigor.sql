CREATE OR REPLACE FUNCTION "INTER_FN_GET_NT_EM_VIGOR" (p_nota_tecnica in varchar2,
                                                       p_ambiente in numeric)
   return varchar2
is
   retorno varchar2(1);

begin
   begin
      select 'S' into retorno from obrf_165
      where obrf_165.cod_nota_tec = p_nota_tecnica
        and obrf_165.ambiente     = p_ambiente
        and obrf_165.data_vigor   <= sysdate;
   exception
      when no_data_found then 
         retorno := 'N';
   end;

   return retorno;
end;
