create synonym systextilrpt.crec_180_simula for crec_180_simula;
create synonym systextilrpt.loja_835 for loja_835;

comment on table loja_835 is 'Tabela para lançamento de sangrias ';
comment on table crec_180_simula is 'Tabela para simulações de Associação de Títulos à Cheques';
comment on column CREC_180_SIMULA.PROCESSO is 'Definir processos diferentes';
comment on column CREC_180_SIMULA.VLR_USADO_CH is 'Soma dos valores usados por cheques';
comment on column CREC_180_SIMULA.FLAG_SELECIONADO is 'Se está selecionado ou não, ou processado';
