alter table fatu_504 add qtde_dias_devolucao number(3) default 999;

comment on column fatu_504.qtde_dias_devolucao is 'Número de dias permitido fazer a devolução, desde a criação da requisição até o processo de devolução';
