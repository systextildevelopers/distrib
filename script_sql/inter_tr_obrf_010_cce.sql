
  CREATE OR REPLACE TRIGGER "INTER_TR_OBRF_010_CCE" 
before update of especie_docto,    qtde_volumes,
                 transpa_forne9,   transpa_forne4,
                 transpa_forne2,   natoper_nat_oper,
                 natoper_est_oper
on obrf_010
for each row
begin
   if :old.situacao_entrada = 1 and (:old.cod_status = '100' or :old.cod_status = '990')
   then :new.st_flag_cce := 1;
   end if;
end inter_tr_obrf_010_cce;
-- ALTER TRIGGER "INTER_TR_OBRF_010_CCE" ENABLE
 

/

exec inter_pr_recompile;

