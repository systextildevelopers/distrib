create or replace procedure wms_pr_gera_abc (p_cod_empresas   in varchar2, --separados por virgula
                                             p_data_entr_ini  in date,
                                             p_data_entr_fim  in date) is
BEGIN
   
   begin
      delete from inte_wms_abc;
   end;
   
   begin

      for f_produtos_abc in (      
         select pedi_110.cd_it_pe_nivel99 nivel,   pedi_110.cd_it_pe_grupo grupo,
                pedi_110.cd_it_pe_subgrupo sub,    pedi_110.cd_it_pe_item item,
                pedi_110.codigo_deposito deposito, sum(pedi_110.qtde_pedida) quantidade
         from pedi_110, pedi_100
         where pedi_110.pedido_venda            = pedi_100.pedido_venda
           and pedi_100.codigo_empresa          in (select  trim(nvl(substr( string_para_quebra,
                                                            decode(level, 1, 1, instr(string_para_quebra, delimitador, 1, level-1)+1), 
                                                            instr(string_para_quebra, delimitador, 1, level) - decode(level, 1, 1, instr(string_para_quebra, delimitador, 1, level-1)+1)),'9'))
                                                    from (select p_cod_empresas || ',' string_para_quebra,
                                                                 ','                      delimitador
                                                          from dual)
                                                    connect by instr(string_para_quebra, delimitador, 1, level)>0)
           and pedi_100.data_entr_venda between p_data_entr_ini and p_data_entr_fim
           and pedi_100.cod_cancelamento        = 0
           and pedi_100.situacao_venda         <> 10
           and pedi_100.tecido_peca             = '1'
           and pedi_110.cd_it_pe_nivel99        = '1'
           and pedi_110.cod_cancelamento        = 0
         group by pedi_110.cd_it_pe_nivel99,   pedi_110.cd_it_pe_grupo,
                  pedi_110.cd_it_pe_subgrupo,  pedi_110.cd_it_pe_item,
                  pedi_110.codigo_deposito)
      LOOP

         begin
   
            insert into inte_wms_abc
            ( nivel_sku,               grupo_sku,
              subgrupo_sku,            item_sku,
              cod_deposito,            quantidade
            )
            values
            ( f_produtos_abc.nivel,    f_produtos_abc.grupo,
              f_produtos_abc.sub,      f_produtos_abc.item,
              f_produtos_abc.deposito, f_produtos_abc.quantidade
            );
         exception
            when others then
               raise_application_error(-20000, 'N�o inseriu na tabela INTE_WMS_ABC.' || Chr(10) || SQLERRM);
         end;
   
         commit WORK;

      END LOOP;
   
   end;

END wms_pr_gera_abc;

/

exec inter_pr_recompile;

declare
  v_ver_oracle       number(9);
  v_comando_exclusao varchar(2000);
begin
   
   begin
   
      select nvl(to_number(substr(version, 1, Instr(version, '.', 1, 1)-1)), 11) versao
      into v_ver_oracle
      from product_component_version
      where upper(product_component_version.product) like 'ORACLE%';
   
   end;
   
   if v_ver_oracle < 10
   then 
      
      v_comando_exclusao := 'drop procedure wms_pr_gera_abc';
      
      EXECUTE IMMEDIATE v_comando_exclusao;
   
   end if;
   
   
end;

/

exec inter_pr_recompile;

/* versao: 1 */


 exit;
