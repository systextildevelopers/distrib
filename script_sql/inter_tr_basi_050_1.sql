
  CREATE OR REPLACE TRIGGER "INTER_TR_BASI_050_1" 
before insert or
       delete or
       update of consumo,  nivel_comp, grupo_comp,
                 sub_comp, item_comp,  estagio
on basi_050
for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   v_recalcular_referencia   fatu_503.recalcular_referencia%type;

begin

-- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);
   begin
      select fatu_503.recalcular_referencia
      into   v_recalcular_referencia
      from fatu_503
      where fatu_503.codigo_empresa = ws_empresa;
      exception when others then
         v_recalcular_referencia := 1;
   end;

   if inserting and v_recalcular_referencia = 0
   then
      inter_pr_verif_ordens(:new.nivel_item,
                            :new.grupo_item,
                            :new.sub_item,
                            :new.item_item);
   end if;
   if updating and v_recalcular_referencia = 0
   then
      inter_pr_verif_ordens(:new.nivel_comp,
                            :new.grupo_comp,
                            :new.sub_comp,
                            :new.item_comp);
   end if;

   if deleting and v_recalcular_referencia = 0
   then
      inter_pr_verif_ordens(:old.nivel_comp,
                            :old.grupo_comp,
                            :old.sub_comp,
                            :old.item_comp);
   end if;
end inter_tr_basi_050_1;


-- ALTER TRIGGER "INTER_TR_BASI_050_1" ENABLE
 

/

exec inter_pr_recompile;

