
  CREATE OR REPLACE FUNCTION "INTER_FN_CALC_TEMPO_OB" (p_ordem_beneficiamento in number)
RETURN number is
-- Variavel a ser retornada no calculo da funcao
   v_tempo_total_ob       number(15,4);
-- Maquina que sera enviada para o calculo
   v_grupo_maquina_ob     varchar2(4);
   v_subgrupo_maquina_ob  varchar2(3);
begin

  select pcpb_010.grupo_maquina, pcpb_010.subgrupo_maquina
  into   v_grupo_maquina_ob,     v_subgrupo_maquina_ob
  from pcpb_010
  where pcpb_010.ordem_producao = p_ordem_beneficiamento;

  v_tempo_total_ob := 0.0000;

  for reg_pcpb_020 in (select pcpb_020.pano_sbg_nivel99, pcpb_020.pano_sbg_grupo,
                              pcpb_020.pano_sbg_subgrupo, pcpb_020.pano_sbg_item,
                              pcpb_020.alternativa_item, pcpb_020.roteiro_opcional,
                              pcpb_020.qtde_quilos_prog
                       from pcpb_020
                       where pcpb_020.ordem_producao = p_ordem_beneficiamento)
  loop
     v_tempo_total_ob := v_tempo_total_ob + inter_fn_calc_tempo_tec(reg_pcpb_020.pano_sbg_nivel99,
                                                                    reg_pcpb_020.pano_sbg_grupo,
                                                                    reg_pcpb_020.pano_sbg_subgrupo,
                                                                    reg_pcpb_020.pano_sbg_item,
                                                                    reg_pcpb_020.alternativa_item,
                                                                    reg_pcpb_020.roteiro_opcional,
                                                                    v_grupo_maquina_ob,
                                                                    v_subgrupo_maquina_ob,
                                                                    reg_pcpb_020.qtde_quilos_prog);
  end loop;

  return(v_tempo_total_ob);
end inter_fn_calc_tempo_ob;
 

/

exec inter_pr_recompile;

