
  CREATE OR REPLACE TRIGGER "INTER_TR_PEDI_101_HIST" 
   after insert
   or delete
   or update of
	   num_pedido, cod_mensagem, seq_mensagem, ind_local,
	   des_mensagem1, des_mensagem2, des_mensagem3, des_mensagem4,
	   des_mensagem5, des_mensagem6, des_mensagem7, des_mensagem8,
	   des_mensagem9, des_mensagem10, ind_msg_pedido_solicitacao, executa_trigger
   on pedi_101
   for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   long_aux                  varchar2(4000);
   v_executa_trigger         number(1);
begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then
      if inserting
      then
         INSERT INTO hist_100
            ( tabela,            operacao,
              data_ocorr,        usuario_rede,
              maquina_rede,      num01,
              aplicacao,         long01
            )
         VALUES
           ( 'PEDI_101',        'I',
             sysdate,           ws_usuario_rede,
             ws_maquina_rede,  :new.num_pedido,
             ws_aplicativo,

             'PEDIDO VENDA: '                          || :new.num_pedido     || chr(10) ||
             'CODIGO MENSAGEM: '                       || :new.cod_mensagem   || chr(10) ||
             'SEQUENCIA MENSAGEM: '                    || :new.seq_mensagem   || chr(10) ||
             'LOCAL MENSAGEM: '                        || :new.ind_local      || chr(10) ||
             'MENSAGEM 1: '                            || :new.des_mensagem1  || chr(10) ||
             'MENSAGEM 2: '                            || :new.des_mensagem2  || chr(10) ||
             'MENSAGEM 3: '                            || :new.des_mensagem3  || chr(10) ||
             'MENSAGEM 4: '                            || :new.des_mensagem4  || chr(10) ||
             'MENSAGEM 5: '                            || :new.des_mensagem5  || chr(10) ||
             'MENSAGEM 6: '                            || :new.des_mensagem6  || chr(10) ||
             'MENSAGEM 7: '                            || :new.des_mensagem7  || chr(10) ||
             'MENSAGEM 8: '                            || :new.des_mensagem8  || chr(10) ||
             'MENSAGEM 9: '                            || :new.des_mensagem9  || chr(10) ||
             'MENSAGEM 10: '                           || :new.des_mensagem10 || chr(10) ||
             'INDICA MENSAGEM PEDIDO OU SOLICITACAO: ' || :new.ind_msg_pedido_solicitacao
           );
      end if;

      if updating and
         (:new.num_pedido                 <> :old.num_pedido     or
          :new.cod_mensagem               <> :old.cod_mensagem   or
          :new.seq_mensagem               <> :old.seq_mensagem   or
          :new.ind_local                  <> :old.ind_local      or
          :new.des_mensagem1              <> :old.des_mensagem1  or
          :new.des_mensagem2              <> :old.des_mensagem2  or
          :new.des_mensagem3              <> :old.des_mensagem3  or
          :new.des_mensagem4              <> :old.des_mensagem4  or
          :new.des_mensagem5              <> :old.des_mensagem5  or
          :new.des_mensagem6              <> :old.des_mensagem6  or
          :new.des_mensagem7              <> :old.des_mensagem7  or
          :new.des_mensagem8              <> :old.des_mensagem8  or
          :new.des_mensagem9              <> :old.des_mensagem9  or
          :new.des_mensagem10             <> :old.des_mensagem10 or
          :new.ind_msg_pedido_solicitacao <> :old.ind_msg_pedido_solicitacao)
      then

         if :new.num_pedido <> :old.num_pedido
         then
            long_aux := long_aux || 'NUMERO PEDIDO: '
                                 || :old.num_pedido     || ' -> '
                                 || :new.num_pedido     ||
            chr(10);
         end if;

         if :new.cod_mensagem <> :old.cod_mensagem
         then
            long_aux := long_aux || 'CODIGO MENSAGEM: '
                                 || :old.cod_mensagem     || ' -> '
                                 || :new.cod_mensagem     ||
            chr(10);
         end if;

         if :new.seq_mensagem <> :old.seq_mensagem
         then
            long_aux := long_aux || 'SEQUENCIA MENSAGEM: '
                                 || :old.seq_mensagem     || ' -> '
                                 || :new.seq_mensagem     ||
            chr(10);
         end if;

         if :new.ind_local <> :old.ind_local
         then
            long_aux := long_aux || 'CODIGO MENSAGEM: '
                                 || :old.ind_local     || ' -> '
                                 || :new.ind_local     ||
            chr(10);
         end if;

         if :new.des_mensagem1 <> :old.des_mensagem1
         then
            long_aux := long_aux || 'MENSAGEM 1: '
                                 || :old.des_mensagem1     || ' -> '
                                 || :new.des_mensagem1     ||
            chr(10);
         end if;

         if :new.des_mensagem2 <> :old.des_mensagem2
         then
            long_aux := long_aux || 'MENSAGEM 2: '
                                 || :old.des_mensagem2     || ' -> '
                                 || :new.des_mensagem2     ||
            chr(10);
         end if;

         if :new.des_mensagem3 <> :old.des_mensagem3
         then
            long_aux := long_aux || 'MENSAGEM 3: '
                                 || :old.des_mensagem3     || ' -> '
                                 || :new.des_mensagem3     ||
            chr(10);
         end if;

         if :new.des_mensagem4 <> :old.des_mensagem4
         then
            long_aux := long_aux || 'MENSAGEM 4: '
                                 || :old.des_mensagem4     || ' -> '
                                 || :new.des_mensagem4     ||
            chr(10);
         end if;

         if :new.des_mensagem5 <> :old.des_mensagem5
         then
            long_aux := long_aux || 'MENSAGEM 5: '
                                 || :old.des_mensagem5    || ' -> '
                                 || :new.des_mensagem5    ||
            chr(10);
         end if;

         if :new.des_mensagem6 <> :old.des_mensagem6
         then
            long_aux := long_aux || 'MENSAGEM 6: '
                                 || :old.des_mensagem6    || ' -> '
                                 || :new.des_mensagem6    ||
            chr(10);
         end if;

         if :new.des_mensagem7 <> :old.des_mensagem7
         then
            long_aux := long_aux || 'MENSAGEM 7: '
                                 || :old.des_mensagem7    || ' -> '
                                 || :new.des_mensagem7    ||
            chr(10);
         end if;

         if :new.des_mensagem8 <> :old.des_mensagem8
         then
            long_aux := long_aux || 'MENSAGEM 8: '
                                 || :old.des_mensagem8    || ' -> '
                                 || :new.des_mensagem8    ||
            chr(10);
         end if;


         if :new.des_mensagem9 <> :old.des_mensagem9
         then
            long_aux := long_aux || 'MENSAGEM 9: '
                                 || :old.des_mensagem9    || ' -> '
                                 || :new.des_mensagem9    ||
            chr(10);
         end if;

         if :new.des_mensagem10 <> :old.des_mensagem10
         then
            long_aux := long_aux || 'MENSAGEM 10: '
                                 || :old.des_mensagem10    || ' -> '
                                 || :new.des_mensagem10    ||
            chr(10);
         end if;

         if :new.ind_msg_pedido_solicitacao <> :old.ind_msg_pedido_solicitacao
         then
            long_aux := long_aux || 'INDICA MENSAGEM PEDIDO OU SOLICITACAO: '
                                 || :old.ind_msg_pedido_solicitacao    || ' -> '
                                 || :new.ind_msg_pedido_solicitacao    ||
            chr(10);
         end if;

         /******* FAZ INSERT ******/

         INSERT INTO hist_100
            ( tabela,            operacao,
              data_ocorr,        usuario_rede,
              maquina_rede,      num01,
              aplicacao,         long01
            )
         VALUES
            ( 'PEDI_101',        'A',
              sysdate,           ws_usuario_rede,
              ws_maquina_rede,   :new.num_pedido,
              ws_aplicativo,     long_aux
           );

      end if;

      if deleting
      then
         INSERT INTO hist_100
            ( tabela,            operacao,
              data_ocorr,        usuario_rede,
              maquina_rede,      num01,
              aplicacao,         long01
            )
         VALUES
            ( 'PEDI_101',        'D',
              sysdate,           ws_usuario_rede,
              ws_maquina_rede,  :old.num_pedido,
              ws_aplicativo,

              'PEDIDO VENDA: '                          || :old.num_pedido     || chr(10) ||
              'CODIGO MENSAGEM: '                       || :old.cod_mensagem   || chr(10) ||
              'SEQUENCIA MENSAGEM: '                    || :old.seq_mensagem   || chr(10) ||
              'LOCAL MENSAGEM: '                        || :old.ind_local      || chr(10) ||
              'MENSAGEM 1: '                            || :old.des_mensagem1  || chr(10) ||
              'MENSAGEM 2: '                            || :old.des_mensagem2  || chr(10) ||
              'MENSAGEM 3: '                            || :old.des_mensagem3  || chr(10) ||
              'MENSAGEM 4: '                            || :old.des_mensagem4  || chr(10) ||
              'MENSAGEM 5: '                            || :old.des_mensagem5  || chr(10) ||
              'MENSAGEM 6: '                            || :old.des_mensagem6  || chr(10) ||
              'MENSAGEM 7: '                            || :old.des_mensagem7  || chr(10) ||
              'MENSAGEM 8: '                            || :old.des_mensagem8  || chr(10) ||
              'MENSAGEM 9: '                            || :old.des_mensagem9  || chr(10) ||
              'MENSAGEM 10: '                           || :old.des_mensagem10 || chr(10) ||
              'INDICA MENSAGEM PEDIDO OU SOLICITACAO: ' || :old.ind_msg_pedido_solicitacao
            );
      end if;

   end if;

end inter_tr_pedi_101_log;
-- ALTER TRIGGER "INTER_TR_PEDI_101_HIST" ENABLE
 

/

exec inter_pr_recompile;

