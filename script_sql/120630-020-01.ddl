alter table oper_275 add ind_considera_ger_dup_xml varchar2(1) default 'N';

comment on column oper_275.ind_considera_ger_dup_xml IS 'Indica se considera duplicatas geradas pelo xml';

exec inter_pr_recompile;
