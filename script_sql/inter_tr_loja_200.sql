
  CREATE OR REPLACE TRIGGER "INTER_TR_LOJA_200" 
  before insert or
         delete or
         update of cred_flag, cred_val_solic, cred_val_liber, cred_usu_liber, situacao on loja_200
  for each row
declare
   v_qtde_liberador number(2);
begin
   if inserting
   then

      if :new.cred_flag = 1
      then
         --------------------------------------------------------
         -- Checa se tem algum usuario apto a liberar o codigo 97
         --------------------------------------------------------
         select count(*) into v_qtde_liberador from hdoc_100
         where hdoc_100.area_bloqueio      = 9
           and hdoc_100.sit_bloqueio       = 97 -- BLOQUEADO PELO FINANCEIRO
           and hdoc_100.usubloq_empr_usu   = :new.cod_empresa
           and hdoc_100.situacao_liberador = 0;

         if v_qtde_liberador > 0
         then

            ------------------------------------------------
            -- Insere bloqueio 97 automaticamente
            ------------------------------------------------
            insert into loja_235
            (cod_empresa     ,      documento      ,
             seq_situacao    ,      cod_situacao   ,
             data_situacao   ,      flag_liberacao ,
             data_liberacao  ,      responsavel)
            values
            (:new.cod_empresa,      :new.documento ,
             1               ,      97             ,
             sysdate         ,      'N'            ,
             null            ,      ''
            );

            :new.situacao := 9; -- Entra como bloqueado

         end if;

      end if;

   end if;

   if updating
   then


     -----------------------------------------------------------
     --- Muda situacao do checkout para faturado
     -----------------------------------------------------------
     if :new.situacao = 3 --Faturado
     then
        BEGIN
          update loja_005
          set loja_005.situacao = 5
          where loja_005.pedido_loja = :new.documento
          and   loja_005.empresa   = :new.cod_empresa;
        EXCEPTION
           WHEN no_data_found then
                null;
           WHEN others then
                raise_application_error(-20000,'Erro atualizar situacao do Chekout para faturado.\n'||SQLERRM);
        END;
     end if;



      if :old.cred_flag = 0 and :new.cred_flag = 1
      then
         --------------------------------------------------------
         -- Checa se tem algum usuario apto a liberar o codigo 97
         --------------------------------------------------------
         select count(*) into v_qtde_liberador from hdoc_100
         where hdoc_100.area_bloqueio      = 9
           and hdoc_100.sit_bloqueio       = 97 -- BLOQUEADO PELO FINANCEIRO
           and hdoc_100.usubloq_empr_usu   = :new.cod_empresa
           and hdoc_100.situacao_liberador = 0;

         if v_qtde_liberador > 0
         then

            select count(*) into v_qtde_liberador from loja_235
            where cod_empresa = :new.cod_empresa
              and documento   = :new.documento
              and cod_situacao = 97;

            if v_qtde_liberador = 0
            then
               ------------------------------------------------
               -- Insere bloqueio 97 automaticamente
               ------------------------------------------------
               insert into loja_235
               (cod_empresa     ,      documento      ,
                seq_situacao    ,      cod_situacao   ,
                data_situacao   ,      flag_liberacao ,
                data_liberacao  ,      responsavel)
               values
               (:new.cod_empresa,      :new.documento ,
                1               ,      97             ,
                sysdate         ,      'N'            ,
                null            ,      ''
               );

               :new.situacao := 9; -- Muda para bloqueado
            else

               update loja_235
               set loja_235.data_situacao  = sysdate,
                   loja_235.flag_liberacao = 'N',
                   loja_235.data_liberacao = null,
                   loja_235.responsavel    = ''
               where cod_empresa = :new.cod_empresa
                 and documento   = :new.documento
                 and cod_situacao = 97;

              :new.situacao := 9; -- Muda para bloqueado

            end if;

         end if;

      end if;

   end if;

end;

-- ALTER TRIGGER "INTER_TR_LOJA_200" ENABLE
 

/

exec inter_pr_recompile;

