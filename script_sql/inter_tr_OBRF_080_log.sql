create or replace trigger inter_tr_OBRF_080_log 
after insert or delete or update 
on OBRF_080 
for each row 
declare 
   ws_usuario_rede           varchar2(250) ; 
   ws_maquina_rede           varchar2(40) ; 
   ws_aplicativo             varchar2(20) ; 
   ws_sid                    number(9) ; 
   ws_empresa                number(3) ; 
   ws_usuario_systextil      varchar2(250) ; 
   ws_locale_usuario         varchar2(5) ; 
   v_nome_programa           varchar2(20) ; 


begin
-- Dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid, 
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario); 

 
     -- begin 
    --    select hdoc_090.programa 
    --    into v_nome_programa 
    --    from hdoc_090 
    --    where hdoc_090.sid = ws_sid 
    --      and rownum       = 1 
    --      and hdoc_090.programa not like '%menu%' 
    --      and hdoc_090.programa not like '%_m%'; 
    --    exception 
    --      when no_data_found then            v_nome_programa := 'SQL'; 
    -- end; 
 
    v_nome_programa := inter_fn_nome_programa(ws_sid);
 
 if inserting 
 then 
    begin 
 
        insert into OBRF_080_log (
           TIPO_OCORR,   /*0*/ 
           DATA_OCORR,   /*1*/ 
           HORA_OCORR,   /*2*/ 
           USUARIO_REDE,   /*3*/ 
           MAQUINA_REDE,   /*4*/ 
           APLICACAO,   /*5*/ 
           USUARIO_SISTEMA,   /*6*/ 
           NOME_PROGRAMA,   /*7*/ 
           numero_ordem_OLD,   /*8*/ 
           numero_ordem_NEW,   /*9*/ 
           cgcterc_forne9_OLD,   /*10*/ 
           cgcterc_forne9_NEW,   /*11*/ 
           cgcterc_forne4_OLD,   /*12*/ 
           cgcterc_forne4_NEW,   /*13*/ 
           cgcterc_forne2_OLD,   /*14*/ 
           cgcterc_forne2_NEW,   /*15*/ 
           data_emissao_OLD,   /*16*/ 
           data_emissao_NEW,   /*17*/ 
           cod_canc_ordem_OLD,   /*18*/ 
           cod_canc_ordem_NEW,   /*19*/ 
           codigo_servico_OLD,   /*20*/ 
           codigo_servico_NEW,   /*21*/ 
           cod_tabela_serv_OLD,   /*22*/ 
           cod_tabela_serv_NEW,   /*23*/ 
           cod_tabela_mes_OLD,   /*24*/ 
           cod_tabela_mes_NEW,   /*25*/ 
           cod_tabela_seq_OLD,   /*26*/ 
           cod_tabela_seq_NEW,   /*27*/ 
           observacao_01_OLD,   /*28*/ 
           observacao_01_NEW,   /*29*/ 
           observacao_02_OLD,   /*30*/ 
           observacao_02_NEW,   /*31*/ 
           situacao_ordem_OLD,   /*32*/ 
           situacao_ordem_NEW,   /*33*/ 
           data_entrega_OLD,   /*34*/ 
           data_entrega_NEW,   /*35*/ 
           transacao_OLD,   /*36*/ 
           transacao_NEW,   /*37*/ 
           peso_enviado_OLD,   /*38*/ 
           peso_enviado_NEW,   /*39*/ 
           peso_recebido_OLD,   /*40*/ 
           peso_recebido_NEW,   /*41*/ 
           observacao_03_OLD,   /*42*/ 
           observacao_03_NEW,   /*43*/ 
           observacao_04_OLD,   /*44*/ 
           observacao_04_NEW,   /*45*/ 
           observacao_old_OLD,   /*46*/ 
           observacao_old_NEW,   /*47*/ 
           observacao_OLD,   /*48*/ 
           observacao_NEW,   /*49*/ 
           moeda_OLD,   /*50*/ 
           moeda_NEW,   /*51*/ 
           cond_pgto_OLD,   /*52*/ 
           cond_pgto_NEW,   /*53*/ 
           executa_trigger_OLD,   /*54*/ 
           executa_trigger_NEW,   /*55*/ 
           exec_alimentacao_autom_OLD,   /*56*/ 
           exec_alimentacao_autom_NEW,   /*57*/ 
           empresa_req_OLD,   /*58*/ 
           empresa_req_NEW,   /*59*/ 
           num_requisicao_OLD,   /*60*/ 
           num_requisicao_NEW,   /*61*/ 
           sol_requisicao_OLD,   /*62*/ 
           sol_requisicao_NEW,   /*63*/ 
           status_requisicao_OLD,   /*64*/ 
           status_requisicao_NEW,   /*65*/ 
           agrupamento_estq_vertical_OLD,   /*66*/ 
           agrupamento_estq_vertical_NEW,   /*67*/ 
           numero_ordem_origem_OLD,   /*68*/ 
           numero_ordem_origem_NEW,   /*69*/ 
           sequencia_origem_OLD,   /*70*/ 
           sequencia_origem_NEW,   /*71*/ 
           sit_fechamento_OLD,   /*72*/ 
           sit_fechamento_NEW,   /*73*/ 
           data_fechamento_OLD,   /*74*/ 
           data_fechamento_NEW,   /*75*/ 
           situacao_impressao_OLD,   /*76*/ 
           situacao_impressao_NEW    /*77*/
        ) values (    
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/ 
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           0,/*8*/
           :new.numero_ordem, /*9*/   
           0,/*10*/
           :new.cgcterc_forne9, /*11*/   
           0,/*12*/
           :new.cgcterc_forne4, /*13*/   
           0,/*14*/
           :new.cgcterc_forne2, /*15*/   
           null,/*16*/
           :new.data_emissao, /*17*/   
           0,/*18*/
           :new.cod_canc_ordem, /*19*/   
           0,/*20*/
           :new.codigo_servico, /*21*/   
           0,/*22*/
           :new.cod_tabela_serv, /*23*/   
           0,/*24*/
           :new.cod_tabela_mes, /*25*/   
           0,/*26*/
           :new.cod_tabela_seq, /*27*/   
           '',/*28*/
           :new.observacao_01, /*29*/   
           '',/*30*/
           :new.observacao_02, /*31*/   
           0,/*32*/
           :new.situacao_ordem, /*33*/   
           null,/*34*/
           :new.data_entrega, /*35*/   
           0,/*36*/
           :new.transacao, /*37*/   
           0,/*38*/
           :new.peso_enviado, /*39*/   
           0,/*40*/
           :new.peso_recebido, /*41*/   
           '',/*42*/
           :new.observacao_03, /*43*/   
           '',/*44*/
           :new.observacao_04, /*45*/   
           '',/*46*/
           :new.observacao_old, /*47*/   
           '',/*48*/
           :new.observacao, /*49*/   
           0,/*50*/
           :new.moeda, /*51*/   
           0,/*52*/
           :new.cond_pgto, /*53*/   
           0,/*54*/
           :new.executa_trigger, /*55*/   
           0,/*56*/
           :new.exec_alimentacao_autom, /*57*/   
           0,/*58*/
           :new.empresa_req, /*59*/   
           0,/*60*/
           :new.num_requisicao, /*61*/   
           0,/*62*/
           :new.sol_requisicao, /*63*/   
           0,/*64*/
           :new.status_requisicao, /*65*/   
           0,/*66*/
           :new.agrupamento_estoque_vertical, /*67*/   
           0,/*68*/
           :new.numero_ordem_origem, /*69*/   
           0,/*70*/
           :new.sequencia_origem, /*71*/   
           0,/*72*/
           :new.sit_fechamento, /*73*/   
           null,/*74*/
           :new.data_fechamento, /*75*/   
           0,/*76*/
           :new.situacao_impressao /*77*/   
         );    
    end;    
 end if;    
  
  
 if updating 
 then 
    begin 
        insert into OBRF_080_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           numero_ordem_OLD, /*8*/   
           numero_ordem_NEW, /*9*/   
           cgcterc_forne9_OLD, /*10*/   
           cgcterc_forne9_NEW, /*11*/   
           cgcterc_forne4_OLD, /*12*/   
           cgcterc_forne4_NEW, /*13*/   
           cgcterc_forne2_OLD, /*14*/   
           cgcterc_forne2_NEW, /*15*/   
           data_emissao_OLD, /*16*/   
           data_emissao_NEW, /*17*/   
           cod_canc_ordem_OLD, /*18*/   
           cod_canc_ordem_NEW, /*19*/   
           codigo_servico_OLD, /*20*/   
           codigo_servico_NEW, /*21*/   
           cod_tabela_serv_OLD, /*22*/   
           cod_tabela_serv_NEW, /*23*/   
           cod_tabela_mes_OLD, /*24*/   
           cod_tabela_mes_NEW, /*25*/   
           cod_tabela_seq_OLD, /*26*/   
           cod_tabela_seq_NEW, /*27*/   
           observacao_01_OLD, /*28*/   
           observacao_01_NEW, /*29*/   
           observacao_02_OLD, /*30*/   
           observacao_02_NEW, /*31*/   
           situacao_ordem_OLD, /*32*/   
           situacao_ordem_NEW, /*33*/   
           data_entrega_OLD, /*34*/   
           data_entrega_NEW, /*35*/   
           transacao_OLD, /*36*/   
           transacao_NEW, /*37*/   
           peso_enviado_OLD, /*38*/   
           peso_enviado_NEW, /*39*/   
           peso_recebido_OLD, /*40*/   
           peso_recebido_NEW, /*41*/   
           observacao_03_OLD, /*42*/   
           observacao_03_NEW, /*43*/   
           observacao_04_OLD, /*44*/   
           observacao_04_NEW, /*45*/   
           observacao_old_OLD, /*46*/   
           observacao_old_NEW, /*47*/   
           observacao_OLD, /*48*/   
           observacao_NEW, /*49*/   
           moeda_OLD, /*50*/   
           moeda_NEW, /*51*/   
           cond_pgto_OLD, /*52*/   
           cond_pgto_NEW, /*53*/   
           executa_trigger_OLD, /*54*/   
           executa_trigger_NEW, /*55*/   
           exec_alimentacao_autom_OLD, /*56*/   
           exec_alimentacao_autom_NEW, /*57*/   
           empresa_req_OLD, /*58*/   
           empresa_req_NEW, /*59*/   
           num_requisicao_OLD, /*60*/   
           num_requisicao_NEW, /*61*/   
           sol_requisicao_OLD, /*62*/   
           sol_requisicao_NEW, /*63*/   
           status_requisicao_OLD, /*64*/   
           status_requisicao_NEW, /*65*/   
           agrupamento_estq_vertical_OLD, /*66*/   
           agrupamento_estq_vertical_NEW, /*67*/   
           numero_ordem_origem_OLD, /*68*/   
           numero_ordem_origem_NEW, /*69*/   
           sequencia_origem_OLD, /*70*/   
           sequencia_origem_NEW, /*71*/   
           sit_fechamento_OLD, /*72*/   
           sit_fechamento_NEW, /*73*/   
           data_fechamento_OLD, /*74*/   
           data_fechamento_NEW, /*75*/   
           situacao_impressao_OLD, /*76*/   
           situacao_impressao_NEW  /*77*/  
        ) values (    
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/ 
           :old.numero_ordem,  /*8*/  
           :new.numero_ordem, /*9*/   
           :old.cgcterc_forne9,  /*10*/  
           :new.cgcterc_forne9, /*11*/   
           :old.cgcterc_forne4,  /*12*/  
           :new.cgcterc_forne4, /*13*/   
           :old.cgcterc_forne2,  /*14*/  
           :new.cgcterc_forne2, /*15*/   
           :old.data_emissao,  /*16*/  
           :new.data_emissao, /*17*/   
           :old.cod_canc_ordem,  /*18*/  
           :new.cod_canc_ordem, /*19*/   
           :old.codigo_servico,  /*20*/  
           :new.codigo_servico, /*21*/   
           :old.cod_tabela_serv,  /*22*/  
           :new.cod_tabela_serv, /*23*/   
           :old.cod_tabela_mes,  /*24*/  
           :new.cod_tabela_mes, /*25*/   
           :old.cod_tabela_seq,  /*26*/  
           :new.cod_tabela_seq, /*27*/   
           :old.observacao_01,  /*28*/  
           :new.observacao_01, /*29*/   
           :old.observacao_02,  /*30*/  
           :new.observacao_02, /*31*/   
           :old.situacao_ordem,  /*32*/  
           :new.situacao_ordem, /*33*/   
           :old.data_entrega,  /*34*/  
           :new.data_entrega, /*35*/   
           :old.transacao,  /*36*/  
           :new.transacao, /*37*/   
           :old.peso_enviado,  /*38*/  
           :new.peso_enviado, /*39*/   
           :old.peso_recebido,  /*40*/  
           :new.peso_recebido, /*41*/   
           :old.observacao_03,  /*42*/  
           :new.observacao_03, /*43*/   
           :old.observacao_04,  /*44*/  
           :new.observacao_04, /*45*/   
           :old.observacao_old,  /*46*/  
           :new.observacao_old, /*47*/   
           :old.observacao,  /*48*/  
           :new.observacao, /*49*/   
           :old.moeda,  /*50*/  
           :new.moeda, /*51*/   
           :old.cond_pgto,  /*52*/  
           :new.cond_pgto, /*53*/   
           :old.executa_trigger,  /*54*/  
           :new.executa_trigger, /*55*/   
           :old.exec_alimentacao_autom,  /*56*/  
           :new.exec_alimentacao_autom, /*57*/   
           :old.empresa_req,  /*58*/  
           :new.empresa_req, /*59*/   
           :old.num_requisicao,  /*60*/  
           :new.num_requisicao, /*61*/   
           :old.sol_requisicao,  /*62*/  
           :new.sol_requisicao, /*63*/   
           :old.status_requisicao,  /*64*/  
           :new.status_requisicao, /*65*/   
           :old.agrupamento_estoque_vertical,  /*66*/  
           :new.agrupamento_estoque_vertical, /*67*/   
           :old.numero_ordem_origem,  /*68*/  
           :new.numero_ordem_origem, /*69*/   
           :old.sequencia_origem,  /*70*/  
           :new.sequencia_origem, /*71*/   
           :old.sit_fechamento,  /*72*/  
           :new.sit_fechamento, /*73*/   
           :old.data_fechamento,  /*74*/  
           :new.data_fechamento, /*75*/   
           :old.situacao_impressao,  /*76*/  
           :new.situacao_impressao  /*77*/  
         );    
    end;    
 end if;    
  
  
 if deleting 
 then 
    begin 
        insert into OBRF_080_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           numero_ordem_OLD, /*8*/   
           numero_ordem_NEW, /*9*/   
           cgcterc_forne9_OLD, /*10*/   
           cgcterc_forne9_NEW, /*11*/   
           cgcterc_forne4_OLD, /*12*/   
           cgcterc_forne4_NEW, /*13*/   
           cgcterc_forne2_OLD, /*14*/   
           cgcterc_forne2_NEW, /*15*/   
           data_emissao_OLD, /*16*/   
           data_emissao_NEW, /*17*/   
           cod_canc_ordem_OLD, /*18*/   
           cod_canc_ordem_NEW, /*19*/   
           codigo_servico_OLD, /*20*/   
           codigo_servico_NEW, /*21*/   
           cod_tabela_serv_OLD, /*22*/   
           cod_tabela_serv_NEW, /*23*/   
           cod_tabela_mes_OLD, /*24*/   
           cod_tabela_mes_NEW, /*25*/   
           cod_tabela_seq_OLD, /*26*/   
           cod_tabela_seq_NEW, /*27*/   
           observacao_01_OLD, /*28*/   
           observacao_01_NEW, /*29*/   
           observacao_02_OLD, /*30*/   
           observacao_02_NEW, /*31*/   
           situacao_ordem_OLD, /*32*/   
           situacao_ordem_NEW, /*33*/   
           data_entrega_OLD, /*34*/   
           data_entrega_NEW, /*35*/   
           transacao_OLD, /*36*/   
           transacao_NEW, /*37*/   
           peso_enviado_OLD, /*38*/   
           peso_enviado_NEW, /*39*/   
           peso_recebido_OLD, /*40*/   
           peso_recebido_NEW, /*41*/   
           observacao_03_OLD, /*42*/   
           observacao_03_NEW, /*43*/   
           observacao_04_OLD, /*44*/   
           observacao_04_NEW, /*45*/   
           observacao_old_OLD, /*46*/   
           observacao_old_NEW, /*47*/   
           observacao_OLD, /*48*/   
           observacao_NEW, /*49*/   
           moeda_OLD, /*50*/   
           moeda_NEW, /*51*/   
           cond_pgto_OLD, /*52*/   
           cond_pgto_NEW, /*53*/   
           executa_trigger_OLD, /*54*/   
           executa_trigger_NEW, /*55*/   
           exec_alimentacao_autom_OLD, /*56*/   
           exec_alimentacao_autom_NEW, /*57*/   
           empresa_req_OLD, /*58*/   
           empresa_req_NEW, /*59*/   
           num_requisicao_OLD, /*60*/   
           num_requisicao_NEW, /*61*/   
           sol_requisicao_OLD, /*62*/   
           sol_requisicao_NEW, /*63*/   
           status_requisicao_OLD, /*64*/   
           status_requisicao_NEW, /*65*/   
           agrupamento_estq_vertical_OLD, /*66*/   
           agrupamento_estq_vertical_NEW, /*67*/   
           numero_ordem_origem_OLD, /*68*/   
           numero_ordem_origem_NEW, /*69*/   
           sequencia_origem_OLD, /*70*/   
           sequencia_origem_NEW, /*71*/   
           sit_fechamento_OLD, /*72*/   
           sit_fechamento_NEW, /*73*/   
           data_fechamento_OLD, /*74*/   
           data_fechamento_NEW, /*75*/   
           situacao_impressao_OLD, /*76*/   
           situacao_impressao_NEW /*77*/   
        ) values (    
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede,/*4*/ 
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           :old.numero_ordem, /*8*/   
           0, /*9*/
           :old.cgcterc_forne9, /*10*/   
           0, /*11*/
           :old.cgcterc_forne4, /*12*/   
           0, /*13*/
           :old.cgcterc_forne2, /*14*/   
           0, /*15*/
           :old.data_emissao, /*16*/   
           null, /*17*/
           :old.cod_canc_ordem, /*18*/   
           0, /*19*/
           :old.codigo_servico, /*20*/   
           0, /*21*/
           :old.cod_tabela_serv, /*22*/   
           0, /*23*/
           :old.cod_tabela_mes, /*24*/   
           0, /*25*/
           :old.cod_tabela_seq, /*26*/   
           0, /*27*/
           :old.observacao_01, /*28*/   
           '', /*29*/
           :old.observacao_02, /*30*/   
           '', /*31*/
           :old.situacao_ordem, /*32*/   
           0, /*33*/
           :old.data_entrega, /*34*/   
           null, /*35*/
           :old.transacao, /*36*/   
           0, /*37*/
           :old.peso_enviado, /*38*/   
           0, /*39*/
           :old.peso_recebido, /*40*/   
           0, /*41*/
           :old.observacao_03, /*42*/   
           '', /*43*/
           :old.observacao_04, /*44*/   
           '', /*45*/
           :old.observacao_old, /*46*/   
           '', /*47*/
           :old.observacao, /*48*/   
           '', /*49*/
           :old.moeda, /*50*/   
           0, /*51*/
           :old.cond_pgto, /*52*/   
           0, /*53*/
           :old.executa_trigger, /*54*/   
           0, /*55*/
           :old.exec_alimentacao_autom, /*56*/   
           0, /*57*/
           :old.empresa_req, /*58*/   
           0, /*59*/
           :old.num_requisicao, /*60*/   
           0, /*61*/
           :old.sol_requisicao, /*62*/   
           0, /*63*/
           :old.status_requisicao, /*64*/   
           0, /*65*/
           :old.agrupamento_estoque_vertical, /*66*/   
           0, /*67*/
           :old.numero_ordem_origem, /*68*/   
           0, /*69*/
           :old.sequencia_origem, /*70*/   
           0, /*71*/
           :old.sit_fechamento, /*72*/   
           0, /*73*/
           :old.data_fechamento, /*74*/   
           null, /*75*/
           :old.situacao_impressao, /*76*/   
           0 /*77*/
         );    
    end;    
 end if;    
end inter_tr_OBRF_080_log;
