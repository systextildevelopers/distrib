create or replace procedure wms_pr_gera_sem_pedido (frua              in  number,
                                                    fbox              in  number,
                                                    fcod_emb          in  number,
                                                    rua_aux           in  number,  --Verificar
                                                    box_aux           in  number,  --Verificar
                                                    frua_aux          out number,
                                                    fbox_aux          out number,
                                                    frua_usa          out number,
                                                    fbox_usa          out number) is
  v_encontrou         number; --0: N�o Encontrou; 1: Encontrou
  v_retorno_int       number;
  qt_caixas           number;
  capacidade_endereco number;
  embalagem_endereco  number;
BEGIN

   frua_aux := frua;
   fbox_aux := fbox;

   frua_usa := 0;
   fbox_usa := 0;

   v_retorno_int := 0;
   begin
      select max(1)
      into   v_retorno_int
      from  fatu_110
      where fatu_110.cod_rua       = frua_aux
        and fatu_110.cod_box       = fbox_aux
        and rownum                <= 1;
   exception
     when no_data_found then
        v_retorno_int := 0;
   end;

   if v_retorno_int = 1
   then

      begin
         select count(*)
         into   qt_caixas
         from  fatu_115
         where fatu_115.cod_rua = frua_aux
           and fatu_115.cod_box = fbox_aux;
      exception
        when no_data_found then
           qt_caixas := 0;
      end;

      if qt_caixas  = 0 /*box_aux is undefined or box_aux is null*/
      then

         begin
            select nvl(max(fatu_105.capacidade), 0)
            into   capacidade_endereco
            from fatu_105
            where fatu_105.cod_rua           = frua_aux
              and fatu_105.cod_box           = fbox_aux
              and fatu_105.cod_embalagem     = fcod_emb;
         exception
           when no_data_found then
              capacidade_endereco := 0;
         end;

         if capacidade_endereco > 0
         then

            frua_usa := frua_aux;
            fbox_usa := fbox_aux;

            frua_aux := 1000;
            fbox_aux := 1000;
         end if;

      else

         begin
            select nvl(max(fatu_115.cod_embalagem), 0)
            into   embalagem_endereco
            from  fatu_115
            where fatu_115.cod_rua = frua_aux
              and fatu_115.cod_box = fbox_aux;
         exception
           when no_data_found then
              embalagem_endereco := 0;
         end;

         if embalagem_endereco = fcod_emb
         then

            begin
               select nvl(max(fatu_105.capacidade), 0)
               into   capacidade_endereco
               from  fatu_105
               where fatu_105.cod_rua          = rua_aux
                 and fatu_105.cod_box          = box_aux
                 and fatu_105.cod_embalagem    = embalagem_endereco;
            exception
              when no_data_found then
                 capacidade_endereco := 0;
            end;

            if qt_caixas < capacidade_endereco
            then
               frua_usa := frua_aux;
               fbox_usa := fbox_aux;

               frua_aux := 1000;
               fbox_aux := 1000;
            end if;

         end if;

      end if;

   end if;

END wms_pr_gera_sem_pedido;
/

exec inter_pr_recompile;

/* versao: 2 */


 exit;


 exit;

 exit;
