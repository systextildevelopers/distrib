alter table tmrp_690 add nr_solicitacao_nec number(3) default 0;
comment on column tmrp_690.nr_solicitacao_nec is 'Número da solicitação de necessidades exportada';

alter table tmrp_690 add desc_solicitacao_nec varchar2(30) default '';
comment on column tmrp_690.desc_solicitacao_nec is 'Descrição da solicitação de necessidades exportada';

exec inter_pr_recompile;
