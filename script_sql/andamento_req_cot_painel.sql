CREATE OR REPLACE FORCE VIEW "ANDAMENTO_REQ_COT_PAINEL" ("DATA_REQUISICAO", "CODIGO_EMPRESA", "CODIGO_DEPOSITO", "NOME_REQUISIT", "CODIGO_AGRUPADOR", "NUM_REQUISICAO", "SEQ_ITEM_REQ", "DATA_PREV_ENTR", "ITEM_REQ_NIVEL99", "ITEM_REQ_GRUPO", "ITEM_REQ_SUBGRUPO", "ITEM_REQ_ITEM", "QTDE_REQUISITADA", "DESCRICAO_ITEM", "UNIDADE_MEDIDA", "OBSERVACAO_ITEM", "COMPRADOR_REQUIS", "NUMERO_COTACAO", "PROJETO", "SUBPROJETO", "SERVICO", "NUMERO_PEDIDO", "SEQ_ITEM_PEDIDO", "NUMERO_COLETA", "DT_PREV_ENTR_ITEM_PEDIDO", "CONTA_ESTOQUE", "SOLICITACAO", "CENTRO_CUSTO", "CCUSTO_APLICACAO", "COD_CANCELAMENTO", "CODIGO", "SITUACAO_REQUIS", "SITUACAO_COT") AS 
  SELECT supr_065.data_requisicao,                       supr_065.codigo_empresa,                    supr_065.codigo_deposito,
       supr_065.nome_requisit,                         supr_067.codigo_agrupador,                  supr_067.num_requisicao,
       supr_067.seq_item_req,                          supr_067.data_prev_entr,                    supr_067.item_req_nivel99,
       supr_067.item_req_grupo,                        supr_067.item_req_subgrupo,                 supr_067.item_req_item,
       supr_067.qtde_requisitada,                      supr_067.descricao_item,                    supr_067.unidade_medida,
       supr_067.observacao_item,                       supr_067.codigo_comprador,                  supr_067.numero_cotacao,
       supr_067.projeto,                                supr_067.subprojeto,                         supr_067.servico,
       supr_067.numero_pedido,                         nvl((select supr_100.seq_item_pedido from supr_100 where supr_067.numero_pedido    = supr_100.num_ped_compra and supr_067.num_requisicao = supr_100.num_requisicao and supr_067.seq_item_req     = supr_100.seq_item_req and rownum = 1),0),            supr_067.numero_coleta,
       (select supr_100.data_prev_entr from supr_100 where supr_067.numero_pedido    = supr_100.num_ped_compra and supr_067.num_requisicao = supr_100.num_requisicao and supr_067.seq_item_req     = supr_100.seq_item_req and rownum = 1),
       nvl(basi_030.conta_estoque,0),                  supr_067.solicitacao,                       supr_065.centro_custo,
       supr_067.ccusto_aplicacao,                      supr_067.cod_cancelamento,                  supr_065.codigo,
       decode(supr_067.cod_cancelamento, 0,            -- se supr_067.cod_cancelamento = 0 (nao esta cancelado)
       decode((select supr_100.situacao_item from supr_100 where supr_067.numero_pedido    = supr_100.num_ped_compra and supr_067.num_requisicao = supr_100.num_requisicao and supr_067.seq_item_req     = supr_100.seq_item_req and rownum = 1),
                                      3, 1,            -- entao se supr_100.situacao_item = 3 entao "1-entrega total"
                                      2, 2,            -- senao se supr_100.situacao_item = 2 entao "2-entrega parcial"
       decode(supr_067.numero_pedido, 0,               -- senao se supr_067.numero_pedidos = 0
       decode(supr_067.numero_cotacao, 0,              -- entao se supr_067.numero_cotacao = 0
         decode(supr_067.numero_coleta, 0,               -- entao se supr_067.numero_coleta = 0
           decode(supr_067.situacao_historico, 12, supr_067.situacao_historico, 13, supr_067.situacao_historico,             -- entao se supr_003.pessoa = 0 entao "12 - Pendente requisitante" senao "13 - Retorno requisitante"
             decode(supr_067.situacao, 0, 7, 6)             -- entao se supr_067.situacao = 0 entao "7-aprovada" senao "6-bloqueado"
           ),
         5),          -- senao "5-coletado"
       decode(supr_070.situacao_cotacao,4,11,9,10,4),  -- senao se supr_070.situacao_cotacao  = 4   entao "11-cotacao aprovada"
       decode(supr_067.situacao_historico, 15, supr_067.situacao_historico, 14, supr_067.situacao_historico)
       ),            -- senao se supr_003.pessoa = 1 entao "15 - Retorno requisitante com cota��o" senao "14 - Pendente requisitante com cota��o"
                                                       -- senao se supr_070.situacao_cotacao  = 9   entao "10-cotacao bloqueada"
                                                       -- senao se supr_070.situacao_cotacao <> 4,9 entao "4-com cotacao"
       3)),                                            -- senao "3-com pedido"
       9),                                             -- senao "9-cancelado"
       supr_070.situacao_cotacao
from supr_065,supr_067, supr_070, basi_030
where supr_065.num_requisicao   = supr_067.num_requisicao
  and supr_067.item_req_nivel99 = basi_030.nivel_estrutura (+)
  and supr_067.item_req_grupo   = basi_030.referencia      (+)
  and supr_067.numero_cotacao   = supr_070.numero_cotacao  (+)
/
