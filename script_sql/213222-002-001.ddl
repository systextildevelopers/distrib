update hdoc_035
    set hdoc_035.descricao ='Associação de Componentes à Partes da Máquina'
    where hdoc_035.codigo_programa ='mqop_f031';
update hdoc_036
   set hdoc_036.descricao       = 'Asociación de componentes a piezas de máquinas'
 where hdoc_036.codigo_programa = 'mqop_f032'
   and hdoc_036.locale          = 'es_ES';
update hdoc_036
   set hdoc_036.descricao       = 'Associação de Componentes à Partes da Máquina'
 where hdoc_036.codigo_programa = 'mqop_f032'
   and hdoc_036.locale          = 'pt_BR';

update hdoc_035
    set hdoc_035.descricao ='Associação de Partes à Máquina'
    where hdoc_035.codigo_programa ='mqop_f031';

update hdoc_036
   set hdoc_036.descricao       = 'Asociación de piezas de máquinas'
 where hdoc_036.codigo_programa = 'mqop_f031'
   and hdoc_036.locale          = 'es_ES';

update hdoc_036
   set hdoc_036.descricao       = 'Associação de Partes à Máquina'
 where hdoc_036.codigo_programa = 'mqop_f031'
   and hdoc_036.locale          = 'pr_BR';
commit;
