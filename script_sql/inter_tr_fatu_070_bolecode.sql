CREATE OR REPLACE TRIGGER inter_tr_fatu_070_bolecode
BEFORE INSERT OR UPDATE OF nr_identificacao ON fatu_070 
FOR EACH ROW 
DECLARE
      ptipopix pedi_051.TIPO_CHAVE_CONTA%type;
      pchavepix pedi_051.CONTEUDO_CHAVE%type;
BEGIN
    BEGIN
        -- Consulta SQL para buscar os valores de tipopix e chavepix
        SELECT TIPO_CHAVE_CONTA, CONTEUDO_CHAVE 
        INTO ptipopix, pchavepix
        FROM pedi_051
        WHERE codigo_empresa = :new.codigo_empresa
            AND codigo_banco = :new.portador_duplic
            AND nr_identificacao = :new.nr_identificacao;
        
        -- Atribuir os valores aos campos :new.tipopix e :new.chavepix
        :new.tipopix := ptipopix;
        :new.chavepix := pchavepix;
        
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            :new.tipopix := 0;
            :new.chavepix := ' ';
    END;
END inter_tr_fatu_070_bolecode;
