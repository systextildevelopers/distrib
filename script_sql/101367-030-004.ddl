ALTER TABLE PCPC_045 ADD (
  solicitacao_conserto NUMBER(9));

ALTER TABLE PCPC_045 MODIFY (
  solicitacao_conserto DEFAULT 0);

COMMENT ON COLUMN PCPC_045.solicitacao_conserto IS 'Numero solicitacao que é gerado ao criar uma solicitacao de conserto';

exec inter_pr_recompile;

/
