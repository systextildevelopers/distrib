
  CREATE OR REPLACE TRIGGER "INTER_TR_CREC_180_LOG" 
after insert or
update of numero_titulo, parcela_titulo, valor_usado, sequencia,
	flag_reg, empresa, cgc9_cli, cgc4_cli,
	cgc2_cli, banco, agencia, conta,
	num_cheque, tipo_titulo
or delete or update
on CREC_180
for each row
declare
   ws_usuario_rede           varchar2(20) ;
   ws_maquina_rede           varchar2(40) ;
   ws_aplicativo             varchar2(20) ;
   ws_sid                    number(9) ;
   ws_empresa                number(3) ;
   ws_usuario_systextil      varchar2(250) ;
   ws_locale_usuario         varchar2(5) ;
   v_nome_programa           varchar2(20) ;


begin
-- Dados do usuￜrio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   v_nome_programa := inter_fn_nome_programa(ws_sid); 

 if inserting
 then
    begin

        insert into CREC_180_log (
           TIPO_OCORR,   /*0*/
           DATA_OCORR,   /*1*/
           HORA_OCORR,   /*2*/
           USUARIO_REDE,   /*3*/
           MAQUINA_REDE,   /*4*/
           APLICACAO,   /*5*/
           USUARIO_SISTEMA,   /*6*/
           NOME_PROGRAMA,   /*7*/
           EMPRESA_OLD,   /*8*/
           EMPRESA_NEW,   /*9*/
           CGC9_CLI_OLD,   /*10*/
           CGC9_CLI_NEW,   /*11*/
           CGC4_CLI_OLD,   /*12*/
           CGC4_CLI_NEW,   /*13*/
           CGC2_CLI_OLD,   /*14*/
           CGC2_CLI_NEW,   /*15*/
           BANCO_OLD,   /*16*/
           BANCO_NEW,   /*17*/
           AGENCIA_OLD,   /*18*/
           AGENCIA_NEW,   /*19*/
           CONTA_OLD,   /*20*/
           CONTA_NEW,   /*21*/
           NUM_CHEQUE_OLD,   /*22*/
           NUM_CHEQUE_NEW,   /*23*/
           TIPO_TITULO_OLD,   /*24*/
           TIPO_TITULO_NEW,   /*25*/
           NUMERO_TITULO_OLD,   /*26*/
           NUMERO_TITULO_NEW,   /*27*/
           PARCELA_TITULO_OLD,   /*28*/
           PARCELA_TITULO_NEW,   /*29*/
           VALOR_USADO_OLD,   /*30*/
           VALOR_USADO_NEW,   /*31*/
           SEQUENCIA_OLD,   /*32*/
           SEQUENCIA_NEW,   /*33*/
           FLAG_REG_OLD,   /*34*/
           FLAG_REG_NEW    /*35*/
        ) values (
            'I', /*1*/
            sysdate, /*2*/
            sysdate,/*3*/
            ws_usuario_rede,/*4*/
            ws_maquina_rede, /*5*/
            ws_aplicativo, /*6*/
            ws_usuario_systextil,/*7*/
            v_nome_programa, /*8*/
           0,/*8*/
           :new.EMPRESA, /*9*/
           0,/*10*/
           :new.CGC9_CLI, /*11*/
           0,/*12*/
           :new.CGC4_CLI, /*13*/
           0,/*14*/
           :new.CGC2_CLI, /*15*/
           0,/*16*/
           :new.BANCO, /*17*/
           0,/*18*/
           :new.AGENCIA, /*19*/
           0,/*20*/
           :new.CONTA, /*21*/
           0,/*22*/
           :new.NUM_CHEQUE, /*23*/
           0,/*24*/
           :new.TIPO_TITULO, /*25*/
           0,/*26*/
           :new.NUMERO_TITULO, /*27*/
           0,/*28*/
           :new.PARCELA_TITULO, /*29*/
           0,/*30*/
           :new.VALOR_USADO, /*31*/
           0,/*32*/
           :new.SEQUENCIA, /*33*/
           0,/*34*/
           :new.FLAG_REG /*35*/
         );
    end;
 end if;


 if updating
 then
    begin
        insert into CREC_180_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           EMPRESA_OLD, /*8*/
           EMPRESA_NEW, /*9*/
           CGC9_CLI_OLD, /*10*/
           CGC9_CLI_NEW, /*11*/
           CGC4_CLI_OLD, /*12*/
           CGC4_CLI_NEW, /*13*/
           CGC2_CLI_OLD, /*14*/
           CGC2_CLI_NEW, /*15*/
           BANCO_OLD, /*16*/
           BANCO_NEW, /*17*/
           AGENCIA_OLD, /*18*/
           AGENCIA_NEW, /*19*/
           CONTA_OLD, /*20*/
           CONTA_NEW, /*21*/
           NUM_CHEQUE_OLD, /*22*/
           NUM_CHEQUE_NEW, /*23*/
           TIPO_TITULO_OLD, /*24*/
           TIPO_TITULO_NEW, /*25*/
           NUMERO_TITULO_OLD, /*26*/
           NUMERO_TITULO_NEW, /*27*/
           PARCELA_TITULO_OLD, /*28*/
           PARCELA_TITULO_NEW, /*29*/
           VALOR_USADO_OLD, /*30*/
           VALOR_USADO_NEW, /*31*/
           SEQUENCIA_OLD, /*32*/
           SEQUENCIA_NEW, /*33*/
           FLAG_REG_OLD, /*34*/
           FLAG_REG_NEW  /*35*/
        ) values (
            'A', /*1*/
            sysdate, /*2*/
            sysdate, /*3*/
            ws_usuario_rede,/*4*/
            ws_maquina_rede, /*5*/
            ws_aplicativo, /*6*/
            ws_usuario_systextil,/*7*/
            v_nome_programa, /*8*/
           :old.EMPRESA,  /*8*/
           :new.EMPRESA, /*9*/
           :old.CGC9_CLI,  /*10*/
           :new.CGC9_CLI, /*11*/
           :old.CGC4_CLI,  /*12*/
           :new.CGC4_CLI, /*13*/
           :old.CGC2_CLI,  /*14*/
           :new.CGC2_CLI, /*15*/
           :old.BANCO,  /*16*/
           :new.BANCO, /*17*/
           :old.AGENCIA,  /*18*/
           :new.AGENCIA, /*19*/
           :old.CONTA,  /*20*/
           :new.CONTA, /*21*/
           :old.NUM_CHEQUE,  /*22*/
           :new.NUM_CHEQUE, /*23*/
           :old.TIPO_TITULO,  /*24*/
           :new.TIPO_TITULO, /*25*/
           :old.NUMERO_TITULO,  /*26*/
           :new.NUMERO_TITULO, /*27*/
           :old.PARCELA_TITULO,  /*28*/
           :new.PARCELA_TITULO, /*29*/
           :old.VALOR_USADO,  /*30*/
           :new.VALOR_USADO, /*31*/
           :old.SEQUENCIA,  /*32*/
           :new.SEQUENCIA, /*33*/
           :old.FLAG_REG,  /*34*/
           :new.FLAG_REG  /*35*/
         );
    end;
 end if;


 if deleting
 then
    begin
        insert into CREC_180_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           EMPRESA_OLD, /*8*/
           EMPRESA_NEW, /*9*/
           CGC9_CLI_OLD, /*10*/
           CGC9_CLI_NEW, /*11*/
           CGC4_CLI_OLD, /*12*/
           CGC4_CLI_NEW, /*13*/
           CGC2_CLI_OLD, /*14*/
           CGC2_CLI_NEW, /*15*/
           BANCO_OLD, /*16*/
           BANCO_NEW, /*17*/
           AGENCIA_OLD, /*18*/
           AGENCIA_NEW, /*19*/
           CONTA_OLD, /*20*/
           CONTA_NEW, /*21*/
           NUM_CHEQUE_OLD, /*22*/
           NUM_CHEQUE_NEW, /*23*/
           TIPO_TITULO_OLD, /*24*/
           TIPO_TITULO_NEW, /*25*/
           NUMERO_TITULO_OLD, /*26*/
           NUMERO_TITULO_NEW, /*27*/
           PARCELA_TITULO_OLD, /*28*/
           PARCELA_TITULO_NEW, /*29*/
           VALOR_USADO_OLD, /*30*/
           VALOR_USADO_NEW, /*31*/
           SEQUENCIA_OLD, /*32*/
           SEQUENCIA_NEW, /*33*/
           FLAG_REG_OLD, /*34*/
           FLAG_REG_NEW /*35*/
        ) values (
            'D', /*1*/
            sysdate, /*2*/
            sysdate, /*3*/
            ws_usuario_rede,/*4*/
            ws_maquina_rede,/*5*/
            ws_aplicativo, /*6*/
            ws_usuario_systextil,/*7*/
            v_nome_programa, /*8*/
           :old.EMPRESA, /*8*/
           0, /*9*/
           :old.CGC9_CLI, /*10*/
           0, /*11*/
           :old.CGC4_CLI, /*12*/
           0, /*13*/
           :old.CGC2_CLI, /*14*/
           0, /*15*/
           :old.BANCO, /*16*/
           0, /*17*/
           :old.AGENCIA, /*18*/
           0, /*19*/
           :old.CONTA, /*20*/
           0, /*21*/
           :old.NUM_CHEQUE, /*22*/
           0, /*23*/
           :old.TIPO_TITULO, /*24*/
           0, /*25*/
           :old.NUMERO_TITULO, /*26*/
           0, /*27*/
           :old.PARCELA_TITULO, /*28*/
           0, /*29*/
           :old.VALOR_USADO, /*30*/
           0, /*31*/
           :old.SEQUENCIA, /*32*/
           0, /*33*/
           :old.FLAG_REG, /*34*/
           0 /*35*/
         );
    end;
 end if;
end inter_tr_CREC_180_log;
-- ALTER TRIGGER "INTER_TR_CREC_180_LOG" ENABLE
 

/

exec inter_pr_recompile;

