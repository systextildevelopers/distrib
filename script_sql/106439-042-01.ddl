create table ORGT_008 (
  ORDEM_TINGIMENTO  NUMBER(9),
  NR_TUBOS          NUMBER(9) default 0
);

alter table ORGT_008
add constraint PK_ORGT_008 primary key (ORDEM_TINGIMENTO);
/
