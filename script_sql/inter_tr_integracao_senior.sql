
  CREATE OR REPLACE TRIGGER "INTER_TR_INTEGRACAO_SENIOR" 
after insert
on INTEGRACAO_SENIOR
for each row

-- Finalidade: Atualizar cadastro de funcionarios
-- Autor.....: Edson Pio
-- Data......: 17/02/09
--
-- Historicos de alteracoes na trigger
--
-- Data    Autor    Observacoes
--
declare
   w_cod_empresa        efic_050.cod_empresa%type;
   w_nome               efic_050.nome%type;
   w_turno              efic_050.turno%type;
   w_data_admissao      efic_050.data_admissao%type;
   w_sit_funcionario    efic_050.sit_funcionario%type;
   w_cod_empr_systextil empr_syst_senior.cod_empr_systextil%type;
   w_cod_empresa_ant    empr_syst_senior.cod_empr_systextil%type;
   w_erro               varchar2(1);
   w_ja_tem_funcionario number(3);
   w_nome1               efic_050.nome%type;

begin
   w_erro := 'n';
   w_ja_tem_funcionario := 0;

   begin
      select empr_syst_senior.cod_empr_systextil
      into   w_cod_empr_systextil
      from empr_syst_senior
      where empr_syst_senior.cod_empr_senior    = :new.cod_empresa
        and empr_syst_senior.ind_matriz_filial  = :new.ind_matriz_filial;
   exception
   when no_data_found then
      w_cod_empr_systextil := 0;
      w_erro := 's';
      raise_application_error(-20101,'Empresa nao cadastrada na tabela empresa Systextil X empresa Senior' || Chr(10) ||
      'Cod. Empresa Senior: ' || to_char(:new.cod_empresa,'000'));
   end;

   -- PROCURA A EMRPESA DO FUNCIONARIO ANTERIOR, NO CASO DE MUDANCA DE EMPRESA
   if :new.cod_empresa <> :new.cod_empresa_ant and :new.cod_empresa_ant > 0
   then
      begin
         select empr_syst_senior.cod_empr_systextil
         into   w_cod_empresa_ant
         from empr_syst_senior
         where empr_syst_senior.cod_empr_senior    = :new.cod_empresa_ant
           and empr_syst_senior.ind_matriz_filial  = :new.ind_matriz_filial;
      exception
      when no_data_found then
         w_cod_empresa_ant := 0;
         w_erro := 's';
         raise_application_error(-20101,'Empresa nao cadastrada na tabela empresa Systextil X empresa Senior' || Chr(10) ||
         'Cod. Empresa Senior: ' || to_char(:new.cod_empresa,'000'));
      end;
   else
      w_cod_empresa_ant := w_cod_empr_systextil; -- FICA IGUAL
   end if;

   if :new.ind_ocorrencia = 'A'  -- 'I-Incluir,A-Alterar'
   then
      begin
         select efic_050.cod_empresa,      efic_050.nome,
                efic_050.turno,            efic_050.data_admissao,
                efic_050.sit_funcionario
          into  w_cod_empresa,             w_nome,
                w_turno,                   w_data_admissao,
                w_sit_funcionario
          from efic_050
          where efic_050.cod_empresa     = w_cod_empresa_ant
            and efic_050.cod_funcionario = :new.cod_funcionario;
      exception
      when no_data_found then
         w_cod_empresa     := 0;
         w_nome            := '';
         w_turno           := 0;
         w_data_admissao   := null;
         w_sit_funcionario := 0;
      end;

      if w_cod_empr_systextil is not null and w_cod_empr_systextil > 0 and
         w_cod_empr_systextil <> w_cod_empresa
      then
         w_cod_empresa := w_cod_empr_systextil;
      end if;

      if :new.nome is not null and :new.nome <> ' ' and :new.nome <> w_nome
      then
         w_nome := :new.nome;
      end if;


      if :new.turno is not null and :new.turno > 0 and
         :new.turno <> w_turno
      then
         w_turno := :new.turno;
      end if;

      if :new.ind_ocorrencia = 'I' and :new.turno not in (1,2,3,4)
      then
         w_erro := 's';
         raise_application_error(-20101,'Erro ao atualizar cadastro de funcionarios:' || Chr(10) ||
         'Turno invalido: ' || to_char(:new.turno,'0') || Chr(10) ||
         'Cod. Funcionario: ' || to_char(:new.cod_funcionario,'000000') || Chr(10) ||
         'Cod. Empresa: ' || to_char(w_cod_empr_systextil,'000'));
      end if;

      if :new.ind_ocorrencia = 'A' and w_turno not in (1,2,3,4) and w_turno > 0
      then
         w_erro := 's';
         raise_application_error(-20101,'Erro ao atualizar cadastro de funcionarios:' || Chr(10) ||
         'Turno invalido: ' || to_char(:new.turno,'0') || Chr(10) ||
         'Cod. Funcionario: ' || to_char(:new.cod_funcionario,'000000') || Chr(10) ||
         'Cod. Empresa: ' || to_char(w_cod_empr_systextil,'000'));
      end if;

      if :new.data_admissao is not null and :new.data_admissao <> w_data_admissao
      then
         w_data_admissao := :new.data_admissao;
      end if;

      if :new.sit_funcionario is not null and :new.sit_funcionario > 0 and
         :new.sit_funcionario <> w_sit_funcionario
      then
         w_sit_funcionario := :new.sit_funcionario;
      end if;

      if :new.ind_ocorrencia = 'I' and :new.sit_funcionario not in (1,2)
      then
         w_erro := 's';
         raise_application_error(-20101,'Erro ao atualizar cadastro de funcionarios:' || Chr(10) ||
         'Situacao invalida: ' || to_char(:new.sit_funcionario,'0') || Chr(10) ||
         'Cod. Funcionario: ' || to_char(:new.cod_funcionario,'000000') || Chr(10) ||
         'Cod. Empresa: ' || to_char(w_cod_empr_systextil,'000'));
      end if;

      if :new.ind_ocorrencia = 'A' and :new.sit_funcionario not in (1,2) and w_sit_funcionario > 0
      then
         w_erro := 's';
         raise_application_error(-20101,'Erro ao atualizar cadastro de funcionarios:' || Chr(10) ||
         'Situacao invalida: ' || to_char(:new.sit_funcionario,'0') || Chr(10) ||
         'Cod. Funcionario: ' || to_char(:new.cod_funcionario,'000000') || Chr(10) ||
         'Cod. Empresa: ' || to_char(w_cod_empr_systextil,'000'));
      end if;

      if w_erro = 'n'
      then
         begin
            update efic_050
               set efic_050.cod_empresa     = w_cod_empresa,
                   efic_050.nome            = w_nome,
                   efic_050.turno           = w_turno,
                   efic_050.data_admissao   = w_data_admissao,
                   efic_050.sit_funcionario = w_sit_funcionario
               where efic_050.cod_empresa     = w_cod_empresa_ant
                 and efic_050.cod_funcionario = :new.cod_funcionario;
         exception
         when no_data_found then
            w_erro := 's';
            raise_application_error(-20101,'Erro ao atualizar cadastro de funcionarios:' || Chr(10) ||
            'Cod. Funcionario: ' || to_char(:new.cod_funcionario,'000000') || Chr(10) ||
            'Cod. Empresa: ' || to_char(w_cod_empresa_ant,'000'));
         end;
      end if;
   end if;

   if :new.ind_ocorrencia = 'I'  -- 'I-Incluir,A-Alterar'
   then
      begin
         select count(*)
         into w_ja_tem_funcionario
         from efic_050
          where efic_050.cod_empresa     = w_cod_empresa_ant
            and efic_050.cod_funcionario = :new.cod_funcionario;
      exception
         when no_data_found then
            w_ja_tem_funcionario := 0;
      end;

      if w_ja_tem_funcionario > 0
      then
          raise_application_error(-20101,'Funcionario ja cadastrado no Systextil, nao e possivel incluir novamente.' || Chr(10) ||
          'Cod. Funcionario: ' || to_char(:new.cod_funcionario,'000000') || Chr(10) ||
          'Cod. Empresa: ' || to_char(w_cod_empresa_ant,'000'));
      else
         begin
            insert into efic_050
               (cod_empresa,             cod_funcionario,
                nome,                    turno,
                lanca_parada,            lanca_rejeicao,
                codigo_cargo,            centro_custo,
                data_nascimento,         data_admissao,
                sexo,                    estado_civil,
                grau_instrucao,          plano_saude,
                cpf_func,                custo_hora,
                e_mail,                  ramal,
                cracha_funcionario,      setor_responsavel)
            values
               (w_cod_empresa_ant,       :new.cod_funcionario,
                :new.nome,               :new.turno,
                :new.lanca_parada,       :new.lanca_rejeicao,
                0,                       0,
                :new.data_nascimento,    :new.data_admissao,
                :new.sexo,               :new.estado_civil,
                :new.grau_instrucao,     :new.plano_saude,
                :new.cpf_func,           :new.custo_hora,
                :new.e_mail,             :new.ramal,
                :new.cracha_funcionario, 0);
         exception
         when others then
            w_erro := 's';
            raise_application_error(-20101,'Erro ao incluir cadastro de funcionarios:' || Chr(10) ||
            'Cod. Funcionario: ' || to_char(:new.cod_funcionario,'000000') || Chr(10) ||
            'Cod. Empresa: ' || to_char(w_cod_empr_systextil,'000'));
         end;
      end if;
   end if;
end;
-- ALTER TRIGGER "INTER_TR_INTEGRACAO_SENIOR" DISABLE
 

/

exec inter_pr_recompile;

