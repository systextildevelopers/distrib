
  CREATE OR REPLACE TRIGGER "INTER_TR_CONT_500" 
before update of situacao on cont_500
for each row








begin
   if UPDATING
   then
      if :new.situacao = 0
      then
         :new.situacao_orcm := 1;
      else 
         :new.situacao_orcm := 9;
      end if;
   end if;

end inter_tr_cont_500;


-- ALTER TRIGGER "INTER_TR_CONT_500" ENABLE
 

/

exec inter_pr_recompile;

