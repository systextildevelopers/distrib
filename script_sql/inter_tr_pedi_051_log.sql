
  CREATE OR REPLACE TRIGGER "INTER_TR_PEDI_051_LOG" 
after insert or delete or update
on PEDI_051
for each row
declare
   ws_usuario_rede           varchar2(20) ;
   ws_maquina_rede           varchar2(40) ;
   ws_aplicativo             varchar2(20) ;
   ws_sid                    number(9) ;
   ws_empresa                number(3) ;
   ws_usuario_systextil      varchar2(250) ;
   ws_locale_usuario         varchar2(5) ;
   v_nome_programa           varchar2(20) ;


begin
-- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   v_nome_programa := inter_fn_nome_programa(ws_sid);                            

 if inserting
 then
    begin

        insert into PEDI_051_log (
           TIPO_OCORR,   /*0*/
           DATA_OCORR,   /*1*/
           HORA_OCORR,   /*2*/
           USUARIO_REDE,   /*3*/
           MAQUINA_REDE,   /*4*/
           APLICACAO,   /*5*/
           USUARIO_SISTEMA,   /*6*/
           NOME_PROGRAMA,   /*7*/
           CODIGO_BANCO_OLD,   /*8*/
           CODIGO_BANCO_NEW,   /*9*/
           CODIGO_AGENCIA_OLD,   /*10*/
           CODIGO_AGENCIA_NEW,   /*11*/
           CODIGO_CIDADE_OLD,   /*12*/
           CODIGO_CIDADE_NEW,   /*13*/
           DIGITO_AGENCIA_OLD,   /*14*/
           DIGITO_AGENCIA_NEW,   /*15*/
           CONSIDERA_COBRAN_OLD,   /*16*/
           CONSIDERA_COBRAN_NEW,   /*17*/
           NUMERO_REMESSA_OLD,   /*18*/
           NUMERO_REMESSA_NEW,   /*19*/
           CONTA_CORRENTE_OLD,   /*20*/
           CONTA_CORRENTE_NEW,   /*21*/
           DIGITO_CONTA_OLD,   /*22*/
           DIGITO_CONTA_NEW,   /*23*/
           CODIGO_CARTEIRA_OLD,   /*24*/
           CODIGO_CARTEIRA_NEW,   /*25*/
           ACEITE_OLD,   /*26*/
           ACEITE_NEW,   /*27*/
           PRIMEIRA_INSTR_OLD,   /*28*/
           PRIMEIRA_INSTR_NEW,   /*29*/
           SEGUNDA_INSTR_OLD,   /*30*/
           SEGUNDA_INSTR_NEW,   /*31*/
           MENSAGEM_OLD,   /*32*/
           MENSAGEM_NEW,   /*33*/
           NUMERO_CONTRATO_OLD,   /*34*/
           NUMERO_CONTRATO_NEW,   /*35*/
           VARIACAO_OLD,   /*36*/
           VARIACAO_NEW,   /*37*/
           POSICAO_OLD,   /*38*/
           POSICAO_NEW,   /*39*/
           DIAS_INSTR1_OLD,   /*40*/
           DIAS_INSTR1_NEW,   /*41*/
           DIAS_INSTR2_OLD,   /*42*/
           DIAS_INSTR2_NEW,   /*43*/
           PERC_INSTR1_OLD,   /*44*/
           PERC_INSTR1_NEW,   /*45*/
           PORTADOR_BAIXA_OLD,   /*46*/
           PORTADOR_BAIXA_NEW,   /*47*/
           SEQ_NR_TIT_BANCO_OLD,   /*48*/
           SEQ_NR_TIT_BANCO_NEW,   /*49*/
           CAL_NR_TIT_BANCO_OLD,   /*50*/
           CAL_NR_TIT_BANCO_NEW,   /*51*/
           CARTEIRA_ITAU_OLD,   /*52*/
           CARTEIRA_ITAU_NEW,   /*53*/
           FAIXA_MINIMA_OLD,   /*54*/
           FAIXA_MINIMA_NEW,   /*55*/
           FAIXA_MAXIMA_OLD,   /*56*/
           FAIXA_MAXIMA_NEW,   /*57*/
           CODIGO_EMPRESA_OLD,   /*58*/
           CODIGO_EMPRESA_NEW,   /*59*/
           BANCO_REJEICAO_OLD,   /*60*/
           BANCO_REJEICAO_NEW,   /*61*/
           CONTA_CORRENTE_CRED_OLD,   /*62*/
           CONTA_CORRENTE_CRED_NEW,   /*63*/
           TROCA_TIPO_OLD,   /*64*/
           TROCA_TIPO_NEW,   /*65*/
           TAXA_VENDOR_OLD,   /*66*/
           TAXA_VENDOR_NEW,   /*67*/
           DESCRICAO_OLD,   /*68*/
           DESCRICAO_NEW,   /*69*/
           TAXA_IOF_OLD,   /*70*/
           TAXA_IOF_NEW,   /*71*/
           CONTA_CPAG_OLD,   /*72*/
           CONTA_CPAG_NEW,   /*73*/
           DIGITO_CPAG_OLD,   /*74*/
           DIGITO_CPAG_NEW,   /*75*/
           PORTADOR_BAIXA1_OLD,   /*76*/
           PORTADOR_BAIXA1_NEW,   /*77*/
           PORTADOR_BAIXA2_OLD,   /*78*/
           PORTADOR_BAIXA2_NEW,   /*79*/
           OCORRENCIA_OLD,   /*80*/
           OCORRENCIA_NEW,   /*81*/
           OCORRENCIA1_OLD,   /*82*/
           OCORRENCIA1_NEW,   /*83*/
           OCORRENCIA2_OLD,   /*84*/
           OCORRENCIA2_NEW,   /*85*/
           NR_CONVENIO_OLD,   /*86*/
           NR_CONVENIO_NEW,   /*87*/
           CODIGO_CONTRATO_OLD,   /*88*/
           CODIGO_CONTRATO_NEW,   /*89*/
           TIPO_MORA_OLD,   /*90*/
           TIPO_MORA_NEW,   /*91*/
           COD_PRODUTO_OLD,   /*92*/
           COD_PRODUTO_NEW,   /*93*/
           COD_SUBPRODUTO_OLD,   /*94*/
           COD_SUBPRODUTO_NEW,   /*95*/
           CART_COND_PGTO_COBR_OLD,   /*96*/
           CART_COND_PGTO_COBR_NEW,   /*97*/
           TIPO_SERVICO_OLD,   /*98*/
           TIPO_SERVICO_NEW,   /*99*/
           COD_OPERACAO_OLD,   /*100*/
           COD_OPERACAO_NEW,   /*101*/
           BANCO_CRED_FAC_OLD,   /*102*/
           BANCO_CRED_FAC_NEW,   /*103*/
           COD_CONT_DUPL_DESC_OLD,   /*104*/
           COD_CONT_DUPL_DESC_NEW,   /*105*/
           VERSAO_LAYOUT_OLD,   /*106*/
           VERSAO_LAYOUT_NEW,   /*107*/
           MENSAGEM_COMPL_1_OLD,   /*108*/
           MENSAGEM_COMPL_1_NEW,   /*109*/
           MENSAGEM_COMPL_2_OLD,   /*110*/
           MENSAGEM_COMPL_2_NEW,   /*111*/
           MENSAGEM_COMPL_3_OLD,   /*112*/
           MENSAGEM_COMPL_3_NEW,   /*113*/
           MENSAGEM_COMPL_4_OLD,   /*114*/
           MENSAGEM_COMPL_4_NEW,   /*115*/
           PADRAO_CNAB_OLD,   /*116*/
           PADRAO_CNAB_NEW,   /*117*/
           LIBERACAO_OPERACAO_OLD,   /*118*/
           LIBERACAO_OPERACAO_NEW,   /*119*/
           OCORRENCIA3_OLD,   /*120*/
           OCORRENCIA3_NEW,   /*121*/
           OCORRENCIA4_OLD,   /*122*/
           OCORRENCIA4_NEW,   /*123*/
           POSICAO3_OLD,   /*124*/
           POSICAO3_NEW,   /*125*/
           POSICAO4_OLD,   /*126*/
           POSICAO4_NEW,   /*127*/
           NR_IDENTIFICACAO_OLD,   /*128*/
           NR_IDENTIFICACAO_NEW,   /*129*/
           TIPO_MULTA_OLD,   /*130*/
           TIPO_MULTA_NEW,   /*131*/
           NUM_SEQ_TIT_BANCO_OLD,   /*132*/
           NUM_SEQ_TIT_BANCO_NEW,   /*133*/
           COD_BANCO_ENVIO_OLD,   /*134*/
           COD_BANCO_ENVIO_NEW    /*135*/
        ) values (
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           0,/*8*/
           :new.CODIGO_BANCO, /*9*/
           0,/*10*/
           :new.CODIGO_AGENCIA, /*11*/
           0,/*12*/
           :new.CODIGO_CIDADE, /*13*/
           '',/*14*/
           :new.DIGITO_AGENCIA, /*15*/
           '',/*16*/
           :new.CONSIDERA_COBRAN, /*17*/
           0,/*18*/
           :new.NUMERO_REMESSA, /*19*/
           0,/*20*/
           :new.CONTA_CORRENTE, /*21*/
           '',/*22*/
           :new.DIGITO_CONTA, /*23*/
           0,/*24*/
           :new.CODIGO_CARTEIRA, /*25*/
           '',/*26*/
           :new.ACEITE, /*27*/
           0,/*28*/
           :new.PRIMEIRA_INSTR, /*29*/
           0,/*30*/
           :new.SEGUNDA_INSTR, /*31*/
           '',/*32*/
           :new.MENSAGEM, /*33*/
           0,/*34*/
           :new.NUMERO_CONTRATO, /*35*/
           0,/*36*/
           :new.VARIACAO, /*37*/
           0,/*38*/
           :new.POSICAO, /*39*/
           0,/*40*/
           :new.DIAS_INSTR1, /*41*/
           0,/*42*/
           :new.DIAS_INSTR2, /*43*/
           0,/*44*/
           :new.PERC_INSTR1, /*45*/
           0,/*46*/
           :new.PORTADOR_BAIXA, /*47*/
           0,/*48*/
           :new.SEQ_NR_TIT_BANCO, /*49*/
           0,/*50*/
           :new.CAL_NR_TIT_BANCO, /*51*/
           '',/*52*/
           :new.CARTEIRA_ITAU, /*53*/
           0,/*54*/
           :new.FAIXA_MINIMA, /*55*/
           0,/*56*/
           :new.FAIXA_MAXIMA, /*57*/
           0,/*58*/
           :new.CODIGO_EMPRESA, /*59*/
           0,/*60*/
           :new.BANCO_REJEICAO, /*61*/
           0,/*62*/
           :new.CONTA_CORRENTE_CRED, /*63*/
           '',/*64*/
           :new.TROCA_TIPO, /*65*/
           0,/*66*/
           :new.TAXA_VENDOR, /*67*/
           '',/*68*/
           :new.DESCRICAO, /*69*/
           0,/*70*/
           :new.TAXA_IOF, /*71*/
           0,/*72*/
           :new.CONTA_CPAG, /*73*/
           '',/*74*/
           :new.DIGITO_CPAG, /*75*/
           0,/*76*/
           :new.PORTADOR_BAIXA1, /*77*/
           0,/*78*/
           :new.PORTADOR_BAIXA2, /*79*/
           0,/*80*/
           :new.OCORRENCIA, /*81*/
           0,/*82*/
           :new.OCORRENCIA1, /*83*/
           0,/*84*/
           :new.OCORRENCIA2, /*85*/
           0,/*86*/
           :new.NR_CONVENIO, /*87*/
           '',/*88*/
           :new.CODIGO_CONTRATO, /*89*/
           0,/*90*/
           :new.TIPO_MORA, /*91*/
           0,/*92*/
           :new.COD_PRODUTO, /*93*/
           0,/*94*/
           :new.COD_SUBPRODUTO, /*95*/
           0,/*96*/
           :new.CART_COND_PGTO_COBR, /*97*/
           '',/*98*/
           :new.TIPO_SERVICO, /*99*/
           0,/*100*/
           :new.COD_OPERACAO, /*101*/
           0,/*102*/
           :new.BANCO_CRED_FAC, /*103*/
           0,/*104*/
           :new.COD_CONT_DUPL_DESC, /*105*/
           0,/*106*/
           :new.VERSAO_LAYOUT, /*107*/
           '',/*108*/
           :new.MENSAGEM_COMPL_1, /*109*/
           '',/*110*/
           :new.MENSAGEM_COMPL_2, /*111*/
           '',/*112*/
           :new.MENSAGEM_COMPL_3, /*113*/
           '',/*114*/
           :new.MENSAGEM_COMPL_4, /*115*/
           0,/*116*/
           :new.PADRAO_CNAB, /*117*/
           '',/*118*/
           :new.LIBERACAO_OPERACAO, /*119*/
           0,/*120*/
           :new.OCORRENCIA3, /*121*/
           0,/*122*/
           :new.OCORRENCIA4, /*123*/
           0,/*124*/
           :new.POSICAO3, /*125*/
           0,/*126*/
           :new.POSICAO4, /*127*/
           0,/*128*/
           :new.NR_IDENTIFICACAO, /*129*/
           0,/*130*/
           :new.TIPO_MULTA, /*131*/
           0,/*132*/
           :new.NUM_SEQ_TIT_BANCO, /*133*/
           0,/*134*/
           :new.COD_BANCO_ENVIO /*135*/
         );
    end;
 end if;


 if updating
 then
    begin
        insert into PEDI_051_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           CODIGO_BANCO_OLD, /*8*/
           CODIGO_BANCO_NEW, /*9*/
           CODIGO_AGENCIA_OLD, /*10*/
           CODIGO_AGENCIA_NEW, /*11*/
           CODIGO_CIDADE_OLD, /*12*/
           CODIGO_CIDADE_NEW, /*13*/
           DIGITO_AGENCIA_OLD, /*14*/
           DIGITO_AGENCIA_NEW, /*15*/
           CONSIDERA_COBRAN_OLD, /*16*/
           CONSIDERA_COBRAN_NEW, /*17*/
           NUMERO_REMESSA_OLD, /*18*/
           NUMERO_REMESSA_NEW, /*19*/
           CONTA_CORRENTE_OLD, /*20*/
           CONTA_CORRENTE_NEW, /*21*/
           DIGITO_CONTA_OLD, /*22*/
           DIGITO_CONTA_NEW, /*23*/
           CODIGO_CARTEIRA_OLD, /*24*/
           CODIGO_CARTEIRA_NEW, /*25*/
           ACEITE_OLD, /*26*/
           ACEITE_NEW, /*27*/
           PRIMEIRA_INSTR_OLD, /*28*/
           PRIMEIRA_INSTR_NEW, /*29*/
           SEGUNDA_INSTR_OLD, /*30*/
           SEGUNDA_INSTR_NEW, /*31*/
           MENSAGEM_OLD, /*32*/
           MENSAGEM_NEW, /*33*/
           NUMERO_CONTRATO_OLD, /*34*/
           NUMERO_CONTRATO_NEW, /*35*/
           VARIACAO_OLD, /*36*/
           VARIACAO_NEW, /*37*/
           POSICAO_OLD, /*38*/
           POSICAO_NEW, /*39*/
           DIAS_INSTR1_OLD, /*40*/
           DIAS_INSTR1_NEW, /*41*/
           DIAS_INSTR2_OLD, /*42*/
           DIAS_INSTR2_NEW, /*43*/
           PERC_INSTR1_OLD, /*44*/
           PERC_INSTR1_NEW, /*45*/
           PORTADOR_BAIXA_OLD, /*46*/
           PORTADOR_BAIXA_NEW, /*47*/
           SEQ_NR_TIT_BANCO_OLD, /*48*/
           SEQ_NR_TIT_BANCO_NEW, /*49*/
           CAL_NR_TIT_BANCO_OLD, /*50*/
           CAL_NR_TIT_BANCO_NEW, /*51*/
           CARTEIRA_ITAU_OLD, /*52*/
           CARTEIRA_ITAU_NEW, /*53*/
           FAIXA_MINIMA_OLD, /*54*/
           FAIXA_MINIMA_NEW, /*55*/
           FAIXA_MAXIMA_OLD, /*56*/
           FAIXA_MAXIMA_NEW, /*57*/
           CODIGO_EMPRESA_OLD, /*58*/
           CODIGO_EMPRESA_NEW, /*59*/
           BANCO_REJEICAO_OLD, /*60*/
           BANCO_REJEICAO_NEW, /*61*/
           CONTA_CORRENTE_CRED_OLD, /*62*/
           CONTA_CORRENTE_CRED_NEW, /*63*/
           TROCA_TIPO_OLD, /*64*/
           TROCA_TIPO_NEW, /*65*/
           TAXA_VENDOR_OLD, /*66*/
           TAXA_VENDOR_NEW, /*67*/
           DESCRICAO_OLD, /*68*/
           DESCRICAO_NEW, /*69*/
           TAXA_IOF_OLD, /*70*/
           TAXA_IOF_NEW, /*71*/
           CONTA_CPAG_OLD, /*72*/
           CONTA_CPAG_NEW, /*73*/
           DIGITO_CPAG_OLD, /*74*/
           DIGITO_CPAG_NEW, /*75*/
           PORTADOR_BAIXA1_OLD, /*76*/
           PORTADOR_BAIXA1_NEW, /*77*/
           PORTADOR_BAIXA2_OLD, /*78*/
           PORTADOR_BAIXA2_NEW, /*79*/
           OCORRENCIA_OLD, /*80*/
           OCORRENCIA_NEW, /*81*/
           OCORRENCIA1_OLD, /*82*/
           OCORRENCIA1_NEW, /*83*/
           OCORRENCIA2_OLD, /*84*/
           OCORRENCIA2_NEW, /*85*/
           NR_CONVENIO_OLD, /*86*/
           NR_CONVENIO_NEW, /*87*/
           CODIGO_CONTRATO_OLD, /*88*/
           CODIGO_CONTRATO_NEW, /*89*/
           TIPO_MORA_OLD, /*90*/
           TIPO_MORA_NEW, /*91*/
           COD_PRODUTO_OLD, /*92*/
           COD_PRODUTO_NEW, /*93*/
           COD_SUBPRODUTO_OLD, /*94*/
           COD_SUBPRODUTO_NEW, /*95*/
           CART_COND_PGTO_COBR_OLD, /*96*/
           CART_COND_PGTO_COBR_NEW, /*97*/
           TIPO_SERVICO_OLD, /*98*/
           TIPO_SERVICO_NEW, /*99*/
           COD_OPERACAO_OLD, /*100*/
           COD_OPERACAO_NEW, /*101*/
           BANCO_CRED_FAC_OLD, /*102*/
           BANCO_CRED_FAC_NEW, /*103*/
           COD_CONT_DUPL_DESC_OLD, /*104*/
           COD_CONT_DUPL_DESC_NEW, /*105*/
           VERSAO_LAYOUT_OLD, /*106*/
           VERSAO_LAYOUT_NEW, /*107*/
           MENSAGEM_COMPL_1_OLD, /*108*/
           MENSAGEM_COMPL_1_NEW, /*109*/
           MENSAGEM_COMPL_2_OLD, /*110*/
           MENSAGEM_COMPL_2_NEW, /*111*/
           MENSAGEM_COMPL_3_OLD, /*112*/
           MENSAGEM_COMPL_3_NEW, /*113*/
           MENSAGEM_COMPL_4_OLD, /*114*/
           MENSAGEM_COMPL_4_NEW, /*115*/
           PADRAO_CNAB_OLD, /*116*/
           PADRAO_CNAB_NEW, /*117*/
           LIBERACAO_OPERACAO_OLD, /*118*/
           LIBERACAO_OPERACAO_NEW, /*119*/
           OCORRENCIA3_OLD, /*120*/
           OCORRENCIA3_NEW, /*121*/
           OCORRENCIA4_OLD, /*122*/
           OCORRENCIA4_NEW, /*123*/
           POSICAO3_OLD, /*124*/
           POSICAO3_NEW, /*125*/
           POSICAO4_OLD, /*126*/
           POSICAO4_NEW, /*127*/
           NR_IDENTIFICACAO_OLD, /*128*/
           NR_IDENTIFICACAO_NEW, /*129*/
           TIPO_MULTA_OLD, /*130*/
           TIPO_MULTA_NEW, /*131*/
           NUM_SEQ_TIT_BANCO_OLD, /*132*/
           NUM_SEQ_TIT_BANCO_NEW, /*133*/
           COD_BANCO_ENVIO_OLD, /*134*/
           COD_BANCO_ENVIO_NEW  /*135*/
        ) values (
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.CODIGO_BANCO,  /*8*/
           :new.CODIGO_BANCO, /*9*/
           :old.CODIGO_AGENCIA,  /*10*/
           :new.CODIGO_AGENCIA, /*11*/
           :old.CODIGO_CIDADE,  /*12*/
           :new.CODIGO_CIDADE, /*13*/
           :old.DIGITO_AGENCIA,  /*14*/
           :new.DIGITO_AGENCIA, /*15*/
           :old.CONSIDERA_COBRAN,  /*16*/
           :new.CONSIDERA_COBRAN, /*17*/
           :old.NUMERO_REMESSA,  /*18*/
           :new.NUMERO_REMESSA, /*19*/
           :old.CONTA_CORRENTE,  /*20*/
           :new.CONTA_CORRENTE, /*21*/
           :old.DIGITO_CONTA,  /*22*/
           :new.DIGITO_CONTA, /*23*/
           :old.CODIGO_CARTEIRA,  /*24*/
           :new.CODIGO_CARTEIRA, /*25*/
           :old.ACEITE,  /*26*/
           :new.ACEITE, /*27*/
           :old.PRIMEIRA_INSTR,  /*28*/
           :new.PRIMEIRA_INSTR, /*29*/
           :old.SEGUNDA_INSTR,  /*30*/
           :new.SEGUNDA_INSTR, /*31*/
           :old.MENSAGEM,  /*32*/
           :new.MENSAGEM, /*33*/
           :old.NUMERO_CONTRATO,  /*34*/
           :new.NUMERO_CONTRATO, /*35*/
           :old.VARIACAO,  /*36*/
           :new.VARIACAO, /*37*/
           :old.POSICAO,  /*38*/
           :new.POSICAO, /*39*/
           :old.DIAS_INSTR1,  /*40*/
           :new.DIAS_INSTR1, /*41*/
           :old.DIAS_INSTR2,  /*42*/
           :new.DIAS_INSTR2, /*43*/
           :old.PERC_INSTR1,  /*44*/
           :new.PERC_INSTR1, /*45*/
           :old.PORTADOR_BAIXA,  /*46*/
           :new.PORTADOR_BAIXA, /*47*/
           :old.SEQ_NR_TIT_BANCO,  /*48*/
           :new.SEQ_NR_TIT_BANCO, /*49*/
           :old.CAL_NR_TIT_BANCO,  /*50*/
           :new.CAL_NR_TIT_BANCO, /*51*/
           :old.CARTEIRA_ITAU,  /*52*/
           :new.CARTEIRA_ITAU, /*53*/
           :old.FAIXA_MINIMA,  /*54*/
           :new.FAIXA_MINIMA, /*55*/
           :old.FAIXA_MAXIMA,  /*56*/
           :new.FAIXA_MAXIMA, /*57*/
           :old.CODIGO_EMPRESA,  /*58*/
           :new.CODIGO_EMPRESA, /*59*/
           :old.BANCO_REJEICAO,  /*60*/
           :new.BANCO_REJEICAO, /*61*/
           :old.CONTA_CORRENTE_CRED,  /*62*/
           :new.CONTA_CORRENTE_CRED, /*63*/
           :old.TROCA_TIPO,  /*64*/
           :new.TROCA_TIPO, /*65*/
           :old.TAXA_VENDOR,  /*66*/
           :new.TAXA_VENDOR, /*67*/
           :old.DESCRICAO,  /*68*/
           :new.DESCRICAO, /*69*/
           :old.TAXA_IOF,  /*70*/
           :new.TAXA_IOF, /*71*/
           :old.CONTA_CPAG,  /*72*/
           :new.CONTA_CPAG, /*73*/
           :old.DIGITO_CPAG,  /*74*/
           :new.DIGITO_CPAG, /*75*/
           :old.PORTADOR_BAIXA1,  /*76*/
           :new.PORTADOR_BAIXA1, /*77*/
           :old.PORTADOR_BAIXA2,  /*78*/
           :new.PORTADOR_BAIXA2, /*79*/
           :old.OCORRENCIA,  /*80*/
           :new.OCORRENCIA, /*81*/
           :old.OCORRENCIA1,  /*82*/
           :new.OCORRENCIA1, /*83*/
           :old.OCORRENCIA2,  /*84*/
           :new.OCORRENCIA2, /*85*/
           :old.NR_CONVENIO,  /*86*/
           :new.NR_CONVENIO, /*87*/
           :old.CODIGO_CONTRATO,  /*88*/
           :new.CODIGO_CONTRATO, /*89*/
           :old.TIPO_MORA,  /*90*/
           :new.TIPO_MORA, /*91*/
           :old.COD_PRODUTO,  /*92*/
           :new.COD_PRODUTO, /*93*/
           :old.COD_SUBPRODUTO,  /*94*/
           :new.COD_SUBPRODUTO, /*95*/
           :old.CART_COND_PGTO_COBR,  /*96*/
           :new.CART_COND_PGTO_COBR, /*97*/
           :old.TIPO_SERVICO,  /*98*/
           :new.TIPO_SERVICO, /*99*/
           :old.COD_OPERACAO,  /*100*/
           :new.COD_OPERACAO, /*101*/
           :old.BANCO_CRED_FAC,  /*102*/
           :new.BANCO_CRED_FAC, /*103*/
           :old.COD_CONT_DUPL_DESC,  /*104*/
           :new.COD_CONT_DUPL_DESC, /*105*/
           :old.VERSAO_LAYOUT,  /*106*/
           :new.VERSAO_LAYOUT, /*107*/
           :old.MENSAGEM_COMPL_1,  /*108*/
           :new.MENSAGEM_COMPL_1, /*109*/
           :old.MENSAGEM_COMPL_2,  /*110*/
           :new.MENSAGEM_COMPL_2, /*111*/
           :old.MENSAGEM_COMPL_3,  /*112*/
           :new.MENSAGEM_COMPL_3, /*113*/
           :old.MENSAGEM_COMPL_4,  /*114*/
           :new.MENSAGEM_COMPL_4, /*115*/
           :old.PADRAO_CNAB,  /*116*/
           :new.PADRAO_CNAB, /*117*/
           :old.LIBERACAO_OPERACAO,  /*118*/
           :new.LIBERACAO_OPERACAO, /*119*/
           :old.OCORRENCIA3,  /*120*/
           :new.OCORRENCIA3, /*121*/
           :old.OCORRENCIA4,  /*122*/
           :new.OCORRENCIA4, /*123*/
           :old.POSICAO3,  /*124*/
           :new.POSICAO3, /*125*/
           :old.POSICAO4,  /*126*/
           :new.POSICAO4, /*127*/
           :old.NR_IDENTIFICACAO,  /*128*/
           :new.NR_IDENTIFICACAO, /*129*/
           :old.TIPO_MULTA,  /*130*/
           :new.TIPO_MULTA, /*131*/
           :old.NUM_SEQ_TIT_BANCO,  /*132*/
           :new.NUM_SEQ_TIT_BANCO, /*133*/
           :old.COD_BANCO_ENVIO,  /*134*/
           :new.COD_BANCO_ENVIO  /*135*/
         );
    end;
 end if;


 if deleting
 then
    begin
        insert into PEDI_051_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           CODIGO_BANCO_OLD, /*8*/
           CODIGO_BANCO_NEW, /*9*/
           CODIGO_AGENCIA_OLD, /*10*/
           CODIGO_AGENCIA_NEW, /*11*/
           CODIGO_CIDADE_OLD, /*12*/
           CODIGO_CIDADE_NEW, /*13*/
           DIGITO_AGENCIA_OLD, /*14*/
           DIGITO_AGENCIA_NEW, /*15*/
           CONSIDERA_COBRAN_OLD, /*16*/
           CONSIDERA_COBRAN_NEW, /*17*/
           NUMERO_REMESSA_OLD, /*18*/
           NUMERO_REMESSA_NEW, /*19*/
           CONTA_CORRENTE_OLD, /*20*/
           CONTA_CORRENTE_NEW, /*21*/
           DIGITO_CONTA_OLD, /*22*/
           DIGITO_CONTA_NEW, /*23*/
           CODIGO_CARTEIRA_OLD, /*24*/
           CODIGO_CARTEIRA_NEW, /*25*/
           ACEITE_OLD, /*26*/
           ACEITE_NEW, /*27*/
           PRIMEIRA_INSTR_OLD, /*28*/
           PRIMEIRA_INSTR_NEW, /*29*/
           SEGUNDA_INSTR_OLD, /*30*/
           SEGUNDA_INSTR_NEW, /*31*/
           MENSAGEM_OLD, /*32*/
           MENSAGEM_NEW, /*33*/
           NUMERO_CONTRATO_OLD, /*34*/
           NUMERO_CONTRATO_NEW, /*35*/
           VARIACAO_OLD, /*36*/
           VARIACAO_NEW, /*37*/
           POSICAO_OLD, /*38*/
           POSICAO_NEW, /*39*/
           DIAS_INSTR1_OLD, /*40*/
           DIAS_INSTR1_NEW, /*41*/
           DIAS_INSTR2_OLD, /*42*/
           DIAS_INSTR2_NEW, /*43*/
           PERC_INSTR1_OLD, /*44*/
           PERC_INSTR1_NEW, /*45*/
           PORTADOR_BAIXA_OLD, /*46*/
           PORTADOR_BAIXA_NEW, /*47*/
           SEQ_NR_TIT_BANCO_OLD, /*48*/
           SEQ_NR_TIT_BANCO_NEW, /*49*/
           CAL_NR_TIT_BANCO_OLD, /*50*/
           CAL_NR_TIT_BANCO_NEW, /*51*/
           CARTEIRA_ITAU_OLD, /*52*/
           CARTEIRA_ITAU_NEW, /*53*/
           FAIXA_MINIMA_OLD, /*54*/
           FAIXA_MINIMA_NEW, /*55*/
           FAIXA_MAXIMA_OLD, /*56*/
           FAIXA_MAXIMA_NEW, /*57*/
           CODIGO_EMPRESA_OLD, /*58*/
           CODIGO_EMPRESA_NEW, /*59*/
           BANCO_REJEICAO_OLD, /*60*/
           BANCO_REJEICAO_NEW, /*61*/
           CONTA_CORRENTE_CRED_OLD, /*62*/
           CONTA_CORRENTE_CRED_NEW, /*63*/
           TROCA_TIPO_OLD, /*64*/
           TROCA_TIPO_NEW, /*65*/
           TAXA_VENDOR_OLD, /*66*/
           TAXA_VENDOR_NEW, /*67*/
           DESCRICAO_OLD, /*68*/
           DESCRICAO_NEW, /*69*/
           TAXA_IOF_OLD, /*70*/
           TAXA_IOF_NEW, /*71*/
           CONTA_CPAG_OLD, /*72*/
           CONTA_CPAG_NEW, /*73*/
           DIGITO_CPAG_OLD, /*74*/
           DIGITO_CPAG_NEW, /*75*/
           PORTADOR_BAIXA1_OLD, /*76*/
           PORTADOR_BAIXA1_NEW, /*77*/
           PORTADOR_BAIXA2_OLD, /*78*/
           PORTADOR_BAIXA2_NEW, /*79*/
           OCORRENCIA_OLD, /*80*/
           OCORRENCIA_NEW, /*81*/
           OCORRENCIA1_OLD, /*82*/
           OCORRENCIA1_NEW, /*83*/
           OCORRENCIA2_OLD, /*84*/
           OCORRENCIA2_NEW, /*85*/
           NR_CONVENIO_OLD, /*86*/
           NR_CONVENIO_NEW, /*87*/
           CODIGO_CONTRATO_OLD, /*88*/
           CODIGO_CONTRATO_NEW, /*89*/
           TIPO_MORA_OLD, /*90*/
           TIPO_MORA_NEW, /*91*/
           COD_PRODUTO_OLD, /*92*/
           COD_PRODUTO_NEW, /*93*/
           COD_SUBPRODUTO_OLD, /*94*/
           COD_SUBPRODUTO_NEW, /*95*/
           CART_COND_PGTO_COBR_OLD, /*96*/
           CART_COND_PGTO_COBR_NEW, /*97*/
           TIPO_SERVICO_OLD, /*98*/
           TIPO_SERVICO_NEW, /*99*/
           COD_OPERACAO_OLD, /*100*/
           COD_OPERACAO_NEW, /*101*/
           BANCO_CRED_FAC_OLD, /*102*/
           BANCO_CRED_FAC_NEW, /*103*/
           COD_CONT_DUPL_DESC_OLD, /*104*/
           COD_CONT_DUPL_DESC_NEW, /*105*/
           VERSAO_LAYOUT_OLD, /*106*/
           VERSAO_LAYOUT_NEW, /*107*/
           MENSAGEM_COMPL_1_OLD, /*108*/
           MENSAGEM_COMPL_1_NEW, /*109*/
           MENSAGEM_COMPL_2_OLD, /*110*/
           MENSAGEM_COMPL_2_NEW, /*111*/
           MENSAGEM_COMPL_3_OLD, /*112*/
           MENSAGEM_COMPL_3_NEW, /*113*/
           MENSAGEM_COMPL_4_OLD, /*114*/
           MENSAGEM_COMPL_4_NEW, /*115*/
           PADRAO_CNAB_OLD, /*116*/
           PADRAO_CNAB_NEW, /*117*/
           LIBERACAO_OPERACAO_OLD, /*118*/
           LIBERACAO_OPERACAO_NEW, /*119*/
           OCORRENCIA3_OLD, /*120*/
           OCORRENCIA3_NEW, /*121*/
           OCORRENCIA4_OLD, /*122*/
           OCORRENCIA4_NEW, /*123*/
           POSICAO3_OLD, /*124*/
           POSICAO3_NEW, /*125*/
           POSICAO4_OLD, /*126*/
           POSICAO4_NEW, /*127*/
           NR_IDENTIFICACAO_OLD, /*128*/
           NR_IDENTIFICACAO_NEW, /*129*/
           TIPO_MULTA_OLD, /*130*/
           TIPO_MULTA_NEW, /*131*/
           NUM_SEQ_TIT_BANCO_OLD, /*132*/
           NUM_SEQ_TIT_BANCO_NEW, /*133*/
           COD_BANCO_ENVIO_OLD, /*134*/
           COD_BANCO_ENVIO_NEW /*135*/
        ) values (
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede,/*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.CODIGO_BANCO, /*8*/
           0, /*9*/
           :old.CODIGO_AGENCIA, /*10*/
           0, /*11*/
           :old.CODIGO_CIDADE, /*12*/
           0, /*13*/
           :old.DIGITO_AGENCIA, /*14*/
           '', /*15*/
           :old.CONSIDERA_COBRAN, /*16*/
           '', /*17*/
           :old.NUMERO_REMESSA, /*18*/
           0, /*19*/
           :old.CONTA_CORRENTE, /*20*/
           0, /*21*/
           :old.DIGITO_CONTA, /*22*/
           '', /*23*/
           :old.CODIGO_CARTEIRA, /*24*/
           0, /*25*/
           :old.ACEITE, /*26*/
           '', /*27*/
           :old.PRIMEIRA_INSTR, /*28*/
           0, /*29*/
           :old.SEGUNDA_INSTR, /*30*/
           0, /*31*/
           :old.MENSAGEM, /*32*/
           '', /*33*/
           :old.NUMERO_CONTRATO, /*34*/
           0, /*35*/
           :old.VARIACAO, /*36*/
           0, /*37*/
           :old.POSICAO, /*38*/
           0, /*39*/
           :old.DIAS_INSTR1, /*40*/
           0, /*41*/
           :old.DIAS_INSTR2, /*42*/
           0, /*43*/
           :old.PERC_INSTR1, /*44*/
           0, /*45*/
           :old.PORTADOR_BAIXA, /*46*/
           0, /*47*/
           :old.SEQ_NR_TIT_BANCO, /*48*/
           0, /*49*/
           :old.CAL_NR_TIT_BANCO, /*50*/
           0, /*51*/
           :old.CARTEIRA_ITAU, /*52*/
           '', /*53*/
           :old.FAIXA_MINIMA, /*54*/
           0, /*55*/
           :old.FAIXA_MAXIMA, /*56*/
           0, /*57*/
           :old.CODIGO_EMPRESA, /*58*/
           0, /*59*/
           :old.BANCO_REJEICAO, /*60*/
           0, /*61*/
           :old.CONTA_CORRENTE_CRED, /*62*/
           0, /*63*/
           :old.TROCA_TIPO, /*64*/
           '', /*65*/
           :old.TAXA_VENDOR, /*66*/
           0, /*67*/
           :old.DESCRICAO, /*68*/
           '', /*69*/
           :old.TAXA_IOF, /*70*/
           0, /*71*/
           :old.CONTA_CPAG, /*72*/
           0, /*73*/
           :old.DIGITO_CPAG, /*74*/
           '', /*75*/
           :old.PORTADOR_BAIXA1, /*76*/
           0, /*77*/
           :old.PORTADOR_BAIXA2, /*78*/
           0, /*79*/
           :old.OCORRENCIA, /*80*/
           0, /*81*/
           :old.OCORRENCIA1, /*82*/
           0, /*83*/
           :old.OCORRENCIA2, /*84*/
           0, /*85*/
           :old.NR_CONVENIO, /*86*/
           0, /*87*/
           :old.CODIGO_CONTRATO, /*88*/
           '', /*89*/
           :old.TIPO_MORA, /*90*/
           0, /*91*/
           :old.COD_PRODUTO, /*92*/
           0, /*93*/
           :old.COD_SUBPRODUTO, /*94*/
           0, /*95*/
           :old.CART_COND_PGTO_COBR, /*96*/
           0, /*97*/
           :old.TIPO_SERVICO, /*98*/
           '', /*99*/
           :old.COD_OPERACAO, /*100*/
           0, /*101*/
           :old.BANCO_CRED_FAC, /*102*/
           0, /*103*/
           :old.COD_CONT_DUPL_DESC, /*104*/
           0, /*105*/
           :old.VERSAO_LAYOUT, /*106*/
           0, /*107*/
           :old.MENSAGEM_COMPL_1, /*108*/
           '', /*109*/
           :old.MENSAGEM_COMPL_2, /*110*/
           '', /*111*/
           :old.MENSAGEM_COMPL_3, /*112*/
           '', /*113*/
           :old.MENSAGEM_COMPL_4, /*114*/
           '', /*115*/
           :old.PADRAO_CNAB, /*116*/
           0, /*117*/
           :old.LIBERACAO_OPERACAO, /*118*/
           '', /*119*/
           :old.OCORRENCIA3, /*120*/
           0, /*121*/
           :old.OCORRENCIA4, /*122*/
           0, /*123*/
           :old.POSICAO3, /*124*/
           0, /*125*/
           :old.POSICAO4, /*126*/
           0, /*127*/
           :old.NR_IDENTIFICACAO, /*128*/
           0, /*129*/
           :old.TIPO_MULTA, /*130*/
           0, /*131*/
           :old.NUM_SEQ_TIT_BANCO, /*132*/
           0, /*133*/
           :old.COD_BANCO_ENVIO, /*134*/
           0 /*135*/
         );
    end;
 end if;
end inter_tr_PEDI_051_log;

-- ALTER TRIGGER "INTER_TR_PEDI_051_LOG" ENABLE
 

/

exec inter_pr_recompile;

