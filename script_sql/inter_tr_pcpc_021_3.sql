
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPC_021_3" 
     before insert
         or delete
         or update of
         quantidade, sequencia_tamanho, executa_trigger, qtde_programada_orig,
         ordem_producao, tamanho, sortimento
     on pcpc_021
     for each row
declare
   v_executa_trigger  number;
   v_periodo_producao number;
begin
   -- INICIO - Logica implementada para controle de "gatilho" das triggers do banco
   -- para atender a implementacao executada para limpeza da base de dados do cliente
   -- e replicacao dos dados para base historico. (SS.38405)
   if inserting
   then
      if :new.executa_trigger = 1
      then
         v_executa_trigger := 1;
      else
         v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then
         v_executa_trigger := 1;
      else
         v_executa_trigger := 0;
      end if;
   end if;
   -- FINAL - Logica implementada para controle de "gatilho" das triggers do banco. (SS.38405)

   if v_executa_trigger = 0
   then
      if inserting
      then
         begin
            select pcpc_020.periodo_producao
            into v_periodo_producao
            from pcpc_020
            where pcpc_020.ordem_producao = :new.ordem_producao;
         exception when others then
            v_periodo_producao := 0;
         end;

         inter_pr_verif_reg_em_producao (0,:new.executa_trigger);

         inter_pr_verif_per_fechado(v_periodo_producao,1);
      end if;

      if deleting
      then
         begin
            select pcpc_020.periodo_producao
            into v_periodo_producao
            from pcpc_020
            where pcpc_020.ordem_producao = :old.ordem_producao;
         exception when others then
            v_periodo_producao := 0;
         end;

         inter_pr_verif_reg_em_producao (0,:old.executa_trigger);

         inter_pr_verif_per_fechado(v_periodo_producao,1);
      end if;
   end if;

end inter_tr_pcpc_021_3;

-- ALTER TRIGGER "INTER_TR_PCPC_021_3" ENABLE
 

/

exec inter_pr_recompile;

