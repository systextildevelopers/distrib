create table pcpc_029(ordem_origem number(9) not null, ordem_destino number(9)not null,codigo_rolo number(9) not null,
primary key(ordem_origem,ordem_destino,codigo_rolo));

comment on table pcpc_029 is 'Tabela de associação de ordens de produção para rateio de rolos';

comment on column pcpc_029.ordem_origem            is 'Ordem de origem associada ao rolo';
comment on column pcpc_029.ordem_destino           is 'Ordem de destino do rolo associado';
comment on column pcpc_029.codigo_rolo             is 'Código do rolo associado a ordem de origem';

alter table pcpt_025 add rolo_rateado varchar2(1);

comment on column pcpt_025.rolo_rateado            is 'Confirmação de rolo rateado pelo processo via apex pcpc_fa01, S = rateado';

commit;
