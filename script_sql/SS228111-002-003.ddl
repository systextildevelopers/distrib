ALTER TABLE HAUS_003
ADD NUMERO_PEDIDO NUMBER(9);

ALTER TABLE HAUS_003
ADD DATA_FATURAMENTO DATE;

ALTER TABLE HAUS_003
DROP COLUMN ITEM_PEDIDO;

ALTER TABLE HAUS_003
DROP COLUMN PRODUTO_EAN;

ALTER TABLE HAUS_003
DROP COLUMN NIVEL;

ALTER TABLE HAUS_003
DROP COLUMN GRUPO;

ALTER TABLE HAUS_003
DROP COLUMN SUBGRUPO;

ALTER TABLE HAUS_003
DROP COLUMN ITEM;

ALTER TABLE HAUS_003
DROP COLUMN QUANTIDADE;

ALTER TABLE HAUS_003
DROP COLUMN VALOR_VENDA;

alter table haus_003
add (
    codigo_empresa number(3) default 0,
    pedido_gerado number(9) default 0,
    solicitacao_gerada number(5) default 0,
    num_nota_gerada number(9) default 0,
    serie_nota_gerada varchar2(3) default '',
    cgc9_nota_gerada number(9) default 0,
    cgc4_nota_gerada number(4) default 0,
    cgc2_nota_gerada number(2) default 0
);

alter table haus_003 
add (
    situacao number(1) default 0,
    observacao varchar2(4000) default ''
);
