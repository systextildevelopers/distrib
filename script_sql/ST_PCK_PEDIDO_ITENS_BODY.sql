create or replace package body ST_PCK_PEDIDO_ITENS is
    
    FUNCTION get_itens_pedido(p_pedido_venda number, p_msg_erro in out varchar2) return t_dados_pedi_110
    AS 
        v_dados_pedido_venda t_dados_pedi_110;
    BEGIN
        begin
            SELECT *  
            BULK COLLECT 
            INTO v_dados_pedido_venda
            FROM pedi_110 i
            WHERE i.pedido_venda = p_pedido_venda
            AND i.COD_CANCELAMENTO = 0;
        exception when others then
            p_msg_erro := 'N?o foi possivel buscar os itens do pedido!. p_pedido_venda:' || p_pedido_venda;
            return null;
        end;
        return v_dados_pedido_venda;
    END;


    FUNCTION get_itens_pedido_saldo(p_pedido_venda NUMBER, p_msg_erro IN OUT VARCHAR2) 
    RETURN t_dados_pedi_110_saldo
    AS 
        v_dados_pedido_venda_saldo t_dados_pedi_110_saldo;
    BEGIN
        BEGIN
            SELECT *  
            BULK COLLECT 
            INTO v_dados_pedido_venda_saldo
            FROM pedi_110 i
            WHERE i.pedido_venda = p_pedido_venda
            AND i.COD_CANCELAMENTO = 0
            AND (i.situacao_fatu_it <> 1 OR i.qtde_afaturar > 0.00);  -- considera item em aberto ou com saldo;
        EXCEPTION 
            WHEN OTHERS THEN
                p_msg_erro := 'Nao foi possivel buscar os itens do pedido!. p_pedido_venda:' || p_pedido_venda;
                RETURN NULL;
        END;
        RETURN v_dados_pedido_venda_saldo;
    END;

    FUNCTION get_itens_pedido_saldo_by_seq(p_pedido_venda NUMBER, p_cod_proforma VARCHAR2,  p_msg_erro IN OUT VARCHAR2) 
    RETURN t_dados_pedi_110_saldo
    AS 
        v_dados_pedido_venda_saldo_seq t_dados_pedi_110_saldo;
    BEGIN
        BEGIN
            SELECT *  
            BULK COLLECT 
            INTO v_dados_pedido_venda_saldo_seq
            FROM pedi_110 i
            WHERE i.pedido_venda = p_pedido_venda
            AND i.COD_CANCELAMENTO = 0
            AND (i.situacao_fatu_it <> 1 OR i.qtde_afaturar > 0.00) -- considera item em aberto ou com saldo;
            AND i.cod_proforma = p_cod_proforma;  
        EXCEPTION 
            WHEN OTHERS THEN
                p_msg_erro := 'Nao foi possivel buscar os itens do pedido!. p_pedido_venda:' || p_pedido_venda;
                RETURN NULL;
        END;
        RETURN v_dados_pedido_venda_saldo_seq;
    END;


end ST_PCK_PEDIDO_ITENS;
