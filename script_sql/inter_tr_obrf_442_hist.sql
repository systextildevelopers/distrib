CREATE OR REPLACE TRIGGER "INTER_TR_OBRF_442_HIST"
   after insert or delete or update
   on obrf_442
   for each row

-- Finalidade: Registrar alteracoes feitas na tabela obrf_442 - Comandos SQL para Apura��o dos Impostos (ICMS)
-- Autor.....: Luana Giese
-- Data......: 08/11/21
--
-- Historicos de alteracoes na trigger
--
-- Data      Autor           SS          Observacoes
-- 08/11/21  Luana Giese     208562-011  Adicionado campos na verificacao da trigger.

declare	
   ws_tipo_ocorr                   varchar2 (1);
   ws_usuario_rede                 varchar2(20);
   ws_maquina_rede                 varchar2(40);
   ws_aplicativo                   varchar2(20);
   ws_sid                          number(9);
   ws_empresa                      number(3);
   ws_usuario_systextil            varchar2(250);
   ws_locale_usuario               varchar2(5);
   ws_nome_programa                varchar2(20);
   ws_codigo_old				           obrf_442.codigo%type;
   ws_codigo_new				           obrf_442.codigo%type;
   ws_nome_old				           obrf_442.nome%type;
   ws_nome_new				           obrf_442.nome%type;
   ws_descricao_old				           obrf_442.descricao%type;
   ws_descricao_new				           obrf_442.descricao%type;
   ws_script_sql_old			         obrf_442.script_sql%type;
   ws_script_sql_new			         obrf_442.script_sql%type;

begin
   if INSERTING then
   
      ws_tipo_ocorr                   := 'I';    
      ws_codigo_old				            := 0;
      ws_codigo_new				            := :new.codigo;
      ws_nome_old				              := '';
      ws_nome_new				              := :new.nome;
      ws_descricao_old				        := '';
      ws_descricao_new				        := :new.descricao;
      ws_script_sql_old			          := null;
      ws_script_sql_new			          := :new.script_sql;
	 
   elsif UPDATING then

      ws_tipo_ocorr              	    := 'A';      
      ws_codigo_old				            := :old.codigo;
      ws_codigo_new				            := :new.codigo;
      ws_nome_old				              := :old.nome;
      ws_nome_new				              := :new.nome;
      ws_descricao_old				        := :old.descricao;
      ws_descricao_new				        := :new.descricao;
      ws_script_sql_old			          := :old.script_sql;
      ws_script_sql_new			          := :new.script_sql;

   elsif DELETING then
   
      ws_tipo_ocorr                 	:= 'D';    
      ws_codigo_old				            := :old.codigo;
      ws_codigo_new				            := 0;
      ws_nome_old				              := :old.nome;
      ws_nome_new				              := '';
      ws_descricao_old				        := :old.descricao;
      ws_descricao_new				        := '';
      ws_script_sql_old			          := :old.script_sql;
      ws_script_sql_new			          := null;
      
   end if;

   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   ws_nome_programa := inter_fn_nome_programa(ws_sid);

   INSERT INTO OBRF_442_HIST
    (
        APLICACAO,          TIPO_OCORR,
        DATA_OCORR,         USUARIO_REDE,
        MAQUINA_REDE,       
        CODIGO_OLD,         CODIGO_NEW,
        NOME_OLD,           NOME_NEW,
        DESCRICAO_OLD,      DESCRICAO_NEW,
        SCRIPT_SQL_OLD,     SCRIPT_SQL_NEW
    )
   VALUES
      ( ws_aplicativo,                    ws_tipo_ocorr,
        sysdate,						              ws_usuario_rede,                   
		    ws_maquina_rede,
		    ws_codigo_old,                    ws_codigo_new,
		    ws_nome_old,                      ws_nome_new,
		    ws_descricao_old,                 ws_descricao_new,
		    ws_script_sql_old,                ws_script_sql_new
	);
	
end inter_tr_obrf_442_hist;
-- ALTER TRIGGER "INTER_TR_OBRF_442_HIST" ENABLE





/
