
  CREATE OR REPLACE PROCEDURE "INTER_PR_ATUALIZA_DEBITO" 
   (p_empresa  in numeric,   p_documento in numeric, p_serie in varchar2,
    p_cnpj9    in numeric,   p_cnpj4    in numeric,  p_cnpj2 in numeric,
    p_nr_draw  in numeric)
is
   cursor obrf015 is
      select obrf_015.coditem_nivel99 pnivel,         obrf_015.coditem_grupo pgrupo,
             obrf_015.coditem_subgrupo psub ,         obrf_015.coditem_item pitem,
             obrf_015.quantidade pqtde,               obrf_015.valor_unitario pvlr_uni,
             obrf_015.codigo_deposito pdep,           obrf_015.formulario_num pformulario_num,
             obrf_015.formulario_ser pformulario_ser
      from obrf_015
      where obrf_015.capa_ent_nrdoc   = p_documento
        and obrf_015.capa_ent_serie   = p_serie
        and obrf_015.capa_ent_forcli9 = p_cnpj9
        and obrf_015.capa_ent_forcli4 = p_cnpj4
        and obrf_015.capa_ent_forcli2 = p_cnpj2
        and obrf_015.formulario_num   > 0;

   v_ctrl_draw      basi_015.controla_draw_back%type;
   v_seq            number;
   v_draw_back      number;
   psequencia       number;
   total_debito     float;
begin

   for reg_obrf015 in obrf015
   loop

     begin
        /*Verifica se o produto e controlado por draw back*/
         select basi_015.controla_draw_back into  v_ctrl_draw
         from basi_015
            where basi_015.codigo_empresa    = p_empresa
              and  basi_015.nivel_estrutura   = reg_obrf015.pnivel
              and basi_015.grupo_estrutura    = reg_obrf015.pgrupo
              and basi_015.subgru_estrutura   = reg_obrf015.psub
              and basi_015.item_estrutura     = reg_obrf015.pitem
              and basi_015.controla_draw_back = 1;
         exception
            when others then
               v_ctrl_draw := 0;
      end;

      /*Se o produto, da nota de debito for controlado por drawback*/
      if v_ctrl_draw = 1
      then

         v_draw_back := 0;
         begin
            /*verifica se a nota de fatura desse debito existe no drawback tipo importado*/
            select obrf_011.draw_back_entrada into v_draw_back
            from obrf_011
            where obrf_011.empresa             = p_empresa
              and obrf_011.nr_fatura_import    = reg_obrf015.pformulario_num
              and obrf_011.serie_fatura_import = reg_obrf015.pformulario_ser
              and obrf_011.cnpj9_import        = p_cnpj9
              and obrf_011.cnpj4_import        = p_cnpj4
              and obrf_011.cnpj2_import        = p_cnpj2;
           exception
               when others then
                 v_draw_back := 0;
        end;
        /*verifica se a nota de fatura desse debito existe no drawback tipo local*/
        if v_draw_back = 0
        then

           begin
              select obrf_011.draw_back_entrada into v_draw_back
              from obrf_011
              where obrf_011.empresa            = p_empresa
                and obrf_011.nr_fatura_local    = reg_obrf015.pformulario_num
                and obrf_011.serie_fatura_local = reg_obrf015.pformulario_ser
                and obrf_011.cnpj9_local        = p_cnpj9
                and obrf_011.cnpj4_local        = p_cnpj4
                and obrf_011.cnpj2_local        = p_cnpj2;
              exception
                 when others then
                    v_draw_back := 0;
           end;
        end if;

      end if;
      /*se existir drawback para essa nota de debito, fazer o update em cima do numero do drawback
        Verificar se o item da nota de debito existe no nmero de drawback*/
      if v_draw_back > 0
      then

         psequencia := 0;
         begin
            select obrf_018.sequencia_entrada into psequencia
            from obrf_018
            where obrf_018.empresa            = p_empresa
              and obrf_018.draw_back_entrada  = v_draw_back
              and obrf_018.nivel_insumo       = reg_obrf015.pnivel
              and obrf_018.grupo_insumo       = reg_obrf015.pgrupo
              and obrf_018.subgrupo_insumo    = reg_obrf015.psub
              and obrf_018.item_insumo        = reg_obrf015.pitem;
            exception
              when others then
                 psequencia := 0;
         end;

         if psequencia > 0
         then


            total_debito := 0.00;
            total_debito := reg_obrf015.pqtde * reg_obrf015.pvlr_uni;

            /*atualiza nota de debito*/
            begin
               update obrf_018
                 set obrf_018.valor_debitos = obrf_018.valor_debitos + total_debito
                 where obrf_018.empresa            = p_empresa
                   and obrf_018.draw_back_entrada  = v_draw_back
                   and obrf_018.nivel_insumo       = reg_obrf015.pnivel
                   and obrf_018.grupo_insumo       = reg_obrf015.pgrupo
                   and obrf_018.subgrupo_insumo    = reg_obrf015.psub
                   and obrf_018.item_insumo        = reg_obrf015.pitem;
            end;

         end if;

      end if;



   end loop;
end inter_pr_atualiza_debito;



 

/

exec inter_pr_recompile;

