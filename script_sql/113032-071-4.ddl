insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('obrf_f279', 'Entrada para lotes multiplos',0,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'obrf_f279', ' ' ,1, 0, 'S', 'S', 'S', 'S');

update hdoc_036
set hdoc_036.descricao       = 'Entrada para lotes multiplos'
where hdoc_036.codigo_programa = 'obrf_f279'
  and hdoc_036.locale          = 'es_ES';

exec inter_pr_recompile;
