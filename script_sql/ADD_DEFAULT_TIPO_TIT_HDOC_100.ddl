alter table hdoc_100
modify tipo_titulo numeric(2) default 0;

update hdoc_100
set tipo_titulo = 0
where tipo_titulo is null;
