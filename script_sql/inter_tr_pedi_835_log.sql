
  CREATE OR REPLACE TRIGGER "INTER_TR_PEDI_835_LOG" 
   after insert
   or delete
   or update of
	   pedido_venda, codigo_variante, seq_variante, cor_sortimento,
	   comprimento, cod_cor_urdume, cod_cor_overloque, lado
   on pedi_835
   for each row

declare
   ws_usuario_rede          varchar2(20);
   ws_maquina_rede          varchar2(40);
   ws_aplicativo            varchar2(20);
   long_aux                 varchar2(2000);
begin

    if inserting
    then


       select substr(osuser,1,20), substr(machine,1,40), substr(program,1,20)
       into   ws_usuario_rede,     ws_maquina_rede,       ws_aplicativo
       from sys.gv_$session
       where audsid  = userenv('SESSIONID')
         and inst_id = userenv('INSTANCE')
         and rownum < 2;


       INSERT INTO hist_100
          ( tabela,            operacao,
            data_ocorr,        aplicacao,
            usuario_rede,      maquina_rede,
            num01,             long01
          )
       VALUES
          ( 'PEDI_835',        'I',
            sysdate,           ws_aplicativo,
            ws_usuario_rede,   ws_maquina_rede,
            :new.pedido_venda,
            '                              PEDIDO DE VENDAS - VARIACOES'||
                                     chr(10)                  ||
                                     chr(10)                  ||
            'PEDIDO VENDA.....: ' || :new.pedido_venda        ||
            chr(10)                                           ||
            'CODIGO VARIANTE..: ' || :new.codigo_variante     ||
            chr(10)                                           ||
            'SEQ VARIANTE.....: ' || :new.seq_variante        ||
            chr(10)                                           ||
            'COR SORTIMENTO...: ' || :new.cor_sortimento      ||
            chr(10)                                           ||
            'COMPRIMENTO......: ' || :new.comprimento         ||
            chr(10)                                           ||
            'COD COR URDUME...: ' || :new.cod_cor_urdume      ||
            chr(10)                                           ||
            'COD COR OVERLOQUE: ' || :new.cod_cor_overloque   ||
            chr(10)                                           ||
            'LADO.............: ' || :new.lado
         );
    end if;

    if updating and
       (:old.pedido_venda        <> :new.pedido_venda      or
        :old.codigo_variante     <> :new.codigo_variante   or
        :old.seq_variante        <> :new.seq_variante      or
        :old.cor_sortimento      <> :new.cor_sortimento    or
        :old.comprimento         <> :new.comprimento       or
        :old.cod_cor_urdume      <> :new.cod_cor_urdume    or
        :old.cod_cor_overloque   <> :new.cod_cor_overloque or
        :old.lado                <> :new.lado
      )
    then
       select substr(osuser,1,20), substr(machine,1,40), substr(program,1,20)
       into   ws_usuario_rede,     ws_maquina_rede,       ws_aplicativo
       from sys.gv_$session
       where audsid  = userenv('SESSIONID')
         and inst_id = userenv('INSTANCE')
         and rownum < 2;

       long_aux := long_aux ||
                  '                              PEDIDO DE VENDAS - VARIACOES'||
                   chr(10)                    ||
                   chr(10);

       if  :old.pedido_venda <> :new.pedido_venda
       then
           long_aux := long_aux ||
                'PEDIDO VENDA....: ' || :old.pedido_venda    || ' - '
                                     || :new.pedido_venda
                                     || chr(10);
       end if;

       if  :old.codigo_variante  <> :new.codigo_variante
       then
           long_aux := long_aux ||
                'CODIGO VARIANTE.: ' || :old.codigo_variante     || ' - '
                                     || :new.codigo_variante
                                     || chr(10);
       end if;

       if  :old.seq_variante  <> :new.seq_variante
       then
           long_aux := long_aux ||
                'SEQ VARIANTE....: ' || :old.seq_variante     || ' - '
                                     || :new.seq_variante
                                     || chr(10);
       end if;

       if  :old.cor_sortimento  <> :new.cor_sortimento
       then
           long_aux := long_aux ||
                'COR SORTIMENTO..: ' || :old.cor_sortimento     || ' - '
                                     || :new.cor_sortimento
                                     || chr(10);
       end if;

       if  :old.comprimento  <> :new.comprimento
       then
           long_aux := long_aux ||
                'COMPRIMENTO.....: ' || :old.comprimento     || ' - '
                                     || :new.comprimento
                                     || chr(10);
       end if;

       if  :old.cod_cor_urdume  <> :new.cod_cor_urdume
       then
           long_aux := long_aux ||
                'COD COR URDUME..: ' || :old.cod_cor_urdume     || ' - '
                                     || :new.cod_cor_urdume
                                     || chr(10);
       end if;

       if  :old.cod_cor_overloque  <> :new.cod_cor_overloque
       then
           long_aux := long_aux ||
                'COD COR OVER....: ' || :old.cod_cor_overloque     || ' - '
                                     || :new.cod_cor_overloque
                                     || chr(10);
       end if;

       if  :old.lado  <> :new.lado
       then
           long_aux := long_aux ||
                'LADO............: ' || :old.lado     || ' - '
                                     || :new.lado
                                     || chr(10);
       end if;


       INSERT INTO hist_100
          ( tabela,            operacao,
            data_ocorr,        aplicacao,
            usuario_rede,      maquina_rede,
            num01,             long01
          )
       VALUES
          ( 'PEDI_835',        'A',
            sysdate,           ws_aplicativo,
            ws_usuario_rede,   ws_maquina_rede,
            :new.pedido_venda, long_aux
         );
    end if;

    if deleting
    then


       select substr(osuser,1,20), substr(machine,1,40), substr(program,1,20)
       into   ws_usuario_rede,     ws_maquina_rede,       ws_aplicativo
       from sys.gv_$session
       where audsid  = userenv('SESSIONID')
         and inst_id = userenv('INSTANCE')
         and rownum < 2;


       INSERT INTO hist_100
          ( tabela,            operacao,
            data_ocorr,        aplicacao,
            usuario_rede,      maquina_rede,
            num01
          )
       VALUES
          ( 'PEDI_835',        'D',
            sysdate,           ws_aplicativo,
            ws_usuario_rede,   ws_maquina_rede,
            :old.pedido_venda
         );
    end if;

end inter_tr_pedi_835_log;

-- ALTER TRIGGER "INTER_TR_PEDI_835_LOG" ENABLE
 

/

exec inter_pr_recompile;

