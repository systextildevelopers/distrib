create or replace PACKAGE BODY ST_PCK_BAIXA_TITULO AS

    function exists_titulo( p_nr_duplicata IN varchar2,   p_parcela IN varchar2,  p_cgc9 IN number, 
                            p_cgc4 IN number,             p_cgc2 IN number,       p_tipo_titulo IN number) return boolean
     AS
        v_nr_adiantam number;
        v_exists numeric;
    BEGIN
        select 1
        into v_exists
        from cpag_015
        where cpag_015.dupl_for_nrduppag = p_nr_duplicata
        and   cpag_015.dupl_for_no_parc  = p_parcela
        and   cpag_015.dupl_for_for_cli9 = p_cgc9
        and   cpag_015.dupl_for_for_cli4 = p_cgc4
        and   cpag_015.dupl_for_for_cli2 = p_cgc2
        and   cpag_015.dupl_for_tipo_tit = p_tipo_titulo
        and   ROWNUM = 1;

        return (v_exists = 1);
    EXCEPTION WHEN 
    others then
        return false;
    END exists_titulo;

    function new_nr_titulo( p_cod_empresa IN number,    p_tipo_titulo IN number,
                            p_cgc9 IN number,           p_cgc4 IN number,
                            p_cgc2 IN number,           p_parcela in varchar2,
                            p_des_erro OUT varchar2,    p_has_error OUT boolean) return number
     AS
        v_num_titulo number;
        v_nr_inicial number;
        tmpInt number;
        busca_nova_sequencia boolean;
    BEGIN
        p_des_erro := '';
        v_num_titulo := 0;
        p_has_error := false;
        v_nr_inicial := 0;
        busca_nova_sequencia := true;

        begin
            select registros_vazios
            into v_num_titulo
            from(
                select ROWNUM as registros_vazios
                from dual
                CONNECT BY LEVEL <= 999999 
                MINUS
                select nr_duplicata
                from cpag_010
                where cpag_010.parcela = p_parcela
                and   cpag_010.tipo_titulo = p_tipo_titulo
                and   cpag_010.cgc_9 = p_cgc9
                and   cpag_010.cgc_4 = p_cgc4
                and   cpag_010.cgc_2 = p_cgc2
            )
            where ROWNUM = 1;

        EXCEPTION WHEN 
        others then
            p_has_error := true;
            p_des_erro := 'Erro criar o sequencia do adiantamento. ERRO: ' || SQLERRM;
            v_num_titulo := 0;
            return v_num_titulo;
        end;

        return v_num_titulo;
    EXCEPTION WHEN 
    others then
        p_has_error := true;
        p_des_erro := 'Erro PROCEDURE criar o sequencia do adiantamento. ERRO: ' || SQLERRM;
        v_num_titulo := 0;

        return v_num_titulo;
    END new_nr_titulo;

    --Adiantamento relacionado 

    --Retorna uma string concatenada por + para os ids e ; para os registros
    --Pode pegar o registro unico utilizando o id da integracao
    function exists_integracao( p_tipos_titulo IN varchar2,     p_tipo_adiantamento IN number,
                                p_id_integracao OUT varchar2,   p_nr_adiantam OUT number,
                                p_retorno_ids OUT varchar2) return boolean
     AS
        v_nr_adiantam number;
        v_id_integracao varchar2(4000);
        v_exists boolean;
        sqlFormatadoBaixa varchar2(10000);
        a  SYS_REFCURSOR;
        v_numero_adiantamento number;
        v_cgc_9 number; 
        v_cgc_4 number;
        v_cgc_2 number;
    BEGIN
        v_exists := false;

        sqlFormatadoBaixa := 'select id_integracao, numero_adiantamento,
                                     cgc_9, cgc_4, cgc_2
                            from (  select  c.num_importacao as id_integracao, a.numero_adiantam as numero_adiantamento,
                                            c.CGC_9, c.CGC_4, c.CGC_2
                                    from cpag_015 b, cpag_010 a, cpag_200 c
                                    where a.NR_DUPLICATA = b.DUPL_FOR_NRDUPPAG
                                    and   a.PARCELA = b.DUPL_FOR_NO_PARC
                                    and   a.CGC_9 = b.DUPL_FOR_FOR_CLI9
                                    and   a.CGC_4 = b.DUPL_FOR_FOR_CLI4
                                    and   a.CGC_2 = b.DUPL_FOR_FOR_CLI2
                                    and   a.TIPO_TITULO = b.DUPL_FOR_TIPO_TIT
                                    and   a.numero_adiantam > 0
                                    and   a.saldo_titulo = 0
                                    and   a.tipo_titulo in (' || p_tipos_titulo || ')
                                    and   a.numero_adiantam = c.NUMERO_ADIANTAM
                                    and   c.TIPO_ADIANTAM = ' || p_tipo_adiantamento || '
                                    and   a.cgc_9 = c.CGC_9
                                    and   a.cgc_4 = c.CGC_4
                                    and   a.cgc_2 = c.CGC_2
                                    and   c.num_importacao LIKE ''%EXPEN-AD%''
                                    and   (trim(b.integrado) is null or trim(b.integrado) = 0)
                                    GROUP BY c.num_importacao, a.numero_adiantam, 
                                            c.CGC_9, c.CGC_4, c.CGC_2
                                    UNION ALL
                                    select  a.num_importacao as id_integracao, a.nr_duplicata as numero_duplicata,
                                            c.CGC_9, c.CGC_4, c.CGC_2
                                    from cpag_015 b, cpag_010 a, cpag_200 c
                                    where a.nr_duplicata = b.DUPL_FOR_NRDUPPAG
                                    and   a.PARCELA = b.DUPL_FOR_NO_PARC
                                    and   a.CGC_9 = b.DUPL_FOR_FOR_CLI9
                                    and   a.CGC_4 = b.DUPL_FOR_FOR_CLI4
                                    and   a.CGC_2 = b.DUPL_FOR_FOR_CLI2
                                    and   a.TIPO_TITULO = b.DUPL_FOR_TIPO_TIT
                                    and   a.tipo_titulo in (' || p_tipos_titulo || ')
                                    and   b.numero_documento > 0
                                    and   b.pago_adiantam = 1
                                    and   a.saldo_titulo = 0
                                    and   b.numero_documento = c.NUMERO_ADIANTAM
                                    and   c.TIPO_ADIANTAM = ' || p_tipo_adiantamento || '
                                    and   b.DUPL_FOR_FOR_CLI9 = c.CGC_9
                                    and   b.DUPL_FOR_FOR_CLI4 = c.CGC_4
                                    and   b.DUPL_FOR_FOR_CLI2 = c.CGC_2
                                    and   a.num_importacao LIKE ''%EXPEN-RT%''
                                    and   (trim(b.integrado) is null or trim(b.integrado) = 0)
                                    GROUP BY a.num_importacao, a.nr_duplicata, 
                                            c.CGC_9, c.CGC_4, c.CGC_2
                                    UNION ALL
                                    select  a.num_importacao as id_integracao, a.nr_duplicata as numero_duplicata,
                                            b.DUPL_FOR_FOR_CLI9 CGC_9, b.DUPL_FOR_FOR_CLI4 CGC_4, b.DUPL_FOR_FOR_CLI2 CGC_2
                                    from cpag_015 b, cpag_010 a
                                    where a.nr_duplicata = b.DUPL_FOR_NRDUPPAG
                                    and   a.PARCELA = b.DUPL_FOR_NO_PARC
                                    and   a.CGC_9 = b.DUPL_FOR_FOR_CLI9
                                    and   a.CGC_4 = b.DUPL_FOR_FOR_CLI4
                                    and   a.CGC_2 = b.DUPL_FOR_FOR_CLI2
                                    and   a.TIPO_TITULO = b.DUPL_FOR_TIPO_TIT
                                    and   a.tipo_titulo in (' || p_tipos_titulo || ')
                                    and   b.pago_adiantam = 0
                                    and   a.saldo_titulo = 0
                                    and   a.num_importacao LIKE ''%EXPEN-RT%''
                                    and   (trim(b.integrado) is null or trim(b.integrado) = 0)
                                    and not exists ( select 1 from cpag_200 c
                                                    where c.NUMERO_ADIANTAM   = b.numero_documento
                                                    and c.TIPO_ADIANTAM     = ' || p_tipo_adiantamento || '
                                                    and c.CGC_9             = b.DUPL_FOR_FOR_CLI9
                                                    and c.CGC_4             = b.DUPL_FOR_FOR_CLI4
                                                    and c.CGC_2             = b.DUPL_FOR_FOR_CLI2
                                    )
                                    GROUP BY a.num_importacao, a.nr_duplicata, 
                                            b.DUPL_FOR_FOR_CLI9, b.DUPL_FOR_FOR_CLI4, b.DUPL_FOR_FOR_CLI2
                                )
                                WHERE ROWNUM < 101';
        EXECUTE IMMEDIATE sqlFormatadoBaixa;
        OPEN a FOR sqlFormatadoBaixa;
        loop
            FETCH a INTO v_id_integracao, v_numero_adiantamento,
                         v_cgc_9, v_cgc_4, v_cgc_2;
            EXIT WHEN a%NOTFOUND;

            v_exists := true;
            p_id_integracao := v_id_integracao;
            p_nr_adiantam   := v_numero_adiantamento;
            if  v_id_integracao is not null and v_numero_adiantamento is not null then
                if p_retorno_ids is null then
                    p_retorno_ids := v_id_integracao 
                                        || '+' || v_numero_adiantamento
                                        || '+' || v_cgc_9
                                        || '+' || v_cgc_4
                                        || '+' || v_cgc_2;
                else 
                    p_retorno_ids := p_retorno_ids 
                                    || ';' || v_id_integracao 
                                    || '+' || v_numero_adiantamento
                                    || '+' || v_cgc_9
                                    || '+' || v_cgc_4
                                    || '+' || v_cgc_2;
                end if;
            end if;
        end loop;

        return v_exists;
    EXCEPTION WHEN 
    others then
        p_id_integracao := '0';
        p_nr_adiantam   := 0;
        
        raise_application_error(-20001, 'Ocorreu um erro na procedure de busca dos títulos, contate o administrador.' ||SQLERRM);
    END exists_integracao;

    
    --Passar a sequencia, esta function a ideia é contabilizar a inserir os dados.
    function baixa_titulo(  p_nr_duplicata IN varchar2,  p_parcela IN varchar2,     p_cgc9 IN number, 
                            p_cgc4 IN number,            p_cgc2 IN number,          p_tipo_titulo IN number,
                            p_sequencia_pagto IN number, p_data_pgmt IN varchar2,   p_numero_documento IN number,
                            p_cod_historico IN number,   p_valor_pago IN number,    p_cod_portador IN number,
                            p_nr_conta_porta IN number,  p_transacao IN OUT number,     p_data_baixa IN varchar2,
                            p_num_contabil IN varchar2,  p_empresa IN number,       p_centro_custo IN number,
                            p_cod_contab_deb IN OUT number,  p_pago_adiantam IN number) return varchar2
     AS
        v_des_erro varchar2(1000);
        v_origem varchar2(4000);
        v_cod_contab_for_cre number;
        v_cod_contab_cre     number;
        p_lancamento_contabil number;
        v_cod_cont_banco number;
        v_gera_contab_conta number;
    begin
        declare
            p_has_error boolean;
            v_origem number;
            v_contabiliza_trans number;
            v_tipo_contab_deb number;
        BEGIN
            p_has_error := false;
            p_lancamento_contabil := 0;
            v_origem := 6;--Fixo para baixa_titulo

            begin
                select supr_010.codigo_contabil
                into v_cod_contab_for_cre
                from supr_010 
                where supr_010.fornecedor9 = p_cgc9
                and   supr_010.fornecedor4 = p_cgc4
                and   supr_010.fornecedor2 = p_cgc2;
            exception
            when no_data_found then
                v_cod_contab_for_cre := 0;
            end;

            begin
                select atualiza_contabi
                INTO v_contabiliza_trans
                from estq_005
                where codigo_transacao = p_transacao;
                
                select  codigo_contabil, gera_contabil
                INTO v_cod_contab_cre, v_gera_contab_conta
                from cpag_020
                where portador = p_cod_portador
                and   conta_corrente = p_nr_conta_porta;
            EXCEPTION WHEN 
            others then
                v_contabiliza_trans := 0;
                v_cod_contab_cre := 0;
                v_gera_contab_conta := 0;
            end;
            
            if v_contabiliza_trans <> 2 and v_gera_contab_conta = 1 then
                if v_contabiliza_trans = 3 then
                    v_tipo_contab_deb := 4;
                else
                    v_tipo_contab_deb := 2;
                    p_cod_contab_deb := v_cod_contab_for_cre;
                end if; 

                inter_pr_contabilizacao(p_empresa,              p_centro_custo,     p_cgc9,   p_cgc4,   p_cgc2, 
                                        p_transacao,            p_cod_historico,    p_nr_conta_porta,   p_cod_contab_deb,  
                                        v_cod_contab_cre,       p_nr_duplicata,     p_data_pgmt,        p_valor_pago,
                                        v_origem,               v_tipo_contab_deb,  20,                 p_lancamento_contabil,  
                                        v_des_erro);

                if v_des_erro is not null then
                    v_des_erro := ' Ocorreu um erro ao realizar a contabilização do adiantamento. ERRO: ' || SQLERRM;
                    return v_des_erro;
                end if;
            end if;
        end;

        begin
            insert into cpag_015 (
                dupl_for_nrduppag,  dupl_for_no_parc,   dupl_for_for_cli9, 
                dupl_for_for_cli4,  dupl_for_for_cli2,  dupl_for_tipo_tit,
                sequencia_pagto,    data_pagamento,     numero_documento, 
                codigo_historico,   valor_pago,         cd_porta_cod_port,
                cd_porta_nr_conta,  codigo_transacao,   data_baixa,
                num_contabil,       pago_adiantam
            ) VALUES (
                p_nr_duplicata,     p_parcela,          p_cgc9,
                p_cgc4,             p_cgc2,             p_tipo_titulo, 
                p_sequencia_pagto,  p_data_pgmt,        p_numero_documento,
                p_cod_historico,    p_valor_pago,       p_cod_portador, 
                p_nr_conta_porta,   p_transacao,        p_data_baixa,
                p_lancamento_contabil,  p_pago_adiantam
            );
        EXCEPTION WHEN 
        others then
            v_des_erro := 'Erro ao baixar titulo no ERP!. Nr. Dupli.: ' || p_nr_duplicata || ' ERRO: ' || SQLERRM;
        end;
    
        return v_des_erro;
    end baixa_titulo;

    function sequencia_baixa(  p_nr_duplicata IN varchar2,  p_parcela IN varchar2,     p_cgc9 IN number, 
                            p_cgc4 IN number,            p_cgc2 IN number,          p_tipo_titulo IN number) return number
    AS
        v_seq_bx_titulo number;
    begin
        begin
            select nvl(max(SEQUENCIA_PAGTO), 0) + 1
            into v_seq_bx_titulo
            from cpag_015
            where dupl_for_nrduppag = p_nr_duplicata
            and   dupl_for_no_parc = p_parcela
            and   dupl_for_for_cli9 = p_cgc9
            and   dupl_for_for_cli4 = p_cgc4
            and   dupl_for_for_cli2 = p_cgc2
            and   dupl_for_tipo_tit = p_tipo_titulo;
        EXCEPTION WHEN 
        others then
            v_seq_bx_titulo := 1;
        end;

        return v_seq_bx_titulo;
    end sequencia_baixa;

    function pagamento_titulo(  p_cgc9              IN number,      p_cgc4              IN number,      p_cgc2             IN number,      
                                p_tipo_titulo       IN number,      p_codigo_transacao  IN OUT number,      p_empresa          IN number,      
                                p_centro_custo      IN number,      p_emitente_titulo   IN varchar2,    p_valor_parcela    IN number,      
                                p_data_contrato     IN date,        p_data_vencimento   IN date,        p_cod_contab_deb   IN number,      
                                p_codigo_historico  IN number,      p_cod_portador      IN number,      p_num_adiantam     IN number,      
                                p_conta             IN number,      p_origem_debito     IN varchar2,    p_usuario_logado   IN varchar2,
                                p_parcela           IN varchar2,    p_baixar_titulo     IN boolean,     p_referencia       IN varchar2,
                                p_valor_estorno IN number,
                                p_lancamento_contabil IN OUT number
                            ) return varchar2
    AS
        v_contabiliza_trans estq_005.codigo_transacao%type;
        v_num_lcto number;
        v_des_erro varchar2(1000);  
        v_cod_contab_cre number;
        v_valor_parcela number;
    BEGIN
        v_des_erro := '';
		
        declare
            p_has_error boolean;
            v_origem number;
        BEGIN
            p_has_error := false;
            v_origem := 5;--Fixo para baixa_titulo

            begin
                select supr_010.codigo_contabil
                into v_cod_contab_cre
                from supr_010 
                where supr_010.fornecedor9 = p_cgc9
                and   supr_010.fornecedor4 = p_cgc4
                and   supr_010.fornecedor2 = p_cgc2;
            exception
            when no_data_found then
                v_cod_contab_cre := 0;
            end;
          
            inter_pr_contabilizacao(p_empresa,          p_centro_custo,     p_cgc9,   p_cgc4,            p_cgc2, 
                                    p_codigo_transacao, p_codigo_historico, 0,        p_cod_contab_deb,  v_cod_contab_cre, 
                                    p_num_adiantam,         
                                    p_data_contrato,    p_valor_parcela,    v_origem, 4,                 2,
                                    p_lancamento_contabil,     v_des_erro);
                               
            if v_des_erro is not null then
                v_des_erro := ' Ocorreu um erro ao realizar a contabilização do adiantamento. ERRO: ' || v_des_erro || ' p_num_adiantam => ' || p_num_adiantam
                                                                                                    || ' p_cgc9 => ' || p_cgc9
                                                                                                    || ' p_cgc4 =>' || p_cgc4
                                                                                                    || ' p_cgc2 =>' || p_cgc2
                                                                                                    || ' p_empresa =>' || p_empresa
                                                                                                    || ' p_tipo_titulo =>' || p_tipo_titulo
                                                                                                    || ' p_data_contrato =>' || p_data_contrato
                                                                                                    || ' p_data_vencimento =>' || p_data_vencimento
                                                                                                    || ' p_valor_parcela =>' || p_valor_parcela
                                                                                                    || ' p_codigo_historico =>' || p_codigo_historico
                                                                                                    || ' p_codigo_transacao =>' || p_codigo_transacao
                                                                                                    || ' p_emitente_titulo =>' || p_emitente_titulo
                                                                                                    || ' p_cod_portador =>' || p_cod_portador 
                                                                                                    || ' p_centro_custo =>' || p_centro_custo 
                                                                                                    || ' p_num_adiantam =>' || p_num_adiantam 
                                                                                                    || ' p_cod_contab_deb =>' || p_cod_contab_deb
                                                                                                    || ' v_cod_contab_cre =>' || v_cod_contab_cre
                                                                                                    || ' v_num_lcto =>' || p_lancamento_contabil
                                                                                                    || ' p_parcela =>' || p_parcela;
                return v_des_erro;
            end if; 
        
            v_valor_parcela := 0;
            if p_valor_estorno > 0 then
                v_valor_parcela := p_valor_parcela + p_valor_estorno;
            else
                v_valor_parcela := p_valor_parcela;
            end if;

            INTER_PR_INSERE_TITULO (
                p_num_adiantam,           p_parcela,
                p_cgc9,                   p_cgc4,
                p_cgc2,                   p_tipo_titulo,
                p_empresa,                -1,
                p_emitente_titulo,        -1,
                '0',                      p_data_contrato,
                p_data_vencimento,        p_data_contrato,
                p_centro_custo,           p_cod_portador,
                p_codigo_historico,       p_codigo_transacao,
                -1,                       p_cod_contab_deb,
                -1,                       -1,
                p_origem_debito,          -1,
                v_valor_parcela,          -1,
                -1,                       -1,
                -1,                       -1,
                -1,                       -1,
                -1,                       -1,
                -1,                       -1,
                -1,                       -1,
                -1,                       -1,
                -1,                       -1,
                -1,                       -1,
                -1,                       -1,
                -1,                       -1,
                -1,                       -1,
                -1,                       -1,
                -1,                       -1,
                -1,                       -1,
                -1,                       -1,
                -1,                       -1,
                -1,                       -1,
                -1,                       '0',
                p_lancamento_contabil,    p_usuario_logado,
                p_referencia,
                p_has_error,              v_des_erro 
            );
        EXCEPTION
        WHEN OTHERS
        THEN
            v_des_erro := 'Ocorreu um erro ao transformar o adiantamento. ERRO: ' || SQLERRM || ' p_num_adiantam => ' || p_num_adiantam
                                                                                                --|| ' p_cgc9 => ' || p_cgc9
                                                                                                --|| ' p_cgc4 =>' || p_cgc4
                                                                                                --|| ' p_cgc2 =>' || p_cgc2
                                                                                                || ' p_empresa =>' || p_empresa
                                                                                                || ' p_tipo_titulo =>' || p_tipo_titulo
                                                                                                || ' p_data_contrato =>' || p_data_contrato
                                                                                                || ' p_data_vencimento =>' || p_data_vencimento
                                                                                                || ' p_valor_parcela =>' || p_valor_parcela
                                                                                                || ' p_codigo_historico =>' || p_codigo_historico
                                                                                                || ' p_codigo_transacao =>' || p_codigo_transacao
                                                                                                || ' p_emitente_titulo =>' || p_emitente_titulo 
                                                                                                || ' p_data_contrato =>' || p_data_contrato 
                                                                                                || ' p_cod_portador =>' || p_cod_portador 
                                                                                                || ' p_centro_custo =>' || p_centro_custo 
                                                                                                || ' p_num_adiantam =>' || p_num_adiantam 
                                                                                                || ' p_cod_contab_deb =>' || p_cod_contab_deb
                                                                                                || ' v_num_lcto =>' || p_lancamento_contabil
                                                                                                || ' p_parcela =>' || p_parcela;
        end;
        return v_des_erro;
    end pagamento_titulo;

    function contabiliza_estorno(  p_cgc9       IN number,      p_cgc4             IN number,      p_cgc2             IN number,      
                                p_tipo_titulo   IN number,      p_codigo_transacao IN OUT number,  p_empresa          IN number,      
                                p_centro_custo  IN number,      p_emitente_titulo  IN varchar2,    p_valor_parcela    IN number,      
                                p_data_contrato IN date,        p_cod_contab_deb   IN number,      p_codigo_historico IN number,      
                                p_cod_portador  IN number,      p_num_adiantam     IN number,
                                p_lancamento_contabil IN OUT number
                            ) return varchar2
    AS
        v_contabiliza_trans estq_005.codigo_transacao%type;
        v_num_lcto number;
        v_des_erro varchar2(1000);  
        v_cod_contab_cre number;
    BEGIN
        v_des_erro := '';
		
        declare
            p_has_error boolean;
            v_origem number;
        BEGIN
            p_has_error := false;
            v_origem := 5;--Fixo para titulo a pagar

            begin
                select supr_010.codigo_contabil
                into v_cod_contab_cre
                from supr_010 
                where supr_010.fornecedor9 = p_cgc9
                and   supr_010.fornecedor4 = p_cgc4
                and   supr_010.fornecedor2 = p_cgc2;
            exception
            when no_data_found then
                v_cod_contab_cre := 0;
            end;
            /*
            raise_application_error(-20001,'CNPJ ' || p_cgc9||p_cgc4||p_cgc2 || ' ' ||
            'v_cod_contab_cre ' || v_cod_contab_cre || ' ' ||
            'p_cod_contab_deb ' || p_cod_contab_deb || ' ' ||
            'p_codigo_transacao ' || p_codigo_transacao || ' ' ||
            'p_lancamento_contabil ' || p_lancamento_contabil);
            */
            inter_pr_contabilizacao(p_empresa,          p_centro_custo,     p_cgc9,   p_cgc4,            p_cgc2, 
                                    p_codigo_transacao, p_codigo_historico, 0,        p_cod_contab_deb,  v_cod_contab_cre, 
                                    p_num_adiantam,         
                                    p_data_contrato,    p_valor_parcela,    v_origem, 4,                 2,
                                    p_lancamento_contabil,     v_des_erro);
                                
            if v_des_erro is not null then
                v_des_erro := ' Ocorreu um erro ao realizar a contabilização do adiantamento. ERRO: ' || v_des_erro || ' p_num_adiantam => ' || p_num_adiantam
                                                                                                    || ' p_cgc9 => ' || p_cgc9
                                                                                                    || ' p_cgc4 =>' || p_cgc4
                                                                                                    || ' p_cgc2 =>' || p_cgc2
                                                                                                    || ' p_empresa =>' || p_empresa
                                                                                                    || ' p_tipo_titulo =>' || p_tipo_titulo
                                                                                                    || ' p_data_contrato =>' || p_data_contrato
                                                                                                    || ' p_valor_parcela =>' || p_valor_parcela
                                                                                                    || ' p_codigo_historico =>' || p_codigo_historico
                                                                                                    || ' p_codigo_transacao =>' || p_codigo_transacao
                                                                                                    || ' p_emitente_titulo =>' || p_emitente_titulo
                                                                                                    || ' p_cod_portador =>' || p_cod_portador 
                                                                                                    || ' p_centro_custo =>' || p_centro_custo 
                                                                                                    || ' p_num_adiantam =>' || p_num_adiantam 
                                                                                                    || ' p_cod_contab_deb =>' || p_cod_contab_deb
                                                                                                    || ' v_cod_contab_cre =>' || v_cod_contab_cre
                                                                                                    || ' v_num_lcto =>' || p_lancamento_contabil;
                return v_des_erro;
            end if; 
        end;
        return v_des_erro;
    end contabiliza_estorno;
    
    function insere_rateio( x IN number,                            p_tipo_titulo IN number,
                            p_nr_duplicata IN number,               p_fornecedor_rateio IN varchar2, 
                            p_cod_transacao_rateio IN OUT number,   p_cod_contabil_rateio IN number,
                            p_centro_custo_rateio IN number,        p_valor_rateio IN number,
                            p_cod_empresa_rateio IN number,         p_emitente_titulo_rateio IN varchar2,
                            p_cod_portador_rateio IN number,        p_date_contrato_rateio IN varchar2,   
                            p_codigo_historico_rateio IN number,    p_retorno_nr_lanc IN OUT number,
                            p_cgc9_out IN OUT number,               p_cgc4_out IN OUT number,
                            p_cgc2_out IN OUT number) return varchar2
    AS
        v_tamanho_cnpj number;
        v_has_error boolean;
        v_date_contrato_rateio varchar2(100);
        v_mensagem_retorno varchar2(4000);
        v_des_erro varchar2(4000);
        v_cgc9 number;
        v_cgc4 number;
        v_cgc2 number;
        v_formatted_date_rateio date;
        
    BEGIN
        v_mensagem_retorno := '';
        
        begin
            select LENGTH(p_fornecedor_rateio)    
            INTO v_tamanho_cnpj
            from dual;

            if(v_tamanho_cnpj = 11) then
                select  to_number(substr(to_char(replace(replace(p_fornecedor_rateio,'.',''),'-',''),'00000000000'),0,10)),
                        0,
                        to_number(substr(to_char(replace(replace(p_fornecedor_rateio,'.',''),'-',''),'00000000000'),11))
                into v_cgc9, v_cgc4, v_cgc2
                from dual;
            else
                select  to_number(substr(to_char(replace(replace(p_fornecedor_rateio,'.',''),'-',''),'000000000000000'),0,10)),
                        to_number(substr(to_char(replace(replace(p_fornecedor_rateio,'.',''),'-',''),'000000000000000'),11, 4)),
                        to_number(substr(to_char(replace(replace(p_fornecedor_rateio,'.',''),'-',''),'000000000000000'),15, 6))
                into v_cgc9, v_cgc4, v_cgc2
                from dual;
            end if;
        EXCEPTION when 
        others then
            v_has_error := true;
            v_mensagem_retorno := 'Formato do cnpj incorreto!.' || SQLERRM;
            raise_application_error(-20001, 'Formato do cnpj incorreto!.' || SQLERRM);
        end;

        if p_date_contrato_rateio is null then
            v_has_error := true;
            v_mensagem_retorno :=  v_mensagem_retorno || 'Data do rateio é obrigatória.';
        else 
            select TRUNC( to_date(p_date_contrato_rateio, 'YYYY-MM-DD"T"HH24:MI:SS"Z"') )
            into v_formatted_date_rateio from dual;
        end if;
        
        v_des_erro := '';
        
        v_des_erro := ST_PCK_BAIXA_TITULO.contabiliza_estorno(
                                v_cgc9,                     v_cgc4,                    v_cgc2,
                                p_tipo_titulo,              p_cod_transacao_rateio,    p_cod_empresa_rateio, 
                                p_centro_custo_rateio,      p_emitente_titulo_rateio,  p_valor_rateio, 
                                v_formatted_date_rateio,    p_cod_contabil_rateio,     p_codigo_historico_rateio,         
                                p_cod_portador_rateio,      p_nr_duplicata,
                                p_retorno_nr_lanc);
        
        if v_des_erro is not null then
            v_has_error := true;
            raise_application_error(-20001, v_des_erro);
        end if;

        p_cgc9_out := v_cgc9;
        p_cgc4_out := v_cgc4;
        p_cgc2_out := v_cgc2;
      
        insert into cpag_090 (
            nr_duplicata, cgc_9, 
            cgc_4, cgc_2, 
            tipo_titulo, transacao, 
            centro_custo, valor_rateio, 
            codigo_contabil
        ) values (
            p_nr_duplicata,         v_cgc9, 
            v_cgc4,                 v_cgc2, 
            p_tipo_titulo,          p_cod_transacao_rateio, 
            p_centro_custo_rateio,  p_valor_rateio, 
            p_cod_contabil_rateio
        );
        return v_des_erro;
    EXCEPTION
            WHEN OTHERS
            THEN
                v_has_error := true;
                v_mensagem_retorno :=   'Rateio já existente, verificar. ' ||
                                        ' Cod Empresa: ' || p_cod_empresa_rateio ||
                                        ' Tipo título: ' || p_tipo_titulo ||
                                        ' Cod transação: ' || p_cod_transacao_rateio ||
                                        ' Centro custo: ' || p_centro_custo_rateio ||
                                        ' Cod contabil: ' || p_cod_contabil_rateio || 
                                        ' ' || SQLERRM;
                return v_mensagem_retorno;
    end insere_rateio;

    function atualiza_rateio( p_nr_lanc IN number,        p_nr_duplicata IN number,
                            p_tipo_titulo IN number,    p_parcela IN varchar2, 
                            p_cgc9_new IN number,       p_cgc4_new IN number,
                            p_cgc2_new IN number) return varchar2
    AS
        v_decr_erro varchar2(4000);
    BEGIN
        v_decr_erro := '';
        begin
            update cpag_010
            set num_contabil = p_nr_lanc,
                tem_rateio_desp = 1
            where nr_duplicata = p_nr_duplicata
            and   tipo_titulo = tipo_titulo
            and   parcela = p_parcela
            and   cgc_9 = p_cgc9_new
            and   cgc_4 = p_cgc4_new
            and   cgc_2 = p_cgc2_new;
        exception when others then
            v_decr_erro := 'Não foi criado o título para atualizar o nr_lancamento';
            return v_decr_erro;
        end;
        return v_decr_erro;
    end atualiza_rateio;

END st_pck_baixa_titulo;
