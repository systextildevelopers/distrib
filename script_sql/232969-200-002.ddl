insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def, item_apex)
values
('estq_fa29', 'Montagem de Produtos', 0, 1, 1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'estq_fa29', 'menu_gp30', 1, 0, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'estq_fa29', 'menu_gp30', 1, 0, 'S', 'S', 'S', 'S');

commit;



insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def, item_apex)
values
('estq_fa30', 'Produtos a serem consumidos na montagem', 0, 0, 1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'estq_fa30', 'menu_gp30', 0, 0, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'estq_fa30', 'menu_gp30', 0, 0, 'S', 'S', 'S', 'S');

commit;



insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def, item_apex)
values
('estq_fa31', 'Produto a ser consumido na montagem', 0, 0, 1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'estq_fa31', 'menu_gp30', 0, 0, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'estq_fa31', 'menu_gp30', 0, 0, 'S', 'S', 'S', 'S');

commit;



insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def, item_apex)
values
('estq_fa32', 'Consulta e Desmontagem de Produtos', 0, 1, 1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'estq_fa32', 'menu_gp30', 1, 0, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'estq_fa32', 'menu_gp30', 1, 0, 'S', 'S', 'S', 'S');

commit;

