CREATE OR REPLACE FUNCTION INTER_FN_GET_PARAM_HOMOLOG RETURN BOOLEAN IS
  temReg number;
BEGIN
  -- Seleciona um valor booleano baseado no resultado da contagem
  SELECT COUNT(*) INTO temReg
  FROM empr_007
  WHERE param = 'obrf.processoEmHomologacao' AND DEFAULT_STR = 'S';

  if temReg > 0
  then
     RETURN TRUE;
  else
     RETURN FALSE;
  end if;

EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN FALSE;

END INTER_FN_GET_PARAM_HOMOLOG;
/
