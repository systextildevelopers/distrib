-- Create table
create table OBRF_926
( ID_926     NUMBER(9) default 0 not null, 
  ID_920     NUMBER(9) default 0 not null,  
  COD_QUADRO NUMBER(9) default 0 not null, 
  COD_OR     VARCHAR2(3) default ' ', 
  VL_OR      NUMBER(15,2) default 0,
  DT_VCTO    NUMBER(8) default 0, 
  COD_REC    VARCHAR2(4000) default ' ',
  NUM_PROC   VARCHAR2(15) default ' ',
  IND_PROC   NUMBER(1) default 0,
  PROC       VARCHAR2(4000) default ' ',
  TXT_COMPL  VARCHAR2(4000) default ' ',
  MES_REF    NUMBER(6) default 0);
  
-- Add comments to the columns 
comment on column OBRF_926.COD_OR    is 'C�digo da obriga��o a recolher, conforme a Tabela 5.4.';
comment on column OBRF_926.VL_OR     is 'Valor da obriga��o a recolher.';
comment on column OBRF_926.DT_VCTO   is 'Data de vencimento da obriga��o.';
comment on column OBRF_926.COD_REC   is 'C�digo de receita referente � obriga��o, pr�prio da unidade da federa��o, conforme legisla��o estadual.';
comment on column OBRF_926.NUM_PROC  is 'N�mero do processo ou auto de infra��o ao qual a obriga��o est� vinculada, se houver.';
comment on column OBRF_926.IND_PROC  is 'Indicador da origem do processo: 0- SEFAZ, 1- Justi�a Federal, 2- Justi�a Estadual, 9- Outros.';
comment on column OBRF_926.PROC      is 'Descri��o resumida do processo que embasou o lan�amento.';
comment on column OBRF_926.TXT_COMPL is 'Descri��o complementar das obriga��es a recolher.';
comment on column OBRF_926.MES_REF   is 'Informe o m�s de refer�ncia no formato �mmaaaa�.';

-- Create/Recreate primary, unique and foreign key constraints 
alter table OBRF_926 add constraint PK_OBRF_926 primary key (ID_926);
alter table OBRF_926 add constraint UNIQ_OBRF_926 unique (ID_920, COD_QUADRO);

create synonym systextilrpt.OBRF_926 for OBRF_926; 

create sequence seq_obrf_926
minvalue 0
maxvalue 99999999999999999999
start with 1
increment by 1;

CREATE OR REPLACE TRIGGER INTER_TR_OBRF_926_SEQ
BEFORE INSERT ON OBRF_926 FOR EACH ROW
DECLARE
    next_value number;
BEGIN
    if :new.ID_926 is null or :new.ID_926 = 0 then
        select seq_obrf_926.nextval
        into next_value from dual;
        :new.ID_926 := next_value;
    end if;
END INTER_TR_OBRF_926_SEQ;

/

exec inter_pr_recompile;
