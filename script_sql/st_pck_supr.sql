create or replace package ST_PCK_SUPR as

    function exists_fornecedor(p_fornecedor9 number, p_fornecedor4 number, p_fornecedor2 number) return boolean;

    function cria_fornecedor(p_cnpj_for9 	    number, 	p_cnpj_for4 		number, 	p_cnpj_for2 	number, 	p_nome 				varchar2, 
							p_telefone  		varchar2, 	p_celular_forne 	varchar2, 	p_email 		varchar2, 	p_nfe_email 		varchar2, 
							p_cep 				varchar2, 	p_endereco 			varchar2, 	p_numero_imovel varchar2, 	p_complemento 		varchar2, 
							p_bairro 			varchar2, 	p_cidade 			varchar2, 	p_empresa 		varchar2, 	p_cod_fornecedor 	varchar2, 
							p_tipo_fornecedor 	number, 	p_codigo_contabil 	number) return varchar2;

END ST_PCK_SUPR;
