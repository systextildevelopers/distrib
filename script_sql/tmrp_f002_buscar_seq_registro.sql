
  CREATE OR REPLACE FUNCTION "TMRP_F002_BUSCAR_SEQ_REGISTRO" 
        (f_nome_programa    varchar2,
         f_codigo_usuario   number,
         f_nr_solicitacao   number,
         f_codigo_empresa   number,
         f_usuario          varchar2,
         f_tipo_registro    number)
RETURN number
IS
   contador   number;

BEGIN
   /*
      VERIFCA SE TEM OB A PROGRAMAR NO SUB-FORM (ob_a_prog)
   */
   select nvl(max(tmrp_615.seq_registro),0) + 1
   into contador
   from tmrp_615
   where tmrp_615.nome_programa    = f_nome_programa
     and tmrp_615.codigo_usuario   = f_codigo_usuario
     and tmrp_615.nr_solicitacao   = f_nr_solicitacao
     and tmrp_615.codigo_empresa   = f_codigo_empresa
     and tmrp_615.usuario          = f_usuario
     and tmrp_615.situacao         = 'X'   /* NAO CALCULADO */
     and tmrp_615.tipo_registro    = f_tipo_registro;

   return(contador);

END tmrp_f002_buscar_seq_registro;

 

/

exec inter_pr_recompile;

