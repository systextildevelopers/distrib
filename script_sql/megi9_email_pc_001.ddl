-- Desabilita trigger de log
alter trigger TRIGGER_HDOC_100_HIST disable;

alter table hdoc_100
add (envia_email_liberacao number(1) default 0); 

-- Habilita trigger de log
alter trigger TRIGGER_HDOC_100_HIST enable;

exec inter_pr_recompile;

