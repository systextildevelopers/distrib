create or replace trigger "INTER_TR_PCPT_020_5"
  after insert or delete or
        update of codigo_deposito, transacao_ent on pcpt_020
  for each row

declare
   v_existe_21        number;
   v_existe_21_origem number;
   v_arriada          pcpt_021.arriada%type;
   v_nuance           pcpt_021.cod_nuance%type;
   v_pontuacao        pcpt_021.pontuacao_qualidade%type;
   v_executa_trigger  number;
   v_rol              number(2);
   v_sequencia_tingimento number(3);
   v_sequencia_acabamento number(3);
   v_cod_tipo_ordem number(9);
   v_ordem_engomagem number(9);
   v_emenda number(12,4);
   v_restricao varchar2(30);
   v_agrupador_prod number(6);
   v_seq_rolada number(2);

begin
   -- INICIO - Logica implementada para controle de "gatilho" das triggers do banco
   -- para atender a implementacao executada para limpeza da base de dados do cliente
   -- e replicacao dos dados para base historico. (SS.38405)
   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;
   -- FINAL - Logica implementada para controle de "gatilho" das triggers do banco. (SS.38405)

   if v_executa_trigger = 0
   then
      -- Inserindo rolo...
      if inserting
      then
        v_rol       := 1;
        v_sequencia_tingimento:= 1;
        v_seq_rolada := 1;
        v_cod_tipo_ordem := 0;

        begin
          select ftec_030.cod_tipo_ordem
          into v_cod_tipo_ordem
          from ftec_030, basi_020
          where ftec_030.cod_prod_global = basi_020.tipo_produto_global
          and basi_020.basi030_nivel030 = :new.panoacab_nivel99
          and basi_020.basi030_referenc = :new.panoacab_grupo
          and basi_020.tamanho_ref      = :new.panoacab_subgrupo
          and rownum <= 1;
        exception when others then
          v_cod_tipo_ordem := 0;
        end;

        --ftec_030.cod_tipo_ordem = 2 tigimento de urdume
        --ftec_030.cod_tipo_ordem = 3 tecelagem
        if v_cod_tipo_ordem = 2 then
            begin
                select a.sequencia_tingimento,a.rolada,a.seq_rolada
                into v_sequencia_tingimento, v_rol,v_seq_rolada
                from pcpb_030 a
                where a.ordem_producao = :new.ordem_producao
                and a.pano_nivel99 = :new.panoacab_nivel99
                and a.pano_grupo = :new.panoacab_grupo
                and a.pano_subgrupo = :new.panoacab_subgrupo
                and a.pano_item = :new.panoacab_item
                and a.codigo_deposito = :new.codigo_deposito
                and a.pedido_corte  = :new.pedido_corte
                and a.sequenci_periodo = :new.sequenci_periodo_dest
                and a.nr_pedido_ordem = :new.nr_pedido_ordem_dest
                and rownum <= 1;
            exception when others then
                v_sequencia_tingimento := 1;
                v_rol := 1;
                v_seq_rolada := 1;
            end;
        end if;

        if v_cod_tipo_ordem = 3 then
  begin
            select a.sequencia_tingimento,a.rolada,a.seq_rolada
            into v_sequencia_tingimento, v_rol, v_seq_rolada
            from pcpb_030 a
            where a.ordem_producao = :new.ordem_producao
            and a.pano_nivel99 = :new.panoacab_nivel99
            and a.pano_grupo = :new.panoacab_grupo
            and a.pano_subgrupo = :new.panoacab_subgrupo
            and a.pano_item = :new.panoacab_item
            and a.codigo_deposito = :new.codigo_deposito
            and a.pedido_corte  = :new.pedido_corte
            and a.sequenci_periodo = :new.sequenci_periodo_dest
            and rownum <= 1;
  exception when others then
        v_sequencia_tingimento := 1;
        v_rol := 1;
        v_seq_rolada := 1;
      end;
        end if;
        if v_cod_tipo_ordem = 4 then
          begin
            select a.sequencia_tingimento,a.rolada, a.seq_rolada
            into v_sequencia_tingimento, v_rol, v_seq_rolada
            from pcpb_030 a
            where a.ordem_producao = :new.ordem_producao
            and a.pano_nivel99 = :new.panoacab_nivel99
            and a.pano_grupo = :new.panoacab_grupo
            and a.pano_subgrupo = :new.panoacab_subgrupo
            and a.pano_item = :new.panoacab_item
            and a.codigo_deposito = :new.codigo_deposito
            and a.pedido_corte  = :new.pedido_corte
            and a.sequenci_periodo = :new.sequenci_periodo_dest
            and rownum <= 1;
   exception when others then
        v_sequencia_tingimento := 1;
        v_rol := 1;
        v_seq_rolada := 1;
          end;
          begin
            select nvl(max(a.sequencia_acabamento),0)+1
            into v_sequencia_acabamento
            from pcpt_021 a , pcpt_020 b
            where a.codigo_rolo = b.codigo_rolo
            and b.ordem_producao = :new.ordem_producao;
          exception when others then
            v_sequencia_acabamento := 1;
          end;
        end if;

         begin
            -- VERIFICA SE O NOVO ROLO JA EXISTE
            begin
               v_existe_21 := 0;

               select count(*)
               into v_existe_21
               from pcpt_021
               where pcpt_021.codigo_rolo = :new.codigo_rolo;
            end;

            -- SE O NOVO ROLO JA EXISTE ATUALIZA AS SUAS INFORMACOES
            if v_existe_21 > 0
            then
               begin
                  select pcpt_021.arriada,    pcpt_021.cod_nuance,
                         pcpt_021.pontuacao_qualidade
                  into   v_arriada,           v_nuance,
                         v_pontuacao
                  from pcpt_021
                  where pcpt_021.codigo_rolo = :new.codigo_rolo;
               end;

               begin
                  update pcpt_021
                  set pcpt_021.transacao_ent_orig  = :new.transacao_ent,
                      pcpt_021.deposito_original   = :new.codigo_deposito,
                      pcpt_021.arriada             = v_arriada,
                      pcpt_021.pontuacao_qualidade = v_pontuacao,
                      pcpt_021.cod_nuance          = v_nuance
                  where pcpt_021.codigo_rolo = :new.codigo_rolo;
               end;
            else
               -- VERIFICA SE O ROLO QUE DEU ORIGEM A ESTE ROLO EXISTE
               begin

                  v_existe_21_origem := 0;

                  select count(*)
                  into v_existe_21_origem
                  from pcpt_021
                  where pcpt_021.codigo_rolo = :new.nr_rolo_origem;
               end;

               -- SE NAO EXISTE O ROLO ORIGEM INSERE O NOVO ROLO SEM INFORMACOES DO ROLO ORIGEM
               if v_existe_21_origem = 0
               then
                  v_arriada   := '';
                  v_nuance    := '';
                  v_pontuacao := 0.00;

                  if v_cod_tipo_ordem = 4 then
                    begin
                       insert into pcpt_021 (
                           pcpt_021.codigo_rolo,         pcpt_021.transacao_ent_orig,
                           pcpt_021.deposito_original,   pcpt_021.arriada,
                           pcpt_021.cod_nuance,          pcpt_021.pontuacao_qualidade,
                           pcpt_021.sequencia_tingimento,pcpt_021.rolada,
                           pcpt_021.sequencia_acabamento,pcpt_021.seq_rolada
                       ) values (
                           :new.codigo_rolo,             :new.transacao_ent,
                           :new.codigo_deposito,         v_arriada,
                           v_nuance,                     v_pontuacao,
                           v_sequencia_tingimento,       v_rol,
                           v_sequencia_acabamento,       v_seq_rolada
                       );
                    exception when others then
                      update pcpt_021
                        set
                        pcpt_021.transacao_ent_orig   = :new.transacao_ent,
                        pcpt_021.deposito_original    = :new.codigo_deposito,
                        pcpt_021.arriada              = v_arriada,
                        pcpt_021.cod_nuance           = v_nuance,
                        pcpt_021.pontuacao_qualidade  = v_pontuacao ,
                        pcpt_021.sequencia_tingimento = v_sequencia_tingimento,
                        pcpt_021.rolada               = v_rol,
                        pcpt_021.sequencia_acabamento = v_sequencia_acabamento,
                        pcpt_021.seq_rolada           = v_seq_rolada

                        where pcpt_021.codigo_rolo    = :new.codigo_rolo;
                    end;
                  end if;

                  if v_cod_tipo_ordem = 2 or v_cod_tipo_ordem = 3 then
                      begin
                         insert into pcpt_021 (
                             pcpt_021.codigo_rolo,         pcpt_021.transacao_ent_orig,
                             pcpt_021.deposito_original,   pcpt_021.arriada,
                             pcpt_021.cod_nuance,          pcpt_021.pontuacao_qualidade,
                             pcpt_021.sequencia_tingimento,pcpt_021.rolada,
                             pcpt_021.seq_rolada
                         ) values (
                             :new.codigo_rolo,             :new.transacao_ent,
                             :new.codigo_deposito,         v_arriada,
                             v_nuance,                     v_pontuacao,
                             v_sequencia_tingimento,       v_rol,
                             v_seq_rolada
                         );
                         exception when others then
                           update pcpt_021
                             set
                             pcpt_021.transacao_ent_orig   = :new.transacao_ent,
                             pcpt_021.deposito_original    = :new.codigo_deposito,
                             pcpt_021.arriada              = v_arriada,
                             pcpt_021.cod_nuance           = v_nuance,
                             pcpt_021.pontuacao_qualidade  = v_pontuacao ,
                             pcpt_021.sequencia_tingimento = v_sequencia_tingimento,
                             pcpt_021.rolada               = v_rol,
                             pcpt_021.seq_rolada           = v_seq_rolada

                             where pcpt_021.codigo_rolo    = :new.codigo_rolo;
                      end;
                    elsif v_cod_tipo_ordem <> 4 then -- Era ELSE - Modificado para nao reexecutar o insert (error trigger)
                      begin
                         insert into pcpt_021 (
                             pcpt_021.codigo_rolo,         pcpt_021.transacao_ent_orig,
                             pcpt_021.deposito_original,   pcpt_021.arriada,
                             pcpt_021.cod_nuance,          pcpt_021.pontuacao_qualidade
                         ) values (
                             :new.codigo_rolo,             :new.transacao_ent,
                             :new.codigo_deposito,         v_arriada,
                             v_nuance,                     v_pontuacao
                         );
                         exception when others then
                           update pcpt_021
                                  set
                                  pcpt_021.transacao_ent_orig = :new.transacao_ent,
                                  pcpt_021.deposito_original = :new.codigo_deposito,
                                  pcpt_021.arriada = v_arriada,
                                  pcpt_021.cod_nuance = v_nuance,
                                  pcpt_021.pontuacao_qualidade = v_pontuacao
                          where pcpt_021.codigo_rolo = :new.codigo_rolo;
                      end;

                    end if;
               else
                  -- SE EXISTE O ROLO ORIGEM INSERE O NOVO ROLO COM AS INFORMACOES DO ROLO ORIGEM
                  if v_cod_tipo_ordem = 4 then
                    begin
                       insert into pcpt_021 (
                           pcpt_021.codigo_rolo,         pcpt_021.transacao_ent_orig,
                           pcpt_021.deposito_original,   pcpt_021.arriada,
                           pcpt_021.cod_nuance,          pcpt_021.pontuacao_qualidade,
                           pcpt_021.sequencia_tingimento,pcpt_021.rolada,
                           pcpt_021.sequencia_acabamento,pcpt_021.seq_rolada
                       ) values (
                           :new.codigo_rolo,             :new.transacao_ent,
                           :new.codigo_deposito,         v_arriada,
                           v_nuance,                     v_pontuacao,
                           v_sequencia_tingimento,       v_rol,
                           v_sequencia_acabamento,       v_seq_rolada
                       );
                      exception when others then
                        update pcpt_021
                          set
                          pcpt_021.transacao_ent_orig   = :new.transacao_ent,
                          pcpt_021.deposito_original    = :new.codigo_deposito,
                          pcpt_021.arriada              = v_arriada,
                          pcpt_021.cod_nuance           = v_nuance,
                          pcpt_021.pontuacao_qualidade  = v_pontuacao,
                          pcpt_021.sequencia_tingimento = v_sequencia_tingimento,
                          pcpt_021.rolada               = v_rol,
                          pcpt_021.sequencia_acabamento = v_sequencia_acabamento,
                          pcpt_021.seq_rolada           = v_seq_rolada

                          where pcpt_021.codigo_rolo    = :new.codigo_rolo;
                    end;
                  end if;

                  if v_cod_tipo_ordem = 2 or v_cod_tipo_ordem = 3 then
                      begin
                         begin
                            select pcpt_021.arriada,
                                   pcpt_021.pontuacao_qualidade,    pcpt_021.ordem_engomagem,
                                   pcpt_021.restricao,              pcpt_021.agrupador_producao,
                                   pcpt_021.cod_nuance,             pcpt_021.emenda
                            into v_arriada,v_pontuacao, v_ordem_engomagem, v_restricao, v_agrupador_prod,
                            v_nuance, v_emenda
                            from pcpt_021
                            where codigo_rolo = :new.codigo_rolo;
                        exception when others then
                          v_arriada     := 0;
                          v_pontuacao     := 0;
                          v_ordem_engomagem  := 0;
                          v_restricao    := '';
                          v_agrupador_prod  := 0;
                          v_nuance    := '';
                          v_emenda    := 0.0;
                        end;
                         insert into pcpt_021 (
                                codigo_rolo,                     arriada,
                                pontuacao_qualidade,             ordem_engomagem,
                                restricao,                       agrupador_producao,
                                cod_nuance,                      emenda,
                                transacao_ent_orig,              deposito_original,
                                sequencia_tingimento,            rolada,
                                seq_rolada
                                )
                         select :new.codigo_rolo,                pcpt_021.arriada,
                                pcpt_021.pontuacao_qualidade,    pcpt_021.ordem_engomagem,
                                pcpt_021.restricao,              pcpt_021.agrupador_producao,
                                pcpt_021.cod_nuance,             pcpt_021.emenda,
                                :new.transacao_ent,              :new.codigo_deposito,
                                v_sequencia_tingimento,          v_rol,
                                v_seq_rolada
                         from pcpt_021
                         where pcpt_021.codigo_rolo = :new.nr_rolo_origem;
                         exception when others then
                           update pcpt_021
                             set
                             pcpt_021.transacao_ent_orig    = :new.transacao_ent,
                             pcpt_021.deposito_original     = :new.codigo_deposito,
                             pcpt_021.arriada               = v_arriada,
                             pcpt_021.cod_nuance            = v_nuance,
                             pcpt_021.pontuacao_qualidade   = v_pontuacao,
                             pcpt_021.sequencia_tingimento  = v_sequencia_tingimento,
                             pcpt_021.rolada                = v_rol,
                             pcpt_021.sequencia_acabamento  = v_sequencia_acabamento,
                             pcpt_021.seq_rolada            = v_seq_rolada,
                             pcpt_021.pontuacao_qualidade   = v_pontuacao,
                             pcpt_021.ordem_engomagem       = v_ordem_engomagem,
                             pcpt_021.restricao             = v_restricao,
                             pcpt_021.agrupador_producao    = v_agrupador_prod,
                             pcpt_021.cod_nuance            = v_nuance,
                             pcpt_021.emenda                = v_emenda

                             where pcpt_021.codigo_rolo = :new.codigo_rolo;
                  end;
                  elsif v_cod_tipo_ordem <> 4 then -- Era ELSE - Modificado para nao reexecutar o insert (error trigger)
                    begin
                       insert into pcpt_021 (
                              codigo_rolo,                     arriada,
                              pontuacao_qualidade,             ordem_engomagem,
                              restricao,                       agrupador_producao,
                              cod_nuance,                      emenda,
                              transacao_ent_orig,              deposito_original
                              )
                       select :new.codigo_rolo,                pcpt_021.arriada,
                              pcpt_021.pontuacao_qualidade,    pcpt_021.ordem_engomagem,
                              pcpt_021.restricao,              pcpt_021.agrupador_producao,
                              pcpt_021.cod_nuance,             pcpt_021.emenda,
                              :new.transacao_ent,              :new.codigo_deposito
                       from pcpt_021
                       where pcpt_021.codigo_rolo = :new.nr_rolo_origem;
                       exception when others then
                         update pcpt_021
                           set  pcpt_021.arriada              = pcpt_021.arriada,
                                pcpt_021.pontuacao_qualidade  = pcpt_021.pontuacao_qualidade,
                                pcpt_021.ordem_engomagem      = pcpt_021.ordem_engomagem,
                                pcpt_021.restricao            = pcpt_021.restricao,
                                pcpt_021.agrupador_producao   = pcpt_021.agrupador_producao,
                                pcpt_021.cod_nuance           = pcpt_021.cod_nuance,
                                pcpt_021.emenda               = pcpt_021.emenda,
                                pcpt_021.transacao_ent_orig   = :new.transacao_ent,
                                pcpt_021.deposito_original    = :new.codigo_deposito
                          where codigo_rolo = :new.codigo_rolo;
                    end;
                  end if;
              end if;
            end if;
         end;
      end if;

      --- Alterando...
      if updating
      then
         -- Somente tecidoa acabados.
         if :new.panoacab_nivel99 = '4'
         then
            if  :old.codigo_deposito = 0 and :new.codigo_deposito <> 0
            and :old.transacao_ent   = 0 and :new.transacao_ent   <> 0
            then
               begin
                  v_existe_21 := 0;

                  begin
                     select count(*)
                     into v_existe_21
                     from pcpt_021
                     where pcpt_021.codigo_rolo = :new.codigo_rolo;
                  end;
               end;

               if v_existe_21 > 0
               then
                  begin
                     update pcpt_021
                     set   pcpt_021.transacao_ent_orig = :new.transacao_ent,
                           pcpt_021.deposito_original  = :new.codigo_deposito
                     where pcpt_021.codigo_rolo        = :new.codigo_rolo;
                  end;
               else
                  begin
                     insert into pcpt_021
                        (pcpt_021.codigo_rolo,         pcpt_021.transacao_ent_orig,
                         pcpt_021.deposito_original,   pcpt_021.sequencia_tingimento,
                       pcpt_021.rolada)
                     values
                        (:new.codigo_rolo,             :new.transacao_ent,
                         :new.codigo_deposito, 1,1);
                      exception when others then
                          update pcpt_021
                            set
                                  pcpt_021.transacao_ent_orig   = :new.transacao_ent,
                                  pcpt_021.deposito_original    = :new.codigo_deposito,
                                  pcpt_021.sequencia_tingimento = 1,
                                  pcpt_021.rolada               = 1
                            where codigo_rolo = :new.codigo_rolo;

                  end;
               end if;
            end if;
         end if;
      end if;

      if deleting
      then
         begin
            delete from pcpt_021
            where pcpt_021.codigo_rolo = :old.codigo_rolo;
         exception when others then
            raise_application_error(-20000,'ATENCAO! Nao deletou pcpt_021, informacao adicionais do rolo.');
         end;
      end if;
   end if;
end inter_tr_pcpt_020_5;

-- ALTER TRIGGER "INTER_TR_PCPT_020_5" ENABLE
