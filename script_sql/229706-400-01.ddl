alter table mqop_005 add origem_ncm number(1) default 0;

comment on column mqop_005.origem_ncm is 'Origem NCM: 0-NCM do Produto Acabado (Padrao) ou 1-NCM do Componente Principal';
