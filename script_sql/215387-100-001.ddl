alter table PROJ_015 modify SUBGRU_MAQUINA 			  VARCHAR2(4000);
alter table PROJ_015 modify QTDE_MAQ_DISPONIVEL       NUMBER(20,2);
alter table PROJ_015 modify MINUTOS_DISPONIVEL        NUMBER(20,2);
alter table PROJ_015 modify MINUTOS_DIAS_DISPONIVEL   NUMBER(20,2);
alter table PROJ_015 modify QTDE_MAQ_NECESSIDADE      NUMBER(20,4);
alter table PROJ_015 modify MINUTOS_PROD_NECESSIDADE  NUMBER(20,2);
alter table PROJ_015 modify PERC_SETUP_NECESSIDADE    NUMBER(20,2);
alter table PROJ_015 modify MINUTOS_TOTAL_NECESSIDADE NUMBER(20,2);
alter table PROJ_015 modify MINUTOS_DIAS_NECESSIDADE  NUMBER(20,2);
alter table PROJ_015 modify PERC_NECESSIDADE          NUMBER(20,2);
alter table PROJ_015 modify QTDE_NECESSARIA           NUMBER(20,3);
alter table PROJ_015 modify MINUTOS                   NUMBER(20,4);
alter table PROJ_015 modify DIAS_UTEIS                NUMBER(20,2);

alter table PROJ_015 add 	MIN_TOTAL_SUBMAQUINAS     NUMBER(20,2);
alter table PROJ_015 add 	SUBGRU_MAQUINAS_CONS      VARCHAR2(4000);
alter table PROJ_015 add 	SOMA_PERC_NECE_MAQ        NUMBER(20,2);
alter table PROJ_015 add 	QTDE_SUBGRUPOS_MAQ        NUMBER(9);
