create or replace procedure inter_pr_gera_sped_hkestqProp(p_cod_empresa in number,
                                                          p_dat_final   IN DATE,
                                                          p_des_erro    out varchar2) is

  w_mes             number(2);
  w_ano             number(4);
  w_codigo_contabil basi_010.codigo_contabil%type;
  w_uni_med         basi_030.unidade_medida%type;
  w_conta_estoque   basi_030.conta_estoque%type;
  w_valor_unitario  sped_k200_h010.valor_unitario%type;
  v_tipo_produto_sped  sped_k200_h010.tipo_produto_sped%type;
  w_erro EXCEPTION;
  v_conta_reg number;
  v_existe_prod     number(1);
  v_cod_empresa     fatu_500.codigo_empresa%type;
  
  CURSOR u_fatu_500(p_cod_empresa NUMBER) IS
    select fatu_500.cgc_9, fatu_500.cgc_4, fatu_500.cgc_2
      from fatu_500
     where fatu_500.codigo_empresa = p_cod_empresa;

  --
  -- ESTOQUE PRODUTO EM ELABORACAO
  --

  CURSOR u_k200_h010(p_cod_empresa_500 NUMBER, p_dat_final DATE) IS

    select k200_h010.nivel_estrutura nivel_estrutura,
           k200_h010.grupo_estrutura grupo_estrutura,
           k200_h010.subgru_estrutura subgru_estrutura,
           k200_h010.item_estrutura item_estrutura,
           k200_h010.estagio_agrupador estagio_agrupador,
           k200_h010.estagio_agrupador_simultaneo estagio_agrupador_simultaneo,
           sum(k200_h010.quantidade) quantidade,
           k200_h010.seq_agrupador,
           sum(k200_h010.valor_total) valor_total
      from ((select sped_k200_h010.nivel nivel_estrutura,
                    sped_k200_h010.grupo grupo_estrutura,
                    sped_k200_h010.subgrupo subgru_estrutura,
                    sped_k200_h010.item item_estrutura,
                    sped_k200_h010.estagio_agrupador estagio_agrupador,
                    sped_k200_h010.estagio_agrupador_simultaneo estagio_agrupador_simultaneo,
                    sum(sped_k200_h010.quantidade) quantidade,
                    nvl(sped_k200_h010.sequencia_operacao_agrupador,0) as seq_agrupador,
                    sum(sped_k200_h010.valor_total) as valor_total
               from sped_k200_h010
              where sped_k200_h010.empresa = p_cod_empresa_500
                   -- Considera saldo mes anterior
                and sped_k200_h010.ano = extract(year from(trunc(add_months(p_dat_final, -1), 'YYYY')))
                and sped_k200_h010.mes = extract(month from(trunc(add_months(p_dat_final, -1), 'MM')))
                and sped_k200_h010.tipo_propriedade = 0
                and sped_k200_h010.tipo_produto_sped = 3
                and nvl(sped_k200_h010.estagio_agrupador, 0) <> 0
              group by sped_k200_h010.nivel,
                       sped_k200_h010.grupo,
                       sped_k200_h010.subgrupo,
                       sped_k200_h010.item,
                       sped_k200_h010.estagio_agrupador,
                       sped_k200_h010.estagio_agrupador_simultaneo,
                       sped_k200_h010.sequencia_operacao_agrupador)
           union all
           ---------------------- BLOCOK_230 ----------------------------
            (select blocok_230.cod_item_nivel nivel_estrutura,
                    blocok_230.cod_item_grupo grupo_estrutura,
                    blocok_230.cod_item_subgrupo subgru_estrutura,
                    blocok_230.cod_item_item item_estrutura,
                    nvl(blocok_230.cod_item_est_agrup, 0) as estagio_agrupador,
                    0 as estagio_agrupador_simultaneo,
                    sum(blocok_230.qtd_enc) quantidade,
                    nvl(blocok_230.sequencia_operacao_agrupador,0) as seq_agrupador,
                    0 as valor_total
               from blocok_230
              where blocok_230.cod_empresa = p_cod_empresa_500
                and blocok_230.ano_periodo_apur = extract(year from p_dat_final)
                and blocok_230.mes_periodo_apur = extract(month from p_dat_final)
                and nvl(blocok_230.cod_item_est_agrup, 0) <> 0
              group by blocok_230.cod_item_nivel,
                       blocok_230.cod_item_grupo,
                       blocok_230.cod_item_subgrupo,
                       blocok_230.cod_item_item,
                       blocok_230.cod_item_est_agrup,
                       blocok_230.sequencia_operacao_agrupador)

            union all
           ---------------------- BLOCOK_250 ----------------------------
            (select blocok_250.cod_item_nivel nivel_estrutura,
                    blocok_250.cod_item_grupo grupo_estrutura,
                    blocok_250.cod_item_subgrupo subgru_estrutura,
                    blocok_250.cod_item_item item_estrutura,
                    nvl(blocok_250.cod_item_est_agrup, 0) as estagio_agrupador,
                    0 as estagio_agrupador_simultaneo,
                    sum(blocok_250.qtd) quantidade,
                    0 as seq_agrupador,
                    0 as valor_total
               from blocok_250
              where blocok_250.cod_empresa = p_cod_empresa_500
                and blocok_250.ano_periodo_apur = extract(year from p_dat_final)
                and blocok_250.mes_periodo_apur = extract(month from p_dat_final)
                and nvl(blocok_250.cod_item_est_agrup, 0) <> 0

              group by blocok_250.cod_item_nivel,
                       blocok_250.cod_item_grupo,
                       blocok_250.cod_item_subgrupo,
                       blocok_250.cod_item_item,
                       blocok_250.cod_item_est_agrup)

            union all
           ---------------------- BLOCOK_291 ----------------------------
            (select blocok_291.cod_item_nivel nivel_estrutura,
                    blocok_291.cod_item_grupo grupo_estrutura,
                    blocok_291.cod_item_subgrupo subgru_estrutura,
                    blocok_291.cod_item_item item_estrutura,
                    nvl(blocok_291.cod_item_est_agrup, 0) as estagio_agrupador,
                    nvl(blocok_291.cod_item_est_agrup_simult,0) estagio_agrupador_simultaneo ,
                    sum(blocok_291.qtd) quantidade,
                    0 as seq_agrupador,
                    0 as valor_total
               from blocok_291
              where blocok_291.cod_empresa = p_cod_empresa_500
                and blocok_291.ano_periodo_apur = extract(year from p_dat_final)
                and blocok_291.mes_periodo_apur = extract(month from p_dat_final)
                and nvl(blocok_291.cod_item_est_agrup, 0) <> 0

              group by blocok_291.cod_item_nivel,
                       blocok_291.cod_item_grupo,
                       blocok_291.cod_item_subgrupo,
                       blocok_291.cod_item_item,
                       blocok_291.cod_item_est_agrup,
                       blocok_291.cod_item_est_agrup_simult)

            union all
           ---------------------- BLOCOK_235 ----------------------------
            (select blocok_235.cod_item_nivel nivel_estrutura,
                    blocok_235.cod_item_grupo grupo_estrutura,
                    blocok_235.cod_item_subgrupo subgru_estrutura,
                    blocok_235.cod_item_item item_estrutura,
                    nvl(blocok_235.cod_item_est_agrup_insu, 0) as estagio_agrupador,
                    nvl(blocok_235.cod_item_est_simult_insu, 0) as estagio_agrupador_simultaneo,
                    sum(blocok_235.qtd * -1) quantidade,
                    nvl(blocok_235.SEQUENCIA_OPERACAO_AGRUPADOR,0) as seq_agrupador,
                    0 as valor_total
               from blocok_235
              where blocok_235.cod_empresa = p_cod_empresa_500
                and blocok_235.ano_periodo_apur = extract(year from p_dat_final)
                and blocok_235.mes_periodo_apur = extract(month from p_dat_final)
                and nvl(blocok_235.cod_item_est_agrup_insu, 0) <> 0

              group by blocok_235.cod_item_nivel,
                       blocok_235.cod_item_grupo,
                       blocok_235.cod_item_subgrupo,
                       blocok_235.cod_item_item,
                       blocok_235.cod_item_est_agrup_insu,
                       blocok_235.cod_item_est_simult_insu,
                       blocok_235.SEQUENCIA_OPERACAO_AGRUPADOR)

            union all
           ---------------------- BLOCOK_255 ----------------------------
            (select blocok_255.cod_item_nivel nivel_estrutura,
                    blocok_255.cod_item_grupo grupo_estrutura,
                    blocok_255.cod_item_subgrupo subgru_estrutura,
                    blocok_255.cod_item_item item_estrutura,
                    nvl(blocok_255.cod_item_est_agrup_insu, 0) as estagio_agrupador,
                    nvl(blocok_255.cod_item_est_simult_insu, 0) as estagio_agrupador_simultaneo,
                    sum(blocok_255.qtd * -1) quantidade,
                    0 as seq_agrupador,
                    0 as valor_total
               from blocok_255
              where blocok_255.cod_empresa = p_cod_empresa_500
                and blocok_255.ano_periodo_apur = extract(year from p_dat_final)
                and blocok_255.mes_periodo_apur = extract(month from p_dat_final)
                and nvl(blocok_255.cod_item_est_agrup_insu, 0) <> 0

              group by blocok_255.cod_item_nivel,
                       blocok_255.cod_item_grupo,
                       blocok_255.cod_item_subgrupo,
                       blocok_255.cod_item_item,
                       blocok_255.cod_item_est_agrup_insu,
                       blocok_255.cod_item_est_simult_insu)

            union all

           ---------------------- BLOCOK_292 ----------------------------
            (select blocok_292.cod_item_nivel nivel_estrutura,
                    blocok_292.cod_item_grupo grupo_estrutura,
                    blocok_292.cod_item_subgrupo subgru_estrutura,
                    blocok_292.cod_item_item item_estrutura,
                    nvl(blocok_292.cod_item_est_agrup_insu, 0) as estagio_agrupador,
                    nvl(blocok_292.cod_item_est_simult_insu, 0) as estagio_agrupador_simultaneo,
                    sum(blocok_292.qtd * -1) quantidade,
                    0 as seq_agrupador,
                    0 as valor_total
               from blocok_292
              where blocok_292.cod_empresa = p_cod_empresa_500
                and blocok_292.ano_periodo_apur = extract(year from p_dat_final)
                and blocok_292.mes_periodo_apur = extract(month from p_dat_final)
                and nvl(blocok_292.cod_item_est_agrup_insu, 0) <> 0

              group by blocok_292.cod_item_nivel,
                       blocok_292.cod_item_grupo,
                       blocok_292.cod_item_subgrupo,
                       blocok_292.cod_item_item,
                       blocok_292.cod_item_est_agrup_insu,
                       blocok_292.cod_item_est_simult_insu)

            union all

            ---------------------- NOTAS DE ENTRADA ----------------------------
            (select obrf_015.coditem_nivel99 nivel_estrutura,
                    obrf_015.coditem_grupo grupo_estrutura,
                    obrf_015.coditem_subgrupo subgru_estrutura,
                    obrf_015.coditem_item item_estrutura,
                    nvl(obrf_015.cod_estagio_agrupador_insu, 0) as estagio_agrupador,
                    nvl(obrf_015.cod_estagio_simultaneo_insu, 0) as estagio_agrupador_simultaneo,
                    sum(obrf_015.quantidade) quantidade,
                    nvl(obrf_015.seq_operacao_agrupador_insu, 0) as seq_agrupador,
                    sum(obrf_015.valor_total) valor_total
               from obrf_015, obrf_010
              where obrf_010.local_entrega = p_cod_empresa_500
                and obrf_015.capa_ent_nrdoc = obrf_010.documento
                and obrf_015.capa_ent_serie = obrf_010.serie
                and trunc(obrf_010.data_transacao, 'YYYY') = trunc(p_dat_final, 'YYYY')
                and trunc(obrf_010.data_transacao, 'MM') = trunc(p_dat_final, 'MM')
                and nvl(obrf_015.cod_estagio_agrupador_insu, 0) > 0
                and obrf_010.situacao_entrada in (1, 4)
                and obrf_015.capa_ent_forcli9 = obrf_010.cgc_cli_for_9
                and obrf_015.capa_ent_forcli4 = obrf_010.cgc_cli_for_4
                and obrf_015.capa_ent_forcli2 = obrf_010.cgc_cli_for_2

                and (
                   (
                      exists (
                         select 1
                         from blocok_cfop cfop
                         where cfop.codigo_empresa = obrf_010.local_entrega
                      )
                      and not exists (
                          select 1 from blocok_cfop cfop2
                          where cfop2.codigo_empresa = obrf_010.local_entrega
                            and replace(cfop2.cfop,'.','') = replace((select pedi_080.cod_natureza || pedi_080.divisao_natur
                                                              from pedi_080
                                                              where pedi_080.natur_operacao = obrf_015.natitem_nat_oper
                                                                and pedi_080.estado_natoper = obrf_015.natitem_est_oper),'.','')
                            and (cfop2.cfop_de_cobranca = 1 or cfop2.tipo_propriedade = 2)
                     )
                  )
                  or exists (select 1
                             from estq_005
                             where estq_005.codigo_transacao = obrf_015.codigo_transacao
                               and estq_005.tipo_transacao   = 'I'
                               and not exists(select 1
                                              from blocok_cfop cfop
                                              where cfop.codigo_empresa = obrf_010.local_entrega)
                  )
                )
              group by obrf_015.coditem_nivel99,
                       obrf_015.coditem_grupo,
                       obrf_015.coditem_subgrupo,
                       obrf_015.coditem_item,
                       obrf_015.cod_estagio_agrupador_insu,
                       obrf_015.cod_estagio_simultaneo_insu,
                       obrf_015.seq_operacao_agrupador_insu)

            union all

            ---------------------- NOTAS DE SAIDA ----------------------------
            (select fatu_060.nivel_estrutura nivel_estrutura,
                    fatu_060.grupo_estrutura grupo_estrutura,
                    fatu_060.subgru_estrutura subgru_estrutura,
                    fatu_060.item_estrutura item_estrutura,
                    nvl(fatu_060.cod_estagio_agrupador_insu, 0) as estagio_agrupador,
                    nvl(fatu_060.cod_estagio_simultaneo_insu, 0) as estagio_agrupador_simultaneo,
                    sum((fatu_060.qtde_item_fatur * -1)) quantidade,
                    nvl(fatu_060.seq_operacao_agrupador_insu, 0) as seq_agrupador,
                    sum((fatu_060.valor_faturado * -1)) valor_total
               from fatu_060, fatu_050
              where fatu_050.codigo_empresa = p_cod_empresa_500
                and fatu_050.situacao_nfisc in (1, 4)
                and fatu_060.ch_it_nf_cd_empr = fatu_050.codigo_empresa
                and fatu_060.ch_it_nf_num_nfis = fatu_050.num_nota_fiscal
                and fatu_060.ch_it_nf_ser_nfis = fatu_050.serie_nota_fisc
                and trunc(fatu_060.data_emissao, 'YYYY') = trunc(p_dat_final, 'YYYY')
                and trunc(fatu_060.data_emissao, 'MM') = trunc(p_dat_final, 'MM')
                and nvl(fatu_060.cod_estagio_agrupador_insu, 0) > 0
                   -- Considerar somente cadastro de transacao
                and (
                    (
                        exists (
                           select 1
                           from blocok_cfop_saida cfop
                           where cfop.codigo_empresa = fatu_050.codigo_empresa
                        )
                        and not exists (
                            select 1 from blocok_cfop_saida cfop2
                            where cfop2.codigo_empresa = fatu_050.codigo_empresa
                              and replace(cfop2.cfop,'.','') = replace((select pedi_080.cod_natureza || pedi_080.divisao_natur
                                                                        from pedi_080
                                                                        where pedi_080.natur_operacao = fatu_060.natopeno_nat_oper
                                                                          and pedi_080.estado_natoper = fatu_060.natopeno_est_oper),'.','')
                              and (cfop2.cfop_de_cobranca = 1 or cfop2.tipo_propriedade = 2)
                       )
                    )
                    or exists (select 1
                            from estq_005
                            where estq_005.codigo_transacao = fatu_060.transacao
                              and estq_005.tipo_transacao   = 'E'
                              and not exists(select 1
                                             from blocok_cfop_saida cfop
                                             where cfop.codigo_empresa = fatu_050.codigo_empresa)
                    )
                )
              group by fatu_060.nivel_estrutura,
                       fatu_060.grupo_estrutura,
                       fatu_060.subgru_estrutura,
                       fatu_060.item_estrutura,
                       fatu_060.cod_estagio_agrupador_insu,
                       fatu_060.cod_estagio_simultaneo_insu,
                       fatu_060.seq_operacao_agrupador_insu)

            ) k200_h010
     /*where nivel_estrutura  = '1'
       and grupo_estrutura  = '34261'
       and subgru_estrutura = 'P'
       and item_estrutura   = '011015'
       and estagio_agrupador   = 63*/
     group by k200_h010.nivel_estrutura,
              k200_h010.grupo_estrutura,
              k200_h010.subgru_estrutura,
              k200_h010.item_estrutura,
              k200_h010.estagio_agrupador,
              k200_h010.estagio_agrupador_simultaneo,
              k200_h010.seq_agrupador;

BEGIN
  begin
      select codigo_matriz
      into v_cod_empresa
      from fatu_500
      where fatu_500.codigo_empresa = p_cod_empresa
      and  (select empr_008.val_int
            from empr_008
            where empr_008.codigo_empresa = p_cod_empresa
            and empr_008.param = 'estq.formaCalcCardex') = 3;
  exception when no_data_found then
      v_cod_empresa := p_cod_empresa;
  end;
  
  --
  --
  -- CARREGA VARIAVEIS PARA EM ELEABORACAO
  w_mes := to_char(p_dat_final, 'MM');
  w_ano := to_char(p_dat_final, 'YYYY');
  v_conta_reg := 0;

  FOR fatu_500 in u_fatu_500(p_cod_empresa) LOOP

    FOR k200_h010 IN u_k200_h010(p_cod_empresa, p_dat_final) LOOP

      begin
        select basi_010.codigo_contabil
          into w_codigo_contabil
          from basi_010
         where basi_010.nivel_estrutura = k200_h010.nivel_estrutura
           and basi_010.grupo_estrutura = k200_h010.grupo_estrutura
           and basi_010.subgru_estrutura = k200_h010.subgru_estrutura
           and basi_010.item_estrutura = k200_h010.item_estrutura;

      exception
        when no_data_found then
          w_codigo_contabil := 0;
      end;

      begin
        select basi_030.unidade_medida, basi_030.conta_estoque
          into w_uni_med, w_conta_estoque
          from basi_030
         where basi_030.nivel_estrutura = k200_h010.nivel_estrutura
           and basi_030.referencia = k200_h010.grupo_estrutura;
      exception
        when no_data_found then
          w_uni_med := ' ';
          w_conta_estoque := 0;
      end;

      w_valor_unitario := 0;
      
      --1o) prod + est + est_sim + seq => no mês/ano valor <> 0
      begin
          select nvl(rcnb_859.custo_acumulado,0)
          into w_valor_unitario
          from rcnb_859
          where rcnb_859.codigo_empresa                 = v_cod_empresa
          and   rcnb_859.mes                            = w_mes
          and   rcnb_859.ano                            = w_ano
          and   rcnb_859.nivel_estrutura                = k200_h010.nivel_estrutura
          and   rcnb_859.grupo_estrutura                = k200_h010.grupo_estrutura
          and   rcnb_859.subgru_estrutura               = k200_h010.subgru_estrutura
          and   rcnb_859.item_estrutura                 = k200_h010.item_estrutura
          and   nvl(rcnb_859.codigo_estagio,0)          = nvl(k200_h010.estagio_agrupador,0)
          and   nvl(rcnb_859.seq_operacao_agrupador,0)  = nvl(k200_h010.seq_agrupador,0)
          and   nvl(rcnb_859.estagio_agrupador_simul,0) = nvl(k200_h010.estagio_agrupador_simultaneo,0);
      exception when no_data_found then
          w_valor_unitario := 0;
      end;
      
      --2o prod + est + est_sim (ultimo seq. de operação do estágio da rcnb_859) => no mês/ano valor <> 0
      if w_valor_unitario = 0 then
          begin
              select nvl(rcnb_859.custo_acumulado,0)
              into w_valor_unitario
              from rcnb_859
              where rcnb_859.codigo_empresa                 = v_cod_empresa
              and   rcnb_859.mes                            = w_mes
              and   rcnb_859.ano                            = w_ano
              and   rcnb_859.nivel_estrutura                = k200_h010.nivel_estrutura
              and   rcnb_859.grupo_estrutura                = k200_h010.grupo_estrutura
              and   rcnb_859.subgru_estrutura               = k200_h010.subgru_estrutura
              and   rcnb_859.item_estrutura                 = k200_h010.item_estrutura
              and   nvl(rcnb_859.codigo_estagio,0)          = nvl(k200_h010.estagio_agrupador,0)
              and   nvl(rcnb_859.seq_operacao_agrupador,0)  = (
                                                              select nvl(max(rcnb_859.seq_operacao_agrupador),0)
                                                              from rcnb_859
                                                              where rcnb_859.codigo_empresa                 = v_cod_empresa
                                                              and   rcnb_859.mes                            = w_mes
                                                              and   rcnb_859.ano                            = w_ano
                                                              and   rcnb_859.nivel_estrutura                = k200_h010.nivel_estrutura
                                                              and   rcnb_859.grupo_estrutura                = k200_h010.grupo_estrutura
                                                              and   rcnb_859.subgru_estrutura               = k200_h010.subgru_estrutura
                                                              and   rcnb_859.item_estrutura                 = k200_h010.item_estrutura
                                                              and   nvl(rcnb_859.codigo_estagio,0)          = nvl(k200_h010.estagio_agrupador,0)
                                                              )
              and   nvl(rcnb_859.estagio_agrupador_simul,0) = nvl(k200_h010.estagio_agrupador_simultaneo,0);
          exception when no_data_found then
              w_valor_unitario := 0;
          end;
          
          --3o prod + est + est_sim + seq => do mês/ano anterior valor <> 0
          if w_valor_unitario = 0 then
              begin
                  select nvl(rcnb_859.custo_acumulado,0)
                  into w_valor_unitario
                  from rcnb_859
                  where rcnb_859.codigo_empresa                 = v_cod_empresa
                  and   rcnb_859.mes                            = extract(month from(trunc(add_months(p_dat_final, -1), 'MM')))
                  and   rcnb_859.ano                            = extract(year from(trunc(add_months(p_dat_final, -1), 'YYYY')))
                  and   rcnb_859.nivel_estrutura                = k200_h010.nivel_estrutura
                  and   rcnb_859.grupo_estrutura                = k200_h010.grupo_estrutura
                  and   rcnb_859.subgru_estrutura               = k200_h010.subgru_estrutura
                  and   rcnb_859.item_estrutura                 = k200_h010.item_estrutura
                  and   nvl(rcnb_859.codigo_estagio,0)          = nvl(k200_h010.estagio_agrupador,0)
                  and   nvl(rcnb_859.seq_operacao_agrupador,0)  = nvl(k200_h010.seq_agrupador,0)
                  and   nvl(rcnb_859.estagio_agrupador_simul,0) = nvl(k200_h010.estagio_agrupador_simultaneo,0);
              exception when no_data_found then
                w_valor_unitario := 0;
              end;
              
              if w_valor_unitario = 0 then
                  -- 4o prod + est + est_sim (ultimo seq. de operação do estágio da rcnb_859) => do mês/ano anterior valor <> 0
                  begin
                      select nvl(rcnb_859.custo_acumulado,0)
                      into w_valor_unitario
                      from rcnb_859
                      where rcnb_859.codigo_empresa                 = v_cod_empresa
                      and   rcnb_859.mes                            = extract(month from(trunc(add_months(p_dat_final, -1), 'MM')))
                      and   rcnb_859.ano                            = extract(year from(trunc(add_months(p_dat_final, -1), 'YYYY')))
                      and   rcnb_859.nivel_estrutura                = k200_h010.nivel_estrutura
                      and   rcnb_859.grupo_estrutura                = k200_h010.grupo_estrutura
                      and   rcnb_859.subgru_estrutura               = k200_h010.subgru_estrutura
                      and   rcnb_859.item_estrutura                 = k200_h010.item_estrutura
                      and   nvl(rcnb_859.codigo_estagio,0)          = nvl(k200_h010.estagio_agrupador,0)
                      and   nvl(rcnb_859.seq_operacao_agrupador,0)  = (
                                                                      select nvl(max(rcnb_859.seq_operacao_agrupador),0)
                                                                      from rcnb_859
                                                                      where rcnb_859.codigo_empresa                 = v_cod_empresa
                                                                      and   rcnb_859.mes                            = w_mes
                                                                      and   rcnb_859.ano                            = w_ano
                                                                      and   rcnb_859.nivel_estrutura                = k200_h010.nivel_estrutura
                                                                      and   rcnb_859.grupo_estrutura                = k200_h010.grupo_estrutura
                                                                      and   rcnb_859.subgru_estrutura               = k200_h010.subgru_estrutura
                                                                      and   rcnb_859.item_estrutura                 = k200_h010.item_estrutura
                                                                      and   nvl(rcnb_859.codigo_estagio,0)          = nvl(k200_h010.estagio_agrupador,0)
                                                                      )
                      and   nvl(rcnb_859.estagio_agrupador_simul,0) = nvl(k200_h010.estagio_agrupador_simultaneo,0);
                  exception when no_data_found then
                    w_valor_unitario := 0;
                  end;
                  
                  if w_valor_unitario = 0 then
                      --prod + seq anterior => no mês/ano valor <> 0
                      begin
                          select nvl(rcnb_859.custo_acumulado,0)
                          into w_valor_unitario
                          from rcnb_859
                          where rcnb_859.codigo_empresa                 = v_cod_empresa
                          and   rcnb_859.mes                            = w_mes
                          and   rcnb_859.ano                            = w_ano
                          and   rcnb_859.nivel_estrutura                = k200_h010.nivel_estrutura
                          and   rcnb_859.grupo_estrutura                = k200_h010.grupo_estrutura
                          and   rcnb_859.subgru_estrutura               = k200_h010.subgru_estrutura
                          and   rcnb_859.item_estrutura                 = k200_h010.item_estrutura
                          and   nvl(rcnb_859.seq_operacao_agrupador,0)  = (
                                                                          select nvl(max(rcnb_859.seq_operacao_agrupador),0)
                                                                          from rcnb_859
                                                                          where rcnb_859.codigo_empresa                 = v_cod_empresa
                                                                          and   rcnb_859.mes                            = w_mes
                                                                          and   rcnb_859.ano                            = w_ano
                                                                          and   rcnb_859.nivel_estrutura                = k200_h010.nivel_estrutura
                                                                          and   rcnb_859.grupo_estrutura                = k200_h010.grupo_estrutura
                                                                          and   rcnb_859.subgru_estrutura               = k200_h010.subgru_estrutura
                                                                          and   rcnb_859.item_estrutura                 = k200_h010.item_estrutura
                                                                          and   rcnb_859.seq_operacao_agrupador         <= nvl(k200_h010.seq_agrupador,0)
                                                                          );
                      exception when no_data_found then
                        w_valor_unitario := 0;
                      end;
                      
                      if k200_h010.quantidade > 0
                      then
                         w_valor_unitario := k200_h010.valor_total / k200_h010.quantidade;
                      end if;
                  end if;
              end if;
          end if;
      end if;
 
      v_tipo_produto_sped := inter_fn_tp_prod_sped_recur(p_cod_empresa, w_conta_estoque, k200_h010.nivel_estrutura,
                          k200_h010.grupo_estrutura, k200_h010.subgru_estrutura, k200_h010.item_estrutura,
                          3, k200_h010.estagio_agrupador);

      v_conta_reg := v_conta_reg + 1;

      BEGIN

        INSERT INTO SPED_K200_H010
          (EMPRESA,
           MES,
           ANO,
           CNPJ9_PARTICIPANTE,
           CNPJ4_PARTICIPANTE,
           CNPJ2_PARTICIPANTE,
           NIVEL,
           GRUPO,
           SUBGRUPO,
           ITEM,
           ESTAGIO_AGRUPADOR,
           ESTAGIO_AGRUPADOR_SIMULTANEO,
           QUANTIDADE,
           VALOR_UNITARIO,
           VALOR_TOTAL,
           TIPO_PROPRIEDADE,
           CODIGO_CONTABIL,
           UNIDADE_MEDIDA,
           VALOR_IR,
           CODIGO_DEPOSITO,
           NCM,
           TIPO_PRODUTO_SPED,
           SEQUENCIA_OPERACAO_AGRUPADOR)
        values
          (p_cod_empresa,
           w_mes,
           w_ano,
           0,
           0,
           0,
           k200_h010.nivel_estrutura,
           k200_h010.grupo_estrutura,
           k200_h010.subgru_estrutura,
           k200_h010.item_estrutura,
           k200_h010.estagio_agrupador,
           k200_h010.estagio_agrupador_simultaneo,
           k200_h010.quantidade,
           w_valor_unitario,
           (k200_h010.quantidade * w_valor_unitario),
           0,
           w_codigo_contabil,
           w_uni_med,
           (k200_h010.quantidade * w_valor_unitario),
           0,
           0,
           v_tipo_produto_sped,
           k200_h010.seq_agrupador);

      EXCEPTION
           WHEN Dup_Val_On_Index THEN
              if k200_h010.quantidade <> 0
              then
                 update SPED_K200_H010 s
                 set s.QUANTIDADE        = s.QUANTIDADE + k200_h010.quantidade,
                   s.VALOR_UNITARIO    = w_valor_unitario,
                   s.VALOR_TOTAL       = (s.QUANTIDADE + k200_h010.quantidade) * w_valor_unitario

                 where s.EMPRESA                       = p_cod_empresa
                   and s.MES                           = w_mes
                   and s.ANO                           = w_ano
                   and s.CNPJ9_PARTICIPANTE            = 0
                   and s.CNPJ4_PARTICIPANTE            = 0
                   and s.CNPJ2_PARTICIPANTE            = 0
                   and s.NIVEL                         = k200_h010.nivel_estrutura
                   and s.GRUPO                         = k200_h010.grupo_estrutura
                   and s.SUBGRUPO                      = k200_h010.subgru_estrutura
                   and s.ITEM                          = k200_h010.item_estrutura
                   and s.ESTAGIO_AGRUPADOR             = k200_h010.estagio_agrupador
                   and s.ESTAGIO_AGRUPADOR_SIMULTANEO  = k200_h010.estagio_agrupador_simultaneo
                   and s.CODIGO_CONTABIL               = w_codigo_contabil
                   and s.CODIGO_DEPOSITO               = 0
                   and s.SEQUENCIA_OPERACAO_AGRUPADOR  = k200_h010.seq_agrupador
                   and s.TIPO_PROPRIEDADE              = 0
                   and s.TIPO_PRODUTO_SPED             = v_tipo_produto_sped;
              end if;

        WHEN OTHERS THEN
          p_des_erro := 'Erro na inclusao da tabela SPED_K200_H010' ||
                        Chr(10) || ' PRODUTO: ' ||
                        k200_h010.nivel_estrutura ||
                        k200_h010.grupo_estrutura ||
                        k200_h010.subgru_estrutura ||
                        k200_h010.item_estrutura ||
                        k200_h010.estagio_agrupador  ||
                        k200_h010.estagio_agrupador_simultaneo  ||
                        k200_h010.seq_agrupador  ||
                        ' ' || Chr(10) ||
                        SQLERRM;
          RAISE W_ERRO;
      END;

      if v_conta_reg = 10000
      then
         v_conta_reg := 0;
         commit;
      end if;

    END LOOP;


  END LOOP; -- LOOP DO FATU_500
  commit;

EXCEPTION
  WHEN W_ERRO then
    p_des_erro := 'Erro na procedure pr_gera_sped_atualiza_estoque_proprio ' || Chr(10) ||
                  p_des_erro;
  WHEN OTHERS THEN
    p_des_erro := 'Outros erros na procedure pr_gera_sped_atualiza_estoque_proprio ' || Chr(10) ||
                  SQLERRM;
END inter_pr_gera_sped_hkestqProp;
/
