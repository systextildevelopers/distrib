alter table pcpb_160 add opcao number(1) default 1;
comment on column pcpb_160.opcao is 'Opcao de utilização deste perfil';

exec inter_pr_recompile;
