alter table imp_ordens_marcacoes modify (
      eficiencia number(11,2),
      area       number(11,1),
      perimetro  number(11,2)
);

alter table pcpc_030 modify (
      eficiencia_opt number(11,2),
      area_opt       number(11,1),
      perimetro_opt  number(11,2)
);
