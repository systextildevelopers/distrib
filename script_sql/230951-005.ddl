CREATE table "MANU_012" (
    "ID"           NUMBER,
    "SOLICITACAO"  NUMBER(6,0),
    "COD_EMPRESA"  NUMBER(3,0),
    "TIPO_SERVICO" NUMBER(5,0),
    "ARQUIVO"      BLOB,
    "DATA_INSERT"  DATE,
    "MIME_TYPE"    VARCHAR2(255),
    "FILE_NAME"    VARCHAR2(255),
    "USUARIO_OPERACAO" VARCHAR2(255),
    constraint  "MANU_012_PK" primary key ("ID")
);

CREATE sequence "MANU_012_SEQ";
