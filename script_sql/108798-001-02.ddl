insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('loja_f835', 'Lançamento de sangria', 0,0);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'loja_f835', 'Nenhum', 0, 1, 'S', 'N', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'loja_f835', 'Nenhum', 0, 1, 'S', 'N', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Lançamento de sangria'
 where hdoc_036.codigo_programa = 'loja_f835'
   and hdoc_036.locale          = 'es_ES';
commit;
/
