
  CREATE OR REPLACE PROCEDURE "INTER_PR_ARMAZENA_QTDE_ORIG_OP" (
   p_ordem_producao in number
) is

v_nro_reg    number;

begin
   begin
      select nvl(count(1),0)
      into   v_nro_reg
      from pcpc_049
      where pcpc_049.ordem_producao = p_ordem_producao;
   exception when others then
      v_nro_reg := 0;
   end;

   if v_nro_reg = 0
   then
      for reg_pcpc040 in (select pcpc_040.proconf_subgrupo,
                                 pcpc_040.proconf_item,
                                 nvl(sum(pcpc_040.qtde_pecas_prog),0) as qtde_original
                          from pcpc_040
                          where pcpc_040.ordem_producao   = p_ordem_producao
                            and pcpc_040.estagio_anterior = 0
                          group by pcpc_040.proconf_subgrupo,   pcpc_040.proconf_item)
      loop
         v_nro_reg := 0;

         begin
            select nvl(count(1),0)
            into   v_nro_reg
            from pcpc_049
            where pcpc_049.ordem_producao   = p_ordem_producao
              and pcpc_049.subgrupo_produto = reg_pcpc040.proconf_subgrupo
              and pcpc_049.item_produto     = reg_pcpc040.proconf_item;

         exception when others then
            v_nro_reg := 0;
         end;

         if v_nro_reg = 0
         then
            begin
               insert into pcpc_049 (
                  ordem_producao,        subgrupo_produto,
                  item_produto,          qtde_original,
                  dta_criacao
               ) values (
                  p_ordem_producao,           reg_pcpc040.proconf_subgrupo,
                  reg_pcpc040.proconf_item,   reg_pcpc040.qtde_original,
                  sysdate
               );
            end;
         end if;
      end loop;
   end if;
end inter_pr_armazena_qtde_orig_op;

 

/

exec inter_pr_recompile;

