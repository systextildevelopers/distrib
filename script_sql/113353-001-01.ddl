CREATE TABLE OBRF_990
(
  TIPO_NOTA VARCHAR2(1) NOT NULL,
  COD_EMPRESA NUMBER(3) NOT NULL,
  NUM_NOTA NUMBER(9) NOT NULL,
  SERIE_NOTA VARCHAR2(3) NOT NULL,
  CNPJ9 NUMBER(9) NOT NULL,
  CNPJ4 NUMBER(4) NOT NULL,
  CNPJ2 NUMBER(2) NOT NULL,
  CHAVE_NFE VARCHAR2(44) NOT NULL,
  ESPECIE_NFE VARCHAR2(5) DEFAULT ''
);
