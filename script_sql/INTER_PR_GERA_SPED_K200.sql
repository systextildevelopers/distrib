create or replace procedure inter_pr_gera_sped_k200(p_id_k100       IN NUMBER,
													                          p_cod_empresa   IN NUMBER,
													                          p_dat_final     IN DATE,
													                          p_cnpj9         IN NUMBER,
                                                    p_cnpj4         IN NUMBER,
                                                    p_cnpj2         IN NUMBER,
                                                    p_junta_empresa IN NUMBER,
													                          p_des_erro    out varchar2) is



--
-- FAZ LEITURA DE PRODUTO PARA DEPOSITOS PRÓPRIOS
--


            
w_ind_achou     varchar2(1);
w_erro          EXCEPTION;


BEGIN

   select '1' into w_ind_achou from dual;
    
  EXCEPTION
    WHEN W_ERRO then
      p_des_erro := 'Erro na procedure inter_pr_gera_sped_k200' || Chr(10) || p_des_erro;
    WHEN OTHERS THEN
      p_des_erro := 'Outros erros na procedure inter_pr_gera_sped_k200' || Chr(10) || SQLERRM;
  END inter_pr_gera_sped_k200;


/
exec inter_pr_recompile;
