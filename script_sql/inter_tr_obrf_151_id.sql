
  CREATE OR REPLACE TRIGGER "INTER_TR_OBRF_151_ID" 
before insert on OBRF_151
  for each row

declare
   v_nr_registro number;

begin
   select seq_OBRF_151.nextval into v_nr_registro from dual;

   :new.id := v_nr_registro;

end inter_tr_OBRF_151_id;
-- ALTER TRIGGER "INTER_TR_OBRF_151_ID" ENABLE
 

/

exec inter_pr_recompile;

