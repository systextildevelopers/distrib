create or replace procedure inter_pr_pedi_340
is

   -- Criando de seleção de pedidos
   cursor pedi341 is

      select pedi_341.cnpj9,            pedi_341.cnpj4,               pedi_341.cnpj2,
             pedi_341.pedido_venda,     pedi_341.valor_pedido,        pedi_341.cod_emp_serasa,
             pedi_341.senha_emp_serasa ,pedi_341.cod_usuario_serasa,  pedi_341.senha_usuario_serasa,
             rowid indice
        from pedi_341
       where pedi_341.flag_geracao  = 0
       order by pedi_341.cnpj9, pedi_341.cnpj4, pedi_341.cnpj2;

  v_existe number(09);
  v_altera number(01);
  v_repr number(9);
  v_empresa number(9);

begin
   -- le pedidos
   for reg341 in pedi341
   loop

       v_altera := 0 ;

       --verificando se já existe lançamento
       select COUNT(pedi_340.pedido_venda) into v_existe
         from pedi_340
        where pedi_340.cnpj9 = reg341.cnpj9
          and pedi_340.cnpj4 = reg341.cnpj4
          and pedi_340.cnpj2 = reg341.cnpj2
          and pedi_340.flag_retorno = 0;
          
        /*seleciona empresa e rpresentante*/
        
       select pedi_100.codigo_empresa, pedi_100.cod_rep_cliente
       into v_empresa, v_repr
       from pedi_100
       where pedi_100.pedido_venda = reg341.pedido_venda;

       if v_existe = 0 then -- se não existir dados então insere
         begin
           INSERT INTO pedi_340(
                  cnpj9,
                  cnpj4,
                  cnpj2,
                  numeros_pedidos,
                  valor_pedido,
                  data_consulta,
                  cod_emp_serasa,
                  senha_emp_serasa,
                  cod_usuario_serasa,
                  senha_usuario_serasa,
                  cod_empresa,
                  cod_repr)
               VALUES(
                  reg341.cnpj9,
                  reg341.cnpj4,
                  reg341.cnpj2,
                  reg341.pedido_venda,
                  reg341.valor_pedido,
                  trunc(sysdate,'DD'),
                  reg341.cod_emp_serasa,
                  reg341.senha_emp_serasa,
                  reg341.cod_usuario_serasa,
                  reg341.senha_usuario_serasa,
                  v_empresa,
                  v_repr);
         exception
           when OTHERS then
               raise_application_error (-20000, 'Nao inseriu na tabela pedi_340 de relacionamento com Serasa. '|| Chr(10) || SQLERRM);
               v_altera := 1 ;
         end;
       else     -- se já houver dados então acrescenta o nr do pedido e o seu valor
         begin
            update pedi_340 set
                   pedi_340.valor_pedido    = pedi_340.valor_pedido + reg341.valor_pedido,
                   pedi_340.numeros_pedidos = pedi_340.numeros_pedidos || ',' || reg341.pedido_venda
             where pedi_340.cnpj9 = reg341.cnpj9
               and pedi_340.cnpj4 = reg341.cnpj4
               and pedi_340.cnpj2 = reg341.cnpj2
               and pedi_340.flag_retorno = 0;
         exception
           when OTHERS then
             raise_application_error (-20000, 'Nao atualizou a tabela pedi_340 de relacionamento com Serasa. ' || Chr(10) || SQLERRM );
             v_altera := 1 ;
         end;
       end if;

       if v_altera = 0 then
         begin
           update pedi_341 set
                  flag_geracao    = 1
            where pedi_341.rowid = reg341.indice;
         exception
           when OTHERS then
              raise_application_error (-20000, 'Nao atualizou a tabela pedi_341 de relacionamento temporario com Serasa. '|| Chr(10) || SQLERRM);
              v_altera := 1 ;
         end;

         commit;
       end if;

   end loop; -- pedi_341

end inter_pr_pedi_340;

/

exec inter_pr_recompile;

