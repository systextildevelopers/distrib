create or replace trigger inter_tr_ESTQ_043_log 
after insert or delete or update 
on ESTQ_043 
for each row 
declare 
   ws_usuario_rede           varchar2(20) ; 
   ws_maquina_rede           varchar2(40) ; 
   ws_aplicativo             varchar2(20) ; 
   ws_sid                    number(9) ; 
   ws_empresa                number(3) ; 
   ws_usuario_systextil      varchar2(250) ; 
   ws_locale_usuario         varchar2(5) ; 
   v_nome_programa           varchar2(20) ; 


begin
-- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   v_nome_programa := inter_fn_nome_programa(ws_sid);  
 
 
 if inserting 
 then 
    begin 
 
        insert into ESTQ_043_log (
           TIPO_OCORR,   /*0*/ 
           DATA_OCORR,   /*1*/ 
           HORA_OCORR,   /*2*/ 
           USUARIO_REDE,   /*3*/ 
           MAQUINA_REDE,   /*4*/ 
           APLICACAO,   /*5*/ 
           USUARIO_SISTEMA,   /*6*/ 
           NOME_PROGRAMA,   /*7*/ 
           nivel_estrutura_OLD,   /*8*/ 
           nivel_estrutura_NEW,   /*9*/ 
           grupo_estrutura_OLD,   /*10*/ 
           grupo_estrutura_NEW,   /*11*/ 
           subgru_estrutura_OLD,   /*12*/ 
           subgru_estrutura_NEW,   /*13*/ 
           item_estrutura_OLD,   /*14*/ 
           item_estrutura_NEW,   /*15*/ 
           codigo_deposito_OLD,   /*16*/ 
           codigo_deposito_NEW,   /*17*/ 
           numero_lote_OLD,   /*18*/ 
           numero_lote_NEW,   /*19*/ 
           qtde_estoque_atu_OLD,   /*20*/ 
           qtde_estoque_atu_NEW,   /*21*/ 
           qtde_estoque_bal_OLD,   /*22*/ 
           qtde_estoque_bal_NEW,   /*23*/ 
           data_imagem_OLD,   /*24*/ 
           data_imagem_NEW,   /*25*/ 
           seq_balanco_OLD,   /*26*/ 
           seq_balanco_NEW,   /*27*/ 
           nr_contagem_OLD,   /*28*/ 
           nr_contagem_NEW,   /*29*/ 
           qtde_estoque_bal2_OLD,   /*30*/ 
           qtde_estoque_bal2_NEW,   /*31*/ 
           qtde_estoque_bal3_OLD,   /*32*/ 
           qtde_estoque_bal3_NEW,   /*33*/ 
           flag_balanco1_OLD,   /*34*/ 
           flag_balanco1_NEW,   /*35*/ 
           flag_balanco2_OLD,   /*36*/ 
           flag_balanco2_NEW,   /*37*/ 
           flag_balanco3_OLD,   /*38*/ 
           flag_balanco3_NEW,   /*39*/ 
           usuario_systextil_OLD,   /*40*/ 
           usuario_systextil_NEW,   /*41*/ 
           usuario_rede_OLD,   /*42*/ 
           usuario_rede_NEW,   /*43*/ 
           maquina_rede_OLD,   /*44*/ 
           maquina_rede_NEW,   /*45*/ 
           data_operacao_OLD,   /*46*/ 
           data_operacao_NEW,   /*47*/ 
           programa_OLD,   /*48*/ 
           programa_NEW,   /*49*/ 
           qtde_coletada_OLD,   /*50*/ 
           qtde_coletada_NEW    /*51*/
        ) values (    
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/ 
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           '',/*8*/
           :new.nivel_estrutura, /*9*/   
           '',/*10*/
           :new.grupo_estrutura, /*11*/   
           '',/*12*/
           :new.subgru_estrutura, /*13*/   
           '',/*14*/
           :new.item_estrutura, /*15*/   
           0,/*16*/
           :new.codigo_deposito, /*17*/   
           0,/*18*/
           :new.numero_lote, /*19*/   
           0,/*20*/
           :new.qtde_estoque_atu, /*21*/   
           0,/*22*/
           :new.qtde_estoque_bal, /*23*/   
           null,/*24*/
           :new.data_imagem, /*25*/   
           0,/*26*/
           :new.seq_balanco, /*27*/   
           0,/*28*/
           :new.nr_contagem, /*29*/   
           0,/*30*/
           :new.qtde_estoque_bal2, /*31*/   
           0,/*32*/
           :new.qtde_estoque_bal3, /*33*/   
           0,/*34*/
           :new.flag_balanco1, /*35*/   
           0,/*36*/
           :new.flag_balanco2, /*37*/   
           0,/*38*/
           :new.flag_balanco3, /*39*/   
           '',/*40*/
           :new.usuario_systextil, /*41*/   
           '',/*42*/
           :new.usuario_rede, /*43*/   
           '',/*44*/
           :new.maquina_rede, /*45*/   
           null,/*46*/
           :new.data_operacao, /*47*/   
           '',/*48*/
           :new.programa, /*49*/   
           0,/*50*/
           :new.qtde_coletada /*51*/   
         );    
    end;    
 end if;    
  
  
 if updating 
 then 
    begin 
        insert into ESTQ_043_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           nivel_estrutura_OLD, /*8*/   
           nivel_estrutura_NEW, /*9*/   
           grupo_estrutura_OLD, /*10*/   
           grupo_estrutura_NEW, /*11*/   
           subgru_estrutura_OLD, /*12*/   
           subgru_estrutura_NEW, /*13*/   
           item_estrutura_OLD, /*14*/   
           item_estrutura_NEW, /*15*/   
           codigo_deposito_OLD, /*16*/   
           codigo_deposito_NEW, /*17*/   
           numero_lote_OLD, /*18*/   
           numero_lote_NEW, /*19*/   
           qtde_estoque_atu_OLD, /*20*/   
           qtde_estoque_atu_NEW, /*21*/   
           qtde_estoque_bal_OLD, /*22*/   
           qtde_estoque_bal_NEW, /*23*/   
           data_imagem_OLD, /*24*/   
           data_imagem_NEW, /*25*/   
           seq_balanco_OLD, /*26*/   
           seq_balanco_NEW, /*27*/   
           nr_contagem_OLD, /*28*/   
           nr_contagem_NEW, /*29*/   
           qtde_estoque_bal2_OLD, /*30*/   
           qtde_estoque_bal2_NEW, /*31*/   
           qtde_estoque_bal3_OLD, /*32*/   
           qtde_estoque_bal3_NEW, /*33*/   
           flag_balanco1_OLD, /*34*/   
           flag_balanco1_NEW, /*35*/   
           flag_balanco2_OLD, /*36*/   
           flag_balanco2_NEW, /*37*/   
           flag_balanco3_OLD, /*38*/   
           flag_balanco3_NEW, /*39*/   
           usuario_systextil_OLD, /*40*/   
           usuario_systextil_NEW, /*41*/   
           usuario_rede_OLD, /*42*/   
           usuario_rede_NEW, /*43*/   
           maquina_rede_OLD, /*44*/   
           maquina_rede_NEW, /*45*/   
           data_operacao_OLD, /*46*/   
           data_operacao_NEW, /*47*/   
           programa_OLD, /*48*/   
           programa_NEW, /*49*/   
           qtde_coletada_OLD, /*50*/   
           qtde_coletada_NEW  /*51*/  
        ) values (    
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/ 
           :old.nivel_estrutura,  /*8*/  
           :new.nivel_estrutura, /*9*/   
           :old.grupo_estrutura,  /*10*/  
           :new.grupo_estrutura, /*11*/   
           :old.subgru_estrutura,  /*12*/  
           :new.subgru_estrutura, /*13*/   
           :old.item_estrutura,  /*14*/  
           :new.item_estrutura, /*15*/   
           :old.codigo_deposito,  /*16*/  
           :new.codigo_deposito, /*17*/   
           :old.numero_lote,  /*18*/  
           :new.numero_lote, /*19*/   
           :old.qtde_estoque_atu,  /*20*/  
           :new.qtde_estoque_atu, /*21*/   
           :old.qtde_estoque_bal,  /*22*/  
           :new.qtde_estoque_bal, /*23*/   
           :old.data_imagem,  /*24*/  
           :new.data_imagem, /*25*/   
           :old.seq_balanco,  /*26*/  
           :new.seq_balanco, /*27*/   
           :old.nr_contagem,  /*28*/  
           :new.nr_contagem, /*29*/   
           :old.qtde_estoque_bal2,  /*30*/  
           :new.qtde_estoque_bal2, /*31*/   
           :old.qtde_estoque_bal3,  /*32*/  
           :new.qtde_estoque_bal3, /*33*/   
           :old.flag_balanco1,  /*34*/  
           :new.flag_balanco1, /*35*/   
           :old.flag_balanco2,  /*36*/  
           :new.flag_balanco2, /*37*/   
           :old.flag_balanco3,  /*38*/  
           :new.flag_balanco3, /*39*/   
           :old.usuario_systextil,  /*40*/  
           :new.usuario_systextil, /*41*/   
           :old.usuario_rede,  /*42*/  
           :new.usuario_rede, /*43*/   
           :old.maquina_rede,  /*44*/  
           :new.maquina_rede, /*45*/   
           :old.data_operacao,  /*46*/  
           :new.data_operacao, /*47*/   
           :old.programa,  /*48*/  
           :new.programa, /*49*/   
           :old.qtde_coletada,  /*50*/  
           :new.qtde_coletada  /*51*/  
         );    
    end;    
 end if;    
  
  
 if deleting 
 then 
    begin 
        insert into ESTQ_043_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           nivel_estrutura_OLD, /*8*/   
           nivel_estrutura_NEW, /*9*/   
           grupo_estrutura_OLD, /*10*/   
           grupo_estrutura_NEW, /*11*/   
           subgru_estrutura_OLD, /*12*/   
           subgru_estrutura_NEW, /*13*/   
           item_estrutura_OLD, /*14*/   
           item_estrutura_NEW, /*15*/   
           codigo_deposito_OLD, /*16*/   
           codigo_deposito_NEW, /*17*/   
           numero_lote_OLD, /*18*/   
           numero_lote_NEW, /*19*/   
           qtde_estoque_atu_OLD, /*20*/   
           qtde_estoque_atu_NEW, /*21*/   
           qtde_estoque_bal_OLD, /*22*/   
           qtde_estoque_bal_NEW, /*23*/   
           data_imagem_OLD, /*24*/   
           data_imagem_NEW, /*25*/   
           seq_balanco_OLD, /*26*/   
           seq_balanco_NEW, /*27*/   
           nr_contagem_OLD, /*28*/   
           nr_contagem_NEW, /*29*/   
           qtde_estoque_bal2_OLD, /*30*/   
           qtde_estoque_bal2_NEW, /*31*/   
           qtde_estoque_bal3_OLD, /*32*/   
           qtde_estoque_bal3_NEW, /*33*/   
           flag_balanco1_OLD, /*34*/   
           flag_balanco1_NEW, /*35*/   
           flag_balanco2_OLD, /*36*/   
           flag_balanco2_NEW, /*37*/   
           flag_balanco3_OLD, /*38*/   
           flag_balanco3_NEW, /*39*/   
           usuario_systextil_OLD, /*40*/   
           usuario_systextil_NEW, /*41*/   
           usuario_rede_OLD, /*42*/   
           usuario_rede_NEW, /*43*/   
           maquina_rede_OLD, /*44*/   
           maquina_rede_NEW, /*45*/   
           data_operacao_OLD, /*46*/   
           data_operacao_NEW, /*47*/   
           programa_OLD, /*48*/   
           programa_NEW, /*49*/   
           qtde_coletada_OLD, /*50*/   
           qtde_coletada_NEW /*51*/   
        ) values (    
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede,/*4*/ 
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           :old.nivel_estrutura, /*8*/   
           '', /*9*/
           :old.grupo_estrutura, /*10*/   
           '', /*11*/
           :old.subgru_estrutura, /*12*/   
           '', /*13*/
           :old.item_estrutura, /*14*/   
           '', /*15*/
           :old.codigo_deposito, /*16*/   
           0, /*17*/
           :old.numero_lote, /*18*/   
           0, /*19*/
           :old.qtde_estoque_atu, /*20*/   
           0, /*21*/
           :old.qtde_estoque_bal, /*22*/   
           0, /*23*/
           :old.data_imagem, /*24*/   
           null, /*25*/
           :old.seq_balanco, /*26*/   
           0, /*27*/
           :old.nr_contagem, /*28*/   
           0, /*29*/
           :old.qtde_estoque_bal2, /*30*/   
           0, /*31*/
           :old.qtde_estoque_bal3, /*32*/   
           0, /*33*/
           :old.flag_balanco1, /*34*/   
           0, /*35*/
           :old.flag_balanco2, /*36*/   
           0, /*37*/
           :old.flag_balanco3, /*38*/   
           0, /*39*/
           :old.usuario_systextil, /*40*/   
           '', /*41*/
           :old.usuario_rede, /*42*/   
           '', /*43*/
           :old.maquina_rede, /*44*/   
           '', /*45*/
           :old.data_operacao, /*46*/   
           null, /*47*/
           :old.programa, /*48*/   
           '', /*49*/
           :old.qtde_coletada, /*50*/   
           0 /*51*/
         );    
    end;    
 end if;    
end inter_tr_ESTQ_043_log;

/

exec inter_pr_recompile;
