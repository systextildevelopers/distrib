create or replace procedure inter_pr_bloq_cif(p_cod_empresa       in  number,
                                                    p_cnpj9_cliente     in  number,
                                                    p_cnpj4_cliente     in  number,
                                                    p_cnpj2_cliente     in  number,
                                                    p_cod_representante in  number,
                                                    p_estado_atuacao    in  varchar2,
                                                    p_cod_cidade        in  number,
                                                    p_marca             in  number,
                                                    p_tipoFrete            in  number,
                                                    p_bloqueiaAlteracao       out  number) is


  TYPE cnpjTransp9 IS TABLE OF supr_010.fornecedor9%TYPE;
  cnpj_transp9 cnpjTransp9;

  TYPE contratoCif IS TABLE OF supr_132.contrato_cif %TYPE;
  contrato_cif contratoCif;
   
begin
  
   begin
      select supr_132.cnpj_transp9,
             supr_132.contrato_cif
      BULK COLLECT INTO
             cnpj_transp9,
             contrato_cif
      from supr_132
      where (supr_132.empresa           = p_cod_empresa       or supr_132.empresa           = 0)
        and (supr_132.cnpj9_cliente     = p_cnpj9_cliente     or supr_132.cnpj9_cliente     = 0)
        and (supr_132.cnpj4_cliente     = p_cnpj4_cliente     or supr_132.cnpj4_cliente     = 0)
        and (supr_132.cnpj2_cliente     = p_cnpj2_cliente     or supr_132.cnpj2_cliente     = 0)
        and (supr_132.cod_representante = p_cod_representante or supr_132.cod_representante = 0)
        and (supr_132.estado_atuacao    = p_estado_atuacao    or supr_132.estado_atuacao    = 'XX')
        and (supr_132.cod_cidade        = p_cod_cidade        or supr_132.cod_cidade        = 0)
        and (supr_132.marca             = p_marca             or supr_132.marca             = 0)
      order by supr_132.empresa desc,           supr_132.cnpj9_cliente desc,
               supr_132.cnpj4_cliente desc,     supr_132.cnpj4_cliente desc,
               supr_132.cod_representante desc, supr_132.cod_cidade desc,
               supr_132.estado_atuacao,         supr_132.marca desc;
      exception
         when others then
           p_bloqueiaAlteracao := 0; 
   end;

   if sql%found
   then
      FOR i IN cnpj_transp9.FIRST .. cnpj_transp9.LAST
      LOOP
         if contrato_cif(i) = 1 and p_tipoFrete = 1
         then  p_bloqueiaAlteracao := 1;
         else  p_bloqueiaAlteracao := 0;
         end if;

         exit;

      END LOOP;
   else
       p_bloqueiaAlteracao := 0;
   end if;
  
end inter_pr_bloq_cif;

/

exec inter_pr_recompile;
