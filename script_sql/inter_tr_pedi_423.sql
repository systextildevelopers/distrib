CREATE OR REPLACE TRIGGER "INTER_TR_PEDI_423"
BEFORE INSERT ON PEDI_423 FOR EACH ROW
DECLARE
    proximo_valor number;
BEGIN
    if :new.id is null then
        select ID_PEDI_423.nextval into proximo_valor from dual;
        :new.id := proximo_valor;
    end if;
END INTER_TR_PEDI_423;

/

exec inter_pr_recompile;
