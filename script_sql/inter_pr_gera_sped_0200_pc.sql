create or replace procedure inter_pr_gera_sped_0200_pc(p_cod_empresa     IN  NUMBER,
                                                       p_cod_matriz      IN  NUMBER,
                                                       p_cnpj9_empresa   IN  NUMBER,
                                                       p_cnpj4_empresa   IN  NUMBER,
                                                       p_cnpj2_empresa   IN  NUMBER,
                                                       p_dat_inicial     IN  DATE,
                                                       p_dat_final     IN  DATE,
                                                       p_des_erro     out varchar2) is
--
-- Finalidade: gerar a tabela sped_0200 - Produtos
--                            sped_0190 - Unidade de Medida
-- Autor.....: Edson Pio
-- Data......: 21/02/11
--
-- Hist?ricos
--
-- Data    Autor    Observa??es
--

w_erro                EXCEPTION;
w_ind_achou           VARCHAR2(1);


w_ex_ipi                 sped_0200.cod_export_ipi%type;
w_ex_ipi_aux             sped_0200.cod_export_ipi%type;
w_cod_ncm                basi_240.classific_fiscal%type;
ws_aliquota_icms_sped    basi_240.aliquota_icms_sped%type;
ws_icms_sped             basi_240.aliquota_icms_sped%type;
ws_cod_enquadramento_ipi basi_240.cod_enquadramento_ipi%type;
ws_aliquota_ipi          basi_240.aliquota_ipi%type;
ws_descr_class_fisc      basi_240.descr_class_fisc%type;
ws_cod_unid_medida       sped_c170.cod_unid_medida%type;
w_tem_ex_ipi             number(1);
w_tipo_produto_sped      number(2);
v_controleNarrativaBlocoK NUMBER;
v_controleDescSimulBlocoK varchar(2);
v_estagio_agrupador_simultaneo NUMBER;
v_narrativaSped          varchar(250);

w_data_ant_alteracao     sped_hist_prod.data_ocorrencia%type;
indConsolDetalhe      number;

CURSOR u_sped_pc_c170 (p_cod_empresa     NUMBER,
                       p_cod_matriz      NUMBER,
                       indConsolDetalhe NUMBER) IS

  SELECT DISTINCT  cod_nivel, cod_grupo, cod_subgru, cod_item, estagio_agrupador, estagio_agrupador_simultaneo, seq_operacao_agrupador_insu
  from (
     (SELECT DISTINCT  sped_pc_c170.cod_nivel, sped_pc_c170.cod_grupo, sped_pc_c170.cod_subgru,
      sped_pc_c170.cod_item, sped_pc_c170.estagio_agrupador, sped_pc_c170.estagio_agrupador_simultaneo, seq_operacao_agrupador_insu
      FROM   sped_pc_c170
      WHERE   sped_pc_c170.cod_empresa        = p_cod_empresa
      and     sped_pc_c170.cod_matriz         = p_cod_matriz
      and     sped_pc_c170.cod_modelo         in ('0A','01','1B','04','55','02','2D','59','65')
      and     sped_pc_c170.cod_situacao_nota  in (0,6)
      and     sped_pc_c170.ind_nf_consumo    = 'N'
      and ((sped_pc_c170.val_pis               > 0.00 or indConsolDetalhe = 2)
           or sped_pc_c170.nota_devolucao_compra = 'S'
           or sped_pc_c170.tip_entrada_saida     = 'S'
           or sped_pc_c170.cod_modelo           in ('0A','01')))
   union all -- F100
      (select DISTINCT obrf_700.cod_nivel, obrf_700.cod_grupo, obrf_700.cod_subgru, obrf_700.cod_item,
          0 estagio_agrupador, 0 estagio_agrupador_simultaneo , 0 seq_operacao_agrupador_insu
       from obrf_700
             where obrf_700.cod_empresa = p_cod_empresa
               and obrf_700.mes_apur    = to_char(p_dat_inicial,'MM')
               and obrf_700.ano_apur    = to_char(p_dat_inicial,'YYYY')
               and trim(obrf_700.cod_grupo) is not null)
   union all -- 1101
      (select DISTINCT obrf_711.nivel, obrf_711.grupo, obrf_711.subgrupo, obrf_711.item,
      0 estagio_agrupador, 0 estagio_agrupador_simultaneo, 0 seq_operacao_agrupador_insu
       from obrf_711
             where obrf_711.cod_empresa = p_cod_empresa
               and obrf_711.per_apu_cred_mes = to_char(p_dat_inicial,'MM')
               and obrf_711.per_apu_cred_ano = to_char(p_dat_inicial,'YYYY')
               and trim(obrf_711.grupo)      is not null)
   );


CURSOR u_basi_010 (p_cod_nivel          VARCHAR2,
                   p_cod_grupo          VARCHAR2,
                   p_cod_subgru         VARCHAR2,
                   p_cod_item           VARCHAR2) IS
   select basi_010.nivel_estrutura,     basi_010.grupo_estrutura,        decode(p_cod_subgru,null,' ',basi_010.subgru_estrutura) as subgru_estrutura,
         decode(p_cod_item,null,' ',basi_010.item_estrutura) as item_estrutura,      basi_010.narrativa,              basi_010.codigo_barras,
          decode(basi_240.exporta_classif_sped,'S',basi_010.classific_fiscal,null) classific_fiscal,    basi_030.unidade_medida,         basi_240.codigo_ex_ipi,
          basi_240.aliquota_icms_sped,  basi_240.cod_enquadramento_ipi,
          basi_150.tipo_produto_sped,   basi_240.aliquota_ipi,           basi_010.cod_servico_lst, basi_150.conta_estoque
   from   basi_010,
          basi_240,
          basi_150,
          basi_030
   where  basi_010.classific_fiscal   = basi_240.classific_fiscal
   and    basi_010.nivel_estrutura    = basi_030.nivel_estrutura
   and    basi_010.grupo_estrutura    = basi_030.referencia
   and    basi_030.conta_estoque      = basi_150.conta_estoque
   and    basi_010.nivel_estrutura    = p_cod_nivel
   and    basi_010.grupo_estrutura    = p_cod_grupo
   and   (basi_010.subgru_estrutura   = p_cod_subgru or p_cod_subgru is null)
   and   (basi_010.item_estrutura     = p_cod_item   or p_cod_item   is null)
   and    rownum                      = 1
   group by basi_010.nivel_estrutura,     basi_010.grupo_estrutura,
            decode(p_cod_subgru,null,' ',basi_010.subgru_estrutura),
            decode(p_cod_item,null,' ',basi_010.item_estrutura),
            basi_010.narrativa,           basi_010.codigo_barras,
            basi_010.classific_fiscal,    basi_030.unidade_medida,         basi_240.codigo_ex_ipi,
            basi_240.aliquota_icms_sped,  basi_240.cod_enquadramento_ipi,
            basi_150.tipo_produto_sped,   basi_240.aliquota_ipi,           basi_010.cod_servico_lst,
            basi_240.exporta_classif_sped, basi_150.conta_estoque;



CURSOR u_sped_0200 (p_cod_empresa     NUMBER) IS
   select distinct sped_pc_0200.cod_empresa, sped_pc_0200.cod_unid_medida
   from sped_pc_0200
   where sped_pc_0200.cod_empresa   = p_cod_empresa
     and sped_pc_0200.cod_matriz    = p_cod_matriz;

CURSOR u_basi_200 (p_cod_unid_medida      basi_200.unidade_medida%TYPE) IS
   select basi_200.descr_unidade,  basi_200.unidade_medida
   from   basi_200
   where  basi_200.unidade_medida = p_cod_unid_medida;



BEGIN

   indConsolDetalhe := inter_fn_get_param_int(p_cod_matriz, 'sped.indivConsol');
   begin
      SELECT val_int INTO v_controleNarrativaBlocoK from empr_008 where param = 'obrf.descricaoProdutoEmElaboracaoNF' and codigo_empresa = p_cod_empresa;
   exception
      when no_data_found then
      v_controleNarrativaBlocoK := 1;
   end;
   
   begin
      SELECT val_str INTO v_controleDescSimulBlocoK from empr_008 where param = 'obrf.descricaoSimultaneoEmElaboracaoNF' and codigo_empresa = p_cod_empresa;
   exception
      when no_data_found then
      v_controleDescSimulBlocoK := 'S';
   end;
   
   -- le a tabela de itens dos documentos para buscar os produtos
   FOR sped_pc_c170 IN u_sped_pc_c170 (p_cod_empresa, p_cod_matriz, indConsolDetalhe)
   LOOP

      w_ind_achou := 'N';

      if trim(sped_pc_c170.cod_nivel) is null and trim(sped_pc_c170.cod_item) is null
      then
         ws_cod_unid_medida := '';

         begin
            SELECT i.cod_unid_medida into ws_cod_unid_medida
            FROM   sped_pc_c170 i,
                   sped_pc_c100 c
            WHERE  i.cod_empresa     = c.cod_empresa
            and    i.num_nota_fiscal = c.num_nota_fiscal
            and    i.cod_serie_nota  = c.cod_serie_nota
            and    i.num_cnpj_9      = c.num_cnpj_9
            and    i.num_cnpj_4      = c.num_cnpj_4
            and    i.num_cnpj_2      = c.num_cnpj_2
            and    i.cod_empresa     = p_cod_empresa
            and    i.cod_matriz      = p_cod_matriz
            and    c.cod_modelo not in ('06','07','08','09','10','11','22','27','28','57')
            and    c.cod_situacao_nota not in (2,3,5)
            and    i.cod_nivel              = sped_pc_c170.cod_nivel
            and    i.cod_grupo              = sped_pc_c170.cod_grupo
            and    i.cod_subgru             = sped_pc_c170.cod_subgru
            and    i.cod_item               = sped_pc_c170.cod_item
            and    rownum                   = 1;
         exception
             when no_data_found then
              ws_cod_unid_medida := '';
         end;

         begin
            select basi_240.codigo_ex_ipi,                       basi_240.aliquota_icms_sped,
                   basi_240.cod_enquadramento_ipi,               basi_240.aliquota_ipi,
                   decode(basi_240.exporta_classif_sped,'S',REPLACE(basi_240.classific_fiscal,'.'),null),       trim(basi_240.descr_class_fisc)
            into   w_ex_ipi,                                     ws_aliquota_icms_sped,
                   ws_cod_enquadramento_ipi,                     ws_aliquota_ipi,
                   w_cod_ncm,                                    ws_descr_class_fisc
            from basi_240
            where REPLACE(basi_240.classific_fiscal,'.') = trim(sped_pc_c170.cod_grupo) || trim(sped_pc_c170.cod_subgru)
            and    rownum                   = 1;
         exception
             when no_data_found then
                w_ex_ipi                 := 0;
                ws_aliquota_icms_sped    := 0.00;
                ws_cod_enquadramento_ipi := '';
                ws_aliquota_ipi          := 0.00;
                w_cod_ncm                := '';
                ws_descr_class_fisc      := '';
         end;

         begin
            select obrf_245.codigo_ex_ipi,   1
            into   w_ex_ipi_aux,             w_tem_ex_ipi
            from obrf_245
            where obrf_245.nivel    = sped_pc_c170.cod_nivel
              and obrf_245.grupo    = sped_pc_c170.cod_grupo
              and obrf_245.subgrupo = sped_pc_c170.cod_subgru
              and obrf_245.item     = sped_pc_c170.cod_item;
         exception
             when no_data_found then
                w_ex_ipi_aux := 0;
                w_tem_ex_ipi := 0;
         end;

         if w_tem_ex_ipi = 1
         then
            w_ex_ipi := w_ex_ipi_aux;
         end if;

         begin
            select basi_061.perc_icms
            into ws_icms_sped
            from basi_061
            where basi_061.codigo_empresa = p_cod_empresa
              and REPLACE(basi_061.classific_fiscal,'.') = w_cod_ncm;
         exception
            when no_data_found then
            ws_icms_sped := ws_aliquota_icms_sped;
         end;

         begin
            insert into sped_pc_0200
               (cod_nivel,                        cod_grupo,                       cod_subgrupo,
                cod_item,                         des_narrativa,                  cod_barra,

                cod_unid_medida,                  tip_destinacao_item,             num_classifi_fiscal,
                cod_export_ipi,                   cod_genero_item,
                per_icms,                         cod_empresa,
                cod_lst,                          cod_matriz,                      num_cnpj9_empr,
                num_cnpj4_empr,                   num_cnpj2_empr)
            values
               (sped_pc_c170.cod_nivel,           sped_pc_c170.cod_grupo,           sped_pc_c170.cod_subgru,
                sped_pc_c170.cod_item,            ws_descr_class_fisc,              null,

                ws_cod_unid_medida,               7,                               w_cod_ncm,
                w_ex_ipi,                         null,
                ws_icms_sped,                     p_cod_empresa,
                null,                             p_cod_matriz,                    p_cnpj9_empresa,
                p_cnpj4_empresa,                  p_cnpj2_empresa);

         EXCEPTION
            WHEN OTHERS THEN
               p_des_erro := 'Erro na inclus?o da tabela sped_pc_0200 (1)' || Chr(10) ||
                             'Produto: ' || sped_pc_c170.cod_nivel || '-' ||
                                            sped_pc_c170.cod_grupo  || '-' ||
                                            sped_pc_c170.cod_subgru || '-' ||
                                            sped_pc_c170.cod_item || Chr(10) || SQLERRM;
               RAISE W_ERRO;
         END;
      else
         -- le a tabela de itens do Systextil
         FOR basi_010 IN u_basi_010 (sped_pc_c170.cod_nivel, sped_pc_c170.cod_grupo, sped_pc_c170.cod_subgru, sped_pc_c170.cod_item)
         LOOP

            w_ind_achou := 'S';

            -- verifica se o tamanho da classificacao fiscal esta correta, caso nao esteja, gera mensagem de
            IF  Nvl(Length(REPLACE(basi_010.classific_fiscal,'.')),0) > 8
            THEN
                basi_010.classific_fiscal := substr(trim(REPLACE(basi_010.classific_fiscal,'.')),1,8);
            END IF;

            begin
               select obrf_245.codigo_ex_ipi,   1
               into   w_ex_ipi_aux,             w_tem_ex_ipi
               from obrf_245
               where obrf_245.nivel    = sped_pc_c170.cod_nivel
                 and obrf_245.grupo    = sped_pc_c170.cod_grupo
                 and obrf_245.subgrupo = sped_pc_c170.cod_subgru
                 and obrf_245.item     = sped_pc_c170.cod_item;
            exception
                when no_data_found then
                   w_ex_ipi_aux := 0;
                   w_tem_ex_ipi := 0;
            end;

            if w_tem_ex_ipi = 1
            then
               basi_010.codigo_ex_ipi := w_ex_ipi_aux;
            end if;

            ws_aliquota_icms_sped := basi_010.aliquota_icms_sped;
            
            w_tipo_produto_sped := basi_010.tipo_produto_sped;

           if(sped_pc_c170.estagio_agrupador + sped_pc_c170.estagio_agrupador_simultaneo) > 0
           then
              w_tipo_produto_sped := 3;
           end if;
           
           w_tipo_produto_sped := inter_fn_tp_prod_sped_recur(p_cod_empresa, basi_010.conta_estoque, sped_pc_c170.cod_nivel, sped_pc_c170.cod_grupo, sped_pc_c170.cod_subgru, sped_pc_c170.cod_item, 
                                   w_tipo_produto_sped, sped_pc_c170.estagio_agrupador);

           v_narrativaSped := basi_010.narrativa;
           
           if(sped_pc_c170.estagio_agrupador + sped_pc_c170.estagio_agrupador_simultaneo) > 0
           then
              basi_010.cod_servico_lst := '';
              basi_010.codigo_barras := '';
              v_narrativaSped := '';
              
              --Verifica se ira atribuir estagio agrupador a narrativa do produto.
              case v_controleDescSimulBlocoK
                when 'N' then
                v_estagio_agrupador_simultaneo := 0;
                ELSE
                v_estagio_agrupador_simultaneo := sped_pc_c170.estagio_agrupador_simultaneo;
              end case;
              --Cria narrativa conforme parametro por empresa, coloca estagios na frente ou atras.
              case v_controleNarrativaBlocoK
                when 2 then
                v_narrativaSped := inter_fn_descr_blk(sped_pc_c170.estagio_agrupador, v_estagio_agrupador_simultaneo) || ' ' || basi_010.narrativa ;

                ELSE
                v_narrativaSped := basi_010.narrativa || ' ' || inter_fn_descr_blk(sped_pc_c170.estagio_agrupador, v_estagio_agrupador_simultaneo);

              end case;
              basi_010.classific_fiscal := inter_fn_get_classific(sped_pc_c170.cod_nivel, sped_pc_c170.cod_grupo, sped_pc_c170.cod_subgru, sped_pc_c170.cod_item,
               sped_pc_c170.estagio_agrupador, sped_pc_c170.estagio_agrupador_simultaneo);
              basi_010.cod_enquadramento_ipi :=  999;
              basi_010.codigo_ex_ipi :=  0;
           end if;
           
            basi_010.classific_fiscal := substr(REPLACE(basi_010.classific_fiscal,'.'),1,8);

            if not trim(basi_010.cod_servico_lst) is null
            then
               basi_010.classific_fiscal := null;
            end if;
            
            begin
               select basi_061.perc_icms
               into ws_icms_sped
               from basi_061
               where basi_061.codigo_empresa = p_cod_empresa
                 and substr(REPLACE(basi_061.classific_fiscal,'.'),1,8) = basi_010.classific_fiscal;
            exception
               when no_data_found then
               ws_icms_sped := ws_aliquota_icms_sped;
            end;
            

            -- insere o registro do produto na base de dados
            begin
               insert into sped_pc_0200
                  (cod_nivel,                        cod_grupo,                       cod_subgrupo,
                   cod_item,                         des_narrativa,                   cod_barra,
                   cod_unid_medida,                  tip_destinacao_item,             num_classifi_fiscal,
                   cod_export_ipi,                   cod_genero_item,
                   per_icms,                         cod_empresa,
                   cod_lst,                          cod_matriz,                      num_cnpj9_empr,
                   num_cnpj4_empr,                   num_cnpj2_empr,
                   estagio_agrupador,                estagio_agrupador_simultaneo)
               values
                  (basi_010.nivel_estrutura,         basi_010.grupo_estrutura,              basi_010.subgru_estrutura,
                   basi_010.item_estrutura,          substr(trim(v_narrativaSped),1,250),   basi_010.codigo_barras,
                   basi_010.unidade_medida,          w_tipo_produto_sped,                   basi_010.classific_fiscal,
                   basi_010.codigo_ex_ipi,           null,
                   ws_icms_sped,                     p_cod_empresa,
                   REPLACE(basi_010.cod_servico_lst,'.'),p_cod_matriz,                p_cnpj9_empresa,
                   p_cnpj4_empresa,                  p_cnpj2_empresa,
                   sped_pc_c170.estagio_agrupador,   nvl(sped_pc_c170.estagio_agrupador_simultaneo,0));

            EXCEPTION
               WHEN OTHERS THEN
                  p_des_erro := 'Erro na inclus?o da tabela sped_pc_0200 (2)' || Chr(10) ||
                                'Produto: ' || sped_pc_c170.cod_nivel || '-' ||
                                               sped_pc_c170.cod_grupo  || '-' ||
                                               sped_pc_c170.cod_subgru || '-' ||
                                               sped_pc_c170.cod_item || Chr(10) || SQLERRM;
                  RAISE W_ERRO;
            END;
         END LOOP;
      end if;
   END LOOP;

   COMMIT;


   for f_hist_100 in (select trim(sped_hist_prod.narrativa) as narrativa,  to_date(sped_hist_prod.data_ocorrencia) - 1 as data_anter,
         to_date(sped_hist_prod.data_ocorrencia) as data_ocorr,
         sped_pc_0200.cod_nivel,      sped_pc_0200.cod_grupo,
         sped_pc_0200.cod_subgrupo,   sped_pc_0200.cod_item
         from sped_hist_prod, sped_pc_0200
         where sped_hist_prod.data_ocorrencia between p_dat_inicial and p_dat_final
           and sped_hist_prod.nivel      = sped_pc_0200.cod_nivel
           and sped_hist_prod.grupo      = sped_pc_0200.cod_grupo
           and (sped_hist_prod.subgrupo     = sped_pc_0200.cod_subgrupo or sped_pc_0200.cod_subgrupo is null)
           and (sped_hist_prod.item     = trim(sped_pc_0200.cod_item)   or trim(sped_pc_0200.cod_item)   is null)
           and sped_pc_0200.cod_matriz  = p_cod_matriz
           and sped_pc_0200.cod_empresa = p_cod_empresa
           and not exists (select 1 from sped_pc_hist
                           where sped_pc_hist.cod_matriz  = sped_pc_0200.cod_matriz
                             and sped_pc_hist.cod_empresa = sped_pc_0200.cod_empresa
                             and sped_pc_hist.nivel       = sped_pc_0200.cod_nivel
                             and sped_pc_hist.grupo       = sped_pc_0200.cod_grupo
                             and (sped_pc_hist.subgrupo    = trim(sped_pc_0200.cod_subgrupo) or trim(sped_pc_0200.cod_subgrupo) is null)
                             and (sped_pc_hist.item        = trim(sped_pc_0200.cod_item)   or trim(sped_pc_0200.cod_item)   is null)
                             and sped_pc_hist.data_alteracao = to_date(sped_hist_prod.data_ocorrencia))
         order by sped_hist_prod.data_ocorrencia)
   LOOP


      begin
         insert into sped_pc_hist
           (cod_matriz,
            cod_empresa,
            num_cnpj9_empr,
            num_cnpj4_empr,
            num_cnpj2_empr,
            nivel,
            grupo,
            subgrupo,
            item,
            narrativa_anterior,
            data_alteracao,
            data_alteracao_ant)
      values
            (p_cod_matriz,
             p_cod_empresa,
             p_cnpj9_empresa,
             p_cnpj4_empresa,
             p_cnpj2_empresa,
             f_hist_100.cod_nivel,
             f_hist_100.cod_grupo,
             f_hist_100.cod_subgrupo,
             f_hist_100.cod_item,
             f_hist_100.narrativa,
             f_hist_100.data_ocorr,
             f_hist_100.data_anter);
      exception
          when dup_val_on_index then
             p_des_erro := null;
          when others then
             p_des_erro := 'Erro na inclus?o da tabela sped_pc_hist (1)' || Chr(10) ||
                           'Produto: ' || f_hist_100.cod_nivel || '-' ||
                                          f_hist_100.cod_grupo  || '-' ||
                                          f_hist_100.cod_subgrupo || '-' ||
                                          f_hist_100.cod_item || Chr(10) || SQLERRM;
              RAISE W_ERRO;
      end;
   END LOOP;


   FOR sped_0200 IN u_sped_0200 (p_cod_empresa)
   LOOP
      w_ind_achou := 'N';

      -- le os dados dos cadastro de unidade de medida
      FOR basi_200 IN u_basi_200 (sped_0200.cod_unid_medida)
      LOOP

         w_ind_achou := 'S';

         begin
            insert into sped_pc_0190
               (cod_empresa
               ,cod_matriz
               ,num_cnpj9_empr
               ,num_cnpj4_empr
               ,num_cnpj2_empr
               ,cod_unid_medida
               ,des_unid_medida)
            VALUES
               (p_cod_empresa
               ,p_cod_matriz
               ,p_cnpj9_empresa
               ,p_cnpj4_empresa
               ,p_cnpj2_empresa
               ,basi_200.unidade_medida
               ,trim(basi_200.descr_unidade));

         EXCEPTION
            WHEN DUP_VAL_ON_INDEX then
               p_des_erro:= null;
            WHEN OTHERS THEN
               p_des_erro := 'Erro na inclus?o da tabela sped_0190_pc ' || Chr(10) ||
                             'Unidade de Medida: ' || basi_200.unidade_medida || Chr(10) || SQLERRM;
               RAISE W_ERRO;
         END;
      END LOOP;
   END LOOP;

   COMMIT;

EXCEPTION
   WHEN W_ERRO then
      p_des_erro := 'Erro na procedure p_gera_sped_0200_pc ' || Chr(10) || p_des_erro;

   WHEN OTHERS THEN
      p_des_erro := 'Outros erros na procedure p_gera_sped_0200_pc ' || Chr(10) || SQLERRM;
END inter_pr_gera_sped_0200_pc;

/
