insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('pcpb_f187', 'Metragem de fios usados na trama', 0,0);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'pcpb_f187', 'NENHUM', 0, 0, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'pcpb_f187', 'NENHUM', 0, 0, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Tamaño del hilo utilizado en la trama'
 where hdoc_036.codigo_programa = 'pcpb_f187'
   and hdoc_036.locale          = 'es_ES';
commit;


insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('pcpb_f187', 'Apontamento de paletes', 0,0);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'pcpb_f187', 'NENHUM', 0, 0, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'pcpb_f187', 'NENHUM', 0, 0, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Palet apuntando'
 where hdoc_036.codigo_programa = 'pcpb_f187'
   and hdoc_036.locale          = 'es_ES';
commit;
