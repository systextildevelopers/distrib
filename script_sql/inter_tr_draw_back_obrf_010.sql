create or replace trigger inter_tr_draw_back_obrf_010
   before update of despesas_importacao, flag_nota_fechada, qtd_pcs_faltantes, nota_estorno, 
	nota_dev, serie_dev, relac_nf_orig, moeda_nota, 
	num_form_final, tipo_doc_importacao_sped, nr_recibo, ano_inutilizacao, 
	cod_justificativa, valor_iss, nr_duplicata, situacao_entrada, 
	num_conhecimento, val_conhecimento, tipo_frete, transpa_forne9, 
	transpa_forne4, transpa_forne2, numero_volume, peso_liquido, 
	peso_bruto, observacao1, observacao2, fornec_cliente, 
	classif_contabil, qtde_itens, local_entrega, rol_embalagens, 
	base_diferenca, terceiros, valor_desconto, valor_funrural, 
	total_docto, valor_icms_sub, base_icms_sub, marca_volumes, 
	especie_volumes, qtde_volumes, historico_cont, flag_devolucao, 
	sit_divergencia, ser_conhecimento, tipo_conhecimento, num_contabil, 
	despesas_extras, nota_rateada_estq, valor_desp_post, divisao_qualif, 
	responsavel9, responsavel4, responsavel2, selo_inicial, 
	selo_final, data_de_digitacao, data_digitacao, numero_di, 
	tarifa_frete, sit_dup_uni, complemento_dup, msg_corpo1, 
	msg_corpo2, valor_iva_1, valor_iva_2, data_nsu, 
	hora_nsu, nsu, numero_formulario, num_form_inicial, 
	origem, valor_indice_moeda, codigo_cai, dt_valida_cai, 
	executa_trigger, tipo_valor_nf, numero_danf_nfe, tipo_valores_fiscal, 
	num_processo_imp, valor_pis_import_sped, valor_cofins_import_sped, data_desembaraco_sped, 
	valor_cif_sped, valor_desp_sem_icms_sped, valor_desp_com_icms_sped, valor_imposto_opera_finan_sped, 
	valor_imposto_import_sped, data_registro_sisco_sped, nr_protocolo, justificativa, 
	nr_final_inut, cod_status, msg_status, cod_solicitacao_nfe, 
	status_impressao_danfe, nfe_contigencia, serie_original, cod_canc_nfisc, 
	tipo_nf_referenciada, nota_referenciada, serie_referenciada, cnpj9_ref, 
	cnpj4_ref, cnpj2_ref, doc_conta_corrente, xml_nprot, 
	versao_systextilnfe, local_impressao, chave_contingencia, data_autorizacao_nfe, 
	exec_processo_imp, tipo_desconto, st_flag_cce, documento, 
	serie, cgc_cli_for_9, cgc_cli_for_4, cgc_cli_for_2, 
	especie_docto, natoper_nat_oper, natoper_est_oper, via_transporte, 
	codigo_transacao, data_transacao, data_emissao, base_icms, 
	valor_itens, valor_icms, valor_total_ipi, valor_despesas, 
	valor_frete, valor_seguro, icms_frete, condicao_pagto
   or delete 
   on obrf_010
   for each row
   /*Esta trigger serve para atualizar a qtde produzida ou a qtde rejeitada no exec_010 */

declare
   v_nr_draw          obrf_011.draw_back_entrada%type;
   v_tem_itens        number;
   v_executa_trigger  number;

begin
   -- INICIO - L�gica implementada para controle de "gatilho" das triggers do banco
   -- para atender a implementa��o executada para limpeza da base de dados do cliente
   -- e replica��o dos dados para base hist�rico. (SS.38405)
   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then
      if  updating
      then
         if :new.situacao_entrada = 4 and :old.situacao_entrada = 5
         then
            if :old.especie_docto = 'FAC' or :old.especie_docto = 'NFF' or :old.especie_docto = 'NFE'
            then
               /*verifica o nr do draw back*/
               begin
                  select nvl(max(obrf_011.draw_back_entrada),0) 
                  into v_nr_draw 
                  from obrf_011
                  where obrf_011.empresa = :old.local_entrega;
               end;

               v_nr_draw := v_nr_draw + 1;

               /**Insere dados nos itens***/
               inter_pr_insere_itens_draw
               (:old.local_entrega, :old.documento,     :old.serie, :old.cgc_cli_for_9,
                :old.cgc_cli_for_4, :old.cgc_cli_for_2,  v_nr_draw);
               
               begin
                  select count(*) 
                  into v_tem_itens 
                  from obrf_018
                  where obrf_018.empresa           = :old.local_entrega
                    and obrf_018.draw_back_entrada = v_nr_draw;
               end;

               if :old.natoper_est_oper = 'EX' and v_tem_itens > 0
               then
                  insert into obrf_011
                    (empresa,
                     draw_back_entrada,
                     nr_fatura_import,
                     serie_fatura_import,
                     cnpj9_import,
                     cnpj4_import,
                     cnpj2_import,
                     especie_import,
                     emissao_import,
                     moeda,
                     tipo_entrada)
                  values
                    (:old.local_entrega,
                     v_nr_draw,
                     :old.documento,
                     :old.serie,
                     :old.cgc_cli_for_9,
                     :old.cgc_cli_for_4,
                     :old.cgc_cli_for_2,
                     :old.especie_docto,
                     :old.data_emissao,
                     :old.moeda_nota,
                     2);
               end if;

               if :old.natoper_est_oper <> 'EX' and v_tem_itens > 0
               then
                 insert into obrf_011
                  (empresa,
                   draw_back_entrada,
                   nr_fatura_local,
                   serie_fatura_local,
                   cnpj9_local,
                   cnpj4_local,
                   cnpj2_local,
                   especie_local,
                   emissao_local,
                   moeda,
                   tipo_entrada)
                 values
                  (:old.local_entrega,
                   v_nr_draw,
                   :old.documento,
                   :old.serie,
                   :old.cgc_cli_for_9,
                   :old.cgc_cli_for_4,
                   :old.cgc_cli_for_2,
                   :old.especie_docto,
                   :old.data_emissao,
                   :old.moeda_nota,
                   1);
               end if;
            end if;

            if :old.especie_docto = 'NDE'
            then
               inter_pr_atualiza_debito
               (:old.local_entrega, :old.documento,     :old.serie, :old.cgc_cli_for_9,
                :old.cgc_cli_for_4, :old.cgc_cli_for_2,  v_nr_draw);
            end if;

            if :old.especie_docto = 'NCR'
            then
               inter_pr_atualiza_credito
               (:old.local_entrega, :old.documento,     :old.serie, :old.cgc_cli_for_9,
                :old.cgc_cli_for_4, :old.cgc_cli_for_2,  v_nr_draw);
            end if;
         end if;
      end if;
   end if;
end inter_tr_draw_back_obrf_010;


/

exec inter_pr_recompile;

/* versao: 4 */
