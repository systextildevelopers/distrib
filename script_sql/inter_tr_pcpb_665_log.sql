CREATE OR REPLACE trigger inter_tr_PCPB_665_log 
after insert or delete or update 
on PCPB_665 
for each row 
declare 
   ws_usuario_rede           varchar2(20) ; 
   ws_maquina_rede           varchar2(40) ; 
   ws_aplicativo             varchar2(20) ; 
   ws_sid                    number(9) ; 
   ws_empresa                number(3) ; 
   ws_usuario_systextil      varchar2(250) ; 
   ws_locale_usuario         varchar2(5) ; 
   v_nome_programa           varchar2(20) ; 


begin
-- Dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid, 
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario); 



    begin 
       select hdoc_090.programa 
       into v_nome_programa 
       from hdoc_090 
       where hdoc_090.sid = ws_sid 
         and rownum       = 1 
         and hdoc_090.programa not like '%menu%' 
         and hdoc_090.programa not like '%_m%'; 
       exception 
         when no_data_found then            v_nome_programa := 'SQL'; 
    end; 
 
 
 
 if inserting 
 then 
    begin 
 
        insert into PCPB_665_log (
           TIPO_OCORR,   /*0*/ 
           DATA_OCORR,   /*1*/ 
           HORA_OCORR,   /*2*/ 
           USUARIO_REDE,   /*3*/ 
           MAQUINA_REDE,   /*4*/ 
           APLICACAO,   /*5*/ 
           USUARIO_SISTEMA,   /*6*/ 
           NOME_PROGRAMA,   /*7*/ 
           SEQUENCIA_NUM_OLD,   /*8*/ 
           SEQUENCIA_NUM_NEW,   /*9*/ 
           ORDEM_BENEFICIAMENTO_OLD,   /*10*/ 
           ORDEM_BENEFICIAMENTO_NEW,   /*11*/ 
           LARGURA_MINIMA_OLD,   /*12*/ 
           LARGURA_MINIMA_NEW,   /*13*/ 
           LARGURA_MEDIA_OLD,   /*14*/ 
           LARGURA_MEDIA_NEW,   /*15*/ 
           COMPRIMENTO_OLD,   /*16*/ 
           COMPRIMENTO_NEW,   /*17*/ 
           DATA_ATUALIZACAO_OLD,   /*18*/ 
           DATA_ATUALIZACAO_NEW,   /*19*/ 
           STATUS_IMPRESSAO_OLD,   /*20*/ 
           STATUS_IMPRESSAO_NEW,    /*21*/
           SITUACAO_OLD,   /*22*/ 
           SITUACAO_NEW,   /*23*/ 
           OBS_MOTIVO_APROVACAO_OLD,   /*24*/ 
           OBS_MOTIVO_APROVACAO_new,   /*25*/ 
           MOTIVO_APROVACAO_OLD,   /*26*/ 
           MOTIVO_APROVACAO_NEW,   /*27*/ 
           QUALIDADE_ROLO_OLD, --28
           QUALIDADE_ROLO_NEW, --29
           CODIGO_MOTIVO_OLD,  --30
           CODIGO_MOTIVO_NEW   --31
        ) values (    
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/ 
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           0,/*8*/
           :new.SEQUENCIA_NUM, /*9*/   
           0,/*10*/
           :new.ORDEM_BENEFICIAMENTO, /*11*/   
           0,/*12*/
           :new.LARGURA_MINIMA, /*13*/   
           0,/*14*/
           :new.LARGURA_MEDIA, /*15*/   
           0,/*16*/
           :new.COMPRIMENTO, /*17*/   
           null,/*18*/
           :new.DATA_ATUALIZACAO, /*19*/   
           0,/*20*/
           :new.STATUS_IMPRESSAO, /*21*/   
           0, /*22*/
           :new.SITUACAO, /*23*/
           '',   /*24*/ 
           :new.OBS_MOTIVO_APROVACAO,   /*25*/ 
           0,   /*26*/ 
           :new.MOTIVO_APROVACAO,   /*27*/
           null, --28
           :new.QUALIDADE_ROLO, --29
           null, --30
           :new.CODIGO_MOTIVO -- 31
         );    
    end;    
 end if;    
  
  
 if updating 
 then 
    begin 
        insert into PCPB_665_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           SEQUENCIA_NUM_OLD, /*8*/   
           SEQUENCIA_NUM_NEW, /*9*/   
           ORDEM_BENEFICIAMENTO_OLD, /*10*/   
           ORDEM_BENEFICIAMENTO_NEW, /*11*/   
           LARGURA_MINIMA_OLD, /*12*/   
           LARGURA_MINIMA_NEW, /*13*/   
           LARGURA_MEDIA_OLD, /*14*/   
           LARGURA_MEDIA_NEW, /*15*/   
           COMPRIMENTO_OLD, /*16*/   
           COMPRIMENTO_NEW, /*17*/   
           DATA_ATUALIZACAO_OLD, /*18*/   
           DATA_ATUALIZACAO_NEW, /*19*/   
           STATUS_IMPRESSAO_OLD, /*20*/   
           STATUS_IMPRESSAO_NEW,  /*21*/  
           SITUACAO_OLD,   /*22*/ 
           SITUACAO_NEW,   /*23*/ 
           OBS_MOTIVO_APROVACAO_OLD,   /*24*/ 
           OBS_MOTIVO_APROVACAO_new,   /*25*/ 
           MOTIVO_APROVACAO_OLD,   /*26*/ 
           MOTIVO_APROVACAO_NEW,   /*27*/
           QUALIDADE_ROLO_OLD, --28
           QUALIDADE_ROLO_NEW, --29
           CODIGO_MOTIVO_OLD,  --30
           CODIGO_MOTIVO_NEW   --31
        ) values (    
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/ 
           :old.SEQUENCIA_NUM,  /*8*/  
           :new.SEQUENCIA_NUM, /*9*/   
           :old.ORDEM_BENEFICIAMENTO,  /*10*/  
           :new.ORDEM_BENEFICIAMENTO, /*11*/   
           :old.LARGURA_MINIMA,  /*12*/  
           :new.LARGURA_MINIMA, /*13*/   
           :old.LARGURA_MEDIA,  /*14*/  
           :new.LARGURA_MEDIA, /*15*/   
           :old.COMPRIMENTO,  /*16*/  
           :new.COMPRIMENTO, /*17*/   
           :old.DATA_ATUALIZACAO,  /*18*/  
           :new.DATA_ATUALIZACAO, /*19*/   
           :old.STATUS_IMPRESSAO,  /*20*/  
           :new.STATUS_IMPRESSAO,  /*21*/  
           :OLD.SITUACAO, /*22*/
           :new.SITUACAO, /*23*/
           :old.OBS_MOTIVO_APROVACAO,   /*24*/ 
           :new.OBS_MOTIVO_APROVACAO,   /*25*/ 
           :old.MOTIVO_APROVACAO,   /*26*/ 
           :new.MOTIVO_APROVACAO,   /*27*/
           :old.QUALIDADE_ROLO, --28
           :new.QUALIDADE_ROLO, --29
           :old.CODIGO_MOTIVO, -- 30
           :new.CODIGO_MOTIVO -- 31
         );    
    end;    
 end if;    
  
  
 if deleting 
 then 
    begin 
        insert into PCPB_665_log (
           TIPO_OCORR, /*0*/   
           DATA_OCORR, /*1*/   
           HORA_OCORR, /*2*/   
           USUARIO_REDE, /*3*/   
           MAQUINA_REDE, /*4*/   
           APLICACAO, /*5*/   
           USUARIO_SISTEMA, /*6*/   
           NOME_PROGRAMA, /*7*/   
           SEQUENCIA_NUM_OLD, /*8*/   
           SEQUENCIA_NUM_NEW, /*9*/   
           ORDEM_BENEFICIAMENTO_OLD, /*10*/   
           ORDEM_BENEFICIAMENTO_NEW, /*11*/   
           LARGURA_MINIMA_OLD, /*12*/   
           LARGURA_MINIMA_NEW, /*13*/   
           LARGURA_MEDIA_OLD, /*14*/   
           LARGURA_MEDIA_NEW, /*15*/   
           COMPRIMENTO_OLD, /*16*/   
           COMPRIMENTO_NEW, /*17*/   
           DATA_ATUALIZACAO_OLD, /*18*/   
           DATA_ATUALIZACAO_NEW, /*19*/   
           STATUS_IMPRESSAO_OLD, /*20*/   
           STATUS_IMPRESSAO_NEW, /*21*/   
           SITUACAO_OLD,   /*22*/ 
           SITUACAO_NEW,   /*23*/ 
           OBS_MOTIVO_APROVACAO_OLD,   /*24*/ 
           OBS_MOTIVO_APROVACAO_new,   /*25*/ 
           MOTIVO_APROVACAO_OLD,   /*26*/ 
           MOTIVO_APROVACAO_NEW,   /*27*/
           QUALIDADE_ROLO_OLD, --28
           QUALIDADE_ROLO_NEW, --29
           CODIGO_MOTIVO_OLD,  --30
           CODIGO_MOTIVO_NEW   --31
        ) values (    
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/ 
            ws_maquina_rede,/*4*/ 
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/ 
            v_nome_programa, /*7*/
           :old.SEQUENCIA_NUM, /*8*/   
           0, /*9*/
           :old.ORDEM_BENEFICIAMENTO, /*10*/   
           0, /*11*/
           :old.LARGURA_MINIMA, /*12*/   
           0, /*13*/
           :old.LARGURA_MEDIA, /*14*/   
           0, /*15*/
           :old.COMPRIMENTO, /*16*/   
           0, /*17*/
           :old.DATA_ATUALIZACAO, /*18*/   
           null, /*19*/
           :old.STATUS_IMPRESSAO, /*20*/   
           0, /*21*/
           :OLD.SITUACAO, /*22*/
           0, /*23*/
           :old.OBS_MOTIVO_APROVACAO,   /*24*/ 
           null,   /*25*/ 
           :old.MOTIVO_APROVACAO,   /*26*/ 
           0, /*27*/
           :old.QUALIDADE_ROLO, --28
           null, --29
           :old.CODIGO_MOTIVO, -- 30
           null -- 31
         );    
    end;    
 end if;    
end inter_tr_PCPB_665_log;

/
