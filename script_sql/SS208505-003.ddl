drop table LIST_CRITICA_ESTOQUE;

-- Create table
create table LIST_CRITICA_ESTOQUE
(
  ind_partida          VARCHAR2(400) default ' ',
  id_execucao          NUMBER(15) default 0,
  nivel                VARCHAR2(1) default ' ',
  grupo                VARCHAR2(5) default ' ',
  subgrupo             VARCHAR2(3) default ' ',
  item                 VARCHAR2(6) default ' ',
  deposito             NUMBER(5) default 0,
  lote                 NUMBER(6) default 0,
  qtde_estoque         NUMBER(13,3) default 0.0,
  qtde_em_vol          NUMBER(13,3) default 0.0,
  qtde_em_card         NUMBER(13,3) default 0.0,
  cor_estoque          VARCHAR2(6),
  qtde_sugerida        NUMBER(13,3) default 0.0,
  qtde_empenhada       NUMBER(13,3) default 0.0,
  preco_medio_unitario NUMBER(15,5) default 0.0,
  data_execucao        DATE,
  observacao           VARCHAR2(1000) default ' ',
  data_diverg_estq     DATE
);

/
