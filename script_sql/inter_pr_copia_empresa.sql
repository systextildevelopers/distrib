
  CREATE OR REPLACE PROCEDURE "INTER_PR_COPIA_EMPRESA" (
      p_tabela_empresa  in varchar2,
      p_empresa_origem  in number,
      p_empresa_destino in number)
is
  v_sql_insert      varchar2(32000);
  v_colunas_tabela  varchar2(32000);

begin

  v_colunas_tabela := ' ';

  for tabela in (select lower(column_name) coluna
                 from all_tab_columns
                 where lower(table_name) = lower(p_tabela_empresa))
  loop
    if trim(v_colunas_tabela) is null
    then v_colunas_tabela := tabela.coluna;
    else v_colunas_tabela := v_colunas_tabela ||','||tabela.coluna;
    end if;
  end loop;

  if v_colunas_tabela is not null
  then
    v_sql_insert := 'insert into'            ||' '||
                    p_tabela_empresa         ||' '||chr(10)||
                    '('                      ||' '||
                    v_colunas_tabela         ||' '||
                    ')'                      ||' '||chr(10)||
                    'select '                ||' '||
                    replace(v_colunas_tabela, 'codigo_empresa,', to_char(p_empresa_destino)||',') ||' '||chr(10)||
                    'from '                  ||' '||
                    p_tabela_empresa         ||' '||chr(10)||
                    'where codigo_empresa =' ||' '||
                    to_char(p_empresa_origem);
    begin
      execute immediate v_sql_insert;
    exception when others then
       --raise_application_error(-20000,'Nao conseguiu fazer o insert da tabela ' || p_tabela_empresa || ' com os dados abaixo:'||chr(10)|| v_sql_insert||chr(10)||SQLERRM);
       raise_application_error(-20000,'Nao conseguiu fazer o insert da tabela ' || p_tabela_empresa || '. ERRO:'||SQLERRM);
    end;
  else
    raise_application_error(-20000,'Nao conseguiu recuperar as colunas da tabela ' || p_tabela_empresa);
  end if;
end;

 

/

exec inter_pr_recompile;

