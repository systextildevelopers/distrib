

insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('mqop_f031', 'Adicionar as maquinas suas respectivas partes', 0,0);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'mqop_f031', 'NENHUM', 0, 0, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'mqop_f031', 'NENHUM', 0, 0, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Agregue las máquinas y sus respectivas partes'
 where hdoc_036.codigo_programa = 'mqop_f031'
   and hdoc_036.locale          = 'es_ES';
commit;



insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('mqop_f032', 'Adicionar as partes os respectivos componentes', 0,0);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'mqop_f032', 'NENHUM', 0, 0, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'mqop_f032', 'NENHUM', 0, 0, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Agregar piezas a sus respectivos componentes'
 where hdoc_036.codigo_programa = 'mqop_f032'
   and hdoc_036.locale          = 'es_ES';
commit;
