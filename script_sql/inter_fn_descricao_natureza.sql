
  CREATE OR REPLACE FUNCTION "INTER_FN_DESCRICAO_NATUREZA" (p_cod_empresa in number, p_num_nota in number, p_serie_nota in varchar2)
return varchar2 is

   Result              varchar2(100);
   v_descr_nat_oper    pedi_080.descr_nat_oper%type;

begin

   begin
     select trim(pedi_080.razao_social_natureza)
     into   v_descr_nat_oper
     from fatu_500, pedi_080, fatu_050
     where fatu_050.codigo_empresa    = p_cod_empresa
       and fatu_050.num_nota_fiscal   = p_num_nota
       and fatu_050.serie_nota_fisc   = p_serie_nota
       and pedi_080.flag_razao_social = 1
       and fatu_050.cgc_9             = fatu_500.cgc_9
       and fatu_050.cgc_4             = fatu_500.cgc_4
       and fatu_050.cgc_2             = fatu_500.cgc_2
       and fatu_050.codigo_empresa    = fatu_500.codigo_empresa
       and pedi_080.natur_operacao    = fatu_050.natop_nf_nat_oper
       and pedi_080.estado_natoper    = fatu_050.natop_nf_est_oper;
   exception when no_data_found then
     v_descr_nat_oper := '';
   end;

   result := trim(v_descr_nat_oper);

   return(Result);

end inter_fn_descricao_natureza;

 

/

exec inter_pr_recompile;

