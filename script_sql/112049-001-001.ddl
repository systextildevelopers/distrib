alter table basi_320 add (cod_representante number(5));

alter table basi_320 add (codigo_cor2 varchar2(6));
alter table basi_320 add (descr_cor2 varchar2(20));

alter table basi_320 add (codigo_cor3 varchar2(6));
alter table basi_320 add (descr_cor3 varchar2(20));

alter table basi_320 add (codigo_cor4 varchar2(6));
alter table basi_320 add (descr_cor4 varchar2(20));

alter table basi_320 add (tipo_cor number(1));
alter table basi_320 add (descr_tipo_cor varchar2(30));


comment on COLUMN basi_320.cod_representante is 'Código do Representante (ERC) que solicitou';
comment on COLUMN basi_320.codigo_cor2 is 'Código da cor 2';
comment on COLUMN basi_320.descr_cor2 is 'Descrição da cor 2';
comment on COLUMN basi_320.codigo_cor3 is 'Código da cor 3';
comment on COLUMN basi_320.descr_cor3 is 'Descrição da cor 3';
comment on COLUMN basi_320.codigo_cor4 is 'Código da cor 4';
comment on COLUMN basi_320.descr_cor4 is 'Descrição da cor 4';
comment on COLUMN basi_320.tipo_cor is 'Tipo da cor (1: Pantone TC, 2: Pantone TPX, 3: Conforme amostra)';
comment on COLUMN basi_320.descr_tipo_cor is 'Descrição do tipo da cor';

exec inter_pr_recompile;
