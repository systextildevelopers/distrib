--mqop_f031
create table mqop_033 (
  GRUPO_MAQUINA varchar2(4) not null,
  SUBGRUPO_MAQUINA varchar2(3) not null,
  NUMERO_MAQUINA number(5) not null,
  COD_PARTE number(4) not null,
  primary key(GRUPO_MAQUINA, SUBGRUPO_MAQUINA, NUMERO_MAQUINA, COD_PARTE));
--mqop_f032
create table mqop_034 (
  GRUPO_MAQUINA varchar2(4) not null,
  SUBGRUPO_MAQUINA varchar2(3) not null,
  NUMERO_MAQUINA number(5) not null,
  COD_PARTE number(4) not null,
  COD_COMP number(4) not null,
  primary key(GRUPO_MAQUINA, SUBGRUPO_MAQUINA, NUMERO_MAQUINA,COD_PARTE, COD_COMP));

  alter table mqop_034 add constraint fk_mqop_034_033 foreign key (grupo_maquina,subgrupo_maquina,numero_maquina,cod_parte)
    references mqop_033 (grupo_maquina,subgrupo_maquina,numero_maquina,cod_parte) on delete cascade enable novalidate;

  comment on table mqop_033 is 'Tabela de associação das partes de maquinas';
  comment on table mqop_034 is 'Tabela de associação dos componentes das partes de maquinas';


  comment on column mqop_033.cod_parte            is 'Codigo da parte de maquina cadastrado na mqop_f031';
  comment on column mqop_034.cod_comp             is 'Codigo do componente da parte da maquina mqop_f032';
