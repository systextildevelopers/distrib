alter table oper_277 rename column data_hora_consulta to data_hora;

alter table oper_277 add tipo_processo varchar2(15);

alter table oper_277 add constraint oper_277_ck_tipo_processo check (tipo_processo in ('importacao','consulta'));
