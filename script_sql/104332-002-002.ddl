-- Create table
create table obrf_820
( COD_EMPRESA           NUMBER(3) default 0 not null,
  MES                   NUMBER(2) default 0 not null,
  ANO                   NUMBER(4) default 0 not null,
  DEB_SAIDAS            NUMBER(12,2) default 0.00,
  DEB_AJUSTES_CNF       NUMBER(12,2) default 0.00,
  DEB_AJUSTES_SNF       NUMBER(12,2) default 0.00,
  CRE_ESTORNOS          NUMBER(12,2) default 0.00,
  CRE_NF                NUMBER(12,2) default 0.00,
  CRE_AJUSTES_CNF       NUMBER(12,2) default 0.00,
  CRE_AJUSTES_SNF       NUMBER(12,2) default 0.00,
  DEB_ESTORNOS          NUMBER(12,2) default 0.00,      
  SALDO_ANTERIOR        NUMBER(12,2) default 0.00,
  SALDO_APURADO         NUMBER(12,2) default 0.00,
  VLR_TOT_DEDUCOES      NUMBER(12,2) default 0.00,
  VLR_ICM_RECOLHER      NUMBER(12,2) default 0.00,
  VLR_CREDOR_TRANSP     NUMBER(12,2) default 0.00,
  DEB_ESPECIAIS         NUMBER(12,2) default 0.00,      
  VLR_COMPLEMENTARES    NUMBER(12,2) default 0.00);
  
-- Add comments to the columns 
comment on column OBRF_820.COD_EMPRESA           is 'Identifica a empresa';
comment on column OBRF_820.MES                   is 'Mes da apuracao';
comment on column OBRF_820.ANO                   is 'Ano apuracao';
comment on column OBRF_820.DEB_SAIDAS            is '02 Valor dos debitos por nota de saida.';
comment on column OBRF_820.DEB_AJUSTES_CNF       is '03 Valor de Ajustes de debitos por nota fiscal C197 e D197';
comment on column OBRF_820.DEB_AJUSTES_SNF       is '04 Valor total de ajustes de debitos sem nota fiscal E111';
comment on column OBRF_820.CRE_ESTORNOS          is '05 Valor de estornos de creditos E111';
comment on column OBRF_820.CRE_NF                is '06 Creditos por Notas Fiscais';
comment on column OBRF_820.CRE_AJUSTES_CNF       is '07 Valor d Ajustes de creditos por nota fiscal C197 e D197';
comment on column OBRF_820.CRE_AJUSTES_SNF       is '08 Valor total de ajustes de creditos sem nota fiscal E111';
comment on column OBRF_820.DEB_ESTORNOS          is '09 valor de Estorno debito E111';
comment on column OBRF_820.SALDO_ANTERIOR        is '10 valor do saldo credor anterior';
comment on column OBRF_820.SALDO_APURADO         is '11 valor do saldo apurado';
comment on column OBRF_820.VLR_TOT_DEDUCOES      is '12 valor total de deducoes C197,D197,E111';
comment on column OBRF_820.VLR_ICM_RECOLHER      is '13 Valor Icms a Recolher';
comment on column OBRF_820.VLR_CREDOR_TRANSP     is '14 Valor saldo credor a transportar';
comment on column OBRF_820.DEB_ESPECIAIS         is '15 Debitos especiais';
comment on column OBRF_820.VLR_COMPLEMENTARES    is '99 Valores Complementares';

-- Create/Recreate primary, unique and foreign key constraints 
alter table OBRF_820 add constraint PK_OBRF_820 primary key (COD_EMPRESA, ANO, MES);

create synonym systextilrpt.OBRF_820 for OBRF_820; 
