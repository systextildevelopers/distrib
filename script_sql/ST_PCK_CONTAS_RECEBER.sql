create or replace package ST_PCK_CONTAS_RECEBER as

  TYPE t_valida_titulo IS RECORD (
        validar_cliente BOOLEAN,
        validar_tipo_titulo BOOLEAN,
        validar_cotacao BOOLEAN,
        validar_codigo_moeda BOOLEAN,
        validar_valor BOOLEAN,
        validar_empresa BOOLEAN
    );

  -- Func?o para realizar as validac?es
    FUNCTION valida_titulo(p_valida_titulo t_valida_titulo, p_codigo_empresa int, p_nr_duplicata int, p_seq_duplicata int, p_tipo_titulo int, p_cnpj9 int, p_cnpj4 int, p_cnpj2 int, p_cotacao int, p_valor int, p_codigo_moeda int, p_campo_erro_valida out varchar2) RETURN VARCHAR2;

    procedure criar_titulo(
            p_codigo_empresa in fatu_070.codigo_empresa%type,
            p_cli_dup_cgc_cli9 in fatu_070.cli_dup_cgc_cli9%type,
            p_cli_dup_cgc_cli4 in fatu_070.cli_dup_cgc_cli4%type, 
            p_cli_dup_cgc_cli2 in fatu_070.cli_dup_cgc_cli9%type,
            p_tipo_titulo in fatu_070.tipo_titulo%type,
            p_num_duplicata in fatu_070.num_duplicata%type,
            p_seq_duplicatas in fatu_070.seq_duplicatas%type,
            p_data_venc_duplic in fatu_070.data_venc_duplic%type,
            p_valor_duplicata in fatu_070.valor_duplicata%type,
            p_cod_rep_cliente in fatu_070.cod_rep_cliente%type,
            p_data_emissao in fatu_070.data_emissao%type,
            p_data_transacao IN date,
            p_numero_titulo in fatu_070.numero_titulo%type,
            p_numero_sequencia in fatu_070.numero_sequencia%type,
            p_cod_historico in fatu_070.cod_historico%type,
            p_cod_local in fatu_070.cod_local%type,
            p_port_anterior in fatu_070.port_anterior%type,
            p_portador_duplic in fatu_070.portador_duplic%type,
            p_posicao_duplic in fatu_070.posicao_duplic%type,
            p_data_prorrogacao in fatu_070.data_prorrogacao%type,
            p_tecido_peca in fatu_070.tecido_peca%type,
            p_seq_end_cobranca in fatu_070.seq_end_cobranca%type,
            p_comissao_lancada in fatu_070.comissao_lancada%type,
            p_tipo_tit_origem in fatu_070.tipo_tit_origem%type,
            p_num_dup_origem in fatu_070.num_dup_origem%type,
            p_seq_dup_origem in fatu_070.seq_dup_origem%type,
            p_cli9resptit in fatu_070.cli9resptit%type,
            p_cli4resptit in fatu_070.cli4resptit%type,
            p_cli2resptit in fatu_070.cli2resptit%type,
            p_origem_pedido in fatu_070.origem_pedido%type,
            p_cod_transacao in out fatu_070.cod_transacao%type,
            p_codigo_contabil in fatu_070.codigo_contabil%type,
            p_valor_moeda in fatu_070.valor_moeda%type,
            p_nr_identificacao in fatu_070.nr_identificacao%type,
            p_conta_corrente in fatu_070.conta_corrente%type,
            p_num_contabil in fatu_070.num_contabil%type,
            p_duplic_emitida in fatu_070.duplic_emitida%type,
            p_pedido_venda in fatu_070.pedido_venda%type,
            p_cod_processo    in fatu_070.cod_processo%type,
            p_codigo_depto IN number,
            p_emitente_titulo IN varchar2,
            p_codigo_historico IN number,
            p_moeda_titulo in number,
            p_cotacao_moeda in number,
            p_prg_gerador IN varchar2,
            p_usuario in varchar2,
            p_num_lcmt IN OUT number,
            r_success out varchar2,
            r_des_erro out varchar2
        );

end;
