CREATE OR REPLACE VIEW stview_consumo_tecido AS
SELECT vi_estr.nivel_item NIVEL,
       vi_estr.grupo_item GRUPO,
       vi_estr.sub_item SUBGRUPO,
       vi_estr.item_item ITEM,
       basi_030.descr_referencia DESCR_REFERENCIA,
       basi_030.colecao COLECAO,
       basi_140.descr_colecao DESCR_COLECAO,
       basi_030.linha_produto LINHA_PRODUTO,
       basi_120.descricao_linha DESCR_LINHA,
       vi_estr.alternativa_item ALTERNATIVA_ITEM,
       vi_estr.sequencia SEQUENCIA,
       vi_estr.nivel_comp NIVEL_COMP,
       vi_estr.grupo_comp GRUPO_COMP,
       vi_estr.sub_comp SUBGRUPO_COMP,
       vi_estr.item_comp ITEM_COMP,
       basi_010.narrativa NARRATIVA,
       basi_010.item_ativo ITEM_ATIVO,
       vi_estr.alternativa_comp ALTERNATIVA_COMP,
       vi_estr.consumo CONSUMO,
       vi_estr.estagio ESTAGIO,
       mqop_005.descricao DESCRICAO
FROM inter_vi_estrutura vi_estr,
     mqop_005,
     basi_010,
     basi_030,
     basi_140,
     basi_120
WHERE vi_estr.estagio        = mqop_005.codigo_estagio
  AND vi_estr.nivel_comp     = basi_010.nivel_estrutura
  AND vi_estr.grupo_comp     = basi_010.grupo_estrutura
  AND vi_estr.sub_comp       = basi_010.subgru_estrutura
  AND vi_estr.item_comp      = basi_010.item_estrutura
  AND vi_estr.nivel_item     = basi_030.nivel_estrutura
  AND vi_estr.grupo_item     = basi_030.referencia
  AND basi_030.colecao       = basi_140.colecao
  AND basi_030.linha_produto = basi_120.linha_produto;
/

exec inter_pr_recompile;
