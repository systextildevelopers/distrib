CREATE OR REPLACE PROCEDURE "INTER_PR_FLX_CREC" (p_cod_usuario          IN NUMBER,
                                               p_nom_usuario          IN VARCHAR2,
                                               p_data_lida            IN DATE,
                                               p_data_ini_consulta    IN DATE,
                                               p_data_fim_consulta    IN DATE,
                                               p_inclui_port_rec      IN NUMBER,
                                               p_filtro_empresa       IN NUMBER,
                                               p_incl_exce            IN NUMBER,
                                               p_opcao_posic          IN NUMBER,
                                               p_considera_exportacao IN NUMBER,
                                               p_posic01              IN NUMBER,
                                               p_posic02              IN NUMBER,
                                               p_posic03              IN NUMBER,
                                               p_posic04              IN NUMBER,
                                               p_posic05              IN NUMBER,
                                               p_cod_emp1             IN NUMBER,
                                               p_cod_emp2             IN NUMBER,
                                               p_cod_emp3             IN NUMBER,
                                               p_cod_emp4             IN NUMBER,
                                               p_cod_emp5             IN NUMBER,
                                               p_moeda_titulo         IN NUMBER,
                                               p_origem_dados_lh_tp   IN NUMBER,
                                               p_rec_confir_vencido   IN NUMBER,
                                               p_rec_previs_vencido   IN NUMBER,
                                               p_programa             IN VARCHAR2,
                                               p_data_ini             IN NUMBER,
                                               p_des_erro             OUT varchar2) is

    w_erro                    EXCEPTION;
    w_nome_cliente            pedi_010.nome_cliente%type;
    w_cidade_cliente          pedi_010.cod_cidade%type;
    w_nome_portador           pedi_050.nome_banco%type;
    w_nome_moeda              basi_265.descricao%type;
    w_registro_ok             varchar2(1);
    w_uf_cliente              basi_160.estado%type;
    w_tipo_historico          cont_010.tipo_historico%type;
    w_nome_tipo               cpag_040.descricao%type;
    w_nome_empr               fatu_500.nome_empresa%type;
    w_controla_vencto         fatu_500.controla_vencto%type;
    w_fluxo                   cont_010.fluxo%type;
    w_tp_cliente              pedi_010.tipo_cliente%type;
    w_descr_tp_clie           pedi_085.descr_tipo_clien%type;
    w_indice_moeda_informado  basi_270.valor_moeda%type;
    w_entra_fluxo_caixa       cpag_040.entra_fluxo_caixa%type;
    w_tipo_registro_flxc      varchar2(10);
    w_sequencia               number(9);
    w_he_dia_util_3           basi_260.dia_util_finan%type;
    w_data_util_3             basi_260.data_calendario%type;
    w_letra                   varchar2(1);
    w_descr_conta_fluxo       pedi_085.descr_tipo_clien%type;
    w_ja_inseriu              number(1);
    w_achou_registros         number(1);
    w_valor_aux               cpag_825.valor%type;
    w_valor_aplic             cpag_825.valor%type;
    w_vencimento_aux          fatu_070.data_prorrogacao%type;
BEGIN
   w_ja_inseriu := 0;
   FOR crec in (
      select 
             ((select min(data_calendario)   
               from basi_260
               where data_calendario >= fatu_070.data_prorrogacao
               and dia_util_finan = 0) + 
               decode(fatu_503.tipo_fluxo_caixa, 1, loja_010.dias_fluxo, 0)) data_prorrogacao, --Correto
             --(fatu_070.data_prorrogacao + 
             --decode(fatu_503.tipo_fluxo_caixa, 1, loja_010.dias_fluxo, 0)) data_prorrogacao, --Antigo
             fatu_070.saldo_duplicata,
             fatu_070.cod_historico,     fatu_070.cli_dup_cgc_cli9,
             fatu_070.cli_dup_cgc_cli4,  fatu_070.cli_dup_cgc_cli2,
             fatu_070.tipo_titulo,       fatu_070.num_duplicata,
             fatu_070.seq_duplicatas,    fatu_070.codigo_empresa,
             fatu_070.cod_canc_duplic,   fatu_070.serie_nota_fisc,
             fatu_070.posicao_duplic,    fatu_070.data_venc_duplic,
             fatu_070.moeda_titulo,      fatu_070.previsao,
             fatu_070.valor_moeda,       fatu_070.valor_duplicata,
             fatu_070.portador_duplic
   from fatu_070, fatu_503, loja_010
   where (fatu_070.data_prorrogacao = p_data_lida or
          (p_data_ini = 1                          and
          fatu_070.data_prorrogacao < p_data_ini_consulta     and
          fatu_070.saldo_duplicata  > 0)
          )
     and fatu_070.cod_canc_duplic  = 0
     and fatu_070.situacao_duplic <> 2
     and fatu_070.moeda_titulo     = p_moeda_titulo
     and ((p_inclui_port_rec = 1
           and exists (select 1 from rcnb_060
                       where rcnb_060.tipo_registro    = 139
                         and rcnb_060.nr_solicitacao   = 810
                         and rcnb_060.subgru_estrutura = p_cod_usuario
                         and rcnb_060.item_estrutura   = 1
                         and rcnb_060.grupo_estrutura  = fatu_070.portador_duplic))
       or  (p_inclui_port_rec = 2
            and not exists (select 1 from rcnb_060
                        where rcnb_060.tipo_registro    = 139
                          and rcnb_060.nr_solicitacao   = 810
                          and rcnb_060.subgru_estrutura = p_cod_usuario
                          and rcnb_060.item_estrutura   = 1
                          and rcnb_060.grupo_estrutura = fatu_070.portador_duplic
                          and rcnb_060.grupo_estrutura <> 9999)))

     and (p_filtro_empresa = 1
            and (p_incl_exce = 1
                and fatu_070.codigo_empresa in (p_cod_emp1,p_cod_emp2,p_cod_emp3,p_cod_emp4,p_cod_emp5))
            or  (p_incl_exce = 2
                and fatu_070.codigo_empresa not in (p_cod_emp1,p_cod_emp2,p_cod_emp3,p_cod_emp4,p_cod_emp5))
        or (p_filtro_empresa = 2
            and (p_incl_exce = 1
                and fatu_070.responsavel_receb in (p_cod_emp1,p_cod_emp2,p_cod_emp3,p_cod_emp4,p_cod_emp5))
            or  (p_incl_exce = 2 )
                and fatu_070.responsavel_receb in (p_cod_emp1,p_cod_emp2,p_cod_emp3,p_cod_emp4,p_cod_emp5)))

     and (   (p_opcao_posic = 1 and fatu_070.posicao_duplic in (p_posic01, p_posic02,p_posic03,p_posic04,p_posic05))
          or (p_opcao_posic = 2 and fatu_070.posicao_duplic not in (p_posic01, p_posic02,p_posic03,p_posic04,p_posic05))
          )
     and fatu_070.cod_forma_pagto = loja_010.forma_pgto
     and fatu_070.codigo_empresa = fatu_503.codigo_empresa
   )
   LOOP
      w_vencimento_aux  := crec.data_prorrogacao;
      w_achou_registros := 1;

      BEGIN
         select cont_010.fluxo into w_fluxo from cont_010
         where cont_010.codigo_historico = crec.cod_historico;
      EXCEPTION
         when no_data_found then
            w_fluxo := 0;
      END;

      if w_fluxo = 1
      then
          BEGIN
             select pedi_010.nome_cliente,  pedi_010.cod_cidade,
                    pedi_010.tipo_cliente
             into   w_nome_cliente,         w_cidade_cliente,
                    w_tp_cliente
             from pedi_010
             where pedi_010.cgc_9 = crec.cli_dup_cgc_cli9
               and pedi_010.cgc_4 = crec.cli_dup_cgc_cli4
               and pedi_010.cgc_2 = crec.cli_dup_cgc_cli2;
           EXCEPTION
              when no_data_found then
                 w_nome_cliente   := '';
                 w_cidade_cliente := 0;
           END;

           BEGIN
              select pedi_085.descr_tipo_clien into w_descr_tp_clie from pedi_085
              where pedi_085.tipo_cliente = w_tp_cliente;
           EXCEPTION
              when no_data_found then
                 w_descr_tp_clie := ' ';
           END;

           BEGIN
              select cpag_040.descricao, cpag_040.entra_fluxo_caixa
              into   w_nome_tipo,        w_entra_fluxo_caixa
                from cpag_040
                where cpag_040.tipo_titulo = crec.tipo_titulo;
           EXCEPTION
              when no_data_found then
                 w_nome_tipo := ' ';
                 w_entra_fluxo_caixa := 0;
           END;

           if w_entra_fluxo_caixa = 1
           then
              w_descr_tp_clie := w_nome_tipo;
              w_tp_cliente    := crec.tipo_titulo;
           end if;

           BEGIN
              select pedi_050.nome_banco into w_nome_portador from pedi_050
              where pedi_050.cod_portador = crec.portador_duplic;
           EXCEPTION
              when no_data_found then
                 w_nome_portador   := '';
           END;

           BEGIN
              select  basi_265.descricao into w_nome_moeda from basi_265
              where basi_265.codigo_moeda = crec.moeda_titulo;
           EXCEPTION
              when no_data_found then
                 w_nome_moeda   := '';
           END;

           w_registro_ok := 's';

           BEGIN
              select basi_160.estado into w_uf_cliente from basi_160
              where basi_160.cod_cidade = w_cidade_cliente;
           EXCEPTION
              when no_data_found then
                 w_uf_cliente := 'KK';
           END;

           if w_uf_cliente = 'EX' and p_considera_exportacao = 1
           then
              w_registro_ok := 'n';
           end if;

           if w_registro_ok = 's'
           then
             BEGIN
                 select cont_010.tipo_historico into w_tipo_historico from cont_010
                 where cont_010.codigo_historico = crec.cod_historico;
             EXCEPTION
                when no_data_found then
                   w_tipo_historico := 0;
             END;

             if w_tipo_historico <> 'NC'
             then
                BEGIN
                   select fatu_500.nome_empresa, fatu_500.controla_vencto
                   into   w_nome_empr,           w_controla_vencto
                   from fatu_500
                   where fatu_500.codigo_empresa = crec.codigo_empresa;
                EXCEPTION
                   when no_data_found then
                      w_nome_empr        := ' ';
                      w_controla_vencto  := 0;
                END;


               if crec.moeda_titulo > 0 and crec.saldo_duplicata <> 0.00
               then
                  BEGIN
                     select basi_270.valor_moeda into w_indice_moeda_informado  from basi_270
                     where basi_270.codigo_moeda  = crec.moeda_titulo
                       and basi_270.data_moeda   <= crec.data_prorrogacao
                       and rownum                 = 1
                     order by basi_270.data_moeda desc;
                  EXCEPTION
                     when no_data_found then
                        w_indice_moeda_informado := 1.00;
                  END;

                  if w_indice_moeda_informado = 0.00
                  then
                     w_indice_moeda_informado := 1.00;
                  end if;

                  crec.saldo_duplicata  := round(crec.saldo_duplicata * w_indice_moeda_informado,2);
               end if;

               w_letra              := 'B';

              /*if p_tipo_tit_aplic_fina = crec.tipo_titulo
               then
                  -- TIPO DE TITULOS DE APLICACOES FINANCEIRAS
                  w_tipo_registro_flxc  := 'aplic';
                  w_letra               := 'E';
                  crec.data_prorrogacao := p_data_ini_consulta;
               else*/
                if crec.previsao = 0
                then
                   -- TIPO DE TITULOS DE CONFIRMADOS
                   w_tipo_registro_flxc := 'duplic';

                   if crec.saldo_duplicata > 0.00 and crec.data_prorrogacao < to_date(sysdate, 'DD/MM/YY')
                   then
                      /*
                        1- Descartar Titulo;
                        2- Trazer a data de vencimento para a data atual;
                        3- Trazer a data de vencimento para o primeiro dia util apos a data corrente;
                        4- Trazer a data de vencimento para o ultimo dia do mes de vencimento;
                      */
                      if p_rec_confir_vencido = 1
                      then
                         crec.saldo_duplicata := 0.00;
                      else
                         if p_rec_confir_vencido = 2
                         then
                            crec.data_prorrogacao := to_date(sysdate, 'DD/MM/YY');
                         else
                            if p_rec_confir_vencido = 3
                            then
                               /* PROCURA DIA UTIL FINAL */
                               BEGIN
                                  select basi_260.dia_util_finan into w_he_dia_util_3 from basi_260
                                  where basi_260.data_calendario = to_date(sysdate, 'DD/MM/YY') + 1;
                               EXCEPTION
                                  when no_data_found then
                                     w_he_dia_util_3 := null;
                               END;

                               if w_he_dia_util_3 is null
                               then
                                  w_he_dia_util_3 := 1;
                               end if;

                               if w_he_dia_util_3 = 1
                               then
                                  BEGIN
                                     select data_calendario into w_data_util_3 from basi_260
                                     where basi_260.data_calendario >= to_date(sysdate, 'DD/MM/YY') + 1
                                       and basi_260.dia_util_finan   = 0
                                       and rownum                    = 1
                                     order by basi_260.data_calendario asc;
                                  EXCEPTION
                                     when no_data_found then
                                        w_data_util_3 := null;
                                  END;
                               else
                                  w_data_util_3 := to_date(sysdate, 'DD/MM/YY') + 1;
                               end if;

                               crec.data_prorrogacao := w_data_util_3;
                            else
                               if p_rec_confir_vencido = 4
                               then
                                 if to_number(to_char(crec.data_prorrogacao, 'MM')) < to_number(to_char(sysdate, 'MM'))
                                 then
                                    crec.data_prorrogacao := to_date(sysdate, 'DD/MM/YY');
                                 end if;

                                 /* CASO O ULTIMO DIA SEJA O DIA CORRENTE, VAI PARA O ULTIMO DIA DO MES SUBSEQUENTE */
                                 if Last_day(to_date(crec.data_prorrogacao, 'DD/MM/YY')) = to_date(sysdate, 'DD/MM/YY')
                                 then
                                    crec.data_prorrogacao := ADD_MONTHS(crec.data_prorrogacao, 1);
                                 end if;

                                  BEGIN
                                     select data_calendario into w_data_util_3 from basi_260
                                     where basi_260.data_calendario <= Last_day(to_date(crec.data_prorrogacao, 'DD/MM/YY'))
                                       and basi_260.dia_util_finan   = 0
                                       and rownum                    = 1
                                     order by basi_260.data_calendario desc;
                                  EXCEPTION
                                     when no_data_found then
                                        w_data_util_3 := Last_day(to_date(sysdate, 'DD/MM/YY'));
                                  END;

                                  crec.data_prorrogacao := w_data_util_3;
                               end if;
                            end if;
                         end if;
                      end if;
                   end if;
                else
                   -- TIPO DE TITULOS DE PREVISTOS
                   w_tipo_registro_flxc := 'previs';

                   if crec.saldo_duplicata > 0.00 and crec.data_prorrogacao < to_date(sysdate, 'DD/MM/YY')
                   then
                      /*
                        1- Descartar Titulo;
                        2- Trazer a data de vencimento para a data atual;
                        3- Trazer a data de vencimento para o primeiro dia util apos a data corrente;
                        4- Trazer a data de vencimento para o ultimo dia do mes de vencimento;
                      */
                      if p_rec_previs_vencido = 1
                      then
                         crec.saldo_duplicata := 0.00;
                      else
                         if p_rec_previs_vencido = 2
                         then
                            crec.data_prorrogacao := to_date(sysdate, 'DD/MM/YY');
                         else
                            if p_rec_previs_vencido = 3
                            then
                               /* PROCURA DIA UTIL FINAL */
                               BEGIN
                                  select basi_260.dia_util_finan into w_he_dia_util_3 from basi_260
                                  where basi_260.data_calendario = to_date(sysdate, 'DD/MM/YY') + 1;
                               EXCEPTION
                                  when no_data_found then
                                     w_he_dia_util_3 := null;
                               END;

                               if w_he_dia_util_3 is not null
                               then
                                  w_he_dia_util_3 := 1;
                               end if;

                               if w_he_dia_util_3 = 1
                               then
                                  BEGIN
                                     select data_calendario into w_data_util_3 from basi_260
                                     where basi_260.data_calendario >= to_date(sysdate, 'DD/MM/YY') + 1
                                       and basi_260.dia_util_finan   = 0
                                       and rownum                    = 1
                                     order by basi_260.data_calendario asc;
                                  EXCEPTION
                                     when no_data_found then
                                        w_data_util_3 := null;
                                  END;
                               else
                                  w_data_util_3 := to_date(sysdate, 'DD/MM/YY') + 1;
                               end if;

                               crec.data_prorrogacao := w_data_util_3;
                            else
                               if p_rec_previs_vencido = 4
                               then
                                 if to_number(to_char(crec.data_prorrogacao, 'MM')) < to_number(to_char(sysdate, 'MM'))
                                 then
                                    crec.data_prorrogacao := to_date(sysdate, 'DD/MM/YY');
                                 end if;

                                 /* CASO O ULTIMO DIA SEJA O DIA CORRENTE, VAI PARA O ULTIMO DIA DO MES SUBSEQUENTE */
                                 if Last_day(to_date(crec.data_prorrogacao, 'DD/MM/YY')) = to_date(sysdate, 'DD/MM/YY')
                                 then
                                    crec.data_prorrogacao := ADD_MONTHS(crec.data_prorrogacao, 1);
                                 end if;

                                  BEGIN
                                     select data_calendario into w_data_util_3 from basi_260
                                     where basi_260.data_calendario <= Last_day(to_date(crec.data_prorrogacao, 'DD/MM/YY'))
                                       and basi_260.dia_util_finan   = 0
                                       and rownum                    = 1
                                     order by basi_260.data_calendario desc;
                                  EXCEPTION
                                     when no_data_found then
                                        w_data_util_3 := Last_day(to_date(sysdate, 'DD/MM/YY'));
                                  END;

                                  crec.data_prorrogacao := w_data_util_3;
                               end if;
                            end if;
                         end if;
                      end if;
                   end if;
                end if;
               --end if /*tipo de titulo de aplicacoes*/;



               -- CASO ESTIVER PARAMETRIZADO PARA LER OS DADOS DIA DO FLUXO NA TABELA DE CADASTRO
               if w_vencimento_aux     = to_date(sysdate, 'DD/MM/YY') and
                  p_origem_dados_lh_tp = 2                            and
                  w_ja_inseriu         = 0
               then
                  w_ja_inseriu := 1;

                  FOR dados_lh_tp in (
                      select * from cpag_825
                      where cpag_825.data_fluxo     = p_data_lida
                        and cpag_825.valor          > 0.00
                        and cpag_825.tipo           = 1
                        and ((p_incl_exce = 1
                          and cpag_825.codigo_empresa in (p_cod_emp1,p_cod_emp2,p_cod_emp3,p_cod_emp4,p_cod_emp5))
                            or  (p_incl_exce = 2
                          and cpag_825.codigo_empresa not in (p_cod_emp1,p_cod_emp2,p_cod_emp3,p_cod_emp4,p_cod_emp5)))
                      )
                  LOOP


                     w_tipo_registro_flxc := 'duplic';
                     w_letra              := 'B';

                     BEGIN
                     select pedi_085.descr_tipo_clien into w_descr_conta_fluxo
                     from pedi_085
                     where pedi_085.tipo_cliente = dados_lh_tp.conta_fluxo;
                     EXCEPTION
                        when no_data_found then
                           w_descr_conta_fluxo := ' ';
                     END;


                     BEGIN
                     select
                     nvl(max(oper_flx.sequencia),0) + 1 into w_sequencia from oper_flx
                     where oper_flx.int_01           = p_cod_usuario
                       and oper_flx.int_12           = 1
                       and oper_flx.nr_solicitacao   = p_cod_usuario
                       and oper_flx.nome_relatorio   = p_programa
                       and oper_flx.str_61           = p_nom_usuario;
                     END;

                     BEGIN
                        INSERT /*+APPEND*/ INTO oper_flx
                            (dat_01,                        int_02,
                             int_03,                        int_04,
                             int_05,                        str_01,

                             int_06,                        str_92,
                             int_07,                        int_08,
                             int_09,                        str_60,

                             int_10,                        str_06,
                             int_11,                        str_07,
                             flo_01,                        flo_02,

                             flo_03,                        str_08,
                             str_62,                        int_13,
                             int_01,                        int_12,

                             int_14,                        int_15,
                             int_16,                        int_17,
                             int_18,

                             str_63,                        dat_02,
                             dat_03,                        dat_04,

                             nr_solicitacao,                nome_relatorio,
                             sequencia,                     data_criacao,
                             str_61)
                         VALUES
                            (p_data_lida,                   0,
                             0,                             dados_lh_tp.conta_fluxo,
                             0,                             0,

                             0,                             w_descr_conta_fluxo,
                             0,                             0,
                             0,                             ' ',

                             0,                             ' ',
                             0,                             ' ',
                             dados_lh_tp.valor,             dados_lh_tp.valor,

                             0.00,                          w_tipo_registro_flxc,
                             w_nome_empr,                   crec.codigo_empresa,
                             p_cod_usuario,                 1, /*destalhado*/

                             p_cod_emp1,                    p_cod_emp2,
                             p_cod_emp3,                    p_cod_emp4,
                             p_cod_emp5,

                             w_letra,                       p_data_fim_consulta,
                             p_data_ini_consulta,           sysdate,

                             p_cod_usuario,                p_programa,
                             w_sequencia,                   sysdate,
                             p_nom_usuario);

                     EXCEPTION
                     WHEN OTHERS THEN
                        p_des_erro := 'Erro na atualizacao da tabela oper_flx - CONTA A RECEBER ' || Chr(10) || SQLERRM;
                        RAISE W_ERRO;
                     END;

                     COMMIT;
                 END LOOP;

              else

            if w_vencimento_aux > to_date(sysdate, 'dd/mm/yy') or
               (w_vencimento_aux = to_date(sysdate, 'dd/mm/yy') and p_origem_dados_lh_tp = 1)
               then
                  BEGIN
                     select basi_270.valor_moeda into w_indice_moeda_informado from basi_270
                     where basi_270.codigo_moeda = crec.moeda_titulo
                       and basi_270.data_moeda   <= p_data_lida
                       and rownum                 = 1
                     order by basi_270.data_moeda desc;
                  EXCEPTION
                    when no_data_found then
                         w_indice_moeda_informado := 1.00;
                  END;

                  if w_indice_moeda_informado = 0.00
                  then
                     w_indice_moeda_informado := 1.00;
                  end if;

                  FOR baixa in (
                       select
                          sum(decode(cont_010.sinal_titulo,2,(fatu_075.valor_pago * (-1)),fatu_075.valor_pago))-
                          sum(decode(cont_010.sinal_titulo,2,(fatu_075.valor_juros  * (-1)),fatu_075.valor_juros)) +
                          sum(decode(cont_010.sinal_titulo,2,(fatu_075.valor_descontos  * (-1)),fatu_075.valor_descontos)) -
                          sum(decode(cont_010.sinal_titulo,2,(fatu_075.vlr_var_cambial * (-1)), fatu_075.vlr_var_cambial)) valor_pago
                       from fatu_075,
                            cont_010
                       where fatu_075.historico_pgto            = cont_010.codigo_historico
                         and cont_010.fluxo                     = 2
                         and fatu_075.nr_titul_cli_dup_cgc_cli9 = crec.cli_dup_cgc_cli9
                         and fatu_075.nr_titul_cli_dup_cgc_cli4 = crec.cli_dup_cgc_cli4
                         and fatu_075.nr_titul_cli_dup_cgc_cli2 = crec.cli_dup_cgc_cli2
                         and fatu_075.nr_titul_cod_tit          = crec.tipo_titulo
                         and fatu_075.nr_titul_num_dup          = crec.num_duplicata
                         and fatu_075.nr_titul_seq_dup          = crec.seq_duplicatas
                         and fatu_075.nr_titul_codempr          = crec.codigo_empresa
                        -- and fatu_075.data_pagamento            = p_data_lida
                         and crec.saldo_duplicata               > 0
                       having
                          (sum(decode(cont_010.sinal_titulo,2,(fatu_075.valor_pago * (-1)),fatu_075.valor_pago))-
                          sum(decode(cont_010.sinal_titulo,2,(fatu_075.valor_juros  * (-1)),fatu_075.valor_juros)) +
                          sum(decode(cont_010.sinal_titulo,2,(fatu_075.valor_descontos  * (-1)),fatu_075.valor_descontos)) -
                          sum(decode(cont_010.sinal_titulo,2,(fatu_075.vlr_var_cambial * (-1)), fatu_075.vlr_var_cambial))) > 0.00
                     )
                  LOOP

                    if baixa.valor_pago > 0.00
                    then
                       crec.saldo_duplicata  := crec.saldo_duplicata + round(baixa.valor_pago * w_indice_moeda_informado,2);
                    end if;
                  END LOOP;

                  if crec.saldo_duplicata > 0.00
                  then
                        BEGIN
                         select
                          nvl(max(oper_flx.sequencia),0) + 1 into w_sequencia from oper_flx
                         where oper_flx.int_01           = p_cod_usuario
                           and oper_flx.int_12           = 1
                           and oper_flx.nr_solicitacao   = p_cod_usuario
                           and oper_flx.nome_relatorio   = p_programa
                           and oper_flx.str_61           = p_nom_usuario;
                         END;

                         BEGIN
                           INSERT /*+APPEND*/ INTO oper_flx
                              (dat_01,                        int_02,
                               int_03,                        int_04,
                               int_05,                        str_01,

                               int_06,                        str_92,
                               int_07,                        int_08,
                               int_09,                        str_60,

                               int_10,                        str_06,
                               int_11,                        str_07,
                               flo_01,                        flo_02,

                               flo_03,                        str_08,
                               str_62,                        int_13,
                               int_01,                        int_12,

                               int_14,                        int_15,
                               int_16,                        int_17,
                               int_18,

                               str_63,                        dat_02,
                               dat_03,                        dat_04,

                               nr_solicitacao,                nome_relatorio,
                               sequencia,                     data_criacao,
                               str_61)
                             VALUES
                              (crec.data_prorrogacao,         crec.previsao,
                               0,                             w_tp_cliente,
                               crec.num_duplicata,            crec.seq_duplicatas,

                               crec.tipo_titulo,              w_descr_tp_clie,
                               crec.cli_dup_cgc_cli9,         crec.cli_dup_cgc_cli4,
                               crec.cli_dup_cgc_cli2,         w_nome_cliente,

                               crec.portador_duplic,          w_nome_portador,
                               crec.moeda_titulo,             w_nome_moeda,
                               crec.valor_duplicata,          crec.saldo_duplicata,

                               0.00,                          w_tipo_registro_flxc,
                               w_nome_empr,                   crec.codigo_empresa,
                               p_cod_usuario,                 1, /*destalhado*/

                               p_cod_emp1,                    p_cod_emp2,
                               p_cod_emp3,                    p_cod_emp4,
                               p_cod_emp5,

                               w_letra,                       p_data_fim_consulta,
                               p_data_ini_consulta,           w_vencimento_aux,

                               p_cod_usuario,                p_programa,
                               w_sequencia,                   sysdate,
                               p_nom_usuario);

                         EXCEPTION
                         WHEN OTHERS THEN
                            p_des_erro := 'erro na atualizacao da tabela oper_flx - conta a receber ' || chr(10) || sqlerrm;
                            RAISE w_erro;
                         END;

                         COMMIT;
                      end if;
                end if;
              end if;


             -- DATA LIDA MENOR QUE A LINHA DO TEMPO (DATA EM QUE FOI GERADO O FLUXO DE CAIXA)
             if w_vencimento_aux < to_date(sysdate, 'DD/MM/YY')
             then

              BEGIN
                 select
                 basi_270.valor_moeda into w_indice_moeda_informado from basi_270
                 where basi_270.codigo_moeda = crec.moeda_titulo
                   and basi_270.data_moeda   <= p_data_lida
                   and rownum                 = 1
                 order by basi_270.data_moeda desc;
              EXCEPTION
              when no_data_found then
                   w_indice_moeda_informado := 1.00;
              END;

              if w_indice_moeda_informado = 0.00
              then
                 w_indice_moeda_informado := 1.00;
              end if;

              if crec.num_duplicata = 180819
              then
                w_indice_moeda_informado := w_indice_moeda_informado;
              end if;

              FOR baixa in (
                  select
                         sum(decode(cont_010.sinal_titulo,2,(fatu_075.valor_pago * (-1)),fatu_075.valor_pago))-
                         sum(decode(cont_010.sinal_titulo,2,(fatu_075.valor_juros  * (-1)),fatu_075.valor_juros)) +
                         sum(decode(cont_010.sinal_titulo,2,(fatu_075.valor_descontos  * (-1)),fatu_075.valor_descontos)) -
                         sum(decode(cont_010.sinal_titulo,2,(fatu_075.vlr_var_cambial * (-1)), fatu_075.vlr_var_cambial)) valor_pago
                  from fatu_075,
                       cont_010
                  where fatu_075.historico_pgto            = cont_010.codigo_historico
                    and cont_010.fluxo                     = 2
                    and fatu_075.nr_titul_cli_dup_cgc_cli9 = crec.cli_dup_cgc_cli9
                    and fatu_075.nr_titul_cli_dup_cgc_cli4 = crec.cli_dup_cgc_cli4
                    and fatu_075.nr_titul_cli_dup_cgc_cli2 = crec.cli_dup_cgc_cli2
                    and fatu_075.nr_titul_cod_tit          = crec.tipo_titulo
                    and fatu_075.nr_titul_num_dup          = crec.num_duplicata
                    and fatu_075.nr_titul_seq_dup          = crec.seq_duplicatas
                    and fatu_075.nr_titul_codempr          = crec.codigo_empresa
                    --and fatu_075.data_pagamento            = p_data_lida
                    and crec.saldo_duplicata               > 0
                  having
                   (sum(decode(cont_010.sinal_titulo,2,(fatu_075.valor_pago * (-1)),fatu_075.valor_pago))-
                   sum(decode(cont_010.sinal_titulo,2,(fatu_075.valor_juros  * (-1)),fatu_075.valor_juros)) +
                   sum(decode(cont_010.sinal_titulo,2,(fatu_075.valor_descontos  * (-1)),fatu_075.valor_descontos)) -
                    sum(decode(cont_010.sinal_titulo,2,(fatu_075.vlr_var_cambial * (-1)), fatu_075.vlr_var_cambial))) > 0.00
                )
              LOOP
                  if baixa.valor_pago > 0.00
                  then
                     crec.saldo_duplicata  := crec.saldo_duplicata + round(baixa.valor_pago * w_indice_moeda_informado,2);
                  end if;
              END LOOP;

                if crec.saldo_duplicata > 0.00
                then
                    BEGIN
                    select
                     nvl(max(oper_flx.sequencia),0) + 1 into w_sequencia from oper_flx
                    where oper_flx.int_01           = p_cod_usuario
                      and oper_flx.int_12           = 1
                      and oper_flx.nr_solicitacao   = p_cod_usuario
                      and oper_flx.nome_relatorio   = p_programa
                      and oper_flx.str_61           = p_nom_usuario;
                    END;

                    BEGIN
                       INSERT /*+APPEND*/ INTO oper_flx
                           (dat_01,                        int_02,
                            int_03,                        int_04,
                            int_05,                        str_01,

                            int_06,                        str_92,
                            int_07,                        int_08,
                            int_09,                        str_60,

                            int_10,                        str_06,
                            int_11,                        str_07,
                            flo_01,                        flo_02,

                            flo_03,                        str_08,
                            str_62,                        int_13,
                            int_01,                        int_12,

                            int_14,                        int_15,
                            int_16,                        int_17,
                            int_18,

                            str_63,                        dat_02,
                            dat_03,                        dat_04,

                            nr_solicitacao,                nome_relatorio,
                            sequencia,                     data_criacao,
                            str_61)
                         VALUES
                           (crec.data_prorrogacao,         crec.previsao,
                            0,                             w_tp_cliente,
                            crec.num_duplicata,            crec.seq_duplicatas,

                            crec.tipo_titulo,              w_descr_tp_clie,
                            crec.cli_dup_cgc_cli9,         crec.cli_dup_cgc_cli4,
                            crec.cli_dup_cgc_cli2,         w_nome_cliente,

                            crec.portador_duplic,          w_nome_portador,
                            crec.moeda_titulo,             w_nome_moeda,
                            crec.valor_duplicata,          crec.saldo_duplicata,

                            0.00,                          w_tipo_registro_flxc,
                            w_nome_empr,                   crec.codigo_empresa,
                            p_cod_usuario,                 1, /*destalhado*/

                            p_cod_emp1,                    p_cod_emp2,
                            p_cod_emp3,                    p_cod_emp4,
                            p_cod_emp5,

                            w_letra,                       p_data_fim_consulta,
                            p_data_ini_consulta,           w_vencimento_aux,

                            p_cod_usuario,                p_programa,
                            w_sequencia,                  sysdate,
                            p_nom_usuario);

                    EXCEPTION
                    WHEN OTHERS THEN
                       p_des_erro := 'Erro na atualizacao da tabela oper_flx - CONTA A RECEBER ' || Chr(10) || SQLERRM;
                       RAISE W_ERRO;
                    END;

                    COMMIT;
                end if;
              end if;
            end if; /* FIM if w_fluxo = 1 */
         end if; /*historico NC*/
      end if; /*registro ok*/
   END LOOP;  /* fim.do.executing_begin.do.FATU_070 */

   if w_achou_registros is null and p_origem_dados_lh_tp = 2 /*and p_data_lida  = to_date(sysdate, 'DD/MM/YY')*/ -- Caso nao ache nenhum registro, procura na tabela para o dia atual
   then

      FOR dados_lh_tp in (
          select
          * from cpag_825
          where cpag_825.data_fluxo     = to_date(sysdate, 'DD/MM/YY')
            and cpag_825.valor          > 0.00
            and cpag_825.tipo           = 1
            and p_data_lida             = to_date(sysdate, 'DD/MM/YY')
            and ((p_incl_exce = 1
                and cpag_825.codigo_empresa in (p_cod_emp1,p_cod_emp2,p_cod_emp3,p_cod_emp4,p_cod_emp5))
           or  (p_incl_exce = 2
                and cpag_825.codigo_empresa not in (p_cod_emp1,p_cod_emp2,p_cod_emp3,p_cod_emp4,p_cod_emp5)))

       )
      LOOP

         w_tipo_registro_flxc := 'duplic';
         w_letra              := 'B';

         BEGIN
         select
         pedi_085.descr_tipo_clien into w_descr_conta_fluxo
         from pedi_085
         where pedi_085.tipo_cliente = dados_lh_tp.conta_fluxo;
         EXCEPTION
            when no_data_found then
               w_descr_conta_fluxo := ' ';
         END;

         BEGIN
         select nvl(max(oper_flx.sequencia),0) + 1 into w_sequencia from oper_flx
         where oper_flx.int_01           = p_cod_usuario
           and oper_flx.int_12           = 1
           and oper_flx.nr_solicitacao   = p_cod_usuario
           and oper_flx.nome_relatorio   = p_programa
           and oper_flx.str_61           = p_nom_usuario;
         END;

         BEGIN
            INSERT /*+APPEND*/ INTO oper_flx
                (dat_01,                        int_02,
                 int_03,                        int_04,
                 int_05,                        str_01,

                 int_06,                        str_92,
                 int_07,                        int_08,
                 int_09,                        str_60,

                 int_10,                        str_06,
                 int_11,                        str_07,
                 flo_01,                        flo_02,

                 flo_03,                        str_08,
                 str_62,                        int_13,
                 int_01,                        int_12,

                 int_14,                        int_15,
                 int_16,                        int_17,
                 int_18,

                 str_63,                        dat_02,
                 dat_03,                        dat_04,

                 nr_solicitacao,                nome_relatorio,
                 sequencia,                     data_criacao,
                 str_61)
             VALUES
                (p_data_lida,                   0,
                 0,                             dados_lh_tp.conta_fluxo,
                 0,                             0,

                 0,                             w_descr_conta_fluxo,
                 0,                             0,
                 0,                             ' ',

                 0,                             ' ',
                 0,                             ' ',
                 dados_lh_tp.valor,             dados_lh_tp.valor,

                 0.00,                          w_tipo_registro_flxc,
                 w_nome_empr,                   dados_lh_tp.codigo_empresa,
                 p_cod_usuario,                 1, /*destalhado*/

                 p_cod_emp1,                    p_cod_emp2,
                 p_cod_emp3,                    p_cod_emp4,
                 p_cod_emp5,

                 w_letra,                       p_data_fim_consulta,
                 p_data_ini_consulta,           sysdate,

                 p_cod_usuario,                p_programa,
                 w_sequencia,                  sysdate,
                 p_nom_usuario);

         EXCEPTION
         WHEN OTHERS THEN
            p_des_erro := 'Erro na atualizacao da tabela oper_flx - CONTA A RECEBER ' || Chr(10) || SQLERRM;
            RAISE W_ERRO;
         END;
         COMMIT;
     END LOOP;
  end if;

  /* REGRA PARA APLICACOES FINANCEIRAS */

   if p_data_lida <= to_date(sysdate, 'DD/MM/YY')
   then
     FOR dados_lh_tp in (
            select
             * from cpag_825
            where cpag_825.data_fluxo     = p_data_lida
              and cpag_825.valor          > 0.00
              and cpag_825.tipo           = 3
              and ((p_incl_exce = 1
                  and cpag_825.codigo_empresa in (p_cod_emp1,p_cod_emp2,p_cod_emp3,p_cod_emp4,p_cod_emp5))
             or  (p_incl_exce = 2
                  and cpag_825.codigo_empresa not in (p_cod_emp1,p_cod_emp2,p_cod_emp3,p_cod_emp4,p_cod_emp5)))

             order by cpag_825.data_fluxo asc

       )
       LOOP
          BEGIN
            w_descr_conta_fluxo := 'Aplicacoes Financeiras';
            w_tipo_registro_flxc := 'aplic';
            w_letra              := 'E';


             BEGIN
             select
             nvl(max(oper_flx.sequencia),0) + 1 into w_sequencia from oper_flx
             where oper_flx.int_01           = p_cod_usuario
               and oper_flx.int_12           = 1
               and oper_flx.nr_solicitacao   = p_cod_usuario
               and oper_flx.nome_relatorio   = p_programa
               and oper_flx.str_61           = p_nom_usuario;
             END;

             BEGIN
                INSERT /*+APPEND*/ INTO oper_flx
                    (dat_01,                        int_02,
                     int_03,                        int_04,
                     int_05,                        str_01,

                     int_06,                        str_92,
                     int_07,                        int_08,
                     int_09,                        str_60,

                     int_10,                        str_06,
                     int_11,                        str_07,
                     flo_01,                        flo_02,

                     flo_03,                        str_08,
                     str_62,                        int_13,
                     int_01,                        int_12,

                     int_14,                        int_15,
                     int_16,                        int_17,
                     int_18,

                     str_63,                        dat_02,
                     dat_03,

                     nr_solicitacao,                nome_relatorio,
                     sequencia,                     data_criacao,
                     str_61)
                 VALUES
                    (dados_lh_tp.data_fluxo,        0,
                     0,                             dados_lh_tp.conta_fluxo,
                     0,                             0,

                     0,                             w_descr_conta_fluxo,
                     0,                             0,
                     0,                             ' ',

                     0,                             ' ',
                     0,                             ' ',
                     dados_lh_tp.valor,             dados_lh_tp.valor,

                     0.00,                          w_tipo_registro_flxc,
                     w_nome_empr,                   dados_lh_tp.codigo_empresa,
                     p_cod_usuario,                 1, /*destalhado*/

                     p_cod_emp1,                    p_cod_emp2,
                     p_cod_emp3,                    p_cod_emp4,
                     p_cod_emp5,

                     w_letra,                       p_data_fim_consulta,
                     p_data_ini_consulta,

                     p_cod_usuario,                p_programa,
                     w_sequencia,                  sysdate,
                     p_nom_usuario);

             EXCEPTION
             WHEN OTHERS THEN
                p_des_erro := 'Erro na atualizacao da tabela oper_flx - CONTA A RECEBER ' || Chr(10) || SQLERRM;
                RAISE W_ERRO;
             END;
             COMMIT;
         END;
       END LOOP;
    else
       BEGIN
          select
          oper_flx.flo_02 into w_valor_aux
          from oper_flx
          where oper_flx.nr_solicitacao = p_cod_usuario
            and oper_flx.nome_relatorio = p_programa
            and oper_flx.int_01         = p_cod_usuario
            and oper_flx.str_61         = p_nom_usuario
            and oper_flx.str_63         = 'E'
            and oper_flx.dat_01         = p_data_lida - 1;
       EXCEPTION
          WHEN no_data_found then
            w_valor_aux := 0.00;
       END;
       BEGIN
         select
         basi_270.valor_moeda into w_indice_moeda_informado
         from basi_270
         where basi_270.codigo_moeda = p_moeda_titulo
           and basi_270.data_moeda   = p_data_lida
           and rownum                = 1
         order by basi_270.data_moeda desc;
        EXCEPTION
          when no_data_found then
             w_indice_moeda_informado := 0.00;
        END;

        w_valor_aplic := w_valor_aux + ((w_valor_aux * w_indice_moeda_informado) / 100);

        w_descr_conta_fluxo := 'Aplicacoes Financeiras';
        w_tipo_registro_flxc := 'aplic';
        w_letra              := 'E';

        BEGIN
         select
         nvl(max(oper_flx.sequencia),0) + 1 into w_sequencia from oper_flx
         where oper_flx.int_01           = p_cod_usuario
           and oper_flx.int_12           = 1
           and oper_flx.nr_solicitacao   = p_cod_usuario
           and oper_flx.nome_relatorio   = p_programa
           and oper_flx.str_61           = p_nom_usuario;
        END;

        BEGIN
          INSERT /*+APPEND*/ INTO oper_flx
              (dat_01,                        int_02,
               int_03,                        int_04,
               int_05,                        str_01,

               int_06,                        str_92,
               int_07,                        int_08,
               int_09,                        str_60,

               int_10,                        str_06,
               int_11,                        str_07,
               flo_01,                        flo_02,

               flo_03,                        str_08,
               str_62,                        int_13,
               int_01,                        int_12,

               int_14,                        int_15,
               int_16,                        int_17,
               int_18,

               str_63,                        dat_02,
               dat_03,

               nr_solicitacao,                nome_relatorio,
               sequencia,                     data_criacao,
               str_61)
           VALUES
              (p_data_lida,                   0,
               0,                             3,
               0,                             0,

               0,                             w_descr_conta_fluxo,
               0,                             0,
               0,                             ' ',

               0,                             ' ',
               0,                             ' ',
               w_valor_aplic,                 w_valor_aplic,

               0.00,                          w_tipo_registro_flxc,
               '',                            0,
               p_cod_usuario,                 1, /*destalhado*/

               p_cod_emp1,                    p_cod_emp2,
               p_cod_emp3,                    p_cod_emp4,
               p_cod_emp5,

               w_letra,                       p_data_fim_consulta,
               p_data_ini_consulta,

               p_cod_usuario,                p_programa,
               w_sequencia,                  sysdate,
               p_nom_usuario);

        EXCEPTION
        WHEN OTHERS THEN
          p_des_erro := 'Erro na atualizacao da tabela oper_flx - CONTA A RECEBER ' || Chr(10) || SQLERRM;
          RAISE W_ERRO;
        END;
        COMMIT;
    end if;
EXCEPTION
   WHEN W_ERRO then
      p_des_erro := 'Erro na procedure inter_pr_flx_crec ' || Chr(10) || p_des_erro;
   WHEN OTHERS THEN
      p_des_erro := 'Outros erros na procedure inter_pr_flx_crec ' || Chr(10) || SQLERRM;

END inter_pr_flx_crec;
 


/
