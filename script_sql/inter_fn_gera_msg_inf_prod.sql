CREATE OR REPLACE FUNCTION inter_fn_gera_msg_inf_prod (
    p_empresa IN NUMBER,
    p_num_nota IN NUMBER,
    p_serie IN VARCHAR2,
    p_nivel IN VARCHAR2,
    p_grupo IN VARCHAR2,
    p_subgrupo IN VARCHAR2,
    p_item IN VARCHAR2,
    p_marca IN NUMBER,
    p_colecao IN NUMBER,
    p_codigo IN NUMBER
) RETURN VARCHAR2
IS
    type v_tp_cursor is REF CURSOR;
    PRAGMA AUTONOMOUS_TRANSACTION;
    v_mensagem VARCHAR2(4000);
    v_script_sql VARCHAR2(4000);
    v_resultado_sql VARCHAR2(4000);
    v_cursor         v_tp_cursor;

BEGIN

    SELECT obrf_182.script_sql
    INTO v_script_sql
    FROM obrf_182
    WHERE obrf_182.codigo = p_codigo;

    v_script_sql := replace(v_script_sql, '#codigo_empresa#', p_empresa);
    v_script_sql := replace(v_script_sql, '#num_nota_fiscal#',p_num_nota);
    v_script_sql := replace(v_script_sql, '#serie_nota_fisc#', '''' || p_serie || '''');
    v_script_sql := replace(v_script_sql, '#nivel#', '''' || p_nivel || '''');
    v_script_sql := replace(v_script_sql, '#grupo#', '''' || p_grupo || '''');
    v_script_sql := replace(v_script_sql, '#subgrupo#', '''' || p_subgrupo || '''');
    v_script_sql := replace(v_script_sql, '#item#', '''' || p_item || '''');
    v_script_sql := replace(v_script_sql, '#marca#', p_marca);
    v_script_sql := replace(v_script_sql, '#colecao#', p_colecao);
    

    -- Executa o script SQL
    BEGIN
        v_mensagem := '';
        
        OPEN v_cursor FOR v_script_sql;

        LOOP
            FETCH v_cursor INTO v_resultado_sql;
            EXIT WHEN v_cursor%NOTFOUND;
            if v_mensagem is null
            then v_mensagem := TRIM(v_resultado_sql);
            else v_mensagem := v_mensagem || ' ' || TRIM(v_resultado_sql);
            end if;

        END LOOP;

        -- Fecha o cursor do SQL
        CLOSE v_cursor;

    EXCEPTION
        WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR(-20000, 'Erro na execucao do SQL: ' || SQLERRM || ' ' || v_script_sql);
    END;

    -- Retorna a mensagem gerada
    RETURN v_mensagem;
END inter_fn_gera_msg_inf_prod;

/
