create table ixml_010 (
       cnpj9_transp number(9),
       cnpj4_transp number(4),
       cnpj2_transp number(2),
       dia_vencto number(2),
       dia_mais number(2),
       mes_mais number(2)
);

alter table ixml_010
  add constraint PK_ixml_010 primary key (cnpj9_transp, dia_vencto);
  
  /
exec inter_pr_recompile;
