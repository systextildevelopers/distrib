insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('obrf_f059', 'Demonstrativo da apura��o do imposto pela apropria��o de cr�dito presumido (DAICP)', 0, 1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'obrf_f059', ' ', 0, 99, 'N', 'S', 'N', 'S');

update hdoc_036
set hdoc_036.descricao       = 'Demonstrativo da apura��o do imposto pela apropria��o de cr�dito presumido (DAICP)'
where hdoc_036.codigo_programa = 'obrf_f059'
  and hdoc_036.locale          = 'es_ES';

commit;
