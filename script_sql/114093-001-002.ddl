alter table supr_100 add  origem_item number(1);

alter table supr_100 modify origem_item default 0;

comment on column supr_100.origem_item is 'Indica a origem do pedido de compra: 0 - Systexil, 1 - Integrado';

alter table cpag_010 add observacao_libercaco varchar2(4000);
alter table cpag_010 modify observacao_libercaco default ' ';

comment on column cpag_010.observacao_libercaco is 'Observacao para liberacao de titulos';

/
exec inter_pr_recompile;
