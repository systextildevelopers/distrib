create or replace package body ST_PCK_CLIENTE is

FUNCTION get_dados_cliente(p_cnpj_9 number,  p_cnpj_4 number,  p_cnpj_2 number, p_msg_erro in out varchar2) return t_dados_pedi_010
AS 
    v_dados_pedi_010 t_dados_pedi_010;
BEGIN
    begin
        SELECT *  
        BULK COLLECT 
        INTO v_dados_pedi_010
        FROM pedi_010
        WHERE cgc_9 = p_cnpj_9
        AND cgc_4 = p_cnpj_4
        AND cgc_2 = p_cnpj_2;
    exception when others then
        p_msg_erro := 'N?o foi possivel buscar os dados do cliente!. p_cnpj_9: ' || p_cnpj_9 || ' p_cnpj_4: ' || p_cnpj_4 || ' p_cnpj_2: ' || p_cnpj_2;
        return null;
    end;

    return v_dados_pedi_010;

END;

FUNCTION get_dados_embalagem( p_pedido_venda number,  p_seq_item_pedido number,  p_rolo_estoque number, p_msg_erro in out varchar2) return t_dados_pcpt_020
AS 
    v_dados_pcpt_020 t_dados_pcpt_020;
BEGIN
    begin
        SELECT *  
        BULK COLLECT 
        INTO v_dados_pcpt_020
        FROM pcpt_020
        WHERE pcpt_020.pedido_venda = p_pedido_venda
        AND pcpt_020.seq_item_pedido = p_seq_item_pedido
        AND pcpt_020.rolo_estoque = p_rolo_estoque;
    exception when others then
        p_msg_erro := 'N?o foi possivel buscar os dados da embalagem!. p_pedido_venda: ' || p_pedido_venda || ' p_seq_item_pedido: ' || p_seq_item_pedido || ' p_rolo_estoque: ' || p_rolo_estoque;
        return null;
    end;

    return v_dados_pcpt_020;

END;



FUNCTION get_dados_embalagem_fatu_100( p_codigo_embalagem number,  p_msg_erro in out varchar2) return t_dados_fatu_100
AS 
    v_dados_fatu_100 t_dados_fatu_100;
BEGIN
    begin
        SELECT *  
        BULK COLLECT 
        INTO v_dados_fatu_100
        FROM fatu_100
        WHERE fatu_100.codigo_embalagem = p_codigo_embalagem;
    exception when others then
        p_msg_erro := 'N?o foi possivel buscar os dados da embalagem!. p_codigo_embalagem: ' || p_codigo_embalagem ;
        return null;
    end;

    return v_dados_fatu_100;

END;

FUNCTION get_dados_embalagem_pcpc_320( p_nr_volume number,  p_msg_erro in out varchar2) return t_dados_pcpc_320
AS 
    v_dados_pcpc_320 t_dados_pcpc_320;
BEGIN
    begin
        SELECT *  
        BULK COLLECT 
        INTO v_dados_pcpc_320
        FROM pcpc_320
        WHERE pcpc_320.numero_volume = p_nr_volume;
    exception when others then
        p_msg_erro := 'N?o foi possivel buscar os dados da embalagem!. p_nr_volume: ' || p_nr_volume ;
        return null;
    end;

    return v_dados_pcpc_320;

END;

FUNCTION get_dados_cliente_narwal(p_msg_erro in out varchar2) return t_dados_pedi_010
AS 
    v_dados_pedi_010 t_dados_pedi_010;
BEGIN
    begin
        SELECT *  
        BULK COLLECT 
        INTO v_dados_pedi_010
        FROM pedi_010
        WHERE IND_INTEG_CLIENTE_NARW = 1
        AND exists (select 1 from basi_160 where basi_160.estado = 'EX' AND basi_160.cod_cidade = pedi_010.cod_cidade);
    exception when others then
        p_msg_erro := 'Nao foi possivel buscar os dados dos clientes de exporta??o!.';
        return null;
    end;

    return v_dados_pedi_010;

END;



end ST_PCK_CLIENTE;
/
