CREATE OR REPLACE VIEW PCPT_020_025 ("CODIGO_ROLO", "PERIODO_PRODUCAO", "TURNO_PRODUCAO",
    "NUMERO_LOTE", "PANOACAB_NIVEL99", "PANOACAB_GRUPO", "PANOACAB_SUBGRUPO", "PANOACAB_ITEM",
    "CODIGO_DEPOSITO", "QTDE_QUILOS_ACAB", "PESO_BRUTO", "TARA", "MT_LINEARES_PROD",
    "PEDIDO_VENDA", "SEQ_ITEM_PEDIDO", "NR_VOLUME", "DATA_PROD_TECEL", "AREA_PRODUCAO", "ORDEM_PRODUCAO",
    "PRE_ROMANEIO", "GRUPO_MAQUINA", "SUB_MAQUINA", "NUMERO_MAQUINA", "LARGURA", "NR_ROLO_ORIGEM", "GRAMATURA",
    "TRANSACAO_ENT", "DATA_ENTRADA", "ROLO_ESTOQUE", "NOTA_FISCAL", "SERIE_FISCAL_SAI", "NUMERO_OB",
    "LOTE_ACOMP", "CERTIFICACAO_QUALIDADE", "ENDERECO_ROLO", "COD_NUANCE", "PONTUACAO_QUALIDADE", "EMENDA",
    "QTDE_ROLOS_REAL", "QUALIDADE_ROLO", "CODIGO_OPERADOR", "SEQUENCIA_CORTE", "NUMERO_LACRE",
    "ORDEM_TINGIMENTO_ROLO", "NUMERO_EMBARQUE", "SEQU_FISCAL_ENT", "PROCEDENCIA", "DS_PROCEDENCIA",
    "UNIDADE_MEDIDA", "CODIGO_BARRAS_ANTIGO", "SEQ_ROLO_PREV_ROMANEIO", "FORNECEDOR_CGC9", "FORNECEDOR_CGC4", "FORNECEDOR_CGC2",
    "NOTA_FISCAL_ENT", "SERI_FISCAL_ENT", "ORDEM_BENEFIC_MODULAR", "GRUPO_ATENDIMENTO", "SEQUENCIA_TINGIMENTO",
    "ROLADA", "SEQ_ROLADA", "ORDEM_ENGOMAGEM", "PONTOS_QUALIDADE", "PONTOS_QUALIDADE_M2", "PESO_LIQUIDO_REAL",
    "DATA_INICIO", "HORA_INICIO", "DATA_FIM", "HORA_FIM", "NUM_CARRETEL", "METROS_CUBICOS") AS
  SELECT
 pcpt_020.codigo_rolo            , pcpt_020.periodo_producao          ,
 pcpt_020.turno_producao         , pcpt_020.numero_lote               ,
 pcpt_020.panoacab_nivel99       , pcpt_020.panoacab_grupo            ,
 pcpt_020.panoacab_subgrupo      , pcpt_020.panoacab_item             ,
 pcpt_020.codigo_deposito        , pcpt_020.qtde_quilos_acab          ,
 pcpt_020.peso_bruto             , pcpt_020.tara                      ,
 pcpt_020.mt_lineares_prod       , pcpt_020.pedido_venda              ,
 pcpt_020.seq_item_pedido        , pcpt_020.nr_volume                 ,
 pcpt_020.data_prod_tecel        , pcpt_020.area_producao             ,
 pcpt_020.ordem_producao         , pcpt_020.pre_romaneio              ,
 pcpt_020.grupo_maquina          , pcpt_020.sub_maquina               ,
 pcpt_020.numero_maquina         , pcpt_020.largura                   ,
 pcpt_020.nr_rolo_origem         , pcpt_020.gramatura                 ,
 pcpt_020.transacao_ent          , pcpt_020.data_entrada              ,
 pcpt_020.rolo_estoque           , pcpt_020.nota_fiscal               ,
 pcpt_020.serie_fiscal_sai       , pcpt025.ordem_producao             ,
 pcpt_020.lote_acomp             , pcpt_020.certificacao_qualidade    ,
 pcpt_020.endereco_rolo          , pcpt_021.cod_nuance                ,
 pcpt_021.pontuacao_qualidade    , pcpt_021.emenda                    ,
 pcpt_020.qtde_rolos_real        , pcpt_020.qualidade_rolo            ,
 pcpt_020.codigo_operador        , pcpt_021.sequencia_corte           ,
 pcpt_021.numero_lacre           , DECODE(pcpt_020.area_producao,2,pcpb_010.ordem_tingimento,0),
 rcnb_060a.numero_embarque       , pcpt_020.sequ_fiscal_ent           ,
 pcpt_021.procedencia            , pcpf_019.descricao                 ,
 basi_030.unidade_medida         , oper183.codigo_barras_antigo       ,
 pcpt_021.seq_rolo_prev_romaneio , pcpt_020.fornecedor_cgc9           ,
 pcpt_020.fornecedor_cgc4        , pcpt_020.fornecedor_cgc2           ,
 pcpt_020.nota_fiscal_ent        , pcpt_020.seri_fiscal_ent           ,
 pcpt_010.ordem_benefic_modular  , pcpt_021.grupo_atendimento         ,
 pcpt_021.sequencia_tingimento   , pcpt_021.rolada                    ,
 pcpt_021.seq_rolada             , pcpt_021.ordem_engomagem           ,
 pcpt_020.pontos_qualidade       , pcpt_021.pontos_qualidade_m2       ,
 pcpt_020.peso_liquido_real,
 pcpt_020.data_inicio_prod as data_inicio,
 pcpt_020.hora_inicio as hora_inicio,
 pcpt_020.data_prod_tecel as data_fim,
 pcpt_020.hora_termino as hora_fim,
 pcpt_020.num_carretel,
 pcpt_020.metros_cubicos
 from (select pcpt_025.codigo_rolo,
              pcpt_025.ordem_producao
       from pcpt_025
       where pcpt_025.area_ordem <> 1)
    pcpt025, pcpt_020, pcpt_021, pcpb_010, pcpt_010, basi_030, pcpf_019,
    (select rcnb_060.grupo_estrutura          as codigo_rolo,
        nvl(rcnb_060.campo_str_01,' ')        as numero_embarque
     from rcnb_060
     where rcnb_060.tipo_registro = 517
       and rcnb_060.nr_solicitacao = 315
       and rcnb_060.grupo_estrutura > 0
     group by rcnb_060.grupo_estrutura,nvl(rcnb_060.campo_str_01,' ')) rcnb_060a,
    (select oper_183.codigo_barras_antigo, oper_183.numero_vol_systextil
       from oper_183
      where oper_183.nivel_systextil in ('2', '4')) oper183
where pcpt_020.codigo_rolo            = pcpt025.codigo_rolo          (+)
  and pcpt_020.codigo_rolo            = pcpt_021.codigo_rolo         (+)
  and pcpt_020.codigo_rolo            > 0
  and pcpt_020.ordem_producao         = pcpb_010.ordem_producao      (+)
  and pcpt_020.ordem_producao         = pcpt_010.ordem_tecelagem     (+)
  and pcpt_020.codigo_rolo            = rcnb_060a.codigo_rolo        (+)
  and pcpt_020.panoacab_nivel99       = basi_030.nivel_estrutura
  and pcpt_020.panoacab_grupo         = basi_030.referencia
  and pcpt_021.procedencia            = pcpf_019.codigo              (+)
  and pcpt_020.codigo_rolo            = oper183.numero_vol_systextil (+);
