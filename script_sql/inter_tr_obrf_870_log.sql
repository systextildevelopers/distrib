
  CREATE OR REPLACE TRIGGER "INTER_TR_OBRF_870_LOG" 
after insert or delete or update
on OBRF_870
for each row
declare
   ws_usuario_rede           varchar2(20) ;
   ws_maquina_rede           varchar2(40) ;
   ws_aplicativo             varchar2(20) ;
   ws_sid                    number(9) ;
   ws_empresa                number(3) ;
   ws_usuario_systextil      varchar2(250) ;
   ws_locale_usuario         varchar2(5) ;
   v_nome_programa           varchar2(20) ;


begin
-- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);


   v_nome_programa := inter_fn_nome_programa(ws_sid);  
 


 if inserting
 then
    begin

        insert into OBRF_870_log (
           TIPO_OCORR,   /*0*/
           DATA_OCORR,   /*1*/
           HORA_OCORR,   /*2*/
           USUARIO_REDE,   /*3*/
           MAQUINA_REDE,   /*4*/
           APLICACAO,   /*5*/
           USUARIO_SISTEMA,   /*6*/
           NOME_PROGRAMA,   /*7*/
           COD_MENSAGEM_OLD,   /*8*/
           COD_MENSAGEM_NEW,   /*9*/
           SEQ_MENSAGEM_OLD,   /*10*/
           SEQ_MENSAGEM_NEW,   /*11*/
           SERIE_MENSAGEM_OLD,   /*12*/
           SERIE_MENSAGEM_NEW,   /*13*/
           DESCR_MENSAGEM_OLD,   /*14*/
           DESCR_MENSAGEM_NEW,   /*15*/
           TIPO_OBSERVACAO_OLD,   /*16*/
           TIPO_OBSERVACAO_NEW    /*17*/
        ) values (
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           0,/*8*/
           :new.COD_MENSAGEM, /*9*/
           0,/*10*/
           :new.SEQ_MENSAGEM, /*11*/
           '',/*12*/
           :new.SERIE_MENSAGEM, /*13*/
           '',/*14*/
           :new.DESCR_MENSAGEM, /*15*/
           0,/*16*/
           :new.TIPO_OBSERVACAO /*17*/
         );
    end;
 end if;


 if updating
 then
    begin
        insert into OBRF_870_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           COD_MENSAGEM_OLD, /*8*/
           COD_MENSAGEM_NEW, /*9*/
           SEQ_MENSAGEM_OLD, /*10*/
           SEQ_MENSAGEM_NEW, /*11*/
           SERIE_MENSAGEM_OLD, /*12*/
           SERIE_MENSAGEM_NEW, /*13*/
           DESCR_MENSAGEM_OLD, /*14*/
           DESCR_MENSAGEM_NEW, /*15*/
           TIPO_OBSERVACAO_OLD, /*16*/
           TIPO_OBSERVACAO_NEW  /*17*/
        ) values (
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.COD_MENSAGEM,  /*8*/
           :new.COD_MENSAGEM, /*9*/
           :old.SEQ_MENSAGEM,  /*10*/
           :new.SEQ_MENSAGEM, /*11*/
           :old.SERIE_MENSAGEM,  /*12*/
           :new.SERIE_MENSAGEM, /*13*/
           :old.DESCR_MENSAGEM,  /*14*/
           :new.DESCR_MENSAGEM, /*15*/
           :old.TIPO_OBSERVACAO,  /*16*/
           :new.TIPO_OBSERVACAO  /*17*/
         );
    end;
 end if;


 if deleting
 then
    begin
        insert into OBRF_870_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           COD_MENSAGEM_OLD, /*8*/
           COD_MENSAGEM_NEW, /*9*/
           SEQ_MENSAGEM_OLD, /*10*/
           SEQ_MENSAGEM_NEW, /*11*/
           SERIE_MENSAGEM_OLD, /*12*/
           SERIE_MENSAGEM_NEW, /*13*/
           DESCR_MENSAGEM_OLD, /*14*/
           DESCR_MENSAGEM_NEW, /*15*/
           TIPO_OBSERVACAO_OLD, /*16*/
           TIPO_OBSERVACAO_NEW /*17*/
        ) values (
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede,/*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.COD_MENSAGEM, /*8*/
           0, /*9*/
           :old.SEQ_MENSAGEM, /*10*/
           0, /*11*/
           :old.SERIE_MENSAGEM, /*12*/
           '', /*13*/
           :old.DESCR_MENSAGEM, /*14*/
           '', /*15*/
           :old.TIPO_OBSERVACAO, /*16*/
           0 /*17*/
         );
    end;
 end if;
end inter_tr_OBRF_870_log;
-- ALTER TRIGGER "INTER_TR_OBRF_870_LOG" ENABLE
 

/

exec inter_pr_recompile;

