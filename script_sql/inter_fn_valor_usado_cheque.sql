
  CREATE OR REPLACE FUNCTION "INTER_FN_VALOR_USADO_CHEQUE" 
(empresa_tit    in fatu_070.codigo_empresa%type,
 cli9           in fatu_070.cli_dup_cgc_cli9%type,
 cli4           in fatu_070.cli_dup_cgc_cli4%type,
 cli2           in fatu_070.cli_dup_cgc_cli2%type,
 tipo_tit       in fatu_070.tipo_titulo%type,
 nr_tit         in fatu_070.num_duplicata%type,
 parc_tit       in fatu_070.seq_duplicatas%type)
return number
   is total_cheque    number;
begin
   select nvl(sum(crec_180.valor_usado),0)
   into total_cheque
   from  crec_180, fatu_070
   where fatu_070.codigo_empresa    = empresa_tit
     and fatu_070.cli_dup_cgc_cli9  = cli9
     and fatu_070.cli_dup_cgc_cli4  = cli4
     and fatu_070.cli_dup_cgc_cli2  = cli2
     and fatu_070.tipo_titulo       = tipo_tit
     and fatu_070.num_duplicata     = nr_tit
     and fatu_070.seq_duplicatas    = parc_tit
     and crec_180.empresa           = fatu_070.codigo_empresa
     and crec_180.cgc9_cli          = fatu_070.cli_dup_cgc_cli9
     and crec_180.cgc4_cli          = fatu_070.cli_dup_cgc_cli4
     and crec_180.cgc2_cli          = fatu_070.cli_dup_cgc_cli2
     and crec_180.tipo_titulo       = fatu_070.tipo_titulo
     and crec_180.numero_titulo     = fatu_070.num_duplicata
     and crec_180.parcela_titulo    = fatu_070.seq_duplicatas
     and crec_180.flag_reg          = 0;
return(total_cheque);
end inter_fn_valor_usado_cheque;

/* Versao: 1 */

 

/

exec inter_pr_recompile;

