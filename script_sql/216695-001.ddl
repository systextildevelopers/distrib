--Menu Filho
insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def, item_apex)
values
('cpag_fa02', 'Relatórios Contas a Pagar', 0, 1, 1);
insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir,
modificar, excluir, procurar)
values
('INTERSYS', 1, 'cpag_fa02', 'menu_ap10', 1, 0, 'S', 'S', 'S', 'S');
insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir,
modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'cpag_fa02', 'menu_ap10', 1, 0, 'S', 'S', 'S', 'S');
commit;

--Menu Filho
insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def, item_apex)
values
('crec_fa03', 'Relatórios de Contas a Receber', 0, 1, 1);
insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir,
modificar, excluir, procurar)
values
('INTERSYS', 1, 'crec_fa03', 'menu_ap22', 1, 0, 'S', 'S', 'S', 'S');
insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir,
modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'crec_fa03', 'menu_ap22', 1, 0, 'S', 'S', 'S', 'S');
commit;
