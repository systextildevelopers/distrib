
  CREATE OR REPLACE TRIGGER "INTER_TR_HDOC_060_LOG" 
after insert or delete or update
on hdoc_060
for each row
declare
   ws_usuario_rede           varchar2(20) ;
   ws_maquina_rede           varchar2(40) ;
   ws_aplicativo             varchar2(20) ;
   ws_sid                    number(9) ;
   ws_empresa                number(3) ;
   ws_usuario_systextil      varchar2(250) ;
   ws_locale_usuario         varchar2(5) ;
   v_nome_programa           varchar2(20) ;


begin
-- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   v_nome_programa:= inter_fn_nome_programa(ws_sid);   

 if inserting
 then
    begin

        insert into hdoc_060_log (
           TIPO_OCORR,   /*0*/
           DATA_OCORR,   /*1*/
           HORA_OCORR,   /*2*/
           USUARIO_REDE,   /*3*/
           MAQUINA_REDE,   /*4*/
           APLICACAO,   /*5*/
           USUARIO_SISTEMA,   /*6*/
           NOME_PROGRAMA,   /*7*/
           codigo_empresa_OLD,   /*8*/
           codigo_empresa_NEW,   /*9*/
           tipo_operacao_OLD,   /*10*/
           tipo_operacao_NEW,   /*11*/
           nome_usuario_OLD,   /*12*/
           nome_usuario_NEW,   /*13*/
           nivel_produto_OLD,   /*14*/
           nivel_produto_NEW,   /*15*/
           grupo_produto_OLD,   /*16*/
           grupo_produto_NEW,   /*17*/
           subgru_produto_OLD,   /*18*/
           subgru_produto_NEW,   /*19*/
           item_produto_OLD,   /*20*/
           item_produto_NEW,   /*21*/
           codigo_deposito_OLD,   /*22*/
           codigo_deposito_NEW,   /*23*/
           codigo_transacao_OLD,   /*24*/
           codigo_transacao_NEW,   /*25*/
           banco_OLD,   /*26*/
           banco_NEW,   /*27*/
           conta_OLD,   /*28*/
           conta_NEW,   /*29*/
           tipo_titulo_OLD,   /*30*/
           tipo_titulo_NEW,   /*31*/
           empresas_OLD,   /*32*/
           empresas_NEW,   /*33*/
           cnpj9_OLD,   /*34*/
           cnpj9_NEW,   /*35*/
           cnpj4_OLD,   /*36*/
           cnpj4_NEW,   /*37*/
           cnpj2_OLD,   /*38*/
           cnpj2_NEW,   /*39*/
           centro_custo_OLD,   /*40*/
           centro_custo_NEW,   /*41*/
           conta_estoque_OLD,   /*42*/
           conta_estoque_NEW,   /*43*/
           linha_produto_OLD,   /*44*/
           linha_produto_NEW,   /*45*/
           valor_inicial_OLD,   /*46*/
           valor_inicial_NEW,   /*47*/
           valor_final_OLD,   /*48*/
           valor_final_NEW,   /*49*/
           unidade_medida_OLD,   /*50*/
           unidade_medida_NEW,   /*51*/
           chave_numerica_OLD,   /*52*/
           chave_numerica_NEW   /*53*/
        ) values (
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           0,/*8*/
           :new.codigo_empresa, /*9*/
           0,/*10*/
           :new.tipo_operacao, /*11*/
           '',/*12*/
           :new.nome_usuario, /*13*/
           '',/*14*/
           :new.nivel_produto, /*15*/
           '',/*16*/
           :new.grupo_produto, /*17*/
           '',/*18*/
           :new.subgru_produto, /*19*/
           '',/*20*/
           :new.item_produto, /*21*/
           0,/*22*/
           :new.codigo_deposito, /*23*/
           0,/*24*/
           :new.codigo_transacao, /*25*/
           0,/*26*/
           :new.banco, /*27*/
           0,/*28*/
           :new.conta, /*29*/
           0,/*30*/
           :new.tipo_titulo, /*31*/
           0,/*32*/
           :new.empresas, /*33*/
           0,/*34*/
           :new.cnpj9, /*35*/
           0,/*36*/
           :new.cnpj4, /*37*/
           0,/*38*/
           :new.cnpj2, /*39*/
           0,/*40*/
           :new.centro_custo, /*41*/
           0,/*42*/
           :new.conta_estoque, /*43*/
           0,/*44*/
           :new.linha_produto, /*45*/
           0,/*46*/
           :new.valor_inicial, /*47*/
           0,/*48*/
           :new.valor_final, /*49*/
           '',/*50*/
           :new.unidade_medida, /*51*/
           0,/*52*/
           :new.chave_numerica /*53*/
         );
    end;
 end if;


 if updating
 then
    begin
        insert into hdoc_060_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           codigo_empresa_OLD, /*8*/
           codigo_empresa_NEW, /*9*/
           tipo_operacao_OLD, /*10*/
           tipo_operacao_NEW, /*11*/
           nome_usuario_OLD, /*12*/
           nome_usuario_NEW, /*13*/
           nivel_produto_OLD, /*14*/
           nivel_produto_NEW, /*15*/
           grupo_produto_OLD, /*16*/
           grupo_produto_NEW, /*17*/
           subgru_produto_OLD, /*18*/
           subgru_produto_NEW, /*19*/
           item_produto_OLD, /*20*/
           item_produto_NEW, /*21*/
           codigo_deposito_OLD, /*22*/
           codigo_deposito_NEW, /*23*/
           codigo_transacao_OLD, /*24*/
           codigo_transacao_NEW, /*25*/
           banco_OLD, /*26*/
           banco_NEW, /*27*/
           conta_OLD, /*28*/
           conta_NEW, /*29*/
           tipo_titulo_OLD, /*30*/
           tipo_titulo_NEW, /*31*/
           empresas_OLD, /*32*/
           empresas_NEW, /*33*/
           cnpj9_OLD, /*34*/
           cnpj9_NEW, /*35*/
           cnpj4_OLD, /*36*/
           cnpj4_NEW, /*37*/
           cnpj2_OLD, /*38*/
           cnpj2_NEW, /*39*/
           centro_custo_OLD, /*40*/
           centro_custo_NEW, /*41*/
           conta_estoque_OLD, /*42*/
           conta_estoque_NEW, /*43*/
           linha_produto_OLD, /*44*/
           linha_produto_NEW, /*45*/
           valor_inicial_OLD, /*46*/
           valor_inicial_NEW, /*47*/
           valor_final_OLD, /*48*/
           valor_final_NEW, /*49*/
           unidade_medida_OLD, /*50*/
           unidade_medida_NEW, /*51*/
           chave_numerica_OLD, /*52*/
           chave_numerica_NEW /*53*/
        ) values (
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.codigo_empresa,  /*8*/
           :new.codigo_empresa, /*9*/
           :old.tipo_operacao,  /*10*/
           :new.tipo_operacao, /*11*/
           :old.nome_usuario,  /*12*/
           :new.nome_usuario, /*13*/
           :old.nivel_produto,  /*14*/
           :new.nivel_produto, /*15*/
           :old.grupo_produto,  /*16*/
           :new.grupo_produto, /*17*/
           :old.subgru_produto,  /*18*/
           :new.subgru_produto, /*19*/
           :old.item_produto,  /*20*/
           :new.item_produto, /*21*/
           :old.codigo_deposito,  /*22*/
           :new.codigo_deposito, /*23*/
           :old.codigo_transacao,  /*24*/
           :new.codigo_transacao, /*25*/
           :old.banco,  /*26*/
           :new.banco, /*27*/
           :old.conta,  /*28*/
           :new.conta, /*29*/
           :old.tipo_titulo,  /*30*/
           :new.tipo_titulo, /*31*/
           :old.empresas,  /*32*/
           :new.empresas, /*33*/
           :old.cnpj9,  /*34*/
           :new.cnpj9, /*35*/
           :old.cnpj4,  /*36*/
           :new.cnpj4, /*37*/
           :old.cnpj2,  /*38*/
           :new.cnpj2, /*39*/
           :old.centro_custo,  /*40*/
           :new.centro_custo, /*41*/
           :old.conta_estoque,  /*42*/
           :new.conta_estoque, /*43*/
           :old.linha_produto,  /*44*/
           :new.linha_produto, /*45*/
           :old.valor_inicial,  /*46*/
           :new.valor_inicial, /*47*/
           :old.valor_final,  /*48*/
           :new.valor_final, /*49*/
           :old.unidade_medida,  /*50*/
           :new.unidade_medida, /*51*/
           :old.chave_numerica,  /*52*/
           :new.chave_numerica /*53*/
         );
    end;
 end if;


 if deleting
 then
    begin
        insert into hdoc_060_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           codigo_empresa_OLD, /*8*/
           codigo_empresa_NEW, /*9*/
           tipo_operacao_OLD, /*10*/
           tipo_operacao_NEW, /*11*/
           nome_usuario_OLD, /*12*/
           nome_usuario_NEW, /*13*/
           nivel_produto_OLD, /*14*/
           nivel_produto_NEW, /*15*/
           grupo_produto_OLD, /*16*/
           grupo_produto_NEW, /*17*/
           subgru_produto_OLD, /*18*/
           subgru_produto_NEW, /*19*/
           item_produto_OLD, /*20*/
           item_produto_NEW, /*21*/
           codigo_deposito_OLD, /*22*/
           codigo_deposito_NEW, /*23*/
           codigo_transacao_OLD, /*24*/
           codigo_transacao_NEW, /*25*/
           banco_OLD, /*26*/
           banco_NEW, /*27*/
           conta_OLD, /*28*/
           conta_NEW, /*29*/
           tipo_titulo_OLD, /*30*/
           tipo_titulo_NEW, /*31*/
           empresas_OLD, /*32*/
           empresas_NEW, /*33*/
           cnpj9_OLD, /*34*/
           cnpj9_NEW, /*35*/
           cnpj4_OLD, /*36*/
           cnpj4_NEW, /*37*/
           cnpj2_OLD, /*38*/
           cnpj2_NEW, /*39*/
           centro_custo_OLD, /*40*/
           centro_custo_NEW, /*41*/
           conta_estoque_OLD, /*42*/
           conta_estoque_NEW, /*43*/
           linha_produto_OLD, /*44*/
           linha_produto_NEW, /*45*/
           valor_inicial_OLD, /*46*/
           valor_inicial_NEW, /*47*/
           valor_final_OLD, /*48*/
           valor_final_NEW, /*49*/
           unidade_medida_OLD, /*50*/
           unidade_medida_NEW, /*51*/
           chave_numerica_OLD, /*52*/
           chave_numerica_NEW /*53*/
        ) values (
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede,/*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.codigo_empresa, /*8*/
           0, /*9*/
           :old.tipo_operacao, /*10*/
           0, /*11*/
           :old.nome_usuario, /*12*/
           '', /*13*/
           :old.nivel_produto, /*14*/
           '', /*15*/
           :old.grupo_produto, /*16*/
           '', /*17*/
           :old.subgru_produto, /*18*/
           '', /*19*/
           :old.item_produto, /*20*/
           '', /*21*/
           :old.codigo_deposito, /*22*/
           0, /*23*/
           :old.codigo_transacao, /*24*/
           0, /*25*/
           :old.banco, /*26*/
           0, /*27*/
           :old.conta, /*28*/
           0, /*29*/
           :old.tipo_titulo, /*30*/
           0, /*31*/
           :old.empresas, /*32*/
           0, /*33*/
           :old.cnpj9, /*34*/
           0, /*35*/
           :old.cnpj4, /*36*/
           0, /*37*/
           :old.cnpj2, /*38*/
           0, /*39*/
           :old.centro_custo, /*40*/
           0, /*41*/
           :old.conta_estoque, /*42*/
           0, /*43*/
           :old.linha_produto, /*44*/
           0, /*45*/
           :old.valor_inicial, /*46*/
           0, /*47*/
           :old.valor_final, /*48*/
           0, /*49*/
           :old.unidade_medida, /*50*/
           '', /*51*/
           :old.chave_numerica, /*52*/
           0 /*53*/
         );
    end;
 end if;
end inter_tr_hdoc_060_log;

-- ALTER TRIGGER "INTER_TR_HDOC_060_LOG" ENABLE
 

/

exec inter_pr_recompile;

