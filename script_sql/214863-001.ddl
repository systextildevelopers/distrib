update hdoc_035
   set hdoc_035.descricao = 'Baixa da Guia de Produção -1'
where codigo_programa = 'pcpt_f080';

update hdoc_035
   set hdoc_035.descricao = 'Baixa da Guia de Produção'
where codigo_programa = 'pcpt_f085';


update hdoc_036
   set hdoc_036.descricao       = 'Cancelación de la guía de producción -1'
 where hdoc_036.codigo_programa = 'pcpt_f080'
   and hdoc_036.locale          = 'es_ES';

update hdoc_036
   set hdoc_036.descricao       = 'Cancelación de la guía de producción'
 where hdoc_036.codigo_programa = 'pcpt_f085'
   and hdoc_036.locale          = 'es_ES';
