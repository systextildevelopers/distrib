create table cont_k115 (
	COD_CONTROLADORA    	NUMBER(9) DEFAULT 0,
	EXERCICIO           	NUMBER(9) DEFAULT 0,
	EMP_COD             	NUMBER(9) DEFAULT 0,
	EVENTO 					NUMBER(1) CHECK(EVENTO IN (1,2,3,4,5,6,7,8)) NOT NULL,
	EMP_COD_PART           	NUMBER(9) DEFAULT 0,
	COND_PART              	NUMBER(1) CHECK(COND_PART IN (1,2,3)),
	PER_EVT                	NUMBER(8,4) DEFAULT 0
);

alter table cont_k115 
       add constraint pk_cont_k115 primary key (COD_CONTROLADORA,EXERCICIO,EMP_COD,EVENTO,EMP_COD_PART,COND_PART);
       
alter table cont_k115 
      add constraint fk_cont_k115_k110 FOREIGN KEY(COD_CONTROLADORA,EXERCICIO,EMP_COD,EVENTO) REFERENCES cont_k110 (COD_CONTROLADORA,EXERCICIO,EMP_COD,EVENTO);
      
comment on table cont_k115              		is 'EMPRESAS PARTICIPANTES DO EVENTO SOCIETÁRIO';
comment on column cont_k115.COD_CONTROLADORA 	is 'CÓDIGO DA EMPRESA CONTROLADORA';
comment on column cont_k115.EXERCICIO           is 'Exercicio a que se refere as informações da empresa no período';
comment on column cont_k115.EMP_COD             is 'Código do país da empresa, conforme tabela do Banco Central do Brasil.';	
comment on column cont_k115.EVENTO 				is 'Evento societário ocorrido no período';
comment on column cont_k115.EMP_COD_PART 		is 'Código da empresa envolvida na operação';
comment on column cont_k115.COND_PART    		is 'Condição da empresa relacionada à operação: 1– Sucessora; 2– Adquirente; 3– Alienante.';
comment on column cont_k115.PER_EVT      		is 'Percentual da empresa participante envolvida na operação';
