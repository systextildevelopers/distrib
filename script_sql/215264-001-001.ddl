-- Create table
create table BASI_020_LOG
(
  TIPO_OCORR           VARCHAR2(1) default '',
  DATA_OCORR           DATE,
  USUARIO_REDE         VARCHAR2(20) default '',
  MAQUINA_REDE         VARCHAR2(40) default '',
  APLICACAO            VARCHAR2(20) default '',
  USUARIO_SISTEMA      VARCHAR2(20) default '',
  NOME_PROGRAMA        VARCHAR2(20) default '',
  BASI030_NIVEL030_OLD VARCHAR2(1) default '0' not null,
  BASI030_NIVEL030_NEW VARCHAR2(1) default '0' not null,
  BASI030_REFERENC_OLD VARCHAR2(5) default '00000' not null,
  BASI030_REFERENC_NEW VARCHAR2(5) default '00000' not null,
  TAMANHO_REF_OLD      VARCHAR2(3) default '000' not null,
  TAMANHO_REF_NEW      VARCHAR2(3) default '000' not null,
  DESCR_TAM_REFER_OLD  VARCHAR2(11) default '',
  DESCR_TAM_REFER_NEW  VARCHAR2(11) default '',
  RENDIMENTO_OLD       NUMBER(8,2) default 0.0,
  RENDIMENTO_NEW       NUMBER(8,2) default 0.0,
  HORA_OCORR           DATE
)
