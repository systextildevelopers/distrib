create table oper_280
(
   cod_empresa    	number(3)     default 0,
   um_xml           varchar2(6)   default '',
   um_produto       varchar2(6)   default ''
);

alter table oper_280 add constraint pk_oper_280 primary key (cod_empresa,um_xml,um_produto);
