create or replace procedure inter_pr_gera_sped_hkcardex(p_cod_empresa   in number,
                                                     p_dat_final     IN DATE,
                                                     p_des_erro    out varchar2) is


w_erro                EXCEPTION;
w_uni_med             basi_030.unidade_medida%type;
w_codigo_contabil     basi_010.codigo_contabil%type;
w_ind_invent_kardex_nf number(1);
w_cnpj9_ter            number(9);
w_cnpj4_ter            number(4);
w_cnpj2_ter            number(2);
w_tip_propriedade_deposito number(1);
p_str_date_ini         varchar2(10);
p_dat_ini              date;
p_mes                  number(2);
p_ano                  number(4);
w_tipo_produto_sped    basi_150.tipo_produto_sped%type;
w_conta_estoque        basi_150.conta_estoque%type;
w_ind_valoriza_cardex  varchar2(1);
v_ncm                  basi_010.classific_fiscal%type;
v_conta_reg            number;

--
-- FAZ LEITURA DE PRODUTO CADASTRADOS - CARDEX
--

CURSOR u_estq301K (p_cod_empresa          NUMBER,
                   p_dat_final            DATE) IS

    select estq301.codigo_deposito, estq301.nivel_estrutura, estq301.grupo_estrutura, estq301.subgrupo_estrutura,
           estq301.item_estrutura,  sum(estq_301.saldo_fisico) saldo_fisico,
           sum(estq_301.saldo_financeiro) saldo_financeiro, sum(estq_301.preco_medio_unitario) preco_medio_unitario,
           max(estq301.mes_ano_movimento) mesAnoMovimento
    from estq_301, (

       select estq_301.codigo_deposito, estq_301.nivel_estrutura, estq_301.grupo_estrutura, estq_301.subgrupo_estrutura,
              estq_301.item_estrutura,  max(estq_301.mes_ano_movimento) mes_ano_movimento
       from estq_301, basi_205, fatu_503
       where estq_301.codigo_deposito          = basi_205.codigo_deposito
       and   estq_301.mes_ano_movimento       <= p_dat_final
       and   basi_205.controla_ficha_cardex    = 1
       and   basi_205.local_deposito            = p_cod_empresa
       and   fatu_503.codigo_empresa            = p_cod_empresa
       and   ( (basi_205.tipo_valorizacao in (1,4) and fatu_503.apuracao_ir = 2) or fatu_503.apuracao_ir = 1)
       group by estq_301.codigo_deposito,estq_301.nivel_estrutura, estq_301.grupo_estrutura, estq_301.subgrupo_estrutura,
                estq_301.item_estrutura) estq301

    where estq_301.codigo_deposito    = estq301.codigo_deposito
      and estq_301.nivel_estrutura    = estq301.nivel_estrutura
      and estq_301.grupo_estrutura    = estq301.grupo_estrutura
      and estq_301.subgrupo_estrutura = estq301.subgrupo_estrutura
      and estq_301.item_estrutura     = estq301.item_estrutura
      and estq_301.mes_ano_movimento  = estq301.mes_ano_movimento
    
    group by estq301.codigo_deposito,    estq301.nivel_estrutura,
             estq301.grupo_estrutura,    estq301.subgrupo_estrutura,
             estq301.item_estrutura
             
    having sum(estq_301.saldo_fisico) <> 0;



BEGIN


   begin
       select fatu_504.ind_valoriza_cardex,  fatu_504.ind_invent_kardex_nf
       into w_ind_valoriza_cardex,           w_ind_invent_kardex_nf
       from fatu_504
       where fatu_504.codigo_empresa = p_cod_empresa;
   end;

   v_conta_reg := 0;

   -- *********   INICIA FOR PARA ESTOQUE PR?PRIO ******


   FOR estq301 IN u_estq301K(p_cod_empresa, p_dat_final)
   LOOP
      begin
       select basi_205.cnpj9, basi_205.cnpj4, basi_205.cnpj2, basi_205.tip_propriedade_deposito
       into   w_cnpj9_ter,    w_cnpj4_ter,    w_cnpj2_ter,    w_tip_propriedade_deposito
       from basi_205
       where basi_205.codigo_deposito = estq301.codigo_deposito
         and w_ind_invent_kardex_nf   = 1
         and basi_205.tip_propriedade_deposito > 1;
      exception
      when no_data_found then
         w_cnpj9_ter := 0;
         w_cnpj4_ter := 0;
         w_cnpj2_ter := 0;
         w_tip_propriedade_deposito := 1;
       end;

     -- encontra o codigo contabil do produto
     begin
        select basi_010.codigo_contabil, basi_010.classific_fiscal
        into   w_codigo_contabil,        v_ncm
        from basi_010
        where basi_010.nivel_estrutura  = estq301.nivel_estrutura
          and basi_010.grupo_estrutura  = estq301.grupo_estrutura
          and basi_010.subgru_estrutura = estq301.subgrupo_estrutura
          and basi_010.item_estrutura   = estq301.item_estrutura;
     exception

     when no_data_found then
        w_codigo_contabil := 0;
     end;

      -- encontra a unidade de medida do produto
      begin
         select basi_030.unidade_medida, basi_030.conta_estoque  into w_uni_med, w_conta_estoque
         from basi_030
         where basi_030.nivel_estrutura  = estq301.nivel_estrutura
           and basi_030.referencia       = estq301.grupo_estrutura;

      exception
      when no_data_found then
         w_uni_med       := ' ';
         w_conta_estoque := 0;
      end;

      begin
         select basi_150.tipo_produto_sped  into w_tipo_produto_sped
         from basi_150
         where basi_150.conta_estoque = w_conta_estoque;

      exception
      when no_data_found then
         w_tipo_produto_sped := 0;
      end;

      w_tipo_produto_sped := inter_fn_encontra_tp_prod_sped(p_cod_empresa, w_conta_estoque,estq301.nivel_estrutura,estq301.grupo_estrutura,estq301.subgrupo_estrutura,estq301.item_estrutura,w_tipo_produto_sped);

      p_ano := extract(year from p_dat_final);
      p_mes := extract(month from p_dat_final);

      p_str_date_ini := '02-'||to_char(p_mes)||'-'||to_char(p_ano);
      p_dat_ini := to_date(p_str_date_ini,'dd-mm-yyyy');

      if estq301.mesAnoMovimento <= p_dat_ini
      then
         estq301.mesAnoMovimento := p_dat_final;
      end if;
	  
	  if (estq301.saldo_fisico <= 0) then
	       continue;
	  end if;
	  
      v_conta_reg := v_conta_reg + 1;

      BEGIN
      INSERT INTO SPED_K200_H010
        (
        EMPRESA
        ,MES
        ,ANO
        ,CNPJ9_PARTICIPANTE
        ,CNPJ4_PARTICIPANTE
        ,CNPJ2_PARTICIPANTE
        ,NIVEL
        ,GRUPO
        ,SUBGRUPO
        ,ITEM
        ,ESTAGIO_AGRUPADOR
        ,ESTAGIO_AGRUPADOR_SIMULTANEO
        ,QUANTIDADE
        ,VALOR_UNITARIO
        ,VALOR_TOTAL
        ,TIPO_PROPRIEDADE
        ,CODIGO_CONTABIL
        ,UNIDADE_MEDIDA
        ,VALOR_IR
        ,CODIGO_DEPOSITO
        ,TIPO_PRODUTO_SPED
        ,NCM
        )
      values (
         p_cod_empresa
        ,p_mes
        ,p_ano
        ,w_cnpj9_ter
        ,w_cnpj4_ter
        ,w_cnpj2_ter
        ,estq301.nivel_estrutura
        ,estq301.grupo_estrutura
        ,estq301.subgrupo_estrutura
        ,estq301.item_estrutura
        ,0
        ,0
        ,estq301.saldo_fisico
        ,estq301.preco_medio_unitario
        ,estq301.saldo_financeiro
        ,w_tip_propriedade_deposito - 1
        ,w_codigo_contabil
        ,w_uni_med
        ,estq301.preco_medio_unitario
        ,estq301.codigo_deposito
        ,w_tipo_produto_sped
        ,v_ncm
        );
      EXCEPTION
         WHEN OTHERS THEN
          p_des_erro := 'Erro na inclusao da tabela sped_k200_h010 (1)' || Chr(10) ||
                ' PRODUTO: ' || estq301.nivel_estrutura
                       || estq301.grupo_estrutura
                       || estq301.subgrupo_estrutura
                       || estq301.item_estrutura ||
                Chr(10) || SQLERRM;
          RAISE W_ERRO;
      END;

      if v_conta_reg = 10000
      then
         v_conta_reg := 0;
         COMMIT;
      end if;
   END LOOP;

   commit;


EXCEPTION
WHEN W_ERRO then
   p_des_erro := 'Erro na procedure p_gera_saldo_estoque_k200_h010 ' || Chr(10) || p_des_erro;
WHEN OTHERS THEN
   p_des_erro := 'Outros erros na procedure p_gera_saldo_estoque_k200_h010 ' || Chr(10) || SQLERRM;
END inter_pr_gera_sped_hkcardex;
/
