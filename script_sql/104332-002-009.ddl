create table OBRF_828
( COD_EMPRESA          NUMBER(3)    default 0 not null,
  MES                  NUMBER(2)    default 0 not null,
  ANO                  NUMBER(4)    default 0 not null,
  COD_INF_ADIC         VARCHAR2(8) default ' ' not null,
  VALOR_AJUSTE         NUMBER(13,2) default 0.00,
  DESCR_PROCESSO       VARCHAR2(250) default ' ');
  
alter table OBRF_828 add constraint PK_OBRF_828 primary key (COD_EMPRESA,MES,ANO,COD_INF_ADIC);
 
create synonym systextilrpt.OBRF_828 for OBRF_828; 
