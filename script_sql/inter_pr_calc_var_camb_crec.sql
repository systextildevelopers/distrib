create or replace PROCEDURE "INTER_PR_CALC_VAR_CAMB_CREC"(
   p_tipo_calculo in number,    p_empresa     in number,    p_filial      in number,
   p_periodo_mes in varchar2,    p_periodo_ano in varchar2,    
   p_titulo       in number,    p_parcela     in varchar2,  p_tipo_titulo in number,
   p_cnpj9        in number,    p_cnpj4       in number,    p_cnpj2       in number, 
   p_moeda        in number,    p_cotacao     in number)
is
   cursor titulos is 
   select * from fatu_070
   where (fatu_070.codigo_empresa    = p_filial 
     or (p_filial = 9999 
         and exists (select 1 from fatu_500
                     where fatu_500.codigo_matriz  = p_empresa
                       and fatu_500.codigo_empresa = fatu_070.codigo_empresa)))
     and ((
         fatu_070.num_duplicata    = p_titulo
         and fatu_070.seq_duplicatas   = p_parcela
         and fatu_070.tipo_titulo      = p_tipo_titulo
         and fatu_070.cli_dup_cgc_cli9 = p_cnpj9
         and fatu_070.cli_dup_cgc_cli4 = p_cnpj4
         and fatu_070.cli_dup_cgc_cli2 = p_cnpj2
         and p_tipo_calculo = 2) 
         or p_tipo_calculo = 1
     )
     and fatu_070.situacao_duplic <> 2
     and fatu_070.saldo_duplicata <> 0
     and fatu_070.moeda_titulo    = p_moeda;
      
   v_data_ajuste                  cpag_019.data_ajuste%type;
   v_sld_reais_antes              cpag_019.sld_reais_antes%type;
   v_vlr_ajuste                   fatu_070.saldo_duplicata%type;
   v_num_contabil                 fatu_070.num_contabil%type;
   v_conta_credito                cont_535.cod_reduzido%type;
   v_conta_debito                 cont_535.cod_reduzido%type;
   v_ativa_passiva                number;
   v_exercicio                    cont_500.exercicio%type;
   v_cod_plano_cta                cont_500.cod_plano_cta%type;
   v_cta_var_camb_ativ_crec       cont_560.cta_var_camb_ativ_crec%type;
   v_cta_var_camb_pass_crec       cont_560.cta_var_camb_pass_crec%type;
   v_hst_var_camb_crec            cont_560.hst_var_camb_crec%type;
   v_ccusto_var_camb_crec         cont_560.ccusto_var_camb_crec%type;
   v_compl_histor1                cont_600.compl_histor1%type;
   v_cod_contab_cli               pedi_010.codigo_contabil%type;
   v_valor_saldo_moeda            fatu_070.saldo_duplicata%type;
   v_saldo_duplicata_old          fatu_070.saldo_duplicata%type;
   v_saldo_duplicata_new          fatu_070.saldo_duplicata%type;
   v_valor_duplicata_new          fatu_070.valor_duplicata%type;
   v_gera_contabil                fatu_500.gera_contabil%type;
   v_trans_atualiza_contab        estq_005.atualiza_contabi%type;
   v_vlr_ajuste_contab            fatu_070.saldo_duplicata%type;
   v_des_erro                     varchar2(1000);
   v_executou_processo            boolean;
   v_teve_cpag_019                boolean;
   
begin   

   if p_tipo_calculo = 1
    then
        v_data_ajuste := last_day(TO_DATE( p_periodo_mes || '01' || p_periodo_ano, 'MMDDYY'));
    else
        v_data_ajuste := sysdate;
    end if;


   begin
     select fatu_500.gera_contabil 
     into v_gera_contabil
     from fatu_500
     where fatu_500.codigo_empresa = p_empresa;
   end;
   
   v_executou_processo := FALSE;

   for titulo in titulos
   loop 
     
      begin
         v_saldo_duplicata_old := titulo.saldo_duplicata;
   
         begin
           select estq_005.atualiza_contabi 
           into v_trans_atualiza_contab
           from estq_005
           where estq_005.codigo_transacao = titulo.cod_transacao;
         exception
           when others then v_trans_atualiza_contab := 0;
         end;

         v_teve_cpag_019   := TRUE;
   
         begin
           select cpag_019.sld_reais_antes into v_sld_reais_antes
           from cpag_019
           where cpag_019.cod_empresa = titulo.codigo_empresa
             and cpag_019.num_titulo  = titulo.num_duplicata
             and cpag_019.parcela     = titulo.seq_duplicatas
             and cpag_019.tipo_titulo = titulo.tipo_titulo
             and cpag_019.cnpj9       = titulo.cli_dup_cgc_cli9
             and cpag_019.cnpj4       = titulo.cli_dup_cgc_cli4
             and cpag_019.cnpj2       = titulo.cli_dup_cgc_cli2
             and cpag_019.data_ajuste = v_data_ajuste
             and cpag_019.situacao    = 0;
         exception
           when others then 
               v_sld_reais_antes := 0.00;
               v_teve_cpag_019   := FALSE;
         end;
         
         inter_pr_get_saldo_moeda_crec(titulo.codigo_empresa, titulo.num_duplicata, titulo.seq_duplicatas, titulo.tipo_titulo, titulo.cli_dup_cgc_cli9,
            titulo.cli_dup_cgc_cli4, titulo.cli_dup_cgc_cli2, titulo.valor_moeda, v_valor_saldo_moeda);
         
         v_saldo_duplicata_new := v_valor_saldo_moeda * p_cotacao;
         v_vlr_ajuste := v_saldo_duplicata_new - v_saldo_duplicata_old;
         v_valor_duplicata_new := titulo.valor_duplicata + v_vlr_ajuste;
         
         if v_vlr_ajuste >= 0
         then v_ativa_passiva := 0;
         else v_ativa_passiva := 1;
         end if;
         
         if v_vlr_ajuste <> 0
         then
            v_executou_processo := TRUE;
         
           if v_sld_reais_antes <> 0
           then         
              begin
                 delete from cpag_019 
                 where cpag_019.cod_empresa = titulo.codigo_empresa
                   and cpag_019.num_titulo  = titulo.num_duplicata
                   and cpag_019.parcela     = titulo.seq_duplicatas
                   and cpag_019.tipo_titulo = titulo.tipo_titulo
                   and cpag_019.cnpj9       = titulo.cli_dup_cgc_cli9
                   and cpag_019.cnpj4       = titulo.cli_dup_cgc_cli4
                   and cpag_019.cnpj2       = titulo.cli_dup_cgc_cli2
                   and cpag_019.data_ajuste = v_data_ajuste;
              
            
                 if v_gera_contabil = 1 and v_trans_atualiza_contab = 1 
                 then
                   inter_pr_gera_lanc_cont(titulo.codigo_empresa,
                                   22,
                                   titulo.num_contabil,
                                   v_ccusto_var_camb_crec,
                                   v_data_ajuste,
                                   v_hst_var_camb_crec,
                                   v_compl_histor1,
                                   3, 
                                   titulo.cod_transacao,
                                   0,
                                   0.00, 
                                   0,
                                   0.00, 
                                   0,
                                   titulo.conta_corrente,
                                   v_data_ajuste,
                                   titulo.num_duplicata, 
                                   titulo.cli_dup_cgc_cli9, 
                                   titulo.cli_dup_cgc_cli4, 
                                   titulo.cli_dup_cgc_cli2, 
                                   1, 
                                   titulo.num_duplicata, 
                                   '1',
                                   1, 
                                   1,
                                   0,
                                   0, 
                                   0, 
                                   0, 
                                   v_des_erro);
                   if v_des_erro is not null
                   then raise_application_error(-20001, v_des_erro);
                   end if;
                 end if;
              end;
           end if;
         
           v_sld_reais_antes := titulo.saldo_duplicata;
         
           if v_gera_contabil = 1 and v_trans_atualiza_contab = 1
           then
         
             v_exercicio := inter_fn_checa_data(p_empresa, v_data_ajuste,0);
             begin
               select cod_plano_cta 
               into v_cod_plano_cta
               from cont_500
               where exercicio = v_exercicio
               and cod_empresa = p_empresa;
             exception
                when others then v_cod_plano_cta := 0;
             end;
             begin
               select cta_var_camb_ativ_crec, cta_var_camb_pass_crec,
                      hst_var_camb_crec, ccusto_var_camb_crec
               into v_cta_var_camb_ativ_crec, v_cta_var_camb_pass_crec,
                    v_hst_var_camb_crec, v_ccusto_var_camb_crec
               from cont_560
               where cont_560.cod_plano_cta = v_cod_plano_cta;
             exception
                when others 
                then 
                  v_cta_var_camb_ativ_crec  := -1;
                  v_cta_var_camb_pass_crec  := -1;
                  v_hst_var_camb_crec       := 0;
                  v_ccusto_var_camb_crec    := 0;
             end;
             
             if v_cta_var_camb_ativ_crec = 0 then 
                v_cta_var_camb_ativ_crec := -1;
             end if;
             if v_cta_var_camb_pass_crec = 0 then 
                v_cta_var_camb_pass_crec := -1;
             end if;
         
             begin
               select pedi_010.codigo_contabil
               into v_cod_contab_cli
               from pedi_010
               where pedi_010.cgc_9 = titulo.cli_dup_cgc_cli9
                 and pedi_010.cgc_4 = titulo.cli_dup_cgc_cli4
                 and pedi_010.cgc_2 = titulo.cli_dup_cgc_cli2;
             exception
                when others then v_cod_contab_cli := 0;
             end;
         
             v_compl_histor1 := titulo.num_duplicata || '/' || titulo.seq_duplicatas || ' - ' || titulo.tipo_titulo || ' - ' 
                           || titulo.cli_dup_cgc_cli9 || '/' || titulo.cli_dup_cgc_cli4 || '-' || titulo.cli_dup_cgc_cli2;
         
             if v_ativa_passiva = 0
             then 
               v_conta_credito := v_cta_var_camb_ativ_crec;
               v_conta_debito := INTER_FN_ENCONTRA_CONTA(titulo.codigo_empresa,
                                                       1,
                                                       v_cod_contab_cli,
                                                       titulo.cod_transacao,
                                                       v_ccusto_var_camb_crec,
                                                       v_exercicio,
                                                       v_exercicio);
               if v_conta_debito < 0
               THEN
                 v_des_erro := 'Conta contabil de débito não encontrada. ' || 
                               'Esta transação não será confirmada. Contate o contador. ' ||
                               'Tipo contábil: 1, Transação: ' || titulo.cod_transacao || ' ' ||
                               'Código contábil: ' || v_cod_contab_cli;
                 raise_application_error(-20001, v_des_erro);
               END IF;
             else 
               v_conta_credito := INTER_FN_ENCONTRA_CONTA(titulo.codigo_empresa,
                                                       1,
                                                       v_cod_contab_cli,
                                                       titulo.cod_transacao,
                                                       v_ccusto_var_camb_crec,
                                                       v_exercicio,
                                                       v_exercicio);
               v_conta_debito := v_cta_var_camb_pass_crec;
               if v_conta_credito < 0
               THEN
                 v_des_erro := 'Conta contabil de débito não encontrada. ' || 
                               'Esta transação não será confirmada. Contate o contador. ' ||
                               'Tipo contábil: 1, Transação: ' || titulo.cod_transacao || ' ' ||
                               'Código contábil: ' || v_cod_contab_cli;
                 raise_application_error(-20001, v_des_erro);
               END IF;
             end if;
         
         
             v_num_contabil := 0;
           
             if v_vlr_ajuste >= 0
             then v_vlr_ajuste_contab := v_vlr_ajuste;
             else v_vlr_ajuste_contab := v_vlr_ajuste * -1;
             end if;
         
             if v_conta_debito > 0 and v_conta_credito > 0 
             then
               inter_pr_gera_lanc_cont(titulo.codigo_empresa,
                                   22,
                                   v_num_contabil,
                                   v_ccusto_var_camb_crec,
                                   v_data_ajuste,
                                   v_hst_var_camb_crec,
                                   v_compl_histor1,
                                   1, 
                                   titulo.cod_transacao,
                                   v_conta_debito,
                                   v_vlr_ajuste_contab, 
                                   v_conta_credito,
                                   v_vlr_ajuste_contab, 
                                   0,
                                   titulo.conta_corrente,
                                   v_data_ajuste,
                                   titulo.num_duplicata, 
                                   titulo.cli_dup_cgc_cli9, 
                                   titulo.cli_dup_cgc_cli4, 
                                   titulo.cli_dup_cgc_cli2, 
                                   1, 
                                   titulo.num_duplicata, 
                                   '1',
                                   1, 
                                   1,
                                   0,
                                   0, 
                                   0, 
                                   0, 
                                   v_des_erro);
               if v_des_erro is not null 
               then raise_application_error(-20001, v_des_erro);
               end if;
             end if;
           end if;
         
           if v_num_contabil = 0
           then v_num_contabil := titulo.num_contabil;
           end if;
         
           begin
            --raise_application_error(-20001, 'update');
             update fatu_070 
             set saldo_duplicata = v_saldo_duplicata_new,
                 num_contabil    = v_num_contabil,
                 valor_duplicata = v_valor_duplicata_new,
                 variacao_cambial = 1
             where fatu_070.codigo_empresa      = titulo.codigo_empresa
               and fatu_070.cli_dup_cgc_cli9    = titulo.cli_dup_cgc_cli9
               and fatu_070.cli_dup_cgc_cli4    = titulo.cli_dup_cgc_cli4
               and fatu_070.cli_dup_cgc_cli2    = titulo.cli_dup_cgc_cli2
               and fatu_070.tipo_titulo         = titulo.tipo_titulo
               and fatu_070.num_duplicata       = titulo.num_duplicata
               and fatu_070.seq_duplicatas      = titulo.seq_duplicatas;
           end;
         
           begin
             --raise_application_error(-20001, 'insert ');
             insert into cpag_019
               (cod_empresa,      data_ajuste, num_titulo,    parcela,        tipo_titulo, 
                cnpj9,            cnpj4,       cnpj2,         cod_moeda,      cotacao, 
                data_emissao,     data_vencto, vlr_tit_moeda, sld_tit_moeda,  sld_reais_antes, 
                slr_reais_depois, vlr_ajuste,  ativa_passiva, chave_contabil, conta_debito, 
                conta_credito,    situacao, ind_pagar_receb)
             values
               (titulo.codigo_empresa,   v_data_ajuste,           titulo.num_duplicata,    titulo.seq_duplicatas, titulo.tipo_titulo, 
                titulo.cli_dup_cgc_cli9, titulo.cli_dup_cgc_cli4, titulo.cli_dup_cgc_cli2, titulo.moeda_titulo,   p_cotacao,
                titulo.data_emissao,     titulo.data_venc_duplic, titulo.valor_moeda,      v_valor_saldo_moeda,   v_sld_reais_antes, 
                v_saldo_duplicata_new,   v_vlr_ajuste,            v_ativa_passiva,         v_num_contabil,        v_conta_debito,          
                v_conta_credito,         0, 'R');
           end;
         
           commit;
         end if;
     EXCEPTION
        WHEN OTHERS
        THEN
             if p_tipo_calculo = 2 -- por titulo
           then
             raise_application_error(-20001, 'Erro ao processar inter_pr_contabilizacao ' || SQLERRM);
           else -- por periodo
              if v_teve_cpag_019
              then
               -- raise_application_error(-20001, 'update exception' || SQLERRM);
                  update cpag_019
                  set situacao = 1,    observacao = v_des_erro
                  where cpag_019.cod_empresa = titulo.codigo_empresa
                  and cpag_019.num_titulo  = titulo.num_duplicata
                  and cpag_019.parcela     = titulo.seq_duplicatas
                  and cpag_019.tipo_titulo = titulo.tipo_titulo
                  and cpag_019.cnpj9       = titulo.cli_dup_cgc_cli9
                  and cpag_019.cnpj4       = titulo.cli_dup_cgc_cli4
                  and cpag_019.cnpj2       = titulo.cli_dup_cgc_cli2
                  and cpag_019.data_ajuste = v_data_ajuste;

              else
                  --raise_application_error(-20001, 'insert exception' || SQLERRM);
                  insert into cpag_019
                  (cod_empresa,      data_ajuste, num_titulo,    parcela,        tipo_titulo, 
                  cnpj9,            cnpj4,       cnpj2,         cod_moeda,      cotacao, 
                  data_emissao,     data_vencto, vlr_tit_moeda, sld_tit_moeda,  sld_reais_antes, 
                  slr_reais_depois, vlr_ajuste,  ativa_passiva, chave_contabil, conta_debito, 
                  conta_credito,    situacao,    observacao,    ind_pagar_receb)
                  values
                  (titulo.codigo_empresa,   v_data_ajuste,           titulo.num_duplicata,    titulo.seq_duplicatas, titulo.tipo_titulo, 
                  titulo.cli_dup_cgc_cli9, titulo.cli_dup_cgc_cli4, titulo.cli_dup_cgc_cli2, titulo.moeda_titulo,   p_cotacao,
                  titulo.data_emissao,     titulo.data_venc_duplic, titulo.valor_moeda,      v_valor_saldo_moeda,   v_sld_reais_antes, 
                  v_saldo_duplicata_new,   v_vlr_ajuste,            v_ativa_passiva,         v_num_contabil,        v_conta_debito,          
                  v_conta_credito,         1, v_des_erro,           'R');
                
              end if;
           commit;
        end if;
     END;
   end loop; 
   if not v_executou_processo
   then
      raise_application_error(-20001, 'Nao foi alterado valor cambial de nenhum titulo.');
   end if;
end inter_pr_calc_var_camb_crec;
