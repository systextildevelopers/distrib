alter table fatu_050 add
(numero_caixa_ecf               number(6)             default 0);

alter table fatu_050 add
(serie_integracao               varchar2(3 byte)      default ' ');

alter table fatu_050 add
(nota_integracao                number(9)             default 0);

alter table fatu_050 add
(e_mail_envio_nota              varchar2(100 byte)    default '');

alter table fatu_050 add
(e_mail_a_utilizar              varchar2(100 byte)    default '');

alter table fatu_050 add
(data_embarque_despacho         date);

alter table fatu_050 add
(nr_solicitacao_dav             varchar2(10 byte));

/
