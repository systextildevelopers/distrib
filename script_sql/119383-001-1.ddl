CREATE TABLE PCPC_TAGS_IMPRESSAS
(
  periodo_producao     NUMBER(4) default 0 not null,
  ordem_producao       NUMBER(9) default 0 not null,
  ordem_confeccao      NUMBER(5) default 0 not null,
  sequencia            NUMBER(9) default 0 not null,
  nivel                VARCHAR2(1) default '0',
  grupo                VARCHAR2(5) default '00000',
  subgrupo             VARCHAR2(3) default '000',
  item                 VARCHAR2(6) default '000000',
  codigo_usuario       NUMBER(5) default 0 not null,
  nome_usuario         VARCHAR2(80) default '',
  data_impressao       DATE,
  hora_impressao       DATE
);

/
