create or replace view vi_danfe_nfe_itens as
select   cod_nota
         ,serie
         ,cgc9
         ,cgc4
         ,cgc2
         ,cod_empresa
         ,cod_prod
         ,descr_prod
         ,ncm
         ,cst
         ,cfop
         ,unidade
         ,quant
         ,vlr_unit
         ,vlr_tot
         ,bc_icms
         ,v_icms
         ,v_ipi
         ,aliq_icms
         ,aliq_ipi
from 
(select  o.capa_ent_nrdoc as cod_nota
         ,o.capa_ent_serie as serie
         ,o.capa_ent_forcli9 as cgc9
         ,o.capa_ent_forcli4 as cgc4
         ,o.capa_ent_forcli2 as cgc2
         ,c.local_entrega as cod_empresa
         ,o.coditem_nivel99||o.coditem_grupo||o.coditem_subgrupo||o.coditem_item as cod_prod
         ,o.descricao_item as descr_prod
         ,o.classific_fiscal as ncm
         ,o.cod_vlfiscal_icm as cst
         ,replace(p.cod_natureza || p.operacao_fiscal,'.','') as cfop
         ,o.unidade_medida as unidade
         ,o.quantidade as quant
         ,o.valor_unitario as vlr_unit
         ,o.valor_total as vlr_tot
         ,o.base_calc_icm as bc_icms
         ,o.valor_icms as v_icms
         ,o.valor_ipi as v_ipi
         ,o.percentual_icm as aliq_icms
         ,o.percentual_ipi as aliq_ipi
 from obrf_015 o, obrf_010 c, pedi_080 p
 where c.documento      = o.capa_ent_nrdoc
   and c.serie          = o.capa_ent_serie
   and c.cgc_cli_for_9  = o.capa_ent_forcli9
   and c.cgc_cli_for_4  = o.capa_ent_forcli4
   and c.cgc_cli_for_2  = o.capa_ent_forcli2
   and c.cod_status     = '100'
   and p.natur_operacao = o.natitem_nat_oper
   and p.estado_natoper = o.natitem_est_oper)
union all
(select f.ch_it_nf_num_nfis
        ,f.ch_it_nf_ser_nfis
        ,c_s.cgc_9
        ,c_s.cgc_4
        ,c_s.cgc_2
        ,f.ch_it_nf_cd_empr
        ,f.nivel_estrutura || f.grupo_estrutura || f.subgru_estrutura || f.item_estrutura
        ,f.descricao_item
        ,f.classific_fiscal
        ,f.cvf_icms
        ,replace(p.cod_natureza || p.operacao_fiscal,'.','')
        ,f.unidade_medida
        ,f.qtde_item_fatur
        ,f.valor_unitario
        ,(f.valor_unitario * f.qtde_item_fatur)
        ,f.base_icms
        ,f.valor_icms
        ,f.valor_ipi
        ,f.perc_icms
        ,f.perc_ipi
 from fatu_060 f, fatu_050 c_s, pedi_080 p
 where c_s.codigo_empresa  = f.ch_it_nf_cd_empr
   and c_s.num_nota_fiscal = f.ch_it_nf_num_nfis
   and c_s.serie_nota_fisc = f.ch_it_nf_ser_nfis
   and c_s.cod_status     = '100'
   and p.natur_operacao = f.natopeno_nat_oper
   and p.estado_natoper = f.natopeno_est_oper)
       
        
