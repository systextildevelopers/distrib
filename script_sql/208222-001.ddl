-- Cópia de Composição por Idioma

insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('basi_f596', 'Cópia de Composição por Idioma', 0,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'basi_f596', 'basi_menu', 1, 0, 'S', 'S', 'S', 'S');

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'basi_f596', 'basi_menu', 1, 0, 'S', 'S', 'S', 'S');

update hdoc_036
   set hdoc_036.descricao       = 'Copia de composición por idioma'
 where hdoc_036.codigo_programa = 'basi_f596'
   and hdoc_036.locale          = 'es_ES';
commit;
