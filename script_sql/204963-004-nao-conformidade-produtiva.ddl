create table efic_305(  
id number(6),
data date,
usuario varchar2(30),
cod_setor number(3),
ordem_producao number(9),
referencia_peca varchar2(5),
cod_estagio number(2),
qtde_pecas number(3),
tempo_improd number(3),
cod_setor_ocorr number(3),
cod_tipo number (4),
detalhes long,
cod_setor_impacto number(3),
fase varchar2(30),
constraint efic_305_pk primary key (id),
constraint fk_efic_305_mqop_005	foreign key (cod_estagio) references mqop_005 (codigo_estagio),
constraint fk_efic_305_efic_300 foreign key (cod_tipo) references efic_300 (id),
constraint fk_efic_305_basi_006 foreign key (cod_setor) references basi_006 (setor_responsavel),
constraint fk_efic_305_basi_006_ocorr foreign key (cod_setor_ocorr) references basi_006 (setor_responsavel),
constraint fk_efic_305_basi_006_impac foreign key (cod_setor_impacto) references basi_006 (setor_responsavel)
);

comment on column efic_305.id is 'Identificador da não conformidade';
comment on column efic_305.data is 'Data de registro da não conformidade';
comment on column efic_305.usuario is 'Usuário que registrou a não conformidade';
comment on column efic_305.cod_setor is 'Setor do usuário que registrou a não conformidade';
comment on column efic_305.ordem_producao is 'Ordem de produção do registro da não conformidade';
comment on column efic_305.referencia_peca is 'Referencia da peça da não conformidade';
comment on column efic_305.cod_estagio is 'Código estágio que gerou a não conformidade';
comment on column efic_305.qtde_pecas is 'Quantidade de peças com problemas';
comment on column efic_305.tempo_improd is 'Tempo improdutivo em minutos';
comment on column efic_305.cod_setor_ocorr is 'Código do setor que gerou a não conformidade';
comment on column efic_305.cod_tipo is 'Código do tipo de não conformidade';
comment on column efic_305.detalhes is 'Detalhes da não conformidade';
comment on column efic_305.cod_setor_impacto is 'Código do setor impactado pela não conformidade';
comment on column efic_305.fase is 'Fase da ordem de produção';
comment on table efic_305 is 'Tabela de cadastro de não conformidade produtivas';

/	
	
