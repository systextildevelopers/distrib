insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('obrf_f741', 'Documentos de Ajustes Fiscais (DIFAL)',0,1);


insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'obrf_f741', ' ' ,0, 99, 'S', 'S', 'S', 'S');

update hdoc_036
set hdoc_036.descricao       = 'Documentos de Ajustes Fiscais (DIFAL)'
where hdoc_036.codigo_programa = 'obrf_f741'
  and hdoc_036.locale          = 'es_ES';

commit;
