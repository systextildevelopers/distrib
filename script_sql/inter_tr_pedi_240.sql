
  CREATE OR REPLACE TRIGGER "INTER_TR_PEDI_240" 
  before insert or delete or update of status_inf on pedi_240
  for each row

declare
   v_cgc_cli9          pedi_240.infocli_cgc_cli9%type;
   v_cgc_cli4          pedi_240.infocli_cgc_cli4%type;
   v_cgc_cli2          pedi_240.infocli_cgc_cli2%type;
   v_tipo_informacao   pedi_240.tipo_informacao%type;
   v_data_informacao   pedi_240.data_informacao%type;
   v_informante        pedi_240.informante%type;
   v_historico_inf     pedi_240.historico_inf%type;

   v_status_inf_old    pedi_240.status_inf%type;
   v_status_inf_new    pedi_240.status_inf%type;

   v_data_ocorr        date;
   v_usuario_rede      varchar2(20);
   v_maquina_rede      varchar2(40);
   v_aplicacao         varchar2(40);
   v_tipo_ocorr        varchar2(1);

begin

   if inserting
   then
      v_cgc_cli9        := :new.infocli_cgc_cli9;
      v_cgc_cli4        := :new.infocli_cgc_cli4;
      v_cgc_cli2        := :new.infocli_cgc_cli2;
      v_tipo_informacao := :new.tipo_informacao;
      v_data_informacao := :new.data_informacao;
      v_informante      := :new.informante;
      v_historico_inf   := :new.historico_inf;

      v_status_inf_old := null;
      v_status_inf_new := :new.status_inf;

      v_tipo_ocorr     := 'I';
   else

      if updating
      then
         v_cgc_cli9        := :new.infocli_cgc_cli9;
         v_cgc_cli4        := :new.infocli_cgc_cli4;
         v_cgc_cli2        := :new.infocli_cgc_cli2;
         v_tipo_informacao := :new.tipo_informacao;
         v_data_informacao := :new.data_informacao;
         v_informante      := :new.informante;
         v_historico_inf   := :new.historico_inf;

         v_status_inf_old := :old.status_inf;
         v_status_inf_new := :new.status_inf;

         v_tipo_ocorr     := 'A';
      else
         v_cgc_cli9        := :old.infocli_cgc_cli9;
         v_cgc_cli4        := :old.infocli_cgc_cli4;
         v_cgc_cli2        := :old.infocli_cgc_cli2;
         v_tipo_informacao := :old.tipo_informacao;
         v_data_informacao := :old.data_informacao;
         v_informante      := :old.informante;
         v_historico_inf   := :old.historico_inf;

         v_status_inf_old := :old.status_inf;
         v_status_inf_new := null;

         v_tipo_ocorr     := 'D';
      end if;
   end if;

   select substr(osuser,1,20), substr(machine,1,40), substr(program,1,20)
   into   v_usuario_rede,      v_maquina_rede,       v_aplicacao
   from sys.gv_$session
   where audsid  = userenv('SESSIONID')
     and inst_id = userenv('INSTANCE')
     and rownum < 2;

   v_data_ocorr := sysdate;

   insert into pedi_240_hist
     (infocli_cgc_cli9,    infocli_cgc_cli4,    infocli_cgc_cli2,
      tipo_informacao,     data_informacao,     informante,
      historico_inf,       status_inf_old,      status_inf_new,
      tipo_ocorr,          data_ocorr,          usuario_rede,
      maquina_rede,        aplicacao)
   values
     (v_cgc_cli9,          v_cgc_cli4,          v_cgc_cli2,
      v_tipo_informacao,   v_data_informacao,   v_informante,
      v_historico_inf,     v_status_inf_old,    v_status_inf_new,
      v_tipo_ocorr,        v_data_ocorr,        v_usuario_rede,
      v_maquina_rede,      v_aplicacao);

end inter_tr_pedi_240;

-- ALTER TRIGGER "INTER_TR_PEDI_240" ENABLE
 

/

exec inter_pr_recompile;

