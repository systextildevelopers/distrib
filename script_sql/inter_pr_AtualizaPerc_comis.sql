CREATE OR REPLACE PROCEDURE INTER_PR_AtualizaPerc_comis(   p_empresa in number,
                                                    p_nivel in varchar2,       v_cod_repr in number,       p_data_lancto in date,
                                                    p_cod_hist in number,   p_nr_doc in number,         p_seq_doc in number,
                                                    p_tp_doc in number,     p_cgc9 in number,           p_cgc4 in number,
                                                    p_cgc2 in number,       p_bcalcComis in number,     p_perc_comis_fatu in number,
                                                    p_perc_comis in number, p_vlr_comissao in number,   p_des_erro in out varchar2)
is
    v_val_int number;
    v_sit_rep_cliente number;
    v_numero_periodo number;
    v_ultima_sequencia number;
    v_sequencia_lcto number;
    v_intTmp number;
    v_encontrou_periodo boolean;
    v_data_inicial date;
begin
  
    v_encontrou_periodo := false;
    begin
        select val_int
        into v_val_int
        from empr_008
        where CODIGO_EMPRESA  = p_empresa
        and   param = 'comissao.permiteComisRepInativo';
    exception when others then
        v_val_int := 0;
    end;

    v_sit_rep_cliente := 0;
    begin
        select SIT_REP_CLIENTE
        into v_sit_rep_cliente
        from pedi_020
        where COD_REP_CLIENTE  = v_cod_repr;
    exception when others then
        v_sit_rep_cliente := 2;
    end;

    if v_sit_rep_cliente <> 2
    or v_val_int = 1 then
    
       v_encontrou_periodo := true;
        begin
            select numero_periodo
            into v_numero_periodo
            from crec_100
            where divisao_produto = p_nivel
            and p_data_lancto BETWEEN data_inicial AND data_final
            and situacao_periodo <> 'F';
        exception when others then
            v_encontrou_periodo := false;
        end;

    /**/
       if  v_encontrou_periodo = false then 
         
          for reg_periodos in (  select crec_100.numero_periodo
                                 from crec_100
                                 where crec_100.divisao_produto = p_nivel
                                   and crec_100.data_inicial >= p_data_lancto
                                   and crec_100.situacao_periodo <> 'F' )
          loop
            
             if v_encontrou_periodo = true then
                 continue;
             end if;
             v_encontrou_periodo := false;
             begin
                 select crec_110.nr_perio_nrperiod
                 into v_numero_periodo
                 from crec_110
                 where crec_110.nr_perio_nrperiod = reg_periodos.numero_periodo
                 and crec_110.nr_perio_divpro = p_nivel
                 and crec_110.codigo_repr = v_cod_repr
                 and crec_110.situacao_lcto = 'F';
             exception when no_data_found then
                 v_encontrou_periodo := true;
                 v_numero_periodo:= reg_periodos.numero_periodo;
             end;
          end loop;
        end if;

        if v_encontrou_periodo = true then
            select nvl(max(crec_110.sequencia_lcto),0) 
            into v_ultima_sequencia
          from crec_110
          where crec_110.nr_perio_nrperiod = v_numero_periodo
            and crec_110.nr_perio_divpro = p_nivel
            and crec_110.codigo_repr = v_cod_repr
            and crec_110.data_lancamento = p_data_lancto
          order by crec_110.sequencia_lcto DESC;

            v_sequencia_lcto := v_ultima_sequencia + 1;

            insert into crec_110 (
                nr_perio_nrperiod, nr_perio_divpro,
                codigo_repr, data_lancamento,
                sequencia_lcto, codigo_historico,
                numero_documento, seq_documento,
                tipo_documento, cgc_cli9,
                cgc_cli4, cgc_cli2,
                base_calc_comis, perc_fatu_crec,
                percentual_comis, valor_lancamento
            ) values (
                v_numero_periodo ,  p_nivel ,
                v_cod_repr ,        p_data_lancto ,
                v_sequencia_lcto ,  p_cod_hist ,
                p_nr_doc ,          p_seq_doc ,
                p_tp_doc ,          p_cgc9 ,
                p_cgc4 ,            p_cgc2 ,
                p_bcalcComis ,      p_perc_comis_fatu ,
                p_perc_comis ,      p_vlr_comissao
            );
        end if;
    else
        null;
    end if;

EXCEPTION WHEN NO_DATA_FOUND
then
    p_des_erro := 'OCORREU UM ERRO DURANTE A EXECUÇÃO' || sqlerrm;
end INTER_PR_AtualizaPerc_comis;

/

exec inter_pr_recompile;

