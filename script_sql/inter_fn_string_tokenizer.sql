
  CREATE OR REPLACE FUNCTION "INTER_FN_STRING_TOKENIZER" (
   texto         varchar2,
   indice        number,
   delimitador   varchar2 := '.'
)
   return    varchar2
is
   inicio number;
   fim    number;
begin
   -- Esta funcao permite particionar uma string (parametro <texto>)
   -- tomando como delimitador determinado caracter, enviado pelo parametro <delimitador>.
   -- O parametro <indice> define qual das partes da string deve ser retornada.
   -- A string "TING.001.000001", se for enviada a funcao, requisitando o indice 2 e considerando o delimitador ".',
   -- retornara o texto "001".

   if indice = 0
   then
      return '';
   end if;

   if indice = 1
   then
      inicio := 1;
   else
       inicio := instr(texto, delimitador, 1, indice - 1);
       if inicio = 0
       then
           return null;
       else
           inicio := inicio + length(delimitador);
       end if;
   end if;

   fim := instr(texto, delimitador, inicio, 1);

   if fim = 0 then
      return substr(texto, inicio);
   else
      return substr(texto, inicio, fim - inicio);
   end if;

end inter_fn_string_tokenizer;
 

/

exec inter_pr_recompile;

