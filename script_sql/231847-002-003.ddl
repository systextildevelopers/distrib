-----------------------
-- Sugestão de Rolos --
-----------------------

create table pcpt_070 (
  id number(9,0) not null primary key,
  pedido_venda number(9,0) not null,
  seq_item_pedido number(3, 0) not null,
  observacao varchar2(300),
  situacao number(1,0) default 0 not null,
  sugestao_escolhida varchar2 (30) default 'Nenhuma' check(sugestao_escolhida in ('Nenhuma', 'Por Nuance', 'Por Ordem de Produção')) not null,
  criado_por varchar2(250),
  criado_em timestamp default current_timestamp
);

alter table pcpt_070 add constraint uni_pcpt_070 unique(pedido_venda, seq_item_pedido); 

comment on table pcpt_070 is 'Capa sugestão de alocação de rolos';
comment on column pcpt_070.id is 'Chave primária';
comment on column pcpt_070.pedido_venda is 'Numero do pedido';
comment on column pcpt_070.seq_item_pedido is 'Sequencia do pedido';
comment on column pcpt_070.observacao is 'Observação do funcionário ao escolher uma das sugestões';
comment on column pcpt_070.situacao is 'Situação da solicitação (0 - Sugestões feitas, 1 - Sugestão escolhida)';
comment on column pcpt_070.sugestao_escolhida is 'Qual das duas possiveis sugestões foi escolhida (Por Nuance / Por Ordem de Produção)';
comment on column pcpt_070.criado_por is 'O usuário que criou o registro';
comment on column pcpt_070.criado_em is 'A data em que o registro foi criado';

create sequence pcpt_070_seq;

create or replace trigger bi_pcpt_070
before insert on pcpt_070
for each row
begin
  :new.id := pcpt_070_seq.nextval;
end;
