
CREATE OR REPLACE TRIGGER "INTER_TR_PLAN_MOV_PEDI_110" 
   after insert or delete or update
       of CD_IT_PE_NIVEL99  , CD_IT_PE_GRUPO  ,
          CD_IT_PE_SUBGRUPO , CD_IT_PE_ITEM   ,
          QTDE_PEDIDA       , COD_CANCELAMENTO
   on pedi_110
   for each row

declare
   v_executa_trigger number(1);
   v_nivel           varchar2(1);
   v_grupo           varchar2(5);
   v_subgrupo        varchar2(3);
   v_item            varchar2(6);
   v_ordem_planej    number(9);
   v_pedido_venda    pedi_110.pedido_venda%type;

   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);

begin
   v_executa_trigger := 1;

   if inserting
   then
      v_pedido_venda := :new.pedido_venda;
   else
      v_pedido_venda := :old.pedido_venda;
   end if;

   begin
      select tmrp_610.ordem_planejamento
      into   v_ordem_planej
      from tmrp_625, tmrp_610
      where   tmrp_625.pedido_venda             = v_pedido_venda
        and   tmrp_625.ordem_planejamento       = tmrp_610.ordem_planejamento
        and   tmrp_610.situacao_ordem           < 9
        and   tmrp_625.nivel_produto_origem     = '0'
        and   tmrp_625.grupo_produto_origem     = '00000'
        and   tmrp_625.subgrupo_produto_origem  = '000'
        and   tmrp_625.item_produto_origem      = '000000'
        and ((tmrp_625.qtde_areceber_programada > 0 and exists (select 1 from tmrp_041, pcpc_020, tmrp_630
                                                                where tmrp_041.periodo_producao   = pcpc_020.periodo_producao
                                                                  and tmrp_041.area_producao      = 1
                                                                  and tmrp_041.nr_pedido_ordem    = tmrp_630.ordem_prod_compra
                                                                  and tmrp_041.codigo_estagio     = pcpc_020.ultimo_estagio
                                                                  and tmrp_041.nivel_estrutura    = tmrp_625.nivel_produto
                                                                  and tmrp_041.grupo_estrutura    = tmrp_625.grupo_produto
                                                                  and tmrp_041.subgru_estrutura   = tmrp_625.subgrupo_produto
                                                                  and tmrp_041.item_estrutura     = tmrp_625.item_produto
                                                                  and tmrp_041.qtde_areceber      > 0
                                                                  and pcpc_020.ordem_producao     = tmrp_630.ordem_prod_compra
                                                                  and pcpc_020.cod_cancelamento   = 0
                                                                  and tmrp_630.ordem_planejamento = tmrp_610.ordem_planejamento
                                                                  and tmrp_630.area_producao      = 1)) or
              tmrp_625.qtde_areceber_programada = 0)
        and rownum = 1;
      exception when no_data_found then
            v_executa_trigger := 0;
   end;

   if v_executa_trigger = 1
   then
      v_nivel := :old.cd_it_pe_nivel99;
      v_grupo := :old.cd_it_pe_grupo;
      v_subgrupo := :old.cd_it_pe_subgrupo;
      v_item := :old.cd_it_pe_item;

      -- DADOS DO LOGIN DO USUARIO
      inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

      -- Alteracao do item do pedido, quantidade vendida do item e codigo de cancelamento.
      if updating
      then
         -- Alteracao do produto
         if :old.cd_it_pe_nivel99  <> :new.cd_it_pe_nivel99
         or :old.cd_it_pe_grupo    <> :new.cd_it_pe_grupo
         or :old.cd_it_pe_subgrupo <> :new.cd_it_pe_subgrupo
         or :old.cd_it_pe_item     <> :new.cd_it_pe_item
         then
            inter_pr_insere_motivo_plan(
                  v_ordem_planej,                         v_pedido_venda,
                  v_nivel,                                v_grupo,
                  v_subgrupo,                             v_item,
                  '0',                                    '00000',
                  --Pendente                              Pedido
                  0,                                      1,
                  -- PEDIDO DE VENDA ALTERADO
                  inter_fn_buscar_tag_composta('lb34587', '','','','','','','','','','', ws_locale_usuario,ws_usuario_systextil),

                  -- Item {0} do pedido de venda {1} alterado para o item {2}.
                  inter_fn_buscar_tag_composta('lb33048', :old.cd_it_pe_nivel99  || '.' ||
                                                          :old.cd_it_pe_grupo    || '.' ||
                                                          :old.cd_it_pe_subgrupo || '.' ||
                                                          :old.cd_it_pe_item,
                                                          to_char(:old.pedido_venda,'000000'),
                                                          :new.cd_it_pe_nivel99  || '.' ||
                                                          :new.cd_it_pe_grupo    || '.' ||
                                                          :new.cd_it_pe_subgrupo || '.' ||
                                                          :new.cd_it_pe_item,
                                                          '','','','','','','', ws_locale_usuario,ws_usuario_systextil));
         end if;

         -- Alteracao da quantidade pedida
         if :old.qtde_pedida <> :new.qtde_pedida
         then
            inter_pr_insere_motivo_plan(
                  v_ordem_planej,                         v_pedido_venda,
                  v_nivel,                                v_grupo,
                  v_subgrupo,                             v_item,
                  '0',                                    '00000',
                  --Pendente                              Pedido
                  0,                                      1,
                  -- PEDIDO DE VENDA ALTERADO
                  inter_fn_buscar_tag_composta('lb34587', '','','','','','','','','','', ws_locale_usuario,ws_usuario_systextil),

                    -- Quantidade pedida do item {0} alterada de {1} para {2}.
                    inter_fn_buscar_tag_composta('lb33049',:old.cd_it_pe_nivel99   || '.' ||
                                                            :old.cd_it_pe_grupo    || '.' ||
                                                            :old.cd_it_pe_subgrupo || '.' ||
                                                            :old.cd_it_pe_item,
                                                            to_char(:old.qtde_pedida),
                                                            to_char(:new.qtde_pedida),
                                                            '','','','','','','', ws_locale_usuario,ws_usuario_systextil));
         end if;

         if :old.cod_cancelamento <> :new.cod_cancelamento
         then
            inter_pr_insere_motivo_plan(
                  v_ordem_planej,                         v_pedido_venda,
                  v_nivel,                                v_grupo,
                  v_subgrupo,                             v_item,
                  '0',                                    '00000',
                  --Pendente                              Pedido
                  0,                                      1,
                  -- PEDIDO DE VENDA ALTERADO
                  inter_fn_buscar_tag_composta('lb34587', '','','','','','','','','','', ws_locale_usuario,ws_usuario_systextil),

                    -- Item {0}, na sequencia {1}, cancelado ou eliminado.
                    inter_fn_buscar_tag_composta('lb33050',:old.cd_it_pe_nivel99  || '.' ||
                                                           :old.cd_it_pe_grupo    || '.' ||
                                                           :old.cd_it_pe_subgrupo || '.' ||
                                                           :old.cd_it_pe_item,
                                                           to_char(:old.seq_item_pedido),
                                                           '','','','','','','','', ws_locale_usuario,ws_usuario_systextil));
         end if;
      end if;

      -- Inclusao de um item do pedido.
      if inserting
      then
         v_nivel    := :new.cd_it_pe_nivel99;
         v_grupo    := :new.cd_it_pe_grupo;
         v_subgrupo := :new.cd_it_pe_subgrupo;
         v_item     := :new.cd_it_pe_item;

         inter_pr_insere_motivo_plan(
                  v_ordem_planej,                         v_pedido_venda,
                  v_nivel,                                v_grupo,
                  v_subgrupo,                             v_item,
                  '0',                                    '00000',
                  --Pendente                              Pedido
                  0,                                      1,
                  -- PEDIDO DE VENDA ALTERADO
                  inter_fn_buscar_tag_composta('lb34587', '','','','','','','','','','', ws_locale_usuario,ws_usuario_systextil),

                 -- Novo item: {0}.
                 inter_fn_buscar_tag_composta('lb33051',:new.cd_it_pe_nivel99  || '.' ||
                                                        :new.cd_it_pe_grupo    || '.' ||
                                                        :new.cd_it_pe_subgrupo || '.' ||
                                                        :new.cd_it_pe_item,
                                                        '','','','','','','','','', ws_locale_usuario,ws_usuario_systextil));
      end if;

      -- Eliminacao de um item do pedido.
      if  deleting
      then
         inter_pr_insere_motivo_plan(
                  v_ordem_planej,                         v_pedido_venda,
                  v_nivel,                                v_grupo,
                  v_subgrupo,                             v_item,
                  '0',                                    '00000',
                  --Pendente                              Pedido
                  0,                                      1,
                  -- PEDIDO DE VENDA ALTERADO
                  inter_fn_buscar_tag_composta('lb34587', '','','','','','','','','','', ws_locale_usuario,ws_usuario_systextil),

                 -- Item {0}, na sequencia {1}, cancelado ou eliminado.
                 inter_fn_buscar_tag_composta('lb33050',:old.cd_it_pe_nivel99  || '.' ||
                                                        :old.cd_it_pe_grupo    || '.' ||
                                                        :old.cd_it_pe_subgrupo || '.' ||
                                                        :old.cd_it_pe_item,
                                                        to_char(:old.seq_item_pedido),
                                                        '','','','','','','','', ws_locale_usuario,ws_usuario_systextil));
      end if;
   end if;
end inter_tr_plan_mov_pedi_110;

-- ALTER TRIGGER "INTER_TR_PLAN_MOV_PEDI_110" ENABLE
 

/

exec inter_pr_recompile;

