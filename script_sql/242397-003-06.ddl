-- Create table 
drop table PEDI_155_LOG;
create table PEDI_155_LOG  
( 
  TIPO_OCORR                VARCHAR2(1) default '' null,
  DATA_OCORR                DATE null,
  HORA_OCORR                DATE null,
  USUARIO_REDE              VARCHAR2(20) default '' null,
  MAQUINA_REDE              VARCHAR2(40) default '' null,
  APLICACAO                 VARCHAR2(20) default '' null,
  USUARIO_SISTEMA           VARCHAR2(20) default '' null,
  NOME_PROGRAMA             VARCHAR2(20) default '' null,
  metros_de_OLD                     NUMBER(9,3) ,  
  metros_de_NEW                     NUMBER(9,3) , 
  metros_ate_OLD                    NUMBER(9,3) ,  
  metros_ate_NEW                    NUMBER(9,3) , 
  qtde_nuances_OLD                  NUMBER ,  
  qtde_nuances_NEW                  NUMBER , 
  id_OLD                            NUMBER(9),  
  id_NEW                            NUMBER(9), 
  qtde_prmtda_nuance_minima_OLD  NUMBER(9) default 1,  
  qtde_prmtda_nuance_minima_NEW  NUMBER(9) default 1, 
  qtde_minima_metros_nuance_OLD     NUMBER(12,3) default 500,  
  qtde_minima_metros_nuance_NEW     NUMBER(12,3) default 500, 
  qtde_tolerancia_mts_nuance_OLD NUMBER(9,2) default 10,  
  qtde_tolerancia_mts_nuance_NEW NUMBER(9,2) default 10, 
  qtde_maxima_de_ordens_OLD         NUMBER(9) default 3,  
  qtde_maxima_de_ordens_NEW         NUMBER(9) default 3, 
  qtde_minima_metros_ordem_OLD      NUMBER(12,3) default 500, 
  qtde_minima_metros_ordem_NEW      NUMBER(12,3) default 500 
) ;

