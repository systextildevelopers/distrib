create or replace trigger wms_tr_rfid_tags_log
  before insert
  or delete
  or update of codigo_unico_tag, rfid_caixa, periodo_tag, ordem_prod_tag, 
	ordem_conf_tag, sequencia_tag, liberar_tag_st 
  on inte_wms_rfid_tags
  for each row
declare
  -- local variables here
  v_rfid_caixa                     inte_wms_rfid_tags.rfid_caixa                   %type;
  v_periodo_tag_new                inte_wms_rfid_tags.periodo_tag                  %type;
  v_periodo_tag_old                inte_wms_rfid_tags.periodo_tag                  %type;
  v_ordem_prod_tag_new             inte_wms_rfid_tags.ordem_prod_tag               %type;
  v_ordem_prod_tag_old             inte_wms_rfid_tags.ordem_prod_tag               %type;
  v_ordem_conf_tag_new             inte_wms_rfid_tags.ordem_conf_tag               %type;
  v_ordem_conf_tag_old             inte_wms_rfid_tags.ordem_conf_tag               %type;
  v_sequencia_tag_new              inte_wms_rfid_tags.sequencia_tag                %type;
  v_sequencia_tag_old              inte_wms_rfid_tags.sequencia_tag                %type;
  v_liberar_tag_st_new             inte_wms_rfid_tags.liberar_tag_st               %type;
  v_liberar_tag_st_old             inte_wms_rfid_tags.liberar_tag_st               %type;

  v_codigo_unico_tag_new           inte_wms_rfid_tags.codigo_unico_tag             %type;
  v_codigo_unico_tag_old           inte_wms_rfid_tags.codigo_unico_tag             %type;

  v_sid                            number(9);
  v_empresa                        number(3);
  v_usuario_systextil              varchar2(250);
  v_locale_usuario                 varchar2(5);
  v_nome_programa                  varchar2(20);
 

  v_operacao                       varchar(1);
  v_data_operacao                  date;
  v_usuario_rede                   varchar(20);
  v_maquina_rede                   varchar(40);
  v_aplicativo                     varchar(20);
begin

   -- grava a data/hora da insercao do registro (log)
   v_data_operacao := sysdate();

   --alimenta as variaveis new caso seja insert ou update
   if inserting or updating
   then
      if inserting
      then v_operacao := 'i';
      else v_operacao := 'u';
      end if;

      v_rfid_caixa           := :new.rfid_caixa;
      v_periodo_tag_new      := :new.periodo_tag;
      v_ordem_prod_tag_new   := :new.ordem_prod_tag;
      v_ordem_conf_tag_new   := :new.ordem_conf_tag;
      v_sequencia_tag_new    := :new.sequencia_tag;
      v_liberar_tag_st_new   := :new.liberar_tag_st;
      v_codigo_unico_tag_new := :new.codigo_unico_tag;

   end if; --fim do if inserting or updating

   --alimenta as variaveis old caso seja insert ou update
   if deleting or updating
   then
      if deleting
      then
         v_operacao      := 'd';
      else
         v_operacao      := 'u';
      end if;

      v_rfid_caixa           := :old.rfid_caixa;
      v_periodo_tag_old      := :old.periodo_tag;
      v_ordem_prod_tag_old   := :old.ordem_prod_tag;
      v_ordem_conf_tag_old   := :old.ordem_conf_tag;
      v_sequencia_tag_old    := :old.sequencia_tag;
      v_liberar_tag_st_old   := :old.liberar_tag_st;
      v_codigo_unico_tag_old := :old.codigo_unico_tag;

   end if; --fim do if deleting or updating


   -- Dados do usu�rio logado
   inter_pr_dados_usu_inte (v_usuario_rede,        v_maquina_rede,   v_aplicativo,     v_sid,
                            v_usuario_systextil,   v_empresa,        v_locale_usuario);


    v_nome_programa := ''; --Deixado de fora por quest�es de performance, a pedido do cliente

   --insere na inte_wms_rfid_log o registro.
   insert into inte_wms_rfid_tags_log (
      rfid_caixa,            
      periodo_tag_new,         periodo_tag_old,
      ordem_prod_tag_new,      ordem_prod_tag_old,
      ordem_conf_tag_new,      ordem_conf_tag_old,
      sequencia_tag_new,       sequencia_tag_old,
      liberar_tag_st_new,      liberar_tag_st_old, 
      codigo_unico_tag_new,    codigo_unico_tag_old,
      operacao,
      data_operacao,           usuario_rede,
      maquina_rede,            aplicativo,
      nome_programa
   )
   values (
      v_rfid_caixa,            
      v_periodo_tag_new,       v_periodo_tag_old,
      v_ordem_prod_tag_new,    v_ordem_prod_tag_old,
      v_ordem_conf_tag_new,    v_ordem_conf_tag_old,
      v_sequencia_tag_new,     v_sequencia_tag_old,
      v_liberar_tag_st_new,    v_liberar_tag_st_old,   
      v_codigo_unico_tag_new,  v_codigo_unico_tag_old,
      v_operacao,
      v_data_operacao,         v_usuario_rede,
      v_maquina_rede,          v_aplicativo,
      v_nome_programa
   );

end wms_tr_rfid_tags_log;
/

execute inter_pr_recompile;

/* versao: 3 */


 exit;


 exit;
