
  CREATE OR REPLACE FUNCTION "INTER_FN_CALC_MOD_11" (numero number)
return VARCHAR2 is
   aux number(3);
begin
   aux := mod((to_number(SUBSTR(numero,1,1))*3 +
   to_number(SUBSTR(numero,2,1))*2 +
   to_number(SUBSTR(numero,3,1))*9 +
   to_number(SUBSTR(numero,4,1))*8 +
   to_number(SUBSTR(numero,5,1))*7 +
   to_number(SUBSTR(numero,6,1))*6 +
   to_number(SUBSTR(numero,7,1))*5 +
   to_number(SUBSTR(numero,8,1))*4 +
   to_number(SUBSTR(numero,9,1))*3 +
   to_number(SUBSTR(numero,10,1))*2)*10,11);

   return nvl(aux,0);

end;
 

/

exec inter_pr_recompile;

