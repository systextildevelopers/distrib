CREATE OR REPLACE PROCEDURE "INTER_PR_EQUALIZA_ESTOQUE" (p_empresa in number,   p_nivel    in varchar2,
                                                      p_grupo   in varchar2, p_subgrupo in varchar2,
                                                      p_item    in varchar2, p_data_inicial in date,
                                                      p_estq_300_estq_310 in varchar2)
is
    v_periodo_estoque     empr_001.periodo_estoque%type;
    v_valor_acerto        number;
    v_entrada_saida       varchar2(1);
    v_transacao           number;
    v_data_acerto         date;
    v_saldo_fisico        number;
    v_sequencia_ficha     number;
    v_executa_equalizacao empr_002.executa_equalizacao%type;
    v_tran_e_equalizacao  empr_002.tran_e_equalizacao%type;
    v_tran_s_equalizacao  empr_002.tran_s_equalizacao%type;
    v_apuracao_ir         fatu_503.apuracao_ir%type;
    v_centro_custo        basi_185.centro_custo%type;

begin
    v_periodo_estoque := p_data_inicial;

    select empr_002.executa_equalizacao,
           empr_002.tran_e_equalizacao,
           empr_002.tran_s_equalizacao
    into   v_executa_equalizacao,
           v_tran_e_equalizacao,
           v_tran_s_equalizacao
    from empr_002;

    begin
       select fatu_503.apuracao_ir
       into   v_apuracao_ir
       from fatu_503
       where fatu_503.codigo_empresa = p_empresa;

    exception
       when others then
          v_apuracao_ir := 1;
    end;

    if v_executa_equalizacao = 1
    then
        for estq301 in (select a.nivel_estrutura,    a.grupo_estrutura,
                               a.subgrupo_estrutura, a.item_estrutura,
                               avg(a.preco_medio_unitario) preco_medio_unitario
                        from estq_301 a
            where a.codigo_deposito   in (select basi_205.codigo_deposito from basi_205
                                                      where basi_205.controla_ficha_cardex = 1
                                                      and  (basi_205.tipo_valorizacao      = 1 or v_apuracao_ir = 1)                                         
                                                      and  (basi_205.local_deposito        = p_empresa
                                                      or    basi_205.local_deposito in (select fatu_500.codigo_empresa
                                                                                        from fatu_500
                                                                                        where fatu_500.codigo_matriz  = p_empresa
                                                                                          and (select empr_008.val_int 
                                                                                               from empr_008 
                                                                                               where empr_008.codigo_empresa = p_empresa
                                                                                                 and empr_008.param = 'estq.formaCalcCardex') = 3)))
                          and a.nivel_estrutura    = p_nivel
                          and a.grupo_estrutura    = p_grupo
                          and a.subgrupo_estrutura = p_subgrupo
                          and a.item_estrutura     = p_item
                          and a.mes_ano_movimento  = v_periodo_estoque
                          and a.saldo_financeiro <> a.saldo_fisico * a.preco_medio_unitario
                        group by a.nivel_estrutura,    a.grupo_estrutura,
                                 a.subgrupo_estrutura, a.item_estrutura)
        loop
            for reg_estq301 in (select * from estq_301 b
                                where b.nivel_estrutura    = estq301.nivel_estrutura
                                  and b.grupo_estrutura    = estq301.grupo_estrutura
                                  and b.subgrupo_estrutura = estq301.subgrupo_estrutura
                                  and b.item_estrutura     = estq301.item_estrutura
                                  and b.mes_ano_movimento in (select max(d.mes_ano_movimento) from estq_301 d
                                                              where b.nivel_estrutura    = d.nivel_estrutura
                                                                and b.grupo_estrutura    = d.grupo_estrutura
                                                                and b.subgrupo_estrutura = d.subgrupo_estrutura
                                                                and b.item_estrutura     = d.item_estrutura
                                                                and b.codigo_deposito    = d.codigo_deposito
                                                                and d.mes_ano_movimento <= v_periodo_estoque)
                                  and b.codigo_deposito   in (select basi_205.codigo_deposito from basi_205
                                                              where basi_205.controla_ficha_cardex = 1
                                                              and  (basi_205.tipo_valorizacao      = 1 or v_apuracao_ir = 1)                                         
                                                              and  (basi_205.local_deposito        = p_empresa
                                                              or    basi_205.local_deposito in (select fatu_500.codigo_empresa
                                                                                                from fatu_500
                                                                                                where fatu_500.codigo_matriz  = p_empresa
                                                                                                  and (select empr_008.val_int 
                                                                                                       from empr_008 
                                                                                                       where empr_008.codigo_empresa = p_empresa
                                                                                                         and empr_008.param = 'estq.formaCalcCardex') = 3))))
            loop
                v_valor_acerto := round(reg_estq301.saldo_fisico * estq301.preco_medio_unitario,5) - round(reg_estq301.saldo_financeiro,5);

                if round(v_valor_acerto,5) > 0
                then
                   v_entrada_saida   := 'E';
                   v_transacao       := v_tran_e_equalizacao;
                   v_data_acerto     := Trunc(v_periodo_estoque,'MM');
                   v_sequencia_ficha := 1;

                   begin
                      select c.saldo_fisico
                      into   v_saldo_fisico
                      from estq_301 c
                      where c.codigo_deposito    = reg_estq301.codigo_deposito
                        and c.nivel_estrutura    = reg_estq301.nivel_estrutura
                        and c.grupo_estrutura    = reg_estq301.grupo_estrutura
                        and c.subgrupo_estrutura = reg_estq301.subgrupo_estrutura
                        and c.item_estrutura     = reg_estq301.item_estrutura
                        and c.mes_ano_movimento in (select max(e.mes_ano_movimento) from estq_301 e
                                                    where c.nivel_estrutura    = e.nivel_estrutura
                                                      and c.grupo_estrutura    = e.grupo_estrutura
                                                      and c.subgrupo_estrutura = e.subgrupo_estrutura
                                                      and c.item_estrutura     = e.item_estrutura
                                                      and c.codigo_deposito    = e.codigo_deposito
                                                      and e.mes_ano_movimento  < v_periodo_estoque);
                   exception
                     when no_data_found then
                       v_saldo_fisico := 0.00;
                   end;

                else
                   v_valor_acerto    := -v_valor_acerto;
                   v_entrada_saida   := 'S';
                   v_transacao       := v_tran_s_equalizacao;
                   v_data_acerto     := Last_day(v_periodo_estoque);
                   v_sequencia_ficha := 9999;
                   v_saldo_fisico    := reg_estq301.saldo_fisico;
                end if;

                if round(v_valor_acerto,5) <> 0.00
                then
                    begin
                       select basi_205.centro_custo_equalizacao
           into   v_centro_custo
           from   basi_205
           where  basi_205.codigo_deposito = reg_estq301.codigo_deposito;
           exception
              when no_data_found then
               v_centro_custo := 0;
                    end;

                    if lower(p_estq_300_estq_310) = 'estq_300'
                    then
                        insert into estq_300
                          (codigo_deposito,               nivel_estrutura,                  grupo_estrutura,
                           subgrupo_estrutura,            item_estrutura,                   data_movimento,
                           sequencia_ficha,               sequencia_insercao,               numero_documento,
                           codigo_transacao,              entrada_saida,                    centro_custo,
                           quantidade,                    saldo_fisico,                     valor_movimento_unitario,
                           valor_contabil_unitario,       preco_medio_unitario,             saldo_financeiro,
                           usuario_systextil,             processo_systextil,               data_insercao,
                           tabela_origem,                 valor_movimento_unitario_proj,    valor_contabil_unitario_proj,
                           preco_medio_unitario_proj,     saldo_financeiro_proj,            valor_movto_unit_estimado,
                           preco_medio_unit_estimado,     saldo_financeiro_estimado,        valor_total)
                        values
                          (reg_estq301.codigo_deposito,   reg_estq301.nivel_estrutura,      reg_estq301.grupo_estrutura,
                           reg_estq301.subgrupo_estrutura,reg_estq301.item_estrutura,       v_data_acerto,
                           v_sequencia_ficha,             9999,                             to_char(Last_day(v_periodo_estoque),'DDMMYY'),
                           v_transacao,                   v_entrada_saida,                  v_centro_custo,
                           0.00,                          v_saldo_fisico,                   v_valor_acerto,
                           v_valor_acerto,                estq301.preco_medio_unitario,     v_saldo_fisico * estq301.preco_medio_unitario,
                           'SYSTEXTIL',                   'EQUALIZACAO',                    SYSDATE,
                           'ESTQ_300',                    v_valor_acerto,                   v_valor_acerto,
                           v_valor_acerto,                v_saldo_fisico * estq301.preco_medio_unitario, v_valor_acerto,
                           v_valor_acerto,                v_saldo_fisico * estq301.preco_medio_unitario, v_valor_acerto);

                        begin
                           update estq_300 a
                           set a.saldo_financeiro          = a.saldo_financeiro          + v_valor_acerto,
                               a.saldo_financeiro_proj     = a.saldo_financeiro_proj     + v_valor_acerto,
                               a.saldo_financeiro_estimado = a.saldo_financeiro_estimado + v_valor_acerto
                           where  a.codigo_deposito    = reg_estq301.codigo_deposito
                             and  a.nivel_estrutura    = reg_estq301.nivel_estrutura
                             and  a.grupo_estrutura    = reg_estq301.grupo_estrutura
                             and  a.subgrupo_estrutura = reg_estq301.subgrupo_estrutura
                             and  a.item_estrutura     = reg_estq301.item_estrutura
                             and  a.data_movimento     between v_data_acerto and Last_day(v_periodo_estoque)
                             and  a.sequencia_ficha    > v_sequencia_ficha
                             and (a.processo_systextil <> 'EQUALIZACAO'
                              or  a.processo_systextil is null);
                        end;
                    else
                        insert into estq_310
                          (codigo_deposito,               nivel_estrutura,                  grupo_estrutura,
                           subgrupo_estrutura,            item_estrutura,                   data_movimento,
                           sequencia_ficha,               sequencia_insercao,               numero_documento,
                           codigo_transacao,              entrada_saida,                    centro_custo,
                           quantidade,                    saldo_fisico,                     valor_movimento_unitario,
                           valor_contabil_unitario,       preco_medio_unitario,             saldo_financeiro,
                           usuario_systextil,             processo_systextil,               data_insercao,
                           tabela_origem,                 valor_movimento_unitario_proj,    valor_contabil_unitario_proj,
                           preco_medio_unitario_proj,     saldo_financeiro_proj,            valor_movto_unit_estimado,
                           preco_medio_unit_estimado,     saldo_financeiro_estimado,        valor_total)
                        values
                          (reg_estq301.codigo_deposito,   reg_estq301.nivel_estrutura,      reg_estq301.grupo_estrutura,
                           reg_estq301.subgrupo_estrutura,reg_estq301.item_estrutura,       v_data_acerto,
                           v_sequencia_ficha,             9999,                             to_char(Last_day(v_periodo_estoque),'DDMMYY'),
                           v_transacao,                   v_entrada_saida,                  v_centro_custo,
                           0.00,                          v_saldo_fisico,                   v_valor_acerto,
                           v_valor_acerto,                estq301.preco_medio_unitario,     v_saldo_fisico * estq301.preco_medio_unitario,
                           'SYSTEXTIL',                   'EQUALIZACAO',                    SYSDATE,
                           'ESTQ_300',                    v_valor_acerto,                   v_valor_acerto,
                           v_valor_acerto,                v_saldo_fisico * estq301.preco_medio_unitario, v_valor_acerto,
                           v_valor_acerto,                v_saldo_fisico * estq301.preco_medio_unitario, v_valor_acerto);

                        begin
                           update estq_310 a
                           set a.saldo_financeiro          = a.saldo_financeiro          + v_valor_acerto,
                               a.saldo_financeiro_proj     = a.saldo_financeiro_proj     + v_valor_acerto,
                               a.saldo_financeiro_estimado = a.saldo_financeiro_estimado + v_valor_acerto
                           where  a.codigo_deposito    = reg_estq301.codigo_deposito
                             and  a.nivel_estrutura    = reg_estq301.nivel_estrutura
                             and  a.grupo_estrutura    = reg_estq301.grupo_estrutura
                             and  a.subgrupo_estrutura = reg_estq301.subgrupo_estrutura
                             and  a.item_estrutura     = reg_estq301.item_estrutura
                             and  a.data_movimento     between v_data_acerto and Last_day(v_periodo_estoque)
                             and  a.sequencia_ficha    > v_sequencia_ficha
                             and (a.processo_systextil <> 'EQUALIZACAO'
                              or  a.processo_systextil is null);
                        end;
                    end if;

                    begin

                       insert into estq_301
                         (codigo_deposito,            nivel_estrutura,                  grupo_estrutura,
                          subgrupo_estrutura,         item_estrutura,                   mes_movimento,
                          ano_movimento,              mes_ano_movimento,                saldo_fisico,
                          saldo_financeiro,           preco_medio_unitario,             preco_custo_unitario,
                          saldo_financeiro_estimado,  preco_medio_unit_estimado,        preco_custo_unit_estimado,
                          saldo_financeiro_proj,      preco_medio_unit_proj,            preco_custo_unit_proj)
                       values
                         (reg_estq301.codigo_deposito,                             reg_estq301.nivel_estrutura,  reg_estq301.grupo_estrutura,
                          reg_estq301.subgrupo_estrutura,                          reg_estq301.item_estrutura,   to_char(v_periodo_estoque,'MM'),
                          to_char(v_periodo_estoque,'YYYY'),                       v_periodo_estoque,            reg_estq301.saldo_fisico,
                          reg_estq301.saldo_fisico * estq301.preco_medio_unitario, estq301.preco_medio_unitario, estq301.preco_medio_unitario,
                          reg_estq301.saldo_fisico * estq301.preco_medio_unitario, estq301.preco_medio_unitario, estq301.preco_medio_unitario,
                          reg_estq301.saldo_fisico * estq301.preco_medio_unitario, estq301.preco_medio_unitario, estq301.preco_medio_unitario);

                    exception
                       when others then

                          update estq_301
                          set saldo_fisico              = reg_estq301.saldo_fisico,
                              saldo_financeiro          = reg_estq301.saldo_fisico * estq301.preco_medio_unitario,
                              preco_medio_unitario      = estq301.preco_medio_unitario,
                              preco_custo_unitario      = estq301.preco_medio_unitario,
                              saldo_financeiro_estimado = reg_estq301.saldo_fisico * estq301.preco_medio_unitario,
                              preco_medio_unit_estimado = estq301.preco_medio_unitario,
                              preco_custo_unit_estimado = estq301.preco_medio_unitario,
                              saldo_financeiro_proj     = reg_estq301.saldo_fisico * estq301.preco_medio_unitario,
                              preco_medio_unit_proj     = estq301.preco_medio_unitario,
                              preco_custo_unit_proj     = estq301.preco_medio_unitario
                        where codigo_deposito    = reg_estq301.codigo_deposito
                          and nivel_estrutura    = reg_estq301.nivel_estrutura
                          and grupo_estrutura    = reg_estq301.grupo_estrutura
                          and subgrupo_estrutura = reg_estq301.subgrupo_estrutura
                          and item_estrutura     = reg_estq301.item_estrutura
                          and mes_movimento      = to_char(v_periodo_estoque,'MM')
                          and ano_movimento      = to_char(v_periodo_estoque,'YYYY');
                    end;

                    commit;
                end if;
            end loop;
        end loop;
    end if;
end inter_pr_equaliza_estoque;


/

exec inter_pr_recompile;
/
