alter table fatu_075
add nr_mtv_abatimento number(2) default 0;

comment on COLUMN fatu_075.nr_mtv_abatimento is 'Motivo de abatimento na baixa de t�tulos';

exec inter_pr_recompile;
