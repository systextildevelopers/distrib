
  CREATE OR REPLACE TRIGGER "INTER_TR_PEDI_838_LOG" 
   after insert
   or delete
   or update of
	   pedido_venda, variante, data_entrega, qtde_metro,
	   qtde_unidade, nivel, grupo, sub,
	   item, seq_entrega
   on pedi_838
   for each row

declare
   ws_usuario_rede          varchar2(20);
   ws_maquina_rede          varchar2(40);
   ws_aplicativo            varchar2(20);
   long_aux                 varchar2(2000);
begin

    if inserting
    then


       select substr(osuser,1,20), substr(machine,1,40), substr(program,1,20)
       into   ws_usuario_rede,     ws_maquina_rede,       ws_aplicativo
       from sys.gv_$session
       where audsid  = userenv('SESSIONID')
         and inst_id = userenv('INSTANCE')
         and rownum < 2;


       INSERT INTO hist_100
          ( tabela,            operacao,
            data_ocorr,        aplicacao,
            usuario_rede,      maquina_rede,
            num01,             long01
          )
       VALUES
          ( 'PEDI_838',        'I',
            sysdate,           ws_aplicativo,
            ws_usuario_rede,   ws_maquina_rede,
            :new.pedido_venda,
            '                              PEDIDO DE VENDAS - ENTREGAS'||
                                     chr(10)                  ||
                                     chr(10)                  ||
            'PEDIDO VENDA.....: ' || :new.pedido_venda        ||
            chr(10)                                           ||
            'VARIANTE.........: ' || :new.variante            ||
            chr(10)                                           ||
            'DATA ENTREGA.....: ' || :new.data_entrega        ||
            chr(10)                                           ||
            'QTDE METRO.......: ' || :new.qtde_metro          ||
            chr(10)                                           ||
            'QTDE UNIDADE.....: ' || :new.qtde_unidade        ||
            chr(10)                                           ||
            'NIVEL............: ' || :new.nivel               ||
            chr(10)                                           ||
            'GRUPO............: ' || :new.grupo               ||
            chr(10)                                           ||
            'SUB..............: ' || :new.sub                 ||
            chr(10)                                           ||
            'ITEM.............: ' || :new.item                ||
            chr(10)                                           ||
            'SEQ ENTREGA......: ' || :new.seq_entrega
         );
    end if;

    if updating and
       (:old.pedido_venda   <> :new.pedido_venda  or
        :old.variante       <> :new.variante      or
        :old.data_entrega   <> :new.data_entrega  or
        :old.qtde_metro     <> :new.qtde_metro    or
        :old.qtde_unidade   <> :new.qtde_unidade  or
        :old.nivel          <> :new.nivel         or
        :old.grupo          <> :new.grupo         or
        :old.sub            <> :new.sub           or
        :old.item           <> :new.item          or
        :old.seq_entrega    <> :new.seq_entrega
      )
    then
       select substr(osuser,1,20), substr(machine,1,40), substr(program,1,20)
       into   ws_usuario_rede,     ws_maquina_rede,       ws_aplicativo
       from sys.gv_$session
       where audsid  = userenv('SESSIONID')
         and inst_id = userenv('INSTANCE')
         and rownum < 2;

       long_aux := long_aux ||
                  '                              PEDIDO DE VENDAS - ENTREGAS'||
                   chr(10)                    ||
                   chr(10);

       if  :old.pedido_venda <> :new.pedido_venda
       then
           long_aux := long_aux ||
                'PEDIDO VENDA....: ' || :old.pedido_venda    || ' - '
                                     || :new.pedido_venda
                                     || chr(10);
       end if;

       if  :old.variante   <> :new.variante
       then
           long_aux := long_aux ||
                'VARIANTE........: ' || :old.variante      || ' - '
                                     || :new.variante
                                     || chr(10);
       end if;

       if  :old.data_entrega   <> :new.data_entrega
       then
           long_aux := long_aux ||
                'DATA ENTREGA....: ' || :old.data_entrega      || ' - '
                                     || :new.data_entrega
                                     || chr(10);
       end if;

       if  :old.qtde_metro   <> :new.qtde_metro
       then
           long_aux := long_aux ||
                'QTDE METRO......: ' || :old.qtde_metro      || ' - '
                                     || :new.qtde_metro
                                     || chr(10);
       end if;

       if  :old.qtde_unidade   <> :new.qtde_unidade
       then
           long_aux := long_aux ||
                'QTDE UNIDADE....: ' || :old.qtde_unidade      || ' - '
                                     || :new.qtde_unidade
                                     || chr(10);
       end if;

       if  :old.nivel   <> :new.nivel
       then
           long_aux := long_aux ||
                'NIVEL...........: ' || :old.nivel      || ' - '
                                     || :new.nivel
                                     || chr(10);
       end if;

       if  :old.grupo   <> :new.grupo
       then
           long_aux := long_aux ||
                'GRUPO...........: ' || :old.grupo      || ' - '
                                     || :new.grupo
                                     || chr(10);
       end if;

       if  :old.sub   <> :new.sub
       then
           long_aux := long_aux ||
                'SUB.............: ' || :old.sub      || ' - '
                                     || :new.sub
                                     || chr(10);
       end if;

       if  :old.item   <> :new.item
       then
           long_aux := long_aux ||
                'ITEM............: ' || :old.item      || ' - '
                                     || :new.item
                                     || chr(10);
       end if;

       if  :old.seq_entrega   <> :new.seq_entrega
       then
           long_aux := long_aux ||
                'SEQ ENTREGA.....: ' || :old.seq_entrega      || ' - '
                                     || :new.seq_entrega
                                     || chr(10);
       end if;

       INSERT INTO hist_100
          ( tabela,            operacao,
            data_ocorr,        aplicacao,
            usuario_rede,      maquina_rede,
            num01,             long01
          )
       VALUES
          ( 'PEDI_838',        'A',
            sysdate,           ws_aplicativo,
            ws_usuario_rede,   ws_maquina_rede,
            :new.pedido_venda, long_aux
         );
    end if;

    if deleting
    then


       select substr(osuser,1,20), substr(machine,1,40), substr(program,1,20)
       into   ws_usuario_rede,     ws_maquina_rede,       ws_aplicativo
       from sys.gv_$session
       where audsid  = userenv('SESSIONID')
         and inst_id = userenv('INSTANCE')
         and rownum < 2;


       INSERT INTO hist_100
          ( tabela,            operacao,
            data_ocorr,        aplicacao,
            usuario_rede,      maquina_rede,
            num01
          )
       VALUES
          ( 'PEDI_838',        'D',
            sysdate,           ws_aplicativo,
            ws_usuario_rede,   ws_maquina_rede,
            :old.pedido_venda
         );
    end if;

end inter_tr_pedi_838_log;

-- ALTER TRIGGER "INTER_TR_PEDI_838_LOG" ENABLE
 

/

exec inter_pr_recompile;

