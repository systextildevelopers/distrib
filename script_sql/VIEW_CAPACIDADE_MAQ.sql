CREATE OR REPLACE VIEW "VIEW_CAPACIDADE_MAQ" ("GRUPO_MAQ", "SUBGRU_MAQ", "NUMERO_MAQUINA", "NIVEL_ESTRUTURA", "GRUPO_ESTRUTURA", "SUBGRU_ESTRUTURA", "ITEM_ESTRUTURA", "TEMPO_PRODUCAO", "CAPAC_PRODUCAO", "FINURA", "PRODUCAO_DIA", "NUMERO_AGULHAS", "PLATINA1", "CODIGO_ROTEIRO", "VELOCIDADE", "ALIMENTADORES", "SAIDA_TECIDO", "DIAMETRO", "PISTA_DISCO1", "PISTA_CILINDRO1", "CAMOS_DISCO1", "CAMOS_DISCO2", "CAMOS_CILINDRO1", "CAMOS_CILINDRO2", "CAMOS_CILINDRO3", "CAMOS_CILINDRO4", "CAMOS_CILINDRO5") AS
  select  mqop_060.maq_sbgr_grupo_mq as grupo_maq, mqop_060.maq_sbgr_sbgr_maq as subgru_maq, mqop_060.NUMERO_MAQUINA
      , basi_010.nivel_estrutura, basi_010.grupo_estrutura, basi_010.subgru_estrutura, basi_010.item_estrutura
      , mqop_060.tempo_producao, mqop_060.capac_producao, mqop_060.finura, mqop_060.PRODUCAO_DIA
      , mqop_060.NUMERO_AGULHAS, mqop_060.PLATINA1, mqop_060.CODIGO_ROTEIRO, MQOP_060.VELOCIDADE
      , mqop_060.ALIMENTADORES, mqop_060.SAIDA_TECIDO, mqop_060.DIAMETRO
      , mqop_060.PISTA_DISCO1, mqop_060.PISTA_CILINDRO1
      , mqop_060.CAMOS_DISCO1, mqop_060.CAMOS_DISCO2
      , mqop_060.camos_cilindro1, mqop_060.camos_cilindro2, mqop_060.camos_cilindro3, mqop_060.camos_cilindro4, mqop_060.camos_cilindro5

from mqop_060, basi_010
where mqop_060.nivel_estrutura  = basi_010.nivel_estrutura
  and mqop_060.grupo_estrutura  = basi_010.grupo_estrutura
  and mqop_060.subgru_estrutura = basi_010.subgru_estrutura
  and mqop_060.item_estrutura   = basi_010.item_estrutura


union

select
        mqop_060.maq_sbgr_grupo_mq as grupo_maq, mqop_060.maq_sbgr_sbgr_maq as subgru_maq, NUMERO_MAQUINA
      , basi_010.nivel_estrutura, basi_010.grupo_estrutura, basi_010.subgru_estrutura, basi_010.item_estrutura
      , mqop_060.tempo_producao, mqop_060.capac_producao, mqop_060.finura, mqop_060.PRODUCAO_DIA
      , mqop_060.NUMERO_AGULHAS, mqop_060.PLATINA1, mqop_060.CODIGO_ROTEIRO, MQOP_060.VELOCIDADE
      , mqop_060.ALIMENTADORES, mqop_060.SAIDA_TECIDO, mqop_060.DIAMETRO
      , mqop_060.PISTA_DISCO1, mqop_060.PISTA_CILINDRO1
      , mqop_060.CAMOS_DISCO1, mqop_060.CAMOS_DISCO2
      , mqop_060.camos_cilindro1, mqop_060.camos_cilindro2, mqop_060.camos_cilindro3, mqop_060.camos_cilindro4, mqop_060.camos_cilindro5
from mqop_060, basi_010
where mqop_060.nivel_estrutura  = basi_010.nivel_estrutura
  and mqop_060.grupo_estrutura  = basi_010.grupo_estrutura
  and mqop_060.subgru_estrutura = basi_010.subgru_estrutura
  and mqop_060.item_estrutura   = '000000'


union

select
        mqop_060.maq_sbgr_grupo_mq as grupo_maq, mqop_060.maq_sbgr_sbgr_maq as subgru_maq, mqop_060.NUMERO_MAQUINA
      , basi_010.nivel_estrutura, basi_010.grupo_estrutura, basi_010.subgru_estrutura, basi_010.item_estrutura
      , mqop_060.tempo_producao, mqop_060.capac_producao, mqop_060.finura, mqop_060.PRODUCAO_DIA
      , mqop_060.NUMERO_AGULHAS, mqop_060.PLATINA1, mqop_060.CODIGO_ROTEIRO, MQOP_060.VELOCIDADE
      , mqop_060.ALIMENTADORES, mqop_060.SAIDA_TECIDO, mqop_060.DIAMETRO
      , mqop_060.PISTA_DISCO1, mqop_060.PISTA_CILINDRO1
      , mqop_060.CAMOS_DISCO1, mqop_060.CAMOS_DISCO2
      , mqop_060.camos_cilindro1, mqop_060.camos_cilindro2, mqop_060.camos_cilindro3, mqop_060.camos_cilindro4, mqop_060.camos_cilindro5
from mqop_060, basi_010
where mqop_060.nivel_estrutura  = basi_010.nivel_estrutura
  and mqop_060.grupo_estrutura  = basi_010.grupo_estrutura
  and mqop_060.subgru_estrutura = '000'
  and mqop_060.item_estrutura   = '000000'
/
