create or replace function inter_fn_num_to_char(p_number number) return varchar2 is 
    v_retorno varchar2(1); 
begin 
     
    v_retorno := '';
     
    case p_number
        when 1 then v_retorno := 'A'; 
        when 2 then v_retorno := 'B'; 
        when 3 then v_retorno := 'C'; 
        when 4 then v_retorno := 'D'; 
        when 5 then v_retorno := 'E'; 
        when 6 then v_retorno := 'F'; 
        when 7 then v_retorno := 'G'; 
        when 8 then v_retorno := 'H'; 
        when 9 then v_retorno := 'I'; 
        when 10 then v_retorno := 'J'; 
        when 11 then v_retorno := 'K'; 
        when 12 then v_retorno := 'L'; 
        when 13 then v_retorno := 'M'; 
        when 14 then v_retorno := 'N'; 
        when 15 then v_retorno := 'O'; 
        when 16 then v_retorno := 'P'; 
        when 17 then v_retorno := 'Q'; 
        when 18 then v_retorno := 'R'; 
        when 19 then v_retorno := 'S'; 
        when 20 then v_retorno := 'T'; 
        when 21 then v_retorno := 'U'; 
        when 22 then v_retorno := 'V'; 
        when 23 then v_retorno := 'X'; 
        when 24 then v_retorno := 'Y'; 
        when 25 then v_retorno := 'Z'; 
        else v_retorno := ''; 
    end case; 
     
    return v_retorno; 
end; 
/
