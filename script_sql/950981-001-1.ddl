alter table inte_510 drop constraint PK_INTE_510;

drop index PK_INTE_510;

alter table INTE_510 add constraint PK_INTE_510 primary key (CODIGO_EMPRESA, CNPJ9, CNPJ4, CNPJ2, QUALIDADE_ROLO, DEPOSITO_SAIDA) novalidate; 
