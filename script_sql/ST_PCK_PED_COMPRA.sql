create or replace package ST_PCK_PED_COMPRA as

    procedure valida_tab_preco_forn(p_tab_preco number, p_cgc_forn9 number,
                               p_cgc_forn4 number, p_cgc_forn2 number);

    procedure cria_pedido(p_codigo_comprador NUMBER,      p_codigo_empresa NUMBER,
                          p_usuario VARCHAR2,             p_cgc_forn9 NUMBER,
                          p_cgc_forn4 NUMBER,             p_cgc_forn2 NUMBER,
                          p_data_prev_entr DATE,          p_tab_preco NUMBER,
                          p_codigo_transacao NUMBER,      p_cond_pgto_compra NUMBER,
                          p_tipo_pedido NUMBER,           p_pedido_pai NUMBER, 
                          p_tipo_frete_redespacho NUMBER, p_ordem_servico NUMBER, 
                          p_tran_ped_forne9 NUMBER,       p_tran_ped_forne4 NUMBER, 
                          p_tran_ped_forne2 NUMBER,       p_cod_portador NUMBER, 
                          p_vendedor_contato VARCHAR2,    p_cod_moeda NUMBER, 
                          p_peso_total NUMBER,            p_valor_outras NUMBER, 
                          p_val_enc_finan NUMBER,         p_vlr_frete NUMBER,
                          v_pedido_compra out NUMBER);
                          
    procedure adicionar_item(p_cod_empresa IN NUMBER,p_pedido_compra IN NUMBER,
                            p_seq_item_pedido IN NUMBER,p_item_nivel IN VARCHAR2,
                            p_item_grupo IN VARCHAR2,p_item_subgru IN VARCHAR2,
                            p_item_item IN VARCHAR2, p_preco_item IN NUMBER,
                            p_perc_ipi IN NUMBER, p_data_prev_entr IN DATE,
                            p_cod_contabil IN NUMBER, p_cod_transacao IN NUMBER,
                            p_num_requis IN NUMBER, p_seq_item_req IN NUMBER,
                            p_qtde_pedida IN NUMBER, p_cgc9 IN NUMBER,
                            p_cgc4 IN NUMBER, p_cgc2 IN NUMBER,
                            p_ccusto IN NUMBER, p_valor_conv IN NUMBER, 
                            p_cod_fabricante_prod IN VARCHAR2, p_cod_prod_fabricante IN VARCHAR2, 
                            p_cnpj9_destino IN NUMBER, p_cnpj4_destino IN NUMBER, 
                            p_cnpj2_destino IN NUMBER, p_percentual_subs IN NUMBER, 
                            p_perc_enc_finan IN NUMBER, p_cod_deposito IN NUMBER);
                            
    procedure cancela_pedido(p_pedido_compra     NUMBER, p_cod_cancelamento NUMBER,
                             p_canc_itens_requis NUMBER, p_cod_empresa NUMBER);
                             
    procedure cancelar_pedidos(p_pedidos_compra    VARCHAR2, p_cod_cancelamento NUMBER,
                               p_canc_itens_requis NUMBER, p_cod_empresa NUMBER);
                               
    function busca_valor_pedido(p_pedido_compra NUMBER) return number;
    
    function busca_valor_saldo_pedido(p_pedido_compra NUMBER) return number;

END ST_PCK_PED_COMPRA;
