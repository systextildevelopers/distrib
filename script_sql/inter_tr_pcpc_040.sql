create or replace trigger inter_tr_pcpc_040
   before insert
       or delete
       or update of qtde_pecas_prog,
                    qtde_pecas_prod,
                    qtde_pecas_2a,
                    qtde_perdas,
                    qtde_conserto,
                    qtde_a_produzir_pacote
   on pcpc_040
   for each row

-- Hist?ricos de altera??es na trigger
--
-- Data       Autor           Motivo         Observa??es
-- 22/06/10   Aline Blanski   SS-55655/001   Ao incluir ou excluir pacotes de confec??o tabela PCPC_040, salvar as informa??es para
--                                           atualiza??es na tabela PCPC_090.
-- 13/08/10   Aline Blanski   SS-59142/001   Altera??o na Busca do roteiro do produto
--
-- 06/06/11   Aldro? Klinger  SS-65071/001   Adicionado o campo DATA_ALTERACAO e o mesmo ser? setado com a data corrente quando o campo QTDE_PECAS_PROG for alterado.

declare
   t_estagio_corte                       basi_030.estagio_altera_programado%type;
   t_alternativa_020                     number;
   t_periodo_producao                    number;
   t_ultimo_estagio                      number;
   t_qtde_atu                            number;
   t_qtde_prog                           number;
   v_executa_trigger                     number;
   t_existe                              number;
   t_alternativa_mqop_057                mqop_057.numero_alternativa%type;
   t_roteiro_mqop_057                    mqop_057.numero_roteiro%type;
   --t_descr_parte_peca_mqop_057           mqop_057.descr_parte_peca %type;
   --t_controle_na_entrada_saida           mqop_005.controle_na_entrada_saida%type;
   t_codigo_estagio_confec               empr_001.estagio_confec%type;
   t_seq_operacao_confec                 mqop_050.seq_operacao%type;
   t_seq_operacao_estagio                mqop_050.seq_operacao%type;
   t_sequencia_parte                     pcpc_090.sequencia_parte%type;
   ws_usuario_rede                       varchar2(20);
   ws_maquina_rede                       varchar2(40);
   ws_aplicativo                         varchar2(20);
   ws_sid                                number(9);
   ws_empresa                            number(3);
   ws_usuario_systextil                  varchar2(250);
   ws_locale_usuario                     varchar2(5);
   v_deleta                              number(9);
   v_ultimp_pacote                       pcpc_010.ultimo_numero%type;

   v_sub_ler                             mqop_050.subgru_estrutura%type := '###';
   v_item_ler                            mqop_050.item_estrutura%type   := '######';

   v_nr_registro                         number;

   v_cod_estagio_agrupador               number;
   v_regra_estagio_agrupador             number;

   l_cod_emp_centro_custo                basi_185.local_entrega%type;
   v_centro_custo                        mqop_050.centro_custo%type;
   v_ccusto_homem                        mqop_050.centro_custo%type;
   v_centro_custo_rot                    pcpc_040.centro_custo_rot%type;
begin
   -- INICIO - L?gica implementada para controle de "gatilho" das triggers do banco
   -- para atender a implementa??o executada para limpeza da base de dados do cliente
   -- e replica??o dos dados para base hist?rico. (SS.38405)
   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;
   -- FINAL - L?gica implementada para controle de "gatilho" das triggers do banco. (SS.38405)

   -- Dados do usu?rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   if v_executa_trigger = 0
   then
      /**
       * Quando estiver inserindo e o pacote j? existir no per?odo de produ??o
       * o registro ser? marcado como alterado, porque j? passou uma vez pela
       * numera??o de pacote.
       */
       if inserting
       then
       	  if :new.periodo_producao = 0 then
       	     raise_application_error (-20000,'PERIODO DE PRODU??O ZERADO, AVISE O SETOR DE TI!');
       	  end if;
          begin
             select pcpc_010.ultimo_numero
             into v_ultimp_pacote
             from pcpc_010
             where pcpc_010.area_periodo = 1
               and pcpc_010.periodo_producao = :new.periodo_producao;
          exception when OTHERS
          then
             v_ultimp_pacote := 0;
          end;

          if v_ultimp_pacote <> 0 and :new.ordem_confeccao > v_ultimp_pacote
          then
             :new.data_alteracao := sysdate;
          end if;

          -- Carrega informa??o da capa da ordem de produ??o.
          if :new.codigo_empresa is null
          or :new.codigo_empresa = 0
          then
             begin
                select pcpc_010.codigo_empresa
                into   :new.codigo_empresa
                from pcpc_020, pcpc_010
                where pcpc_020.ordem_producao   = :new.ordem_producao
                  and pcpc_010.area_periodo     = 1
                  and pcpc_010.periodo_producao = pcpc_020.periodo_producao;
             exception when OTHERS then
                :new.codigo_empresa := 0;
             end;
          end if;
       end if;

      /**
       * Controle de parte de pe?a
       */
      if inserting or updating
      then
         t_existe := 0;
         -- com base na ordem de produ??o ir? buscar as informa??es da alternativa e roteiro da pe?a.
         begin
            select pcpc_020.alternativa_peca,
                   pcpc_020.roteiro_peca
            into   t_alternativa_mqop_057,
                   t_roteiro_mqop_057
            from pcpc_020
            where pcpc_020.ordem_producao = :new.ordem_producao;
         end;

         begin
           v_sub_ler  := :new.proconf_subgrupo;
           v_item_ler := :new.proconf_item;
         end;
         begin
            select count(*)
            into v_nr_registro
            from mqop_050
            where mqop_050.nivel_estrutura  = :new.proconf_nivel99
              and mqop_050.grupo_estrutura  = :new.proconf_grupo
              and mqop_050.subgru_estrutura = v_sub_ler
              and mqop_050.item_estrutura   = v_item_ler
              and mqop_050.numero_alternati = t_alternativa_mqop_057
              and mqop_050.numero_roteiro   = t_roteiro_mqop_057;
            if v_nr_registro = 0
            then
               v_item_ler := '000000';
               begin
                  select count(*)
                  into v_nr_registro
                  from mqop_050
                  where mqop_050.nivel_estrutura  = :new.proconf_nivel99
                    and mqop_050.grupo_estrutura  = :new.proconf_grupo
                    and mqop_050.subgru_estrutura = v_sub_ler
                    and mqop_050.item_estrutura   = v_item_ler
                    and mqop_050.numero_alternati =  t_alternativa_mqop_057
                    and mqop_050.numero_roteiro   = t_roteiro_mqop_057;
                  if v_nr_registro = 0
                  then
                     v_sub_ler  := '000';
                     v_item_ler := :new.proconf_item;
                     begin
                        select count(*)
                        into v_nr_registro
                        from mqop_050
                        where mqop_050.nivel_estrutura  = :new.proconf_nivel99
                          and mqop_050.grupo_estrutura  = :new.proconf_grupo
                          and mqop_050.subgru_estrutura = v_sub_ler
                          and mqop_050.item_estrutura   = v_item_ler
                          and mqop_050.numero_alternati =  t_alternativa_mqop_057
                          and mqop_050.numero_roteiro   = t_roteiro_mqop_057;
                        if v_nr_registro = 0
                        then
                           v_item_ler := '000000';
                        end if;
                     end;
                  end if;
               end;
            end if;
         end;

         --ir? verificar se controla por parte de pe?a o est?gio e existe cadastrado na tabela mqop_057
         select count(1)
         into t_existe
         from mqop_005
         where mqop_005.codigo_estagio     = :new.codigo_estagio
           and mqop_005.controle_por_parte > 0
           and exists ( select 1 from mqop_057
                         where mqop_057.codigo_estagio      = mqop_005.codigo_estagio
                           and mqop_057.nivel_estrutura     = :new.proconf_nivel99
                           and mqop_057.grupo_estrutura     = :new.proconf_grupo
                           and mqop_057.subgrupo_estrutura  = v_sub_ler
                           and mqop_057.item_estrutura      = v_item_ler
                           and mqop_057.numero_alternativa  = t_alternativa_mqop_057
                           and mqop_057.numero_roteiro      = t_roteiro_mqop_057
                           and mqop_057.controle_por_parte  = 1 );

         --ir? verificar se a sequencia do est?gio ? menor que a sequencia do est?gio da costura.
         select empr_001.estagio_confec into t_codigo_estagio_confec from empr_001;

         select min(mqop_050.seq_operacao) into t_seq_operacao_confec from mqop_050
         where mqop_050.codigo_estagio      = t_codigo_estagio_confec
           and mqop_050.nivel_estrutura     = :new.proconf_nivel99
           and mqop_050.grupo_estrutura     = :new.proconf_grupo
           and mqop_050.subgru_estrutura    = v_sub_ler
           and mqop_050.item_estrutura      = v_item_ler
           and mqop_050.numero_alternati    = t_alternativa_mqop_057
           and mqop_050.numero_roteiro      = t_roteiro_mqop_057;

         select min(mqop_050.seq_operacao) into t_seq_operacao_estagio from mqop_050
         where mqop_050.codigo_estagio      = :new.codigo_estagio
           and mqop_050.nivel_estrutura     = :new.proconf_nivel99
           and mqop_050.grupo_estrutura     = :new.proconf_grupo
           and mqop_050.subgru_estrutura    = v_sub_ler
           and mqop_050.item_estrutura      = v_item_ler
           and mqop_050.numero_alternati    = t_alternativa_mqop_057
           and mqop_050.numero_roteiro      = t_roteiro_mqop_057;



		    --Buscar estagio agrupador da mqop_050 quando estiver inserindo se nao tiver recebido estagio agrupador do programa (se estiver nulo)
        if  inserting and (:new.cod_estagio_agrupador is null or :new.cod_estagio_agrupador = 0) then
            
            v_cod_estagio_agrupador := null;
			      v_regra_estagio_agrupador := null;
            
            begin
                select mqop_050.cod_estagio_agrupador,  mqop_050.regra_estagio_agrupador,
                       mqop_050.centro_custo,           mqop_050.ccusto_homem
                into   v_cod_estagio_agrupador,         v_regra_estagio_agrupador,
				               v_centro_custo,                  v_ccusto_homem
                from mqop_050
                where mqop_050.nivel_estrutura   = :new.proconf_nivel99
                and mqop_050.grupo_estrutura     = :new.proconf_grupo
                and mqop_050.subgru_estrutura    = v_sub_ler
                and mqop_050.item_estrutura      = v_item_ler
                and mqop_050.numero_alternati    = t_alternativa_mqop_057
                and mqop_050.numero_roteiro      = t_roteiro_mqop_057
                and mqop_050.codigo_estagio      = :new.codigo_estagio
                and mqop_050.seq_operacao        = t_seq_operacao_estagio;
            exception when others then
                v_cod_estagio_agrupador := null;
				        v_regra_estagio_agrupador := null;
         				v_centro_custo := null;
				        v_ccusto_homem := null;
            end;

            --BUSCAR EMPRESA DO CENTRO DE CUSTO
            l_cod_emp_centro_custo := 0;
            
            begin
                select nvl(basi_185.local_entrega,0)
                into l_cod_emp_centro_custo
                from basi_185
                where centro_custo = v_centro_custo;
                
                v_centro_custo_rot := v_centro_custo;
            exception when others then
                  l_cod_emp_centro_custo := 0;
                  v_centro_custo_rot     := 0;
                  null;
            end;
            
            if l_cod_emp_centro_custo = 0 then
                v_centro_custo_rot := 0;
                
                begin
                  select local_entrega
                  into l_cod_emp_centro_custo
                  from basi_185
                  where centro_custo = v_ccusto_homem;
                  
                  v_centro_custo_rot := v_ccusto_homem;
                exception when others then
                  l_cod_emp_centro_custo := 0;
                  v_centro_custo_rot     := 0;
                end;
            end if;
            
            declare
                controleProcessoBlocoK number;
            begin
                select count(*) 
                into controleProcessoBlocoK
                from empr_007
                where param       = 'obrf.controleProcessoBlocoK'
                and   default_int = 1;
                
                if  controleProcessoBlocoK > 0 
                and :new.estagio_anterior <> 0 
                and v_cod_estagio_agrupador = 0 then
                    raise_application_error (-20000,'ATENCAO! Não foi informado o estágio agrupador, verificar o roteiro de produção');
                end if;
                
                if controleProcessoBlocoK > 0
                and :new.estagio_anterior <> 0
                and l_cod_emp_centro_custo = 0 then
                    raise_application_error (-20000,'ATENCAO! Não foi informado um centro de custo que tenha a empresa informada, verificar o roteiro de produção');
                end if;
				
                if controleProcessoBlocoK > 0
                and :new.sequencia_estagio = 0 then
                    raise_application_error (-20000,'ATENCAO! A sequencia de estágio não pode ficar zerada.');
                end if;    
            end;
            
            :new.cod_estagio_agrupador   := v_cod_estagio_agrupador;
            :new.regra_estagio_agrupador := v_regra_estagio_agrupador;
            :new.cod_empresa_produtiva   := l_cod_emp_centro_custo;
            :new.centro_custo_rot        := v_centro_custo_rot;
         end if;
         
         if t_existe <> 0 and t_seq_operacao_estagio <= t_seq_operacao_confec
         then
            t_sequencia_parte := 0;
            for reg_mqop_057 in  (select mqop_057.descr_parte_peca ,   mqop_005.controle_na_entrada_saida
                                    from mqop_057 , mqop_005
                                   where mqop_057.codigo_estagio      = mqop_005.codigo_estagio
                                     and mqop_005.controle_por_parte  > 0
                                     and mqop_057.nivel_estrutura     = :new.proconf_nivel99
                                     and mqop_057.grupo_estrutura     = :new.proconf_grupo
                                     and mqop_057.subgrupo_estrutura  = v_sub_ler
                                     and mqop_057.item_estrutura      = v_item_ler
                                     and mqop_057.numero_alternativa  = t_alternativa_mqop_057
                                     and mqop_057.numero_roteiro      = t_roteiro_mqop_057
                                     and mqop_057.codigo_estagio      = :new.codigo_estagio
                                     and mqop_057.controle_por_parte  = 1)
            loop
               begin
                  select sequencia_parte  into t_sequencia_parte
                  from pcpc_090
                  where pcpc_090.ordem_producao     = :new.ordem_producao
                    and pcpc_090.descr_parte_peca   = reg_mqop_057.descr_parte_peca
                    and rownum =1 ;
               exception when no_data_found then
                  select max(sequencia_parte) + 1 into t_sequencia_parte
                  from pcpc_090
                  where pcpc_090.ordem_producao    = :new.ordem_producao
                    and pcpc_090.sequencia_enfesto = :new.sequencia_enfesto
                    and pcpc_090.ordem_confeccao   = :new.ordem_confeccao;
               end;

               if t_sequencia_parte is null
               or t_sequencia_parte = 0
               then
                  t_sequencia_parte := 1;
               end if;

               begin
                  select nvl(count(*),0)
                  into v_nr_registro
                  from pcpc_090
                  where pcpc_090.ordem_producao    = :new.ordem_producao
                    and pcpc_090.sequencia_enfesto = :new.sequencia_enfesto
                    and pcpc_090.ordem_confeccao   = :new.ordem_confeccao
                    and pcpc_090.sequencia_parte   = t_sequencia_parte
                    and pcpc_090.codigo_estagio    = :new.codigo_estagio;
               exception when OTHERS then
                  v_nr_registro := 0;
               end;

               if v_nr_registro = 0
               then
                  begin
                     insert into pcpc_090 (
                        ordem_producao,            sequencia_enfesto,
                        ordem_confeccao,           sequencia_parte,
                        codigo_estagio,            descr_parte_peca,
                        etiqueta_impressa,         controle_na_entrada_saida,
                        data_conferencia_parte,    hora_conferencia_parte,
                        usuario_conferencia_parte, data_entrada_estagio,
                        hora_entrada_estagio,      usuario_entrada_estagio,
                        data_saida_estagio,        hora_saida_estagio,
                        usuario_saida_estagio
                       )
                     values(
                        :new.ordem_producao,       :new.sequencia_enfesto,
                        :new.ordem_confeccao,      t_sequencia_parte,
                        :new.codigo_estagio,       reg_mqop_057.descr_parte_peca,
                        null,                      reg_mqop_057.controle_na_entrada_saida,
                        null,                      null,
                        null,                      null,
                        null,                      null,
                        null,                      null,
                        null
                       );
                  exception when OTHERS then
                     raise_application_error (-20000, inter_fn_buscar_tag_composta ('ds27971', :new.ordem_producao , :new.sequencia_enfesto , :new.ordem_confeccao , t_sequencia_parte , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
                  end;
               end if;

            end loop;
         end if;

         -- CHAMA PROCEDURE QUE INSERE NA TABELA TEMPORARIA DO RECALCULO - bruno 64792-001
         inter_pr_marca_referencia_op('0', '00000',
                                      '000', '000000',
                                      :new.ordem_producao, 1);

      end if;

      /**
       * Quando atualizar executa consistencia das tabelas de empenho e reserva
       * de produto (tmrp_141 e tmrp_041). Quando as mesmas estiverem com suas
       * quantidades zeradas os registros s?o eliminados
       */
      if updating
      then
         -- Carrega informa??o da capa da ordem de produ??o.
         begin
            select pcpc_020.periodo_producao, pcpc_020.alternativa_peca,
                   pcpc_020.ultimo_estagio
            into   t_periodo_producao,        t_alternativa_020,
                   t_ultimo_estagio
            from pcpc_020
            where pcpc_020.ordem_producao = :new.ordem_producao;
         end;

         ----------------------------------------------------------------------------------
         ----- INICIO - ATUALIZA??O DO PLANEJAMENTO *** QUANTIDADE PROGRAMADA ***     -----
         ----------------------------------------------------------------------------------
         if :new.qtde_pecas_prog <> :old.qtde_pecas_prog
         then
            t_qtde_prog := :old.qtde_pecas_prog - :new.qtde_pecas_prog;

            --Atualiza o campo DATA_ALTERACAO com a data da altera??o da quantidade de pe?as programadas
            :new.data_alteracao := sysdate;

            --raise_application_error(-20000,t_qtde_prog);
            if t_qtde_prog <> 0 and :new.codigo_estagio = t_ultimo_estagio
            then
               begin
                  update tmrp_041
                  set tmrp_041.qtde_reservada        = tmrp_041.qtde_reservada - (t_qtde_prog * tmrp_041.consumo)
                  where tmrp_041.periodo_producao    = t_periodo_producao
                    and tmrp_041.area_producao       = 1
                    and tmrp_041.nr_pedido_ordem     = :new.ordem_producao
                    and tmrp_041.seq_pedido_ordem    = :new.ordem_confeccao;
               end;

               begin
                  update tmrp_041
                  set tmrp_041.qtde_areceber         = tmrp_041.qtde_areceber - t_qtde_prog
                  where tmrp_041.periodo_producao    = t_periodo_producao
                    and tmrp_041.area_producao       = 1
                    and tmrp_041.nr_pedido_ordem     = :new.ordem_producao
                    and tmrp_041.nivel_estrutura     = :new.proconf_nivel99
                    and tmrp_041.grupo_estrutura     = :new.proconf_grupo
                    and tmrp_041.subgru_estrutura    = :new.proconf_subgrupo
                    and tmrp_041.item_estrutura      = :new.proconf_item
                    and tmrp_041.sequencia_estrutura = 0
                    and tmrp_041.codigo_estagio      = t_ultimo_estagio
                    and tmrp_041.seq_pedido_ordem    = :new.ordem_confeccao;
               end;
            end if;
         else

            ----------------------------------------------------------------------------------
            ----- INICIO - ATUALIZA??O DO PLANEJAMENTO *** QUANTIDADE PRODUZIDA ***     -----
            ----- Com a quantidade produzida, ? feita a atualiza??o do planejamento da   -----
            ----- produ??o, abatendo ou aumentando as quantidade reservadas e a receber  -----
            ----------------------------------------------------------------------------------
            -- ATUALIZA??O DO PLANEJAMENTO PARA PRODU??O.
            t_qtde_atu := (:new.qtde_pecas_prod - :old.qtde_pecas_prod) +
                          (:new.qtde_pecas_2a   - :old.qtde_pecas_2a)   +
                          (:new.qtde_perdas     - :old.qtde_perdas)     +
                          (:new.qtde_conserto   - :old.qtde_conserto);

            t_qtde_atu  := (:new.qtde_a_produzir_pacote - :old.qtde_a_produzir_pacote);

            if t_qtde_atu <> 0
            then
               begin
                  update tmrp_041
                  set   tmrp_041.qtde_reservada      = tmrp_041.qtde_reservada + (t_qtde_atu * tmrp_041.consumo)
                  where tmrp_041.periodo_producao    = t_periodo_producao
                    and tmrp_041.area_producao       = 1
                    and tmrp_041.nr_pedido_ordem     = :new.ordem_producao
                    and tmrp_041.codigo_estagio      = :new.codigo_estagio
                    and tmrp_041.seq_pedido_ordem    = :new.ordem_confeccao;
               end;

               if :new.codigo_estagio = t_ultimo_estagio
               then
                  begin
                     update tmrp_041
                     set   tmrp_041.qtde_areceber       = tmrp_041.qtde_areceber + t_qtde_atu
                     where tmrp_041.periodo_producao    = t_periodo_producao
                       and tmrp_041.area_producao       = 1
                       and tmrp_041.nr_pedido_ordem     = :new.ordem_producao
                       and tmrp_041.nivel_estrutura     = :new.proconf_nivel99
                       and tmrp_041.grupo_estrutura     = :new.proconf_grupo
                       and tmrp_041.subgru_estrutura    = :new.proconf_subgrupo
                       and tmrp_041.item_estrutura      = :new.proconf_item
                       and tmrp_041.sequencia_estrutura = 0
                       and tmrp_041.codigo_estagio      = t_ultimo_estagio
                       and tmrp_041.seq_pedido_ordem    = :new.ordem_confeccao;
                  end;

               end if;
            end if;
         end if;

         begin
            select nvl(sum(1),0)
            into v_deleta
            from tmrp_041
            where tmrp_041.periodo_producao    = t_periodo_producao
              and tmrp_041.area_producao       = 1
              and tmrp_041.nr_pedido_ordem     = :new.ordem_producao
              and tmrp_041.codigo_estagio      = :new.codigo_estagio
              and tmrp_041.seq_pedido_ordem    = :new.ordem_confeccao
              and tmrp_041.qtde_reservada      <= 0.00000
              and tmrp_041.qtde_areceber       <= 0.00000;
         exception
         when no_data_found then
            v_deleta := 0;
         end;

         if (v_deleta > 0)
         then
            begin
               delete from tmrp_041
               where tmrp_041.periodo_producao    = t_periodo_producao
                 and tmrp_041.area_producao       = 1
                 and tmrp_041.nr_pedido_ordem     = :new.ordem_producao
                 and tmrp_041.codigo_estagio      = :new.codigo_estagio
                 and tmrp_041.seq_pedido_ordem    = :new.ordem_confeccao
                 and tmrp_041.qtde_reservada     <= 0.00000
                 and tmrp_041.qtde_areceber      <= 0.00000;
            end;
         end if;
         ----------------------------------------------------------------------------------
         ----- FINAL - ATUALIZA??O DO PLANEJAMENTO                                    -----
         ----------------------------------------------------------------------------------
      end if; -- if inserting or updating

      if deleting
      then

         -- CHAMA PROCEDURE QUE INSERE NA TABELA TEMPORARIA DO RECALCULO - bruno 64792-001
         inter_pr_marca_referencia_op('0', '00000',
                                      '000', '000000',
                                      :old.ordem_producao, 1);

         -- excluir da tabela pcpc_090 os registros por ordem de confec??o
         begin
            delete from pcpc_090
             where pcpc_090.ordem_producao  = :old.ordem_producao
               and pcpc_090.ordem_confeccao = :old.ordem_confeccao;
         exception when others then
            raise_application_error (-20000, inter_fn_buscar_tag_composta('ds27972', :old.ordem_producao , :old.ordem_confeccao , '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));

         end;

         -- Abater do planejamento da produ??o no momento da dele??o de um pacote
         -- da ordem de produ??o.
         begin
            -- Carrega informa??o da capa da ordem de produ??o.
            select pcpc_020.periodo_producao, pcpc_020.alternativa_peca,
                   pcpc_020.ultimo_estagio
            into   t_periodo_producao,        t_alternativa_020,
                   t_ultimo_estagio
            from pcpc_020
            where pcpc_020.ordem_producao = :old.ordem_producao;
         end;

         begin
            -- Atualiza a quantidade a receber do produto na ordem de confec??o.
            delete tmrp_041
            where tmrp_041.periodo_producao    = t_periodo_producao
              and tmrp_041.area_producao       = 1
              and tmrp_041.codigo_estagio      = :old.codigo_estagio
              and tmrp_041.nr_pedido_ordem     = :old.ordem_producao
              and tmrp_041.seq_pedido_ordem    = :old.ordem_confeccao;
         end;
      end if;
   end if;

   if inserting
   then
     :new.nome_programa_criacao := inter_fn_nome_programa(ws_sid);
   end if;

   if updating
   then
      :new.nome_programa := inter_fn_nome_programa(ws_sid);
   end if;

end inter_tr_pcpc_040;
/

