create or replace trigger wms_tr_rfid_inv_log
  before insert
  or delete
  or update of codigo_unico_sku, rfid_caixa, nivel_sku, grupo_sku, 
  subgrupo_sku, item_sku, cod_deposito, qtd_pecas, 
  flag, timestamp_liberacao, timestamp_armazenagem, timestamp_insercao, mensagem 
  on inte_wms_rfid_inv
  for each row
declare
  -- local variables here
  v_rfid_caixa                 inte_wms_rfid_inv.rfid_caixa              %type;
  v_nivel_sku_new              inte_wms_rfid_inv.nivel_sku               %type;
  v_nivel_sku_old              inte_wms_rfid_inv.nivel_sku               %type;
  v_grupo_sku_new              inte_wms_rfid_inv.grupo_sku               %type;
  v_grupo_sku_old              inte_wms_rfid_inv.grupo_sku               %type;
  v_subgrupo_sku_new           inte_wms_rfid_inv.subgrupo_sku            %type;
  v_subgrupo_sku_old           inte_wms_rfid_inv.subgrupo_sku            %type;
  v_item_sku_new               inte_wms_rfid_inv.item_sku                %type;
  v_item_sku_old               inte_wms_rfid_inv.item_sku                %type;
  v_cod_deposito_new           inte_wms_rfid_inv.cod_deposito            %type;
  v_cod_deposito_old           inte_wms_rfid_inv.cod_deposito            %type;
  v_qtd_pecas_new              inte_wms_rfid_inv.qtd_pecas               %type;
  v_qtd_pecas_old              inte_wms_rfid_inv.qtd_pecas               %type;
  v_flag_new                   inte_wms_rfid_inv.flag                    %type;
  v_flag_old                   inte_wms_rfid_inv.flag                    %type;
  v_timestamp_lib_new          inte_wms_rfid_inv.timestamp_liberacao     %type;
  v_timestamp_lib_old          inte_wms_rfid_inv.timestamp_liberacao     %type;
  v_timestamp_arm_new          inte_wms_rfid_inv.timestamp_armazenagem   %type;
  v_timestamp_arm_old          inte_wms_rfid_inv.timestamp_armazenagem   %type;
  v_timestamp_ins_new          inte_wms_rfid_inv.timestamp_insercao      %type;
  v_timestamp_ins_old          inte_wms_rfid_inv.timestamp_insercao      %type;
  v_mensagem_new               inte_wms_rfid_inv.mensagem                %type;
  v_mensagem_old               inte_wms_rfid_inv.mensagem                %type;
  v_codigo_unico_sku_new       inte_wms_rfid_inv.codigo_unico_sku        %type;
  v_codigo_unico_sku_old       inte_wms_rfid_inv.codigo_unico_sku        %type;

  v_sid                            number(9);
  v_empresa                        number(3);
  v_usuario_systextil              varchar2(250);
  v_locale_usuario                 varchar2(5);
  v_nome_programa                  varchar2(20);
 

  v_operacao                       varchar(1);
  v_data_operacao                  date;
  v_usuario_rede                   varchar(20);
  v_maquina_rede                   varchar(40);
  v_aplicativo                     varchar(20);
begin

   -- grava a data/hora da insercao do registro (log)
   v_data_operacao := sysdate();

   --alimenta as variaveis new caso seja insert ou update
   if inserting or updating
   then
      if inserting
      then v_operacao := 'i';
      else v_operacao := 'u';
      end if;

      v_rfid_caixa            := :new.rfid_caixa;
      v_nivel_sku_new         := :new.nivel_sku;
      v_grupo_sku_new         := :new.grupo_sku;
      v_subgrupo_sku_new      := :new.subgrupo_sku;
      v_item_sku_new          := :new.item_sku;
      v_cod_deposito_new      := :new.cod_deposito;
      v_qtd_pecas_new         := :new.qtd_pecas;
      v_flag_new              := :new.flag;
      v_timestamp_lib_new     := :new.timestamp_liberacao;
      v_timestamp_arm_new     := :new.timestamp_armazenagem;
      v_timestamp_ins_new     := :new.timestamp_insercao;
      v_mensagem_new          := :new.mensagem;
      v_codigo_unico_sku_new  := :new.codigo_unico_sku;
   end if; --fim do if inserting or updating

   --alimenta as variaveis old caso seja insert ou update
   if deleting or updating
   then
      if deleting
      then
         v_operacao      := 'd';
      else
         v_operacao      := 'u';
      end if;

      v_rfid_caixa            := :old.rfid_caixa;
      v_nivel_sku_old         := :old.nivel_sku;
      v_grupo_sku_old         := :old.grupo_sku;
      v_subgrupo_sku_old      := :old.subgrupo_sku;
      v_item_sku_old          := :old.item_sku;
      v_cod_deposito_old      := :old.cod_deposito;
      v_qtd_pecas_old         := :old.qtd_pecas;
      v_flag_old              := :old.flag;
      v_timestamp_lib_old     := :old.timestamp_liberacao;
      v_timestamp_arm_old     := :old.timestamp_armazenagem;
      v_timestamp_ins_old     := :old.timestamp_insercao;
      v_mensagem_old          := :old.mensagem;
      v_codigo_unico_sku_old  := :old.codigo_unico_sku;

   end if; --fim do if deleting or updating


   -- Dados do usu�rio logado
   inter_pr_dados_usu_inte (v_usuario_rede,        v_maquina_rede,   v_aplicativo,     v_sid,
                            v_usuario_systextil,   v_empresa,        v_locale_usuario);


    v_nome_programa := ''; --Deixado de fora por quest�es de performance, a pedido do cliente

   --insere na inte_wms_rfid_inv_log o registro.
   insert into inte_wms_rfid_inv_log (
      rfid_caixa,            
      nivel_sku_old,           nivel_sku_new,
      grupo_sku_old,           grupo_sku_new,
      subgrupo_sku_old,        subgrupo_sku_new,
      item_sku_old,            item_sku_new,
      cod_deposito_old,        cod_deposito_new,
      qtd_pecas_old,           qtd_pecas_new,
      flag_old,                flag_new,
      timestamp_lib_old,       timestamp_lib_new,
      timestamp_arm_old,       timestamp_arm_new,
      timestamp_ins_old,       timestamp_ins_new,
      mensagem_new,            
      codigo_unico_sku_old,    codigo_unico_sku_new,
      operacao,
      data_operacao,           usuario_rede,
      maquina_rede,            aplicativo,
      nome_programa
   )
   values (
      v_rfid_caixa,            
      v_nivel_sku_old,         v_nivel_sku_new,
      v_grupo_sku_old,         v_grupo_sku_new,
      v_subgrupo_sku_old,      v_subgrupo_sku_new,
      v_item_sku_old,          v_item_sku_new,
      v_cod_deposito_old,      v_cod_deposito_new,
      v_qtd_pecas_old,         v_qtd_pecas_new,
      v_flag_old,              v_flag_new,
      v_timestamp_lib_old,     v_timestamp_lib_new,
      v_timestamp_arm_old,     v_timestamp_arm_new,
      v_timestamp_ins_old,     v_timestamp_ins_new,
      v_mensagem_new,          
      v_codigo_unico_sku_old,  v_codigo_unico_sku_new,
      v_operacao,
      v_data_operacao,         v_usuario_rede,
      v_maquina_rede,          v_aplicativo,
      v_nome_programa
   );

end wms_tr_rfid_inv_log;
/

execute inter_pr_recompile;

/* versao: 1 */

exit;


 exit;

 exit;
