alter table obrf_158 add destaca_desp_imp_danf number(1) default 1;

comment on column obrf_158.destaca_desp_imp_danf is '1: Na DANFE de Entrada (por emissão) o campo obrf_010.despesas_importacao é somado ao campo OUTRAS DESPESAS ACESSORIAS da Danfe. 0: O campo obrf_010.despesas_importacao não será somado ao campo OUTRAS DESPESAS ACESSORIAS da Danfe.';

exec inter_pr_recompile;
