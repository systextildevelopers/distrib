create or replace TRIGGER "INTER_TR_PEDI_110_DESCONTO" 
after insert or update
on pedi_110
for each row
begin
   -- MZ - ID_7049 - 24487 - Regra para a Natureza da Operação devido o disparo da trigger INTER_TR_PEDI_100_8 para não dar o erro 
   -- ORA-04091: a tabela PEDI_100 é mutante;
   if nvl(:old.cod_nat_op,-999) = nvl(:new.cod_nat_op,-999) then 
       for reg_pedi_100 in (select pedi_100.tipo_desconto from pedi_100
                            where pedi_100.pedido_venda = :new.pedido_venda)
       loop
          begin
             if reg_pedi_100.tipo_desconto not in (1,2)
             then
                update pedi_100
                   set pedi_100.tipo_desconto = 1
                where pedi_100.pedido_venda = :new.pedido_venda;
              end if;
          end;
       end loop;
   end if;
end inter_tr_pedi_110_desconto;
-- ALTER TRIGGER "INTER_TR_PEDI_110_DESCONTO" ENABLE
/
