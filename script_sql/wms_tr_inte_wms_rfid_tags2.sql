create or replace trigger wms_tr_inte_wms_rfid_tags2
before insert on inte_wms_rfid_tags
for each row
begin

   update pcpc_330
   set pcpc_330.cod_caixa_rfid = :new.rfid_caixa
   where pcpc_330.periodo_producao = :new.periodo_tag
     and pcpc_330.ordem_producao   = :new.ordem_prod_tag
     and pcpc_330.ordem_confeccao  = :new.ordem_conf_tag
     and pcpc_330.sequencia        = :new.sequencia_tag;
   
end wms_tr_inte_wms_rfid_tags2;

/

exec inter_pr_recompile;
/* versao: 1 */


 exit;
