CREATE OR REPLACE TRIGGER "INTER_TR_BASI_461"
BEFORE INSERT ON BASI_461 FOR EACH ROW
DECLARE
    proximo_valor number;
BEGIN
    if :new.id is null then
        select ID_BASI_461.nextval into proximo_valor from dual;
        :new.id := proximo_valor;
    end if;
END INTER_TR_BASI_461;
