CREATE OR REPLACE VIEW PCPT_020_PCPT021
(
  CODIGO_ROLO,
  PANOACAB_NIVEL99,
  PANOACAB_GRUPO,
  PANOACAB_SUBGRUPO,
  PANOACAB_ITEM,
  LARGURA,
  MT_LINEARES_PROD,
  LOTE_ACOMP,
  CODIGO_DEPOSITO,
  QTDE_QUILOS_ACAB,
  DATA_PROD_TECEL,
  ENDERECO_ROLO,
  SEQUENCIA,
  ROLO_ESTOQUE,
  GRUPO_MAQUINA,
  SUB_MAQUINA,
  NUMERO_MAQUINA,
  DATA_LEITURA_ROLO,
  DESCRICAO_ENDERECO,
  LOTE,
  NUMERO_LOTE,
  ORDEM_ENGOMAG,
  RESTRICAO
)
AS 
select pcpt_020.codigo_rolo,        pcpt_020.panoacab_nivel99,
       pcpt_020.panoacab_grupo,     pcpt_020.panoacab_subgrupo,
       pcpt_020.panoacab_item,      pcpt_020.largura,
       pcpt_020.mt_lineares_prod,   pcpt_020.lote_acomp,
       pcpt_020.codigo_deposito,    pcpt_020.qtde_quilos_acab,
       pcpt_020.data_prod_tecel,    pcpt_020.endereco_rolo,
       pcpt_021.sequencia,          pcpt_020.rolo_estoque,
       pcpt_020.grupo_maquina,      pcpt_020.sub_maquina,
       pcpt_020.numero_maquina,     t043.data_leitura_rolo,
       t043.descricao_endereco,     inter_fn_agg_lote_fio(pcpt_020.codigo_rolo) lote,
       pcpt_020.numero_lote,        pcpt_021.ordem_engomagem AS ordem_engomag,
       pcpt_021.restricao
from pcpt_020, pcpt_021,
     (select basi_043.codigo_container,   basi_043.data_leitura_rolo,
             basi_043.descricao_endereco
      from basi_043) t043
where  pcpt_020.codigo_rolo   = pcpt_021.codigo_rolo(+)
  and  pcpt_020.endereco_rolo = to_char(t043.codigo_container (+))
  AND  NOT EXISTS (select 1 
                     from tmrp_141
                     where tmrp_141.tipo_reserva          = 4
                     and tmrp_141.codigo_rolo         =  pcpt_020.codigo_rolo
                     and tmrp_141.area_producao_reserva = 1);

