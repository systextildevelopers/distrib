-- -----------------------------------------------------
-- Create Table BASI_208
-- -----------------------------------------------------
CREATE TABLE BASI_208 (
  deposito_origem NUMBER(3) NOT NULL,
  empresa_destino NUMBER(3) NOT NULL,
  deposito_destino NUMBER(3) NOT NULL,
  PRIMARY KEY (deposito_origem, empresa_destino)
  );

/

declare
    id_grid    number;
    id_profile number;
    id_column  number;

    procedure insert_column(p_id number,
                            p_id_grid number,
                            p_key varchar2,
                            p_name varchar2,
                            p_type varchar2,
                            p_formatter_index number,
                            p_scale number) is
    begin
        insert into conf_grid_column (id, id_grid, key, name, type, formatter_index, scale)
        values (p_id, p_id_grid, p_key, p_name, p_type, p_formatter_index, p_scale);
    end;

    procedure insert_column_profile(p_id_grid_perfil number,
                                    p_id_grid_column number,
                                    p_position number) is
    begin
        insert into conf_grid_perfil_column (id_grid_perfil, id_grid_column, position)
        values (p_id_grid_perfil, p_id_grid_column, p_position);
    end;
begin
    id_grid := id_conf_grid.nextval;

    insert into conf_grid (id, nome) values (id_grid, 'DepositosDestinos');

    id_profile := id_conf_grid_perfil.nextval;

    insert into conf_grid_perfil (id, id_grid, usuario_gerenciador,
                                  empresa_usuario_gerenciador, nome,
                                  publico, padrao, padrao_usuario)
    values (id_profile, id_grid, 'INTERSYS', 1, 'Padr�o', 1, 1, 1);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'deposito_origem', 'Dep�sito Origem',
                  'NUMBER', 1, 0);
    insert_column_profile(id_profile, id_column, 1);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'empresa_destino', 'Empresa Destino',
                  'NUMBER', 1, 0);
    insert_column_profile(id_profile, id_column, 2);

    id_column := id_conf_grid_column.nextval;
    insert_column(id_column, id_grid, 'deposito_destino', 'Dep�sito Destino',
                  'NUMBER', 1, 0);
    insert_column_profile(id_profile, id_column, 3);


end;

/
