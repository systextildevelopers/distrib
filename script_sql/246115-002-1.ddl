declare 

v_count number(3);

begin

   v_count := 0;

   for titulo in (
       select * from cpag_010 
       where cpag_010.data_vencimento > '31-dec-2023'
   ) loop
      v_count := v_count + 1;
   
      update cpag_010
      set VALOR_DESCONTO_MOEDA_AUX = 0.00,
          VALOR_JUROS_MOEDA_AUX = 0.00,
          VALOR_SALDO_MOEDA_AUX = 0.00,
          VALOR_VAR_CAMBIAL = 0.00
      where cpag_010.cgc_9        = titulo.cgc_9
        and cpag_010.cgc_4        = titulo.cgc_4
        and cpag_010.cgc_2        = titulo.cgc_2
        and cpag_010.nr_duplicata = titulo.nr_duplicata
        and cpag_010.parcela      = titulo.parcela
        and cpag_010.tipo_titulo  = titulo.tipo_titulo;
     
      if v_count = 100
      then
         commit;
         v_count := 0;
      end if;
   
   end loop;
   commit;
   
end;
