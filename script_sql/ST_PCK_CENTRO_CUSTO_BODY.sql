create or replace package body ST_PCK_CENTRO_CUSTO is

FUNCTION get_dados_ccusto(p_ccusto number, p_msg_erro in out varchar2) return t_dados_basi_185
AS 
    v_dados_basi_185 t_dados_basi_185;
BEGIN
    begin
        SELECT *  
        BULK COLLECT 
        INTO v_dados_basi_185
        FROM basi_185
        WHERE centro_custo = p_ccusto;
    exception when others then
        p_msg_erro := 'Não foi possível buscar centro custo!. p_ccusto:' || p_ccusto;
		return null;
    end;

    return v_dados_basi_185;

END;

end ST_PCK_CENTRO_CUSTO;
