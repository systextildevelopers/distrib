CREATE OR REPLACE PROCEDURE INTER_PR_INSERE_OPER_550 (
       p_programa varchar2,
       p_subprograma varchar2,
       p_field varchar2,
       p_requerido number default 0,
       p_acessivel number default 1,
       p_inicia_campo number default 0
)
IS
  cursor programa is
    select * from hdoc_033 where hdoc_033.programa = p_programa;
  v_usuario varchar2(250);

BEGIN
  for reg_programa in programa loop
    begin
      select oper_550.usuario
        into v_usuario
        from oper_550
       where oper_550.usuario = reg_programa.usu_prg_cdusu
         and oper_550.empresa = reg_programa.usu_prg_empr_usu
         and oper_550.nome_programa = reg_programa.programa
         and oper_550.nome_field = p_field;
    exception
      when no_data_found then
        insert into oper_550
          (usuario,
           empresa,
           nome_programa,
           nome_subprograma,
           nome_field,
           requerido,
           acessivel,
           inicia_campo)
        values
          (reg_programa.usu_prg_cdusu,
           reg_programa.usu_prg_empr_usu,
           reg_programa.programa,
           p_subprograma,
           p_field,
           p_requerido,
           p_acessivel,
           p_inicia_campo);
        commit;
    end;
  end loop;
END;
/
