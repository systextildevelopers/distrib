
  CREATE OR REPLACE TRIGGER "INTER_TR_CPAG_010_SPED_CTB" 
   before insert or
   update of projeto, finalidade, valor_pis_imp, base_irrf_moeda,
	aliq_irrf, cod_ret_inss, valor_csrf_imp, numero_ci,
	executa_trigger, numero_comprov_retencao, data_comprov_retencao, selecionado,
	valor_desconto_aux, valor_juros_aux, valor_saldo_aux, aceita_dda,
	sequencia_dda, lote, data_lote, sel_lote_pgto,
	valor_avp, indice_mensal, indice_diario, valor_codbar,
	vlr_lido_barra_sispag, tipo_conta_sys, valor_cheque, situacao_sispag,
	nr_solicitacao, portador_pagto, dig_cta_sispag2, agencia_sispag,
	cheque_carta, cgc2_subs, valor_iss, valor_inss,
	referente_nf, cgc4_favorecido, desconto_sispag, dig_age_sispag_s,
	data_vencimento, valor_parcela, valor_irrf, valor_estimado,
	valor_abatimento, data_digitacao, forn_ou_cli, cod_cancelamento,
	codigo_depto, codigo_historico, codigo_transacao, emitente_titulo,
	data_transacao, origem_debito, cod_portador, previsao,
	posicao_titulo, valor_moeda, moeda_titulo, data_canc_tit,
	numero_cheque, banco_cheque, conta_cheque, seq_cheque,
	opcao_cheque, codigo_contabil, finalidade_lote, compl_historico,
	nr_titulo_banco, aviso_favorecido, cheque_nominal, tit_origem,
	codigo_barras, tipo_pagto, forma_pagto, vlr_barra_sispag,
	data_real_pagto, sit_bloqueio, banco_sispag, dig_age_sispag,
	conta_sispag, numero_adiantam, num_tit_subs, cgc9_subs,
	cgc4_subs, tipo_tit_subs, opcao_carta, num_contabil,
	valor_desc, valor_juros, conta_pagto, dig_cta_sispag,
	valor_juros_cheque, valor_desconto_cheque, cgc9_favorecido, cgc2_favorecido,
	numero_contrato, juros_sispag, usuario_digitacao, sequencia_citibank,
	tipo_pagamento, valor_irrf_moeda, data_vencto_original, subprojeto,
	servico, seq_forma_pag, div_prod, valor_inss_imp,
	valor_cofins_imp, valor_csl_imp, sit_impostos_atu, tem_rateio_desp,
	data_ult_movim_pagto, saldo_titulo, nr_processo_export, abate_comis_export,
	nr_processo_import, data_vencto_cheque, base_irrf, base_iss,
	base_inss, base_pis, base_cofins, base_csl,
	aliq_iss, aliq_inss, aliq_pis, aliq_cofins,
	aliq_csl, cod_ret_irrf, cod_ret_iss, cod_ret_pis,
	cod_ret_cofins, cod_ret_csl, cod_end_cobranca, cod_ret_csrf,
	base_csrf, aliq_csrf, nr_duplicata, parcela,
	cgc_9, cgc_4, cgc_2, tipo_titulo,
	codigo_empresa, documento, serie, situacao,
	data_contrato
   on cpag_010
   for each row

declare
   v_cliente_fornec  cont_600.cliente_fornecedor_part%type;
   v_cnpj_9          cont_600.cnpj9_participante%type;
   v_cnpj_4          cont_600.cnpj4_participante%type;
   v_cnpj_2          cont_600.cnpj2_participante%type;
   v_sid             cont_601.sid%type;
   v_instancia       cont_601.instancia%type;

begin

   if inserting
   then
      v_cnpj_9 := :new.cgc_9;
      v_cnpj_4 := :new.cgc_4;
      v_cnpj_2 := :new.cgc_2;
   else
      v_cnpj_9 := :old.cgc_9;
      v_cnpj_4 := :old.cgc_4;
      v_cnpj_2 := :old.cgc_2;
   end if;

   v_cliente_fornec := 2; -- fornecedor

   select sid,   inst_id
   into   v_sid, v_instancia
   from   v_lista_sessao_banco;

   insert into cont_601
      (sid,
       instancia,
       data_insercao,
       tabela_origem,
       cnpj_9,
       cnpj_4,
       cnpj_2,
       cliente_fornec)
   values
      (v_sid,
       v_instancia,
       sysdate,
       'CPAG_010',
       v_cnpj_9,
       v_cnpj_4,
       v_cnpj_2,
       v_cliente_fornec);

   exception
      when others then
         null;
end inter_tr_cpag_010_sped_ctb;
-- ALTER TRIGGER "INTER_TR_CPAG_010_SPED_CTB" ENABLE
 

/

exec inter_pr_recompile;

