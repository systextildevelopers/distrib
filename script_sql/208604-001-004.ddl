create table cpvd_050
(
  codigo_empresa           NUMBER(3) default 0 not null,
  pedido_venda             NUMBER(9) default 0 not null,
  data_exclusao            DATE not null,
  num_nota_fiscal          NUMBER(9) default 0 not null,
  serie                    VARCHAR2(3) default '' not null
);

alter table CPVD_050
  add constraint PK_CPVD_050 primary key (codigo_empresa, pedido_venda, data_exclusao, num_nota_fiscal, serie);

/

exec inter_pr_recompile;
