-- Add/modify columns 
alter table FATU_030 add cod_banco number(3) default 0;

-- Create/Recreate primary, unique and foreign key constraints 
alter table FATU_030
  add constraint REF_FATU_030_PEDI_050 foreign key (COD_BANCO)
  references pedi_050 (COD_PORTADOR);

