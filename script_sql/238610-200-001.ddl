alter table blocok_230 add flag_perda number(1);

alter table blocok_230 modify flag_perda number(1) default 0;

comment on column blocok_230.flag_perda is '0: Não tem quantidade de perda OU 1: Tem quantidade de perda.';


alter table blocok_235 add flag_perda number(1);

alter table blocok_235 modify flag_perda number(1) default 0;

comment on column blocok_235.flag_perda is '0: Não tem quantidade de perda OU 1: Tem quantidade de perda.';


update blocok_230
set flag_perda = 0
where flag_perda is null;
commit;

update blocok_235
set flag_perda = 0
where flag_perda is null;
commit;
