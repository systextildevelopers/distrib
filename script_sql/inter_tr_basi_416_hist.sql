
CREATE OR REPLACE TRIGGER "INTER_TR_BASI_416_HIST" 
   before insert or
          delete or
          update on basi_416
   for each row

declare
   v_identificacao_informacao number(9);
   v_chave_processo_origem    varchar2(60);
   v_status_informacao_chave  number(1);
   v_seq_informacao           number(3);
   v_tipo_informacao          number(2);
   v_resumo_informacao        varchar2(60);
   v_data_cadastro            date;
   v_usuario_cadastro         varchar2(15);
   v_descr_informacao         varchar2(4000);
   v_status_publicacao        number(1);
   v_data_publicacao          date;
   v_situacao                 number(1);
   v_data_hora_ocorr          date;
   v_operacao                 varchar2(1);

   ws_sid                    number(9);
   ws_empresa                number(3);
   v_usuario_operacao      varchar2(250);
   ws_locale_usuario         varchar2(5);
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);

begin

   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           v_usuario_operacao,   ws_empresa,        ws_locale_usuario);

   if inserting
   then
      v_identificacao_informacao := :new.numero_identificacao;
      v_seq_informacao           := :new.seq_informacao;
      v_tipo_informacao          := :new.tipo_informacao;
      v_resumo_informacao        := :new.resumo_informacao;
      v_data_cadastro            := :new.data_cadastro;
      v_usuario_cadastro         := :new.usuario_cadastro;
      v_descr_informacao         := :new.descr_informacao;
      v_status_publicacao        := :new.status_publicacao;
      v_data_publicacao          := :new.data_publicacao;
      v_situacao                 := :new.situacao;
      v_data_hora_ocorr          := sysdate;
      v_operacao                 := 'I';
      v_usuario_operacao         := v_usuario_operacao;
   end if;

   if updating
   then
      v_identificacao_informacao := :new.numero_identificacao;
      v_seq_informacao           := :new.seq_informacao;
      v_tipo_informacao          := :new.tipo_informacao;
      v_resumo_informacao        := :new.resumo_informacao;
      v_data_cadastro            := :new.data_cadastro;
      v_usuario_cadastro         := :new.usuario_cadastro;
      v_descr_informacao         := :new.descr_informacao;
      v_status_publicacao        := :new.status_publicacao;
      v_data_publicacao          := :new.data_publicacao;
      v_situacao                 := :new.situacao;
      v_data_hora_ocorr          := sysdate;
      v_operacao                 := 'A';
      v_usuario_operacao         := v_usuario_operacao;
   end if;

   if deleting
   then
      v_identificacao_informacao := :old.numero_identificacao;
      v_seq_informacao           := :old.seq_informacao;
      v_tipo_informacao          := :old.tipo_informacao;
      v_resumo_informacao        := :old.resumo_informacao;
      v_data_cadastro            := :old.data_cadastro;
      v_usuario_cadastro         := :old.usuario_cadastro;
      v_descr_informacao         := :old.descr_informacao;
      v_status_publicacao        := :old.status_publicacao;
      v_data_publicacao          := :old.data_publicacao;
      v_situacao                 := :old.situacao;
      v_data_hora_ocorr          := sysdate;
      v_operacao                 := 'D';
      v_usuario_operacao         := v_usuario_operacao;
   end if;

   begin
      select basi_415.chave_processo_origem, basi_415.cod_status
        into v_chave_processo_origem,        v_status_informacao_chave
        from basi_415
       where basi_415.identificacao_informacao = v_identificacao_informacao;
      exception when no_data_found then
        v_chave_processo_origem   := '';
        v_status_informacao_chave := 0;
   end;

   insert into basi_416_hist
               (identificacao_informacao,      chave_processo_origem,
                status_informacao_chave,       seq_informacao,
                tipo_informacao,               resumo_informacao,
                data_cadastro,                 usuario_cadastro,
                descr_informacao,              status_publicacao,
                data_publicacao,               situacao,
                data_hora_ocorr,               operacao,
                usuario_operacao)
        values (v_identificacao_informacao,    v_chave_processo_origem,
                v_status_informacao_chave,     v_seq_informacao,
                v_tipo_informacao,             v_resumo_informacao,
                v_data_cadastro,               v_usuario_cadastro,
                v_descr_informacao,            v_status_publicacao,
                v_data_publicacao,             v_situacao,
                v_data_hora_ocorr,             v_operacao,
                v_usuario_operacao);

end inter_tr_basi_416_hist;

-- ALTER TRIGGER "INTER_TR_BASI_416_HIST" ENABLE
 

/

exec inter_pr_recompile;

