insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('supr_f121', 'Rateio do item do pedido de compra por centro de custo',0,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('TREINAMENTO', 1, 'supr_f121', ' ' ,0, 99, 'S', 'S', 'S', 'S');

update hdoc_036
set hdoc_036.descricao       = 'Rateio do item do pedido de compra por centro de custo'
where hdoc_036.codigo_programa = 'supr_f121'
  and hdoc_036.locale          = 'es_ES';

commit;
