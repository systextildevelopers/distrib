alter table obrf_015 add COD_SERVICO_LST VARCHAR2(5);

alter table obrf_015 modify COD_SERVICO_LST default ' ';

COMMENT ON COLUMN obrf_015.COD_SERVICO_LST IS 'Codigo do produto de servico conforme lista anexo I Lei comple. nr 116/03 SPED';

exec inter_pr_recompile;
