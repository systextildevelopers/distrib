create or replace trigger wms_tr_abc_log
  before insert 
  or delete
  or update of nivel_sku, grupo_sku, subgrupo_sku, item_sku, 
	cod_deposito, quantidade, timestamp_insercao 
  on inte_wms_abc
  for each row
declare
  -- local variables here
  v_nivel_sku_new              inte_wms_abc.nivel_sku               %type;
  v_nivel_sku_old              inte_wms_abc.nivel_sku               %type;
  v_grupo_sku_new              inte_wms_abc.grupo_sku               %type;
  v_grupo_sku_old              inte_wms_abc.grupo_sku               %type;
  v_subgrupo_sku_new           inte_wms_abc.subgrupo_sku            %type;
  v_subgrupo_sku_old           inte_wms_abc.subgrupo_sku            %type;
  v_item_sku_new               inte_wms_abc.item_sku                %type;
  v_item_sku_old               inte_wms_abc.item_sku                %type;
  v_cod_deposito_new           inte_wms_abc.cod_deposito            %type;
  v_cod_deposito_old           inte_wms_abc.cod_deposito            %type;
  v_quantidade_new             inte_wms_abc.quantidade              %type;
  v_quantidade_old             inte_wms_abc.quantidade              %type;
  v_timestamp_ins_new          inte_wms_abc.timestamp_insercao      %type;
  v_timestamp_ins_old          inte_wms_abc.timestamp_insercao      %type;

  v_sid                            number(9);
  v_empresa                        number(3);
  v_usuario_systextil              varchar2(250);
  v_locale_usuario                 varchar2(5);
  v_nome_programa                  varchar2(20);
 

  v_operacao                       varchar(1);
  v_data_operacao                  date;
  v_usuario_rede                   varchar(20);
  v_maquina_rede                   varchar(40);
  v_aplicativo                     varchar(20);
begin

   -- grava a data/hora da insercao do registro (log)
   v_data_operacao := sysdate();

   --alimenta as variaveis new caso seja insert ou update
   if inserting or updating
   then
      if inserting
      then v_operacao := 'i';
      else v_operacao := 'u';
      end if;

      v_nivel_sku_new      := :new.nivel_sku;
      v_grupo_sku_new      := :new.grupo_sku;
      v_subgrupo_sku_new   := :new.subgrupo_sku;
      v_item_sku_new       := :new.item_sku;
      v_cod_deposito_new   := :new.cod_deposito;
      v_quantidade_new     := :new.quantidade;
      v_timestamp_ins_new  := :new.timestamp_insercao;
   end if; --fim do if inserting or updating

   --alimenta as variaveis old caso seja insert ou update
   if deleting or updating
   then
      if deleting
      then
         v_operacao      := 'd';
      else
         v_operacao      := 'u';
      end if;

      v_nivel_sku_old      := :old.nivel_sku;
      v_grupo_sku_old      := :old.grupo_sku;
      v_subgrupo_sku_old   := :old.subgrupo_sku;
      v_item_sku_old       := :old.item_sku;
      v_cod_deposito_old   := :old.cod_deposito;
      v_quantidade_old     := :old.quantidade;
      v_timestamp_ins_old  := :old.timestamp_insercao;

   end if; --fim do if deleting or updating


   -- Dados do usu�rio logado
   inter_pr_dados_usu_inte (v_usuario_rede,        v_maquina_rede,   v_aplicativo,     v_sid,
                            v_usuario_systextil,   v_empresa,        v_locale_usuario);


    v_nome_programa := ''; --Deixado de fora por quest�es de performance, a pedido do cliente

   --insere na inte_wms_abc_log o registro.
   insert into inte_wms_abc_log (
      nivel_sku_old,           nivel_sku_new,
      grupo_sku_old,           grupo_sku_new,
      subgrupo_sku_old,        subgrupo_sku_new,
      item_sku_old,            item_sku_new,
      cod_deposito_old,        cod_deposito_new,
      quantidade_old,          quantidade_new,
      timestamp_ins_old,       timestamp_ins_new,
      operacao,
      data_operacao,           usuario_rede,
      maquina_rede,            aplicativo,
      nome_programa
   )
   values (
      v_nivel_sku_old,         v_nivel_sku_new,
      v_grupo_sku_old,         v_grupo_sku_new,
      v_subgrupo_sku_old,      v_subgrupo_sku_new,
      v_item_sku_old,          v_item_sku_new,
      v_cod_deposito_old,      v_cod_deposito_new,
      v_quantidade_old,        v_quantidade_new,
      v_timestamp_ins_old,     v_timestamp_ins_new,
      v_operacao,
      v_data_operacao,         v_usuario_rede,
      v_maquina_rede,          v_aplicativo,
      v_nome_programa
   );

end wms_tr_abc_log;

/

execute inter_pr_recompile;

/* versao: 2 */


 exit;
