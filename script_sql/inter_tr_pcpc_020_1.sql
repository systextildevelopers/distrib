
  CREATE OR REPLACE TRIGGER "INTER_TR_PCPC_020_1" 
   before update of ordem_producao, periodo_producao, numero_programa, referencia_peca,
	alternativa_peca, roteiro_peca, qtde_programada, data_programacao,
	observacao, ultimo_estagio, situacao, cod_cancelamento,
	dt_cancelamento, div_prod_int, prioridade_produ, codigo_risco,
	lote_original, ordem_origem, tipo_programacao, sit_ordem_benefic,
	sit_ordem_tecelag, sit_ordem_fiacao, usuario_cancelamento, pedido_venda,
	deposito_entrada, ordem_principal, ordem_associada, situacao_bloqueio,
	observacao2, historico_ordem, expedidor, data_entrada_rolos,
	periodo_antigo, data_alteracao, data_entrada_corte, situacao_impressao,
	lote_producao, ordem_corte, codigo_molde, codigo_motivo,
	situacao_reposicao, tipo_ordem, executa_trigger, data_hora,
	ordem_mestre, selecionado_afaturar, ordem_agrup_corte, estagio_leitura_bath,
	cd_agrup_col, consumo_comp_conjunto, sit_req_tecidos, data_embarque_pedido,
	situacao_corte
   or delete
   on pcpc_020
   for each row

declare
   t_count_estq_450       number(9);
   v_executa_trigger      number;

begin
   -- INICIO - Logica implementada para controle de "gatilho" das triggers do banco
   -- para atender a implementacao executada para limpeza da base de dados do cliente
   -- e replicacao dos dados para base historico. (SS.38405)
   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   -- FINAL - Logica implementada para controle de "gatilho" das triggers do banco. (SS.38405)
   if v_executa_trigger = 0
   then
      -- TRIGGER PARA ESCLUIR O ENDERECAMENTO DE ESTOQUE
      if deleting
      then
         begin
            select count(1)
            into   t_count_estq_450
            from estq_450
            where estq_450.ordem_producao = :old.ordem_producao;
         end;

         if t_count_estq_450 > 0
         then
            begin
               delete from estq_450
               where estq_450.ordem_producao = :old.ordem_producao;
            end;
         end if;
      end if;

      if updating
      then

         if :old.periodo_producao <> :new.periodo_producao
         then
            begin
               update estq_450
                  set estq_450.periodo_producao = :new.periodo_producao
               where estq_450.ordem_producao    = :new.ordem_producao;
            end;
         end if;
      end if;
   end if;
end inter_tr_pcpc_020_1;

-- ALTER TRIGGER "INTER_TR_PCPC_020_1" ENABLE
 

/

exec inter_pr_recompile;

