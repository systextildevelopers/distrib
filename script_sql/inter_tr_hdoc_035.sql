
  CREATE OR REPLACE TRIGGER "INTER_TR_HDOC_035" 
   before insert or
   update of codigo_programa, descricao, programa_menu, item_menu_def,
	item_comando, nome_comando
   or delete on hdoc_035
   for each row

declare
  -- local variables here
  v_descricao varchar2(100);
begin

   --Ao fazer o insert de um registro na tabela hdoc_035
   if inserting and :new.codigo_programa is not null and :new.descricao is not null
   then
      --:new.hora_producao := to_date('16/11/1989 ' || to_CHAR(sysdate,'hh24:mi'),'dd/mm/yyyy hh24:mi');

      for cc2 in (select locale from hdoc_880)
      loop
         if cc2.locale = 'pt_BR'
         then v_descricao := :new.descricao;
         else v_descricao := :new.descricao;
         end if;

         begin
            insert into HDOC_036
               (codigo_programa, locale, descricao)
            values
               (:new.codigo_programa, cc2.locale, v_descricao);
            exception
               when others then
                 v_descricao := v_descricao;
         end;

      end loop;

   end if;

   --Ao alterar um registro na tabela hdoc_035
   if updating
   then
      begin
         update hdoc_036
            set descricao = :new.descricao
         where codigo_programa = :new.codigo_programa
           and descricao = :old.descricao;
         exception
            when others then
               v_descricao := v_descricao;
      end;
   end if;

   --Ao eliminar um registro na tabela hdoc_035
   if deleting
   then
      begin
         delete HDOC_036
         where codigo_programa = :old.codigo_programa;
         exception
            when others then
               v_descricao := v_descricao;
      end;
   end if;

end inter_tr_hdoc_035;
-- ALTER TRIGGER "INTER_TR_HDOC_035" ENABLE
 

/

exec inter_pr_recompile;

