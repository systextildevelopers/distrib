create or replace trigger "INTER_TR_PEDI_010_HIST"
  after insert or delete or update of
     cgc_9, cgc_4, cgc_2, fisica_juridica, nome_cliente, fantasia_cliente, situacao_cliente, endereco_cliente, numero_imovel, complemento, cep_cliente, cod_cidade,
     insc_est_cliente, portador_cliente, data_cad_cliente, data_exc_cliente, mot_exc_cliente, cdrepres_cliente, bairro, sub_regiao,
     tipo_cliente, cod_sit_credito, conceito_cliente, envia_mala, nr_suframa_cli, data_valid_suframa, tran_cli_forne9, tran_cli_forne4,
     tran_cli_forne2, e_mail, codigo_contabil, codigo_cliente, unidade_lim_ped, limite_max_ped1, limite_max_ped2, limite_max_ped4,
     limite_max_ped7, val_lim_credito, dt_alt_lim_credito, cod_sit_cobranca, digitacao_pedido, valor_faturamento_anual, codigo_empresa,
     tipo_frete, nfe_e_mail, prioridade_sugestao, data_vencto, situacao_cadastro, conta_contabil, forma_pagamento, perc_desc_duplic,
     data_compl_cadastro, criterio_atendimento, data_reativacao, empenho_automatico, cliente_isento, isento_sci, isento_serasa, dt_ult_consulta_sintegra,
     alternativa_cliente, cli_quebra_peca, cli_quebra_pano, cli_quebra_fios, cli_quebra_teci, cliente_integracao, cod_cidade_zona_franca,
     cod_relacion_sped_ctb, ind_desc_pis_cofins, cadastro_dda, auditar_volume, maior_atraso, data_maior_atr, mot_inat_cliente
  on pedi_010
  for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   ws_processo_systextil     hdoc_090.programa%type;

begin
   -- Dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   ws_processo_systextil := inter_fn_nome_programa(ws_sid);

   if inserting
   then

      begin
         insert into pedi_010_hist(
            cgc_9,                        cgc_4,
            cgc_2,
            fisica_juridica_old,          fisica_juridica_new,
            nome_cliente_old,             nome_cliente_new,
            fantasia_cliente_old,         fantasia_cliente_new,
            situacao_cliente_old,         situacao_cliente_new,
            endereco_cliente_old,         endereco_cliente_new,
            numero_imovel_old,            numero_imovel_new,
            complemento_old,              complemento_new,
            cep_cliente_old,              cep_cliente_new,
            cod_cidade_old,               cod_cidade_new,
            insc_est_cliente_old,         insc_est_cliente_new,
            portador_cliente_old,         portador_cliente_new,
            data_cad_cliente_old,         data_cad_cliente_new,
            data_exc_cliente_old,         data_exc_cliente_new,
            mot_exc_cliente_old,          mot_exc_cliente_new,
            cdrepres_cliente_old,         cdrepres_cliente_new,
            bairro_old,                   bairro_new,
            sub_regiao_old,               sub_regiao_new,
            tipo_cliente_old,             tipo_cliente_new,
            cod_sit_credito_old,          cod_sit_credito_new,
            conceito_cliente_old,         conceito_cliente_new,
            envia_mala_old,               envia_mala_new,
            nr_suframa_cli_old,           nr_suframa_cli_new,
            data_valid_suframa_old,       data_valid_suframa_new,
            tran_cli_forne9_old,          tran_cli_forne9_new,
            tran_cli_forne4_old,          tran_cli_forne4_new,
            tran_cli_forne2_old,          tran_cli_forne2_new,
            e_mail_old,                   e_mail_new,
            codigo_contabil_old,          codigo_contabil_new,
            codigo_cliente_old,           codigo_cliente_new,
            unidade_lim_ped_old,          unidade_lim_ped_new,
            limite_max_ped1_old,          limite_max_ped1_new,
            limite_max_ped2_old,          limite_max_ped2_new,
            limite_max_ped4_old,          limite_max_ped4_new,
            limite_max_ped7_old,          limite_max_ped7_new,
            val_lim_credito_old,          val_lim_credito_new,
            dt_alt_lim_credito_old,       dt_alt_lim_credito_new,
            cod_sit_cobranca_old,         cod_sit_cobranca_new,
            digitacao_pedido_old,         digitacao_pedido_new,
            valor_faturamento_anual_old,  valor_faturamento_anual_new,
            codigo_empresa_old,           codigo_empresa_new,
            tipo_frete_old,               tipo_frete_new,
            nfe_e_mail_old,               nfe_e_mail_new,
            prioridade_sugestao_old,      prioridade_sugestao_new,
            data_vencto_old,              data_vencto_new,
            situacao_cadastro_old,        situacao_cadastro_new,
            conta_contabil_old,           conta_contabil_new,
            forma_pagamento_old,          forma_pagamento_new,
            perc_desc_duplic_old,         perc_desc_duplic_new,
            data_compl_cadastro_old,      data_compl_cadastro_new,
            criterio_atendimento_old,     criterio_atendimento_new,
            data_reativacao_old,          data_reativacao_new,
            empenho_automatico_old,       empenho_automatico_new,
            cliente_isento_old,           cliente_isento_new,
            isento_sci_old,               isento_sci_new,
            isento_serasa_old,            isento_serasa_new,
            dt_ult_consulta_sintegra_old, dt_ult_consulta_sintegra_new,
            alternativa_cliente_old,      alternativa_cliente_new,
            cli_quebra_peca_old,          cli_quebra_peca_new,
            cli_quebra_pano_old,          cli_quebra_pano_new,
            cli_quebra_fios_old,          cli_quebra_fios_new,
            cli_quebra_teci_old,          cli_quebra_teci_new,
            cliente_integracao_old,       cliente_integracao_new,
            cod_cidade_zona_franca_old,   cod_cidade_zona_franca_new,
            cod_relacion_sped_ctb_old,    cod_relacion_sped_ctb_new,
            ind_desc_pis_cofins_old,      ind_desc_pis_cofins_new,
            cadastro_dda_old,             cadastro_dda_new,
            auditar_volume_old,           auditar_volume_new,
            maior_atraso_old,             maior_atraso_new,
            data_maior_atr_old,           data_maior_atr_new,
            mot_inat_cliente_old,          mot_inat_cliente_new,
            aplicacao,                    tipo_ocorr,
            data_ocorr,                   usuario_rede,
            maquina_rede,                 nome_programa,
            usuario_systextil,            
            grupo_economico_old,          grupo_economico_new
         )values(
            :new.cgc_9,                    :new.cgc_4,
            :new.cgc_2,
            null,                         :new.fisica_juridica,
            '',                           :new.nome_cliente,
            ' ',                          :new.fantasia_cliente,
            1,                            :new.situacao_cliente,
            '',                           :new.endereco_cliente,
            ' ',                          :new.numero_imovel,
            ' ',                          :new.complemento,
            0,                            :new.cep_cliente,
            0,                            :new.cod_cidade,
            '',                           :new.insc_est_cliente,
            0,                            :new.portador_cliente,
            null,                         :new.data_cad_cliente,
            null,                         :new.data_exc_cliente,
            0,                            :new.mot_exc_cliente,
            0,                            :new.cdrepres_cliente,
            '',                           :new.bairro,
            0,                            :new.sub_regiao,
            0,                            :new.tipo_cliente,
            1,                            :new.cod_sit_credito,
            0,                            :new.conceito_cliente,
            1,                            :new.envia_mala,
            '',                           :new.nr_suframa_cli,
            null,                         :new.data_valid_suframa,
            0,                            :new.tran_cli_forne9,
            0,                            :new.tran_cli_forne4,
            0,                            :new.tran_cli_forne2,
            '',                           :new.e_mail,
            0,                            :new.codigo_contabil,
            0,                            :new.codigo_cliente,
            0,                            :new.unidade_lim_ped,
            0,                            :new.limite_max_ped1,
            0,                            :new.limite_max_ped2,
            0,                            :new.limite_max_ped4,
            0,                            :new.limite_max_ped7,
            sysdate,                      :new.val_lim_credito,
            sysdate,                      :new.dt_alt_lim_credito,
            0,                            :new.cod_sit_cobranca,
            0,                            :new.digitacao_pedido,
            0.00,                         :new.valor_faturamento_anual,
            09,                           :new.codigo_empresa,
            0,                            :new.tipo_frete,
            '',                           :new.nfe_e_mail,
            9,                            :new.prioridade_sugestao,
            null,                         :new.data_vencto,
            4,                            :new.situacao_cadastro,
            '',                           :new.conta_contabil,
            9,                            :new.forma_pagamento,
            0.0,                          :new.perc_desc_duplic,
            null,                         :new.data_compl_cadastro,
            0,                            :new.criterio_atendimento,
            null,                         :new.data_reativacao,
            0,                            :new.empenho_automatico,
            0,                            :new.cliente_isento,
            0,                            :new.isento_sci,
            0,                            :new.isento_serasa,
            null,                         :new.dt_ult_consulta_sintegra,
            0,                            :new.alternativa_cliente,
            0,                            :new.cli_quebra_peca,
            0,                            :new.cli_quebra_pano,
            0,                            :new.cli_quebra_fios,
            0,                            :new.cli_quebra_teci,
            '',                           :new.cliente_integracao,
            0,                            :new.cod_cidade_zona_franca,
            0,                            :new.cod_relacion_sped_ctb,
            'N',                          :new.ind_desc_pis_cofins,
            'N',                          :new.cadastro_dda,
            0,                            :new.auditar_volume,
            0,                            :new.maior_atraso,
            null,                         :new.data_maior_atr,
            null,                         :new.mot_inat_cliente,
            ws_aplicativo,                'I',
            sysdate,                      ws_usuario_rede,
            ws_maquina_rede,              ws_processo_systextil,
            ws_usuario_systextil,
            0,                            :new.grupo_economico
         );
      exception when OTHERS then
         raise_application_error (-20000, inter_fn_buscar_tag_composta('ds22559#ATENi??i??O! Ni??o inseriu {0}. Status: {1}.', 'PEDI_028_HIST(001-1)' , sqlerrm , '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;
   end if;

   if updating
   then

      begin
         insert into pedi_010_hist(
            cgc_9,                        cgc_4,
            cgc_2,
            fisica_juridica_old,          fisica_juridica_new,
            nome_cliente_old,             nome_cliente_new,
            fantasia_cliente_old,         fantasia_cliente_new,
            situacao_cliente_old,         situacao_cliente_new,
            endereco_cliente_old,         endereco_cliente_new,
            numero_imovel_old,            numero_imovel_new,
            complemento_old,              complemento_new,
            cep_cliente_old,              cep_cliente_new,
            cod_cidade_old,               cod_cidade_new,
            insc_est_cliente_old,         insc_est_cliente_new,
            portador_cliente_old,         portador_cliente_new,
            data_cad_cliente_old,         data_cad_cliente_new,
            data_exc_cliente_old,         data_exc_cliente_new,
            mot_exc_cliente_old,          mot_exc_cliente_new,
            cdrepres_cliente_old,         cdrepres_cliente_new,
            bairro_old,                   bairro_new,
            sub_regiao_old,               sub_regiao_new,
            tipo_cliente_old,             tipo_cliente_new,
            cod_sit_credito_old,          cod_sit_credito_new,
            conceito_cliente_old,         conceito_cliente_new,
            envia_mala_old,               envia_mala_new,
            nr_suframa_cli_old,           nr_suframa_cli_new,
            data_valid_suframa_old,       data_valid_suframa_new,
            tran_cli_forne9_old,          tran_cli_forne9_new,
            tran_cli_forne4_old,          tran_cli_forne4_new,
            tran_cli_forne2_old,          tran_cli_forne2_new,
            e_mail_old,                   e_mail_new,
            codigo_contabil_old,          codigo_contabil_new,
            codigo_cliente_old,           codigo_cliente_new,
            unidade_lim_ped_old,          unidade_lim_ped_new,
            limite_max_ped1_old,          limite_max_ped1_new,
            limite_max_ped2_old,          limite_max_ped2_new,
            limite_max_ped4_old,          limite_max_ped4_new,
            limite_max_ped7_old,          limite_max_ped7_new,
            val_lim_credito_old,          val_lim_credito_new,
            dt_alt_lim_credito_old,       dt_alt_lim_credito_new,
            cod_sit_cobranca_old,         cod_sit_cobranca_new,
            digitacao_pedido_old,         digitacao_pedido_new,
            valor_faturamento_anual_old,  valor_faturamento_anual_new,
            codigo_empresa_old,           codigo_empresa_new,
            tipo_frete_old,               tipo_frete_new,
            nfe_e_mail_old,               nfe_e_mail_new,
            prioridade_sugestao_old,      prioridade_sugestao_new,
            data_vencto_old,              data_vencto_new,
            situacao_cadastro_old,        situacao_cadastro_new,
            conta_contabil_old,           conta_contabil_new,
            forma_pagamento_old,          forma_pagamento_new,
            perc_desc_duplic_old,         perc_desc_duplic_new,
            data_compl_cadastro_old,      data_compl_cadastro_new,
            criterio_atendimento_old,     criterio_atendimento_new,
            data_reativacao_old,          data_reativacao_new,
            empenho_automatico_old,       empenho_automatico_new,
            cliente_isento_old,           cliente_isento_new,
            isento_sci_old,               isento_sci_new,
            isento_serasa_old,            isento_serasa_new,
            dt_ult_consulta_sintegra_old, dt_ult_consulta_sintegra_new,
            alternativa_cliente_old,      alternativa_cliente_new,
            cli_quebra_peca_old,          cli_quebra_peca_new,
            cli_quebra_pano_old,          cli_quebra_pano_new,
            cli_quebra_fios_old,          cli_quebra_fios_new,
            cli_quebra_teci_old,          cli_quebra_teci_new,
            cliente_integracao_old,       cliente_integracao_new,
            cod_cidade_zona_franca_old,   cod_cidade_zona_franca_new,
            cod_relacion_sped_ctb_old,    cod_relacion_sped_ctb_new,
            ind_desc_pis_cofins_old,      ind_desc_pis_cofins_new,
            cadastro_dda_old,             cadastro_dda_new,
            auditar_volume_old,           auditar_volume_new,
            maior_atraso_old,             maior_atraso_new,
            data_maior_atr_old,           data_maior_atr_new,
            mot_inat_cliente_old,      mot_inat_cliente_new,
            aplicacao,                    tipo_ocorr,
            data_ocorr,                   usuario_rede,
            maquina_rede,                 nome_programa,
            usuario_systextil,
            grupo_economico_old,          grupo_economico_new
            )values(
            :new.cgc_9,                   :new.cgc_4,
            :new.cgc_2,
            :old.fisica_juridica,         :new.fisica_juridica,
            :old.nome_cliente,            :new.nome_cliente,
            :old.fantasia_cliente,        :new.fantasia_cliente,
            :old.situacao_cliente,        :new.situacao_cliente,
            :old.endereco_cliente,        :new.endereco_cliente,
            :old.numero_imovel,           :new.numero_imovel,
            :old.complemento,             :new.complemento,
            :old.cep_cliente,             :new.cep_cliente,
            :old.cod_cidade,              :new.cod_cidade,
            :old.insc_est_cliente,        :new.insc_est_cliente,
            :old.portador_cliente,        :new.portador_cliente,
            :old.data_cad_cliente,        :new.data_cad_cliente,
            :old.data_exc_cliente,        :new.data_exc_cliente,
            :old.mot_exc_cliente,         :new.mot_exc_cliente,
            :old.cdrepres_cliente,        :new.cdrepres_cliente,
            :old.bairro,                  :new.bairro,
            :old.sub_regiao,              :new.sub_regiao,
            :old.tipo_cliente,            :new.tipo_cliente,
            :old.cod_sit_credito,         :new.cod_sit_credito,
            :old.conceito_cliente,        :new.conceito_cliente,
            :old.envia_mala,              :new.envia_mala,
            :old.nr_suframa_cli,          :new.nr_suframa_cli,
            :old.data_valid_suframa,      :new.data_valid_suframa,
            :old.tran_cli_forne9,         :new.tran_cli_forne9,
            :old.tran_cli_forne4,         :new.tran_cli_forne4,
            :old.tran_cli_forne2,         :new.tran_cli_forne2,
            :old.e_mail,                  :new.e_mail,
            :old.codigo_contabil,         :new.codigo_contabil,
            :old.codigo_cliente,          :new.codigo_cliente,
            :old.unidade_lim_ped,         :new.unidade_lim_ped,
            :old.limite_max_ped1,         :new.limite_max_ped1,
            :old.limite_max_ped2,         :new.limite_max_ped2,
            :old.limite_max_ped4,         :new.limite_max_ped4,
            :old.limite_max_ped7,         :new.limite_max_ped7,
            :old.val_lim_credito,         :new.val_lim_credito,
            :old.dt_alt_lim_credito,      :new.dt_alt_lim_credito,
            :old.cod_sit_cobranca,        :new.cod_sit_cobranca,
            :old.digitacao_pedido,        :new.digitacao_pedido,
            :old.valor_faturamento_anual, :new.valor_faturamento_anual,
            :old.codigo_empresa,          :new.codigo_empresa,
            :old.tipo_frete,              :new.tipo_frete,
            :old.nfe_e_mail,              :new.nfe_e_mail,
            :old.prioridade_sugestao,     :new.prioridade_sugestao,
            :old.data_vencto,             :new.data_vencto,
            :old.situacao_cadastro,       :new.situacao_cadastro,
            :old.conta_contabil,          :new.conta_contabil,
            :old.forma_pagamento,         :new.forma_pagamento,
            :old.perc_desc_duplic,        :new.perc_desc_duplic,
            :old.data_compl_cadastro,     :new.data_compl_cadastro,
            :old.criterio_atendimento,    :new.criterio_atendimento,
            :old.data_reativacao,         :new.data_reativacao,
            :old.empenho_automatico,      :new.empenho_automatico,
            :old.cliente_isento,          :new.cliente_isento,
            :old.isento_sci,              :new.isento_sci,
            :old.isento_serasa,           :new.isento_serasa,
            :old.dt_ult_consulta_sintegra,:new.dt_ult_consulta_sintegra,
            :old.alternativa_cliente,     :new.alternativa_cliente,
            :old.cli_quebra_peca,         :new.cli_quebra_peca,
            :old.cli_quebra_pano,         :new.cli_quebra_pano,
            :old.cli_quebra_fios,         :new.cli_quebra_fios,
            :old.cli_quebra_teci,         :new.cli_quebra_teci,
            :old.cliente_integracao,      :new.cliente_integracao,
            :old.cod_cidade_zona_franca,  :new.cod_cidade_zona_franca,
            :old.cod_relacion_sped_ctb,   :new.cod_relacion_sped_ctb,
            :old.ind_desc_pis_cofins,     :new.ind_desc_pis_cofins,
            :old.cadastro_dda,            :new.cadastro_dda,
            :old.auditar_volume,          :new.auditar_volume,
            :old.maior_atraso,            :new.maior_atraso,
            :old.data_maior_atr,          :new.data_maior_atr,
            :old.mot_inat_cliente,        :new.mot_inat_cliente,
            ws_aplicativo,                'A',
            sysdate,                      ws_usuario_rede,
            ws_maquina_rede,              ws_processo_systextil,
            ws_usuario_systextil,
            :old.grupo_economico,         :new.grupo_economico
         );

      exception when OTHERS then
         raise_application_error (-20000, inter_fn_buscar_tag_composta('ds22559#ATENi??i??O! Ni??o inseriu {0}. Status: {1}.', 'PEDI_028_HIST(002-1)' , sqlerrm , '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));

      end;


   end if;


   if deleting
   then

      begin
         insert into pedi_010_hist(
            cgc_9,                        cgc_4,
            cgc_2,
            fisica_juridica_old,          fisica_juridica_new,
            nome_cliente_old,             nome_cliente_new,
            fantasia_cliente_old,         fantasia_cliente_new,
            situacao_cliente_old,         situacao_cliente_new,
            endereco_cliente_old,         endereco_cliente_new,
            numero_imovel_old,            numero_imovel_new,
            complemento_old,              complemento_new,
            cep_cliente_old,              cep_cliente_new,
            cod_cidade_old,               cod_cidade_new,
            insc_est_cliente_old,         insc_est_cliente_new,
            portador_cliente_old,         portador_cliente_new,
            data_cad_cliente_old,         data_cad_cliente_new,
            data_exc_cliente_old,         data_exc_cliente_new,
            mot_exc_cliente_old,          mot_exc_cliente_new,
            cdrepres_cliente_old,         cdrepres_cliente_new,
            bairro_old,                   bairro_new,
            sub_regiao_old,               sub_regiao_new,
            tipo_cliente_old,             tipo_cliente_new,
            cod_sit_credito_old,          cod_sit_credito_new,
            conceito_cliente_old,         conceito_cliente_new,
            envia_mala_old,               envia_mala_new,
            nr_suframa_cli_old,           nr_suframa_cli_new,
            data_valid_suframa_old,       data_valid_suframa_new,
            tran_cli_forne9_old,          tran_cli_forne9_new,
            tran_cli_forne4_old,          tran_cli_forne4_new,
            tran_cli_forne2_old,          tran_cli_forne2_new,
            e_mail_old,                   e_mail_new,
            codigo_contabil_old,          codigo_contabil_new,
            codigo_cliente_old,           codigo_cliente_new,
            unidade_lim_ped_old,          unidade_lim_ped_new,
            limite_max_ped1_old,          limite_max_ped1_new,
            limite_max_ped2_old,          limite_max_ped2_new,
            limite_max_ped4_old,          limite_max_ped4_new,
            limite_max_ped7_old,          limite_max_ped7_new,
            val_lim_credito_old,          val_lim_credito_new,
            dt_alt_lim_credito_old,       dt_alt_lim_credito_new,
            cod_sit_cobranca_old,         cod_sit_cobranca_new,
            digitacao_pedido_old,         digitacao_pedido_new,
            valor_faturamento_anual_old,  valor_faturamento_anual_new,
            codigo_empresa_old,           codigo_empresa_new,
            tipo_frete_old,               tipo_frete_new,
            nfe_e_mail_old,               nfe_e_mail_new,
            prioridade_sugestao_old,      prioridade_sugestao_new,
            data_vencto_old,              data_vencto_new,
            situacao_cadastro_old,        situacao_cadastro_new,
            conta_contabil_old,           conta_contabil_new,
            forma_pagamento_old,          forma_pagamento_new,
            perc_desc_duplic_old,         perc_desc_duplic_new,
            data_compl_cadastro_old,      data_compl_cadastro_new,
            criterio_atendimento_old,     criterio_atendimento_new,
            data_reativacao_old,          data_reativacao_new,
            empenho_automatico_old,       empenho_automatico_new,
            cliente_isento_old,           cliente_isento_new,
            isento_sci_old,               isento_sci_new,
            isento_serasa_old,            isento_serasa_new,
            dt_ult_consulta_sintegra_old, dt_ult_consulta_sintegra_new,
            alternativa_cliente_old,      alternativa_cliente_new,
            cli_quebra_peca_old,          cli_quebra_peca_new,
            cli_quebra_pano_old,          cli_quebra_pano_new,
            cli_quebra_fios_old,          cli_quebra_fios_new,
            cli_quebra_teci_old,          cli_quebra_teci_new,
            cliente_integracao_old,       cliente_integracao_new,
            cod_cidade_zona_franca_old,   cod_cidade_zona_franca_new,
            cod_relacion_sped_ctb_old,    cod_relacion_sped_ctb_new,
            ind_desc_pis_cofins_old,      ind_desc_pis_cofins_new,
            cadastro_dda_old,             cadastro_dda_new,
            auditar_volume_old,           auditar_volume_new,
            maior_atraso_old,             maior_atraso_new,
            data_maior_atr_old,           data_maior_atr_new,
            mot_inat_cliente_old,         mot_inat_cliente_new,
            aplicacao,                    tipo_ocorr,
            data_ocorr,                   usuario_rede,
            maquina_rede,                 nome_programa,
            usuario_systextil,
            grupo_economico_old,          grupo_economico_new
         )values(
            :old.cgc_9,                   :old.cgc_4,
            :old.cgc_2,
            :old.fisica_juridica,         null,
            :old.nome_cliente,            '',
            :old.fantasia_cliente,        ' ',
            :old.situacao_cliente,        1,
            :old.endereco_cliente,        '',
            :old.numero_imovel,           ' ',
            :old.complemento,             ' ',
            :old.cep_cliente,             0,
            :old.cod_cidade,              0,
            :old.insc_est_cliente,        '',
            :old.portador_cliente,        0,
            :old.data_cad_cliente,        null,
            :old.data_exc_cliente,        null,
            :old.mot_exc_cliente,         0,
            :old.cdrepres_cliente,        0,
            :old.bairro,                  '',
            :old.sub_regiao,              0,
            :old.tipo_cliente,            0,
            :old.cod_sit_credito,         1,
            :old.conceito_cliente,        0,
            :old.envia_mala,              1,
            :old.nr_suframa_cli,          '',
            :old.data_valid_suframa,      null,
            :old.tran_cli_forne9,         0,
            :old.tran_cli_forne4,         0,
            :old.tran_cli_forne2,         0,
            :old.e_mail,                  '',
            :old.codigo_contabil,         0,
            :old.codigo_cliente,          0,
            :old.unidade_lim_ped,         0,
            :old.limite_max_ped1,         0,
            :old.limite_max_ped2,         0,
            :old.limite_max_ped4,         0,
            :old.limite_max_ped7,         0,
            :old.val_lim_credito,         null,
            :old.dt_alt_lim_credito,      null,
            :old.cod_sit_cobranca,        0,
            :old.digitacao_pedido,        '',
            :old.valor_faturamento_anual, 0.00,
            :old.codigo_empresa,          0,
            :old.tipo_frete,              0,
            :old.nfe_e_mail,              '',
            :old.prioridade_sugestao,     '',
            :old.data_vencto,             null,
            :old.situacao_cadastro,       0,
            :old.conta_contabil,          0,
            :old.forma_pagamento,         0,
            :old.perc_desc_duplic,        0.00,
            :old.data_compl_cadastro,     null,
            :old.criterio_atendimento,    0,
            :old.data_reativacao,         null,
            :old.empenho_automatico,      0,
            :old.cliente_isento,          0,
            :old.isento_sci,              0,
            :old.isento_serasa,           0,
            :old.dt_ult_consulta_sintegra,null,
            :old.alternativa_cliente,     0,
            :old.cli_quebra_peca,         0,
            :old.cli_quebra_pano,         0,
            :old.cli_quebra_fios,         0,
            :old.cli_quebra_teci,         0,
            :old.cliente_integracao,      '',
            :old.cod_cidade_zona_franca,  0,
            :old.cod_relacion_sped_ctb,   0,
            :old.ind_desc_pis_cofins,     'N',
            :old.cadastro_dda,            'N',
            :old.auditar_volume,          0,
            :old.maior_atraso,            0,
            :old.data_maior_atr,          null,
            :old.mot_inat_cliente,        null,
            ws_aplicativo,                'D',
            sysdate,                      ws_usuario_rede,
            ws_maquina_rede,              ws_processo_systextil,
            ws_usuario_systextil,
            :old.grupo_economico,         0
         );

      exception when OTHERS then
         raise_application_error (-20000, inter_fn_buscar_tag_composta('ds22559#ATENi??i??O! Ni??o inseriu {0}. Status: {1}.', 'PEDI_028_HIST(001-1)' , sqlerrm , '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));

      end;


   end if;

end inter_tr_pedi_010_hist;
-- ALTER TRIGGER "INTER_TR_PEDI_010_HIST" ENABLE
 

