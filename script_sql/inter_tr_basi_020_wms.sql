create or replace trigger inter_tr_basi_020_wms
before insert or
       update of basi030_nivel030,
                 basi030_referenc,
                 tamanho_ref,
                 peso_liquido
on basi_020
for each row
declare
  Pragma Autonomous_Transaction;
  v_unidade_medida      varchar2(2);
  v_colecao             number;
  v_tipo_tag_ean        varchar2(3);
  v_descr_colecao       varchar2(20);
  v_cubagem             number;
  v_descr_grupo         varchar2(30);
  v_descr_tam           varchar2(10);
  v_ordem_tam           number;
  v_codigo_unico_sku    inte_wms_produtos.codigo_unico_sku   %type;
  v_utiliza_wms         number(1);
  v_artigo_cotas_sku    inte_wms_produtos.artigo_cotas_sku   %type;
  v_descr_artigo_sku    inte_wms_produtos.descr_artigo_sku   %type;
  v_linha_produto_sku   inte_wms_produtos.linha_produto_sku  %type;
  v_descr_linha_sku     inte_wms_produtos.descr_linha_sku    %type;
  v_referencia_original basi_030.ref_original                %type;
begin
   
   v_utiliza_wms := 0;
   
   begin
      select max(fatu_503.uti_integracao_wms)
      into v_utiliza_wms
      from fatu_503
      where fatu_503.uti_integracao_wms = 1;
   exception
     when no_data_found then
        v_utiliza_wms := 0;
   end;      
   
   if :new.basi030_nivel030 = '1' and v_utiliza_wms = 1
   then
      v_referencia_original := '00000';
      /* Buscar campos da basi_030 */
      begin
        select basi_030.unidade_medida, basi_030.colecao, 
               basi_030.tipo_tag_ean,   basi_030.descr_referencia,
               basi_030.artigo_cotas,   basi_030.linha_produto,
               basi_030.ref_original
        into   v_unidade_medida,        v_colecao,
               v_tipo_tag_ean,          v_descr_grupo,
               v_artigo_cotas_sku,      v_linha_produto_sku,
               v_referencia_original
        from basi_030
        where basi_030.nivel_estrutura = :new.basi030_nivel030
          and basi_030.referencia      = :new.basi030_referenc;
      exception
        when no_data_found then
          v_unidade_medida      := '';
          v_colecao             := 0;
          v_tipo_tag_ean        := '';
          v_descr_grupo         := '';
          v_artigo_cotas_sku    := 0;
          v_linha_produto_sku   := 0;
          v_referencia_original := '00000';
      end;

      /* Buscar descri��o da cole��o */
      begin
        select basi_140.descr_colecao
        into   v_descr_colecao
        from basi_140
        where basi_140.colecao = v_colecao;
      exception
        when no_data_found then
          v_descr_colecao := '';
      end;
      
      /* Buscar descri��o do artigo de cota */
      begin
         select basi_295.descr_artigo 
         into v_descr_artigo_sku
         from basi_295 
         where basi_295.artigo_cotas = v_artigo_cotas_sku;
      exception
        when no_data_found then
         v_descr_artigo_sku := '';
      end;
            
      /* Buscar descri��o da linha de produto */
      begin
         select basi_120.descricao_linha 
         into v_descr_linha_sku
         from basi_120 
         where basi_120.linha_produto = v_linha_produto_sku; 
      exception
        when no_data_found then
         v_descr_linha_sku := '';
      end;

      /* Buscar tamanho */
      begin
        select basi_220.descr_tamanho,     basi_220.ordem_tamanho
        into   v_descr_tam,                v_ordem_tam
        from basi_220
        where basi_220.tamanho_ref = :new.tamanho_ref;
      exception
        when no_data_found then
          v_descr_tam := '';
          v_ordem_tam := 0;
      end;

      /* Buscar campos da basi_023 */
      begin
        select basi_023.cubagem
        into   v_cubagem
        from basi_023
        where basi_023.basi030_nivel030 = :new.basi030_nivel030
          and basi_023.basi030_referenc = :new.basi030_referenc
          and basi_023.tamanho_ref      = :new.tamanho_ref;
      exception
        when no_data_found then
          v_cubagem := 0.000000;
      end;

      /* Buscar/Explodir basi_010 */
      for f_itens in (
        select basi_010.item_estrutura,
               basi_010.codigo_barras,    basi_010.narrativa
        from basi_010
        where basi_010.nivel_estrutura  = :new.basi030_nivel030
          and basi_010.grupo_estrutura  = :new.basi030_referenc
          and basi_010.subgru_estrutura = :new.tamanho_ref)
      LOOP
         
         v_codigo_unico_sku := :new.basi030_nivel030 || '.' || :new.basi030_referenc || '.' || :new.tamanho_ref || '.' || f_itens.item_estrutura;

         if (v_referencia_original = '00000' )
         then
            /* Inserir na tabela de integra��o */
            begin
               insert into inte_wms_produtos
                  (nivel_sku,
                   grupo_sku,
                   subgrupo_sku,
                   item_sku,
                   ean_sku,
                   descricao_sku,
                   descr_tam,
                   ordem_tam,
                   descr_grupo,
                   unidade_medida_sku,
                   peso_sku,
                   cubagem_sku,
                   colecao_sku,
                   desc_colecao_sku,
                   tipo_sku,
                   codigo_unico_sku,
                   artigo_cotas_sku,
                   descr_artigo_sku,
                   linha_produto_sku,
                   descr_linha_sku
                  )
               values
                  (:new.basi030_nivel030,
                   :new.basi030_referenc,
                   :new.tamanho_ref,
                   f_itens.item_estrutura,
                   f_itens.codigo_barras,
                   f_itens.narrativa,
                   v_descr_tam,
                   v_ordem_tam,
                   v_descr_grupo,
                   v_unidade_medida,
                   :new.peso_liquido,
                   v_cubagem,
                   v_colecao,
                   v_descr_colecao,
                   v_tipo_tag_ean,
                   v_codigo_unico_sku,
                   v_artigo_cotas_sku,
                   v_descr_artigo_sku,
                   v_linha_produto_sku,
                   v_descr_linha_sku
                  );
            exception
               when others then
                  raise_application_error(-20000, 'N�o inseriu na tabela INTE_WMS_PRODUTOS.' || Chr(10) || SQLERRM);
            end;
         end if;
      END LOOP; 
      COMMIT;

   end if;
   
end inter_tr_basi_020_wms;
/

execute inter_pr_recompile;
/* versao: 3 */


 exit;


 exit;


 exit;

 exit;
