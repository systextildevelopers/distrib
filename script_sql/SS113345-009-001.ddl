ALTER TABLE estq_300 MODIFY VALOR_MOVIMENTO_UNITARIO      NUMBER(18,5) DEFAULT 0.0;
ALTER TABLE estq_300 MODIFY VALOR_CONTABIL_UNITARIO       NUMBER(18,5) DEFAULT 0.0;
ALTER TABLE estq_300 MODIFY PRECO_MEDIO_UNITARIO          NUMBER(18,5) DEFAULT 0.0;
ALTER TABLE estq_300 MODIFY SALDO_FINANCEIRO              NUMBER(18,5) DEFAULT 0.0;
ALTER TABLE estq_300 MODIFY VALOR_MOVIMENTO_UNITARIO_PROJ NUMBER(18,5) DEFAULT 0.00000;
ALTER TABLE estq_300 MODIFY VALOR_CONTABIL_UNITARIO_PROJ  NUMBER(18,5) DEFAULT 0.00000;
ALTER TABLE estq_300 MODIFY PRECO_MEDIO_UNITARIO_PROJ     NUMBER(18,5) DEFAULT 0.00000;
ALTER TABLE estq_300 MODIFY SALDO_FINANCEIRO_PROJ         NUMBER(18,5) DEFAULT 0.00000;
ALTER TABLE estq_300 MODIFY VALOR_MOVTO_UNIT_ESTIMADO     NUMBER(18,5) DEFAULT 0.00000;
ALTER TABLE estq_300 MODIFY PRECO_MEDIO_UNIT_ESTIMADO     NUMBER(18,5) DEFAULT 0.00000;
ALTER TABLE estq_300 MODIFY SALDO_FINANCEIRO_ESTIMADO     NUMBER(18,5) DEFAULT 0.00000;
ALTER TABLE estq_300 MODIFY VALOR_TOTAL                   NUMBER(15,2) DEFAULT 0.00;
/

ALTER TABLE basi_305 MODIFY VALOR_SUBPRODUTO NUMBER(18,5) DEFAULT 0.0;
/

ALTER TABLE estq_301 MODIFY SALDO_FINANCEIRO          NUMBER(18,5) DEFAULT 0.0;
ALTER TABLE estq_301 MODIFY PRECO_MEDIO_UNITARIO      NUMBER(18,5) DEFAULT 0.0;
ALTER TABLE estq_301 MODIFY PRECO_CUSTO_UNITARIO      NUMBER(18,5) DEFAULT 0.0;
ALTER TABLE estq_301 MODIFY SALDO_FINANCEIRO_ESTIMADO NUMBER(18,5) DEFAULT 0.0;
ALTER TABLE estq_301 MODIFY PRECO_MEDIO_UNIT_ESTIMADO NUMBER(18,5) DEFAULT 0.0;
ALTER TABLE estq_301 MODIFY PRECO_CUSTO_UNIT_ESTIMADO NUMBER(18,5) DEFAULT 0.0;
ALTER TABLE estq_301 MODIFY SALDO_FINANCEIRO_PROJ     NUMBER(18,5) DEFAULT 0.0;
ALTER TABLE estq_301 MODIFY PRECO_MEDIO_UNIT_PROJ     NUMBER(18,5) DEFAULT 0.0;
ALTER TABLE estq_301 MODIFY PRECO_CUSTO_UNIT_PROJ     NUMBER(18,5) DEFAULT 0.0;
/

ALTER TABLE basi_010 MODIFY PRECO_MEDIO       NUMBER(18,5) DEFAULT 0.0;
ALTER TABLE basi_010 MODIFY PRECO_ULT_COMPRA  NUMBER(18,5) DEFAULT 0.0;
ALTER TABLE basi_010 MODIFY PRECO_CUSTO       NUMBER(18,5) DEFAULT 0.0;
ALTER TABLE basi_010 MODIFY PRECO_CUSTO_INFO  NUMBER(18,5) DEFAULT 0.0;
ALTER TABLE basi_010 MODIFY PRECO_MEDIO_ANT   NUMBER(18,5) DEFAULT 0.0;
ALTER TABLE basi_010 MODIFY PRECO_CONTRATIPO  NUMBER(18,5) DEFAULT 0.00;
/

ALTER TABLE estq_038 MODIFY VALOR_TOTAL_MP     NUMBER(19,6) DEFAULT 0.000;
ALTER TABLE estq_038 MODIFY VALOR_TOTAL_MO     NUMBER(19,6) DEFAULT 0.000;
ALTER TABLE estq_038 MODIFY VALOR_TOTAL_CP     NUMBER(19,6) DEFAULT 0.000;
ALTER TABLE estq_038 MODIFY VALOR_TOTAL_CD     NUMBER(19,6) DEFAULT 0.000;
/

ALTER TABLE estq_310 MODIFY VALOR_MOVIMENTO_UNITARIO      NUMBER(18,5) DEFAULT 0.0;
ALTER TABLE estq_310 MODIFY VALOR_CONTABIL_UNITARIO       NUMBER(18,5) DEFAULT 0.0;
ALTER TABLE estq_310 MODIFY PRECO_MEDIO_UNITARIO          NUMBER(18,5) DEFAULT 0.0;
ALTER TABLE estq_310 MODIFY SALDO_FINANCEIRO              NUMBER(18,5) DEFAULT 0.0;
ALTER TABLE estq_310 MODIFY VALOR_MOVIMENTO_UNITARIO_PROJ NUMBER(18,5) DEFAULT 0.00000;
ALTER TABLE estq_310 MODIFY VALOR_CONTABIL_UNITARIO_PROJ  NUMBER(18,5) DEFAULT 0.00000;
ALTER TABLE estq_310 MODIFY PRECO_MEDIO_UNITARIO_PROJ     NUMBER(18,5) DEFAULT 0.00000;
ALTER TABLE estq_310 MODIFY SALDO_FINANCEIRO_PROJ         NUMBER(18,5) DEFAULT 0.00000;
ALTER TABLE estq_310 MODIFY VALOR_MOVTO_UNIT_ESTIMADO     NUMBER(18,5) DEFAULT 0.00000;
ALTER TABLE estq_310 MODIFY PRECO_MEDIO_UNIT_ESTIMADO     NUMBER(18,5) DEFAULT 0.00000;
ALTER TABLE estq_310 MODIFY SALDO_FINANCEIRO_ESTIMADO     NUMBER(18,5) DEFAULT 0.00000;
ALTER TABLE estq_310 MODIFY VALOR_TOTAL                   NUMBER(15,2) DEFAULT 0.00;
/

ALTER TABLE rcnb_030 MODIFY CONSUMO      NUMBER(19,6) DEFAULT 0.000000;
ALTER TABLE rcnb_030 MODIFY VALOR_BASE   NUMBER(19,6) DEFAULT 0;
/

ALTER TABLE rcnb_150 MODIFY VALOR_TOTAL_MP     NUMBER(18,5) DEFAULT 0.000;
ALTER TABLE rcnb_150 MODIFY VALOR_TOTAL_MO     NUMBER(18,5) DEFAULT 0.000;
ALTER TABLE rcnb_150 MODIFY VALOR_TOTAL_CP     NUMBER(18,5) DEFAULT 0.000;
ALTER TABLE rcnb_150 MODIFY VALOR_TOTAL_CD     NUMBER(18,5) DEFAULT 0.000;
ALTER TABLE rcnb_150 MODIFY VALOR_TOTAL_MP_EST NUMBER(18,5) DEFAULT 0.000;
ALTER TABLE rcnb_150 MODIFY VALOR_TOTAL_MO_EST NUMBER(18,5) DEFAULT 0.000;
ALTER TABLE rcnb_150 MODIFY VALOR_TOTAL_CP_EST NUMBER(18,5) DEFAULT 0.000;
ALTER TABLE rcnb_150 MODIFY VALOR_TOTAL_CD_EST NUMBER(18,5) DEFAULT 0.000;
/

ALTER TABLE rcnb_085 MODIFY VALOR_DESPESAS          NUMBER(15,2) DEFAULT 0.0;
ALTER TABLE rcnb_085 MODIFY CUSTO_MINUTO            NUMBER(19,6) DEFAULT 0.0;
ALTER TABLE rcnb_085 MODIFY CUSTO_MINUTO_PREVISTO   NUMBER(19,6) DEFAULT 0.000;
ALTER TABLE rcnb_085 MODIFY CUSTO_MINUTO_ESTIMADO   NUMBER(19,6) DEFAULT 0.0;
ALTER TABLE rcnb_085 MODIFY VALOR_DESPESAS_ESTIMADO NUMBER(15,2) DEFAULT 0.00;
/

ALTER TABLE rcnb_087 MODIFY VALOR_DESPESA           NUMBER(15,2) DEFAULT 0.00;
ALTER TABLE rcnb_087 MODIFY CUSTO_MINUTO            NUMBER(19,6) DEFAULT 0.000000;
ALTER TABLE rcnb_087 MODIFY CUSTO_MINUTO_PREVISTO   NUMBER(19,6) DEFAULT 0.000;
ALTER TABLE rcnb_087 MODIFY CUSTO_MINUTO_ESTIMADO   NUMBER(19,6) DEFAULT 0.0;
ALTER TABLE rcnb_087 MODIFY VALOR_DESPESAS_ESTIMADO NUMBER(15,2) DEFAULT 0.00;
/
