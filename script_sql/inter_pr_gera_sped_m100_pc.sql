create or replace procedure inter_pr_gera_sped_m100_pc(p_cod_matriz    IN NUMBER,
                                                       p_dat_inicial   IN DATE,
                                                       p_dat_final     IN DATE,
                                                       p_des_erro      OUT varchar2) is

CURSOR u_pc_m100_pis (p_cod_matriz      NUMBER,
                  p_dat_inicial     IN DATE,
                  p_dat_final       IN DATE) IS
   select merc_int_ext,                  cvf_pis,
          perc_pis,                      sum(valor_base_pis) as valor_base_pis,
          sum(valor_pis) as valor_pis,   cod_cont_crec
   from (
         (select decode(substr(sped_pc_c170.num_cfop,1,1),3,1,0) merc_int_ext, sped_pc_c170.cvf_pis,
                 sped_pc_c170.perc_pis,                          sum(sped_pc_c170.val_basi_pis_cofins) as valor_base_pis,
                 sum(sped_pc_c170.val_pis) as valor_pis,         sped_pc_c170.cod_cont_crec
          from sped_pc_c170
          where sped_pc_c170.cod_matriz           = p_cod_matriz
            and sped_pc_c170.tip_entrada_saida    = 'E'
            and sped_pc_c170.cod_situacao_nota in (0,6)
            and sped_pc_c170.perc_pis             > 0.00000
          group by decode(substr(sped_pc_c170.num_cfop,1,1),3,1,0), sped_pc_c170.cvf_pis,sped_pc_c170.perc_pis,
                   sped_pc_c170.cod_cont_crec)
          -- 0 - Merc interno
          -- 1 - Merc externo
         union all
            (select obrf_700.ind_orig_cred as merc_int_ext,   obrf_700.cst_pis as cvf_pis,
                    obrf_700.aliq_pis as perc_pis,            sum(obrf_700.vl_bc_pis) as valor_base_pis,
                    sum(obrf_700.vl_pis) as valor_pis,        obrf_700.nat_bc_cred as cod_cont_cred
             from obrf_700, fatu_500
             where obrf_700.cod_empresa   = fatu_500.codigo_empresa
               and fatu_500.codigo_matriz = p_cod_matriz
               and obrf_700.mes_apur      = to_char(p_dat_inicial, 'MM')
               and obrf_700.ano_apur      = to_char(p_dat_inicial, 'YYYY')
               and obrf_700.cst_pis       > 49
               and obrf_700.vl_pis        > 0.00
             group by obrf_700.ind_orig_cred, obrf_700.aliq_pis,
                      obrf_700.cst_pis,       obrf_700.nat_bc_cred)
         union all
            (select 0,                                    obrf_707.cst_pis as cvf_pis,
                    obrf_707.aliq_pis as perc_pis,        sum(obrf_707.vl_bc_men_est) valor_base_pis,
                    sum(obrf_707.vl_cred_pis) valor_pis,  obrf_707.nat_bc_cred as cod_cont_crec
            from obrf_707, fatu_500
            where obrf_707.cod_empresa   = fatu_500.codigo_empresa
              and fatu_500.codigo_matriz = p_cod_matriz
              and obrf_707.mes_apur      = to_char(p_dat_inicial, 'MM')
              and obrf_707.ano_apur      = to_char(p_dat_inicial, 'YYYY')
            group by obrf_707.aliq_pis, obrf_707.cst_pis,
                     obrf_707.nat_bc_cred)
         union all
            (select obrf_302.ind_orig_cred,         obrf_302.cst_pis,
                    obrf_302.aliq_pis as perc_pis,  sum(obrf_302.vl_bc_pis) as valor_base_pis,
                    sum(obrf_302.vl_pis) valor_pis, obrf_302.nat_bc_cred as cod_cont_crec
            from obrf_302, fatu_500
            where obrf_302.cod_empresa   = fatu_500.codigo_empresa
              and fatu_500.codigo_matriz = p_cod_matriz
              and obrf_302.mes_apur      = to_char(p_dat_inicial, 'MM')
              and obrf_302.ano_apur      = to_char(p_dat_inicial, 'YYYY')
            group by obrf_302.ind_orig_cred, obrf_302.aliq_pis,
                     obrf_302.cst_pis,       obrf_302.nat_bc_cred)
   )
   group by merc_int_ext,   cvf_pis,
            perc_pis,       cod_cont_crec;

CURSOR u_pc_m100_cofins (p_cod_matriz      NUMBER,
                         p_dat_inicial     IN DATE,
                         p_dat_final       IN DATE) IS
   select merc_int_ext,                        cvf_cofins,
          perc_cofins,                         sum(valor_base_cofins) as valor_base_cofins,
          sum(valor_cofins) as valor_cofins,   cod_cont_crec
   from (
         (select decode(substr(sped_pc_c170.num_cfop,1,1),3,1,0) merc_int_ext, sped_pc_c170.cvf_cofins,
                 sped_pc_c170.perc_cofins,                       sum(sped_pc_c170.val_basi_pis_cofins) as valor_base_cofins,
                 sum(sped_pc_c170.val_cofins) as valor_cofins,   sped_pc_c170.cod_cont_crec
          from sped_pc_c170
          where sped_pc_c170.cod_matriz           = p_cod_matriz
            and sped_pc_c170.tip_entrada_saida    = 'E'
            and sped_pc_c170.cod_situacao_nota in (0,6)
            and sped_pc_c170.perc_cofins          > 0.00000
          group by decode(substr(sped_pc_c170.num_cfop,1,1),3,1,0), sped_pc_c170.cvf_cofins,
                   sped_pc_c170.perc_cofins,                        sped_pc_c170.cod_cont_crec)
          -- 0 - Merc interno
          -- 1 - Merc externo
         union all
            (select obrf_700.ind_orig_cred as merc_int_ext,   obrf_700.cst_cofins as cvf_cofins,
                    obrf_700.aliq_cofins as perc_cofins,      sum(obrf_700.vl_bc_pis) as valor_base_pis,
                    sum(obrf_700.vl_cofins) as valor_cofins,     obrf_700.nat_bc_cred as cod_cont_cred
             from obrf_700, fatu_500
             where obrf_700.cod_empresa   = fatu_500.codigo_empresa
               and fatu_500.codigo_matriz = p_cod_matriz
               and obrf_700.mes_apur      = to_char(p_dat_inicial, 'MM')
               and obrf_700.ano_apur      = to_char(p_dat_inicial, 'YYYY')
               and obrf_700.cst_cofins    > 49
               and obrf_700.vl_cofins     > 0.00
             group by obrf_700.ind_orig_cred, obrf_700.aliq_cofins,
                      obrf_700.cst_cofins,    obrf_700.nat_bc_cred)
         union all
            (select 0,                                          obrf_707.cst_cofins as cvf_cofins,
                    obrf_707.aliq_cofins as perc_cofins,        sum(obrf_707.vl_bc_men_est) valor_base_cofins,
                    sum(obrf_707.vl_cred_cofins) valor_cofins,  obrf_707.nat_bc_cred as cod_cont_crec
            from obrf_707, fatu_500
            where obrf_707.cod_empresa   = fatu_500.codigo_empresa
              and fatu_500.codigo_matriz = p_cod_matriz
              and obrf_707.mes_apur      = to_char(p_dat_inicial, 'MM')
              and obrf_707.ano_apur      = to_char(p_dat_inicial, 'YYYY')
            group by obrf_707.aliq_cofins, obrf_707.cst_cofins,
                     obrf_707.nat_bc_cred)
         union all
            (select obrf_302.ind_orig_cred,               obrf_302.cst_cofins,
                    obrf_302.aliq_cofins as perc_cofins,  sum(obrf_302.vl_bc_cofins) as valor_base_cofins,
                    sum(obrf_302.vl_cofins) valor_cofins, obrf_302.nat_bc_cred as cod_cont_crec
            from obrf_302, fatu_500
            where obrf_302.cod_empresa   = fatu_500.codigo_empresa
              and fatu_500.codigo_matriz = p_cod_matriz
              and obrf_302.mes_apur      = to_char(p_dat_inicial, 'MM')
              and obrf_302.ano_apur      = to_char(p_dat_inicial, 'YYYY')
            group by obrf_302.ind_orig_cred, obrf_302.aliq_cofins,
                     obrf_302.cst_cofins,    obrf_302.nat_bc_cred)
   )
   group by merc_int_ext,   cvf_cofins,
            perc_cofins,    cod_cont_crec;

v_valor_mi                                 sped_pc_m100.vl_cred%type;
v_valor_mint                               sped_pc_m100.vl_cred%type;
v_valor_exp                                sped_pc_m100.vl_cred%type;
valor_base_pis_rat_mi                      sped_pc_m100.vl_cred%type;
valor_base_pis_rat_mint                    sped_pc_m100.vl_cred%type;
valor_base_pis_rat_exp                     sped_pc_m100.vl_cred%type;
valor_pis_mi                               sped_pc_m100.vl_cred%type;
valor_pis_mint                             sped_pc_m100.vl_cred%type;
valor_pis_exp                              sped_pc_m100.vl_cred%type;
v_val_aju_redu                             sped_pc_m100.vl_ajus_reduc%type;
v_val_aju_acres                            sped_pc_m100.vl_ajus_reduc%type;
v_val_aju_acres_rat_mi                     sped_pc_m100.vl_ajus_reduc%type;
v_val_aju_acres_rat_mint                   sped_pc_m100.vl_ajus_reduc%type;
v_val_aju_acres_rat_exp                    sped_pc_m100.vl_ajus_reduc%type;
v_val_aju_redu_rat_mi                      sped_pc_m100.vl_ajus_reduc%type;
v_val_aju_redu_rat_mint                    sped_pc_m100.vl_ajus_reduc%type;
v_val_aju_redu_rat_exp                     sped_pc_m100.vl_ajus_reduc%type;
tot_vlr_acres                              sped_pc_m100.vl_ajus_reduc%type;
tot_vlr_reduc                              sped_pc_m100.vl_ajus_reduc%type;
v_vl_base_pis_cofins_total                 sped_pc_m100.vl_cred%type;
v_cod_credito                              number(3);
id_ajuste                                  varchar2(100);
maior_valor                                sped_pc_m100.vl_ajus_reduc%type;
w_conta_reg                               number;
v_id_ajuste_pr                             varchar2(100);

procedure insere_m100 (pr_cod_matriz number,
                       pr_cod_cred number,
                       pr_ind_cred_ori number,
                       pr_vl_bc_pis number,
                       pr_vl_bc_pis_cofins_rt number,
                       pr_aliq_pis number,
                       pr_cst number,
                       pr_vl_cred_rt number,
                       pr_cod_cont_crec number,
                       pr_tipo varchar2,
                       pr_val_aju_acres  number,
                       pr_val_aju_redu  number,
                       p_des_erro OUT varchar2) is

v_vl_cred_disp                             sped_pc_m100.vl_cred_disp%type;

begin


   v_vl_cred_disp := ((round(pr_vl_bc_pis,2) * pr_aliq_pis) / 100.00) + round(pr_val_aju_acres,2) - round(pr_val_aju_redu,2);

   begin
      insert into sped_pc_m100
        (sped_pc_m100.cod_matriz, /*001*/
         sped_pc_m100.cod_cred, /*002*/
         sped_pc_m100.ind_cred_ori, /*003*/
         sped_pc_m100.vl_bc_pis_cofins, /*004*/
         sped_pc_m100.vl_bc_pis_cofins_rt, /*005*/
         sped_pc_m100.aliq_pis, /*006*/
         sped_pc_m100.cst, /*007*/
         sped_pc_m100.vl_cred, /*008*/
         sped_pc_m100.vl_cred_rt, /*009*/
         sped_pc_m100.vl_ajus_acres, /*010*/
         sped_pc_m100.vl_ajus_reduc, /*011*/
         sped_pc_m100.vl_cred_disp, /*012*/
         sped_pc_m100.ind_desc_cred, /*013*/
         sped_pc_m100.vl_cred_desc, /*014*/
         sped_pc_m100.sld_cred, /*015*/
         sped_pc_m100.cod_cont_crec, /*016*/
         sped_pc_m100.tipo) /*017*/
      values
        (pr_cod_matriz, /*001*/
         pr_cod_cred, /*002*/
         pr_ind_cred_ori, /*003*/
         pr_vl_bc_pis, /*004*/
         pr_vl_bc_pis_cofins_rt, /*005*/
         pr_aliq_pis, /*006*/
         pr_cst, /*007*/
         ((round(pr_vl_bc_pis,2) * pr_aliq_pis) / 100.00), /*008*/
         pr_vl_cred_rt, /*009*/
         round(pr_val_aju_acres,5), /*010*/
         round(pr_val_aju_redu,5), /*011*/
         v_vl_cred_disp, /*012*/
         '0', /*013*/
         v_vl_cred_disp, /*014*/
         v_vl_cred_disp, /*015*/
         pr_cod_cont_crec, /*016*/
         pr_tipo); /*017*/
   exception
       when others then
           p_des_erro := 'N�o inseriu dados na tabela sped_pc_m100' || Chr(10) || SQLERRM;
   end;
   commit;

end insere_m100;

begin

   begin
      delete from sped_pc_m100
      where sped_pc_m100.cod_matriz = p_cod_matriz;
   exception
       when others then
          p_des_erro := 'N�o removeu dados da tabela sped_pc_m100' || Chr(10) || SQLERRM;
   end;
   commit;

   /*Pega o valor do mercado interno*/
   begin
      select nvl(sum(sped_pc_0111.valor_receita), 0.00000)
      into   v_valor_mi
      from sped_pc_0111
      where sped_pc_0111.cod_matriz = p_cod_matriz
        and sped_pc_0111.tip_tributacao = 'MI';
   exception
       when no_data_found then
          v_valor_mi := 0.00000;
   end;

   /*Pega o valor do mercado interno n�o tributado*/
   begin
      select nvl(sum(sped_pc_0111.valor_receita), 0.00000)
      into   v_valor_mint
      from sped_pc_0111
      where sped_pc_0111.cod_matriz = p_cod_matriz
        and sped_pc_0111.tip_tributacao = 'MINT';
   exception
       when no_data_found then
          v_valor_mint := 0.00000;
   end;

   /*Pega o valor do mercado exporta��o*/
   begin
      select nvl(sum(sped_pc_0111.valor_receita), 0.00000)
      into   v_valor_exp
      from sped_pc_0111
      where sped_pc_0111.cod_matriz = p_cod_matriz
        and sped_pc_0111.tip_tributacao = 'EXP'
        and sped_pc_0111.valor_receita  > 0.00;
   exception
       when no_data_found then
          v_valor_exp := 0.00000;
   end;

   /* Pega o valor total da base para fazer o rateio do ajuste */

   select sum(valor_base_pis) as valor_base_pis
   into v_vl_base_pis_cofins_total
   from (
         (select sum(sped_pc_c170.val_basi_pis_cofins) as valor_base_pis
          from sped_pc_c170
          where sped_pc_c170.cod_matriz           = p_cod_matriz
            and sped_pc_c170.tip_entrada_saida    = 'E'
            and sped_pc_c170.cod_situacao_nota in (0,6)
            and sped_pc_c170.perc_pis             > 0.00000
          group by decode(substr(sped_pc_c170.num_cfop,1,1),3,1,0), sped_pc_c170.cvf_pis,sped_pc_c170.perc_pis,
                   sped_pc_c170.cod_cont_crec)
          -- 0 - Merc interno
          -- 1 - Merc externo
         union all
            (select sum(obrf_700.vl_bc_pis) as valor_base_pis
             from obrf_700, fatu_500
             where obrf_700.cod_empresa   = fatu_500.codigo_empresa
               and fatu_500.codigo_matriz = p_cod_matriz
               and obrf_700.mes_apur      = to_char(p_dat_inicial, 'MM')
               and obrf_700.ano_apur      = to_char(p_dat_inicial, 'YYYY')
               and obrf_700.cst_pis       > 49
               and obrf_700.vl_pis        > 0.00
             group by obrf_700.ind_orig_cred, obrf_700.aliq_pis,
                      obrf_700.cst_pis,       obrf_700.nat_bc_cred)
         union all
            (select sum(obrf_707.vl_bc_men_est) valor_base_pis
            from obrf_707, fatu_500
            where obrf_707.cod_empresa   = fatu_500.codigo_empresa
              and fatu_500.codigo_matriz = p_cod_matriz
              and obrf_707.mes_apur      = to_char(p_dat_inicial, 'MM')
              and obrf_707.ano_apur      = to_char(p_dat_inicial, 'YYYY')
            group by obrf_707.aliq_pis, obrf_707.cst_pis,
                     obrf_707.nat_bc_cred)
         union all
            (select sum(obrf_302.vl_bc_pis) as valor_base_pis
            from obrf_302, fatu_500
            where obrf_302.cod_empresa   = fatu_500.codigo_empresa
              and fatu_500.codigo_matriz = p_cod_matriz
              and obrf_302.mes_apur      = to_char(p_dat_inicial, 'MM')
              and obrf_302.ano_apur      = to_char(p_dat_inicial, 'YYYY')
            group by obrf_302.ind_orig_cred, obrf_302.aliq_pis,
                     obrf_302.cst_pis,       obrf_302.nat_bc_cred)
   );

   valor_base_pis_rat_mi   := 0.00000;
   valor_base_pis_rat_mint := 0.00000;
   valor_base_pis_rat_exp  := 0.00000;

   valor_pis_mi            := 0.00000;
   valor_pis_mint          := 0.00000;
   valor_pis_exp           := 0.00000;




   /**************    PIS   ************************************************************************/
   FOR pc_m100 IN u_pc_m100_pis (p_cod_matriz, p_dat_inicial, p_dat_final) LOOP

       valor_base_pis_rat_mi   := 0.00000;
       valor_base_pis_rat_mint := 0.00000;
       valor_base_pis_rat_exp  := 0.00000;

       valor_pis_mi            := 0.00000;
       valor_pis_mint          := 0.00000;
       valor_pis_exp           := 0.00000;

       v_val_aju_redu  := 0.00000;
       v_val_aju_acres := 0.00000;

       v_val_aju_redu_rat_mi   := 0.00000;
       v_val_aju_redu_rat_mint := 0.00000;
       v_val_aju_redu_rat_exp  := 0.00000;
       v_val_aju_acres_rat_mi  := 0.00000;
       v_val_aju_acres_rat_mint:= 0.00000;
       v_val_aju_acres_rat_exp := 0.00000;


       begin
          select nvl(sum(decode(obrf_703.ind_aj,0,obrf_703.vl_aj,0.00000)),0.00000),
                 nvl(sum(decode(obrf_703.ind_aj,1,obrf_703.vl_aj,0.00000)),0.00000)
          into   v_val_aju_redu,
                 v_val_aju_acres
          from obrf_703, fatu_500
          where obrf_703.cod_empresa    = fatu_500.codigo_empresa
            and fatu_500.codigo_matriz  = p_cod_matriz
            and obrf_703.reg            = 'M110'
            and obrf_703.cst_pis_cofins = pc_m100.cvf_pis
            and obrf_703.mes_apur       = to_char(p_dat_inicial,'MM')
            and obrf_703.ano_apur       = to_char(p_dat_final,'YYYY');
       exception
           when no_data_found then
              v_val_aju_redu  := 0.00000;
              v_val_aju_acres := 0.00000;
       end;

       if v_val_aju_acres is null
       then
          v_val_aju_acres := 0.00;
       end if;

       if v_val_aju_redu is null
       then
          v_val_aju_redu := 0.00;
       end if;


      -- grupo 100
      if v_valor_mi > 0.00000
      then
         if pc_m100.cvf_pis in ('56', '66')
         then
            valor_base_pis_rat_mi := (pc_m100.valor_base_pis * v_valor_mi) / (v_valor_mi + v_valor_mint + v_valor_exp);
            valor_pis_mi          := (pc_m100.valor_pis * v_valor_mi) / (v_valor_mi + v_valor_mint + v_valor_exp);

            v_val_aju_redu_rat_mi    := (v_val_aju_redu * v_valor_mi) / (v_valor_mi + v_valor_mint + v_valor_exp);
            v_val_aju_redu_rat_mi    := (v_val_aju_redu_rat_mi * ((pc_m100.valor_base_pis / v_vl_base_pis_cofins_total) * 100.00))/100.00;
            v_val_aju_acres_rat_mi   := (v_val_aju_acres * v_valor_mi) / (v_valor_mi + v_valor_mint + v_valor_exp);
            v_val_aju_acres_rat_mi   := (v_val_aju_acres_rat_mi * ((pc_m100.valor_base_pis / v_vl_base_pis_cofins_total) * 100.00))/100.00;

         elsif pc_m100.cvf_pis in ('53', '63')
         then
            valor_base_pis_rat_mi := (pc_m100.valor_base_pis * v_valor_mi) / (v_valor_mi + v_valor_mint);
            valor_pis_mi          := (pc_m100.valor_pis * v_valor_mi) / (v_valor_mi + v_valor_mint);

            v_val_aju_redu_rat_mi    := (v_val_aju_redu * v_valor_mi) / (v_valor_mi + v_valor_mint);
            v_val_aju_redu_rat_mi    := (v_val_aju_redu_rat_mi * ((pc_m100.valor_base_pis / v_vl_base_pis_cofins_total) * 100.00))/100.00;
            v_val_aju_acres_rat_mi   := (v_val_aju_acres * v_valor_mi) / (v_valor_mi + v_valor_mint);
            v_val_aju_acres_rat_mi   := (v_val_aju_acres_rat_mi * ((pc_m100.valor_base_pis / v_vl_base_pis_cofins_total) * 100.00))/100.00;

         elsif pc_m100.cvf_pis in ('54', '64')
         then
            valor_base_pis_rat_mi := (pc_m100.valor_base_pis * v_valor_mi) / (v_valor_mi + v_valor_exp);
            valor_pis_mi          := (pc_m100.valor_pis * v_valor_mi) / (v_valor_mi + v_valor_exp);

            v_val_aju_redu_rat_mi     := (v_val_aju_redu * v_valor_mi) / (v_valor_mi + v_valor_mint);
            v_val_aju_redu_rat_mi     := (v_val_aju_redu_rat_mi * ((pc_m100.valor_base_pis / v_vl_base_pis_cofins_total) * 100.00))/100.00;
            v_val_aju_acres_rat_mi    := (v_val_aju_acres * v_valor_mi) / (v_valor_mi + v_valor_mint);
            v_val_aju_acres_rat_mi    := (v_val_aju_acres_rat_mi * ((pc_m100.valor_base_pis / v_vl_base_pis_cofins_total) * 100.00))/100.00;
         end if;
      end if;

      -- grupo 200
      if v_valor_mint > 0.00000
      then
         if pc_m100.cvf_pis in ('56', '66')
         then
            valor_base_pis_rat_mint := (pc_m100.valor_base_pis * v_valor_mint) / (v_valor_mi + v_valor_mint + v_valor_exp);
            valor_pis_mint          := (pc_m100.valor_pis * v_valor_mint) / (v_valor_mi + v_valor_mint + v_valor_exp);

            v_val_aju_redu_rat_mint     := (v_val_aju_redu * v_valor_mint) / (v_valor_mi + v_valor_mint + v_valor_exp);
            v_val_aju_redu_rat_mint     := (v_val_aju_redu_rat_mint * ((pc_m100.valor_base_pis / v_vl_base_pis_cofins_total) * 100.00))/100.00;
            v_val_aju_acres_rat_mint    := (v_val_aju_acres * v_valor_mint) / (v_valor_mi + v_valor_mint + v_valor_exp);
            v_val_aju_acres_rat_mint    := (v_val_aju_acres_rat_mint * ((pc_m100.valor_base_pis / v_vl_base_pis_cofins_total) * 100.00))/100.00;
         elsif pc_m100.cvf_pis in ('53', '63')
         then
            valor_base_pis_rat_mint := (pc_m100.valor_base_pis * v_valor_mint) / (v_valor_mi + v_valor_mint);
            valor_pis_mint          := (pc_m100.valor_pis * v_valor_mint) / (v_valor_mi + v_valor_mint);

            v_val_aju_redu_rat_mint    := (v_val_aju_redu * v_valor_mint) / (v_valor_mi + v_valor_mint);
            v_val_aju_redu_rat_mint    := (v_val_aju_redu_rat_mint * ((pc_m100.valor_base_pis / v_vl_base_pis_cofins_total) * 100.00))/100.00;
            v_val_aju_acres_rat_mint   := (v_val_aju_acres * v_valor_mint) / (v_valor_mi + v_valor_mint);
            v_val_aju_acres_rat_mint   := (v_val_aju_acres_rat_mint * ((pc_m100.valor_base_pis / v_vl_base_pis_cofins_total) * 100.00))/100.00;
         elsif pc_m100.cvf_pis in ('55', '65')
         then
            valor_base_pis_rat_mint := (pc_m100.valor_base_pis * v_valor_mint) / (v_valor_mint + v_valor_exp);
            valor_pis_mint          := (pc_m100.valor_pis * v_valor_mint) / (v_valor_mint + v_valor_exp);

            v_val_aju_redu_rat_mint    := (v_val_aju_redu * v_valor_mint) / (v_valor_mint + v_valor_exp);
            v_val_aju_redu_rat_mint    := (v_val_aju_redu_rat_mint * ((pc_m100.valor_base_pis / v_vl_base_pis_cofins_total) * 100.00))/100.00;
            v_val_aju_acres_rat_mint   := (v_val_aju_acres * v_valor_mint) / (v_valor_mint + v_valor_exp);
            v_val_aju_acres_rat_mint   := (v_val_aju_acres_rat_mint * ((pc_m100.valor_base_pis / v_vl_base_pis_cofins_total) * 100.00))/100.00;
         end if;
      end if;

      -- grupo 300
      if v_valor_exp > 0.00000
      then
         if pc_m100.cvf_pis in ('56', '66')
         then
            valor_base_pis_rat_exp := (pc_m100.valor_base_pis * v_valor_exp) / (v_valor_mi + v_valor_mint + v_valor_exp);
            valor_pis_exp          := (pc_m100.valor_pis * v_valor_exp) / (v_valor_mi + v_valor_mint + v_valor_exp);

            v_val_aju_redu_rat_exp    := (v_val_aju_redu * v_valor_exp) / (v_valor_mi + v_valor_mint + v_valor_exp);
            v_val_aju_redu_rat_exp    := (v_val_aju_redu_rat_exp * ((pc_m100.valor_base_pis / v_vl_base_pis_cofins_total) * 100.00))/100.00;
            v_val_aju_acres_rat_exp   := (v_val_aju_acres * v_valor_exp) / (v_valor_mi + v_valor_mint + v_valor_exp);
            v_val_aju_acres_rat_exp   := (v_val_aju_acres_rat_exp * ((pc_m100.valor_base_pis / v_vl_base_pis_cofins_total) * 100.00))/100.00;

         elsif pc_m100.cvf_pis in ('54', '64')
         then
            valor_base_pis_rat_exp := (pc_m100.valor_base_pis * v_valor_exp) / (v_valor_mi + v_valor_exp);
            valor_pis_exp          := (pc_m100.valor_pis * v_valor_exp) / (v_valor_mi + v_valor_exp);

            v_val_aju_redu_rat_exp    := (v_val_aju_redu * v_valor_exp) / (v_valor_mi + v_valor_exp);
            v_val_aju_redu_rat_exp    := (v_val_aju_redu_rat_exp * ((pc_m100.valor_base_pis / v_vl_base_pis_cofins_total) * 100.00))/100.00;
            v_val_aju_acres_rat_exp   := (v_val_aju_acres * v_valor_exp) / (v_valor_mi + v_valor_exp);
            v_val_aju_acres_rat_exp   := (v_val_aju_acres_rat_exp * ((pc_m100.valor_base_pis / v_vl_base_pis_cofins_total) * 100.00))/100.00;
         elsif pc_m100.cvf_pis in ('55', '65')
         then
            valor_base_pis_rat_exp := (pc_m100.valor_base_pis * v_valor_exp) / (v_valor_mint + v_valor_exp);
            valor_pis_exp          := (pc_m100.valor_pis * v_valor_exp) / (v_valor_mint + v_valor_exp);

            v_val_aju_redu_rat_exp    := (v_val_aju_redu * v_valor_exp) / (v_valor_mint + v_valor_exp);
            v_val_aju_redu_rat_exp    := (v_val_aju_redu_rat_exp * ((pc_m100.valor_base_pis / v_vl_base_pis_cofins_total) * 100.00))/100.00;
            v_val_aju_acres_rat_exp   := (v_val_aju_acres * v_valor_exp) / (v_valor_mint + v_valor_exp);
            v_val_aju_acres_rat_exp   := (v_val_aju_acres_rat_exp * ((pc_m100.valor_base_pis / v_vl_base_pis_cofins_total) * 100.00))/100.00;
         end if;
      end if;

      -- pc_m100.merc_int_ext
      -- 0 - Mercado interno
      -- 1 - Mercado externo

      -- regra para encontra o c�digo do credito - grupo 100
      if pc_m100.merc_int_ext = 0 and pc_m100.cvf_pis in (50, 53, 54, 56, 60, 63, 64, 66) and pc_m100.perc_pis = 1.65
      and pc_m100.valor_pis > 0.00000
      then
         v_cod_credito := 101;

         /*N�o faz rateio, pois s�o CST's exclusivamente de um tipo de receita*/
         if pc_m100.cvf_pis in (50, 60)
         then
            valor_base_pis_rat_mi := pc_m100.valor_base_pis;
            valor_pis_mi := pc_m100.valor_pis;

            v_val_aju_acres_rat_mi  := 0.00;
            v_val_aju_redu_rat_mi   := 0.00;
         end if;

         insere_m100(p_cod_matriz, v_cod_credito, 0, valor_base_pis_rat_mi, pc_m100.valor_base_pis,
                     pc_m100.perc_pis, pc_m100.cvf_pis, pc_m100.valor_pis, pc_m100.cod_cont_crec, 'P',
                     v_val_aju_acres_rat_mi, v_val_aju_redu_rat_mi, p_des_erro);

      end if;

      if pc_m100.merc_int_ext = 0 and pc_m100.cvf_pis in (50,53,54,56,60,63,64,66) and pc_m100.perc_pis <> 1.65
      and pc_m100.valor_pis > 0.00000 and pc_m100.cod_cont_crec <> 18
      then
         v_cod_credito := 102;

         /*N�o faz rateio, pois s�o CST's exclusivamente de um tipo de receita*/
         if pc_m100.cvf_pis in (50, 60)
         then
            valor_base_pis_rat_mi := pc_m100.valor_base_pis;
            valor_pis_mi := pc_m100.valor_pis;

            v_val_aju_acres_rat_mi  := 0.00;
            v_val_aju_redu_rat_mi   := 0.00;
         end if;

         insere_m100(p_cod_matriz,  v_cod_credito, 0, valor_base_pis_rat_mi, pc_m100.valor_base_pis, pc_m100.perc_pis, pc_m100.cvf_pis, pc_m100.valor_pis, pc_m100.cod_cont_crec, 'P',
                     v_val_aju_acres_rat_mi, v_val_aju_redu_rat_mi, p_des_erro);

      end if;

      if pc_m100.merc_int_ext = 0 and pc_m100.cvf_pis in (50,53,54,56,60,63,64,66) and pc_m100.perc_pis <> 1.65
      and pc_m100.valor_pis > 0.00000  and pc_m100.cod_cont_crec = 18
      then
         v_cod_credito := 104;

         /*N�o faz rateio, pois s�o CST's exclusivamente de um tipo de receita*/
         if pc_m100.cvf_pis in (50, 60)
         then
            valor_base_pis_rat_mi := pc_m100.valor_base_pis;
            valor_pis_mi := pc_m100.valor_pis;

            v_val_aju_acres_rat_mi  := 0.00;
            v_val_aju_redu_rat_mi   := 0.00;
         end if;

         insere_m100(p_cod_matriz,  v_cod_credito, 0, valor_base_pis_rat_mi, pc_m100.valor_base_pis, pc_m100.perc_pis, pc_m100.cvf_pis, pc_m100.valor_pis, pc_m100.cod_cont_crec, 'P',
                     v_val_aju_acres_rat_mi, v_val_aju_redu_rat_mi, p_des_erro);

      end if;

      if pc_m100.merc_int_ext = 1 and pc_m100.cvf_pis in (50,53,54,56,60,63,64,66) and pc_m100.valor_pis > 0.00000
      then
         v_cod_credito := 108;

         /*N�o faz rateio, pois s�o CST's exclusivamente de um tipo de receita*/
         if pc_m100.cvf_pis in (50, 60)
         then
            valor_base_pis_rat_mi := pc_m100.valor_base_pis;
            valor_pis_mi := pc_m100.valor_pis;

            v_val_aju_acres_rat_mi  := 0.00;
            v_val_aju_redu_rat_mi   := 0.00;
         end if;

         insere_m100(p_cod_matriz,  v_cod_credito, 0, valor_base_pis_rat_mi, pc_m100.valor_base_pis, pc_m100.perc_pis, pc_m100.cvf_pis, pc_m100.valor_pis, pc_m100.cod_cont_crec, 'P',
                     v_val_aju_acres_rat_mi, v_val_aju_redu_rat_mi,  p_des_erro);

      end if;

      -- regra para encontra o c�digo do credito - grupo 200
      if pc_m100.merc_int_ext = 0 and pc_m100.cvf_pis in (52,53,55,56,62,63,65,66) and pc_m100.perc_pis = 1.65
      and pc_m100.valor_pis > 0.00000
      then
         v_cod_credito := 201;

         /*N�o faz rateio, pois s�o CST's exclusivamente de um tipo de receita*/
         if pc_m100.cvf_pis in (52, 62)
         then
            valor_base_pis_rat_mint := pc_m100.valor_base_pis;
            valor_pis_mint := pc_m100.valor_pis;

            v_val_aju_acres_rat_mint:= 0.00;
            v_val_aju_redu_rat_mint := 0.00;
         end if;

         insere_m100(p_cod_matriz,  v_cod_credito, 0, valor_base_pis_rat_mint, pc_m100.valor_base_pis, pc_m100.perc_pis, pc_m100.cvf_pis, pc_m100.valor_pis, pc_m100.cod_cont_crec, 'P',
                     v_val_aju_acres_rat_mint,v_val_aju_redu_rat_mint , p_des_erro);

      end if;

      if pc_m100.merc_int_ext = 0 and pc_m100.cvf_pis in (52,53,55,56,62,63,65,66) and pc_m100.perc_pis <> 1.65
      and pc_m100.valor_pis > 0.00000 and pc_m100.cod_cont_crec <> 18
      then
         v_cod_credito := 202;

         /*N�o faz rateio, pois s�o CST's exclusivamente de um tipo de receita*/
         if pc_m100.cvf_pis in (52, 62)
         then
            valor_base_pis_rat_mint := pc_m100.valor_base_pis;
            valor_pis_mint := pc_m100.valor_pis;

            v_val_aju_acres_rat_mint:= 0.00;
            v_val_aju_redu_rat_mint := 0.00;
         end if;

         insere_m100(p_cod_matriz,  v_cod_credito, 0, valor_base_pis_rat_mint, pc_m100.valor_base_pis, pc_m100.perc_pis, pc_m100.cvf_pis, pc_m100.valor_pis, pc_m100.cod_cont_crec, 'P',
                     v_val_aju_acres_rat_mint,v_val_aju_redu_rat_mint , p_des_erro);

      end if;

      if pc_m100.merc_int_ext = 0 and pc_m100.cvf_pis in (52,53,55,56,62,63,65,66) and pc_m100.perc_pis <> 1.65
      and pc_m100.valor_pis > 0.00000 and pc_m100.cod_cont_crec = 18
      then
         v_cod_credito := 204;

         /*N�o faz rateio, pois s�o CST's exclusivamente de um tipo de receita*/
         if pc_m100.cvf_pis in (52, 62)
         then
            valor_base_pis_rat_mint := pc_m100.valor_base_pis;
            valor_pis_mint := pc_m100.valor_pis;

            v_val_aju_acres_rat_mint:= 0.00;
            v_val_aju_redu_rat_mint := 0.00;
         end if;

         insere_m100(p_cod_matriz,  v_cod_credito, 0, valor_base_pis_rat_mint, pc_m100.valor_base_pis, pc_m100.perc_pis, pc_m100.cvf_pis, pc_m100.valor_pis, pc_m100.cod_cont_crec, 'P',
                     v_val_aju_acres_rat_mint,v_val_aju_redu_rat_mint , p_des_erro);

      end if;

      if pc_m100.merc_int_ext = 1 and pc_m100.cvf_pis in (52,53,55,56,62,63,65,66)
      and pc_m100.valor_pis > 0.00000
      then
         v_cod_credito := 208;

         /*N�o faz rateio, pois s�o CST's exclusivamente de um tipo de receita*/
         if pc_m100.cvf_pis in (52, 62)
         then
            valor_base_pis_rat_mint := pc_m100.valor_base_pis;
            valor_pis_mint := pc_m100.valor_pis;

            v_val_aju_acres_rat_mint:= 0.00;
            v_val_aju_redu_rat_mint := 0.00;
         end if;

         insere_m100(p_cod_matriz,  v_cod_credito, 0, valor_base_pis_rat_mint, pc_m100.valor_base_pis, pc_m100.perc_pis, pc_m100.cvf_pis, pc_m100.valor_pis, pc_m100.cod_cont_crec, 'P',
                     v_val_aju_acres_rat_mint,v_val_aju_redu_rat_mint , p_des_erro);

      end if;

      -- regra para encontra o c�digo do credito - grupo 300
      if pc_m100.merc_int_ext = 0 and pc_m100.cvf_pis in (52,54,55,56,62,64,65,66) and pc_m100.perc_pis = 1.65
      and pc_m100.valor_pis > 0.00000
      then
         v_cod_credito := 301;

         /*N�o faz rateio, pois s�o CST's exclusivamente de um tipo de receita*/
         if pc_m100.cvf_pis in (52, 62)
         then
            valor_base_pis_rat_exp := pc_m100.valor_base_pis;
            valor_pis_exp := pc_m100.valor_pis;

            v_val_aju_acres_rat_exp := 0.00;
            v_val_aju_redu_rat_exp  := 0.00;
         end if;

         insere_m100(p_cod_matriz,  v_cod_credito, 0, valor_base_pis_rat_exp, pc_m100.valor_base_pis, pc_m100.perc_pis, pc_m100.cvf_pis, pc_m100.valor_pis, pc_m100.cod_cont_crec, 'P',
                     v_val_aju_acres_rat_exp,v_val_aju_redu_rat_exp , p_des_erro);

      end if;

      if pc_m100.merc_int_ext = 0 and pc_m100.cvf_pis in (52,54,55,56,62,64,65,66) and pc_m100.perc_pis <> 1.65
      and pc_m100.valor_pis > 0.00000 and pc_m100.cod_cont_crec <> 18
      then
         v_cod_credito := 302;

         /*N�o faz rateio, pois s�o CST's exclusivamente de um tipo de receita*/
         if pc_m100.cvf_pis in (52, 62)
         then
            valor_base_pis_rat_exp := pc_m100.valor_base_pis;
            valor_pis_exp := pc_m100.valor_pis;

            v_val_aju_acres_rat_exp := 0.00;
            v_val_aju_redu_rat_exp  := 0.00;
         end if;

         insere_m100(p_cod_matriz,  v_cod_credito, 0, valor_base_pis_rat_exp, pc_m100.valor_base_pis, pc_m100.perc_pis, pc_m100.cvf_pis, pc_m100.valor_pis, pc_m100.cod_cont_crec, 'P',
                     v_val_aju_acres_rat_exp,v_val_aju_redu_rat_exp , p_des_erro);
      end if;

      if pc_m100.merc_int_ext = 0 and pc_m100.cvf_pis in (52,54,55,56,62,64,65,66) and pc_m100.perc_pis <> 1.65
      and pc_m100.valor_pis > 0.00000 and pc_m100.cod_cont_crec = 18
      then
         v_cod_credito := 304;

         /*N�o faz rateio, pois s�o CST's exclusivamente de um tipo de receita*/
         if pc_m100.cvf_pis in (52, 62)
         then
            valor_base_pis_rat_exp := pc_m100.valor_base_pis;
            valor_pis_exp := pc_m100.valor_pis;

            v_val_aju_acres_rat_exp := 0.00;
            v_val_aju_redu_rat_exp  := 0.00;
         end if;

         insere_m100(p_cod_matriz,  v_cod_credito, 0, valor_base_pis_rat_exp, pc_m100.valor_base_pis, pc_m100.perc_pis, pc_m100.cvf_pis, pc_m100.valor_pis, pc_m100.cod_cont_crec, 'P',
                     v_val_aju_acres_rat_exp,v_val_aju_redu_rat_exp , p_des_erro);
      end if;

      if pc_m100.merc_int_ext = 1 and pc_m100.cvf_pis in (52,54,55,56,62,64,65,66) and pc_m100.valor_pis > 0.00000
      then
         v_cod_credito := 308;

         /*N�o faz rateio, pois s�o CST's exclusivamente de um tipo de receita*/
         if pc_m100.cvf_pis in (52, 62)
         then
            valor_base_pis_rat_exp := pc_m100.valor_base_pis;
            valor_pis_exp := pc_m100.valor_pis;

            v_val_aju_acres_rat_exp := 0.00;
            v_val_aju_redu_rat_exp  := 0.00;
         end if;

         insere_m100(p_cod_matriz,  v_cod_credito, 0, valor_base_pis_rat_exp, pc_m100.valor_base_pis, pc_m100.perc_pis, pc_m100.cvf_pis, pc_m100.valor_pis, pc_m100.cod_cont_crec, 'P',
                     v_val_aju_acres_rat_exp,v_val_aju_redu_rat_exp , p_des_erro);
      end if;

   END LOOP;
   /* ACERTA ARREDONDAMENTO */
   tot_vlr_acres := 0.00;
   tot_vlr_reduc := 0.00;

    select round(sum(sped_pc_m100.vl_ajus_acres),2),     round(sum(sped_pc_m100.vl_ajus_reduc),2)
    into tot_vlr_acres, tot_vlr_reduc
    from sped_pc_m100
    where sped_pc_m100.cod_matriz = p_cod_matriz
      and sped_pc_m100.tipo       = 'P';


    begin
       select nvl(sum(decode(obrf_703.ind_aj,0,obrf_703.vl_aj,0.00000)),0.00000),
              nvl(sum(decode(obrf_703.ind_aj,1,obrf_703.vl_aj,0.00000)),0.00000)
       into   v_val_aju_redu,
              v_val_aju_acres
       from obrf_703, fatu_500
       where obrf_703.cod_empresa    = fatu_500.codigo_empresa
         and fatu_500.codigo_matriz  = p_cod_matriz
         and obrf_703.reg            = 'M110'
         and obrf_703.mes_apur       = to_char(p_dat_inicial,'MM')
         and obrf_703.ano_apur       = to_char(p_dat_final,'YYYY');
    exception
        when no_data_found then
           v_val_aju_redu  := 0.00000;
           v_val_aju_acres := 0.00000;
    end;


    /* ACERTA REDU��O */
    w_conta_reg := 0;
    
    FOR rd_c in(
    select  rowid id_ajuste, sped_pc_m100.vl_ajus_reduc maior_valor
       from sped_pc_m100
       where sped_pc_m100.cod_matriz = p_cod_matriz
         and sped_pc_m100.tipo       = 'P'
         and sped_pc_m100.vl_ajus_reduc > 0.00
    order by sped_pc_m100.vl_ajus_reduc desc)
    LOOP
      w_conta_reg := w_conta_reg + 1;
      
      /* PEGA S� A PRIMEIRA LINHA QUE O MAIOR VALOR*/
      if w_conta_reg = 1
      then
         v_id_ajuste_pr := rd_c.id_ajuste;
      end if;
      
    END LOOP;
    
    if tot_vlr_reduc <> v_val_aju_redu
    then
       update sped_pc_m100
       set sped_pc_m100.vl_ajus_reduc = sped_pc_m100.vl_ajus_reduc + (v_val_aju_redu - tot_vlr_reduc)
       where sped_pc_m100.cod_matriz = p_cod_matriz
         and sped_pc_m100.tipo       = 'P'
         and rowid                   = v_id_ajuste_pr;

       update sped_pc_m100
       set sped_pc_m100.vl_cred_disp  = round(sped_pc_m100.vl_cred,2) +
           round(sped_pc_m100.vl_ajus_acres,2) -     round(sped_pc_m100.vl_ajus_reduc,2)
       where sped_pc_m100.cod_matriz = p_cod_matriz
         and sped_pc_m100.tipo       = 'P'
         and rowid                   = v_id_ajuste_pr;
    end if;


    /* ACERTA ACRESCIMO */
    w_conta_reg := 0;
    
    FOR rd_c in(
    select  rowid id_ajuste, sped_pc_m100.vl_ajus_acres maior_valor
       from sped_pc_m100
       where sped_pc_m100.cod_matriz = p_cod_matriz
         and sped_pc_m100.tipo       = 'P'
         and sped_pc_m100.vl_ajus_acres > 0.00
    order by sped_pc_m100.vl_ajus_reduc desc)
    LOOP
      w_conta_reg := w_conta_reg + 1;
      
      /* PEGA S� A PRIMEIRA LINHA QUE O MAIOR VALOR*/
      if w_conta_reg = 1
      then
         v_id_ajuste_pr := rd_c.id_ajuste;
      end if;
      
    END LOOP;

    if tot_vlr_acres <> v_val_aju_acres
    then
       update sped_pc_m100
       set sped_pc_m100.vl_ajus_acres = sped_pc_m100.vl_ajus_reduc + (v_val_aju_acres - tot_vlr_acres)
       where sped_pc_m100.cod_matriz = p_cod_matriz
         and sped_pc_m100.tipo       = 'P'
         and rowid                   = v_id_ajuste_pr;

       update sped_pc_m100
       set sped_pc_m100.vl_cred_disp  = round(sped_pc_m100.vl_cred,2) +
           round(sped_pc_m100.vl_ajus_acres,2) -     round(sped_pc_m100.vl_ajus_reduc,2)
       where sped_pc_m100.cod_matriz = p_cod_matriz
         and sped_pc_m100.tipo       = 'P';

    end if;

   /*******************************     COFINS  **********************************************************/
   FOR pc_m100 IN u_pc_m100_cofins (p_cod_matriz, p_dat_inicial, p_dat_final) LOOP

      v_val_aju_redu  := 0.00000;
      v_val_aju_acres := 0.00000;

      v_val_aju_redu_rat_mi   := 0.00000;
      v_val_aju_redu_rat_mint := 0.00000;
      v_val_aju_redu_rat_exp  := 0.00000;
      v_val_aju_acres_rat_mi  := 0.00000;
      v_val_aju_acres_rat_mint:= 0.00000;
      v_val_aju_acres_rat_exp := 0.00000;

      begin
         select sum(decode(obrf_703.ind_aj,0,obrf_703.vl_aj,0.00000)),
                sum(decode(obrf_703.ind_aj,1,obrf_703.vl_aj,0.00000))
         into   v_val_aju_redu,
                v_val_aju_acres
         from obrf_703, fatu_500
         where obrf_703.cod_empresa    = fatu_500.codigo_empresa
           and fatu_500.codigo_matriz  = p_cod_matriz
           and obrf_703.reg            = 'M510'
           and obrf_703.cst_pis_cofins = pc_m100.cvf_cofins
           and obrf_703.mes_apur       = to_char(p_dat_inicial,'MM')
           and obrf_703.ano_apur       = to_char(p_dat_final,'YYYY');
      exception
          when no_data_found then
             v_val_aju_redu  := 0.00000;
             v_val_aju_acres := 0.00000;
      end;

      if v_val_aju_acres is null
      then
         v_val_aju_acres := 0.00;
      end if;

      if v_val_aju_redu is null
      then
         v_val_aju_redu := 0.00;
      end if;

      -- grupo 100
      if v_valor_mi > 0.00000
      then
         if pc_m100.cvf_cofins in ('56', '66')
         then
            valor_base_pis_rat_mi := (pc_m100.valor_base_cofins * v_valor_mi) / (v_valor_mi + v_valor_mint + v_valor_exp);
            valor_pis_mi          := (pc_m100.valor_cofins * v_valor_mi) / (v_valor_mi + v_valor_mint + v_valor_exp);

            v_val_aju_redu_rat_mi    := (v_val_aju_redu * v_valor_mi) / (v_valor_mi + v_valor_mint + v_valor_exp);
            v_val_aju_redu_rat_mi    := (v_val_aju_redu_rat_mi * ((pc_m100.valor_base_cofins / v_vl_base_pis_cofins_total) * 100.00))/100.00;
            v_val_aju_acres_rat_mi   := (v_val_aju_acres * v_valor_mi) / (v_valor_mi + v_valor_mint + v_valor_exp);
            v_val_aju_acres_rat_mi   := (v_val_aju_acres_rat_mi * ((pc_m100.valor_base_cofins / v_vl_base_pis_cofins_total) * 100.00))/100.00;
         elsif pc_m100.cvf_cofins in ('53', '63')
         then
            valor_base_pis_rat_mi := (pc_m100.valor_base_cofins * v_valor_mi) / (v_valor_mi + v_valor_mint);
            valor_pis_mi          := (pc_m100.valor_cofins * v_valor_mi) / (v_valor_mi + v_valor_mint + v_valor_exp);

            v_val_aju_redu_rat_mi    := (v_val_aju_redu * v_valor_mi) / (v_valor_mi + v_valor_mint);
            v_val_aju_redu_rat_mi    := (v_val_aju_redu_rat_mi * ((pc_m100.valor_base_cofins / v_vl_base_pis_cofins_total) * 100.00))/100.00;
            v_val_aju_acres_rat_mi   := (v_val_aju_acres * v_valor_mi) / (v_valor_mi + v_valor_mint);
            v_val_aju_acres_rat_mi   := (v_val_aju_acres_rat_mi * ((pc_m100.valor_base_cofins / v_vl_base_pis_cofins_total) * 100.00))/100.00;
         elsif pc_m100.cvf_cofins in ('54', '64')
         then
            valor_base_pis_rat_mi := (pc_m100.valor_base_cofins * v_valor_mi) / (v_valor_mi + v_valor_exp);
            valor_pis_mi          := (pc_m100.valor_cofins * v_valor_mi) / (v_valor_mi + v_valor_exp);

            v_val_aju_redu_rat_mi     := (v_val_aju_redu * v_valor_mi) / (v_valor_mi + v_valor_mint);
            v_val_aju_redu_rat_mi     := (v_val_aju_redu_rat_mi * ((pc_m100.valor_base_cofins / v_vl_base_pis_cofins_total) * 100.00))/100.00;
            v_val_aju_acres_rat_mi    := (v_val_aju_acres * v_valor_mi) / (v_valor_mi + v_valor_mint);
            v_val_aju_acres_rat_mi    := (v_val_aju_acres_rat_mi * ((pc_m100.valor_base_cofins / v_vl_base_pis_cofins_total) * 100.00))/100.00;
         end if;
      end if;

      -- grupo 200
      if v_valor_mint > 0.00000
      then
         if pc_m100.cvf_cofins in ('56', '66')
         then
            valor_base_pis_rat_mint := (pc_m100.valor_base_cofins * v_valor_mint) / (v_valor_mi + v_valor_mint + v_valor_exp);
            valor_pis_mint          := (pc_m100.valor_cofins * v_valor_mint) / (v_valor_mi + v_valor_mint + v_valor_exp);

            v_val_aju_redu_rat_mint     := (v_val_aju_redu * v_valor_mint) / (v_valor_mi + v_valor_mint + v_valor_exp);
            v_val_aju_redu_rat_mint     := (v_val_aju_redu_rat_mint * ((pc_m100.valor_base_cofins / v_vl_base_pis_cofins_total) * 100.00))/100.00;
            v_val_aju_acres_rat_mint    := (v_val_aju_acres * v_valor_mint) / (v_valor_mi + v_valor_mint + v_valor_exp);
            v_val_aju_acres_rat_mint    := (v_val_aju_acres_rat_mint * ((pc_m100.valor_base_cofins / v_vl_base_pis_cofins_total) * 100.00))/100.00;
         elsif pc_m100.cvf_cofins in ('53', '63')
         then
            valor_base_pis_rat_mint := (pc_m100.valor_base_cofins * v_valor_mint) / (v_valor_mi + v_valor_mint);
            valor_pis_mint          := (pc_m100.valor_cofins * v_valor_mint) / (v_valor_mi + v_valor_mint);

            v_val_aju_redu_rat_mint    := (v_val_aju_redu * v_valor_mint) / (v_valor_mi + v_valor_mint);
            v_val_aju_redu_rat_mint    := (v_val_aju_redu_rat_mint * ((pc_m100.valor_base_cofins / v_vl_base_pis_cofins_total) * 100.00))/100.00;
            v_val_aju_acres_rat_mint   := (v_val_aju_acres * v_valor_mint) / (v_valor_mi + v_valor_mint);
            v_val_aju_acres_rat_mint   := (v_val_aju_acres_rat_mint * ((pc_m100.valor_base_cofins / v_vl_base_pis_cofins_total) * 100.00))/100.00;
         elsif pc_m100.cvf_cofins in ('55', '65')
         then
            valor_base_pis_rat_mint := (pc_m100.valor_base_cofins * v_valor_mint) / (v_valor_mint + v_valor_exp);
            valor_pis_mint          := (pc_m100.valor_cofins * v_valor_mint) / (v_valor_mint + v_valor_exp);

            v_val_aju_redu_rat_mint    := (v_val_aju_redu * v_valor_mint) / (v_valor_mint + v_valor_exp);
            v_val_aju_redu_rat_mint    := (v_val_aju_redu_rat_mint * ((pc_m100.valor_base_cofins / v_vl_base_pis_cofins_total) * 100.00))/100.00;
            v_val_aju_acres_rat_mint   := (v_val_aju_acres * v_valor_mint) / (v_valor_mint + v_valor_exp);
            v_val_aju_acres_rat_mint   := (v_val_aju_acres_rat_mint * ((pc_m100.valor_base_cofins / v_vl_base_pis_cofins_total) * 100.00))/100.00;
         end if;
      end if;

      -- grupo 300
      if v_valor_exp > 0.00000
      then
         if pc_m100.cvf_cofins in ('56', '66')
         then
            valor_base_pis_rat_exp := (pc_m100.valor_base_cofins * v_valor_exp) / (v_valor_mi + v_valor_mint + v_valor_exp);
            valor_pis_exp          := (pc_m100.valor_cofins * v_valor_exp) / (v_valor_mi + v_valor_mint + v_valor_exp);

            v_val_aju_redu_rat_exp    := (v_val_aju_redu * v_valor_exp) / (v_valor_mi + v_valor_mint + v_valor_exp);
            v_val_aju_redu_rat_exp    := (v_val_aju_redu_rat_exp * ((pc_m100.valor_base_cofins / v_vl_base_pis_cofins_total) * 100.00))/100.00;
            v_val_aju_acres_rat_exp   := (v_val_aju_acres * v_valor_exp) / (v_valor_mi + v_valor_mint + v_valor_exp);
            v_val_aju_acres_rat_exp   := (v_val_aju_acres_rat_exp * ((pc_m100.valor_base_cofins / v_vl_base_pis_cofins_total) * 100.00))/100.00;
         elsif pc_m100.cvf_cofins in ('54', '64')
         then
            valor_base_pis_rat_exp := (pc_m100.valor_base_cofins * v_valor_exp) / (v_valor_mi + v_valor_exp);
            valor_pis_exp          := (pc_m100.valor_cofins * v_valor_exp) / (v_valor_mi + v_valor_exp);

            v_val_aju_redu_rat_exp    := (v_val_aju_redu * v_valor_exp) / (v_valor_mi + v_valor_exp);
            v_val_aju_redu_rat_exp    := (v_val_aju_redu_rat_exp * ((pc_m100.valor_base_cofins / v_vl_base_pis_cofins_total) * 100.00))/100.00;
            v_val_aju_acres_rat_exp   := (v_val_aju_acres * v_valor_exp) / (v_valor_mi + v_valor_exp);
            v_val_aju_acres_rat_exp   := (v_val_aju_acres_rat_exp * ((pc_m100.valor_base_cofins / v_vl_base_pis_cofins_total) * 100.00))/100.00;
         elsif pc_m100.cvf_cofins in ('55', '65')
         then
            valor_base_pis_rat_exp := (pc_m100.valor_base_cofins * v_valor_exp) / (v_valor_mint + v_valor_exp);
            valor_pis_exp          := (pc_m100.valor_cofins * v_valor_exp) / (v_valor_mint + v_valor_exp);

            v_val_aju_redu_rat_exp    := (v_val_aju_redu * v_valor_exp) / (v_valor_mint + v_valor_exp);
            v_val_aju_redu_rat_exp    := (v_val_aju_redu_rat_exp * ((pc_m100.valor_base_cofins / v_vl_base_pis_cofins_total) * 100.00))/100.00;
            v_val_aju_acres_rat_exp   := (v_val_aju_acres * v_valor_exp) / (v_valor_mint + v_valor_exp);
            v_val_aju_acres_rat_exp   := (v_val_aju_acres_rat_exp * ((pc_m100.valor_base_cofins / v_vl_base_pis_cofins_total) * 100.00))/100.00;
         end if;
      end if;

      -- pc_m100.merc_int_ext
      -- 0 - Mercado interno
      -- 1 - Mercado externo

      -- regra para encontra o c�digo do credito - grupo 100
      if pc_m100.merc_int_ext = 0 and pc_m100.cvf_cofins in (50,53,54,56,60,63,64,66) and pc_m100.perc_cofins = 7.60
      and pc_m100.valor_cofins > 0.00000
      then
         v_cod_credito := 101;

         /*N�o faz rateio, pois s�o CST's exclusivamente de um tipo de receita*/
         if pc_m100.cvf_cofins in (50, 60)
         then
            valor_base_pis_rat_mi := pc_m100.valor_base_cofins;
            valor_pis_mi := pc_m100.valor_cofins;

            v_val_aju_acres_rat_mi  := 0.00;
            v_val_aju_redu_rat_mi   := 0.00;
         end if;

         insere_m100(p_cod_matriz,  v_cod_credito, 0, valor_base_pis_rat_mi, pc_m100.valor_base_cofins, pc_m100.perc_cofins, pc_m100.cvf_cofins, pc_m100.valor_cofins, pc_m100.cod_cont_crec, 'C',
                     v_val_aju_acres_rat_mi, v_val_aju_redu_rat_mi,  p_des_erro);
      end if;

      if pc_m100.merc_int_ext = 0 and pc_m100.cvf_cofins in (50,53,54,56,60,63,64,66) and pc_m100.perc_cofins <> 7.60
      and pc_m100.valor_cofins > 0.00000  and pc_m100.cod_cont_crec <> 18
      then
         v_cod_credito := 102;

         /*N�o faz rateio, pois s�o CST's exclusivamente de um tipo de receita*/
         if pc_m100.cvf_cofins in (50, 60)
         then
            valor_base_pis_rat_mi := pc_m100.valor_base_cofins;
            valor_pis_mi := pc_m100.valor_cofins;

            v_val_aju_acres_rat_mi  := v_val_aju_acres;
            v_val_aju_redu_rat_mi   := v_val_aju_redu;
         end if;

         insere_m100(p_cod_matriz,  v_cod_credito, 0, valor_base_pis_rat_mi, pc_m100.valor_base_cofins, pc_m100.perc_cofins, pc_m100.cvf_cofins, pc_m100.valor_cofins, pc_m100.cod_cont_crec, 'C',
                     v_val_aju_acres_rat_mi, v_val_aju_redu_rat_mi,  p_des_erro);
      end if;
      if pc_m100.merc_int_ext = 0 and pc_m100.cvf_cofins in (50,53,54,56,60,63,64,66) and pc_m100.perc_cofins <> 7.60
      and pc_m100.valor_cofins > 0.00000  and pc_m100.cod_cont_crec = 18
      then
         v_cod_credito := 104;

         /*N�o faz rateio, pois s�o CST's exclusivamente de um tipo de receita*/
         if pc_m100.cvf_cofins in (50, 60)
         then
            valor_base_pis_rat_mi := pc_m100.valor_base_cofins;
            valor_pis_mi := pc_m100.valor_cofins;

            v_val_aju_acres_rat_mi  := 0.00;
            v_val_aju_redu_rat_mi   := 0.00;
         end if;

         insere_m100(p_cod_matriz,  v_cod_credito, 0, valor_base_pis_rat_mi, pc_m100.valor_base_cofins, pc_m100.perc_cofins, pc_m100.cvf_cofins, pc_m100.valor_cofins, pc_m100.cod_cont_crec, 'C',
                     v_val_aju_acres_rat_mi, v_val_aju_redu_rat_mi,  p_des_erro);
      end if;

      if pc_m100.merc_int_ext = 1 and pc_m100.cvf_cofins in (50,53,54,56,60,63,64,66) and pc_m100.valor_cofins > 0.00000
      then
         v_cod_credito := 108;

         /*N�o faz rateio, pois s�o CST's exclusivamente de um tipo de receita*/
         if pc_m100.cvf_cofins in (50, 60)
         then
            valor_base_pis_rat_mi := pc_m100.valor_base_cofins;
            valor_pis_mi := pc_m100.valor_cofins;

            v_val_aju_acres_rat_mi  := v_val_aju_acres;
            v_val_aju_redu_rat_mi   := v_val_aju_redu;
         end if;

         insere_m100(p_cod_matriz,  v_cod_credito, 0, valor_base_pis_rat_mi, pc_m100.valor_base_cofins, pc_m100.perc_cofins, pc_m100.cvf_cofins, pc_m100.valor_cofins, pc_m100.cod_cont_crec, 'C',
                     v_val_aju_acres_rat_mi, v_val_aju_redu_rat_mi,  p_des_erro);
      end if;

      -- regra para encontra o c�digo do credito - grupo 200
      if pc_m100.merc_int_ext = 0 and pc_m100.cvf_cofins in (52,53,55,56,62,63,65,66) and pc_m100.perc_cofins = 7.60
      and pc_m100.valor_cofins > 0.00000
      then
         v_cod_credito := 201;

         /*N�o faz rateio, pois s�o CST's exclusivamente de um tipo de receita*/
         if pc_m100.cvf_cofins in (52, 62)
         then
            valor_base_pis_rat_mint := pc_m100.valor_base_cofins;
            valor_pis_mint          := pc_m100.valor_cofins;

            v_val_aju_acres_rat_mint:= 0.00;
            v_val_aju_redu_rat_mint := 0.00;
         end if;

         insere_m100(p_cod_matriz,  v_cod_credito, 0, valor_base_pis_rat_mint, pc_m100.valor_base_cofins, pc_m100.perc_cofins, pc_m100.cvf_cofins, pc_m100.valor_cofins, pc_m100.cod_cont_crec, 'C',
                     v_val_aju_acres_rat_mint,v_val_aju_redu_rat_mint , p_des_erro);
      end if;

      if pc_m100.merc_int_ext = 0 and pc_m100.cvf_cofins in (52,53,55,56,62,63,65,66) and pc_m100.perc_cofins <> 7.60
      and pc_m100.valor_cofins > 0.00000   and pc_m100.cod_cont_crec <> 18
      then
         v_cod_credito := 202;

         /*N�o faz rateio, pois s�o CST's exclusivamente de um tipo de receita*/
         if pc_m100.cvf_cofins in (52, 62)
         then
            valor_base_pis_rat_mint := pc_m100.valor_base_cofins;
            valor_pis_mint          := pc_m100.valor_cofins;

            v_val_aju_acres_rat_mint:= v_val_aju_acres;
            v_val_aju_redu_rat_mint := v_val_aju_redu;
         end if;

         insere_m100(p_cod_matriz,  v_cod_credito, 0, valor_base_pis_rat_mint, pc_m100.valor_base_cofins, pc_m100.perc_cofins, pc_m100.cvf_cofins, pc_m100.valor_cofins, pc_m100.cod_cont_crec, 'C',
                     v_val_aju_acres_rat_mint,v_val_aju_redu_rat_mint , p_des_erro);
      end if;

      if pc_m100.merc_int_ext = 0 and pc_m100.cvf_cofins in (52,53,55,56,62,63,65,66) and pc_m100.perc_cofins <> 7.60
      and pc_m100.valor_cofins > 0.00000   and pc_m100.cod_cont_crec = 18
      then
         v_cod_credito := 204;

         /*N�o faz rateio, pois s�o CST's exclusivamente de um tipo de receita*/
         if pc_m100.cvf_cofins in (52, 62)
         then
            valor_base_pis_rat_mint := pc_m100.valor_base_cofins;
            valor_pis_mint          := pc_m100.valor_cofins;

            v_val_aju_acres_rat_mint:= 0.00;
            v_val_aju_redu_rat_mint := 0.00;
         end if;

         insere_m100(p_cod_matriz,  v_cod_credito, 0, valor_base_pis_rat_mint, pc_m100.valor_base_cofins, pc_m100.perc_cofins, pc_m100.cvf_cofins, pc_m100.valor_cofins, pc_m100.cod_cont_crec, 'C',
                     v_val_aju_acres_rat_mint,v_val_aju_redu_rat_mint , p_des_erro);
      end if;

      if pc_m100.merc_int_ext = 1 and pc_m100.cvf_cofins in (52,53,55,56,62,63,65,66) and pc_m100.valor_cofins > 0.00000
      then
         v_cod_credito := 208;

         /*N�o faz rateio, pois s�o CST's exclusivamente de um tipo de receita*/
         if pc_m100.cvf_cofins in (52, 62)
         then
            valor_base_pis_rat_mint := pc_m100.valor_base_cofins;
            valor_pis_mint          := pc_m100.valor_cofins;

            v_val_aju_acres_rat_mint:= 0.00;
            v_val_aju_redu_rat_mint := 0.00;
         end if;

         insere_m100(p_cod_matriz,  v_cod_credito, 0, valor_base_pis_rat_mint, pc_m100.valor_base_cofins, pc_m100.perc_cofins, pc_m100.cvf_cofins, pc_m100.valor_cofins, pc_m100.cod_cont_crec, 'C',
                     v_val_aju_acres_rat_mint,v_val_aju_redu_rat_mint , p_des_erro);
      end if;

      -- regra para encontra o c�digo do credito - grupo 300
      if pc_m100.merc_int_ext = 0 and pc_m100.cvf_cofins in (52,54,55,56,62,64,65,66) and pc_m100.perc_cofins = 7.60
      and pc_m100.valor_cofins > 0.00000
      then
         v_cod_credito := 301;

         /*N�o faz rateio, pois s�o CST's exclusivamente de um tipo de receita*/
         if pc_m100.cvf_cofins in (52, 62)
         then
            valor_base_pis_rat_exp := pc_m100.valor_base_cofins;
            valor_pis_exp          := pc_m100.valor_cofins;

            v_val_aju_acres_rat_exp := 0.00;
            v_val_aju_redu_rat_exp  := 0.00;
         end if;

         insere_m100(p_cod_matriz,  v_cod_credito, 0, valor_base_pis_rat_exp, pc_m100.valor_base_cofins, pc_m100.perc_cofins, pc_m100.cvf_cofins, pc_m100.valor_cofins, pc_m100.cod_cont_crec, 'C',
                     v_val_aju_acres_rat_exp,v_val_aju_redu_rat_exp ,  p_des_erro);

      end if;

      if pc_m100.merc_int_ext = 0 and pc_m100.cvf_cofins in (52,54,55,56,62,64,65,66) and pc_m100.perc_cofins <> 7.60
      and pc_m100.valor_cofins > 0.00000   and pc_m100.cod_cont_crec <> 18
      then
         v_cod_credito := 302;

         /*N�o faz rateio, pois s�o CST's exclusivamente de um tipo de receita*/
         if pc_m100.cvf_cofins in (52, 62)
         then
            valor_base_pis_rat_exp := pc_m100.valor_base_cofins;
            valor_pis_exp          := pc_m100.valor_cofins;

            v_val_aju_acres_rat_exp := 0.00;
            v_val_aju_redu_rat_exp  := 0.00;
         end if;

         insere_m100(p_cod_matriz,  v_cod_credito, 0, valor_base_pis_rat_exp, pc_m100.valor_base_cofins, pc_m100.perc_cofins, pc_m100.cvf_cofins, pc_m100.valor_cofins, pc_m100.cod_cont_crec, 'C',
                     v_val_aju_acres_rat_exp,v_val_aju_redu_rat_exp , p_des_erro);
      end if;

      if pc_m100.merc_int_ext = 0 and pc_m100.cvf_cofins in (52,54,55,56,62,64,65,66) and pc_m100.perc_cofins <> 7.60
      and pc_m100.valor_cofins > 0.00000   and pc_m100.cod_cont_crec = 18
      then
         v_cod_credito := 304;

         /*N�o faz rateio, pois s�o CST's exclusivamente de um tipo de receita*/
         if pc_m100.cvf_cofins in (52, 62)
         then
            valor_base_pis_rat_exp := pc_m100.valor_base_cofins;
            valor_pis_exp          := pc_m100.valor_cofins;

            v_val_aju_acres_rat_exp := 0.00;
            v_val_aju_redu_rat_exp  := 0.000;
         end if;

         insere_m100(p_cod_matriz,  v_cod_credito, 0, valor_base_pis_rat_exp, pc_m100.valor_base_cofins, pc_m100.perc_cofins, pc_m100.cvf_cofins, pc_m100.valor_cofins, pc_m100.cod_cont_crec, 'C',
                     v_val_aju_acres_rat_exp,v_val_aju_redu_rat_exp , p_des_erro);
      end if;

      if pc_m100.merc_int_ext = 1 and pc_m100.cvf_cofins in (52,54,55,56,62,64,65,66) and pc_m100.valor_cofins > 0.00000
      then
         v_cod_credito := 308;

         /*N�o faz rateio, pois s�o CST's exclusivamente de um tipo de receita*/
         if pc_m100.cvf_cofins in (52, 62)
         then
            valor_base_pis_rat_exp := pc_m100.valor_base_cofins;
            valor_pis_exp          := pc_m100.valor_cofins;

            v_val_aju_acres_rat_exp := 0.00;
            v_val_aju_redu_rat_exp  := 0.00;
         end if;

         insere_m100(p_cod_matriz,  v_cod_credito, 0, valor_base_pis_rat_exp, pc_m100.valor_base_cofins, pc_m100.perc_cofins, pc_m100.cvf_cofins, pc_m100.valor_cofins, pc_m100.cod_cont_crec, 'C',
                     v_val_aju_acres_rat_exp,v_val_aju_redu_rat_exp , p_des_erro);
      end if;

   END LOOP;

   /* ACERTA ARREDONDAMENTO */
   tot_vlr_acres := 0.00;
   tot_vlr_reduc := 0.00;

    select round(sum(sped_pc_m100.vl_ajus_acres),2),     round(sum(sped_pc_m100.vl_ajus_reduc),2)
    into tot_vlr_acres, tot_vlr_reduc
       from sped_pc_m100
       where sped_pc_m100.cod_matriz = p_cod_matriz
         and sped_pc_m100.tipo       = 'C';


    begin
       select nvl(sum(decode(obrf_703.ind_aj,0,obrf_703.vl_aj,0.00000)),0.00000),
              nvl(sum(decode(obrf_703.ind_aj,1,obrf_703.vl_aj,0.00000)),0.00000)
       into   v_val_aju_redu,
              v_val_aju_acres
       from obrf_703, fatu_500
       where obrf_703.cod_empresa    = fatu_500.codigo_empresa
         and fatu_500.codigo_matriz  = p_cod_matriz
         and obrf_703.reg            = 'M510'
         and obrf_703.mes_apur       = to_char(p_dat_inicial,'MM')
         and obrf_703.ano_apur       = to_char(p_dat_final,'YYYY');
    exception
        when no_data_found then
           v_val_aju_redu  := 0.00000;
           v_val_aju_acres := 0.00000;
    end;

    /* ACERTA REDU��O */
    w_conta_reg := 0;
    
    FOR rd_r in(
    select  rowid id_ajuste, sped_pc_m100.vl_ajus_reduc maior_valor
--    into id_ajuste, maior_valor
       from sped_pc_m100
       where sped_pc_m100.cod_matriz = p_cod_matriz
         and sped_pc_m100.tipo       = 'C'
         and sped_pc_m100.vl_ajus_reduc > 0.00
    order by sped_pc_m100.vl_ajus_reduc desc)
    LOOP
      w_conta_reg := w_conta_reg + 1;
      
      /* PEGA S� A PRIMEIRA LINHA QUE O MAIOR VALOR*/
      if w_conta_reg = 1
      then
         v_id_ajuste_pr := rd_r.id_ajuste;
      end if;
      
    END LOOP;
    
    if tot_vlr_reduc <> v_val_aju_redu
    then
       update sped_pc_m100
       set sped_pc_m100.vl_ajus_reduc = sped_pc_m100.vl_ajus_reduc + (v_val_aju_redu - tot_vlr_reduc)
       where sped_pc_m100.cod_matriz = p_cod_matriz
         and sped_pc_m100.tipo       = 'C'
         and rowid                   = v_id_ajuste_pr;

       update sped_pc_m100
       set sped_pc_m100.vl_cred_disp  = round(sped_pc_m100.vl_cred,2) +
           round(sped_pc_m100.vl_ajus_acres,2) -     round(sped_pc_m100.vl_ajus_reduc,2)
       where sped_pc_m100.cod_matriz = p_cod_matriz
         and sped_pc_m100.tipo       = 'C'
         and rowid                   = v_id_ajuste_pr;

    end if;

    /* ACERTA ACRESCIMO */
    w_conta_reg := 0;
    
    FOR rd_c in(
    select  rowid id_ajuste, sped_pc_m100.vl_ajus_acres maior_valor
       from sped_pc_m100
       where sped_pc_m100.cod_matriz = p_cod_matriz
         and sped_pc_m100.tipo       = 'C'
         and sped_pc_m100.vl_ajus_acres > 0.00
    order by sped_pc_m100.vl_ajus_reduc desc)
    LOOP
      w_conta_reg := w_conta_reg + 1;
      
      /* PEGA S� A PRIMEIRA LINHA QUE O MAIOR VALOR*/
      if w_conta_reg = 1
      then
         v_id_ajuste_pr := rd_c.id_ajuste;
      end if;
      
    END LOOP;
    
    BEGIN
       insert into sped_pc_m_dctf
          (cod_empresa, mes_apuracao, ano_apuracao, ind_pis_cofins, cod_rec, val_debito)
          (select cod_empresa, mes_apuracao, ano_apuracao, ind_pis_cofins, cod_rec, val_debito from obrf_713 
           where obrf_713.cod_empresa = p_cod_matriz
             and obrf_713.mes_apuracao = to_char(p_dat_inicial, 'MM')
             and obrf_713.ano_apuracao = to_char(p_dat_inicial, 'YYYY'));
    END;

    
    if tot_vlr_acres <> v_val_aju_acres
    then
       update sped_pc_m100
       set sped_pc_m100.vl_ajus_acres = sped_pc_m100.vl_ajus_reduc + (v_val_aju_acres - tot_vlr_acres)
       where sped_pc_m100.cod_matriz = p_cod_matriz
         and sped_pc_m100.tipo       = 'C'
         and rowid                   = v_id_ajuste_pr;

       update sped_pc_m100
       set sped_pc_m100.vl_cred_disp  = round(sped_pc_m100.vl_cred,2) +
           round(sped_pc_m100.vl_ajus_acres,2) -     round(sped_pc_m100.vl_ajus_reduc,2)
       where sped_pc_m100.cod_matriz = p_cod_matriz
         and sped_pc_m100.tipo       = 'C';


    end if;

   commit;

end inter_pr_gera_sped_m100_pc;
/
exec inter_pr_recompile;

