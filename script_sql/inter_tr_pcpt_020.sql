create or replace trigger inter_tr_pcpt_020

  before insert or
         delete or
         update of qtde_quilos_acab, peso_bruto, tara, 
                   codigo_deposito,  rolo_estoque, 
                   pedido_venda,     seq_item_pedido, 
                   tabela_origem_cardex 
                on pcpt_020
  for each row

declare
   v_entrada_saida           estq_005.entrada_saida%type;
   v_transac_entrada         estq_005.transac_entrada%type;
   v_transacao_cancel        estq_005.codigo_transacao%type;
   v_peso_rolo               pcpt_020.qtde_quilos_acab%type;
   v_qtde_kilo               pcpt_020.peso_bruto%type;
   v_tipo_volume             basi_205.tipo_volume%type;

   v_numero_documento        estq_300.numero_documento%type;
   v_serie_documento         estq_300.serie_documento%type;
   v_sequencia_documento     estq_300.sequencia_documento%type;
   v_cnpj_9                  estq_300.cnpj_9%type;
   v_cnpj_4                  estq_300.cnpj_4%type;
   v_cnpj_2                  estq_300.cnpj_2%type;
   v_tabela_origem_cardex    estq_300.tabela_origem%type := 'PCPT_020';

   v_data_cancelamento       date;

   v_peso_ficha              pcpt_020.qtde_quilos_acab%type := 0.00;
   v_controle_peso           empr_001.controle_benefic%type; -- Parametro para controle de peso na entrada do estoque.
   -- Parametro que indica se permite romaneio de items cujo lote difere do lote do pedido
   v_permi_lotes_diferentes  empr_002.preromane_lote_dif_pedido%type;

   v_lote_pedi_110           number(9) := 0;
   v_tem_pcpt021             number(9);
   v_ccusto_cardex           number(6);

   v_tipo_prod_deposito      basi_205.tipo_prod_deposito%type;
   v_confirma_coleta         basi_205.confirma_coleta%type;
   v_tipo_prod_deposito_new  basi_205.tipo_prod_deposito%type;
   v_tipo_prod_deposito_old  basi_205.tipo_prod_deposito%type;
   v_nivel_origem            pcpt_020.panoacab_nivel99%type;

   v_encontrou_reg           number(1) := 0;
   v_retorno_select          number(1) := 0;

   v_executa_trigger         number;

   v_encontrou_simbolos      number(1);
   v_rpt_etiq_rolo           fatu_500.rpt_etiq_rolo%type;
   v_simbolo_cons            basi_400.codigo_informacao%type;

   v_numero_op_sped          number(9);
   v_tipo_ordem_sped         number(2);
   v_estagio_op_sped         number(5);
   v_lote_beneficiados       varchar2(1) := 'S';
   v_reposicao_sped          number(9);


   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);
   v_periodo_estoque         empr_001.periodo_estoque%type;
   v_nome_programa           varchar2(20);
   
   v_unidade_medida          varchar2(2); 
   v_peso_rolo_padrao        number(9,4); 
 
   v_largura                 number(7,3); 
   v_gramatura               number(11,7); 
 
   v_largura_padrao          number(7,3); 
   v_gramatura_padrao        number(11,7); 
 
   v_multiplicador           number; 
   
   v_cod_estagio_agrupador   pcpb_015.cod_estagio_agrupador%type;                 
   v_seq_operacao_agrupador  pcpb_015.seq_operacao_agrupador%type;
   
   v_diametro_rolo           number(11,3);
   v_raio                    number(11,3);
   
   data_fechamento_blocok    date;
   controleProcessoBlocoK    number;
   v_data_movimento_rolo     date;

begin

   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   begin
       controleProcessoBlocoK := 0;
	   
	   select 1 
	   INTO controleProcessoBlocoK
	   from empr_007 
	   where param       = 'obrf.controleProcessoBlocoK' 
	   and   default_int = 1;
   exception when others then
	   controleProcessoBlocoK := 0;
   end;
   
   begin
	   data_fechamento_blocok := null;
	   
	   select DEFAULT_DAT
	   INTO data_fechamento_blocok
	   from empr_007
	   where param = 'obrf.dataFechamentoBlocoK';
   exception when others then
	   data_fechamento_blocok := null;
   end;
   
   if v_executa_trigger = 0
   then

      inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                              ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

      v_nome_programa := inter_fn_nome_programa(ws_sid);

      v_ccusto_cardex := 0;

      if inserting or updating
      then
         -- carrega o centro de custo
         if :new.centro_custo_cardex is null
         then
            v_ccusto_cardex := 0;
         else
            v_ccusto_cardex := :new.centro_custo_cardex;
         end if;
         
         if  :new.codigo_rolo > 0
			 or
	 	     (:new.panoacab_nivel99  <> :old.panoacab_nivel99  and
			  :new.panoacab_grupo    <> :old.panoacab_grupo    and
			  :new.panoacab_subgrupo <> :old.panoacab_subgrupo and
			  :new.panoacab_item     <> :old.panoacab_item)
			 or
			 :new.qtde_quilos_acab  <> :old.qtde_quilos_acab
		 then
			--ATUALIZAR METROS_CUBICOS             
			begin 
				select basi_020.peso_rolo, basi_020.diametro_padrao,
					   basi_020.largura_1 
				into   v_peso_rolo_padrao, v_diametro_rolo,
					   v_largura_padrao
				from basi_020 
				where basi_020.basi030_nivel030 = :new.panoacab_nivel99 
				and   basi_020.basi030_referenc = :new.panoacab_grupo 
				and   basi_020.tamanho_ref      = :new.panoacab_subgrupo; 
			exception when others then 
				v_peso_rolo_padrao := 0; 
				v_diametro_rolo    := 0;
				v_largura_padrao   := 0;
			end;
			
			if  v_peso_rolo_padrao > 0 and v_diametro_rolo > 0 and v_largura_padrao > 0
			then
				v_diametro_rolo     := (v_diametro_rolo / v_peso_rolo_padrao) * :new.qtde_quilos_acab;
				v_raio              := v_diametro_rolo / 2 / 100;            
				:new.metros_cubicos := 3.14 * POWER(v_raio, 2) * v_largura_padrao;
			else
				:new.metros_cubicos := 0;
			end if;
		 end if;

         -- consiste qtde_rolos_real zerado, se for zero recebera 1
         if :new.qtde_rolos_real = 0
         then :new.qtde_rolos_real := 1;
         end if;
		 
		 
		 --------- ATUALIZA????O CAMPOS CONTROLE METROS --------- 
         begin

            --ATUALIZAR PESO_LIQUIDO_REAL 
            :new.peso_liquido_real := round(nvl(:new.peso_bruto, 0) - nvl(:new.tara, 0), 3); 
    
            --ATUALIZAR PESO_BRUTO_TEORICO 
            begin 
                select basi_030.unidade_medida 
                into v_unidade_medida 
                from basi_030 
                where basi_030.nivel_estrutura  = :new.panoacab_nivel99 
                and   basi_030.referencia       = :new.panoacab_grupo; 
            exception when others then 
                v_unidade_medida := 'KG'; 
            end; 
    
            if v_unidade_medida = 'KG' then 
    
    
                --Buscar peso padr?#o do rolo (basi_020 
                begin 
                    select basi_020.peso_rolo 
                    into v_peso_rolo_padrao 
                    from basi_020 
                    where basi_020.basi030_nivel030 = :new.panoacab_nivel99 
                    and   basi_020.basi030_referenc = :new.panoacab_grupo 
                    and   basi_020.tamanho_ref      = :new.panoacab_subgrupo; 
                exception when others then 
                    v_peso_rolo_padrao := 0; 
                end; 
    
                :new.peso_bruto_teorico := round(nvl(v_peso_rolo_padrao, 0) + nvl(:new.tara, 0), 3); 
    
    
            else   --N??O ?? KG 
    
                --Considera largura e gramatura do rolo, se n?#o tiver, busca largura e gramatura padr?#o da ficha t??cnica 
                v_largura   := :new.largura; 
                v_gramatura := :new.gramatura; 
                
                if  v_largura   is null or v_largura   = 0 or 
                    v_gramatura is null or v_gramatura = 0 then 
    
                    begin 
                        select basi_020.largura_1, basi_020.gramatura_1 
                        into   v_largura_padrao,   v_gramatura_padrao 
                        from basi_020 
                        where basi_020.basi030_nivel030 = :new.panoacab_nivel99 
                        and   basi_020.basi030_referenc = :new.panoacab_grupo 
                        and   basi_020.tamanho_ref      = :new.panoacab_subgrupo; 
                    exception when others then 
                        v_largura_padrao    := 0; 
                        v_gramatura_padrao  := 0; 
                    end; 
    
                    if v_largura is null or v_largura = 0 then 
                        v_largura := nvl(v_largura_padrao, 0); 
                    end if; 
                    
                    if v_gramatura is null or v_gramatura = 0 then 
                        v_gramatura := nvl(v_gramatura_padrao, 0); 
                    end if; 
    
                end if; --sem largura ou gramatura no rolo 
                
                
                if v_gramatura > 0.999 then 
                    v_multiplicador := 1000; 
                else 
                    v_multiplicador := 1; 
                end if; 
    
                --ATUALIZAR PESO BRUTO TE??RICO 
                :new.peso_bruto_teorico := round( ( (:new.qtde_quilos_acab * v_gramatura * v_largura) / v_multiplicador ) + nvl(:new.tara, 0), 3 ); 
    
            end if;    --Fim n?#o ?? KG 

        exception when others then
            :new.peso_liquido_real  := 0;
            :new.peso_bruto_teorico := 0;
        end;
         --------- ATUALIZA????O CAMPOS CONTROLE METROS --------- 
		 
      end if;	--Fim inserting or updating 
	  
	  

      if inserting or updating
      then
        -- se nao for informada a tabela origem, assume que a tabela e a propria PCPT_020
        if :new.tabela_origem_cardex is null or :new.tabela_origem_cardex = ' '
        then
           :new.tabela_origem_cardex := 'PCPT_020';
        end if;

         -- se o rolo estiver em estoque ou em alguma situacao ate a 5 - coletado nao pode ter peso menor ou igual a 0.00
        if :new.rolo_estoque >= 1 and :new.rolo_estoque <= 5
        and :new.qtde_quilos_acab <= 0.00
        then
           raise_application_error(-20000,'Quilos acabados nao podem ser 0.000 nem negativo, verifique!');
        end if;

         if inserting
         then
            begin
               if :new.rolo_estoque = 1
               then
                  :new.posicao_rolo := 1 ;
               end if;

               if :new.panoacab_nivel99 = '2'
               then

                  begin
                     select fatu_500.rpt_etiq_rolo
                     into v_rpt_etiq_rolo
                     from fatu_500
                     where fatu_500.codigo_empresa = ws_empresa;
                  exception
                  when others then
                     v_rpt_etiq_rolo := '';
                  end;

                  begin
                     select empr_008.val_str
                     into   v_lote_beneficiados
                     from   empr_008
                     where  empr_008.param = 'beneficiamento.controlaLote'
                     and    empr_008.codigo_empresa = ws_empresa;
                  exception
                  when others then
                     v_lote_beneficiados := 'S';
                  end;

                  if v_lote_beneficiados = 'N'
                  then
                    :new.lote_acomp := '0';
                  end if;

                  if v_rpt_etiq_rolo = 'pcpt_rol162'
                  then

                     v_encontrou_simbolos := 1;
                     begin
                        select basi_400.codigo_informacao
                        into v_simbolo_cons
                        from basi_400
                        where basi_400.tipo_informacao = 50
                          and basi_400.nivel           = :new.panoacab_nivel99
                          and basi_400.grupo           = :new.panoacab_grupo
                          and basi_400.subgrupo        = :new.panoacab_subgrupo
                          and basi_400.item            = :new.panoacab_item
                          and rownum                   = 1;
                     exception
                     when no_data_found then
                        begin
                           select basi_400.codigo_informacao
                           into v_simbolo_cons
                           from basi_400
                           where basi_400.tipo_informacao = 50
                             and basi_400.nivel           = :new.panoacab_nivel99
                             and basi_400.grupo           = :new.panoacab_grupo
                             and basi_400.subgrupo        = :new.panoacab_subgrupo
                             --and basi_400.item            = '000000'
                             and rownum                   = 1;
                        exception
                        when no_data_found then
                           begin
                              select basi_400.codigo_informacao
                              into v_simbolo_cons
                              from basi_400
                              where basi_400.tipo_informacao = 50
                                and basi_400.nivel           = :new.panoacab_nivel99
                                and basi_400.grupo           = :new.panoacab_grupo
                                --and basi_400.subgrupo        = '000'
                                --and basi_400.item            = '000000'
                                and rownum                   = 1;
                           exception
                           when no_data_found then
                              v_encontrou_simbolos := 0;
                           end;
                        end;
                     end;

                     if v_encontrou_simbolos = 0
                     then
                        raise_application_error(-20000,inter_fn_buscar_tag('ds31562#ATENCAAO! Nao existem si??mbolos de conservai??i??o cadastrados para este produto, ni??o seri?? possi??vel pesar o rolo ati?? que os mesmos sejam corretamente cadastrados.',ws_locale_usuario, ws_usuario_systextil));
                     end if;
                  end if;
               end if;


               --SS 60598
               if :new.rolo_estoque = 1 and :new.panoacab_nivel99 = '4'
               then

                  begin

                     --Se for da primeira lista de parametros especificos do primeiro ciclo (nivel 4)
                     select 1
                     into v_retorno_select
                     from estq_461
                     where estq_461.cod_deposito = :new.codigo_deposito;
                     exception
                        when others then
                           v_encontrou_reg := 1;

                  end;

                  if v_encontrou_reg = 0
                  then :new.pos_rolo_processo := 1;
                  end if;

               end if;

               --SS 60598
               v_retorno_select := 0;
               if (:new.rolo_estoque = 1 or :new.rolo_estoque = 3 or :new.rolo_estoque = 4) and
                  :new.panoacab_nivel99 = '2' and :new.ordem_producao > 0
               then

                  begin

                     --Verificar se o tipo de volume do deposito e 2
                     select 1
                     into v_retorno_select
                     from basi_205
                     where basi_205.codigo_deposito = :new.codigo_deposito
                       and basi_205.tipo_volume     = 2;
                     exception
                        when others then
                           v_encontrou_reg := 1;

                  end;

                  if v_encontrou_reg = 0
                  then

                     begin

                        --Verificar se o tipo da ordem e 0 (Ordem Normal)
                        select 1
                        into v_retorno_select
                        from pcpb_010
                        where pcpb_010.ordem_producao = :new.ordem_producao
                          and pcpb_010.tipo_ordem     = 0;
                        exception
                           when others then
                              v_encontrou_reg := 1;

                     end;

                     if v_encontrou_reg = 0
                     then

                        begin

                           --Se for da segunda lista de parametros especificos
                           select 1
                           into v_retorno_select
                           from estq_462
                           where estq_462.cod_deposito = :new.codigo_deposito;
                           exception
                              when others then
                                 v_encontrou_reg := 1;

                        end;

                        if v_encontrou_reg = 0
                        then :new.pos_rolo_processo := 13;
                        else

                           begin

                              v_encontrou_reg := 0;

                              --Se for da terceira lista de parametros especificos
                              select 1
                              into v_retorno_select
                              from estq_463
                              where estq_463.cod_deposito = :new.codigo_deposito;
                              exception
                                 when others then
                                    v_encontrou_reg := 1;

                           end;

                           if v_encontrou_reg = 0
                           then :new.pos_rolo_processo := 24;
                           end if;

                        end if;

                     end if;

                  end if;

               end if;

            end;
         end if;

         if updating
         then
            begin
               if :new.rolo_estoque = 1 and :old.posicao_rolo = 0
               then
                  :new.posicao_rolo := 1 ;
               end if;

               --SS 60598
               v_encontrou_reg := 0;
               if :new.rolo_estoque = 1 and :new.panoacab_nivel99 = '4' and (:old.pos_rolo_processo is null or :old.pos_rolo_processo = 0)
               then

                  begin

                     --Se for da primeira lista de parametros especificos do primeiro ciclo (nivel 4)
                     select 1
                     into v_retorno_select
                     from estq_461
                     where estq_461.cod_deposito = :new.codigo_deposito;
                     exception
                        when others then
                           v_encontrou_reg := 1;

                  end;

                  if v_encontrou_reg = 0
                  then :new.pos_rolo_processo := 1;
                  end if;

               end if;

               --SS 60598
               v_encontrou_reg := 0;
               if :old.rolo_estoque = 2 and :old.nota_fiscal > 0 and :new.panoacab_nivel99 = '4' and (:old.pos_rolo_processo is null or :old.pos_rolo_processo = 0) and --ji?? estava faturado (tinha nota)
                  :new.codigo_deposito <> :old.codigo_deposito --trocou o deposito (deu entrada da nota)
               then

                  begin

                     --O novo deposito eh da lista do ciclo 1
                     select 1
                     into v_retorno_select
                     from estq_461
                     where estq_461.cod_deposito = :new.codigo_deposito;
                     exception
                        when others then
                           v_encontrou_reg := 1;

                  end;

                  if v_encontrou_reg = 0
                  then :new.pos_rolo_processo := 9;
                  end if;

               end if;

            end;
         end if;

      end if;

      if (:new.tabela_origem_cardex not in ('FATU_060', 'OBRF_015') or deleting)
      then

         if inserting or updating
         then
            -- Le parametro que indica se a verificacao do lote deve ser ignorada
            select preromane_lote_dif_pedido into v_permi_lotes_diferentes
            from empr_002
            where rownum = 1;
            -- verifica se esta associando ou transferindo o rolo a um pedido com lotes do rolo
            -- diferente do lote do item do pedido.
            if inserting
            then
               if :new.pedido_venda > 0 and :new.seq_item_pedido > 0
               then
                  begin
                     select lote_empenhado into v_lote_pedi_110
                     from pedi_110
                     where pedi_110.pedido_venda    = :new.pedido_venda
                       and pedi_110.seq_item_pedido = :new.seq_item_pedido;

                     exception
                        when no_data_found then
                           raise_application_error(-20000,'Pedido ou sequencia invalido para relacionamento do rolo.');
                  end;

                  if v_permi_lotes_diferentes <> 'S' and v_lote_pedi_110 <> :new.lote_acomp
                  then
                     raise_application_error(-20000,'O rolo: '||to_char(:new.codigo_rolo, '000000000')||
                                                    ' esta no lote: '||to_char(:new.lote_acomp, '000000')||
                                                    ' e a sequencia do item esta com o lote: '||to_char(v_lote_pedi_110,'000000'));
                  end if;
               end if;
            else
               if :new.pedido_venda > 0 and :new.seq_item_pedido > 0 and
                 (:new.pedido_venda <> :old.pedido_venda or :new.seq_item_pedido <> :old.seq_item_pedido)
               then
                  begin
                     select lote_empenhado into v_lote_pedi_110
                     from pedi_110
                     where pedi_110.pedido_venda    = :new.pedido_venda
                       and pedi_110.seq_item_pedido = :new.seq_item_pedido;

                     exception
                        when no_data_found then
                           raise_application_error(-20000,'Pedido ou sequencia invalido para relacionamento do rolo.');
                  end;

                  if v_permi_lotes_diferentes <> 'S' and v_lote_pedi_110 <> :new.lote_acomp
                  then
                     raise_application_error(-20000,'O rolo: '||to_char(:new.codigo_rolo, '000000000')||
                                                    ' esta no lote: '||to_char(:new.lote_acomp, '000000')||
                                                    ' e a sequencia do item esta com o lote: '||to_char(v_lote_pedi_110,'000000'));
                  end if;
               end if;
            end if;

            -- verifica se e necessario fazer alguma conferencia
            if  (updating
            and (:old.rolo_estoque not in (0,2) and :new.rolo_estoque     in (0,2)) -- rolo estava em estoque e nao esta mais
            or  (:old.rolo_estoque     in (0,2) and :new.rolo_estoque not in (0,2)) -- rolo nao estava em estoque e agora esta
            or ((:old.codigo_deposito  <> :new.codigo_deposito                      -- deposito alterado
            or   :old.qtde_quilos_acab <> :new.qtde_quilos_acab)                    -- peso do rolo alterado
            and  :old.rolo_estoque not in (0,2)))                                   -- rolo estava em estoque

            or (inserting and :new.rolo_estoque <> 0)
            then

               if :new.transacao_cardex = 0 and (:new.rolo_estoque <> 0 or updating)
               then
                  raise_application_error(-20000,'Deve-se informar a transac?o de movimentac?o.');
               else

                  begin
                     select entrada_saida, transac_entrada into v_entrada_saida, v_transac_entrada
                     from estq_005
                     where codigo_transacao = :new.transacao_cardex;

                     exception
                        when no_data_found then
                           raise_application_error(-20000,'Transac?o informada n?o cadastrada.');
                  end;

                  if updating                                       -- Alterando o rolo
                  and :old.codigo_deposito  <> :new.codigo_deposito -- Se foi alterado o deposito
                  and :old.rolo_estoque not in (0,2)                -- Rolo nao estava em estoque
                  and :new.rolo_estoque not in (0,2)                -- Rolo nao esta em estoque
                  and  v_entrada_saida      <> 'T'                  -- Consiste transacao transferencia
                  then
                     raise_application_error(-20000,'Deposito alterado, deve-se usar uma transac?o de transferencia.');
                  end if;
               end if;

               if :new.data_cardex is null and (:new.rolo_estoque <> 0 or updating)
               then
                  raise_application_error(-20000,'Deve-se informar a data de movimentac?o.');
               end if;
            end if;
         end if;

         if inserting and :new.rolo_estoque <> 0
         then
            -- le se o deposito controla por volume
            select tipo_volume   , confirma_coleta
            into   v_tipo_volume , v_confirma_coleta
            from basi_205
            where codigo_deposito = :new.codigo_deposito;

            if :new.rolo_estoque = 1 and v_confirma_coleta = 1
            then
               -- se o deposito exige confirmacao de coleta e trata-se de um novo rolo (pesagem),
               -- ele eh inserido como em transito
               if :new.nr_rolo_origem = 0
               then :new.rolo_estoque := 4;
               else
                  -- se eh um rolo cortado ou beneficiado, e o rolo origem
                  -- for cru, entao insere como em transito
                  begin
                     select pcpt_020.panoacab_nivel99
                     into   v_nivel_origem
                     from pcpt_020
                     where pcpt_020.codigo_rolo = :new.nr_rolo_origem;
                     exception when no_data_found then
                        v_nivel_origem := '4';
                  end;

                  if v_nivel_origem = '4'
                  then
                     :new.rolo_estoque := 4;
                  end if;
               end if;
            end if;

            -- rolo_estoque.: 0 - Em producao / 2 - Faturado
            -- v_tipo_volume: 2 ou 4 - Controle por volume(rolo).
            if :new.rolo_estoque not in (0,2) or v_tipo_volume not in (2,4)
            then
               begin
                  -- Carrega parametro para controle do peso na entrada do estoque.
                  select empr_001.controle_benefic
                  into v_controle_peso
                  from empr_001;
               end;

               if v_entrada_saida <> 'E'
               then
                  raise_application_error(-20000,'Na insercao de um rolo novo, a transacao deve ser de entrada. Transai??i??o ' || to_char(:new.transacao_cardex,'000'));
               end if;

               -- se o deposito for por quilo, entao o rolo entra como faturado
               if v_tipo_volume not in (2,4)
               then
                  :new.rolo_estoque := 2;
               end if;

               -- Se a situacao do rolo for faturado em uma inclusao, entao significa que
               -- devera dar entrada no estoque com o peso padrao do rolo da ficha tecnica
               -- conforme parametrizac:10o da empresa ESTOQUE NA PREPARACAO (BENEFICIAMENTO)
               -- v_controle_peso: 0 - PELO NUMERO DO ROLO/CAIXA.
               --                  1 - PELA ESTRUTURA DO PRODUTO - PESO INFORMADO.
               --                  2 - PELA ESTRUTURA DO PRODUTO - PESO PADRAO DA FICHA TECNICA.
               if :new.rolo_estoque = 2 and v_controle_peso = 2
               then
                  select peso_rolo
                  into v_peso_ficha
                  from basi_020
                  where basi030_nivel030 = :new.panoacab_nivel99
                    and basi030_referenc = :new.panoacab_grupo
                    and tamanho_ref      = :new.panoacab_subgrupo;

                  if v_peso_ficha <= 0.00
                  then
                     raise_application_error(-20000,'Valor invalido para o peso do rolo na ficha tecnica.');
                  end if;
               else
                  v_peso_ficha := :new.qtde_quilos_acab;
               end if;

               -- verifica qual o documento e que deve ser enviado para a estq_300,
               -- se e a nota fiscal ou se e o numero do rolo
               if   :new.nota_fiscal_ent is not null and :new.nota_fiscal_ent  > 0
               and (:new.seri_fiscal_ent is not null and :new.seri_fiscal_ent <> ' '
               or   :new.seri_fiscal_ent is not null and :new.seri_fiscal_ent <> ' ')
               and  :new.sequ_fiscal_ent is not null and :new.sequ_fiscal_ent  > 0
               then
                  v_numero_documento    := :new.nota_fiscal_ent;
                  v_serie_documento     := :new.seri_fiscal_ent;
                  v_sequencia_documento := :new.sequ_fiscal_ent;
                  /*
                   trigger estava buscando o cliente do rolo para passar para o
                   cnpj da estq_300, mas neste caso esta sendo lido a nota de
                   entrada na pcpt_020, entao ele deve buscar o fornecedor do
                   rolo e nao o cliente. conforme visto com Claudio e Zequiel
                  */
                  v_cnpj_9              := :new.fornecedor_cgc9;
                  v_cnpj_4              := :new.fornecedor_cgc4;
                  v_cnpj_2              := :new.fornecedor_cgc2;
               else
                  v_numero_documento    := :new.codigo_rolo;
                  v_serie_documento     := ' ';
                  v_sequencia_documento := 0;
                  v_cnpj_9              := 0;
                  v_cnpj_4              := 0;
                  v_cnpj_2              := 0;
               end if;

               /*Busca a ordem de producaoo para enviar ao sped o rolo que foi consumido para a ordem.*/
               if v_nome_programa = 'pcpt_f200'
               then
                  v_numero_documento := :new.codigo_rolo;
				  
                  begin
                     select pcpt_025.ordem_producao
                     into   v_numero_op_sped
                     from pcpt_025
                     where pcpt_025.area_ordem  = 1
                     and   pcpt_025.codigo_rolo = :new.codigo_rolo;
                  exception when no_data_found then
                     v_numero_op_sped := 0;
                     null;
                  end;				  
               end if;
               
               if v_nome_programa = 'inte_p790'
               then
                  v_numero_documento := :new.codigo_rolo;
                  
                  begin
                     select pcpt_025.ordem_producao
                     into   v_numero_op_sped
                     from pcpt_025
                     where pcpt_025.area_ordem  = 1
                     and   pcpt_025.codigo_rolo = :new.codigo_rolo;
                  exception when no_data_found then
                     v_numero_op_sped := 0;
                     null;
                  end; 
				  
                  begin
                     select pcpt_021.reposicao
                     into   v_reposicao_sped
                     from pcpt_021
                     where pcpt_021.codigo_rolo = :new.codigo_rolo;
                  exception when others then
                     v_reposicao_sped := 0;
                     null;
                  end;
               end if;
               
               if v_nome_programa = 'pcpc_f215' 
               then
                  begin
                     select pcpt_021.reposicao
                     into   v_reposicao_sped
                     from pcpt_021
                     where pcpt_021.codigo_rolo = :new.codigo_rolo;
                  exception when others then
                     v_reposicao_sped := 0;
                     null;
                  end;
                  
                  v_numero_documento := :new.codigo_rolo;
                  
                  begin
                     select pcpt_025.ordem_producao
                     into   v_numero_op_sped
                     from pcpt_025
                     where pcpt_025.area_ordem  = 1
                     and   pcpt_025.codigo_rolo = :new.codigo_rolo;
                  exception when no_data_found then
                     v_numero_op_sped := 0;
                     null;
                  end; 
                  
                  begin
                     select empr_001.estagio_corte 
                     into v_estagio_op_sped
                     from empr_001;
                  exception when no_data_found then
                     v_estagio_op_sped := 0;
                     null;
                  end; 
               end if;
         
               if v_nome_programa in ('pcpb_f662','pcpb_f663','inte_p660','inte_p661','inte_p019', 'inte_p670')
               then
                  v_tipo_ordem_sped  := 2;
                  v_numero_documento := :new.codigo_rolo;

                  begin
                      select pcpt_021.numero_op
                      into   v_numero_op_sped
                      from pcpt_021
                      where pcpt_021.codigo_rolo = :new.codigo_rolo;
                  exception when others then
                      null;
                  end;
                  
                  begin
                     select pcpb_015.cod_estagio_agrupador,    pcpb_015.seq_operacao_agrupador,
                            pcpb_015.codigo_estagio
                     into   v_cod_estagio_agrupador,           v_seq_operacao_agrupador,
                            v_estagio_op_sped
                     from  pcpb_015
                     where pcpb_015.ordem_producao = v_numero_op_sped
                     and rownum                    = 1
                     and   pcpb_015.codigo_estagio in (select prep.*
                                                       from ( select estagio_prepara from empr_001
                                                              union all
                                                              SELECT empr_002.estagio_prepara_estamparia from empr_002
                                                              union all
                                                              SELECT empr_002.estagio_prepara_fios from empr_002) prep
                                                       );
                  exception when others then
                     null;
                  end;
               end if;
         
               if v_nome_programa = 'pcpb_f420' then
                  v_tipo_ordem_sped := 2;
                  
                  v_numero_op_sped := :new.codigo_rolo;
               end if;
               
               if v_nome_programa in ('inte_pete','pcpb_f184','pcpb_f082','pcpb_f083')
               then
                   v_numero_op_sped := :new.ordem_producao;
                  
                   v_tipo_ordem_sped := 2;
               end if;
                
               if upper(ws_aplicativo) = 'STADOSVR.EXE'
               then
                   v_numero_op_sped := :new.ordem_producao;
                  
                   v_tipo_ordem_sped := 2;
                  
                   declare
                     nroRegAberto   number;
                   begin
                     select nvl(count(*),0)
                     into   nroRegAberto
                     from  pcpb_015
                     where pcpb_015.ordem_producao  = v_numero_op_sped
                     and   pcpb_015.data_termino    is null;
                     
                     if (nroRegAberto = 0707070)
                     then
                     	raise_application_error(-20000,'ATEN��O! Esta ordem de produ��o j� foi encerrada, verifique os apontamentos produtivos - Acompanhamento da Produ��o do Beneficiamento - pcpb_f674.'
                     	   || ' Ordem Produ��o: ' || v_numero_op_sped);
                     end if;
                     
                     if (nroRegAberto > 1) 
                     then
                     	raise_application_error(-20000,'ATEN��O! Esta ordem de produ��o n�o est� no �ltimo est�gio, verifique os apontamentos produtivos - Acompanhamento da Produ��o do Beneficiamento - pcpb_f674.'
                     	   || ' Ordem Produ��o: ' || v_numero_op_sped);
                     end if;
                   end;
               end if;
               
               if v_nome_programa = 'pcpt_f085' then
                   v_numero_op_sped := :new.ordem_producao;
                   
                   select ESTAGIO_TECELAGEM 
                   into   v_estagio_op_sped
                   from empr_001;
                   
                   v_tipo_ordem_sped := 4;
               end if;

              if v_nome_programa = 'lune_fa25' then
                v_numero_documento := :new.codigo_rolo;
              end if;
         
               -- atualiza a data de insercao com a data corrente (sysdate)
               :new.data_insercao := trunc(sysdate,'dd');

               inter_pr_insere_estq_300_recur (:new.codigo_deposito,   :new.panoacab_nivel99,   :new.panoacab_grupo,
                                               :new.panoacab_subgrupo, :new.panoacab_item,      :new.data_cardex,
                                               :new.lote_acomp,        v_numero_documento,      v_serie_documento,
                                               v_cnpj_9,               v_cnpj_4,                v_cnpj_2,
                                               v_sequencia_documento,
                                               :new.transacao_cardex,  'E',                     v_ccusto_cardex,
                                               v_peso_ficha,           :new.valor_movto_cardex, :new.valor_contabil_cardex,
                                               :new.usuario_cardex,    v_tabela_origem_cardex,  :new.nome_prog_020,
                                               :new.peso_bruto,        v_numero_op_sped,        0,
                                               0,                      v_tipo_ordem_sped,       v_estagio_op_sped,
                                               0,                      0,                       v_reposicao_sped);
            end if;
         end if;     -- if inserting

         if deleting
         then
             -- le se o deposito controla por volume
           select tipo_volume
           into   v_tipo_volume
           from basi_205
           where codigo_deposito = :old.codigo_deposito;

          if :old.rolo_estoque not in (0,2) or (v_tipo_volume = 0 and :old.rolo_estoque = 2)
            then
               -- encontra a transacao de cancelamento de producao
               select estq_005.trans_cancelamento into v_transacao_cancel from estq_005
               where estq_005.codigo_transacao = :old.transacao_ent;

               if v_transacao_cancel = 0
               then
                  select tran_est_produca into v_transacao_cancel from empr_001;
               end if;

               begin
                  select periodo_estoque
                  into   v_periodo_estoque
                  from empr_001;
               end;

               if :old.data_entrada < v_periodo_estoque
               then
                   raise_application_error(-20000,'Rolos com data de entrada menor que a data do peri??odo de estoque. Este rolo deve ser cancelado, ni??o pode ser exclui??do. Rolo: ' ||
                   :old.codigo_rolo ||  ' - Data entrada rolo: '  ||  :old.data_entrada || ' - Data peri??odo estoque: ' || v_periodo_estoque);
               end if;


               /*Busca a ordem de producaoo para enviar ao sped o rolo que foi consumido para a ordem.*/
                              if v_nome_programa = 'pcpt_f200'
               then
                  v_numero_documento := :old.codigo_rolo;
				  
                  begin
                     select pcpt_025.ordem_producao
                     into   v_numero_op_sped
                     from pcpt_025
                     where pcpt_025.area_ordem  = 1
                     and   pcpt_025.codigo_rolo = :old.codigo_rolo;
                  exception when no_data_found then
                     v_numero_op_sped := 0;
                     null;
                  end;
               end if;
               
               if v_nome_programa = 'inte_p790'
               then
                  v_numero_documento := :old.codigo_rolo;
                  
                  begin
                     select pcpt_025.ordem_producao
                     into   v_numero_op_sped
                     from pcpt_025
                     where pcpt_025.area_ordem  = 1
                     and   pcpt_025.codigo_rolo = :old.codigo_rolo;
                  exception when no_data_found then
                     v_numero_op_sped := 0;
                     null;
                  end;
                  
                  begin
                     select pcpt_021.reposicao
                     into   v_reposicao_sped
                     from pcpt_021
                     where pcpt_021.codigo_rolo = :old.codigo_rolo;
                  exception when others then
                     v_reposicao_sped := 0;
                     null;
                  end;          
               end if;
               
               if v_nome_programa = 'pcpc_f215' 
               then
                  begin
                     select pcpt_021.reposicao
                     into   v_reposicao_sped
                     from pcpt_021
                     where pcpt_021.codigo_rolo = :old.codigo_rolo;
                  exception when others then
                     v_reposicao_sped := 0;
                     null;
                  end;
                  
                  v_numero_documento := :old.codigo_rolo;
                  
                  begin
                     select pcpt_025.ordem_producao
                     into   v_numero_op_sped
                     from pcpt_025
                     where pcpt_025.area_ordem  = 1
                     and   pcpt_025.codigo_rolo = :old.codigo_rolo;
                  exception when no_data_found then
                     v_numero_op_sped := 0;
                     null;
                  end; 
                  
                  begin
                     select empr_001.estagio_corte 
                     into v_estagio_op_sped
                     from empr_001;
                  exception when no_data_found then
                     v_estagio_op_sped := 0;
                     null;
                  end; 
               end if;
         
               if v_nome_programa in ('pcpb_f662','pcpb_f663','inte_p660','inte_p661','inte_p019', 'inte_p670')
               then
                  v_tipo_ordem_sped  := 2;
                  v_numero_documento := :old.codigo_rolo;

                  begin
                      select pcpt_021.numero_op
                      into   v_numero_op_sped
                      from pcpt_021
                      where pcpt_021.codigo_rolo = :old.codigo_rolo;
                  exception when others then
                      null;
                  end;
                  
                  begin
                     select pcpb_015.cod_estagio_agrupador,    pcpb_015.seq_operacao_agrupador,
                            pcpb_015.codigo_estagio
                     into   v_cod_estagio_agrupador,           v_seq_operacao_agrupador,
                            v_estagio_op_sped
                     from  pcpb_015
                     where pcpb_015.ordem_producao = v_numero_op_sped
                     and rownum                    = 1
                     and   pcpb_015.codigo_estagio in (select prep.*
                                                       from ( select estagio_prepara from empr_001
                                                              union all
                                                              SELECT empr_002.estagio_prepara_estamparia from empr_002
                                                              union all
                                                              SELECT empr_002.estagio_prepara_fios from empr_002) prep
                                                       );
                  exception when others then
                     null;
                  end;
               end if;
               
               if v_nome_programa = 'pcpb_f420' then
                  v_tipo_ordem_sped := 2;
                  
                  v_numero_op_sped := :old.codigo_rolo;
               end if;
               
               if v_nome_programa in ('inte_pete','pcpb_f184','pcpb_f082','pcpb_f083')
               then
                   v_numero_op_sped := :old.ordem_producao;
                   v_tipo_ordem_sped := 2;
               end if;
                
               if upper(ws_aplicativo) = 'STADOSVR.EXE' 
               then
                   v_numero_op_sped := :old.ordem_producao;
                   v_tipo_ordem_sped := 2;
                  
                   declare
                     nroRegAberto   number;
                   begin
                     select nvl(count(*),0)
                     into   nroRegAberto
                     from  pcpb_015
                     where pcpb_015.ordem_producao  = v_numero_op_sped
                     and   pcpb_015.data_termino    is null;
                     
                     if (nroRegAberto = 0707070)
                     then
                     	raise_application_error(-20000,'ATEN��O! Esta ordem de produ��o j� foi encerrada, verifique os apontamentos produtivos - Acompanhamento da Produ��o do Beneficiamento - pcpb_f674.'
                     	   || ' Ordem Produ��o: ' || v_numero_op_sped);
                     end if;
                     
                     if (nroRegAberto > 1) 
                     then
                     	raise_application_error(-20000,'ATEN��O! Esta ordem de produ��o n�o est� no �ltimo est�gio, verifique os apontamentos produtivos - Acompanhamento da Produ��o do Beneficiamento - pcpb_f674.'
                     	   || ' Ordem Produ��o: ' || v_numero_op_sped);
                     end if;
                   end;
               end if;
         
               if v_nome_programa = 'pcpt_f085' then
                   v_numero_op_sped := :old.ordem_producao;
                   
           select ESTAGIO_TECELAGEM 
                   into   v_estagio_op_sped
                   from empr_001;
           
                   v_tipo_ordem_sped := 4;
               end if;
         
               -- encontra a data de lancamento do movimento no estq_300,
               -- se a data do cancelamento (sysdate) for igual a data de inserc:16o
               -- do rolo, ent:17o a data de cancelamento sera a data de entrada
               -- sen:18o sera a data de cancelamento (sysdate)
               if :old.data_entrada is null
               then
                  v_data_cancelamento := trunc(sysdate,'dd');
               else
                  if v_nome_programa = 'pcpt_f085' then
                     v_data_cancelamento := :old.data_prod_tecel;
                  else
                     v_data_cancelamento := :old.data_entrada;
                  end if;
               end if;
			   
               if  controleProcessoBlocoK = 1
               and data_fechamento_blocok is not null
               and v_data_cancelamento    is not null
               and v_data_cancelamento    <= data_fechamento_blocok then 
                     raise_application_error(-20000,'Rolo com data de cancelamento menor que a data do blocok. ' 
                                                 || ' Rolo: ' || :old.codigo_rolo 
                                                 || ' Data cancelamento: '  ||  v_data_cancelamento 
                                                 || ' Data blocok: ' || data_fechamento_blocok);
               end if;
               
               inter_pr_insere_estq_300_recur (:old.codigo_deposito,   :old.panoacab_nivel99,   :old.panoacab_grupo,
                                               :old.panoacab_subgrupo, :old.panoacab_item,      v_data_cancelamento,
                                               :old.lote_acomp,        :old.codigo_rolo,        '',
                                               0,                      0,                       0,
                                               0,
                                               v_transacao_cancel,     'S',                     v_ccusto_cardex,
                                               :old.qtde_quilos_acab,  :old.valor_movto_cardex, :old.valor_contabil_cardex,
                                               ws_usuario_systextil,   v_tabela_origem_cardex,  ws_aplicativo,  -- DELECAO
                                               :old.peso_bruto,        v_numero_op_sped,        0,
                                               0,                      v_tipo_ordem_sped,       v_estagio_op_sped,
                             0,                      0,                       v_reposicao_sped);
            end if;
         end if;     -- if deleting


         if updating
         then

            -- se o rolo ja estava em estoque, entao nao deixa alterar a
            -- transacao e a data de entrada
            if  :old.rolo_estoque <> 0
            and :new.rolo_estoque <> 0
            then
               :new.transacao_ent := :old.transacao_ent;
               :new.data_entrada  := :old.data_entrada;
            end if;

            if :new.qtde_quilos_acab > :old.qtde_quilos_acab and :old.rolo_estoque <> 0 and lower(:new.nome_prog_020) <> 'estq_f202'
            then
               raise_application_error(-20000,'N?o se pode aumentar o peso dos rolos.');
            end if;

            -- movimento de saida
            if   :old.rolo_estoque not in (0,2)                  -- rolo estava em estoque
            and (:old.codigo_deposito  <> :new.codigo_deposito   -- deposito alterado
            or   :old.qtde_quilos_acab <> :new.qtde_quilos_acab  -- peso do rolo alterado
            or   :new.rolo_estoque     in (0,2))                 -- rolo tirado do estoque
            then

               if v_entrada_saida <> 'S' and v_entrada_saida <> 'T' and :old.qtde_quilos_acab > :new.qtde_quilos_acab
               then
                  raise_application_error(-20000,'N?o se pode realizar uma movimentac?o de saida com uma transacao de entrada.');
               end if;

               if  :old.codigo_deposito   = :new.codigo_deposito -- mesmo deposito
               and :new.rolo_estoque not in (0,2)                -- rolo continua no estoque
               then
                  v_peso_rolo := :old.qtde_quilos_acab - :new.qtde_quilos_acab;
                  v_qtde_kilo := :old.peso_bruto       - :new.peso_bruto;
               else
                  v_peso_rolo := :old.qtde_quilos_acab;
                  v_qtde_kilo := :old.peso_bruto;
               end if;

               select confirma_coleta
               into   v_confirma_coleta
               from basi_205
               where codigo_deposito = :new.codigo_deposito;

               if  :new.nota_fiscal      is not null and :new.nota_fiscal       > 0
               and :new.serie_fiscal_sai is not null and :new.serie_fiscal_sai <> ' '
               and :new.seq_nota_fiscal  is not null and :new.seq_nota_fiscal   > 0
               and :new.nome_prog_020    not in ('pcpt_f200','pcpt_f215','pcpc_f215')
               then
                  v_numero_documento    := :new.nota_fiscal;
                  v_serie_documento     := :new.serie_fiscal_sai;
                  v_sequencia_documento := :new.seq_nota_fiscal;
                  v_cnpj_9              := 0;
                  v_cnpj_4              := 0;
                  v_cnpj_2              := 0;
               else
                  v_numero_documento    := :new.codigo_rolo;
                  v_serie_documento     := ' ';
                  v_sequencia_documento := 0;
                  v_cnpj_9              := 0;
                  v_cnpj_4              := 0;
                  v_cnpj_2              := 0;
               end if;

               /*Busca a ordem de producaoo para enviar ao sped o rolo que foi consumido para a ordem.*/
               if v_nome_programa = 'pcpt_f200'
               then
                  v_numero_documento := :new.codigo_rolo;
				  
                  begin
                     select pcpt_025.ordem_producao
                     into   v_numero_op_sped
                     from pcpt_025
                     where pcpt_025.area_ordem  = 1
                     and   pcpt_025.codigo_rolo = :new.codigo_rolo;
                  exception when no_data_found then
                     v_numero_op_sped := 0;
                     null;
                  end;				  
               end if;
               
               if v_nome_programa = 'inte_p790'
               then
                  v_numero_documento := :new.codigo_rolo;
                  
                  begin
                     select pcpt_025.ordem_producao
                     into   v_numero_op_sped
                     from pcpt_025
                     where pcpt_025.area_ordem  = 1
                     and   pcpt_025.codigo_rolo = :new.codigo_rolo;
                  exception when no_data_found then
                     v_numero_op_sped := 0;
                     null;
                  end;
                            
                  begin
                     select pcpt_021.reposicao
                     into   v_reposicao_sped
                     from pcpt_021
                     where pcpt_021.codigo_rolo = :new.codigo_rolo;
                  exception when others then
                     v_reposicao_sped := 0;
                     null;
                  end;          
               end if;
               
               if v_nome_programa = 'pcpc_f215' 
               then
                  begin
                     select pcpt_021.reposicao
                     into   v_reposicao_sped
                     from pcpt_021
                     where pcpt_021.codigo_rolo = :new.codigo_rolo;
                  exception when others then
                     v_reposicao_sped := 0;
                     null;
                  end;
                  
                  v_numero_documento := :new.codigo_rolo;
                  
                  begin
                     select pcpt_025.ordem_producao
                     into   v_numero_op_sped
                     from pcpt_025
                     where pcpt_025.area_ordem  = 1
                     and   pcpt_025.codigo_rolo = :new.codigo_rolo;
                  exception when no_data_found then
                     v_numero_op_sped := 0;
                     null;
                  end; 
                  
                  begin
                     select empr_001.estagio_corte 
                     into v_estagio_op_sped
                     from empr_001;
                  exception when no_data_found then
                     v_estagio_op_sped := 0;
                     null;
                  end; 
               end if;

               if v_nome_programa in ('pcpb_f662','pcpb_f663','inte_p660','inte_p661','inte_p019', 'inte_p670')
               then
                  v_tipo_ordem_sped  := 2;
                  v_numero_documento := :new.codigo_rolo;

                  begin
                      select pcpt_021.numero_op
                      into   v_numero_op_sped
                      from pcpt_021
                      where pcpt_021.codigo_rolo = :new.codigo_rolo;
                  exception when others then
                      null;
                  end;
                  
                  begin
                     select pcpb_015.cod_estagio_agrupador,    pcpb_015.seq_operacao_agrupador,
                            pcpb_015.codigo_estagio
                     into   v_cod_estagio_agrupador,           v_seq_operacao_agrupador,
                            v_estagio_op_sped
                     from  pcpb_015
                     where pcpb_015.ordem_producao = v_numero_op_sped
                     and rownum                    = 1
                     and   pcpb_015.codigo_estagio in (select prep.*
                                                       from ( select estagio_prepara from empr_001
                                                              union all
                                                              SELECT empr_002.estagio_prepara_estamparia from empr_002
                                                              union all
                                                              SELECT empr_002.estagio_prepara_fios from empr_002) prep
                                                       );
                  exception when others then
                     null;
                  end;
               end if;
               
               if v_nome_programa = 'pcpb_f420' then
                  v_tipo_ordem_sped := 2;
                  
                  v_numero_op_sped := :new.codigo_rolo;
               end if;
               
               if v_nome_programa in ('inte_pete','pcpb_f184','pcpb_f082','pcpb_f083')
               then
                  v_numero_op_sped := :new.ordem_producao;
                  v_tipo_ordem_sped := 2;
               end if;
                
               if upper(ws_aplicativo) = 'STADOSVR.EXE' 
               then
                  v_numero_op_sped := :new.ordem_producao;
                  v_tipo_ordem_sped := 2;
                  
                   declare
                     nroRegAberto   number;
                   begin
                     select nvl(count(*),0)
                     into   nroRegAberto
                     from  pcpb_015
                     where pcpb_015.ordem_producao  = v_numero_op_sped
                     and   pcpb_015.data_termino    is null;
                     
                     if (nroRegAberto = 0707070)
                     then
                     	raise_application_error(-20000,'ATEN��O! Esta ordem de produ��o j� foi encerrada, verifique os apontamentos produtivos - Acompanhamento da Produ��o do Beneficiamento - pcpb_f674.'
                     	   || ' Ordem Produ��o: ' || v_numero_op_sped);
                     end if;
                     
                     if (nroRegAberto > 1) 
                     then
                     	raise_application_error(-20000,'ATEN��O! Esta ordem de produ��o n�o est� no �ltimo est�gio, verifique os apontamentos produtivos - Acompanhamento da Produ��o do Beneficiamento - pcpb_f674.'
                     	   || ' Ordem Produ��o: ' || v_numero_op_sped);
                     end if;
                   end;
               end if;
               
               if v_nome_programa = 'pcpt_f085' then
                   v_numero_op_sped := :new.ordem_producao;
                   
                   select ESTAGIO_TECELAGEM 
                   into   v_estagio_op_sped
                   from empr_001;
                   
                   v_tipo_ordem_sped := 4;
               end if;

              if v_nome_programa = 'lune_fa25' then
                v_numero_documento := :new.codigo_rolo;
              end if;
              
               if v_peso_rolo > 0
               then
                  inter_pr_insere_estq_300_recur (:old.codigo_deposito,   :old.panoacab_nivel99,   :old.panoacab_grupo,
                                         :old.panoacab_subgrupo, :old.panoacab_item,      :new.data_cardex,
                                         :old.lote_acomp,        v_numero_documento,      v_serie_documento,
                                         v_cnpj_9,               v_cnpj_4,                v_cnpj_2,
                                         v_sequencia_documento,
                                         :new.transacao_cardex,  'S',                     v_ccusto_cardex,
                                         v_peso_rolo,            :new.valor_movto_cardex, :new.valor_contabil_cardex,
                                         :new.usuario_cardex,    v_tabela_origem_cardex,  :new.nome_prog_020,
                                         v_qtde_kilo,            v_numero_op_sped,        0,
                                         0,                      v_tipo_ordem_sped,       v_estagio_op_sped,
                                         v_cod_estagio_agrupador, v_seq_operacao_agrupador, v_reposicao_sped);
               else
                  inter_pr_insere_estq_300_recur (:old.codigo_deposito,   :old.panoacab_nivel99,   :old.panoacab_grupo,
                                         :old.panoacab_subgrupo, :old.panoacab_item,      :new.data_cardex,
                                         :old.lote_acomp,        v_numero_documento,      v_serie_documento,
                                         v_cnpj_9,               v_cnpj_4,                v_cnpj_2,
                                         v_sequencia_documento,
                                         :new.transacao_cardex,  'E',                     v_ccusto_cardex,
                                         abs(v_peso_rolo),       :new.valor_movto_cardex, :new.valor_contabil_cardex,
                                         :new.usuario_cardex,    v_tabela_origem_cardex,  :new.nome_prog_020,
                                         v_qtde_kilo,            v_numero_op_sped,     0,
                                         0,                      v_tipo_ordem_sped,      v_estagio_op_sped,
                                         v_cod_estagio_agrupador, v_seq_operacao_agrupador, v_reposicao_sped);
               end if;
            end if;


            -- movimento de entrada
            if   :new.rolo_estoque not in (0,2)                  -- rolo esta em estoque
            and (:old.codigo_deposito  <> :new.codigo_deposito   -- deposito alterado
            or   :old.rolo_estoque     in (0,2))                 -- rolo nao estava em estoque
            then

               if v_entrada_saida <> 'T'
               then
                  if v_entrada_saida <> 'E'
                  then
                     raise_application_error(-20000,'N?o se pode realizar uma movimentac?o de entrada com uma transacao de saida.');
                  end if;

                  v_transac_entrada := :new.transacao_cardex;
               end if;

               -- Rotina exclusiva para Conferencia de Embarque para Poltex
               if v_entrada_saida = 'E'
               then
                  select count(*) into v_tem_pcpt021
                  from pcpt_021
                  where pcpt_021.codigo_rolo = :new.codigo_rolo
                    and pcpt_021.usuario_conf is not null;

                  if v_tem_pcpt021 > 0
                  then
                     update pcpt_021
                     set   pcpt_021.usuario_conf = ''
                     where pcpt_021.codigo_rolo = :new.codigo_rolo;
                  end if;
               end if;

               if  :new.nota_fiscal_ent is not null and :new.nota_fiscal_ent  > 0
               and :new.seri_fiscal_ent is not null and :new.seri_fiscal_ent <> ' '
               and :new.sequ_fiscal_ent is not null and :new.sequ_fiscal_ent  > 0
               then
                  v_numero_documento    := :new.nota_fiscal_ent;
                  v_serie_documento     := :new.seri_fiscal_ent;
                  v_sequencia_documento := :new.sequ_fiscal_ent;
                  v_cnpj_9              := :new.cliente_cgc9;
                  v_cnpj_4              := :new.cliente_cgc4;
                  v_cnpj_2              := :new.cliente_cgc2;
               else
                  v_numero_documento    := :new.codigo_rolo;
                  v_serie_documento     := ' ';
                  v_sequencia_documento := 0;
                  v_cnpj_9              := 0;
                  v_cnpj_4              := 0;
                  v_cnpj_2              := 0;
               end if;


               /* Logica para executar a entrada do rolo no estoque conforme parametrizacao do tipo
                  de volume do deposito e parametro de empresa na preparacao */
               v_peso_ficha := :new.qtde_quilos_acab;

               -- le se o deposito controla por volume
               select tipo_volume
               into   v_tipo_volume
               from basi_205
               where codigo_deposito = :new.codigo_deposito;

               -- rolo_estoque.: 0 - Em producao / 1 - Estoque / 2 - Faturado
               -- v_tipo_volume: 2 ou 4 - Controle por volume(rolo).
               if :old.rolo_estoque = 0 and :new.rolo_estoque not in (0) and :new.panoacab_nivel99 = '4'
               then
                  begin
                     -- Carrega parametro para controle do peso na entrada do estoque.
                     select empr_001.controle_benefic
                     into v_controle_peso
                     from empr_001;
                  end;

                  -- se o deposito for por quilo, entao o rolo entra como faturado
                  if v_tipo_volume not in (2,4)
                  then
                     :new.rolo_estoque := 2;
                  end if;

                  -- Se a situacao do rolo for faturado em uma inclusao, entao significa que
                  -- devera dar entrada no estoque com o peso padrao do rolo da ficha tecnica
                  -- conforme parametrizacao da empresa ESTOQUE NA PREPARACAO (BENEFICIAMENTO)
                  -- v_controle_peso: 0 - PELO NUMERO DO ROLO/CAIXA.
                  --                  1 - PELA ESTRUTURA DO PRODUTO - PESO INFORMADO.
                  --                  2 - PELA ESTRUTURA DO PRODUTO - PESO PADRAO DA FICHA TECNICA.
                  if :new.rolo_estoque = 2 and v_controle_peso = 2
                  then
                     select peso_rolo
                     into v_peso_ficha
                     from basi_020
                     where basi030_nivel030 = :new.panoacab_nivel99
                       and basi030_referenc = :new.panoacab_grupo
                       and tamanho_ref      = :new.panoacab_subgrupo;

                     if v_peso_ficha <= 0.00
                        then
                        raise_application_error(-20000,'Valor invalido para o peso do rolo na ficha tecnica.');
                     end if;
                  else
                     v_peso_ficha := :new.qtde_quilos_acab;
                  end if;
               end if;

               /*Busca a ordem de producaoo para enviar ao sped o rolo que foi consumido para a ordem.*/
               if v_nome_programa = 'pcpt_f200'
               then
                  v_numero_documento := :new.codigo_rolo;
          
                  begin
                     select pcpt_025.ordem_producao
                     into   v_numero_op_sped
                     from pcpt_025
                     where pcpt_025.area_ordem  = 1
                     and   pcpt_025.codigo_rolo = :new.codigo_rolo;
                  exception when no_data_found then
                     v_numero_op_sped := 0;
                     null;
                  end; 
               end if;
               
               if v_nome_programa = 'inte_p790'
               then
                  v_numero_documento := :new.codigo_rolo;
                  
                  begin
                     select pcpt_025.ordem_producao
                     into   v_numero_op_sped
                     from pcpt_025
                     where pcpt_025.area_ordem  = 1
                     and   pcpt_025.codigo_rolo = :new.codigo_rolo;
                  exception when no_data_found then
                     v_numero_op_sped := 0;
                     null;
                  end; 
          
                  begin
                     select pcpt_021.reposicao
                     into   v_reposicao_sped
                     from pcpt_021
                     where pcpt_021.codigo_rolo = :new.codigo_rolo;
                  exception when others then
                     v_reposicao_sped := 0;
                     null;
                  end;          
               end if;
               
               if v_nome_programa = 'pcpc_f215' 
               then
                  begin
                     select pcpt_021.reposicao
                     into   v_reposicao_sped
                     from pcpt_021
                     where pcpt_021.codigo_rolo = :new.codigo_rolo;
                  exception when others then
                     v_reposicao_sped := 0;
                     null;
                  end;
                  
                  v_numero_documento := :new.codigo_rolo;
                  
                  begin
                     select pcpt_025.ordem_producao
                     into   v_numero_op_sped
                     from pcpt_025
                     where pcpt_025.area_ordem  = 1
                     and   pcpt_025.codigo_rolo = :new.codigo_rolo;
                  exception when no_data_found then
                     v_numero_op_sped := 0;
                     null;
                  end; 
                  
                  begin
                     select empr_001.estagio_corte 
                     into v_estagio_op_sped
                     from empr_001;
                  exception when no_data_found then
                     v_estagio_op_sped := 0;
                     null;
                  end; 
               end if;

               if v_nome_programa in ('pcpb_f662','pcpb_f663','inte_p660','inte_p661','inte_p019', 'inte_p670')
               then
                  v_tipo_ordem_sped  := 2;
                  v_numero_documento := :new.codigo_rolo;

                  begin
                      select pcpt_021.numero_op
                      into   v_numero_op_sped
                      from pcpt_021
                      where pcpt_021.codigo_rolo = :new.codigo_rolo;
                  exception when others then
                      null;
                  end;
                  
                  begin
                     select pcpb_015.cod_estagio_agrupador,    pcpb_015.seq_operacao_agrupador,
                            pcpb_015.codigo_estagio
                     into   v_cod_estagio_agrupador,           v_seq_operacao_agrupador,
                            v_estagio_op_sped
                     from  pcpb_015
                     where pcpb_015.ordem_producao = v_numero_op_sped
                     
                     and rownum = 1
                     
                     and   pcpb_015.codigo_estagio in (
                                   select prep.*
                                   from ( select estagio_prepara from empr_001
                                          union all
                                          select empr_002.estagio_prepara_estamparia from empr_002
                                          union all
                                          select empr_002.estagio_prepara_fios from empr_002) prep
                                        );
                  exception when others then
                     null;
                  end;
               end if;
               
               if v_nome_programa = 'pcpb_f420' then
                   v_tipo_ordem_sped := 2;
                   
                   v_numero_op_sped := :new.codigo_rolo;
               end if;
               
               if v_nome_programa in ('inte_pete','pcpb_f184','pcpb_f082','pcpb_f083') 
               then
                   v_numero_op_sped := :new.ordem_producao;
                   v_tipo_ordem_sped := 2;
               end if;
               
               if upper(ws_aplicativo) = 'STADOSVR.EXE'
               then 
                   v_numero_op_sped := :new.ordem_producao;
                   v_tipo_ordem_sped := 2;
                   
                   declare
                     nroRegAberto   number;
                   begin
                     select nvl(count(*),0)
                     into   nroRegAberto
                     from  pcpb_015
                     where pcpb_015.ordem_producao  = v_numero_op_sped
                     and   pcpb_015.data_termino    is null;
                     
                     if (nroRegAberto = 0707070)
                     then
                       raise_application_error(-20000,'ATEN��O! Esta ordem de produ��o j� foi encerrada, verifique os apontamentos produtivos - Acompanhamento da Produ��o do Beneficiamento - pcpb_f674.'
                          || ' Ordem Produ��o: ' || v_numero_op_sped);
                     end if;
                     
                     if (nroRegAberto > 1) 
                     then
                       raise_application_error(-20000,'ATEN��O! Esta ordem de produ��o n�o est� no �ltimo est�gio, verifique os apontamentos produtivos - Acompanhamento da Produ��o do Beneficiamento - pcpb_f674.'
                          || ' Ordem Produ��o: ' || v_numero_op_sped);
                     end if;
                   end;
               end if;
               
               if v_nome_programa = 'pcpt_f085' then
                   v_numero_op_sped := :new.ordem_producao;
                   
                   select ESTAGIO_TECELAGEM 
                   into   v_estagio_op_sped
                   from empr_001;
                   
                   v_tipo_ordem_sped := 4;
               end if;
               /* 247192 - 173973 - Corte de Rolo alocado na produ¿¿o - Bloqueio Bloco K */
               if   v_nome_programa in ('pcpc_f215', 'pcpt_fa21') 
               and  :old.rolo_estoque = 2 
               and  :new.rolo_estoque = 1
               then
                  v_numero_documento := :new.codigo_rolo;
                  select TRUNC(TO_DATE(pcpt_025.data_hora_conf))
                  into   v_data_movimento_rolo
                  from pcpt_025
                  where pcpt_025.area_ordem     = 1
                  and   pcpt_025.rolo_acabado   = :new.codigo_rolo
                  and   pcpt_025.ordem_producao = v_numero_op_sped
                  and   rolo_confirmado         = 'S';
               else 
                  v_data_movimento_rolo := :new.data_cardex; 
               end if;  

               if  controleProcessoBlocoK = 1
                  and data_fechamento_blocok is not null
                  and v_data_movimento_rolo  is not null
                  and v_data_movimento_rolo  <= data_fechamento_blocok then
                        raise_application_error(-20000,'Rolo com data de movimento menor que a data do blocok. '
                                                   || ' Rolo: ' || :old.codigo_rolo
                                                   || ' Data movimento: '  ||  v_data_movimento_rolo
                                                   || ' Data blocok: ' || data_fechamento_blocok);
               end if;
               /* fim 247192 - 173973 - Corte de Rolo alocado na produ¿¿o - Bloqueio Bloco K */

               inter_pr_insere_estq_300_recur (:new.codigo_deposito,   :new.panoacab_nivel99,   :new.panoacab_grupo,
                                             :new.panoacab_subgrupo, :new.panoacab_item,      v_data_movimento_rolo,
                                             :new.lote_acomp,        v_numero_documento,      v_serie_documento,
                                             v_cnpj_9,               v_cnpj_4,                v_cnpj_2,
                                             v_sequencia_documento,
                                             v_transac_entrada,      'E',                     v_ccusto_cardex,
                                             v_peso_ficha,           :new.valor_movto_cardex, :new.valor_contabil_cardex,
                                             :new.usuario_cardex,    v_tabela_origem_cardex,  :new.nome_prog_020,
                                             :new.peso_bruto,        v_numero_op_sped,     0,
                                             0,                      v_tipo_ordem_sped,
                                             v_estagio_op_sped,      v_cod_estagio_agrupador, v_seq_operacao_agrupador,
                     v_reposicao_sped);
             end if;
         end if;     -- if updating
      end if;        -- if (:new.tabela_origem_cardex not in ('FATU_060', 'OBRF_015') or deleting)

      -- INICIO - Atualizacao da quantidade produzida na ordem de planejamento.
      if inserting or updating or deleting
      then
         if inserting and :new.ordem_producao <> 0 and :new.rolo_estoque <> 0
         then
            begin
               -- Le o tipo do deposito, se eh de primeira ou segunda qualidade.
               select  b.tipo_prod_deposito
               into    v_tipo_prod_deposito
               from basi_205 b
               where b.codigo_deposito = :new.codigo_deposito;
            exception when others then
                  v_tipo_prod_deposito := 0;

            end;

            -- Atualiza a quantidade produzida de primeira qualidade.
            if v_tipo_prod_deposito = 1
            then
               inter_pr_atu_prod_ordem_planej(:new.area_producao,
                                              :new.ordem_producao,
                                              0,
                                              :new.panoacab_nivel99,
                                              :new.panoacab_grupo,
                                              :new.panoacab_subgrupo,
                                              :new.panoacab_item,
                                              0,
                                              0,
                                              0,
                                              :new.qtde_quilos_acab,
                                              'P');
            end if;

            -- Atualiza a quantidade produzida de segunda qualidade.
            if v_tipo_prod_deposito = 2
            then
               inter_pr_atu_prod_ordem_planej(:new.area_producao,
                                              :new.ordem_producao,
                                              0,
                                              :new.panoacab_nivel99,
                                              :new.panoacab_grupo,
                                              :new.panoacab_subgrupo,
                                              :new.panoacab_item,
                                              0,
                                              :new.qtde_quilos_acab,
                                              0,
                                              0,
                                              'P');
            end if;
         end if;

         if updating and :new.ordem_producao <> 0 and :new.rolo_estoque <> 0
         then
            -- Atualizando a producao quando o rolo que estava com situacao de previsto entra no estoque.
            if :old.rolo_estoque = 0 and :new.rolo_estoque <> 0
            then
               begin
                  -- Le o tipo do deposito, se eh de primeira ou segunda qualidade.
                  select  b.tipo_prod_deposito
                  into    v_tipo_prod_deposito
                  from basi_205 b
                  where b.codigo_deposito = :new.codigo_deposito;
                  exception
                  when others then
                     v_tipo_prod_deposito := 0;
               end;

               -- Atualiza a quantidade produzida de primeira qualidade.
               if v_tipo_prod_deposito = 1
               then
                  inter_pr_atu_prod_ordem_planej(:new.area_producao,
                                                 :new.ordem_producao,
                                                 0,
                                                 :new.panoacab_nivel99,
                                                 :new.panoacab_grupo,
                                                 :new.panoacab_subgrupo,
                                                 :new.panoacab_item,
                                                 0,
                                                 0,
                                                 0,
                                                 :new.qtde_quilos_acab,
                                                 'P');
               end if;

               -- Atualiza a quantidade produzida de segunda qualidade.
               if v_tipo_prod_deposito = 2
               then
                  inter_pr_atu_prod_ordem_planej(:new.area_producao,
                                                 :new.ordem_producao,
                                                 0,
                                                 :new.panoacab_nivel99,
                                                 :new.panoacab_grupo,
                                                 :new.panoacab_subgrupo,
                                                 :new.panoacab_item,
                                                 0,
                                                 :new.qtde_quilos_acab,
                                                 0,
                                                 0,
                                                 'P');
               end if;
            end if;

            -- Atualizando o estorno quando for alterado o peso do rolo.
            if :old.qtde_quilos_acab <> :new.qtde_quilos_acab
            then
               begin
                 -- Le o tipo do deposito, se eh de primeira ou segunda qualidade.
                  select  b.tipo_prod_deposito
                  into    v_tipo_prod_deposito
                  from basi_205 b
                  where b.codigo_deposito = :old.codigo_deposito;
                  exception
                  when others then
                     v_tipo_prod_deposito := 0;
               end;

               -- Atualiza a quantidade produzida de primeira qualidade.
               if v_tipo_prod_deposito = 1
               then
                  inter_pr_atu_prod_ordem_planej(:old.area_producao,
                                                 :old.ordem_producao,
                                                 0,
                                                 :old.panoacab_nivel99,
                                                 :old.panoacab_grupo,
                                                 :old.panoacab_subgrupo,
                                                 :old.panoacab_item,
                                                 0,
                                                 0,
                                                 0,
                                                 (:old.qtde_quilos_acab - :new.qtde_quilos_acab),
                                                 'E');
               end if;

               -- Atualiza a quantidade produzida de segunda qualidade.
               if v_tipo_prod_deposito = 2
               then
                  inter_pr_atu_prod_ordem_planej(:old.area_producao,
                                                 :old.ordem_producao,
                                                 0,
                                                 :old.panoacab_nivel99,
                                                 :old.panoacab_grupo,
                                                 :old.panoacab_subgrupo,
                                                 :old.panoacab_item,
                                                 0,
                                                 (:old.qtde_quilos_acab - :new.qtde_quilos_acab),
                                                 0,
                                                 0,
                                                 'E');
               end if;
            end if;

            -- Atualizando a quantidade quando houver troca de qualidade, no caso da alteracao do deposito.
            if :old.codigo_deposito <> :new.codigo_deposito
            then
               begin
                  -- Le o tipo do novo deposito, se eh de primeira ou segunda qualidade.
                  select  b.tipo_prod_deposito
                  into    v_tipo_prod_deposito_new
                  from basi_205 b
                  where b.codigo_deposito = :new.codigo_deposito;
                  exception
                  when others then
                     v_tipo_prod_deposito_new := 0;
               end;

               begin
                  -- Le o tipo do antigo deposito, se eh de primeira ou segunda qualidade.
                  select  b.tipo_prod_deposito
                  into    v_tipo_prod_deposito_old
                  from basi_205 b
                  where b.codigo_deposito = :old.codigo_deposito;
                  exception
                  when others then
                     v_tipo_prod_deposito_old := 0;
               end;

               -- Quando houver a troca de deposito, reatualiza a quantidade conforme a qualidade do novo deposito.
               if v_tipo_prod_deposito_old = 1 and v_tipo_prod_deposito_new = 2
               then
                  inter_pr_atu_prod_ordem_planej(:new.area_producao,
                                                 :new.ordem_producao,
                                                 0,
                                                 :new.panoacab_nivel99,
                                                 :new.panoacab_grupo,
                                                 :new.panoacab_subgrupo,
                                                 :new.panoacab_item,
                                                 0,
                                                 0,
                                                 0,
                                                 :new.qtde_quilos_acab,
                                                 'E');

                  inter_pr_atu_prod_ordem_planej(:new.area_producao,
                                                 :new.ordem_producao,
                                                 0,
                                                 :new.panoacab_nivel99,
                                                 :new.panoacab_grupo,
                                                 :new.panoacab_subgrupo,
                                                 :new.panoacab_item,
                                                 0,
                                                 :new.qtde_quilos_acab,
                                                 0,
                                                 0,
                                                 'P');
               end if;

               -- Quando houver a troca de deposito, reatualiza a quantidade conforme a qualidade do novo deposito.
               if v_tipo_prod_deposito_old = 2 and v_tipo_prod_deposito_new = 1
               then
                  inter_pr_atu_prod_ordem_planej(:new.area_producao,
                                                 :new.ordem_producao,
                                                 0,
                                                 :new.panoacab_nivel99,
                                                 :new.panoacab_grupo,
                                                 :new.panoacab_subgrupo,
                                                 :new.panoacab_item,
                                                 0,
                                                 :new.qtde_quilos_acab,
                                                 0,
                                                 0,
                                                 'E');

                  inter_pr_atu_prod_ordem_planej(:new.area_producao,
                                                 :new.ordem_producao,
                                                 0,
                                                 :new.panoacab_nivel99,
                                                 :new.panoacab_grupo,
                                                 :new.panoacab_subgrupo,
                                                 :new.panoacab_item,
                                                 0,
                                                 0,
                                                 0,
                                                 :new.qtde_quilos_acab,
                                                 'P');
               end if;
            end if;

            if deleting or :new.rolo_estoque = 9
            then
               begin
                  if :old.posicao_rolo > 1
                  then
                     raise_application_error(-20000,'Rolo ja entrou no processo de faturamento de servicos.');
                  end if;
               end;
            end if;
         end if;

         if deleting and :old.ordem_producao <> 0 and :old.rolo_estoque <> 0
         then
            begin
               -- Le o tipo do deposito, se eh de primeira ou segunda qualidade.
               select  b.tipo_prod_deposito
               into    v_tipo_prod_deposito
               from basi_205 b
               where b.codigo_deposito = :old.codigo_deposito;
               exception
               when others then
                  v_tipo_prod_deposito := 0;
            end;

            -- Atualiza a quantidade produzida de primeira qualidade.
            if v_tipo_prod_deposito = 1
            then
               inter_pr_atu_prod_ordem_planej(:old.area_producao,
                                              :old.ordem_producao,
                                              0,
                                              :old.panoacab_nivel99,
                                              :old.panoacab_grupo,
                                              :old.panoacab_subgrupo,
                                              :old.panoacab_item,
                                              0,
                                              0,
                                              0,
                                              :old.qtde_quilos_acab,
                                              'E');
            end if;

            -- Atualiza a quantidade produzida de segunda qualidade.
            if v_tipo_prod_deposito = 2
            then
               inter_pr_atu_prod_ordem_planej(:old.area_producao,
                                              :old.ordem_producao,
                                              0,
                                              :old.panoacab_nivel99,
                                              :old.panoacab_grupo,
                                              :old.panoacab_subgrupo,
                                              :old.panoacab_item,
                                              0,
                                              :old.qtde_quilos_acab,
                                              0,
                                              0,
                                              'E');
            end if;
         end if;
      end if;
      -- FIM - Atualizacao da quantidade produzida na ordem de planejamento.

      -- zera campos de controle da ficha cardex, assim nao ha perigo da trigger atualizar
      -- o estoque com dados antigos, caso o usuario nao passar estes parametros
      if inserting or updating
      then
         :new.transacao_cardex     := 0;
         :new.centro_custo_cardex  := 0;
         :new.usuario_cardex       := ' ';
         :new.tabela_origem_cardex := ' ';
         :new.nome_prog_020        := ' ';
         :new.data_cardex          := null;
      end if;

   end if;
end inter_tr_pcpt_020;

