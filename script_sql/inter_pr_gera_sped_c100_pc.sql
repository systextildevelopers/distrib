create or replace procedure inter_pr_gera_sped_c100_pc (p_cod_empresa   IN NUMBER,
                                                        p_cod_matriz    IN NUMBER,
                                                        p_cnpj9_empresa IN  NUMBER,
                                                        p_cnpj4_empresa IN  NUMBER,
                                                        p_cnpj2_empresa IN  NUMBER,
                                                        p_dat_inicial   IN DATE,
                                                        p_dat_final     IN DATE,
                                                        p_des_erro      OUT varchar2) is
--
-- Finalidade: Gerar a tabela sped_pc_c100 - Notas Fiscais
-- Autor.....: Edson Pio
-- Data......: 19/02/11
--
-- Historicos
--
-- Data    Autor    Observac?es
--

CURSOR u_fatu_050 (p_cod_empresa     NUMBER,
                   p_dat_inicial     DATE,
                   p_dat_final       DATE) IS
   SELECT fatu_050.*, trim(to_char(pedi_080.modelo_doc_fisc, '00')) modelo_doc_fisc,
          pedi_080.consumidor_final, pedi_080.cod_natureza,
          pedi_080.divisao_natur
   from   fatu_050,pedi_080,estq_005
   where  fatu_050.natop_nf_nat_oper             = pedi_080.natur_operacao
   and    fatu_050.natop_nf_est_oper             = pedi_080.estado_natoper
   and    pedi_080.codigo_transacao              = estq_005.codigo_transacao
   and    fatu_050.data_emissao                  between p_dat_inicial and p_dat_final
   and    fatu_050.situacao_nfisc                in (1,2,3,4,6)
   and    fatu_050.codigo_empresa                = p_cod_empresa
   and exists(select 1 from fatu_060, pedi_080
              where fatu_060.ch_it_nf_cd_empr       = fatu_050.codigo_empresa
                and fatu_060.ch_it_nf_num_nfis      = fatu_050.num_nota_fiscal
                and fatu_060.ch_it_nf_ser_nfis      = fatu_050.serie_nota_fisc
                and fatu_060.natopeno_nat_oper      = pedi_080.natur_operacao
                and fatu_060.natopeno_est_oper      = pedi_080.estado_natoper
                and fatu_060.valor_unitario         > 0.00
                and pedi_080.envia_sped_pis_cofins  = 0 --Envia Sped PIS COFINS
                and (pedi_080.livros_fiscais        = 1 or -- 1 - Entra nos livros fiscais
                     pedi_080.ind_nf_servico        = 1)
                and pedi_080.ind_nf_ciap                   < 2 -- 2 - n?o entra nos livros
                and pedi_080.faturamento           = 1); -- 1 - notas de faturamento (geram receitas)




CURSOR u_obrf_010 (p_cod_empresa     NUMBER,
                   p_dat_inicial     DATE,
                   p_dat_final       DATE,
                   p_acumul_pis_cofins varchar2) IS
   SELECT distinct obrf_010.*, trim(to_char(pedi_080.modelo_doc_fisc, '00')) modelo_doc_fisc,
          pedi_080.consumidor_final,pedi_080.cod_natureza,
          pedi_080.divisao_natur,   pedi_080.emite_duplicata,
          pedi_080.ind_nf_servico
   from   obrf_010,pedi_080
   where  obrf_010.natoper_nat_oper = pedi_080.natur_operacao
   and    obrf_010.natoper_est_oper = pedi_080.estado_natoper
   and    (pedi_080.livros_fiscais   = 1 or
           pedi_080.ind_nf_servico   = 1) -- 1 - Entra nos livros fiscais
   and    obrf_010.data_transacao   between p_dat_inicial and p_dat_final
   and    obrf_010.situacao_entrada in (1,2,4)
   and    obrf_010.local_entrega     = p_cod_empresa

   and exists (select 1 from obrf_015, pedi_080, estq_005
               where obrf_015.capa_ent_nrdoc   = obrf_010.documento
                 and obrf_015.capa_ent_serie   = obrf_010.serie
                 and obrf_015.capa_ent_forcli9 = obrf_010.cgc_cli_for_9
                 and obrf_015.capa_ent_forcli4 = obrf_010.cgc_cli_for_4
                 and obrf_015.capa_ent_forcli2 = obrf_010.cgc_cli_for_2
                 and obrf_015.natitem_nat_oper = pedi_080.natur_operacao
                 and obrf_015.natitem_est_oper = pedi_080.estado_natoper
                 and obrf_015.valor_unitario   > 0.00
                 and pedi_080.codigo_transacao = estq_005.codigo_transacao
                 and pedi_080.envia_sped_pis_cofins  = 0 --Envia Sped PIS COFINS
                 and (pedi_080.livros_fiscais  = 1 or
                      pedi_080.ind_nf_servico  = 1) -- 1 - Entra nos livros fiscais
                 --and pedi_080.ind_nf_ciap      < 2 -- 2 - n?o entra nos livros COMENTADO POIS NAO ESTAVA PUXANDO ALGUNS CFOP NECESSARIOS NA MASCARENHAS.
                 and (obrf_015.valor_pis        > 0.00
                      or pedi_080.cod_natureza || pedi_080.divisao_natur in ('1.201','1.202','1.203','1.204','1.410','1.411',
                                                                '2.201','2.202','2.203','2.204','2.410','2.411'))); -- notas de devoluc?o de venda
                                                        -- mesmo que valor de imposto zero
                                                        -- tambem deve ir para o arquivo





CURSOR u_fatu_060 (p_cod_empresa    NUMBER,
                   p_num_nota       number,
                   p_cod_serie_nota varchar2) IS
      select fatu_060.nivel_estrutura
            ,fatu_060.grupo_estrutura
            ,decode(decode(fatu_060.nivel_estrutura,'1',niv_quebra_peca,
                    decode(fatu_060.nivel_estrutura,'2',niv_quebra_tecid,
                    decode(fatu_060.nivel_estrutura,'4',niv_quebra_pano,
                    decode(fatu_060.nivel_estrutura,'7',niv_quebra_fio,
                    decode(fatu_060.nivel_estrutura,'9',3,
                    decode(fatu_060.nivel_estrutura,'0',3)))))),1,null,fatu_060.subgru_estrutura)
                    as subgru_estrutura
             ,decode(decode(fatu_060.nivel_estrutura,'1',niv_quebra_peca,
                    decode(fatu_060.nivel_estrutura,'2',niv_quebra_tecid,
                    decode(fatu_060.nivel_estrutura,'4',niv_quebra_pano,
                    decode(fatu_060.nivel_estrutura,'7',niv_quebra_fio,
                    decode(fatu_060.nivel_estrutura,'9',3,
                    decode(fatu_060.nivel_estrutura,'0',3)))))), 1,null,
               decode(decode(fatu_060.nivel_estrutura,'1',niv_quebra_peca,
                    decode(fatu_060.nivel_estrutura,'2',niv_quebra_tecid,
                    decode(fatu_060.nivel_estrutura,'4',niv_quebra_pano,
                    decode(fatu_060.nivel_estrutura,'7',niv_quebra_fio,
                    decode(fatu_060.nivel_estrutura,'9',3,
                    decode(fatu_060.nivel_estrutura,'0',3)))))), 2,null,
                    fatu_060.item_estrutura)) as item_estrutura
                    ,trim(fatu_060.descricao_item)
                    as descricao_item
                    ,fatu_060.unidade_medida
                    ,fatu_060.centro_custo
                    ,fatu_060.cvf_icms
                    ,fatu_060.natopeno_nat_oper
                    ,fatu_060.natopeno_est_oper
                    ,fatu_060.perc_icms
                    ,fatu_060.perc_ipi
                    ,fatu_060.cvf_ipi_saida
                    ,fatu_060.perc_pis
                    ,fatu_060.cvf_pis
                    ,fatu_060.cvf_cofins
                    ,fatu_060.perc_cofins
                    ,fatu_060.perc_iss
                    ,fatu_060.data_emissao
                    ,REPLACE(fatu_060.classific_fiscal,'.') classific_fiscal

                    ,fatu_060.procedencia
                    ,fatu_060.lote_acomp
                    ,fatu_060.ch_it_nf_cd_empr
                    ,fatu_060.num_nota_orig
                    ,trim(fatu_060.serie_nota_orig) serie_nota_orig
                    ,fatu_060.cgc9_origem
                    ,fatu_060.cgc4_origem
                    ,fatu_060.cgc2_origem
                    ,fatu_060.deposito
                    ,fatu_060.transacao
                    ,fatu_060.codigo_contabil
                    ,fatu_060.cvf_ipi
                    ,sum(fatu_060.valor_faturado) valor_faturado
                    ,sum(fatu_060.valor_faturado + fatu_060.rateio_despesa + fatu_060.rateio_desc_propaganda) valor_contabil
                    ,sum(fatu_060.qtde_item_fatur) qtde_item_fatur
                    ,sum(fatu_060.rateio_descontos_ipi) desconto_item
                    ,sum(fatu_060.base_icms + fatu_060.rateio_despesa) base_icms
                    ,sum(fatu_060.valor_icms) valor_icms
                    ,sum(fatu_060.base_ipi) base_ipi
                    ,sum(fatu_060.valor_ipi) valor_ipi
                    ,sum(fatu_060.basi_pis_cofins) base_pis_cofins
                    ,sum(fatu_060.valor_pis) valor_pis
                    ,sum(fatu_060.peso_liquido) peso_liquido
                    ,sum(fatu_060.valor_cofins) valor_cofins
                    ,sum(fatu_060.valor_iss) valor_iss
                    ,sum(fatu_060.rateio_despesa) rateio_despesa
                    ,sum(fatu_060.rateio_despesas_ipi) rateio_despesas_ipi
                    ,sum(decode(fatu_060.valor_icms_difer,0.00,0.00,fatu_060.base_icms_difer)) base_icms_difer
                    ,sum(fatu_060.valor_icms_difer) valor_icms_difer
                    ,nvl(fatu_060.cod_estagio_agrupador_insu, 0) estagio_agrupador
                    ,nvl(fatu_060.cod_estagio_simultaneo_insu, 0) estagio_agrupador_simultaneo
                    ,nvl(fatu_060.seq_operacao_agrupador_insu, 0) seq_operacao_agrupador_insu
   from   fatu_060,
          pedi_080,
          fatu_500
   where fatu_060.ch_it_nf_cd_empr  = p_cod_empresa
     and fatu_060.ch_it_nf_num_nfis = p_num_nota
     and fatu_060.ch_it_nf_ser_nfis = p_cod_serie_nota
     and fatu_060.valor_unitario    > 0.00
     and fatu_500.codigo_empresa    = fatu_060.ch_it_nf_cd_empr
     and pedi_080.natur_operacao    = fatu_060.natopeno_nat_oper
     and pedi_080.estado_natoper    = fatu_060.natopeno_est_oper
     and pedi_080.faturamento       = 1 -- 1 - notas de faturamento (geram receitas)

   group by fatu_060.nivel_estrutura
            ,fatu_060.grupo_estrutura
            ,decode(decode(fatu_060.nivel_estrutura,'1',niv_quebra_peca,
                    decode(fatu_060.nivel_estrutura,'2',niv_quebra_tecid,
                    decode(fatu_060.nivel_estrutura,'4',niv_quebra_pano,
                    decode(fatu_060.nivel_estrutura,'7',niv_quebra_fio,
                    decode(fatu_060.nivel_estrutura,'9',3,
                    decode(fatu_060.nivel_estrutura,'0',3)))))),1,null,fatu_060.subgru_estrutura)
             ,decode(decode(fatu_060.nivel_estrutura,'1',niv_quebra_peca,
                    decode(fatu_060.nivel_estrutura,'2',niv_quebra_tecid,
                    decode(fatu_060.nivel_estrutura,'4',niv_quebra_pano,
                    decode(fatu_060.nivel_estrutura,'7',niv_quebra_fio,
                    decode(fatu_060.nivel_estrutura,'9',3,
                    decode(fatu_060.nivel_estrutura,'0',3)))))), 1,null,
               decode(decode(fatu_060.nivel_estrutura,'1',niv_quebra_peca,
                    decode(fatu_060.nivel_estrutura,'2',niv_quebra_tecid,
                    decode(fatu_060.nivel_estrutura,'4',niv_quebra_pano,
                    decode(fatu_060.nivel_estrutura,'7',niv_quebra_fio,
                    decode(fatu_060.nivel_estrutura,'9',3,
                    decode(fatu_060.nivel_estrutura,'0',3)))))), 2,null,
                    fatu_060.item_estrutura))
                    ,trim(fatu_060.descricao_item)
                    ,fatu_060.unidade_medida
                    ,fatu_060.centro_custo
                    ,fatu_060.cvf_icms
                    ,fatu_060.natopeno_nat_oper
                    ,fatu_060.natopeno_est_oper
                    ,fatu_060.perc_icms
                    ,fatu_060.perc_ipi
                    ,fatu_060.cvf_ipi_saida
                    ,fatu_060.perc_pis
                    ,fatu_060.cvf_pis
                    ,fatu_060.cvf_cofins
                    ,fatu_060.perc_cofins
                    ,fatu_060.perc_iss
                    ,fatu_060.data_emissao
                    ,REPLACE(fatu_060.classific_fiscal,'.')

                    ,fatu_060.procedencia
                    ,fatu_060.lote_acomp
                    ,fatu_060.ch_it_nf_cd_empr
                    ,fatu_060.num_nota_orig
                    ,trim(fatu_060.serie_nota_orig)
                    ,fatu_060.cgc9_origem
                    ,fatu_060.cgc4_origem
                    ,fatu_060.cgc2_origem
                    ,fatu_060.deposito
                    ,fatu_060.transacao
                    ,fatu_060.codigo_contabil
                    ,fatu_060.cvf_ipi
                    ,pedi_080.cod_natureza
                    ,pedi_080.divisao_natur
                    ,nvl(fatu_060.cod_estagio_agrupador_insu, 0)
                    ,nvl(fatu_060.cod_estagio_simultaneo_insu, 0)
                    ,nvl(fatu_060.seq_operacao_agrupador_insu, 0);

CURSOR u_obrf_015 (p_cod_empresa    NUMBER,
                   p_num_nota       number,
                   p_cod_serie_nota VARCHAR2,
                   p_num_cnpj_9     NUMBER,
                   p_num_cnpj_4     NUMBER,
                   p_num_cnpj_2     NUMBER) IS

         select
                    obrf_015.coditem_nivel99
                    ,obrf_015.coditem_grupo
            ,decode(decode(obrf_015.coditem_nivel99,'1',niv_quebra_peca,
                    decode(obrf_015.coditem_nivel99,'2',niv_quebra_tecid,
                    decode(obrf_015.coditem_nivel99,'4',niv_quebra_pano,
                    decode(obrf_015.coditem_nivel99,'7',niv_quebra_fio,
                    decode(obrf_015.coditem_nivel99,'9',3,
                    decode(obrf_015.coditem_nivel99,'0',3)))))),1,null,obrf_015.coditem_subgrupo)
                    as coditem_subgrupo
            ,decode(decode(obrf_015.coditem_nivel99,'1',niv_quebra_peca,
                    decode(obrf_015.coditem_nivel99,'2',niv_quebra_tecid,
                    decode(obrf_015.coditem_nivel99,'4',niv_quebra_pano,
                    decode(obrf_015.coditem_nivel99,'7',niv_quebra_fio,
                    decode(obrf_015.coditem_nivel99,'9',3,
                    decode(obrf_015.coditem_nivel99,'0',3)))))),1,null,
             decode(decode(obrf_015.coditem_nivel99,'1',niv_quebra_peca,
                    decode(obrf_015.coditem_nivel99,'2',niv_quebra_tecid,
                    decode(obrf_015.coditem_nivel99,'4',niv_quebra_pano,
                    decode(obrf_015.coditem_nivel99,'7',niv_quebra_fio,
                    decode(obrf_015.coditem_nivel99,'9',3,
                    decode(obrf_015.coditem_nivel99,'0',3)))))),2,null,obrf_015.coditem_item))
                    as coditem_item
                    ,trim(obrf_015.descricao_item) descricao_item
                    ,obrf_015.unidade_medida
                    ,obrf_015.centro_custo
                    ,obrf_015.natitem_nat_oper
                    ,obrf_015.natitem_est_oper
                    ,obrf_015.percentual_ipi
                    ,obrf_015.cvf_ipi_entrada
                    ,obrf_015.perc_pis
                    ,obrf_015.cvf_pis
                    ,obrf_015.cvf_cofins
                    ,obrf_015.perc_cofins
                    ,obrf_015.classific_fiscal
                    ,obrf_015.procedencia
                    ,obrf_015.cod_vlfiscal_icm
                    ,obrf_015.percentual_icm
                    ,obrf_015.lote_entrega
                    ,obrf_015.num_nota_orig
                    ,trim(obrf_015.serie_nota_orig) serie_nota_orig
                    ,obrf_015.capa_ent_forcli9
                    ,obrf_015.capa_ent_forcli4
                    ,obrf_015.capa_ent_forcli2
                    ,obrf_015.codigo_deposito
                    ,obrf_015.perc_dif_aliq
                    ,obrf_015.codigo_transacao
                    ,obrf_015.codigo_contabil
                    ,obrf_015.cod_vlfiscal_ipi
                    ,obrf_015.perc_subtituicao
                    ,obrf_015.cod_base_cred
                    ,sum(obrf_015.valor_total) valor_total
                    ,sum(obrf_015.quantidade) quantidade
                    ,sum(obrf_015.base_calc_icm) base_calc_icm
                    ,sum(obrf_015.valor_icms) valor_icms
                    ,sum(obrf_015.base_diferenca) base_diferenca
                    ,sum(obrf_015.base_ipi) base_ipi
                    ,sum(obrf_015.valor_ipi) valor_ipi
                    ,sum(obrf_015.base_pis_cofins) base_pis_cofins
                    ,sum(obrf_015.valor_pis) valor_pis
                    ,sum(obrf_015.valor_cofins) valor_cofins
                    ,sum(obrf_015.valor_icms_diferido) valor_icms_diferido
                    ,sum(obrf_015.rateio_despesas) rateio_despesas
                    ,sum(obrf_015.rateio_despesas_ipi) rateio_despesas_ipi
                    ,sum(decode(obrf_015.valor_subtituicao,0.00,0.00,obrf_015.base_subtituicao)) base_subtituicao
                    ,sum(obrf_015.valor_subtituicao) valor_subtituicao
                    ,nvl(obrf_015.cod_estagio_agrupador_insu, 0) as estagio_agrupador
                    ,nvl(obrf_015.cod_estagio_simultaneo_insu, 0) as estagio_agrupador_simultaneo
                    ,nvl(obrf_015.seq_operacao_agrupador_insu, 0) as seq_operacao_agrupador_insu
   from   obrf_015,
          pedi_080,
          fatu_500
   where obrf_015.capa_ent_nrdoc   = p_num_nota
     and obrf_015.capa_ent_serie   = p_cod_serie_nota
     and obrf_015.capa_ent_forcli9 = p_num_cnpj_9
     and obrf_015.capa_ent_forcli4 = p_num_cnpj_4
     and obrf_015.capa_ent_forcli2 = p_num_cnpj_2
     and obrf_015.valor_unitario   > 0.00
     and pedi_080.natur_operacao   = obrf_015.natitem_nat_oper
     and pedi_080.estado_natoper   = obrf_015.natitem_est_oper
     and fatu_500.codigo_empresa   = p_cod_empresa		 
   group by obrf_015.coditem_nivel99
            ,obrf_015.coditem_grupo
            ,decode(decode(obrf_015.coditem_nivel99,'1',niv_quebra_peca,
                    decode(obrf_015.coditem_nivel99,'2',niv_quebra_tecid,
                    decode(obrf_015.coditem_nivel99,'4',niv_quebra_pano,
                    decode(obrf_015.coditem_nivel99,'7',niv_quebra_fio,
                    decode(obrf_015.coditem_nivel99,'9',3,
                    decode(obrf_015.coditem_nivel99,'0',3)))))),1,null,obrf_015.coditem_subgrupo)
            ,decode(decode(obrf_015.coditem_nivel99,'1',niv_quebra_peca,
                    decode(obrf_015.coditem_nivel99,'2',niv_quebra_tecid,
                    decode(obrf_015.coditem_nivel99,'4',niv_quebra_pano,
                    decode(obrf_015.coditem_nivel99,'7',niv_quebra_fio,
                    decode(obrf_015.coditem_nivel99,'9',3,
                    decode(obrf_015.coditem_nivel99,'0',3)))))),1,null,
             decode(decode(obrf_015.coditem_nivel99,'1',niv_quebra_peca,
                    decode(obrf_015.coditem_nivel99,'2',niv_quebra_tecid,
                    decode(obrf_015.coditem_nivel99,'4',niv_quebra_pano,
                    decode(obrf_015.coditem_nivel99,'7',niv_quebra_fio,
                    decode(obrf_015.coditem_nivel99,'9',3,
                    decode(obrf_015.coditem_nivel99,'0',3)))))),2,null,obrf_015.coditem_item))
                    ,trim(obrf_015.descricao_item)
                    ,obrf_015.unidade_medida
                    ,obrf_015.centro_custo
                    ,obrf_015.cod_vlfiscal_icm
                    ,obrf_015.natitem_nat_oper
                    ,obrf_015.natitem_est_oper
                    ,obrf_015.percentual_ipi
                    ,obrf_015.cvf_ipi_entrada
                    ,obrf_015.perc_pis
                    ,obrf_015.cvf_pis
                    ,obrf_015.cvf_cofins
                    ,obrf_015.perc_cofins
                    ,obrf_015.classific_fiscal
                    ,obrf_015.procedencia
                    ,obrf_015.lote_entrega
                    ,obrf_015.num_nota_orig
                    ,obrf_015.serie_nota_orig
                    ,obrf_015.capa_ent_forcli9
                    ,obrf_015.capa_ent_forcli4
                    ,obrf_015.capa_ent_forcli2
                    ,obrf_015.codigo_deposito
                    ,obrf_015.perc_dif_aliq
                    ,obrf_015.codigo_transacao
                    ,obrf_015.codigo_contabil
                    ,obrf_015.percentual_icm
                    ,obrf_015.cod_vlfiscal_ipi
                    ,pedi_080.cod_natureza
                    ,pedi_080.divisao_natur
                    ,obrf_015.perc_subtituicao
                    ,obrf_015.cod_base_cred
                    ,nvl(obrf_015.COD_ESTAGIO_AGRUPADOR_INSU, 0) 
                    ,nvl(obrf_015.COD_ESTAGIO_SIMULTANEO_INSU, 0)
                    ,nvl(obrf_015.cod_estagio_simultaneo_insu, 0)
                    ,nvl(obrf_015.seq_operacao_agrupador_insu, 0);

w_erro                  EXCEPTION;
w_aprazo_avista         number(1);
w_modelo_nota           varchar2(2);
w_tip_nota_cancel       varchar2(1);
w_num_cfop              pedi_080.cod_natureza%type;
w_divisao_natureza      pedi_080.divisao_natur%type;
w_consiste_cvf_icms     pedi_080.consiste_cvf_icms%type;
w_tipo_transacao        estq_005.atualiza_estoque%type;
w_val_total_pis         sped_c100.val_pis%type;
w_val_total_cofins      sped_c100.val_cofins%type;
w_cod_mensagem          pedi_080.cod_mensagem%type;
w_val_base_ipi          sped_c100.val_base_ipi%type;
w_insc_est_cliente      pedi_010.insc_est_cliente%type;
w_codigo_cidade_ibge    number;
w_codigo_fiscal_uf      basi_167.codigo_fiscal_uf%type;
w_nf_expecial           obrf_190.tip_nf_espedical%type;
w_situacao_entrada      obrf_010.situacao_entrada%type;
w_val_base_icms         fatu_060.base_icms%type;
w_val_icms              fatu_060.valor_icms%type;
w_cod_enquadramento_ipi basi_240.cod_enquadramento_ipi%type;
w_tp_forne_elet         empr_001.fornec_ener_elet%type;
w_tp_forne_tele         empr_001.fornec_telecomu%type;
w_tp_forne_trans        empr_001.fornec_transport%type;
w_tp_forne_agua         empr_001.fornec_agua%type;
w_tp_forne_gas          empr_001.fornec_gas%type;
w_tipo_fornecedor       supr_010.tipo_fornecedor%type;
w_num_seq_modelo        sped_c100.num_seq_modelo%type;
w_sequencia             number;
w_conta_msg             number;
w_tem_referenciado      varchar2(1);
w_cod_especie           obrf_200.codigo_especie%type;
w_serie_nfe             fatu_505.serie_nota_fisc%type;
w_modelo_especie        obrf_200.modelo_documento%type;
w_val_ipi               obrf_015.valor_ipi%type;
w_val_desconto          fatu_060.rateio_descontos_ipi%type;
ws_grupo_clas           fatu_060.grupo_estrutura%type;
ws_subgru_clas          fatu_060.subgru_estrutura%type;
w_valor_reducao_icms    obrf_015.base_diferenca%type;
w_perc_substituica      pedi_080.perc_substituica%type;
w_val_base_icms_sub_trib obrf_015.base_subtituicao%type;
w_val_icms_sub_trib      obrf_015.valor_subtituicao%type;
w_valor_contabil         obrf_015.valor_total%type;
w_tipo_titulo            fatu_505.tipo_titulo%type;
w_tp_nf                  fatu_505.tipo_nf%type;
w_qtde_parcela           number(3);
w_intervalo_parcela      pedi_075.vencimento%type;
w_cgc_9                  fatu_500.cgc_9%type;
w_cgc_4                  fatu_500.cgc_4%type;
w_cgc_2                  fatu_500.cgc_2%type;
w_cod_cred               pedi_080.cod_cred%type;
w_cod_cont_cred          pedi_080.cod_cont_crec%type;
w_ind_nat_frt            pedi_080.ind_nat_frt%type;
w_cod_cred_p             pedi_080.cod_cred%type;
encontrou_nat_empresa    boolean;
w_cod_cont_cred_p        pedi_080.cod_cont_crec%type;
w_ind_nat_frt_p          pedi_080.ind_nat_frt%type;
w_base_pis_cofins        fatu_060.basi_pis_cofins%type;
w_valor_pis_ret          cpag_010.valor_pis_imp%type;
w_valor_cofins_ret       cpag_010.valor_cofins_imp%type;
w_valor_pis_imp          obrf_056.valor_pis_imp%type;
w_valor_cofins_imp       obrf_056.valor_cofins_imp%type;
w_numero_di              obrf_056.numero_di%type;
w_tipo_declaracao        obrf_056.tipo_declaracao%type;
w_acumul_pis_cofins      fatu_501.acumul_pis_cofins%type;
w_nota_de_devolucao_compra  varchar2(1);

v_exercicio_doc          number;
v_exercicio              number;
v_plano_conta            cont_500.cod_plano_cta%type;
v_num_cc                 cont_535.cod_reduzido%type;
v_indic_natu_sped        cont_535.indic_natu_sped%type;
v_data_alt               cont_535.data_cadastro%type;
v_ind_cta                cont_535.tipo_conta%type;
v_nivel_cta              cont_535.nivel%type;
v_conta_contabil         cont_535.conta_contabil%type;
v_conta_contabil_ref     cont_535.conta_contabil_ref%type;
v_desc_conta             cont_535.descricao%type;
v_ncm_entrada            basi_240.classific_fiscal%type;
w_ind_nf_ciap            pedi_080.ind_nf_ciap%type;
w_atualiza_cod_base_cred fatu_503.atualiza_cod_base_cred%type;
v_exporta_classif_sped   basi_240.exporta_classif_sped%type;
v_ind_nf_servico         pedi_080.ind_nf_servico%type;
v_cod_mensagem           pedi_080.cod_mensagem%type;
w_ind_nf_consumo         sped_pc_c100.ind_nf_consumo%type;
w_exclui_icms_pc         number(1);
w_red_icms_base_pis_cof       number(1) := 0;
w_dif_pis_cofins         fatu_060.valor_cofins%type;



BEGIN
   begin
      select fatu_501.acumul_pis_cofins
      into   w_acumul_pis_cofins
      from fatu_501
      where fatu_501.codigo_empresa = p_cod_matriz;
   end;

   begin
      select fatu_503.atualiza_cod_base_cred
      into w_atualiza_cod_base_cred
      from fatu_503
      where fatu_503.codigo_empresa = p_cod_matriz;
   end;

   w_exclui_icms_pc := inter_fn_get_param_int(p_cod_empresa, 'fiscal.tipoReducaoBasePisCofins');

   p_des_erro          := NULL;
   --
   -- Notas de Saida
   --
   FOR fatu_050 IN u_fatu_050 (p_cod_empresa, p_dat_inicial, p_dat_final)
   LOOP

      -- verifica se placa do veiculo digitada na nota fiscal tem sete digitos ou mais,
      -- se n?o tiver, coloca o campo como '', pois sera considerado como placa invalida
      w_sequencia := 0;
      w_tem_referenciado := 'n';

      w_modelo_especie := null;

      -- le o tipo de titulo da serie de nota fiscal para poder encontrar os titulos relacionados a nota
      begin
         select fatu_505.tipo_titulo,  fatu_505.serie_nfe, trim(fatu_505.codigo_especie)
         into   w_tipo_titulo,         w_serie_nfe, w_cod_especie
         from fatu_505
         where fatu_505.codigo_empresa = fatu_050.codigo_empresa
           and trim(fatu_505.serie_nota_fisc) = trim(fatu_050.serie_nota_fisc);

       exception
          when no_data_found then
             w_tipo_titulo  := null;
             w_serie_nfe    := null;
       w_cod_especie  := null;
       end;

       -- verifica o numero de parcelas geradas para a nota fiscal para identificar se a nota e a vista ou a prazo
       begin
          select count(*) into w_qtde_parcela from fatu_070
          where fatu_070.codigo_empresa   = fatu_050.codigo_empresa
            and fatu_070.cli_dup_cgc_cli9 = fatu_050.cgc_9
            and fatu_070.cli_dup_cgc_cli4 = fatu_050.cgc_4
            and fatu_070.cli_dup_cgc_cli2 = fatu_050.cgc_2
            and fatu_070.num_duplicata    = fatu_050.num_nota_fiscal
            and fatu_070.tipo_titulo      = w_tipo_titulo;
       exception
         when no_data_found then
            w_qtde_parcela  := 0;
       end;


       -- se encontrar titulos verifica o intervalo de datas para verificar se a nota fiscal e a prazo ou a vista
       w_intervalo_parcela  := 0;

       if w_qtde_parcela > 0
       then
          begin
             select pedi_075.vencimento into w_intervalo_parcela from pedi_075
             where pedi_075.condicao_pagto = fatu_050.cond_pgto_venda
               and rownum = 1;

          exception
             when no_data_found then
                w_intervalo_parcela  := 0;
          end;
       end if;

       if w_qtde_parcela = 0
       then
               if trunc(p_dat_inicial) >= to_date('01-07-12','dd-mm-yy')
               then
                  w_aprazo_avista := 2; -- sem pagamento
               else
                  w_aprazo_avista := 9;
          end if;
       else

          if w_intervalo_parcela = 0 and w_qtde_parcela = 1
          then
              w_aprazo_avista := 0; -- avista
          else
             w_aprazo_avista := 1; -- aprazo
          end if;
       end if;

       if w_serie_nfe = 'S' and trim(fatu_050.numero_danf_nfe) is not null and w_cod_especie <> 'NFC-e' -- em caso de notas antigas que utilizam a mesma serie da NF-e
       then
          fatu_050.modelo_doc_fisc := '55';
       else
          if w_cod_especie is not null
          then
             begin
                select trim(obrf_200.modelo_documento)
                  into w_modelo_especie
                from obrf_200
                where obrf_200.codigo_especie  = w_cod_especie;
             EXCEPTION
                WHEN OTHERS THEN
                   w_modelo_especie := null;
             END;
          end if;
       end if;

       if w_modelo_especie is not null
       then
          if Nvl(Length(w_modelo_especie),0) > 2
          then
              w_modelo_especie := substr(w_modelo_especie,1,2);
          end if;

          fatu_050.modelo_doc_fisc := w_modelo_especie;
       end if;

       if fatu_050.modelo_doc_fisc is null or fatu_050.modelo_doc_fisc = '00'
       then
          w_modelo_nota := '01';
       else
          w_modelo_nota := fatu_050.modelo_doc_fisc;
       end if;

       -- identifica se a nota fiscal esta cancelada ou n?o
       if fatu_050.situacao_nfisc = 2
       then
          w_tip_nota_cancel := 'S';
       else
          w_tip_nota_cancel := 'N';
       end if;

       if fatu_050.situacao_nfisc <> 2
       then
          /* Se a nota for diferente de 1 (Normal) e n?o estiver cancelada,
             classifica ela como nota complementar */
          if fatu_050.tipo_nf_referenciada = 1
          then
             fatu_050.situacao_nfisc := 0;
          else
             fatu_050.situacao_nfisc := 6;
          end if;
       end if;

       if fatu_050.situacao_nfisc = 2 and fatu_050.cod_justificativa = 1
       then
          fatu_050.situacao_nfisc := 5;
       end if;

       -- le a tabela de processos referenciados,
       -- se houver dados e o processo for especial (tip_nf_espedical = 1) seta a situacao da NF para 8
       BEGIN
          select obrf_190.tip_nf_espedical into w_nf_expecial
          from obrf_190
          where obrf_190.cod_empresa       = fatu_050.codigo_empresa
            and obrf_190.num_nota          = fatu_050.num_nota_fiscal
            and trim(obrf_190.cod_serie_nota)    = trim(fatu_050.serie_nota_fisc)
            and obrf_190.num_cnpj_9        = fatu_050.cgc_9
            and obrf_190.num_cnpj_4        = fatu_050.cgc_4
            and obrf_190.num_cnpj_2        = fatu_050.cgc_2
            and trim(obrf_190.num_processo) is not null;

       EXCEPTION
          WHEN OTHERS THEN
             w_nf_expecial := 0;
       END;

       if w_nf_expecial = 1
       then
          fatu_050.situacao_nfisc := 8;
       end if;

       -- le os dados dos cliente para gravar na tabela de NF's
       BEGIN
          SELECT basi_160.codigo_fiscal,        basi_167.codigo_fiscal_uf,
                 pedi_010.insc_est_cliente
          INTO   w_codigo_cidade_ibge,          w_codigo_fiscal_uf,
                 w_insc_est_cliente
          from   basi_160,  basi_167, basi_165,  pedi_010
          where pedi_010.cgc_9       = fatu_050.cgc_9
            and pedi_010.cgc_4       = fatu_050.cgc_4
            and pedi_010.cgc_2       = fatu_050.cgc_2
            and basi_160.cod_cidade  = pedi_010.cod_cidade
            and basi_160.estado      = basi_167.estado
            and basi_160.codigo_pais = basi_167.codigo_pais
            and basi_160.codigo_pais = basi_165.codigo_pais;

       EXCEPTION
          WHEN OTHERS THEN
             w_codigo_fiscal_uf   := NULL;
             w_codigo_cidade_ibge := NULL;
             w_insc_est_cliente   := NULL;
       END;

      if fatu_050.serie_nota_fisc = 'ECF'
       then
          w_modelo_nota := '02';
       end if;


       if w_modelo_nota = '08' or w_modelo_nota = '09' or
          w_modelo_nota = '10' or w_modelo_nota = '11' or
          w_modelo_nota = '57'
       then
         w_num_seq_modelo := 1;
       else
         if w_modelo_nota = '21' or w_modelo_nota = '22'
         then
            w_num_seq_modelo := 2;
         end if;
       end if;

       begin
          select fatu_500.cgc_9, fatu_500.cgc_4,
                 fatu_500.cgc_2
          into w_cgc_9,          w_cgc_4,
               w_cgc_2
          from fatu_500
          where fatu_500.codigo_empresa = p_cod_empresa;
       end;

       if (fatu_050.transpor_forne9 = w_cgc_9 and fatu_050.transpor_forne4 = w_cgc_4 and
          fatu_050.transpor_forne2 = w_cgc_2) or
          (fatu_050.transpor_forne9 = fatu_050.cgc_9 and fatu_050.transpor_forne4 = fatu_050.cgc_9 and
          fatu_050.transpor_forne2 = fatu_050.cgc_9)
       then
          fatu_050.transpor_forne9 := 0;
          fatu_050.transpor_forne4 := 0;
          fatu_050.transpor_forne2 := 0;
       end if;

       begin
           select pedi_080.ind_nf_servico, pedi_080.cod_mensagem
           into v_ind_nf_servico,          v_cod_mensagem
           from pedi_080
           where pedi_080.natur_operacao = fatu_050.natop_nf_nat_oper
             and pedi_080.estado_natoper = fatu_050.natop_nf_est_oper;
       exception
          when no_data_found then
              v_ind_nf_servico := 0;
              v_cod_mensagem := 0;
       end;

       -- insere o regsitro da capa da nota fiscal na tabela
       BEGIN

          INSERT INTO sped_pc_c100
             (cod_matriz
             ,cod_empresa
             ,num_cnpj9_empr
             ,num_cnpj4_empr
             ,num_cnpj2_empr
             ,num_nota_fiscal
             ,cod_serie_nota
             ,num_cnpj_9
             ,num_cnpj_4
             ,num_cnpj_2
             ,tip_entrada_saida
             ,tip_emissao_prop
             ,cod_modelo
             ,cod_situacao_nota
             ,dat_saida
             ,dat_emissao
             ,val_nota
             ,tip_pagto
             ,val_descon
             ,val_itens
             ,tip_frete
             ,val_frete
             ,val_seguro
             ,val_despesas
             ,val_base_icms
             ,val_icms
             ,val_base_icms_sub
             ,val_icms_sub
             ,val_ipi
             ,val_pis
             ,val_cofins
             ,val_pis_sub
             ,val_cofins_sub
             ,val_iss
             ,num_doc_referen
             ,cod_serie_doc_referen
             ,sig_estado
             ,cod_natur_oper
             ,cod_cidade_ibge
             ,num_cnpj_9_transp
             ,num_cnpj_4_transp
             ,num_cnpj_2_transp
             ,num_nota_eletric
             ,cod_impressora_fiscal
             ,num_seq_modelo
             ,num_serie_ecf
             ,ind_nf_servico
             ,cod_mensagem
             ) VALUES
             (p_cod_matriz
             ,p_cod_empresa
             ,p_cnpj9_empresa
             ,p_cnpj4_empresa
             ,p_cnpj2_empresa
             ,fatu_050.num_nota_fiscal
             ,trim(fatu_050.serie_nota_fisc)
             ,fatu_050.cgc_9
             ,fatu_050.cgc_4
             ,fatu_050.cgc_2
             ,'S'
             ,'0' -- saidas sempre emiss?o propria
             ,lpad(w_modelo_nota,2,'0')
             ,fatu_050.situacao_nfisc
             ,fatu_050.data_saida
             ,fatu_050.data_emissao
             ,(fatu_050.valor_itens_nfis + fatu_050.valor_ipi      + fatu_050.valor_encar_nota +
               fatu_050.valor_frete_nfis + fatu_050.valor_seguro   - fatu_050.valor_desc_nota -
               fatu_050.valor_suframa    + fatu_050.valor_despesas - fatu_050.vlr_desc_especial +
               fatu_050.valor_icms_sub)  -- valor da nota
             ,w_aprazo_avista
             ,fatu_050.valor_desc_nota
             ,fatu_050.valor_itens_nfis
             ,fatu_050.tipo_frete
             ,fatu_050.valor_frete_nfis
             ,fatu_050.valor_seguro
             ,fatu_050.valor_despesas
             ,fatu_050.base_icms
             ,fatu_050.valor_icms
             ,fatu_050.base_icms_sub
             ,fatu_050.valor_icms_sub
             ,fatu_050.valor_ipi
             ,NULL  --val_pis
             ,NULL  --val_cofins
             ,0.00  --val_pis_sub
             ,0.00  --val_cofins_sub
             ,fatu_050.valor_iss
             ,decode(fatu_050.nota_fatura,0,fatu_050.nota_entrega,fatu_050.nota_fatura)  --num_doc_referen
             ,trim(fatu_050.serie_fatura)  --cod_serie_doc_referen
             ,fatu_050.natop_nf_est_oper
             ,fatu_050.natop_nf_nat_oper
             ,lpad(w_codigo_fiscal_uf,2,0) || lpad(w_codigo_cidade_ibge,5,0)
             ,fatu_050.transpor_forne9
             ,fatu_050.transpor_forne4
             ,fatu_050.transpor_forne2
             ,trim(fatu_050.numero_danf_nfe) -- num_nota_eletric
             ,fatu_050.cod_impressora_fiscal
             ,w_num_seq_modelo
             ,fatu_050.num_serie_ecf
             ,v_ind_nf_servico
             ,v_cod_mensagem);

       EXCEPTION
          WHEN OTHERS THEN
             p_des_erro := 'Erro na inclus?o da tabela sped_pc_c100 - Saidas ' || Chr(10) || SQLERRM;
             RAISE W_ERRO;
       END;

       COMMIT;


       --
       -- Atualiza os itens da nota fiscal
       --
       FOR fatu_060 IN u_fatu_060 (fatu_050.codigo_empresa, fatu_050.num_nota_fiscal, fatu_050.serie_nota_fisc)
       LOOP
          begin
             select pedi_080.cod_natureza,     pedi_080.divisao_natur,
                    pedi_080.consiste_cvf_icms,pedi_080.cod_mensagem,
                    pedi_080.perc_substituica,
                    pedi_080.tem_mov_fisica, pedi_080.cod_cred,
                    pedi_080.cod_cont_crec,  pedi_080.ind_nat_frt,
                    pedi_080.red_icms_base_pis_cof
             into   w_num_cfop,                w_divisao_natureza,
                    w_consiste_cvf_icms,       w_cod_mensagem,
                    w_perc_substituica,
                    w_tipo_transacao,          w_cod_cred,
                    w_cod_cont_cred,           w_ind_nat_frt,
                    w_red_icms_base_pis_cof
             from pedi_080
             where pedi_080.natur_operacao = fatu_060.natopeno_nat_oper
               and pedi_080.estado_natoper = fatu_060.natopeno_est_oper;
          exception
          when no_data_found then
             w_num_cfop          := '';
             w_divisao_natureza  := 0;
             w_consiste_cvf_icms := 1;
             w_cod_mensagem      := 0;
             w_tipo_transacao    := 0;
             w_cod_cred          := 0;
             w_cod_cont_cred     := 0;
             w_ind_nat_frt       := 0;
             w_red_icms_base_pis_cof := 0;
          end;

          encontrou_nat_empresa := true;

          begin
             select pedi_081.cod_cred,
                    pedi_081.cod_cont_crec,  pedi_081.ind_nat_frt
             into   w_cod_cred_p,
                    w_cod_cont_cred_p,       w_ind_nat_frt_p
             from pedi_081
             where pedi_081.cod_empresa    = fatu_050.codigo_empresa
               and pedi_081.natur_operacao = fatu_060.natopeno_nat_oper
               and pedi_081.estado_natoper = fatu_060.natopeno_est_oper;
          exception
          when no_data_found then
             w_cod_cred_p          := 0;
             w_cod_cont_cred_p     := 0;
             w_ind_nat_frt_p       := 9;
             encontrou_nat_empresa := false;
          end;

          if encontrou_nat_empresa
          then
             w_cod_cred       := w_cod_cred_p;
             w_cod_cont_cred  := w_cod_cont_cred_p;
             w_ind_nat_frt    := w_ind_nat_frt_p;
          end if;

          /*FOI ALTERADO A REGRA DO SPED CONTRIBUIC?ES E PARA NOTAS DE DEVOLUC?O DE COMPRA
            COM CST 49 N?O SERA MAIS APRESENTADAS NO ARQUIVO, REGISTRO C190, POR ISSO A VARIAVEL ABAIXO
            FICA COM N.
          */
          w_nota_de_devolucao_compra := 'N';


          /* DEVIDO AO ITENS DE NOTAS SEREM COMERCIALIZADOS COM NCM DIFERENTE NO MES OPTAMOS POR PEGAR A NCM DO ITEM*/
          if fatu_060.nivel_estrutura <> '0' and
             fatu_060.grupo_estrutura <> '00000'
          then
              begin
                  select basi_010.classific_fiscal
                  into v_ncm_entrada
                  from basi_010
                  where basi_010.nivel_estrutura  = fatu_060.nivel_estrutura
                    and basi_010.grupo_estrutura  = fatu_060.grupo_estrutura
                    and (basi_010.subgru_estrutura = fatu_060.subgru_estrutura or fatu_060.subgru_estrutura is null)
                    and (basi_010.item_estrutura   = fatu_060.item_estrutura or fatu_060.item_estrutura is null)
                    and rownum                     = 1;
              exception
                  when no_data_found then
                     v_ncm_entrada := ' ';
              end;

              if trim(v_ncm_entrada) is not null
              then
                 fatu_060.classific_fiscal := v_ncm_entrada;
              end if;
          end if;
          
          --BALENA
          if (fatu_060.estagio_agrupador + fatu_060.estagio_agrupador_simultaneo) > 0
          then
            fatu_060.classific_fiscal := inter_fn_get_classific(fatu_060.nivel_estrutura, fatu_060.grupo_estrutura, fatu_060.subgru_estrutura,
                                      fatu_060.item_estrutura, fatu_060.estagio_agrupador, fatu_060.estagio_agrupador_simultaneo);
          end if;

          if ((fatu_060.cvf_icms >= 30 and fatu_060.cvf_icms <> 70) or fatu_060.cvf_icms = 90)
          then
             fatu_060.valor_icms := 0.00;
          end if;

          if fatu_060.valor_icms = 0.00
          then
             fatu_060.base_icms   := 0.00;
             fatu_060.perc_icms   := 0.00;
             w_valor_reducao_icms := 0.00;
           else
              w_valor_reducao_icms := fatu_060.valor_faturado - fatu_060.base_icms - fatu_060.rateio_despesa;

              if w_valor_reducao_icms < 0.00
              then
                 w_valor_reducao_icms := 0.00;
              end if;
           end if;

           if fatu_060.valor_pis = 0.00
           then
              fatu_060.base_pis_cofins     := 0.00;
              fatu_060.perc_pis            := 0.00;
              fatu_060.perc_cofins         := 0.00;
           end if;

           if fatu_060.cvf_ipi > 1
           then
              fatu_060.base_ipi        := 0.00;
              fatu_060.valor_ipi       := 0.00;
              fatu_060.cvf_ipi         := 0.00;
           end if;

          begin
             select trim(basi_240.cod_enquadramento_ipi)
             into   w_cod_enquadramento_ipi
             from basi_240
             where basi_240.classific_fiscal = fatu_060.classific_fiscal;
          exception
          when no_data_found then
             w_cod_enquadramento_ipi := null;
          end;

           if fatu_060.nivel_estrutura = '0' and
              fatu_060.grupo_estrutura = '00000'
           then
              if substr(fatu_060.classific_fiscal,1,5) is not null
              then
                 ws_grupo_clas := substr(fatu_060.classific_fiscal,1,5);
              else
                 ws_grupo_clas := ' ';
              end if;

              if substr(fatu_060.classific_fiscal,6,3) is not null
              then
                 ws_subgru_clas := substr(fatu_060.classific_fiscal,6,3);
              else
                 ws_subgru_clas := ' ';
              end if;


              fatu_060.grupo_estrutura  := ws_grupo_clas;
              fatu_060.subgru_estrutura := ws_subgru_clas;
              fatu_060.nivel_estrutura  := ' ';
              fatu_060.item_estrutura   := ' ';
           end if;

           v_exercicio_doc := inter_fn_checa_data(p_cod_matriz, fatu_050.data_emissao, 0);

           v_exercicio := inter_fn_checa_data(p_cod_matriz, fatu_050.data_emissao,0);
           if v_exercicio_doc < 0
           then
              v_exercicio_doc := v_exercicio;
           end if;

           v_plano_conta := 0;

           v_num_cc             := null;
           v_data_alt           := null;
           v_ind_cta            := null;
           v_nivel_cta          := null;
           v_conta_contabil_ref := null;
           v_desc_conta         := null;
           v_indic_natu_sped    := null;

           if v_exercicio > 0 and v_exercicio_doc > 0
           then
              v_num_cc := inter_fn_encontra_conta(p_cod_empresa, 3,
                                                  fatu_060.codigo_contabil,
                                                  fatu_060.transacao,
                                                  fatu_060.centro_custo,
                                                  v_exercicio,
                                                  v_exercicio_doc);

              begin
                 select cont_500.cod_plano_cta
                 into   v_plano_conta
                 from cont_500
                 where cont_500.cod_empresa  = p_cod_matriz
                   and cont_500.exercicio    = v_exercicio_doc;
              exception
                 when no_data_found then
                  v_plano_conta := 0;
              end;

              if v_plano_conta > 0
              then
                 begin
                    select cont_535.indic_natu_sped,    cont_535.data_cadastro,
                           cont_535.tipo_conta,         cont_535.nivel,
                           cont_535.conta_contabil,     cont_535.conta_contabil_ref,
                           cont_535.descricao
                    into   v_indic_natu_sped,           v_data_alt,
                           v_ind_cta,                   v_nivel_cta,
                           v_conta_contabil,            v_conta_contabil_ref,
                           v_desc_conta
                    from cont_535
                    where cont_535.cod_plano_cta = v_plano_conta
                      and cont_535.cod_reduzido  = v_num_cc
                      and rownum                 = 1;
                 exception
                     when no_data_found then
                         v_num_cc             := null;
                         v_data_alt           := null;
                         v_ind_cta            := null;
                         v_nivel_cta          := null;
                         v_conta_contabil_ref := null;
                         v_desc_conta         := null;
                         v_indic_natu_sped    := null;
                 end;
              end if;
           end if;

           begin
              select basi_240.exporta_classif_sped
              into v_exporta_classif_sped from basi_240
              where basi_240.classific_fiscal = fatu_060.classific_fiscal
               and  rownum                    = 1;
           exception
               when no_data_found then
                  v_exporta_classif_sped := 'S';
           end;

           if v_exporta_classif_sped = 'N' and fatu_060.nivel_estrutura = '0' and fatu_060.grupo_estrutura = '00000'
           then
              fatu_060.classific_fiscal := '';
           end if;

           w_sequencia := w_sequencia + 1;

           if w_exclui_icms_pc > 0 and w_red_icms_base_pis_cof > 0 and fatu_050.data_emissao >= to_date('24-05-2021','dd-mm-yy')
           then
             if w_exclui_icms_pc = 1 or fatu_050.data_emissao < to_date('01-02-2022', 'dd-mm-yy')
             then
               w_val_desconto := fatu_060.desconto_item + fatu_060.valor_icms;
             else
               w_dif_pis_cofins := (fatu_060.valor_contabil * ((fatu_060.perc_pis + fatu_060.perc_cofins) / 100)) - (fatu_060.valor_pis + fatu_060.valor_cofins);
               w_val_desconto := fatu_060.desconto_item + fatu_060.valor_icms + w_dif_pis_cofins;
             end if;
           else
             w_val_desconto := fatu_060.desconto_item;
           end if;

           BEGIN
              INSERT INTO sped_pc_c170
                  (cod_matriz
                  ,cod_empresa
                  ,num_nota_fiscal
                  ,cod_serie_nota
                  ,seq_item_nota
                  ,num_cnpj_9
                  ,num_cnpj_4
                  ,num_cnpj_2
                  ,tip_entrada_saida
                  ,cod_nivel
                  ,cod_grupo
                  ,cod_subgru
                  ,cod_item
                  ,des_item
                  ,cod_unid_medida
                  ,val_unitario
                  ,val_faturado
                  ,val_contabil
                  ,qtde_item_fatur
                  ,cod_c_custo
                  ,tip_transacao
                  ,val_desconto
                  ,cvf_icms
                  ,cod_nat_oper
                  ,sig_estado_oper
                  ,num_cfop
                  ,val_base_icms
                  ,perc_icms
                  ,val_icms
                  ,val_base_icms_sub_trib
                  ,perc_icms_sub_trib
                  ,val_base_red_sub_trib
                  ,val_icms_sub_trib
                  ,tip_apuracao
                  ,cod_enquadramento_ipi
                  ,val_base_ipi
                  ,perc_ipi
                  ,val_ipi
                  ,cvf_ipi
                  ,val_basi_pis_cofins
                  ,val_pis
                  ,perc_pis
                  ,cvf_pis
                  ,cvf_cofins
                  ,perc_cofins
                  ,val_cofins
                  ,perc_iss
                  ,dat_emissao
                  ,cod_classific_fiscal
                  ,tip_trib_federal
                  ,tip_trib_estadual
                  ,tip_trib_icms
                  ,val_iss
                  ,cod_empresa_refer
                  ,num_nota_refer
                  ,cod_serie_refer
                  ,seq_nota_refer
                  ,num_cnpj9_refer
                  ,num_cnpj4_refer
                  ,num_cnpj2_refer
                  ,tipo_transacao
                  ,cod_mensagem_nat_oper
                  ,cod_transacao
                  ,cod_contabil
                  ,cod_mensagem_fiscal
                  ,cod_crec
                  ,cod_cont_crec
                  ,ind_nat_frt
                  ,cod_modelo
                  ,cod_situacao_nota
                  ,nota_devolucao_compra
                  ,num_conta_contabil
                  ,data_alter_cc
                  ,ind_cta
                  ,nivel_cc
                  ,cod_cta_ref
                  ,nome_cta
                  ,cod_nat_cc
                  ,estagio_agrupador
                  ,estagio_agrupador_simultaneo
                  ,seq_operacao_agrupador_insu
                  ) VALUES
                  (p_cod_matriz                                --matriz
                  ,p_cod_empresa                               -- cod_empresa
                  ,fatu_050.num_nota_fiscal                    -- num_nota_fiscal
                  ,trim(fatu_050.serie_nota_fisc)              -- cod_serie_nota
                  ,w_sequencia                                 -- seq_item_nota
                  ,fatu_050.cgc_9                              --num_cnpj_9
                  ,fatu_050.cgc_4                              --num_cnpj_4
                  ,fatu_050.cgc_2                              --num_cnpj_2
                  ,'S'                                         --tip_entrada_saida
                  ,fatu_060.nivel_estrutura                    --cod_nivel
                  ,fatu_060.grupo_estrutura                    --cod_grupo
                  ,fatu_060.subgru_estrutura                   --cod_subgru
                  ,fatu_060.item_estrutura                     --cod_item
                  ,trim(replace(fatu_060.descricao_item, chr(9), ' '))   || ' ' || inter_fn_descr_blk(fatu_060.estagio_agrupador,fatu_060.estagio_agrupador_simultaneo)  --des_item
                  ,fatu_060.unidade_medida                     --cod_unid_medida
                  ,decode(fatu_060.qtde_item_fatur,0.00,0.00,fatu_060.valor_faturado/fatu_060.qtde_item_fatur)  --val_unitario
                  ,fatu_060.valor_faturado                     --val_faturado
                  ,fatu_060.valor_contabil                     --val_contabil
                  ,decode(fatu_060.qtde_item_fatur,0.000,1.000,fatu_060.qtde_item_fatur)   --qtde_item_fatur
                  ,fatu_060.centro_custo                       --cod_c_custo
                  ,w_tipo_transacao                                        --tip_transacao
                  ,w_val_desconto                              --val_desconto
                  ,fatu_060.cvf_icms                           --cvf_icms
                  ,fatu_060.natopeno_nat_oper                  --cod_nat_oper
                  ,fatu_060.natopeno_est_oper                  --sig_estado_oper
                  ,REPLACE(w_num_cfop,'.') || w_divisao_natureza
                  ,fatu_060.base_icms                          --val_base_icms
                  ,fatu_060.perc_icms                          --perc_icms
                  ,fatu_060.valor_icms                         --val_icms
                  ,decode(fatu_060.valor_icms_difer,0.00,0.00,fatu_060.base_icms_difer)  --val_base_icms_sub_trib
                  ,w_perc_substituica                          --perc_icms_sub_trib
                  ,w_valor_reducao_icms                        --val_base_red_sub_trib
                  ,fatu_060.valor_icms_difer                   --val_icms_sub_trib
                  ,1                                           --tip_apuracao
                  ,w_cod_enquadramento_ipi                     --cod_enquadramento_ipi
                  ,fatu_060.base_ipi                           --val_base_ipi
                  ,fatu_060.perc_ipi                           --perc_ipi
                  ,fatu_060.valor_ipi                          --val_ipi
                  ,fatu_060.cvf_ipi_saida                      --cvf_ipi
                  ,fatu_060.base_pis_cofins                    --val_basi_pis_cofins
                  ,fatu_060.valor_pis                          --val_pis
                  ,fatu_060.perc_pis                           --perc_pis
                  ,fatu_060.cvf_pis                            --cvf_pis
                  ,fatu_060.cvf_cofins                         --cvf_cofins
                  ,fatu_060.perc_cofins                        --perc_cofins
                  ,fatu_060.valor_cofins                       --val_cofins
                  ,fatu_060.perc_iss                           --perc_iss
                  ,fatu_060.data_emissao                       --dat_emissao
                  ,substr(REPLACE(fatu_060.classific_fiscal,'.'),1,8)      --cod_classific_fiscal
                  ,0.00                                        --tip_trib_federal
                  ,fatu_060.procedencia                        --tip_trib_estadual
                  ,fatu_060.cvf_icms                           --tip_trib_icms
                  ,fatu_060.valor_iss                          --val_iss
                  ,fatu_060.ch_it_nf_cd_empr
                  ,fatu_060.num_nota_orig
                  ,trim(fatu_060.serie_nota_orig)
                  ,null
                  ,fatu_060.cgc9_origem
                  ,fatu_060.cgc4_origem
                  ,fatu_060.cgc2_origem
                  ,w_tipo_transacao
                  ,null
                  ,fatu_060.transacao
                  ,fatu_060.codigo_contabil
                  ,w_cod_mensagem
                  ,w_cod_cred
                  ,w_cod_cont_cred
                  ,w_ind_nat_frt
                  ,lpad(w_modelo_nota,2,'0')
                  ,fatu_050.situacao_nfisc
                  ,w_nota_de_devolucao_compra
                  ,v_num_cc
                  ,v_data_alt
                  ,v_ind_cta
                  ,v_nivel_cta
                  ,v_conta_contabil_ref
                  ,v_desc_conta
                  ,v_indic_natu_sped
                  ,fatu_060.estagio_agrupador
                  ,fatu_060.estagio_agrupador_simultaneo
                  ,nvl(fatu_060.seq_operacao_agrupador_insu,0)
                  );
         EXCEPTION
             WHEN OTHERS THEN
               p_des_erro := 'Erro na inclus?o da tabela sped_pc_c170 - Saidas ' || Chr(10) || SQLERRM;
               RAISE W_ERRO;
         END;


       END LOOP;

       begin
          select sum(sped_pc_c170.val_pis),      sum(sped_pc_c170.val_cofins),
                 sum(sped_pc_c170.val_base_ipi), sum(sped_pc_c170.val_ipi),
                 sum(sped_pc_c170.val_base_icms),sum(sped_pc_c170.val_icms),
                 sum(decode(val_icms_sub_trib,0.00,0.00,val_base_icms_sub_trib)), sum(val_icms_sub_trib),
                 sum(sped_pc_c170.val_basi_pis_cofins)
          into   w_val_total_pis,             w_val_total_cofins,
                 w_val_base_ipi,              w_val_ipi,
                 w_val_base_icms,             w_val_icms,
                 w_val_base_icms_sub_trib,    w_val_icms_sub_trib,
                 w_base_pis_cofins
          from sped_pc_c170
          where sped_pc_c170.cod_empresa     = p_cod_empresa
            and sped_pc_c170.num_nota_fiscal = fatu_050.num_nota_fiscal
            and trim(sped_pc_c170.cod_serie_nota)  = trim(fatu_050.serie_nota_fisc)
            and sped_pc_c170.num_cnpj_9      = fatu_050.cgc_9
            and sped_pc_c170.num_cnpj_4      = fatu_050.cgc_4
            and sped_pc_c170.num_cnpj_2      = fatu_050.cgc_2;
       exception
       when no_data_found then
            w_val_total_pis    := 0.00;
            w_val_total_cofins := 0.00;
            w_val_base_ipi     := 0.00;
            w_val_ipi          := 0.00;
            w_val_base_icms    := 0.00;
            w_val_icms         := 0.00;
            w_val_base_icms_sub_trib := 0.00;
            w_val_icms_sub_trib      := 0.00;
            w_base_pis_cofins := 0.00;
       end;


       begin
          update sped_pc_c100
            set sped_pc_c100.val_pis              = w_val_total_pis,
                sped_pc_c100.val_cofins           = w_val_total_cofins,
                sped_pc_c100.val_base_ipi         = w_val_base_ipi,
                sped_pc_c100.val_ipi              = w_val_ipi,
                sped_pc_c100.val_base_icms        = w_val_base_icms,
                sped_pc_c100.val_icms             = w_val_icms,
                sped_pc_c100.val_base_icms_sub    = w_val_base_icms_sub_trib,
                sped_pc_c100.val_icms_sub         = w_val_icms_sub_trib,
                sped_pc_c100.val_base_pis_cofins  = w_base_pis_cofins
            where sped_pc_c100.cod_empresa     = p_cod_empresa
              and sped_pc_c100.num_nota_fiscal = fatu_050.num_nota_fiscal
              and trim(sped_pc_c100.cod_serie_nota)  = trim(fatu_050.serie_nota_fisc)
              and sped_pc_c100.num_cnpj_9      = fatu_050.cgc_9
              and sped_pc_c100.num_cnpj_4      = fatu_050.cgc_4
              and sped_pc_c100.num_cnpj_2      = fatu_050.cgc_2;
        EXCEPTION
        WHEN OTHERS THEN
             p_des_erro := 'Erro na atualizac?o da tabela sped_pc_c100 - Saidas ' || Chr(10) || SQLERRM;
             RAISE W_ERRO;
        END;


       COMMIT;
   END LOOP;

   --
   -- Notas de Entrada
   --

   select empr_001.fornec_ener_elet, empr_001.fornec_telecomu,
          empr_001.fornec_transport, empr_001.fornec_agua,
          empr_001.fornec_gas
   into   w_tp_forne_elet,           w_tp_forne_tele,
          w_tp_forne_trans,          w_tp_forne_agua,
          w_tp_forne_gas
   from empr_001;

   FOR obrf_010 IN u_obrf_010 (p_cod_empresa, p_dat_inicial, p_dat_final,w_acumul_pis_cofins)
   LOOP


         w_sequencia := 0;

           -- encontra o modelo da nota fiscal conforme regra ja existente no sintegra.
           -- se o modelo que esta na cadastrado no cadastro de natirezas de operac?es for nulo ou zero
           -- ent?o forca o modelo conforme tipo dpo documento ou tipo do fornecedor

           w_cod_especie := null;
           w_serie_nfe   := null;
           w_modelo_especie := null;

           BEGIN
              select trim(fatu_505.codigo_especie), trim(fatu_505.serie_nfe),
                     fatu_505.tipo_nf,              fatu_505.tipo_titulo
              into w_cod_especie,                   w_serie_nfe,
                   w_tp_nf,                         w_tipo_titulo
              from fatu_505
              where fatu_505.codigo_empresa  = obrf_010.local_entrega
                and fatu_505.serie_nota_fisc = obrf_010.serie;
           EXCEPTION
              WHEN OTHERS THEN
                 w_cod_especie := null;
                 w_serie_nfe   := null;
                 w_tp_nf       := null;
                 w_tipo_titulo := null;
           END;

           if obrf_010.emite_duplicata = 2
           then
              w_qtde_parcela  := 0;
           else
              begin
                 select count(*) into w_qtde_parcela from cpag_010
                 where cpag_010.codigo_empresa = obrf_010.local_entrega
                   and cpag_010.documento      = obrf_010.documento
                   and cpag_010.serie          = obrf_010.serie
                   and cpag_010.cgc_9          = obrf_010.cgc_cli_for_9
                   and cpag_010.cgc_4          = obrf_010.cgc_cli_for_4
                   and cpag_010.cgc_2          = obrf_010.cgc_cli_for_2
                   and cpag_010.referente_nf   = 1;
              exception
                when no_data_found then
                   w_qtde_parcela  := 0;
              end;

              /*Soma as retenc?es de PIS/COFINS*/
              begin
                 select sum(cpag_010.valor_pis_imp),   sum(cpag_010.valor_cofins_imp)
                 into   w_valor_pis_ret,               w_valor_cofins_ret
                 from cpag_010
                 where cpag_010.codigo_empresa = obrf_010.local_entrega
                   and cpag_010.documento      = obrf_010.documento
                   and cpag_010.serie          = obrf_010.serie
                   and cpag_010.cgc_9          = obrf_010.cgc_cli_for_9
                   and cpag_010.cgc_4          = obrf_010.cgc_cli_for_4
                   and cpag_010.cgc_2          = obrf_010.cgc_cli_for_2
                   and cpag_010.referente_nf   = 1;
              exception
                 when no_data_found then
                    w_valor_pis_ret := 0.00;
                    w_valor_cofins_ret := 0.00;
              end;
           end if;

            if w_qtde_parcela > 0
            then
               begin
                  select supr_055.vencimento into w_intervalo_parcela from supr_055
                  where supr_055.condicao_pagto = obrf_010.condicao_pagto
                    and rownum = 1;
               exception
               when no_data_found then
                  w_intervalo_parcela  := 0;
               end;
            end if;

            if w_qtde_parcela = 0
            then
               if trunc(p_dat_inicial) >= to_date('01-07-12','dd-mm-yy')
               then
                  w_aprazo_avista := 2; -- sem pagamento
               else
                  w_aprazo_avista := 9;
               end if;
            else
               if w_intervalo_parcela = 0 and w_qtde_parcela = 1
               then
                   w_aprazo_avista := 0; -- avista
               else
                  w_aprazo_avista := 1; -- aprazo
               end if;
            end if;

           if obrf_010.situacao_entrada = 4
           then
              w_cod_especie := obrf_010.especie_docto;
           end if;

           if w_serie_nfe = 'S' and trim(obrf_010.numero_danf_nfe) is not null and obrf_010.tipo_conhecimento = 0 or
              (obrf_010.tipo_conhecimento = 0 and trim(obrf_010.numero_danf_nfe) is not null and obrf_010.situacao_entrada = 4)
           then
              obrf_010.modelo_doc_fisc := '55';
           --else
              if w_cod_especie is not null 
              then
                 begin
                    select trim(obrf_200.modelo_documento)
                      into w_modelo_especie
                    from obrf_200
                    where obrf_200.codigo_especie  = w_cod_especie;
                 EXCEPTION
                    WHEN OTHERS THEN
                       w_modelo_especie := null;
                 END;
              end if;
           else
              if w_cod_especie is not null
              then
                 begin
                    select trim(obrf_200.modelo_documento)
                      into w_modelo_especie
                    from obrf_200
                    where obrf_200.codigo_especie  = w_cod_especie;
                 EXCEPTION
                    WHEN OTHERS THEN
                       w_modelo_especie := null;
                 END;
              end if;
           end if;

           if w_modelo_especie is not null
           then
              if Nvl(Length(w_modelo_especie),0) > 2
              then
                 w_modelo_especie := substr(w_modelo_especie,1,2);
              end if;

              obrf_010.modelo_doc_fisc := w_modelo_especie;
           end if;

           w_ind_nf_consumo := 'N';

           if obrf_010.modelo_doc_fisc = '55' and obrf_010.situacao_entrada = 4 /* PARA NOTAS DE CONSUMO*/
           then
              select tipo_fornecedor into w_tipo_fornecedor
              from supr_010
              where fornecedor9 = obrf_010.cgc_cli_for_9
              and   fornecedor4 = obrf_010.cgc_cli_for_4
              and   fornecedor2 = obrf_010.cgc_cli_for_2;

              if w_tipo_fornecedor = w_tp_forne_elet or w_tipo_fornecedor = w_tp_forne_tele or
                 w_tipo_fornecedor = w_tp_forne_agua or w_tipo_fornecedor = w_tp_forne_gas
              then
                 w_ind_nf_consumo := 'S';
              end if;

           end if;

           if obrf_010.modelo_doc_fisc is null or obrf_010.modelo_doc_fisc = '00'
           then

              if obrf_010.tipo_conhecimento in (1,3)
              then
                 if trim(obrf_010.numero_danf_nfe) is not null
                 then
                    w_modelo_nota := w_modelo_especie;
                 else
                     if obrf_010.via_transporte = 1
                     then
                        w_modelo_nota := '08';
                     else

                        if obrf_010.via_transporte = 2
                        then
                           w_modelo_nota := '10';
                        else

                           if obrf_010.via_transporte = 3 or obrf_010.via_transporte = 5
                           then
                              w_modelo_nota := '09';
                           else

                              if obrf_010.via_transporte = 4
                              then
                                  w_modelo_nota := '11';
                              end if;
                           end if;
                        end if;
                     end if;
                  end if;
              else
                 if w_tipo_fornecedor = w_tp_forne_elet
                 then
                    w_modelo_nota := '06';
                 else

                    if w_tipo_fornecedor = w_tp_forne_tele
                    then
                       w_modelo_nota := '22';
                    else

                       if w_tipo_fornecedor = w_tp_forne_agua
                       then
                          w_modelo_nota := '27';
                       else

                          if w_tipo_fornecedor = w_tp_forne_gas
                          then
                             w_modelo_nota := '28';
                          else
                              w_modelo_nota := '01';
                          end if;
                       end if;
                    end if;
                 end if;
              end if;
           else
              w_modelo_nota := obrf_010.modelo_doc_fisc;
           end if;

           if obrf_010.situacao_entrada = 2
           then
              w_tip_nota_cancel := 'S';
           else
              w_tip_nota_cancel := 'N';
           end if;

           if obrf_010.situacao_entrada <> 2
           then
              /* Se a nota for diferente de 1 (Normal) e n?o estiver cancelada,
              classifica ela como nota complementar */
              if (obrf_010.tipo_nf_referenciada = 1 or obrf_010.tipo_nf_referenciada = 4)
              then
                 w_situacao_entrada := 0;
              else
                 w_situacao_entrada := 6;
              end if;
           else
              if obrf_010.situacao_entrada = 2 and obrf_010.cod_justificativa = 1
              then
                 w_situacao_entrada := 5;
              else
                 w_situacao_entrada := obrf_010.situacao_entrada;
              end if;
           end if;


           BEGIN
              SELECT basi_160.codigo_fiscal,        basi_167.codigo_fiscal_uf,
                     supr_010.inscr_est_forne
              INTO   w_codigo_cidade_ibge,          w_codigo_fiscal_uf,
                     w_insc_est_cliente
              from   basi_160,  basi_167, basi_165, supr_010
              where supr_010.fornecedor9  = obrf_010.cgc_cli_for_9
                and supr_010.fornecedor4  = obrf_010.cgc_cli_for_4
                and supr_010.fornecedor2  = obrf_010.cgc_cli_for_2
                and basi_160.cod_cidade   = supr_010.cod_cidade
                and basi_160.estado       = basi_167.estado
                and basi_160.codigo_pais  = basi_167.codigo_pais
                and basi_160.codigo_pais  = basi_165.codigo_pais;
           EXCEPTION
           WHEN OTHERS THEN
              w_codigo_fiscal_uf   := NULL;
              w_codigo_cidade_ibge := NULL;
              w_insc_est_cliente   := NULL;
           END;

           if w_modelo_nota = '08' or w_modelo_nota = '09' or
              w_modelo_nota = '10' or w_modelo_nota = '11' or
              w_modelo_nota = '57'
           then
             w_num_seq_modelo := 1;
           else
              if w_modelo_nota = '21' or w_modelo_nota = '22'
              then
                 w_num_seq_modelo := 2;
              end if;
           end if;

           if obrf_010.tipo_conhecimento = 1
           then
              obrf_010.tipo_frete := 1;
           else
              if obrf_010.tipo_conhecimento = 3
              then
                 obrf_010.tipo_frete := 2;
              end if;
           end if;

           if obrf_010.ind_nf_servico = 1
           then
              w_modelo_nota := 'A';
           end if;

           begin
              select pedi_080.ind_nf_servico,  pedi_080.cod_mensagem
              into v_ind_nf_servico,           v_cod_mensagem
              from pedi_080
              where pedi_080.natur_operacao = obrf_010.natoper_nat_oper
                and pedi_080.estado_natoper = obrf_010.natoper_est_oper;
           exception
              when no_data_found then
                  v_ind_nf_servico := 0;
                  v_cod_mensagem := 0;
           end;

           for f_nf_imp in (
             select obrf_056.* from obrf_056
             where obrf_056.capa_ent_nrdoc    = obrf_010.documento
               and obrf_056.capa_ent_serie    = obrf_010.serie
               and obrf_056.capa_ent_forcli9  = obrf_010.cgc_cli_for_9
               and obrf_056.capa_ent_forcli4  = obrf_010.cgc_cli_for_4
               and obrf_056.capa_ent_forcli2  = obrf_010.cgc_cli_for_2)
           LOOP
              begin
                 insert into sped_pc_056
                    (cod_matriz
                     ,num_cnpj9_empr
                     ,num_cnpj4_empr
                     ,num_cnpj2_empr
                     ,capa_ent_nrdoc
                     ,capa_ent_serie
                     ,capa_ent_forcli9
                     ,capa_ent_forcli4
                     ,capa_ent_forcli2
                     ,sequencia
                     ,tipo_declaracao
                     ,numero_di
                     ,data_declaracao
                     ,local_desembaraco
                     ,uf_desembaraco
                     ,data_desembaraco
                     ,codigo_exportador
                     ,numero_adicao
                     ,sequencia_adicao
                     ,codigo_fabricante
                     ,valor_desconto_item
                     ,base_calculo_ii
                     ,valor_desp_adu
                     ,valor_ii
                     ,valor_iof
                     ,valor_pis_imp
                     ,valor_cofins_imp
                     ) VALUES
                     (p_cod_matriz
                     ,p_cnpj9_empresa
                     ,p_cnpj4_empresa
                     ,p_cnpj2_empresa
                     ,obrf_010.documento
                     ,obrf_010.serie
                     ,obrf_010.cgc_cli_for_9
                     ,obrf_010.cgc_cli_for_4
                     ,obrf_010.cgc_cli_for_2
                     ,f_nf_imp.sequencia
                     ,f_nf_imp.tipo_declaracao
                     ,REPLACE(REPLACE(f_nf_imp.numero_di,'/'),'-')
                     ,f_nf_imp.data_declaracao
                     ,f_nf_imp.local_desembaraco
                     ,f_nf_imp.uf_desembaraco
                     ,f_nf_imp.data_desembaraco
                     ,f_nf_imp.codigo_exportador
                     ,f_nf_imp.numero_adicao
                     ,f_nf_imp.sequencia_adicao
                     ,f_nf_imp.codigo_fabricante
                     ,f_nf_imp.valor_desconto_item
                     ,f_nf_imp.base_calculo_ii
                     ,f_nf_imp.valor_desp_adu
                     ,f_nf_imp.valor_ii
                     ,f_nf_imp.valor_iof
                     ,f_nf_imp.pis_creditar
                     ,f_nf_imp.cofins_creditar
                     );
              exception
                 WHEN OTHERS THEN
                     p_des_erro := 'Erro na inclus?o da tabela sped_pc_056' || Chr(10) || SQLERRM;
                     RAISE W_ERRO;
              end;
           END LOOP;

           for f_nf_energ in (
             select obrf_019.* from obrf_019
             where obrf_019.cod_empresa    = obrf_010.local_entrega
               and obrf_019.num_nota       = obrf_010.documento
               and obrf_019.cod_serie_nota = obrf_010.serie
               and obrf_019.num_cnpj_9     = obrf_010.cgc_cli_for_9
               and obrf_019.num_cnpj_4     = obrf_010.cgc_cli_for_4
               and obrf_019.num_cnpj_2     = obrf_010.cgc_cli_for_2)
           LOOP

              begin
                 insert into sped_pc_0019
                 (cod_matriz
                 ,cod_empresa
                 ,num_cnpj_empr9
                 ,num_cnpj_empr4
                 ,num_cnpj_empr2
                 ,num_nota
                 ,cod_serie_nota
                 ,num_cnpj_9
                 ,num_cnpj_4
                 ,num_cnpj_2
                 ,tip_classe_consumo
                 ,qtde_consumo_energia
                 ,cod_classif_energia
                 ,tip_prestacao_servico
                 ,dat_inicial_prest_servico
                 ,dat_final_prest_servico
                 ,tp_ligacao
                 ,cod_grupo_tensao
                 ,tipo_assinante)
                 values
                 (p_cod_matriz
                 ,f_nf_energ.cod_empresa
                 ,p_cnpj9_empresa
                 ,p_cnpj4_empresa
                 ,p_cnpj2_empresa
                 ,f_nf_energ.num_nota
                 ,f_nf_energ.cod_serie_nota
                 ,f_nf_energ.num_cnpj_9
                 ,f_nf_energ.num_cnpj_4
                 ,f_nf_energ.num_cnpj_2
                 ,f_nf_energ.tip_classe_consumo
                 ,f_nf_energ.qtde_consumo_energia
                 ,f_nf_energ.cod_classif_energia
                 ,f_nf_energ.tip_prestacao_servico
                 ,f_nf_energ.dat_inicial_prest_servico
                 ,f_nf_energ.dat_final_prest_servico
                 ,f_nf_energ.tp_ligacao
                 ,f_nf_energ.cod_grupo_tensao
                 ,f_nf_energ.tipo_assinante);
              exception
                WHEN OTHERS THEN
                    p_des_erro := 'Erro na inclus?o da tabela sped_pc_056' || Chr(10) || SQLERRM;
                    RAISE W_ERRO;
              end;

           END LOOP;


           BEGIN
              INSERT INTO sped_pc_c100
                 (cod_matriz
                 ,cod_empresa
                 ,num_cnpj9_empr
                 ,num_cnpj4_empr
                 ,num_cnpj2_empr
                 ,num_nota_fiscal
                 ,cod_serie_nota
                 ,num_cnpj_9
                 ,num_cnpj_4
                 ,num_cnpj_2
                 ,tip_entrada_saida
                 ,tip_emissao_prop
                 ,cod_modelo
                 ,cod_situacao_nota
                 ,dat_saida
                 ,dat_emissao
                 ,val_nota
                 ,tip_pagto
                 ,val_descon
                 ,val_itens
                 ,tip_frete
                 ,val_frete
                 ,val_seguro
                 ,val_despesas
                 ,val_base_icms
                 ,val_icms
                 ,val_base_icms_sub
                 ,val_icms_sub
                 ,val_ipi
                 ,val_pis
                 ,val_cofins
                 ,val_pis_sub
                 ,val_cofins_sub
                 ,val_iss
                 ,num_doc_referen
                 ,cod_serie_doc_referen
                 ,sig_estado
                 ,cod_natur_oper
                 ,cod_cidade_ibge
                 ,num_cnpj_9_transp
                 ,num_cnpj_4_transp
                 ,num_cnpj_2_transp
                 ,num_nota_eletric
                 ,num_seq_modelo
                 ,val_pis_ret
                 ,val_cofins_ret
                 ,ind_nf_servico
                 ,cod_mensagem
                 ,tipo_cte
                 ,ind_nf_consumo
                 ,numero_di
                 ) VALUES
                 (p_cod_matriz
                 ,p_cod_empresa
                 ,p_cnpj9_empresa
                 ,p_cnpj4_empresa
                 ,p_cnpj2_empresa
                 ,obrf_010.documento
                 ,trim(obrf_010.serie)
                 ,obrf_010.cgc_cli_for_9
                 ,obrf_010.cgc_cli_for_4
                 ,obrf_010.cgc_cli_for_2
                 ,'E'
                 ,decode(obrf_010.situacao_entrada,4,1,0) -- tip_emissao_prop
                 ,lpad(w_modelo_nota,2,'0')
                 ,w_situacao_entrada
                 ,obrf_010.data_transacao
                 ,obrf_010.data_emissao
                 ,obrf_010.total_docto      -- valor_nota
                 ,w_aprazo_avista           -- avista ou aprazo
                 ,obrf_010.valor_desconto
                 ,obrf_010.valor_itens
                 ,obrf_010.tipo_frete
                 ,obrf_010.valor_frete
                 ,obrf_010.valor_seguro
                 ,obrf_010.valor_despesas
                 ,obrf_010.base_icms
                 ,obrf_010.valor_icms
                 ,obrf_010.base_icms_sub
                 ,obrf_010.valor_icms_sub
                 ,obrf_010.valor_total_ipi
                 ,NULL          -- val_pis
                 ,NULL          -- val_cofins
                 ,0.00          -- val_pis_sub
                 ,000          -- val_cofins_sub
                 ,0.00
                 ,0          -- num_doc_referen
                 ,null          -- cod_serie_doc_referen
                 ,obrf_010.natoper_est_oper
                 ,obrf_010.natoper_nat_oper
                 ,lpad(w_codigo_fiscal_uf,2,0) || lpad(w_codigo_cidade_ibge,5,0)
                 ,obrf_010.transpa_forne9
                 ,obrf_010.transpa_forne4
                 ,obrf_010.transpa_forne2
                 ,trim(obrf_010.numero_danf_nfe)
                 ,w_num_seq_modelo
                 ,w_valor_pis_ret
                 ,w_valor_cofins_ret
                 ,v_ind_nf_servico
                 ,v_cod_mensagem
                 ,obrf_010.tipo_cte
                 ,w_ind_nf_consumo
                 ,REPLACE(REPLACE(obrf_010.numero_di,'/'),'-')
                 );
           EXCEPTION
              WHEN OTHERS THEN
                 p_des_erro := 'Erro na inclus?o da tabela sped_pc_c100 - Entradas ' || Chr(10) || SQLERRM;
                 RAISE W_ERRO;
           END;

           --
           -- Atualiza os itens da nota fiscal - Entradas
           --


           FOR obrf_015 IN u_obrf_015 (obrf_010.local_entrega, obrf_010.documento, obrf_010.serie, obrf_010.cgc_cli_for_9,
                                       obrf_010.cgc_cli_for_4, obrf_010.cgc_cli_for_2)
           LOOP
              w_nota_de_devolucao_compra := 'N';

              begin
                 select pedi_080.cod_natureza,     pedi_080.divisao_natur,
                        pedi_080.consiste_cvf_icms,pedi_080.cod_mensagem,
                        pedi_080.perc_substituica,
                        pedi_080.tem_mov_fisica, pedi_080.cod_cred,
                        pedi_080.cod_cont_crec,  pedi_080.ind_nat_frt,
                        pedi_080.ind_nf_ciap,    pedi_080.red_icms_base_pis_cof
                 into   w_num_cfop,                w_divisao_natureza,
                        w_consiste_cvf_icms,       w_cod_mensagem,
                        w_perc_substituica,
                        w_tipo_transacao,          w_cod_cred,
                        w_cod_cont_cred,           w_ind_nat_frt,
                        w_ind_nf_ciap,             w_red_icms_base_pis_cof
                 from pedi_080
                 where pedi_080.natur_operacao = obrf_015.natitem_nat_oper
                   and pedi_080.estado_natoper = obrf_015.natitem_est_oper;
              exception
                 when no_data_found then
                 w_num_cfop          := '';
                 w_divisao_natureza  := 0;
                 w_consiste_cvf_icms := 1;
                 w_cod_mensagem      := 0;
                 w_tipo_transacao    := 0;
                 w_cod_cred          := 0;
                 w_cod_cont_cred     := 0;
                 w_ind_nat_frt       := 0;
                 w_ind_nf_ciap       := 0;
                 w_red_icms_base_pis_cof := 0;
              end;

              encontrou_nat_empresa := true;

              begin
                 select pedi_081.cod_cred,
                        pedi_081.cod_cont_crec,  pedi_081.ind_nat_frt
                 into   w_cod_cred_p,
                        w_cod_cont_cred_p,         w_ind_nat_frt_p
                 from pedi_081
                 where pedi_081.cod_empresa    = obrf_010.local_entrega
                   and pedi_081.natur_operacao = obrf_015.natitem_nat_oper
                   and pedi_081.estado_natoper = obrf_015.natitem_est_oper;
              exception
                 when no_data_found then
                 w_cod_cred_p          := 0;
                 w_cod_cont_cred_p     := 0;
                 w_ind_nat_frt_p       := 9;
                 encontrou_nat_empresa := false;
              end;

              if w_num_cfop || w_divisao_natureza in ('1.201','1.202','1.203','1.204','1.410','1.411',
                                                       '2.201','2.202','2.203','2.204','2.410','2.411')
              then
                 w_nota_de_devolucao_compra := 'S';
              else
                 w_nota_de_devolucao_compra := 'N';
              end if;

              if encontrou_nat_empresa
              then
                 w_cod_cred       := w_cod_cred_p;
                 w_cod_cont_cred  := w_cod_cont_cred_p;
                 w_ind_nat_frt    := w_ind_nat_frt_p;
              end if;
              
              if obrf_010.valor_desconto > 0
             then
                w_val_desconto := trunc(((obrf_015.valor_total / obrf_010.valor_itens) * obrf_010.valor_desconto),2);
             else
                   w_val_desconto := 0.00;
             end if;
              
              if w_red_icms_base_pis_cof = 1 and obrf_010.data_transacao >= to_date('01-05-2023','dd-mm-yy')
              then
                 w_val_desconto := w_val_desconto + obrf_015.valor_icms;
              end if;

              if obrf_015.cod_vlfiscal_icm >= 30 and obrf_015.cod_vlfiscal_icm <> 70 and obrf_015.cod_vlfiscal_icm <> 51
              then
                 obrf_015.valor_icms := 0.00;
              end if;



             

             if obrf_015.valor_icms = 0.00
             then
                obrf_015.base_calc_icm   := 0.00;
                obrf_015.percentual_icm  := 0.00;
                obrf_015.base_diferenca  := 0.00;
             end if;

             if obrf_015.valor_pis = 0.00 or w_ind_nf_ciap = 2
             then
                obrf_015.base_pis_cofins   := 0.00;
                obrf_015.perc_pis          := 0.00;
                obrf_015.perc_cofins       := 0.00;
                obrf_015.valor_pis         := 0.00;
              end if;

              /* quando for processo igual a zero, soma valor substituc?o no valor do item (variavel e valor item e n?o valor nota) */
        if obrf_010.ind_mudanca_processo = 0
        then
           w_valor_contabil := obrf_015.valor_total + obrf_015.rateio_despesas + obrf_015.valor_ipi + obrf_015.valor_subtituicao;
        else
           w_valor_contabil := obrf_015.valor_total + obrf_015.rateio_despesas + obrf_015.valor_ipi;
              end if;

              if obrf_015.cod_vlfiscal_ipi > 1
              then
                 obrf_015.base_ipi        := 0.00;
                 obrf_015.valor_ipi       := 0.00;
                 obrf_015.percentual_ipi  := 0.00;
              end if;

              w_sequencia := w_sequencia + 1;
              
              begin
                 select trim(basi_240.cod_enquadramento_ipi)
                 into   w_cod_enquadramento_ipi
                 from basi_240
                 where basi_240.classific_fiscal = obrf_015.classific_fiscal;
              exception
              when no_data_found then
                 w_cod_enquadramento_ipi := null;
              end;

              if w_tem_referenciado = 'n'
              then
                 begin
                    select count(*) into w_conta_msg from fatu_052
                    where fatu_052.cod_empresa    = obrf_010.local_entrega
                      and fatu_052.num_nota       = obrf_010.documento
                      and fatu_052.cod_serie_nota = obrf_010.serie
                      and fatu_052.cnpj9          = obrf_010.cgc_cli_for_9
                      and fatu_052.cnpj4          = obrf_010.cgc_cli_for_4
                      and fatu_052.cnpj2          = obrf_010.cgc_cli_for_2
                      and fatu_052.ind_entr_saida = 'E';
                 end;

                 if obrf_015.num_nota_orig > 0 and w_conta_msg = 0
                 then
                    w_tem_referenciado := 's';
                 end if;
              end if;

              obrf_015.classific_fiscal := substr(trim(REPLACE(obrf_015.classific_fiscal,'.')),1,8);

              begin
                 select  sum(obrf_056.pis_creditar),sum(obrf_056.cofins_creditar),
                         REPLACE(REPLACE(obrf_056.numero_di,'/'),'-'),        obrf_056.tipo_declaracao
                 into    w_valor_pis_imp,           w_valor_cofins_imp,
                         w_numero_di,               w_tipo_declaracao
                 from obrf_056, obrf_015 i
                 where obrf_056.capa_ent_nrdoc   = i.capa_ent_nrdoc
                   and obrf_056.capa_ent_serie   = i.capa_ent_serie
                   and obrf_056.capa_ent_forcli9 = i.capa_ent_forcli9
                   and obrf_056.capa_ent_forcli4 = i.capa_ent_forcli4
                   and obrf_056.capa_ent_forcli2 = i.capa_ent_forcli2
                   and obrf_056.sequencia        = i.sequencia
                   and i.valor_unitario          > 0.00
                   and obrf_056.capa_ent_nrdoc   = obrf_010.documento
                   and obrf_056.capa_ent_serie   = obrf_010.serie
                   and obrf_056.capa_ent_forcli9 = obrf_010.cgc_cli_for_9
                   and obrf_056.capa_ent_forcli4 = obrf_010.cgc_cli_for_4
                   and obrf_056.capa_ent_forcli2 = obrf_010.cgc_cli_for_2
                   and i.coditem_nivel99         = obrf_015.coditem_nivel99
                   and i.coditem_grupo           = obrf_015.coditem_grupo
                   and (i.coditem_subgrupo       = obrf_015.coditem_subgrupo or obrf_015.coditem_subgrupo is null)
                   and (i.coditem_item           = obrf_015.coditem_item or obrf_015.coditem_item is null)
                 group by obrf_056.numero_di,  obrf_056.tipo_declaracao;
              exception
              when no_data_found then
                 w_valor_pis_imp       := 0.00;
                 w_valor_cofins_imp    := 0.00;
                 w_numero_di           := ' ';
                 w_tipo_declaracao     := 0;

              WHEN OTHERS THEN
                 p_des_erro := 'Erro na leitura de Dados Importac?o da Nota de Entrada(obrf_056). Verifique: '
                               ||obrf_010.documento || '-' || obrf_010.serie
                               || Chr(10) || SQLERRM;
                 RAISE W_ERRO;

              end;
              -- verifica se o tamanho da classificacao fiscal esta correta, caso nao esteja, gera mensagem de erro

              if obrf_015.coditem_nivel99 = '0' and
                 obrf_015.coditem_grupo = '00000'
              then

                 if substr(obrf_015.classific_fiscal,1,5) is not null
                 then
                    ws_grupo_clas := substr(obrf_015.classific_fiscal,1,5);
                 else
                    ws_grupo_clas := ' ';
                 end if;


                 if substr(obrf_015.classific_fiscal,6,3) is not null
                 then
                    ws_subgru_clas := substr(obrf_015.classific_fiscal,6,3);
                 else
                    ws_subgru_clas := ' ';
                 end if;

                 obrf_015.coditem_grupo    := ws_grupo_clas;
                 obrf_015.coditem_subgrupo := ws_subgru_clas;
                 obrf_015.coditem_nivel99  := ' ';
                 obrf_015.coditem_item     := ' ';
              end if;

              v_exercicio_doc := inter_fn_checa_data(p_cod_matriz, obrf_010.data_transacao, 0);

              v_exercicio := inter_fn_checa_data(p_cod_matriz, obrf_010.data_transacao,0);
              if v_exercicio_doc < 0
              then
                 v_exercicio_doc := v_exercicio;
              end if;

              v_plano_conta := 0;

              v_num_cc             := null;
              v_data_alt           := null;
              v_ind_cta            := null;
              v_nivel_cta          := null;
              v_conta_contabil_ref := null;
              v_desc_conta         := null;
              v_indic_natu_sped    := null;

              if v_exercicio > 0 and v_exercicio_doc > 0
              then
                 v_num_cc := inter_fn_encontra_conta(p_cod_empresa, 3,
                                                     obrf_015.codigo_contabil,
                                                     obrf_015.codigo_transacao,
                                                     obrf_015.centro_custo,
                                                     v_exercicio,
                                                     v_exercicio_doc);

                 begin
                    select cont_500.cod_plano_cta
                    into   v_plano_conta
                    from cont_500
                    where cont_500.cod_empresa  = p_cod_matriz
                      and cont_500.exercicio    = v_exercicio_doc;
                 exception
                    when no_data_found then
                     v_plano_conta := 0;
                 end;

                 if v_plano_conta > 0
                 then
                    begin
                       select cont_535.indic_natu_sped,    cont_535.data_cadastro,
                              cont_535.tipo_conta,         cont_535.nivel,
                              cont_535.conta_contabil,     cont_535.conta_contabil_ref,
                              cont_535.descricao
                       into   v_indic_natu_sped,           v_data_alt,
                              v_ind_cta,                   v_nivel_cta,
                              v_conta_contabil,            v_conta_contabil_ref,
                              v_desc_conta
                       from cont_535
                       where cont_535.cod_plano_cta = v_plano_conta
                         and cont_535.cod_reduzido  = v_num_cc
                         and rownum                 = 1;
                     exception
                        when no_data_found then
                            v_num_cc             := null;
                            v_data_alt           := null;
                            v_ind_cta            := null;
                            v_nivel_cta          := null;
                            v_conta_contabil_ref := null;
                            v_desc_conta         := null;
                            v_indic_natu_sped    := null;
                    end;
                 end if;
              end if;

              if obrf_015.coditem_nivel99 <> '0' and
                 obrf_015.coditem_grupo   <> '00000' 
              then

                 begin
                    select basi_010.classific_fiscal
                    into v_ncm_entrada
                    from basi_010
                    where basi_010.nivel_estrutura   = obrf_015.coditem_nivel99
                      and basi_010.grupo_estrutura   = obrf_015.coditem_grupo
                      and (basi_010.subgru_estrutura = obrf_015.coditem_subgrupo or obrf_015.coditem_subgrupo is null)
                      and (basi_010.item_estrutura   = obrf_015.coditem_item or obrf_015.coditem_item is null)
                      and rownum                     = 1;
                    exception
                     when no_data_found then
                        v_ncm_entrada := ' ';
                 end;
              end if;

              if trim(v_ncm_entrada) is not null
              then
                 obrf_015.classific_fiscal := v_ncm_entrada;
              end if;
              
              --BALENA
              if (obrf_015.estagio_agrupador + obrf_015.estagio_agrupador_simultaneo) > 0
              then
                obrf_015.classific_fiscal := inter_fn_get_classific(obrf_015.coditem_nivel99, obrf_015.coditem_grupo, obrf_015.coditem_subgrupo,
                                          obrf_015.coditem_item, obrf_015.estagio_agrupador, obrf_015.estagio_agrupador_simultaneo);
              end if;

              begin
                 select basi_240.exporta_classif_sped
                 into v_exporta_classif_sped from basi_240
                 where basi_240.classific_fiscal = obrf_015.classific_fiscal
                  and  rownum                    = 1;
              exception
                  when no_data_found then
                     v_exporta_classif_sped := 'S';
              end;

              if v_exporta_classif_sped = 'N' and obrf_015.coditem_nivel99 <> '0' and obrf_015.coditem_grupo   <> '00000'
              then
                 obrf_015.classific_fiscal := '';
              end if;

              

              BEGIN
                 INSERT INTO sped_pc_c170
                   (cod_matriz
                   ,cod_empresa
                   ,num_nota_fiscal
                   ,cod_serie_nota
                   ,seq_item_nota
                   ,num_cnpj_9
                   ,num_cnpj_4
                   ,num_cnpj_2
                   ,tip_entrada_saida
                   ,cod_nivel
                   ,cod_grupo
                   ,cod_subgru
                   ,cod_item
                   ,des_item
                   ,cod_unid_medida
                   ,val_unitario
                   ,val_faturado
                   ,val_contabil
                   ,qtde_item_fatur
                   ,cod_c_custo
                   ,tip_transacao
                   ,val_desconto
                   ,cvf_icms
                   ,cod_nat_oper
                   ,sig_estado_oper
                   ,num_cfop
                   ,val_base_icms
                   ,perc_icms
                   ,val_icms
                   ,val_base_icms_sub_trib
                   ,perc_icms_sub_trib
                   ,val_base_red_sub_trib
                   ,val_icms_sub_trib
                   ,tip_apuracao
                   ,cod_enquadramento_ipi
                   ,val_base_ipi
                   ,perc_ipi
                   ,val_ipi
                   ,cvf_ipi
                   ,val_basi_pis_cofins
                   ,val_pis
                   ,perc_pis
                   ,cvf_pis
                   ,cvf_cofins
                   ,perc_cofins
                   ,val_cofins
                   ,perc_iss
                   ,dat_emissao
                   ,cod_classific_fiscal
                   ,tip_trib_federal
                   ,tip_trib_estadual
                   ,tip_trib_icms
                   ,val_iss
                   ,cod_empresa_refer
                   ,num_nota_refer
                   ,cod_serie_refer
                   ,seq_nota_refer
                   ,num_cnpj9_refer
                   ,num_cnpj4_refer
                   ,num_cnpj2_refer
                   ,tipo_transacao
                   ,cod_mensagem_nat_oper
                   ,cod_transacao
                   ,cod_contabil
                   ,cod_mensagem_fiscal
                   ,cod_crec
                   ,cod_cont_crec
                   ,ind_nat_frt
                   ,cod_doc_imp
                   ,num_doc_imp
                   ,vl_pis_imp
                   ,vl_cofins_imp
                   ,num_acdraw
                   ,cod_modelo
                   ,cod_situacao_nota
                   ,num_conta_contabil
                   ,data_alter_cc
                   ,ind_cta
                   ,nivel_cc
                   ,cod_cta_ref
                   ,nome_cta
                   ,cod_nat_cc
                   ,nota_devolucao_compra
                   ,ind_nf_consumo
                   ,estagio_agrupador
                   ,estagio_agrupador_simultaneo
                   ,seq_operacao_agrupador_insu
                   ) VALUES
                   (p_cod_matriz                       --matriz
                   ,p_cod_empresa                      -- cod_empresa
                   ,obrf_010.documento                          -- num_nota_fiscal
                   ,trim(obrf_010.serie)                        -- cod_serie_nota
                   ,w_sequencia                                 -- seq_item_nota
                   ,obrf_010.cgc_cli_for_9                      --num_cnpj_9
                   ,obrf_010.cgc_cli_for_4                      --num_cnpj_4
                   ,obrf_010.cgc_cli_for_2                      --num_cnpj_2
                   ,'E'                                         --tip_entrada_saida
                   ,obrf_015.coditem_nivel99                    --cod_nivel
                   ,obrf_015.coditem_grupo                      --cod_grupo
                   ,obrf_015.coditem_subgrupo                   --cod_subgru
                   ,obrf_015.coditem_item                       --cod_item
                   ,substr(trim(replace(obrf_015.descricao_item, chr(9), ' '))   || ' ' || inter_fn_descr_blk(obrf_015.estagio_agrupador,obrf_015.estagio_agrupador_simultaneo),1,70)  --des_item
                   ,obrf_015.unidade_medida                     --cod_unid_medida
                   ,decode(obrf_015.quantidade,0.00,0.00,obrf_015.valor_total/obrf_015.quantidade)   --val_unitario
                   ,obrf_015.valor_total                        --val_faturado
                   ,w_valor_contabil                            --val_contabil
                   ,decode(obrf_015.quantidade,0.000,1.000,obrf_015.quantidade)  --qtde_item_fatur
                   ,obrf_015.centro_custo                       --cod_c_custo
                   ,w_tipo_transacao                            --tip_transacao
                   ,w_val_desconto                              --val_desconto
                   ,obrf_015.cod_vlfiscal_icm                   --cvf_icms
                   ,obrf_015.natitem_nat_oper                   --cod_nat_oper
                   ,obrf_015.natitem_est_oper                   --sig_estado_oper
                   ,REPLACE(w_num_cfop,'.') || w_divisao_natureza
                   ,obrf_015.base_calc_icm                      --val_base_icms
                   ,obrf_015.percentual_icm                     --perc_icms
                   ,obrf_015.valor_icms                         --val_icms
                   ,decode(obrf_015.valor_subtituicao,0.00,0.00,obrf_015.base_subtituicao)   --val_base_icms_sub_trib
                   ,obrf_015.perc_subtituicao                   --perc_icms_sub_trib   cra
                   ,obrf_015.base_diferenca                     --val_base_red_sub_trib
                   ,obrf_015.valor_subtituicao                  --val_icms_sub_trib  cra
                   ,NULL                                        --tip_apuracao
                   ,w_cod_enquadramento_ipi                     --cod_enquadramento_ipi
                   ,obrf_015.base_ipi                           --val_base_ipi
                   ,obrf_015.percentual_ipi                     --perc_ipi
                   ,obrf_015.valor_ipi                          --val_ipi
                   ,obrf_015.cvf_ipi_entrada                    --cvf_ipi
                   ,obrf_015.base_pis_cofins                    --val_basi_pis_cofins
                   ,obrf_015.valor_pis                          --val_pis
                   ,obrf_015.perc_pis                           --perc_pis
                   ,obrf_015.cvf_pis                            --cvf_pis
                   ,obrf_015.cvf_cofins                         --cvf_cofins
                   ,obrf_015.perc_cofins                        --perc_cofins
                   ,obrf_015.valor_cofins                       --val_cofins
                   ,0.00                                        --perc_iss
                   ,obrf_010.data_transacao                     --dat_emissao
                   ,REPLACE(obrf_015.classific_fiscal,'.')     --cod_classific_fiscal
                   ,0                                           --tip_trib_federal
                   ,obrf_015.procedencia                        --tip_trib_estadual
                   ,obrf_015.cod_vlfiscal_icm                   --tip_trib_icms
                   ,0.00                                        --val_iss
                   ,obrf_010.local_entrega
                   ,obrf_015.num_nota_orig
                   ,trim(obrf_015.serie_nota_orig)
                   ,null
                   ,decode(obrf_015.num_nota_orig,0,0,obrf_015.capa_ent_forcli9)
                   ,decode(obrf_015.num_nota_orig,0,0,obrf_015.capa_ent_forcli4)
                   ,decode(obrf_015.num_nota_orig,0,0,obrf_015.capa_ent_forcli2)
                   ,w_tipo_transacao
                   ,null
                   ,obrf_015.codigo_transacao
                   ,obrf_015.codigo_contabil
                   ,w_cod_mensagem
                   ,w_cod_cred
                   ,decode(w_atualiza_cod_base_cred,'S',obrf_015.cod_base_cred, w_cod_cont_cred)
                   ,w_ind_nat_frt
                   ,w_tipo_declaracao
                   ,w_numero_di
                   ,w_valor_pis_imp
                   ,w_valor_cofins_imp
                   ,null
                   ,lpad(w_modelo_nota,2,'0')
                   ,w_situacao_entrada
                   ,v_num_cc
                   ,v_data_alt
                   ,v_ind_cta
                   ,v_nivel_cta
                   ,v_conta_contabil_ref
                   ,v_desc_conta
                   ,v_indic_natu_sped
                   ,w_nota_de_devolucao_compra
                   ,w_ind_nf_consumo
                   ,obrf_015.estagio_agrupador
                   ,obrf_015.estagio_agrupador_simultaneo
                   ,nvl(obrf_015.seq_operacao_agrupador_insu,0)
                   );
              EXCEPTION
                 WHEN OTHERS THEN
                    p_des_erro := 'Erro na inclus?o da tabela sped_pc_c170 - Entradas ' || Chr(10) || SQLERRM;
                 RAISE W_ERRO;
              END;

           END LOOP;

           begin
              select sum(sped_pc_c170.val_pis),      sum(sped_pc_c170.val_cofins),
                     sum(sped_pc_c170.val_base_ipi), sum(sped_pc_c170.val_ipi),
                     sum(sped_pc_c170.val_base_icms),sum(sped_pc_c170.val_icms),
                     sum(decode(val_icms_sub_trib,0.00,0.00,val_base_icms_sub_trib)), sum(val_icms_sub_trib),
                     sum(sped_pc_c170.val_basi_pis_cofins)
              into   w_val_total_pis,             w_val_total_cofins,
                     w_val_base_ipi,              w_val_ipi,
                     w_val_base_icms,             w_val_icms,
                     w_val_base_icms_sub_trib,    w_val_icms_sub_trib,
                     w_base_pis_cofins
              from sped_pc_c170
              where sped_pc_c170.cod_empresa     = p_cod_empresa
                and sped_pc_c170.num_nota_fiscal = obrf_010.documento
                and trim(sped_pc_c170.cod_serie_nota)  = trim(obrf_010.serie)
                and sped_pc_c170.num_cnpj_9      = obrf_010.cgc_cli_for_9
                and sped_pc_c170.num_cnpj_4      = obrf_010.cgc_cli_for_4
                and sped_pc_c170.num_cnpj_2      = obrf_010.cgc_cli_for_2;
           exception
           when no_data_found then
                w_val_total_pis    := 0.00;
                w_val_total_cofins := 0.00;
                w_val_base_ipi     := 0.00;
                w_val_ipi          := 0.00;
                w_val_base_icms    := 0.00;
                w_val_icms         := 0.00;
                w_val_base_icms_sub_trib := 0.00;
                w_val_icms_sub_trib      := 0.00;
                w_base_pis_cofins    := 0.00;
           end;


           begin
              update sped_pc_c100
                set sped_pc_c100.val_pis       = w_val_total_pis,
                    sped_pc_c100.val_cofins    = w_val_total_cofins,
                    sped_pc_c100.val_base_pis_cofins = w_base_pis_cofins,
                    sped_pc_c100.val_base_ipi  = w_val_base_ipi,
                    sped_pc_c100.val_ipi       = w_val_ipi,
                    sped_pc_c100.val_base_icms = w_val_base_icms,
                    sped_pc_c100.val_icms      = w_val_icms,
                    sped_pc_c100.val_base_icms_sub = w_val_base_icms_sub_trib,
                    sped_pc_c100.val_icms_sub      = w_val_icms_sub_trib
                where sped_pc_c100.cod_empresa     = p_cod_empresa
                  and sped_pc_c100.num_nota_fiscal = obrf_010.documento
                  and trim(sped_pc_c100.cod_serie_nota)  = trim(obrf_010.serie)
                  and sped_pc_c100.num_cnpj_9      = obrf_010.cgc_cli_for_9
                  and sped_pc_c100.num_cnpj_4      = obrf_010.cgc_cli_for_4
                  and sped_pc_c100.num_cnpj_2      = obrf_010.cgc_cli_for_2;
            EXCEPTION
            WHEN OTHERS THEN
                 p_des_erro := 'Erro na atualizac?o da tabela sped_pc_c100 - Entradas ' || Chr(10) || SQLERRM;
                 RAISE W_ERRO;
            END;

       COMMIT;

   END LOOP;

   COMMIT;
EXCEPTION
   WHEN W_ERRO then
      p_des_erro := 'Erro na procedure p_gera_sped_pc_c100 ' || Chr(10) || p_des_erro;
   WHEN OTHERS THEN
      p_des_erro := 'Outros erros na procedure p_gera_sped_pc_c100 ' || Chr(10) || SQLERRM;
END inter_pr_gera_sped_c100_pc;

/
