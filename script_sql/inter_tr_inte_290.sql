
  CREATE OR REPLACE TRIGGER "INTER_TR_INTE_290" 
before insert or
       update of cod_deposito, onda, tipo_tag_ean
       on inte_290
for each row

begin

   insert into inte_wms_ondas (
      cod_deposito,        onda,
      timestamp_aimportar, tipo_tag_ean
   )
   values (
      :new.cod_deposito,   :new.onda,
      sysdate(),           :new.tipo_tag_ean
   );

end inter_tr_inte_290;

-- ALTER TRIGGER "INTER_TR_INTE_290" ENABLE
 

/

exec inter_pr_recompile;

