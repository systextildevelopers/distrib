CREATE OR REPLACE FUNCTION INTER_FN_NOME_PROGRAMA ( 
        p_sid_usuario   in number)
return varchar2
is
    v_nome_programa     varchar2(60);
    v_usuario_systextil varchar2(60);
    v_client_info       varchar2(60);
    v_module            varchar2(60);
    v_empresa           number;
begin
   v_client_info := sys_context('USERENV', 'CLIENT_INFO');
   v_module := sys_context('USERENV', 'MODULE');
   
   if v_client_info is null then
      begin
         select hdoc_090.programa
         into v_nome_programa
         from hdoc_090
         where hdoc_090.sid       = p_sid_usuario
           and hdoc_090.instancia = userenv('INSTANCE')
           and rownum             = 1
           and hdoc_090.programa  not like '%menu%';
      exception when no_data_found then
         v_nome_programa := 'SQL'; 
      end; 
   else 
      inter_pr_split_module(v_module, v_nome_programa, v_empresa, v_usuario_systextil);
   end if;
   
   return(v_nome_programa); 
end;
 
/ 
exec inter_pr_recompile; 
