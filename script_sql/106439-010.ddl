-- Create table
create table BASI_630
(
  CD_AGRUPADOR NUMBER(4) default 0 not null,
  DS_AGRUPADOR VARCHAR2(20) default '' not null
);

-- Add comments to the columns 
comment on column BASI_630.CD_AGRUPADOR is 'Código agrupador.';
comment on column BASI_630.DS_AGRUPADOR is 'Descrição agrupador.';

-- Create/Recreate primary, unique and foreign key constraints 
alter table BASI_630
  add constraint PK_BASI_630 primary key (CD_AGRUPADOR);

/

exec inter_pr_recompile;
