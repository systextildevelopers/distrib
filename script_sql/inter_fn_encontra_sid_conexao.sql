
  CREATE OR REPLACE FUNCTION "INTER_FN_ENCONTRA_SID_CONEXAO" 
return number 
is
   v_sid_encontrado number;
begin

   begin
      select sid into v_sid_encontrado
      from v_lista_usuario_banco
      where audsid = userenv('SESSIONID');
 
      exception
         when others then
            v_sid_encontrado := -99;
   end;

   return(v_sid_encontrado);
end inter_fn_encontra_sid_conexao;

 

/

exec inter_pr_recompile;

