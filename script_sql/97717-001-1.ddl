alter table SUPR_010 add perm_mult_generico number(1) default 0;
comment on column SUPR_010.perm_mult_generico is 'Indica se o fornecedor permite informar mais de um produto generico para o mesmo produto final.0 nao permite 1 permite';
exec inter_pr_recompile;
/
exit;
