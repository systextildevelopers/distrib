create or replace view inter_vi_basi_020_220 as
select basi_020.basi030_nivel030,
       basi_020.basi030_referenc,
       basi_020.tamanho_ref,
       basi_020.sequencia_tamanho,
       basi_020.tipo_produto,
       basi_020.descr_tam_refer,
       basi_020.artigo_cotas,
       basi_020.grupo_agrupador,
       basi_020.sub_agrupador,
       basi_220.ordem_tamanho,
       basi_220.descr_tamanho,
       basi_220.faixa_etaria,
       basi_220.faixa_etaria_custos,
       basi_020.descr_ing,
       basi_020.descr_esp
from basi_220, basi_020
where basi_220.tamanho_ref = basi_020.tamanho_ref
order by basi_220.ordem_tamanho;
