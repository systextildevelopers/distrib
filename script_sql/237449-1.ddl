CREATE TABLE ESTQ_086 (
CODIGO              NUMBER(9),
ATRIB_MENOR_IMPACTO VARCHAR2(100) NOT NULL,
REQUISITO           VARCHAR2(100),
QUAIS_SAO           VARCHAR2(100),
COMPROVACAO         VARCHAR2(100),
CONSTRAINT PK_ESTQ_084 PRIMARY KEY (CODIGO)
);
COMMENT ON TABLE ESTQ_086 IS 'Requisitos de sustentabilidade';

CREATE TABLE ESTQ_087 (
NIVEL_ESTRUTURA VARCHAR2(1),
GRUPO_ESTRUTURA VARCHAR2(5),
SUB_ESTRUTURA   VARCHAR2(3),
ITEM_ESTRUTURA  VARCHAR2(6),
LOTE_PRODUTO    NUMBER(6),
NOTA_FISCAL     NUMBER(9),
SEQUENCIA       NUMBER(9),
REQUISITO       NUMBER(9),
CONSTRAINT FK_ESTQ_087_080 FOREIGN KEY
    (NIVEL_ESTRUTURA, GRUPO_ESTRUTURA, SUB_ESTRUTURA, ITEM_ESTRUTURA, LOTE_PRODUTO, NOTA_FISCAL, SEQUENCIA)
    REFERENCES ESTQ_080 ON DELETE CASCADE,
CONSTRAINT FK_ESTQ_087_086 FOREIGN KEY (REQUISITO) REFERENCES ESTQ_086 ON DELETE CASCADE,
CONSTRAINT PK_ESTQ_087 PRIMARY KEY
    (NIVEL_ESTRUTURA, GRUPO_ESTRUTURA, SUB_ESTRUTURA, ITEM_ESTRUTURA, LOTE_PRODUTO, NOTA_FISCAL, SEQUENCIA, REQUISITO)
);
COMMENT ON TABLE ESTQ_087 IS 'Relaciona requisitos de sustentabilidade a lotes de produtos';
