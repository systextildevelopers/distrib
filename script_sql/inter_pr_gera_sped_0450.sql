create or replace procedure inter_pr_gera_sped_0450 (p_cod_empresa   IN NUMBER,
                                                     p_cnpj9         IN NUMBER,
                                                     p_cnpj4         IN NUMBER,
                                                     p_cnpj2         IN NUMBER,
                                                     p_junta_empresa IN NUMBER,
                                                     p_data_fim      IN DATE,
                                                     p_des_erro    out varchar2) is
--
-- Finalidade: Gerar a tabela sped_0150 - Clientes e Fornecedores
-- Autor.....: Edson Pio
-- Data......: 24/12/08
--
-- OBSERVA��ES DE NOTAS FISCAIS
--
-- Data    Autor    Observa��es
--

w_ind_achou           varchar2(1);
w_erro                EXCEPTION;
w_cod_mesangem        number(6);
w_descr_mensangem      varchar2(2000);
pNovaApuracao         number(1);


CURSOR u_fatu_052 (p_cod_empresa     NUMBER,
                   p_cnpj9           NUMBER,
                   p_cnpj4           NUMBER,
                   p_cnpj2           NUMBER,
                   p_junta_empresa   NUMBER) IS
   select distinct fatu_052.cod_mensagem, fatu_052.cod_empresa from fatu_052, sped_c100, obrf_874, fatu_504
   where fatu_052.cod_empresa    = sped_c100.cod_empresa
     and fatu_504.codigo_empresa = sped_c100.cod_empresa
     and fatu_052.num_nota       = sped_c100.num_nota_fiscal
     and fatu_052.cod_serie_nota = sped_c100.cod_serie_nota
     and fatu_052.cnpj9          = sped_c100.num_cnpj_9
     and fatu_052.cnpj4          = sped_c100.num_cnpj_4
     and fatu_052.cnpj2          = sped_c100.num_cnpj_2
     and fatu_052.ind_entr_saida = sped_c100.tip_entrada_saida
     and obrf_874.cod_mensagem   = fatu_052.cod_mensagem
     and sped_c100.cod_serie_nota <> 'ECF'
     and sped_c100.cod_serie_nota <> 'CF'
     and fatu_052.cod_mensagem   > 0
     and sped_c100.ind_nf_consumo = 'N'
     and sped_c100.cod_modelo not in ('06','07','08','09','10','11','22','27','28','57')
     and not (fatu_504.tipo_sped = 0 and fatu_504.impr_reg_c110 = 0 and sped_c100.cod_modelo in ('55','65','59') and sped_c100.tip_emissao_prop = 0 and obrf_874.tip_mensagem = 'C')
     and sped_c100.cod_situacao_nota not in (2,3,4,5,6)
     and sped_c100.flag_exp        = 'N'
     and ((fatu_052.cod_empresa in (select fatu_500.codigo_empresa
                                    from   fatu_500
                                    where  fatu_500.cgc_9 = p_cnpj9
                                      and  fatu_500.cgc_4 = p_cnpj4
                                      and  fatu_500.cgc_2 = p_cnpj2
                                      and  p_junta_empresa = 1)
            or  (fatu_052.cod_empresa = p_cod_empresa
            and  p_junta_empresa = 0)
           )
          );

CURSOR u_pedi_080 (p_cod_empresa     NUMBER,
                   p_cnpj9           NUMBER,
                   p_cnpj4           NUMBER,
                   p_cnpj2           NUMBER,
                   p_junta_empresa   NUMBER) IS
   select distinct pedi_080.cod_mensagem, sped_c100.cod_empresa from pedi_080, sped_c170, obrf_874, sped_c100, fatu_504
      where sped_c100.cod_empresa       = sped_c170.cod_empresa
        and fatu_504.codigo_empresa     = sped_c170.cod_empresa
        and sped_c100.num_nota_fiscal   = sped_c170.num_nota_fiscal
        and sped_c100.cod_serie_nota    = sped_c170.cod_serie_nota
        and sped_c100.num_cnpj_9        = sped_c170.num_cnpj_9
        and sped_c100.num_cnpj_4        = sped_c170.num_cnpj_4
        and sped_c100.num_cnpj_2        = sped_c170.num_cnpj_2
        and sped_c170.cod_nat_oper      = pedi_080.natur_operacao
        and sped_c170.sig_estado_oper   = pedi_080.estado_natoper
        and pedi_080.cod_mensagem       > 0
        and obrf_874.cod_mensagem       = pedi_080.cod_mensagem
        and obrf_874.tip_mensagem       = 'F'
        and sped_c100.cod_serie_nota    <> 'ECF'
        and sped_c100.cod_serie_nota    <> 'CF'
        and sped_c100.ind_nf_consumo    = 'N'
        and sped_c100.cod_modelo        not in ('06','07','08','09','10','11','22','27','28','57')
        and not (fatu_504.tipo_sped = 0 and fatu_504.impr_reg_c110 = 0 and sped_c100.cod_modelo   in ('55','65','59') and sped_c100.tip_emissao_prop = 0 and obrf_874.tip_mensagem = 'C')
        and sped_c100.cod_situacao_nota not in (2,3,5,6)
        and sped_c100.flag_exp          = 'N'
        and ((sped_c100.cod_empresa in (select fatu_500.codigo_empresa
                                       from   fatu_500
                                       where  fatu_500.cgc_9 = p_cnpj9
                                         and  fatu_500.cgc_4 = p_cnpj4
                                         and  fatu_500.cgc_2 = p_cnpj2
                                         and  p_junta_empresa = 1)
               or  (sped_c100.cod_empresa = p_cod_empresa
               and  p_junta_empresa = 0)
              )
             );

CURSOR u_obrf_874 (p_cod_mensagem NUMBER) IS
   select obrf_874.cod_mensagem, obrf_874.tip_mensagem,
             substr(trim((obrf_874.des_mensagem1 || '' || obrf_874.des_mensagem2 || '' ||
                   obrf_874.des_mensagem3 || '' || obrf_874.des_mensagem4 || '' ||
                   obrf_874.des_mensagem5 || '' || obrf_874.des_mensagem6 || '' ||
                   obrf_874.des_mensagem7 || '' || obrf_874.des_mensagem8 || '' ||
                   obrf_874.des_mensagem9 || '' || obrf_874.des_mensagem10)),1,255) descr_mensangem
   FROM   obrf_874
   WHERE  obrf_874.cod_mensagem = p_cod_mensagem
     AND  obrf_874.cod_mensagem > 0;

BEGIN

   p_des_erro := NULL;
   
    select count(*) novaApuracao into pNovaApuracao from obrf_820
    where obrf_820.cod_empresa = p_cod_empresa
      and obrf_820.mes = to_char(p_data_fim,'MM')
      and obrf_820.ano = to_char(p_data_fim,'YYYY');
      
   FOR fatu_052 IN u_fatu_052 (p_cod_empresa, p_cnpj9, p_cnpj4, p_cnpj2, p_junta_empresa) LOOP
          w_ind_achou := 'N';

          FOR obrf_874 IN u_obrf_874 (fatu_052.cod_mensagem) LOOP
              w_ind_achou := 'S';


              BEGIN
                 INSERT INTO sped_0450
                    (cod_empresa
                    ,cod_mensagem
                    ,des_mensagem
                    ,tip_mensagem
                    ) values
                    (p_cod_empresa                                               -- cod_empresa
                    ,fatu_052.cod_mensagem                                       -- cod_mensagem
                    ,substr(decode(obrf_874.descr_mensangem,null,'',obrf_874.descr_mensangem),1,255)
                    ,decode(trim(obrf_874.tip_mensagem),null,'C',obrf_874.tip_mensagem)   -- tipo_mensagem
                    );

              EXCEPTION
                 WHEN dup_val_on_index THEN
                    NULL;

                 WHEN OTHERS THEN
                    p_des_erro := 'Erro na inclus�o da tabela sped_0450 ' || Chr(10) ||
                                  ' MENSAGEM: ' || fatu_052.cod_mensagem ||
                                  Chr(10) || SQLERRM;
                    RAISE W_ERRO;
              END;
              COMMIT;

          END LOOP;

          IF  w_ind_achou = 'N' THEN
              inter_pr_insere_erro_sped ('F',p_cod_empresa,
                                  'N�o achou a mensagem da nota fiscal na tabela OBRF_874 ' || Chr(10) ||
                                  '   Mensagem: ' || fatu_052.cod_mensagem);
          END IF;


   END LOOP;

   FOR PEDI_080 IN u_pedi_080 (p_cod_empresa, p_cnpj9, p_cnpj4, p_cnpj2, p_junta_empresa) LOOP
          w_ind_achou := 'N';

          FOR obrf_874 IN u_obrf_874 (pedi_080.cod_mensagem) LOOP
              w_ind_achou := 'S';

              BEGIN
                 INSERT INTO sped_0450
                    (cod_empresa
                    ,cod_mensagem
                    ,des_mensagem
                    ,tip_mensagem
                    ) values
                    (p_cod_empresa                                               -- cod_empresa
                    ,pedi_080.cod_mensagem                                       -- cod_mensagem
                    ,substr(decode(obrf_874.descr_mensangem,null,'',obrf_874.descr_mensangem),1,255)
                    ,decode(trim(obrf_874.tip_mensagem),null,'C',obrf_874.tip_mensagem)   -- tipo_mensagem
                    );

              EXCEPTION
                 WHEN dup_val_on_index THEN
                    NULL;

                 WHEN OTHERS THEN
                    p_des_erro := 'Erro na inclus�o da tabela sped_0450 ' || Chr(10) ||
                                  ' MENSAGEM: ' || pedi_080.cod_mensagem ||
                                  Chr(10) || SQLERRM;
                    RAISE W_ERRO;
              END;
              COMMIT;

          END LOOP;

          IF  w_ind_achou = 'N' THEN
              inter_pr_insere_erro_sped ('F',p_cod_empresa,
                                  'N�o achou a mensagem da nota fiscal na tabela OBRF_874 ' || Chr(10) ||
                                  '   Mensagem: ' || pedi_080.cod_mensagem);
          END IF;


   END LOOP;

   for mensagem_ajuste in (
      select obrf_874.cod_mensagem, obrf_874.tip_mensagem,
             substr(trim((obrf_874.des_mensagem1 || '' || obrf_874.des_mensagem2 || '' ||
                   obrf_874.des_mensagem3 || '' || obrf_874.des_mensagem4 || '' ||
                          obrf_874.des_mensagem5 || '' || obrf_874.des_mensagem6 || '' ||
                          obrf_874.des_mensagem7 || '' || obrf_874.des_mensagem8 || '' ||
                          obrf_874.des_mensagem9 || '' || obrf_874.des_mensagem10)),1,255) descr_mensangem
      from obrf_874
      where (exists (select 1 from obrf_297
                      where obrf_297.cod_empresa    = p_cod_empresa
                        and obrf_297.mes            = to_char(p_data_fim,'MM')
                        and obrf_297.ano            = to_char(p_data_fim,'YYYY')
                        and obrf_297.tipo_recolhimento_icms = 0
                        and obrf_874.cod_mensagem   > 0
                        and obrf_297.num_nota       > 0
                        and trim(obrf_297.cod_ajuste) is not null
                        and Length(trim(obrf_297.cod_ajuste)) <> 8
                        and (obrf_297.cnpj9_nota > 0 or obrf_297.cnpj4_nota > 0 or obrf_297.cnpj2_nota > 0)
                        and obrf_874.cod_mensagem  = obrf_297.cod_mensagem
                        and 0 = pNovaApuracao) 
             or
             exists (select 1 from obrf_823
                         where obrf_823.cod_empresa = p_cod_empresa
                           and obrf_823.mes         = to_char(p_data_fim,'MM')
                           and obrf_823.ano         = to_char(p_data_fim,'YYYY')
                           and (obrf_823.id         in (3,7,12) or obrf_823.reg_c197 = 1)
                           and obrf_823.nota        > 0
                           and obrf_874.cod_mensagem = obrf_823.cod_mensagem
                           and obrf_823.sped_painel = 1
                           and obrf_874.cod_mensagem > 0
                           and trim(obrf_823.cod_ajuste53) is not null
                           and (obrf_823.cnpj9 > 0 or obrf_823.cnpj4 > 0 or obrf_823.cnpj2 > 0)
                           and 1 = pNovaApuracao) )
        
        
        and not exists (select distinct sped_0450.cod_mensagem from sped_0450, obrf_294
                        where sped_0450.cod_empresa  = p_cod_empresa
                          and sped_0450.cod_empresa  = obrf_294.cod_empresa
                          and sped_0450.cod_mensagem = obrf_874.cod_mensagem)
      group by obrf_874.cod_mensagem, obrf_874.tip_mensagem,
             substr(trim((obrf_874.des_mensagem1 || '' || obrf_874.des_mensagem2 || '' ||
                   obrf_874.des_mensagem3 || '' || obrf_874.des_mensagem4 || '' ||
                          obrf_874.des_mensagem5 || '' || obrf_874.des_mensagem6 || '' ||
                          obrf_874.des_mensagem7 || '' || obrf_874.des_mensagem8 || '' ||
                          obrf_874.des_mensagem9 || '' || obrf_874.des_mensagem10)),1,255)
   )
   loop

      BEGIN
         INSERT INTO sped_0450
            (cod_empresa
             ,cod_mensagem
             ,des_mensagem
             ,tip_mensagem
             ) values
             (p_cod_empresa                           -- cod_empresa
              ,mensagem_ajuste.cod_mensagem           -- cod_mensagem
              ,substr(mensagem_ajuste.descr_mensangem,1,255)
              ,decode(trim(mensagem_ajuste.tip_mensagem),null,'C',mensagem_ajuste.tip_mensagem)  -- tipo_mensagem
              );
      EXCEPTION
         WHEN dup_val_on_index THEN
            NULL;

         WHEN OTHERS THEN
            p_des_erro := 'Erro na inclus�o da tabela sped_0450 ' || Chr(10) ||
                          ' MENSAGEM: ' || w_cod_mesangem ||
                          Chr(10) || SQLERRM;
            RAISE W_ERRO;
      END;
   end loop;

   COMMIT;

EXCEPTION
   WHEN W_ERRO then
      p_des_erro := 'Erro na procedure p_gera_sped_0450 ' || Chr(10) || p_des_erro;
   WHEN OTHERS THEN
      p_des_erro := 'Outros erros na procedure p_gera_sped_0450 ' || Chr(10) || SQLERRM;
END inter_pr_gera_sped_0450;
