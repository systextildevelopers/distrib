
  CREATE OR REPLACE TRIGGER "INTER_TR_OBRF_158_LOG" 
after insert or delete or update
on OBRF_158
for each row
declare
   ws_usuario_rede           varchar2(20) ;
   ws_maquina_rede           varchar2(40) ;
   ws_aplicativo             varchar2(20) ;
   ws_sid                    number(9) ;
   ws_empresa                number(3) ;
   ws_usuario_systextil      varchar2(250) ;
   ws_locale_usuario         varchar2(5) ;
   v_nome_programa           varchar2(20) ;


begin
-- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);


   v_nome_programa := inter_fn_nome_programa(ws_sid);  
       
 if inserting
 then
    begin

        insert into OBRF_158_log (
           TIPO_OCORR,   /*0*/
           DATA_OCORR,   /*1*/
           HORA_OCORR,   /*2*/
           USUARIO_REDE,   /*3*/
           MAQUINA_REDE,   /*4*/
           APLICACAO,   /*5*/
           USUARIO_SISTEMA,   /*6*/
           NOME_PROGRAMA,   /*7*/
           cod_empresa_OLD,   /*8*/
           cod_empresa_NEW,   /*9*/
           tipo_calculo_fatu_OLD,   /*10*/
           tipo_calculo_fatu_NEW    /*11*/
        ) values (
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           0,/*8*/
           :new.cod_empresa, /*9*/
           0,/*10*/
           :new.tipo_calculo_fatu /*11*/
         );
    end;
 end if;


 if updating
 then
    begin
        insert into OBRF_158_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           cod_empresa_OLD, /*8*/
           cod_empresa_NEW, /*9*/
           tipo_calculo_fatu_OLD, /*10*/
           tipo_calculo_fatu_NEW  /*11*/
        ) values (
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.cod_empresa,  /*8*/
           :new.cod_empresa, /*9*/
           :old.tipo_calculo_fatu,  /*10*/
           :new.tipo_calculo_fatu  /*11*/
         );
    end;
 end if;


 if deleting
 then
    begin
        insert into OBRF_158_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           cod_empresa_OLD, /*8*/
           cod_empresa_NEW, /*9*/
           tipo_calculo_fatu_OLD, /*10*/
           tipo_calculo_fatu_NEW /*11*/
        ) values (
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede,/*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.cod_empresa, /*8*/
           0, /*9*/
           :old.tipo_calculo_fatu, /*10*/
           0 /*11*/
         );
    end;
 end if;
end inter_tr_OBRF_158_log;
-- ALTER TRIGGER "INTER_TR_OBRF_158_LOG" ENABLE
 

/

exec inter_pr_recompile;

