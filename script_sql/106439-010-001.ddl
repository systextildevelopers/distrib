-- Create table
create table BASI_633
(
  CD_AGRUPADOR NUMBER(4) default 0 not null,
  COL_TABELA   NUMBER(2) default 0,
  MES_TABELA   NUMBER(2) default 0,
  SEQ_TABELA   NUMBER(2) default 0
);

-- Create/Recreate primary, unique and foreign key constraints 
alter table BASI_633
  add constraint REF_BASI_633_BASI_630 foreign key (CD_AGRUPADOR)
  references BASI_630 (CD_AGRUPADOR);
  
alter table BASI_633
  add constraint REF_BASI_633_PEDI_090 foreign key (COL_TABELA, MES_TABELA, SEQ_TABELA)
  references PEDI_090 (COL_TABELA_PRECO, MES_TABELA_PRECO, SEQ_TABELA_PRECO);

/

exec inter_pr_recompile;

