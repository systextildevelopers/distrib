
  CREATE OR REPLACE PROCEDURE "INTER_PR_TMRP_F800" 
 (p_num_aleatorio           in numeric,
  p_data_criacao            in date   ,
  p_deposito_conserto       in numeric,
  p_codigo_usuario          in numeric,
  p_codigo_empresa          in numeric,
  p_periodo_prod_ini        in numeric,
  p_periodo_prod_fim        in numeric,
  p_inc_exc_divprod         in numeric,
  p_inc_exc_divfabr         in numeric,
  p_inc_exc_estagios        in numeric)

IS
  v_data_criacao            date;     /*variavel que recebe a data corrente*/

BEGIN
  /*insere a data atual na variavel v_data_criacao*/
  SELECT sysdate
  INTO   v_data_criacao
  FROM   dual;
  
  /*faz INSERT na OPER_TMP*/
  INSERT /*+APPEND*/ INTO oper_tmp (nr_solicitacao,  nome_relatorio,
                        sequencia,       data_criacao,
                        int_01,          str_01,
                        str_02,          str_03,
                        str_04,          int_02,
                        int_03,          int_04,
                        flo_01,
                        str_05,          str_06,
                        int_05,          int_10,
                        int_11,          flo_10)
  SELECT
          p_num_aleatorio, 'tmrp_f800', rownum, v_data_criacao, p040.periodo_producao, p040.proconf_nivel99,
          p040.proconf_grupo, p040.proconf_subgrupo, p040.proconf_item, p020.alternativa_peca,
          p020.roteiro_peca, p040.codigo_estagio, p040.quantidade,
          '', '', 0, p040.codigo_familia, b030.div_prod,
          INTER_FN_CALC_EM_PROD_PACOTE(p020.periodo_producao, p040.ordem_confeccao,
                                       p040.codigo_estagio)
  FROM
        (SELECT /*+parallel(pcpc_040, 100)*/periodo_producao, proconf_nivel99, proconf_grupo, proconf_subgrupo, proconf_item, codigo_estagio,
                codigo_familia, (qtde_pecas_prog - qtde_pecas_prod - qtde_pecas_2a) quantidade,
                ordem_confeccao, qtde_pecas_prod, ordem_producao
         FROM   pcpc_040) p040,
        (SELECT /*+parallel(basi_030, 100)*/ nivel_estrutura, referencia, div_prod
         FROM   basi_030) b030,
        (SELECT /*+parallel(pcpc_020, 100)*/ ordem_producao, alternativa_peca, roteiro_peca, cod_cancelamento, ultimo_estagio, periodo_producao
         FROM   pcpc_020
         WHERE  (periodo_producao between p_periodo_prod_ini and p_periodo_prod_fim or p_periodo_prod_ini = 99999)
         AND    cod_cancelamento = 0) p020
  WHERE  p040.ordem_producao     = p020.ordem_producao
  AND    p040.proconf_nivel99    = b030.nivel_estrutura
  AND    p040.proconf_grupo      = b030.referencia
  AND    p040.qtde_pecas_prod    = 0
  AND    INTER_FN_CALC_EM_PROD_PACOTE(p020.periodo_producao, p040.ordem_confeccao,
                                       p040.codigo_estagio) != 0
/*  AND    ((p_inc_exc_divprod = 1
  AND EXISTS (SELECT \*+parallel(rcnb_060, 100)*\ rcnb_060.item_estrutura
              FROM   rcnb_060
              WHERE  rcnb_060.tipo_registro        = 595
              AND    rcnb_060.nr_solicitacao       = 800
              AND    rcnb_060.subgru_estrutura     = p_codigo_usuario
              AND    rcnb_060.grupo_estrutura      = p_codigo_empresa
              AND    item_estrutura                = p040.codigo_familia)) */
/*  OR (p_inc_exc_divprod = 2
  AND NOT EXISTS (SELECT \*+parallel(rcnb_060, 100)*\ rcnb_060.item_estrutura
                  FROM   rcnb_060
                  WHERE  rcnb_060.tipo_registro    = 595
                  AND    rcnb_060.nr_solicitacao   = 800
                  AND    rcnb_060.subgru_estrutura = p_codigo_usuario
                  AND    rcnb_060.grupo_estrutura  = p_codigo_empresa
                  AND    item_estrutura            = p040.codigo_familia)))*/
/*  AND ((p_inc_exc_divfabr = 1
  AND EXISTS (SELECT \*+parallel(rcnb_060, 100)*\ rcnb_060.item_estrutura
              FROM   rcnb_060
              WHERE  rcnb_060.tipo_registro        = 597
              AND    rcnb_060.nr_solicitacao       = 800
              AND    rcnb_060.subgru_estrutura     = p_codigo_usuario
              AND    rcnb_060.grupo_estrutura      = p_codigo_empresa
              AND    item_estrutura                = b030.div_prod))*/
/*  OR (p_inc_exc_divfabr = 2
  AND NOT EXISTS (SELECT \*+parallel(rcnb_060, 100)*\ rcnb_060.item_estrutura
                  FROM   rcnb_060
                  WHERE  rcnb_060.tipo_registro    = 597
                  AND    rcnb_060.nr_solicitacao   = 800
                  AND    rcnb_060.subgru_estrutura = p_codigo_usuario
                  AND    rcnb_060.grupo_estrutura  = p_codigo_empresa
                  AND    item_estrutura            = b030.div_prod)))*/
  AND ((p_inc_exc_estagios = 1
  AND EXISTS (SELECT /*+parallel(rcnb_060, 100)*/ rcnb_060.grupo_estrutura
              FROM   rcnb_060
              WHERE  rcnb_060.tipo_registro        = 270
              AND    rcnb_060.nr_solicitacao       = 800
              AND    rcnb_060.subgru_estrutura     = p_codigo_usuario
              AND    rcnb_060.empresa_rel          = p_codigo_empresa
              AND    grupo_estrutura               = p040.codigo_estagio))
  OR (p_inc_exc_estagios = 2
  AND NOT EXISTS (SELECT /*+parallel(rcnb_060, 100)*/ rcnb_060.grupo_estrutura
                  FROM   rcnb_060
                  WHERE  rcnb_060.tipo_registro    = 270
                  AND    rcnb_060.nr_solicitacao   = 800
                  AND    rcnb_060.subgru_estrutura = p_codigo_usuario
                  AND    rcnb_060.empresa_rel      = p_codigo_empresa
                  AND    grupo_estrutura           = p040.codigo_estagio)));
  COMMIT;
END INTER_PR_TMRP_F800;

 

/

exec inter_pr_recompile;

