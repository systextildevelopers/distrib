create table obrf_275 (
	num_nota 				number(9) 		default 0, 
	serie_nota 				varchar2(3)     default ' ',
	cnpj_9_nota             number(9) 		default 0,
	cnpj_4_nota             number(4) 		default 0,
	cnpj_2_nota             number(2) 		default 0,
	seq_nota                number(9) 		default 0,
	cod_empr_nota_venda 	number(9) 		default 0,
	num_nota_venda 			number(9) 		default 0,
	serie_nota_venda 		varchar2(3)     default ' ',
	seq_nota_venda 			number(9) 		default 0,
	nivel 					varchar2(1)     default ' ',
	grupo 					varchar2(5)     default ' ',
	subgrupo 				varchar2(3)     default ' ',
	item 					varchar2(6)     default ' ',
	val_unitario            number(19,5)    default 0.0,
	qtde_venda 				number(15,3)    default 0.0,
	qtde_devol 				number(15,3)    default 0.0,
	qtde_ja_devol 			number(15,3)    default 0.0,
	qtde_saldo_a_devol 		number(15,3)    default 0.0,
	selecionado             varchar2(1)     default 'N'
);

alter table obrf_275 add constraint pk_obrf_275 primary key(num_nota,serie_nota,cnpj_9_nota,cnpj_4_nota,cnpj_2_nota,seq_nota,cod_empr_nota_venda,num_nota_venda,serie_nota_venda,seq_nota_venda);
