INSERT INTO EMPR_007 (PARAM, TIPO, LABEL, FYI_MESSAGE, DEFAULT_STR, DEFAULT_INT, DEFAULT_DBL, DEFAULT_DAT)
VALUES ('contas.a.pagar.permiteBaixaHistZero', 1, 'lb51068', ' ', '', 1, null, null);

COMMIT;


declare tmpInt number;

cursor parametro_c is select codigo_empresa from fatu_500;

begin
  for reg_parametro in parametro_c
  loop
	select count(*) into tmpInt
    from empr_008
    where empr_008.codigo_empresa = reg_parametro.codigo_empresa
      and empr_008.param = 'contas.a.pagar.permiteBaixaHistZero';
    if(tmpInt = 0)
    then
      begin
        insert into empr_008 ( codigo_empresa, param, val_int) 
            values (reg_parametro.codigo_empresa, 'contas.a.pagar.permiteBaixaHistZero', 1);
      end;
    end if;

  end loop;
  commit;
end;
/
