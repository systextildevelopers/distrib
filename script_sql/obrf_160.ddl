-- Create table
create table OBRF_160
(
  codigo_empresa     NUMBER(3) default 0 not null,
  num_nota_fiscal    NUMBER(9) default 0 not null,
  serie_nota_fiscal  VARCHAR2(3) default '' not null,
  cnpj9              NUMBER(9) default 0 not null,
  cnpj4              NUMBER(4) default 0 not null,
  cnpj2              NUMBER(2) default 0 not null,
  nota_entrada_saida VARCHAR2(1) default '' not null,
  arquivo_xml        BLOB,
  numero_danfe_nfe   VARCHAR2(60) default '',
  tipo               CHAR(1) default 'E' not null
);
-- Add comments to the table 
comment on table OBRF_160
  is 'Armazena o XML da NFE';
-- Add comments to the columns 
comment on column OBRF_160.codigo_empresa
  is 'Codigo da empresa da nota fiscal eletronica';
comment on column OBRF_160.num_nota_fiscal
  is 'Numero da nota fiscal eletronica';
comment on column OBRF_160.serie_nota_fiscal
  is 'Serie da nota fiscal eletronica';
comment on column OBRF_160.cnpj9
  is 'Codigo CNPJ/CPF do cliente/fornecedor NF-e';
comment on column OBRF_160.cnpj4
  is 'Codigo CNPJ/CPF do cliente/fornecedor NF-e';
comment on column OBRF_160.cnpj2
  is 'Codigo CNPJ/CPF do cliente/fornecedor NF-e';
comment on column OBRF_160.nota_entrada_saida
  is 'Tipo de operacao, NF-e de entrada ou saida';
comment on column OBRF_160.arquivo_xml
  is 'Arquivo .XML da NF-e';
comment on column OBRF_160.numero_danfe_nfe
  is 'Chave de acesso DANFE da NF-e';
comment on column OBRF_160.tipo
  is 'Indica se a nota e E - Envio, C - Cancelamento ou I - Inutilizada';
-- Create/Recreate primary, unique and foreign key constraints 
alter table OBRF_160
  add constraint PK_OBRF_160 primary key (CODIGO_EMPRESA, NUM_NOTA_FISCAL, SERIE_NOTA_FISCAL, CNPJ9, CNPJ4, CNPJ2, NOTA_ENTRADA_SAIDA, TIPO);

