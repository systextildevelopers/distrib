INSERT INTO hdoc_035
(	codigo_programa, programa_menu, 
	item_menu_def,   descricao)
VALUES
(	'inte_p112',     0,
	1,	             'Conferência Rolos Prep x Conf');

INSERT INTO hdoc_033
(	usu_prg_cdusu, usu_prg_empr_usu, 
	programa,      nome_menu, 
	item_menu,     ordem_menu, 
	incluir,       modificar, 
	excluir,       procurar)
VALUES
(	'INTERSYS',    3, 
	'inte_p112',   'Nenhum', 
	1,             1, 
	'S',           'S', 
	'S',           'S');
	
INSERT INTO hdoc_033
(	usu_prg_cdusu,  usu_prg_empr_usu, 
	programa,       nome_menu, 
	item_menu,      ordem_menu, 
	incluir,        modificar, 
	excluir,        procurar)
VALUES
(	'TREINAMENTO',  3, 
	'inte_p112',    'Nenhum', 
	1,              1, 
	'S',            'S', 
	'S',            'S');

UPDATE hdoc_036
   SET hdoc_036.descricao       = 'Conferência Rolos Prep x Conf'
 WHERE hdoc_036.codigo_programa = 'inte_p112'
   AND hdoc_036.locale          = 'es_ES';
COMMIT;

/
EXEC inter_pr_recompile;
