
  CREATE OR REPLACE TRIGGER "INTER_TR_PEDI_135_LOG" 
after insert or delete or update
       of codigo_situacao, data_situacao, flag_liberacao,
       data_liberacao,     responsavel,   usuario_bloqueio

on pedi_135
for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(100);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   ws_descr_bloqueio         varchar(60);
   ws_descr_situacao         varchar(30);
   ws_descr_situacao_old     varchar(30);
   ws_data_situacao          varchar2(10);
   ws_data_liberacao         varchar2(10);
   v_executa_trigger         number(1);
begin
   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;
   
   if v_executa_trigger = 0
   then

      if inserting
      then
         begin
            select hdoc_120.descricao into ws_descr_bloqueio
            from hdoc_120
            where hdoc_120.cod_bloqueio = :new.codigo_situacao;
         end;

         if :new.flag_liberacao = 'N'
         then
            ws_descr_situacao := inter_fn_buscar_tag('lb12178#BLOQUEADO',ws_locale_usuario,ws_usuario_systextil);
         end if;

         if :new.flag_liberacao = 'M'
         then
             ws_descr_situacao := inter_fn_buscar_tag('lb34890#LIBERADO MANUALMENTE',ws_locale_usuario,ws_usuario_systextil);
         end if;

         if :new.flag_liberacao = 'A'
         then
             ws_descr_situacao := inter_fn_buscar_tag('lb34891#LIBERADO AUTOMATICAMENTE',ws_locale_usuario,ws_usuario_systextil);
         end if;

         begin
            ws_data_situacao := to_char(:new.data_situacao, 'DD/MM/YYYY');

         exception
            when others then
               ws_data_situacao := '  ';
         end;

         

         INSERT INTO hist_100
            ( tabela,            operacao,
              data_ocorr,        usuario_rede,
              maquina_rede,      aplicacao,
              num01,             long01
            )
         VALUES
            ( 'PEDI_135',        'I',
              sysdate,           ws_usuario_rede,
              ws_maquina_rede,   ws_aplicativo,
              :new.pedido_venda,
              '                               ' ||
              inter_fn_buscar_tag('lb34892#BLOQUEIOS DO PEDIDO DE VENDAS',ws_locale_usuario,ws_usuario_systextil) ||
                                     chr(10)               ||
                                     chr(10)               ||
              inter_fn_buscar_tag('lb06762#SEQUENCIA.:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                  || :new.seq_situacao     ||
                                      chr(10)              ||
              inter_fn_buscar_tag('lb06073#MOTIVO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                  || :new.codigo_situacao  || ' - '
                                  ||  ws_descr_bloqueio    ||
                                      chr(10)              ||
              inter_fn_buscar_tag('lb16559#DATA BLOQUEIO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                  || ws_data_situacao      ||
                                      chr(10)              ||
              inter_fn_buscar_tag('lb00303#SITUACAO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                  || :new.flag_liberacao   || ' - '
                                  || ws_descr_situacao     ||
                                      chr(10)              ||
              inter_fn_buscar_tag('lb31305#RESPONSAVEL:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                  || :new.responsavel
            );
      end if;

      if updating and
         (:new.codigo_situacao  <> :old.codigo_situacao or
          :new.data_situacao    <> :old.data_situacao   or
          :new.flag_liberacao   <> :old.flag_liberacao  or
          :new.data_liberacao   <> :old.data_liberacao  or
          :new.responsavel      <> :old.responsavel)
      then
         begin
            select hdoc_120.descricao into ws_descr_bloqueio
            from hdoc_120
            where hdoc_120.cod_bloqueio = :new.codigo_situacao;
         end;

         if :new.flag_liberacao = 'N'
         then
            ws_descr_situacao := inter_fn_buscar_tag('lb12178#BLOQUEADO',ws_locale_usuario,ws_usuario_systextil);
         end if;

         if :new.flag_liberacao = 'M'
         then
             ws_descr_situacao := inter_fn_buscar_tag('lb34890#LIBERADO MANUALMENTE',ws_locale_usuario,ws_usuario_systextil);
         end if;

         if :new.flag_liberacao = 'A'
         then
             ws_descr_situacao := inter_fn_buscar_tag('lb34891#LIBERADO AUTOMATICAMENTE',ws_locale_usuario,ws_usuario_systextil);
         end if;

         if :old.flag_liberacao = 'N'
         then
            ws_descr_situacao_old := inter_fn_buscar_tag('lb12178#BLOQUEADO',ws_locale_usuario,ws_usuario_systextil);
         end if;

         if :old.flag_liberacao = 'M'
         then
             ws_descr_situacao_old := inter_fn_buscar_tag('lb34890#LIBERADO MANUALMENTE',ws_locale_usuario,ws_usuario_systextil);
         end if;

         if :old.flag_liberacao = 'A'
         then
             ws_descr_situacao_old := inter_fn_buscar_tag('lb34891#LIBERADO AUTOMATICAMENTE',ws_locale_usuario,ws_usuario_systextil);
         end if;


         begin
            ws_data_situacao  := to_char(:new.data_situacao, 'DD/MM/YYYY');
            ws_data_liberacao := to_char(:new.data_liberacao, 'DD/MM/YYYY');

         exception
            when others then
               ws_data_situacao  := '  ';
               ws_data_liberacao := '  ';
         end;

         INSERT INTO hist_100
           ( tabela,            operacao,
             data_ocorr,        usuario_rede,
             maquina_rede,      aplicacao,
             num01,             long01
           )
        VALUES
           ( 'PEDI_135',        'A',
             sysdate,           ws_usuario_rede,
             ws_maquina_rede,   ws_aplicativo,
             :new.pedido_venda,
             '                               ' ||
             inter_fn_buscar_tag('lb34892#BLOQUEIOS DO PEDIDO DE VENDAS',ws_locale_usuario,ws_usuario_systextil) ||
                                    chr(10)                ||
                                    chr(10)                ||
              inter_fn_buscar_tag('lb06762#SEQUENCIA.:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                  || :new.seq_situacao     ||
                                      chr(10)              ||
              inter_fn_buscar_tag('lb06073#MOTIVO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                  || :new.codigo_situacao  || ' - '
                                  ||  ws_descr_bloqueio    ||
                                      chr(10)              ||
              inter_fn_buscar_tag('lb00303#SITUACAO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                  || :old.flag_liberacao   || ' - '
                                  || ws_descr_situacao_old || ' -> '
                                  || :new.flag_liberacao   || ' - '
                                  || ws_descr_situacao     ||
                                      chr(10)              ||
              inter_fn_buscar_tag('lb02957#DATA LIBERACAO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                  || ws_data_liberacao     ||
                                      chr(10)              ||
              inter_fn_buscar_tag('lb31305#RESPONSAVEL:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                  || :old.responsavel      || ' -> '
                                  || :new.responsavel
           );

      end if;

      if deleting
      then
         begin
            select hdoc_120.descricao into ws_descr_bloqueio
            from hdoc_120
            where hdoc_120.cod_bloqueio = :old.codigo_situacao;
         end;

         if :old.flag_liberacao = 'N'
         then
            ws_descr_situacao := inter_fn_buscar_tag('lb12178#BLOQUEADO',ws_locale_usuario,ws_usuario_systextil);
         end if;

         if :old.flag_liberacao = 'M'
         then
             ws_descr_situacao := inter_fn_buscar_tag('lb34890#LIBERADO MANUALMENTE',ws_locale_usuario,ws_usuario_systextil);
         end if;

         if :old.flag_liberacao = 'A'
         then
             ws_descr_situacao := inter_fn_buscar_tag('lb34891#LIBERADO AUTOMATICAMENTE',ws_locale_usuario,ws_usuario_systextil);
         end if;


         begin
            ws_data_situacao  := to_char(:old.data_situacao, 'DD/MM/YYYY');
            ws_data_liberacao := to_char(:old.data_liberacao, 'DD/MM/YYYY');

         exception
            when others then
               ws_data_situacao  := '  ';
               ws_data_liberacao := '  ';
         end;


         INSERT INTO hist_100
            ( tabela,            operacao,
              data_ocorr,        usuario_rede,
              maquina_rede,      aplicacao,
              num01,             long01
            )
         VALUES
            ( 'PEDI_135',        'D',
              sysdate,           ws_usuario_rede,
              ws_maquina_rede,   ws_aplicativo,
              :old.pedido_venda,
              '                                ' ||
              inter_fn_buscar_tag('lb34892#BLOQUEIOS DO PEDIDO DE VENDAS',ws_locale_usuario,ws_usuario_systextil) ||
                                     chr(10)               ||
                                     chr(10)               ||
              inter_fn_buscar_tag('lb06762#SEQUENCIA.:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                  || :old.seq_situacao     ||
                                      chr(10)              ||
              inter_fn_buscar_tag('lb06073#MOTIVO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                  || :old.codigo_situacao  || ' - '
                                  ||  ws_descr_bloqueio    ||
                                      chr(10)              ||
              inter_fn_buscar_tag('lb16559#DATA BLOQUEIO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                  || ws_data_situacao      ||
                                      chr(10)              ||
              inter_fn_buscar_tag('lb00303#SITUACAO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                  || :old.flag_liberacao   || ' - '
                                  || ws_descr_situacao     ||
                                      chr(10)              ||
              inter_fn_buscar_tag('lb02957#DATA LIBERACAO:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                  || ws_data_liberacao     ||
                                      chr(10)              ||
              inter_fn_buscar_tag('lb31305#RESPONSAVEL:',ws_locale_usuario,ws_usuario_systextil) || ' '
                                  || :old.responsavel
            );
      end if;

   end if;

end inter_tr_pedi_135_log;

-- ALTER TRIGGER "INTER_TR_PEDI_135_LOG" ENABLE
 

/

exec inter_pr_recompile;

