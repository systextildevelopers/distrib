alter table obrf_015 
add (pDIF51      number(9,2));

comment on column obrf_015.pDIF51
is 'CST 51 - Percentual diferimento. No caso de diferimento total, informar 100 no percentual de diferimento';

alter table obrf_015 
modify ( pDIF51      default 0.00);

/

exec inter_pr_recompile;
/
