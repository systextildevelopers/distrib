create or replace package ST_PCK_PAIS as

    TYPE t_dados_basi_165 IS TABLE OF basi_165%rowtype;

    function get_dados_codigo_pais(p_codigo_pais number, p_msg_erro in out varchar2) return t_dados_basi_165;

end;
