
  CREATE OR REPLACE TRIGGER "INTER_TR_USUARIO_ESTQ_043" 
before insert or update on estq_043
for each row
declare

   v_usuario_rede      varchar(20);
   v_maquina_rede      varchar(40);
   v_aplicativo        varchar(20);
   v_usuario_systextil hdoc_030.usuario%TYPE;
   v_sid               sys.gv_$session.sid%type;
   v_empresa           fatu_500.codigo_empresa%TYPE;
   v_locale_usuario    hdoc_030.locale%TYPE;

begin

    inter_pr_dados_usuario (v_usuario_rede,      v_maquina_rede,
                            v_aplicativo,        v_sid,
                            v_usuario_systextil, v_empresa,
                            v_locale_usuario);

    :new.usuario_systextil := v_usuario_systextil;
    :new.usuario_rede      := v_usuario_rede;
    :new.maquina_rede      := v_maquina_rede;
    :new.data_operacao     := sysdate();
    :new.programa          := inter_fn_nome_programa(v_sid);

end inter_tr_usuario_estq_043;

-- ALTER TRIGGER "INTER_TR_USUARIO_ESTQ_043" ENABLE
 

/

exec inter_pr_recompile;

