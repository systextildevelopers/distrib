CREATE OR REPLACE TRIGGER "INTER_TR_MANU_001_LOG" 
   after insert or delete or update
       of data_solicitacao, hora_solicitacao, motivo_manu, obs_manutencao, responsavel

   on manu_001
   for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   long_aux                  varchar2(2000);
   data_hora                 varchar2(20);
   v_descricao               varchar2(60);

  v_executa_trigger      number;

begin

   -- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   if inserting
   then
      if :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if updating
   then
      if :old.executa_trigger = 1 or :new.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if deleting
   then
      if :old.executa_trigger = 1
      then v_executa_trigger := 1;
      else v_executa_trigger := 0;
      end if;
   end if;

   if v_executa_trigger = 0
   then

      if inserting
      then
         data_hora  := :new.data_solicitacao   || '/' ||
                       to_char(:new.hora_solicitacao,'HH24:MI');

         INSERT INTO hist_100
            ( tabela,                 operacao,
              data_ocorr,             aplicacao,
              usuario_rede,           maquina_rede,
              num01,
              str04,                  long01
            )
         VALUES
            ( 'MANU_001',             'I',
              sysdate,                ws_aplicativo,
              ws_usuario_rede,        ws_maquina_rede,
              :new.solicitacao,
              data_hora,
              '                           ' ||
              inter_fn_buscar_tag('lb10685#SOLICITACAO DE MANUTENCAO',ws_locale_usuario,ws_usuario_systextil) ||
                                       chr(10)               ||
                                       chr(10)               ||
              inter_fn_buscar_tag('lb34848#DATA / HORA:',ws_locale_usuario,ws_usuario_systextil) ||
                                      :new.data_solicitacao  || '/' ||
                                      to_char(:new.hora_solicitacao,'HH24:MI'));
      end if;

      if updating and
         (:old.data_solicitacao     <>     :new.data_solicitacao    or
          :old.hora_solicitacao     <>     :new.hora_solicitacao    or
		  :old.responsavel          <>     :new.responsavel
         )
      then
         data_hora  := :new.data_solicitacao   || '/' ||
                       to_char(:new.hora_solicitacao,'HH24:MI');

         long_aux := long_aux ||
                    '                     ' ||
                    inter_fn_buscar_tag('lb10685#SOLICITACAO DE MANUTENCAO',ws_locale_usuario,ws_usuario_systextil) ||
                     chr(10)                                             ||
                     chr(10)                                             ||
                    inter_fn_buscar_tag('lb31314#SOLICITACAO:',ws_locale_usuario,ws_usuario_systextil) || :old.solicitacao ||
                     chr(10)                                          ||
                     chr(10)                                          ||
                     '                                        '||
                    inter_fn_buscar_tag('lb34849#ALTERACOES',ws_locale_usuario,ws_usuario_systextil) ||
                     chr(10)                                          ||
                     chr(10);

         if :old.data_solicitacao <> :new.data_solicitacao
         then
            long_aux := long_aux ||
                    inter_fn_buscar_tag('lb34850#DATA SOLICITACAO:',ws_locale_usuario,ws_usuario_systextil) ||
                                              :old.data_solicitacao || ' -> ' ||
                                              :new.data_solicitacao ||
                                              chr(10);
         end if;

         if :old.hora_solicitacao <> :new.hora_solicitacao
         then
            long_aux := long_aux ||
                    inter_fn_buscar_tag('lb34851#HORA SOLICITACAO:',ws_locale_usuario,ws_usuario_systextil) ||
                                              to_char(:old.hora_solicitacao,'HH24:MI') || ' -> ' ||
                                              to_char(:new.hora_solicitacao,'HH24:MI') ||
                                              chr(10);
         end if;

         if :old.responsavel <> :new.responsavel
         then
            long_aux := long_aux ||
                    inter_fn_buscar_tag('lb06210#RESPONSAVEL:',ws_locale_usuario,ws_usuario_systextil) ||
                                              to_char(:old.responsavel) || ' -> ' ||
                                              to_char(:new.responsavel) ||
                                              chr(10);
         end if;

         begin
            select descricao into v_descricao
            from hdoc_001
            where hdoc_001.tipo   = 966
              and hdoc_001.codigo = :new.motivo_manu;
         exception when others then v_descricao := '';
         end;


            long_aux := long_aux ||
                    inter_fn_buscar_tag('lb34852#MOTIVO MANUTENCAO:',ws_locale_usuario,ws_usuario_systextil) ||
                                              :new.motivo_manu || ' - ' ||
                                              v_descricao ||
                                              chr(10);
            long_aux := long_aux ||
                    inter_fn_buscar_tag('lb34853#OBS. MANUTENCAO:',ws_locale_usuario,ws_usuario_systextil) ||
                                              :new.obs_manutencao ||
                                              chr(10);

         INSERT INTO hist_100
            ( tabela,                 operacao,
              data_ocorr,             aplicacao,
              usuario_rede,           maquina_rede,
              num01,
              str04,                  long01
            )
         VALUES
            ( 'MANU_001',             'A',
              sysdate,                ws_aplicativo,
              ws_usuario_rede,        ws_maquina_rede,
              :new.solicitacao,
              data_hora,                long_aux
           );
      end if;

      if deleting
      then
         data_hora  := :new.data_solicitacao   || '/' ||
                       to_char(:new.hora_solicitacao,'HH24:MI');


         INSERT INTO hist_100
            ( tabela,                 operacao,
              data_ocorr,             aplicacao,
              usuario_rede,           maquina_rede,
              num01,
              str04
            )
         VALUES
            ( 'MANU_001',             'D',
              sysdate,                ws_aplicativo,
              ws_usuario_rede,        ws_maquina_rede,
              :old.solicitacao,
              data_hora
           );
      end if;
   end if;
end inter_tr_manu_001_log;

-- ALTER TRIGGER "INTER_TR_MANU_001_LOG" ENABLE
 

/

exec inter_pr_recompile;

