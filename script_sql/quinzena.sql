
  CREATE OR REPLACE FUNCTION "QUINZENA" 
        (data_informada  in date)
RETURN varchar2
IS
   quinzena_retorna number;
   dia_do_mes       number;
begin
   select nvl(to_char(data_informada,'DD'),0) into dia_do_mes from dual;

   if dia_do_mes < 16 and dia_do_mes > 0
   then
      quinzena_retorna:=1;
   else
      if dia_do_mes > 15 and dia_do_mes < 32
      then
         quinzena_retorna:=2;
      else
         quinzena_retorna:=0;
      end if;
   end if;

   return(quinzena_retorna);
end quinzena;

 

/

exec inter_pr_recompile;

