update pedi_187 set comprado_fabricado = 0 where comprado_fabricado is null;

alter table pedi_187  drop primary key;

drop index pk_pedi_187;

alter table pedi_187 add constraint pk_pedi_187 primary key (nivel, grupo, subgrupo, item, tipo_pessoa, estado, processo, codigo_empresa, classific_fisc, cnpj_cli9, cnpj_cli4, cnpj_cli2, nat_op_origem, origem_prod, cod_atividade_economica, insc_est,classificacao_pedido,suframa,comprado_fabricado);

exec inter_pr_recompile;
