CREATE OR REPLACE PACKAGE BODY ST_PCK_TRANSACAO AS

    function get(p_codigo_transacao number, p_msg_erro in out varchar2) return t_dados_estq_005 
    as
        row_var t_dados_estq_005;
    begin

        begin
            select *
            bulk collect into row_var
            from estq_005
            where codigo_transacao = p_codigo_transacao;
            exception when others then
                p_msg_erro := 'Transacao informada n?o cadastrada';
            end;
        return row_var;
    end;

    FUNCTION transacao_gera_contabilizacao(p_transacao IN NUMBER, p_validar_por_contab_cardex IN BOOLEAN, p_nome_form IN VARCHAR2) 
    RETURN BOOLEAN 
    AS
        v_dados_transacao  ST_PCK_TRANSACAO.t_dados_estq_005;
        v_msg_error VARCHAR2(4000) := '';

    BEGIN
        v_dados_transacao := ST_PCK_TRANSACAO.get(p_transacao, v_msg_error);
    
    IF v_dados_transacao(1).codigo_transacao IS NULL THEN
        RETURN FALSE;
    END IF;

    IF p_nome_form = 'obrf_f135' OR p_transacao = 0 THEN
        RETURN TRUE;
    END IF;
    
    RETURN CASE
        WHEN p_validar_por_contab_cardex THEN
            v_dados_transacao(1).contab_cardex = 1
        ELSE
            v_dados_transacao(1).atualiza_contabi = 1 OR v_dados_transacao(1).atualiza_contabi = 3
    END;
    END transacao_gera_contabilizacao;

       
END ST_PCK_TRANSACAO;
/
