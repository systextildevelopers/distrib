  CREATE OR REPLACE PROCEDURE "INTER_PR_CARREGA_PCPB_200_FIOS" 
is
      --No futura havera a necessiade de unir as procedures inter_pr_carrega_pcpb_200 e
      --inter_pr_carrega_pcpb_200_fios. Isto porque tanto fios como tecido pode ter a
      --data de entrega calculada com base na data de entrega do pedido como na data
      --prevista das ordens de producao.

      --cursor para pegar todas as ordens de beneficiamento que estejam no periodo aberto
      --que ainda nao passaram pelo estagio de tingimento.
      --cursor para pegar todas as ordens de beneficiamento que estejam no periodo aberto
      --que ainda nao passaram pelo estagio de tingimento.
      cursor carga_pcpb200 is
         (select pcpb_010.grupo_maquina    grupo_maq,
                 decode(PCPB_010.subgrupo_maquina,null,'   ',PCPB_010.subgrupo_maquina) subgrupo_maq,
                 PCPB_010.numero_maquina   numero_maq,
                 rownum sequencia,
                 trunc(sysdate,'DD') data_base,
                 pcpb_010.previsao_termino data_situacao,
                 trunc(pcpb_010.previsao_termino,'DD') - trunc(sysdate,'DD') situacao,
                 pcpb_040.codigo_estagio estagio_atual,
                 pcpb_010.ordem_tingimento numero_ot,
                 pcpb_010.ordem_producao numero_ob,
                 pcpb_010.previsao_termino data_entrega
          from pcpb_010,
               (select pcpb_040.ordem_producao,min(pcpb_040.seq_operacao) seq_operacao from pcpb_040
                where pcpb_040.data_termino is null
                group by pcpb_040.ordem_producao) menor_estagio,
               pcpb_040,
               pcpc_010
          where pcpb_010.ordem_producao    = pcpb_040.ordem_producao
            and pcpb_010.periodo_producao  = pcpc_010.periodo_producao
            and pcpb_010.ordem_producao    = menor_estagio.ordem_producao
            and pcpb_040.seq_operacao      = menor_estagio.seq_operacao
            and pcpb_010.cod_cancelamento  = 0
            and pcpc_010.area_periodo      = '7'
            and pcpc_010.situacao_periodo <> 3 -- Periodo abertos
            and not exists (select estq_060.lote from estq_060, pcpb_020
	                    where estq_060.lote = pcpb_010.ordem_producao
	                      and pcpb_010.ordem_producao  = pcpb_020.ordem_producao
	                      and estq_060.prodcai_nivel99 = pcpb_020.pano_sbg_nivel99
	                      and estq_060.prodcai_grupo   = pcpb_020.pano_sbg_grupo
	                      and estq_060.prodcai_subgrupo= pcpb_020.pano_sbg_subgrupo
                              and estq_060.prodcai_item    = pcpb_020.pano_sbg_item)
            and exists (select pcpb_040.codigo_estagio from pcpb_040,empr_001
                        where pcpb_040.ordem_producao = pcpb_010.ordem_producao
                          and pcpb_040.codigo_estagio = empr_001.estagio_tintur
                          and pcpb_040.data_termino is null));


      v_ordem_producao number      := 0;
      v_nivel          varchar2(1) := ' ';
      v_grupo          varchar2(5) := ' ';
      v_subgrupo       varchar2(3) := ' ';
      v_cor_ot         varchar2(6) := ' ';
      v_tonalidade     varchar2(2) := ' ';
      v_cor_estagio    varchar2(15):= ' ';

      v_tempo          number(15,5):= 0.00;
      v_minutos_tintur number(15,5):= 0.00;
      v_minutos_ja_usados number(15,5):= 0.00;

      v_prioridade_producao number := 0;
      v_seq_operacao   number(3)   := 0;
      v_estagio_tintur number      := 0;

      v_pedido        number       := 0;
      v_data_entrega  date         := null;
      v_data_entrega_final date    := null;
      v_data_inicio   date         := null;
      v_hora_inicio   date         := null;
      v_cliente       varchar2(41) := ' ';
      v_aux           number       := 0;
      v_dias          number(10,3) := 0.000;
begin

      begin
         delete pcpb_200
         where pcpb_200.data_base = trunc(sysdate,'DD');
         commit;
      exception
         when others then
            v_aux := 0;
      end;

      select empr_001.estagio_tintur into v_estagio_tintur from empr_001;

      for reg_pcpb200 in carga_pcpb200
      loop
         --recupara leadtime faltante da ob
         select nvl(sum(mqop_005.leed_time),0) into v_dias
         from mqop_005,(select codigo_estagio
                        from pcpb_015 pcpb015
                        where pcpb015.ordem_producao = reg_pcpb200.numero_ob
                          and pcpb015.seq_operacao > (select max(seq_operacao)
                                                      from pcpb_015,empr_001
                                                      where pcpb_015.codigo_estagio = empr_001.estagio_tintur
                                                        and pcpb_015.ordem_producao = reg_pcpb200.numero_ob)
                        group by codigo_estagio) pcpb015_estagio
         where pcpb015_estagio.codigo_estagio  = mqop_005.codigo_estagio;

         v_data_entrega_final := reg_pcpb200.data_entrega + v_dias;

         --seleciona a cor do estagio para cada OB.
         begin
            select pcpb_020.ordem_producao,
                   pcpb_020.pano_sbg_nivel99,
                   pcpb_020.pano_sbg_grupo,
                   pcpb_020.pano_sbg_subgrupo,
                   pcpb_020.pano_sbg_item cor_ot,-- e cor_estagio (Sao iguais)
                   basi_100.tonalidade
            into v_ordem_producao,
                 v_nivel,
                 v_grupo,
                 v_subgrupo,
                 v_cor_ot,
                 v_tonalidade
            from pcpb_020,basi_100
            where pcpb_020.ordem_producao = reg_pcpb200.numero_ob
              and basi_100.cor_sortimento = pcpb_020.pano_sbg_item
              and basi_100.tipo_cor       = 1;
         exception
            when others then
               v_ordem_producao := 0;
               v_nivel          := '0';
               v_grupo          := '00000';
               v_subgrupo       := '000';
               v_cor_ot         := '000000';
               v_tonalidade     := '00';
         end;

         --busca o tempo de producao da ob.
         begin
            select mqop_060.tempo_producao tempo
            into   v_tempo
            from mqop_060
            where mqop_060.maq_sbgr_grupo_mq = reg_pcpb200.grupo_maq
              and mqop_060.maq_sbgr_sbgr_maq = reg_pcpb200.subgrupo_maq
              and mqop_060.nivel_estrutura   = v_nivel
              and mqop_060.grupo_estrutura   = v_grupo
              and mqop_060.subgru_estrutura  = v_subgrupo
              and mqop_060.item_estrutura    = v_cor_ot;
            exception
               when no_data_found then
                  begin
                     --dbms_output.put_line(v_nivel || '.' || v_grupo || '.' || v_subgrupo || '.' || v_tonalidade);
                     select mqop_060.tempo_producao
                     into   v_tempo
                     from mqop_060
                     where mqop_060.maq_sbgr_grupo_mq = reg_pcpb200.grupo_maq
                       and mqop_060.maq_sbgr_sbgr_maq = reg_pcpb200.subgrupo_maq
                       and mqop_060.nivel_estrutura   = v_nivel
                       and mqop_060.grupo_estrutura   = v_grupo
                       and mqop_060.subgru_estrutura  = v_subgrupo
                       and mqop_060.item_estrutura    = '000000'
                       and mqop_060.tonalidade        = v_tonalidade;
                     exception
                       when no_data_found then
                          begin
                             select mqop_060.tempo_producao
                             into   v_tempo
                             from mqop_060
                             where mqop_060.maq_sbgr_grupo_mq = reg_pcpb200.grupo_maq
                               and mqop_060.maq_sbgr_sbgr_maq = reg_pcpb200.subgrupo_maq
                               and mqop_060.nivel_estrutura   = v_nivel
                               and mqop_060.grupo_estrutura   = v_grupo
                               and mqop_060.subgru_estrutura  = '000'
                               and mqop_060.item_estrutura    = '000000'
                               and mqop_060.tonalidade        = v_tonalidade;
                             exception
                                when others then
                                    v_tempo := 0.00;
                          end;
                  end;
         end;

         --busca o pedido da ob.
         begin
            select pedi_100.pedido_venda,
                   pedi_100.data_entr_venda,
                   pedi_010.nome_cliente
            into   v_pedido,
                   v_data_entrega,
                   v_cliente
            from pcpb_030,pedi_100,pedi_010
            where pcpb_030.ordem_producao   = v_ordem_producao
              and pcpb_030.pedido_corte     = 3
              and pcpb_030.nr_pedido_ordem  = pedi_100.pedido_venda
              and pedi_100.cli_ped_cgc_cli9 = pedi_010.cgc_9
              and pedi_100.cli_ped_cgc_cli4 = pedi_010.cgc_4
              and pedi_100.cli_ped_cgc_cli2 = pedi_010.cgc_2
              and rownum                    = 1
            order by pedi_100.data_entr_venda;
         exception
            when others then
               v_pedido       := 0;
               v_data_entrega := null;
               v_cliente      := ' ';
         end;

         --busca a prioridade de producao da ob
         begin
            select pcpb_100.sequencia_entrada_maquina
            into   v_prioridade_producao
            from pcpb_100
            where pcpb_100.tipo_ordem       in('1')
              and pcpb_100.ordem_agrupamento  = reg_pcpb200.numero_ot
              and pcpb_100.ordem_producao     = reg_pcpb200.numero_ob
              and rownum                      = 1
            order by pcpb_100.ordem_agrupamento, pcpb_100.ordem_producao, pcpb_100.sequencia_entrada_maquina;
         exception
            when others then
               v_prioridade_producao := 0;
         end;

         begin
             --busca a cor representativa para o estagio
             select cor_representativa into v_cor_estagio
             from  mqop_005
             where mqop_005.codigo_estagio = reg_pcpb200.estagio_atual
             group by cor_representativa;

             exception
                when others then
                   --raise_application_error(-20000,'Cor representativa para o estagio: ' || to_char(reg_grafico.int_08) || ' nao encontrada.');
                   v_cor_estagio := '0';
         end;

         --busca a data inicial e hora inicial para a menor sequencia da ob
         begin
             begin
                select min(pcpb_015.data_inicio), min(pcpb_015.hora_inicio)
                into   v_data_inicio,        v_hora_inicio
                from  pcpb_015
                where pcpb_015.ordem_producao = reg_pcpb200.numero_ob
                  and pcpb_015.codigo_estagio = v_estagio_tintur
                  and pcpb_015.data_inicio   is not null
                  and pcpb_015.data_termino  is null;
                exception
                   when no_data_found then
                       v_data_inicio := null;
                       v_hora_inicio := null;
             end;


             v_minutos_tintur    := 0.00;
             v_minutos_ja_usados := 0.00;

             if v_data_inicio is not null then
                begin
                   select sum(pcpb_015.minutos_unitario) into v_minutos_tintur
                   from  pcpb_015
                   where pcpb_015.ordem_producao = reg_pcpb200.numero_ob
                     and pcpb_015.codigo_estagio = v_estagio_tintur
                     and pcpb_015.data_termino  is null;
                   exception
                      when no_data_found then
                          v_minutos_tintur := 0.00;
                end;

                if v_minutos_tintur > 0.00 then
                   v_minutos_ja_usados := inter_fn_calcula_minutos(v_data_inicio,v_hora_inicio,sysdate,sysdate);
                end if;
             end if;
         end;

         begin
            insert into pcpb_200
               (grupo_maq,                          subgrupo_maq,
                numero_maq,                         sequencia,
                data_base,                          data_situacao,
                situacao,                           estagio_atual,
                numero_ot,                          numero_ob,
                cor_ot,                             cor_estagio,
                tempo,                              pedido,
                cliente,                            data_entrega,
                prioridade_producao,                minutos_restantes)
            values
               (reg_pcpb200.grupo_maq,              reg_pcpb200.subgrupo_maq,
                reg_pcpb200.numero_maq,             reg_pcpb200.sequencia,
                reg_pcpb200.data_base,              reg_pcpb200.data_situacao,
                reg_pcpb200.situacao,               reg_pcpb200.estagio_atual,
                reg_pcpb200.numero_ot,              reg_pcpb200.numero_ob,
                v_cor_ot,                           v_cor_estagio,
                v_tempo,                            v_pedido,
                v_cliente,                          v_data_entrega_final,
                v_prioridade_producao,              (v_minutos_tintur - v_minutos_ja_usados));
         exception
            when others then
               dbms_output.put_line(reg_pcpb200.grupo_maq || '.' ||
                              reg_pcpb200.subgrupo_maq || '.' ||
                              reg_pcpb200.numero_maq  || ' | ' ||
                              to_char(reg_pcpb200.sequencia,'00000000')  || ' | ' ||
                              to_char(reg_pcpb200.data_base)  || ' | ' ||
                              to_char(reg_pcpb200.data_situacao)  || ' | ' ||
                              to_char(reg_pcpb200.situacao,'0000')  || ' | ' ||
                              to_char(reg_pcpb200.estagio_atual,'99900')  || ' | ' ||
                              to_char(reg_pcpb200.numero_ot,'000000000')  || ' | ' ||
                              to_char(reg_pcpb200.numero_ob,'000000000')  || ' | ' ||
                              v_cor_ot  || ' | ' ||
                              v_cor_ot  || ' | ' ||
                              to_char(v_tempo,'00000000,00000')  || ' | ' ||
                              to_char(v_pedido,'000000')  || ' | ' ||
                              v_cliente || ' | ' ||
                              to_char(v_data_entrega_final) || ' | ' ||
                              to_char(v_prioridade_producao)
                              );
               raise_application_error(-20000,'Erro ao gravar a pcpb_200.');
         end;

         commit;

      end loop;
end inter_pr_carrega_pcpb_200_fios;

 

/

exec inter_pr_recompile;

