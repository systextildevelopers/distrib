create table sped_1601 (
	cod_empresa		number(3) not null,
	mes				number(2) not null,
	ano 			number(4) not null,
	part_ip_cnpj9	number(9) not null,
	part_ip_cnpj4	number(4) not null,
	part_ip_cnpj2	number(2) not null,
	part_it_cnpj9	number(9) not null,
	part_it_cnpj4	number(4) not null,
	part_it_cnpj2	number(2) not null,
	tot_vs			number(15,2) default 0.00,
	tot_iss			number(15,2) default 0.00,
	tot_outros		number(15,2) default 0.00
);
