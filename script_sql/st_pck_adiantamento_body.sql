create or replace PACKAGE BODY ST_PCK_ADIANTAMENTO AS

    function exists_adiantamento_imp(p_num_importacao IN varchar2,  p_cgc_9 IN number,
                                     p_cgc_4 IN number,             p_cgc_2 IN number,
                                     p_nr_adiantam OUT number) return boolean
    AS
        v_nr_adiantam number;
    BEGIN
        select nvl(numero_adiantam, 0)
        into v_nr_adiantam
        from cpag_200
        where cgc_9 = p_cgc_9
        and cgc_4 = p_cgc_4
        and cgc_2 = p_cgc_2
        and num_importacao = p_num_importacao
        and rownum = 1;

        p_nr_adiantam := v_nr_adiantam;
        IF v_nr_adiantam > 0 THEN
            return true;
        ELSE
            return false;
        END IF;
    EXCEPTION
    when no_data_found then
        p_nr_adiantam := 0;
        return false;
    when others then
        p_nr_adiantam := -1;
        return false;
    END exists_adiantamento_imp;

    function cria_adiantamento_cf(p_cgc9 IN number,                p_cgc4 IN number,            p_cgc2 IN number,       p_empresa IN number,
                               p_centro_custo IN number,        p_nome IN varchar2,          p_valor IN number,      p_data_contrato IN date,
                               p_data_vencimento IN date,       p_cod_portador IN number,    p_moeda IN varchar2,    p_num_importacao IN varchar2,
                               P_origem_adiantam IN varchar2,   v_num_adiantam in out number, p_tipo_titulo IN number,  p_tipo_adiantam IN number) return varchar2
    AS
        v_moeda number;
        p_des_erro varchar2(1000);
    BEGIN
        p_des_erro := '';
        v_moeda := 0;

        declare
            busca_nova_sequencia boolean;
            v_nr_inicial number;
            v_nr_adiantamento number;
            tmpInt number;
        begin
            tmpInt := 0;
            v_nr_inicial := 0;
            busca_nova_sequencia := true;

            if p_tipo_adiantam = 1
            then
              WHILE busca_nova_sequencia
              LOOP
                  select registros_vazios
                  into v_nr_inicial
                  from(
                      select ROWNUM as registros_vazios
                      from dual
                      CONNECT BY LEVEL <= 999999 MINUS
                      select numero_adiantam
                      from cpag_200
                      where cpag_200.tipo_adiantam = p_tipo_adiantam
                  )
                  where registros_vazios > v_nr_inicial
                  and ROWNUM = 1;


                    begin
                        select nr_duplicata
                        into tmpInt
                        from cpag_010
                        where cpag_010.cgc_9 = p_cgc9
                        and   cpag_010.cgc_4 = p_cgc4
                        and   cpag_010.cgc_2 = p_cgc2
                        and   cpag_010.tipo_titulo = p_tipo_titulo
                        and   cpag_010.nr_duplicata = v_nr_inicial;
                    EXCEPTION WHEN
                    no_data_found then
                        v_nr_adiantamento := v_nr_inicial;
                        busca_nova_sequencia := false;
                    end;


              END LOOP;

            else


                WHILE busca_nova_sequencia
                LOOP
                    select nvl(max(numero_adiantam),0) + 1 into v_nr_inicial
                    from cpag_200
                    where cpag_200.tipo_adiantam = p_tipo_adiantam;

                    begin
                       select numero_adiantam
                       into tmpInt
                       from cpag_200
                       where cpag_200.numero_adiantam = v_nr_inicial
                         and cpag_200.tipo_adiantam   = p_tipo_adiantam;
                    EXCEPTION WHEN
                    no_data_found then
                        v_nr_adiantamento := v_nr_inicial;
                        busca_nova_sequencia := false;
                    end;
                END LOOP;

            end if;

            v_num_adiantam := v_nr_adiantamento;
        EXCEPTION WHEN
        others then
            p_des_erro := 'Erro criar o sequencia do adiantamento. ERRO: ' || SQLERRM;
            v_num_adiantam := 0;
           -- return p_des_erro;
        END;

        if p_moeda = 'BRL'
        THEN
            v_moeda := 0;
        end if;

        BEGIN
            insert into cpag_200 (
                tipo_adiantam, numero_adiantam,
                cgc_9, cgc_4,
                cgc_2, data_digitacao,
                valor_adiant, valor_saldo,
                situacao, data_remessa,
                codigo_empresa, data_prev_pgto,
                cod_moeda, cod_portador,
                data_moeda, origem_adiantam,
                num_importacao
            ) values (
                p_tipo_adiantam, v_num_adiantam,
                p_cgc9, p_cgc4,
                p_cgc2, p_data_contrato,
                p_valor, p_valor,
                1, p_data_contrato,
                p_empresa, p_data_vencimento,
                v_moeda, p_cod_portador,
                p_data_contrato, P_origem_adiantam,
                p_num_importacao
            );
        EXCEPTION WHEN
        others then
            p_des_erro := 'Erro ao inserir adiantamento. Nome: ' || p_nome || ' Nr. Adiant.: ' || v_num_adiantam || ' ERRO: ' || SQLERRM;
        END;

        RETURN p_des_erro;
    END cria_adiantamento_cf;

    function cria_adiantamento_moeda(p_cgc9 IN number,                p_cgc4 IN number,            p_cgc2 IN number,       p_empresa IN number, 
                               p_centro_custo IN number,        p_nome IN varchar2,          p_valor IN number,      p_data_contrato IN date, 	
                               p_data_vencimento IN date,       p_cod_portador IN number,    p_moeda IN number,    p_num_importacao IN varchar2,   
                               P_origem_adiantam IN varchar2,   v_num_adiantam in out number, p_tipo_titulo IN number, p_tipo_adiantam IN number,
                               p_cotacao_moeda in number) return varchar2
    AS
        v_moeda number;
        p_des_erro varchar2(1000);
    BEGIN
        p_des_erro := '';
        v_moeda := 0;

        declare
            busca_nova_sequencia boolean;
            v_nr_inicial number;
            v_nr_adiantamento number;
            tmpInt number;
        begin
            tmpInt := 0;
            v_nr_inicial := 0;
            busca_nova_sequencia := true;

            if p_tipo_adiantam = 1
            then
              WHILE busca_nova_sequencia
              LOOP
                  select registros_vazios
                  into v_nr_inicial
                  from(
                      select ROWNUM as registros_vazios
                      from dual
                      CONNECT BY LEVEL <= 999999 MINUS
                      select numero_adiantam
                      from cpag_200
                      where cpag_200.tipo_adiantam = p_tipo_adiantam
                  )
                  where registros_vazios > v_nr_inicial
                  and ROWNUM = 1;


                    begin
                        select nr_duplicata
                        into tmpInt
                        from cpag_010
                        where cpag_010.cgc_9 = p_cgc9
                        and   cpag_010.cgc_4 = p_cgc4
                        and   cpag_010.cgc_2 = p_cgc2
                        and   cpag_010.tipo_titulo = p_tipo_titulo
                        and   cpag_010.nr_duplicata = v_nr_inicial;
                    EXCEPTION WHEN
                    no_data_found then
                        v_nr_adiantamento := v_nr_inicial;
                        busca_nova_sequencia := false;
                    end;


              END LOOP;

            else


                WHILE busca_nova_sequencia
                LOOP
                    select nvl(max(numero_adiantam),0) + 1 into v_nr_inicial
                    from cpag_200
                    where cpag_200.tipo_adiantam = p_tipo_adiantam;

                    begin
                       select numero_adiantam
                       into tmpInt
                       from cpag_200
                       where cpag_200.numero_adiantam = v_nr_inicial
                         and cpag_200.tipo_adiantam   = p_tipo_adiantam;
                    EXCEPTION WHEN
                    no_data_found then
                        v_nr_adiantamento := v_nr_inicial;
                        busca_nova_sequencia := false;
                    end;
                END LOOP;

            end if;

            v_num_adiantam := v_nr_adiantamento;
        EXCEPTION WHEN
        others then
            p_des_erro := 'Erro criar o sequencia do adiantamento. ERRO: ' || SQLERRM;
            v_num_adiantam := 0;
           -- return p_des_erro;
        END;

        BEGIN
            insert into cpag_200 (
                tipo_adiantam, numero_adiantam,
                cgc_9, cgc_4,
                cgc_2, data_digitacao,
                valor_adiant, valor_saldo,
                situacao, data_remessa,
                codigo_empresa, data_prev_pgto,
                cod_moeda, cod_portador,
                data_moeda, origem_adiantam,
                num_importacao, cotacao_moeda
            ) values (
                p_tipo_adiantam, v_num_adiantam,
                p_cgc9, p_cgc4,
                p_cgc2, p_data_contrato,
                p_valor, p_valor,
                1, p_data_contrato,
                p_empresa, p_data_vencimento,
                p_moeda, p_cod_portador,
                p_data_contrato, P_origem_adiantam,
                p_num_importacao, p_cotacao_moeda
            );
        EXCEPTION WHEN
        others then
            p_des_erro := 'Erro ao inserir adiantamento. Nome: ' || p_nome || ' Nr. Adiant.: ' || v_num_adiantam || ' ERRO: ' || SQLERRM;
        END;

        RETURN p_des_erro;
    END cria_adiantamento_moeda;

    function cria_adiantamento(p_cgc9 IN number,                p_cgc4 IN number,            p_cgc2 IN number,       p_empresa IN number,
                               p_centro_custo IN number,        p_nome IN varchar2,          p_valor IN number,      p_data_contrato IN date,
                               p_data_vencimento IN date,       p_cod_portador IN number,    p_moeda IN varchar2,    p_num_importacao IN varchar2,
                               P_origem_adiantam IN varchar2,   v_num_adiantam in out number, p_tipo_titulo IN number) return varchar2
    AS
        v_moeda number;
        p_des_erro varchar2(1000);
        p_tipo_adiantam number;
    BEGIN
        p_tipo_adiantam := 1;
        v_moeda := 0;

        BEGIN
          RETURN cria_adiantamento_cf(p_cgc9,                p_cgc4,            p_cgc2,       p_empresa,
                 p_centro_custo,        p_nome,          p_valor,      p_data_contrato,
                 p_data_vencimento,       p_cod_portador,    p_moeda,    p_num_importacao,
                 P_origem_adiantam,   v_num_adiantam, p_tipo_titulo,  p_tipo_adiantam);
        EXCEPTION when
            others then
            raise_application_error(-20001, 'Adiantamento n?o existe no ERP.' ||
                                                'v_tipo_adiantamento: ');
        end;
    END;

    function valida_saldo_adiantamento( p_tipo_adiantamento IN number,  p_valor_adiantamento IN number,
                                        p_id_importacao IN varchar2,    p_nr_adiantamento IN number) return varchar2
    AS
        v_saldo_adiantamento number;
        v_nr_adiantamento number;
    BEGIN
        if p_nr_adiantamento is null then
            v_nr_adiantamento := 0;
        else
            v_nr_adiantamento := p_nr_adiantamento;
        end if;

        --Caso ele n?o passar o adiantamento
        --o programa vai buscar o nr_adiantamento da cpag_200 utilizando o nr_importacao
        if v_nr_adiantamento = 0 then
            begin
                select cpag_200.valor_saldo, cpag_200.numero_adiantam
                into v_saldo_adiantamento, v_nr_adiantamento
                from cpag_200
                where cpag_200.tipo_adiantam  = 1
                and   cpag_200.num_importacao = p_id_importacao
                and   cpag_200.situacao       = 1;
            EXCEPTION when
            others then
                raise_application_error(-20001, 'Adiantamento n?o existe no ERP.' || p_id_importacao ||
                                                'v_tipo_adiantamento: ' || p_tipo_adiantamento);
            end;

            if v_saldo_adiantamento < p_valor_adiantamento then
                raise_application_error(-20001, 'Adiantamento possui um saldo diferente do ERP. Id integrac?o: ' || p_id_importacao ||
                                                                                        'tipo adiantamento: ' || p_tipo_adiantamento||
                                                                                        'v_saldo_adiantamento: ' || v_saldo_adiantamento ||
                                                                                        'p_valor_adiantamento: ' || p_valor_adiantamento);
            end if;
        else
            begin
                select cpag_200.valor_saldo
                into v_saldo_adiantamento
                from cpag_200
                where cpag_200.tipo_adiantam   = p_tipo_adiantamento
                and   cpag_200.numero_adiantam = v_nr_adiantamento
                and   cpag_200.situacao        = 1;
            EXCEPTION when
            others then
                raise_application_error(-20001, 'Adiantamento n?o cadastrado no ERP.'
                                                || v_nr_adiantamento);
            end;

            if v_saldo_adiantamento < p_valor_adiantamento then
                raise_application_error(-20001, 'Adiantamento possui um saldo diferente do ERP.'
                                                || v_nr_adiantamento);
            end if;
        end if;

        update cpag_200
        set valor_saldo = cpag_200.valor_saldo - p_valor_adiantamento
        where cpag_200.tipo_adiantam   = 1
        and cpag_200.numero_adiantam = v_nr_adiantamento;

        return v_nr_adiantamento;
    END valida_saldo_adiantamento;

END ST_PCK_ADIANTAMENTO;

/
