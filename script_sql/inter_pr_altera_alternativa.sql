
  CREATE OR REPLACE PROCEDURE "INTER_PR_ALTERA_ALTERNATIVA" (
p_ordem_producao              in number,
p_alternativa_new             in number,
p_codigo_risco                in number
) is

/*


      FUNCAO PARA VERIFICAR SE TEM ORDEM PARA QUELA AREA


*/
function f_tem_area (p_ordem_producao in number,
                      p_area          in number)
RETURN integer is
   tem_area integer;

BEGIN
   tem_area := 0;

   begin
     select nvl(count(1),0)
     into tem_area
     from tmrp_637
     where tmrp_637.ordem_prod_compra = p_ordem_producao
       and tmrp_637.area_producao     = p_area;
   exception when others then
      tem_area := 0;
   end;

   return(tem_area);

END;

/*


      FUNCAO PARA VERIFICAR SE TEM ERRO PARA A ORDEM


*/
procedure f_erro_recalculo (
   p1_ordem_producao in number,
   p1_area           in number
) IS

   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   v_count_reg               integer;

begin
   -- Dados do usuario logad
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

    /*
       Marca os produtos da tmrp_630 cujo produto nao esta mais na estrutura do produto da tmrp_625
    */
    for reg_tmrp_630 in (select tmrp_630.ordem_planejamento,
                                tmrp_630.pedido_venda,
                                tmrp_630.nivel_produto,
                                tmrp_630.grupo_produto,
                                tmrp_630.subgrupo_produto,
                                tmrp_630.item_produto,
                                tmrp_630.alternativa_produto
                         from tmrp_630
                         where tmrp_630.ordem_prod_compra = p1_ordem_producao
                           and tmrp_630.area_producao     = p1_area)
    loop
       begin
          select nvl(count(*),0)
          into v_count_reg
          from tmrp_625
          where tmrp_625.ordem_planejamento   = reg_tmrp_630.ordem_planejamento
            and tmrp_625.pedido_venda         = reg_tmrp_630.pedido_venda
            and tmrp_625.nivel_produto        = reg_tmrp_630.nivel_produto
            and tmrp_625.grupo_produto        = reg_tmrp_630.grupo_produto
            and tmrp_625.subgrupo_produto     = reg_tmrp_630.subgrupo_produto
            and tmrp_625.item_produto         = reg_tmrp_630.item_produto
            and tmrp_625.alternativa_produto  = reg_tmrp_630.alternativa_produto;
       exception when others then
          v_count_reg := 0;
       end;

       /*
          Se nao encontrar o produto na tmrp_625, deve marcar o registro da tmrp_630 com ERRO
       */
       if v_count_reg = 0
       then
          begin
             update tmrp_630
             set   tmrp_630.erro_recalculo       = 1         -- tem erro neste produto
             where tmrp_630.ordem_planejamento   = reg_tmrp_630.ordem_planejamento
               and tmrp_630.pedido_venda         = reg_tmrp_630.pedido_venda
               and tmrp_630.area_producao        = p1_area
               and tmrp_630.nivel_produto        = reg_tmrp_630.nivel_produto
               and tmrp_630.grupo_produto        = reg_tmrp_630.grupo_produto
               and tmrp_630.subgrupo_produto     = reg_tmrp_630.subgrupo_produto
               and tmrp_630.item_produto         = reg_tmrp_630.item_produto
               and tmrp_630.alternativa_produto  = reg_tmrp_630.alternativa_produto;
          exception when others then
             -- ATENCAO! Nao eliminou {0}. Mensagem do banco de dados: {1}
             raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26375', 'TMRP_630(E630)' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
          end;
       end if;
    end loop;  --reg_tmrp_630
END;

/*


      PROCEDIMENTO PARA TROCAR ALTERNATIVA DO NIVEL 1


*/

procedure troca_alternativa_area_1 (
   p_ordem_producao     in number,
   p_alternativa_new    in number,
   p_codigo_risco       in number
) is
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   v_codigo_risco            number;

begin
   -- Dados do usuario logad
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   -- Deleta a tmrp_637 da ordem de planejamento
   begin
      delete from tmrp_637
      where tmrp_637.ordem_prod_compra = p_ordem_producao
        and tmrp_637.area_producao     = 1;
   exception when others then
      -- ATENCAO! Nao eliminou {0}. Mensagem do banco de dados: {1}
      raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26373', 'TMRP_637' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
   end;

   -- Cria umm backup da tmrp_630 desta ordem de planejamento
   -- tmrp_637 e uma copia da tmrp_630, todos os campso que tiver na tmrp_630 deve ter na tmrp_637
   begin
      insert into tmrp_637 (
             ordem_planejamento,                      area_producao,
             ordem_prod_compra,
             nivel_produto,                           grupo_produto,
             subgrupo_produto,                        item_produto,
             qtde_produzida_comprada,                 qtde_conserto,
             qtde_2qualidade,                         qtde_perdas,
             pedido_venda,                            programado_comprado,
             alternativa_produto,                     qtde_atendida_estoque)
      select tmrp_630.ordem_planejamento,             tmrp_630.area_producao,
             tmrp_630.ordem_prod_compra,
             tmrp_630.nivel_produto,                  tmrp_630.grupo_produto,
             tmrp_630.subgrupo_produto,               tmrp_630.item_produto,
             tmrp_630.qtde_produzida_comprada,        tmrp_630.qtde_conserto,
             tmrp_630.qtde_2qualidade,                tmrp_630.qtde_perdas,
             tmrp_630.pedido_venda,                   tmrp_630.programado_comprado,
             tmrp_630.alternativa_produto,            tmrp_630.qtde_atendida_estoque
      from tmrp_630
      where tmrp_630.ordem_prod_compra = p_ordem_producao
        and tmrp_630.area_producao     = 1;
   exception when others then
      -- ATENCAO! Nao inseriu {0}. Mensagem do banco de dados: {1}
      raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'TMRP_637' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
   end;

   -- Deleta a tmrp_630 desta ordem de planejamento
   begin
      delete from tmrp_630
      where tmrp_630.ordem_prod_compra = p_ordem_producao
        and tmrp_630.area_producao     = 1;
   exception when others then
      -- ATENCAO! Nao eliminou {0}. Mensagem do banco de dados: {1}
      raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26373', 'TMRP_630' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
   end;

   begin
      insert into tmrp_621 (
             ordem_planejamento,             pedido_venda,
             ordem_prod_compra,              area_producao,
             pedido_reserva,                 tipo_produto_origem,

             seq_produto_origem,             nivel_produto_origem,
             grupo_produto_origem,           subgrupo_produto_origem,
             item_produto_origem,            alternativa_produto_origem,

             tipo_produto,                   seq_produto,
             nivel_produto,                  grupo_produto,
             subgrupo_produto,               item_produto,

             alternativa_produto,            qtde_areceber_programada,
             alternativa_produto_old)
      select tmrp_637.ordem_planejamento,    tmrp_637.pedido_venda,
             p_ordem_producao,               1,
             0,                              ' ',

             0,                              '0',
             '00000',                        '000',
             '000000',                       0,

             ' ',                            0,
             '1',                            tmrp_637.grupo_produto,
             tmrp_637.subgrupo_produto,      tmrp_637.item_produto,

             p_alternativa_new,              tmrp_637.programado_comprado,
             tmrp_637.alternativa_produto
      from tmrp_637
      where tmrp_637.ordem_prod_compra = p_ordem_producao
        and tmrp_637.area_producao     = 1;
   end;

   begin
      update tmrp_637
      set    tmrp_637.alternativa_produto = p_alternativa_new
      where  tmrp_637.ordem_prod_compra   = p_ordem_producao
        and  tmrp_637.area_producao       = 1;
   end;


   /*
   #################################################################################
   #################################################################################
                         rotina de calculo para o NIVEL 1
   #################################################################################
   #################################################################################
   */
   -- Verifica se tem alguma ordem para a Area 1
   if f_tem_area (p_ordem_producao, 1) > 0
   then
      -- Inserir na tabela temporaria os produto origem do nivel 1
      begin
         insert into tmrp_615 (
                nome_programa,                            area_producao,
                nr_solicitacao,                           tipo_registro,
                cgc_cliente9,                             alternativa,
                nivel,                                    grupo,
                subgrupo,                                 item,
                ordem_planejamento,                       pedido_venda)
         select 'trigger_planejamento',                   1,
                888,                                      888,
                ws_sid,                                   0,
                '0',                                      '00000',
                '000',                                    '000000',
                tmrp_637.ordem_planejamento,              tmrp_637.pedido_venda
         from tmrp_637
         where tmrp_637.ordem_prod_compra = p_ordem_producao
           and tmrp_637.area_producao     = 1;
      exception when others then
         -- ATENCAO! Nao inseriu {0}. Mensagem do banco de dados: {1}
         raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'TMRP_615(10)' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;

      v_codigo_risco := p_codigo_risco;

      if v_codigo_risco = 0
      then
         v_codigo_risco := -1;
      end if;

      -- Volta o backup da tmrp_630 para a area 1, para recalcular a qtde areceber e a qtde programada
      begin
         insert into tmrp_630 (
                ordem_planejamento,                      area_producao,
                ordem_prod_compra,
                nivel_produto,                           grupo_produto,
                subgrupo_produto,                        item_produto,
                qtde_produzida_comprada,                 qtde_conserto,
                qtde_2qualidade,                         qtde_perdas,
                pedido_venda,                            programado_comprado,
                alternativa_produto,                     qtde_atendida_estoque,
                codigo_risco)
         select tmrp_637.ordem_planejamento,             tmrp_637.area_producao,
                tmrp_637.ordem_prod_compra,
                tmrp_637.nivel_produto,                  tmrp_637.grupo_produto,
                tmrp_637.subgrupo_produto,               tmrp_637.item_produto,
                tmrp_637.qtde_produzida_comprada,        tmrp_637.qtde_conserto,
                tmrp_637.qtde_2qualidade,                tmrp_637.qtde_perdas,
                tmrp_637.pedido_venda,                   tmrp_637.programado_comprado,
                tmrp_637.alternativa_produto,            tmrp_637.qtde_atendida_estoque,
                v_codigo_risco
         from tmrp_637
         where tmrp_637.ordem_prod_compra = p_ordem_producao
           and tmrp_637.area_producao     = 1;
      exception when others then
         -- ATENCAO! Nao inseriu {0}. Mensagem do banco de dados: {1}
         raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26374', 'TMRP_630(01)' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
      end;

      /*
         Marca os produtos da tmrp_630 cujo produto nao esta mais na estrutura do produto da tmrp_625
      */
      f_erro_recalculo (p_ordem_producao, 1);
   end if;

   -- Deletar tabela temporaria que guarda o produto origem
   begin
      delete from tmrp_615
      where tmrp_615.nome_programa      = 'trigger_planejamento'
        and tmrp_615.nr_solicitacao     = 888
        and tmrp_615.tipo_registro      = 888
        and tmrp_615.ordem_planejamento in (select tmrp_637.ordem_planejamento
                                            from tmrp_637
                                            where tmrp_637.ordem_prod_compra = p_ordem_producao
                                              and tmrp_637.area_producao     = 1
                                            group by tmrp_637.ordem_planejamento);
   exception when others then
      -- ATENCAO! Nao eliminou {0}. Mensagem do banco de dados: {1}
      raise_application_error(-20000,inter_fn_buscar_tag_composta('ds26373', 'TMRP_615(345)' , sqlerrm, '' , '' , '' , '' , '' , '' , '' , '',ws_locale_usuario,ws_usuario_systextil ));
   end;
end;

begin
   troca_alternativa_area_1 (p_ordem_producao,
                             p_alternativa_new,
                             p_codigo_risco);

end inter_pr_altera_alternativa;

 

/

exec inter_pr_recompile;

