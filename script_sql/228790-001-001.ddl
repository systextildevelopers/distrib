CREATE TABLE PEDI_139 (
  pedido NUMBER(9) NOT NULL,
  sequencia NUMBER(9) NOT NULL,
  perc_desc NUMBER (5,2),
  perc_desc_alt number (5,2)
);

alter table PEDI_139
ADD CONSTRAINT pk_pedi_139 PRIMARY KEY (pedido, sequencia);

exec inter_pr_recompile;
/
