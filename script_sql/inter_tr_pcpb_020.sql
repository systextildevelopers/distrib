create or replace TRIGGER "INTER_TR_PCPB_020" 
   before insert or
          delete or
          update of cod_cancelamento, qtde_quilos_prog, qtde_rolos_prog,
                    qtde_quilos_prod, qtde_rolos_prod, alternativa_item,
                    roteiro_opcional, qtde_quilos_real
   on pcpb_020
   for each row

declare
   v_periodo_producao    pcpb_010.periodo_producao%type;
   v_peso_rolo           basi_020.peso_rolo%type;

   v_saldo_rolos_ordem   number;

   v_situacao_ordem      number;
   v_tipo_ordem          number;
   v_ordem_tingimento    number;

   v_sub_ler             pcpb_020.pano_sbg_subgrupo%type := '###';
   v_item_ler            pcpb_020.pano_sbg_item%type     := '######';
   v_nr_registro         number;
   v_nr_registro_tmrp41  number;
   v_nr_registro_pcpb40  number;

   v_registro            number;

   v_data_embarque       date;
   v_nr_registro_031     number;
   v_codigo_empresa      number;
   
   forma_pesagem_rolo_acabado number;
begin
   if inserting
   then
      -- Busca o periodo e tipo da ordem de producao da OB.
      begin
         select pcpb_010.periodo_producao, pcpb_010.tipo_ordem
         into   v_periodo_producao,        v_tipo_ordem
         from pcpb_010
         where pcpb_010.ordem_producao = :new.ordem_producao;
         exception
         when others then
            v_periodo_producao := 0;
            v_tipo_ordem       := 0;
            raise_application_error(-20000,'Capa da Ordem de Beneficiamento nao cadastrada (PCPB_010).');
      end;
	  
	 if :new.pano_sbg_nivel99 = '2' and (:new.qtde_quilos_prod < 0 or :new.qtde_rolos_prod < 0)
     then
        raise_application_error(-20000,'ATENCAO! Nao sera possivel fazer a entrada de rolos via balanca. Os valores de quantidade de quilos ou rolos produzidos irao ficar negativos.');
     end if;
	  
	  if inserting
      then

        --232752 - Ordem Beneficiamento - Se o BlocoK estiver ativo: Nao deixar ter mais de 1 produto na OB!
        declare
            v_existe_blocok             number;
            v_qtd_produtos_ob_ja_ins    number;

            v_cod_emp_periodo           number;
            v_valida_arup_rot_ob        number;

            v_codigo_estagio050_vld     number;
            v_seq_operacao050_vld       number;
        begin
            v_existe_blocok := 0;

            --Verificar se o BlocoK esta ativo
            begin
                select 1 
                into v_existe_blocok
                from empr_007 
                where param = 'obrf.controleProcessoBlocoK' 
                and   default_int = 1
                and rownum <= 1;
            exception when others then
                v_existe_blocok := 0;
            end;

            --BlocoK esta ativo!
            if v_existe_blocok = 1 then

                begin

                    --Buscar quantos produtos ja estao inseridos na pcpb_020 para esta OB
                    select
                        INTER_FN_QTD_PROD_OB_JA_INS (
                            p_ordem_producao_ob      => :new.ordem_producao     --17320--21536
                        )
                    into v_qtd_produtos_ob_ja_ins
                    from dual;

                exception when others then
                    v_qtd_produtos_ob_ja_ins := 0;
                end;

                
                --Ja tem 1 ou mais produtos na OB?
                if v_qtd_produtos_ob_ja_ins >= 1 then
                    --Esta inserindo um produto na OB e ja tem algum produto na OB

                    --Exibir mensagem e nao deixar, cfme documento ticket
                    raise_application_error(-20000,'ATENCAO! Esta ordem de producao ja tem '|| v_qtd_produtos_ob_ja_ins ||' produto(s). Com o BlocoK ativo e permitido apenas 1 produto na OB! Nao sera possivel inserir este produto!');

                end if; --v_qtd_produtos_ob_ja_ins >= 1


                
                
                --233059 - Beneficiamento - Validar se roteiro da OB tem agrupador definido caso o parametro esteja configurado como 2-Alerta e Trava:
                --Buscar empresa do periodo da OB
                v_cod_emp_periodo := 0;
                begin
                    select pcpc_010.codigo_empresa
                    into   v_cod_emp_periodo
                    from pcpc_010
                    where pcpc_010.periodo_producao = v_periodo_producao
                    and pcpc_010.area_periodo       = :new.pano_sbg_nivel99    --Area/Nivel 2 ou 7 (pcpb_f010 ou pcpb_f410) de acordo com o nivel do produto da OB
                    and rownum <= 1;
                exception when others then
                    v_cod_emp_periodo := 0;
                end;

                --Buscar o parametro de validacao (0-Desativado, 1-Apenas alerta ou 2-Alerta e Trava) desta empresa 
                v_valida_arup_rot_ob := 0;
                begin
                    select val_int
                    into v_valida_arup_rot_ob
                    from empr_008 
                    where param        = 'obrf.validaEstAgrupDigitOB' 
                    and codigo_empresa = v_cod_emp_periodo
                    and rownum <= 1;
                exception when others then
                    v_valida_arup_rot_ob := 0;
                end;


                if v_valida_arup_rot_ob = 2     --2: Alerta e Trava
                then
                
                    --Buscar o roteiro
                    begin
                        v_sub_ler  := :new.pano_sbg_subgrupo;
                        v_item_ler := :new.pano_sbg_item;
                    end;

                    begin
                        select count(*)
                        into v_nr_registro
                        from mqop_050
                        where mqop_050.nivel_estrutura  = :new.pano_sbg_nivel99
                        and   mqop_050.grupo_estrutura  = :new.pano_sbg_grupo
                        and   mqop_050.subgru_estrutura = v_sub_ler
                        and   mqop_050.item_estrutura   = v_item_ler
                        and   mqop_050.numero_alternati = :new.alternativa_item
                        and   mqop_050.numero_roteiro   = :new.roteiro_opcional;
                        if v_nr_registro = 0
                        then
                            v_item_ler := '000000';
                            begin
                                select count(*)
                                into v_nr_registro
                                from mqop_050
                                where mqop_050.nivel_estrutura  = :new.pano_sbg_nivel99
                                and   mqop_050.grupo_estrutura  = :new.pano_sbg_grupo
                                and   mqop_050.subgru_estrutura = v_sub_ler
                                and   mqop_050.item_estrutura   = v_item_ler
                                and   mqop_050.numero_alternati = :new.alternativa_item
                                and   mqop_050.numero_roteiro   = :new.roteiro_opcional;
                                if v_nr_registro = 0
                                then
                                    v_sub_ler  := '000';
                                    v_item_ler := :new.pano_sbg_item;
                                    begin
                                        select count(*)
                                        into v_nr_registro
                                        from mqop_050
                                        where mqop_050.nivel_estrutura  = :new.pano_sbg_nivel99
                                        and   mqop_050.grupo_estrutura  = :new.pano_sbg_grupo
                                        and   mqop_050.subgru_estrutura = v_sub_ler
                                        and   mqop_050.item_estrutura   = v_item_ler
                                        and   mqop_050.numero_alternati = :new.alternativa_item
                                        and   mqop_050.numero_roteiro   = :new.roteiro_opcional;
                                        if v_nr_registro = 0
                                        then
                                            v_item_ler := '000000';
                                        end if;
                                    end;
                                end if;
                            end;
                        end if;
                    end;

                    --Verificar se no roteiro tem algum estagio com agrupador nulo ou 0 (sem agrupador)
                    v_codigo_estagio050_vld := 0;
                    v_seq_operacao050_vld   := 0;
                    v_nr_registro           := 0;
                    begin
                        select 1,               mqop_050.codigo_estagio,        mqop_050.seq_operacao
                        into v_nr_registro,     v_codigo_estagio050_vld,        v_seq_operacao050_vld
                        from mqop_050
                        where mqop_050.nivel_estrutura  = :new.pano_sbg_nivel99
                        and   mqop_050.grupo_estrutura  = :new.pano_sbg_grupo
                        and   mqop_050.subgru_estrutura = v_sub_ler
                        and   mqop_050.item_estrutura   = v_item_ler
                        and   mqop_050.numero_alternati = :new.alternativa_item
                        and   mqop_050.numero_roteiro   = :new.roteiro_opcional
                        
                        and   (mqop_050.cod_estagio_agrupador  is null or mqop_050.cod_estagio_agrupador  = 0 or        --Sem agrupador (agrupador nulo ou 0)
                               mqop_050.seq_operacao_agrupador is null or mqop_050.seq_operacao_agrupador = 0)

                        and rownum <= 1
                        ;
                    exception when others then
                        v_codigo_estagio050_vld := 0;
                        v_seq_operacao050_vld   := 0;
                        v_nr_registro           := 0;
                    end;

                    if v_nr_registro = 1    --Encontrou registro no roteiro do produto da OB sem agrupador
                    then

                        raise_application_error(-20000, 'ATENCAO! Roteiro do produto da OB possui estagio sem agrupador definido (mqop_050)! Nao e possivel continuar! Acesse o roteiro, gere os agrupadores e tente novamente. Estagio: '||v_codigo_estagio050_vld||'. Seq Operacao: '||v_seq_operacao050_vld);

                    end if;

                end if;     --v_valida_arup_rot_ob = 2     --2: Alerta e Trava

            end if; --v_existe_blocok = 1

        end;

      end if;

      
	  if inserting
      then
         /*
            SS 82559/001 - Valida��o Situa��o Projeto.
         */
         v_codigo_empresa := 0;

         begin
            select pcpc_010.codigo_empresa
            into    v_codigo_empresa
            from pcpc_010
            where pcpc_010.area_periodo     = 2
              and pcpc_010.periodo_producao = v_periodo_producao;
         exception when others then
            v_codigo_empresa := 0;
         end;

         inter_pr_valida_sit_projeto(:new.pano_sbg_nivel99,:new.pano_sbg_grupo,v_codigo_empresa);

      end if;

      -- Se a OB for de Reprocesso nao executa.
      if v_tipo_ordem <> 4
      then
         -- Insere produto para planejamento da producao.
         insert into tmrp_041
            (periodo_producao,       area_producao,
             nr_pedido_ordem,        seq_pedido_ordem,
             codigo_estagio,         nivel_estrutura,
             grupo_estrutura,        subgru_estrutura,
             item_estrutura,         qtde_reservada,
             qtde_areceber,          consumo)
         values
            (v_periodo_producao,     2,
             :new.ordem_producao,    :new.sequencia,
             0,                      :new.pano_sbg_nivel99,
             :new.pano_sbg_grupo,    :new.pano_sbg_subgrupo,
             :new.pano_sbg_item,     0.00,
             :new.qtde_quilos_prog,  0.00);
         -- Procedure que executar a explosao da estrutura do produto
         -- que sera produzido.
         ---
         -- A explosao de estrutura e realizada na insercao de destinos (pcpb_030)
      end if;
   end if;

   if updating
   then
      if :new.qtde_quilos_real <> :old.qtde_quilos_real
      then
         -- Atualiza todos os estagios produzidos e altera a quantidade preparada.
         begin
            for reg_pcpb040 in (select pcpb_040.codigo_estagio,
                                       pcpb_040.data_termino,
                                       pcpb_040.turno_producao
                                from pcpb_040
                                where pcpb_040.ordem_producao = :new.ordem_producao
                                  and pcpb_040.data_termino  is not null)
            loop
               -- Atualiza EXEC_015
               inter_pr_alimenta_exec_015(reg_pcpb040.data_termino,      0,
                                          0,                             :new.pano_sbg_nivel99,
                                          :new.pano_sbg_grupo,           :new.pano_sbg_subgrupo,
                                          reg_pcpb040.codigo_estagio,    reg_pcpb040.turno_producao,
                                          0.000,                         (:new.qtde_quilos_real - :old.qtde_quilos_real));
            end loop;
         end;
      end if;

      -- Busca o periodo e tipo da ordem de producao da OB.
      begin
         select pcpb_010.periodo_producao, pcpb_010.tipo_ordem,
                pcpb_010.situacao_ordem,   pcpb_010.ordem_tingimento
         into   v_periodo_producao,        v_tipo_ordem,
                v_situacao_ordem,          v_ordem_tingimento
         from pcpb_010
         where pcpb_010.ordem_producao = :old.ordem_producao;
         exception
         when others then
            v_periodo_producao := 0;
            v_tipo_ordem       := 0;
            v_situacao_ordem   := 0;
            v_ordem_tingimento := 0;
            raise_application_error(-20000,'Capa da Ordem de Beneficiamento nao cadastrada (PCPB_010).');
      end;
      
     if :new.qtde_quilos_prod < 0 or :new.qtde_rolos_prod < 0
     then
        raise_application_error(-20000,'ATENCAO! Nao sera possivel fazer a entrada de rolos via balanca. Os valores de quantidade de quilos ou rolos produzidos irao ficar negativos.');
     end if;

      if v_tipo_ordem <> 4 -- Reprocesso.
      then
         -- Se informou codigo de cancelamento para a OB deleta do
         -- planejamento da producao.
         if :new.cod_cancelamento > 0
         then
            -- Deleta o produto da ordem de tecelagem e seus componentes.
            delete tmrp_041
            where tmrp_041.area_producao    = 2
              and tmrp_041.nr_pedido_ordem  = :old.ordem_producao
              and tmrp_041.seq_pedido_ordem = :old.sequencia;

            -- Diminui  operacoes e estagios da alternativa e roteiro antigo.
            -- Sempre antes de chamar a procedure para atualizar estagios e operacoes
            -- a logica abaixo deve ser executada para definicao do produto.
            begin
               v_sub_ler  := :old.pano_sbg_subgrupo;
               v_item_ler := :old.pano_sbg_item;
            end;

            begin
               select count(*)
               into v_nr_registro
               from mqop_050
               where mqop_050.nivel_estrutura  = :old.pano_sbg_nivel99
                 and mqop_050.grupo_estrutura  = :old.pano_sbg_grupo
                 and mqop_050.subgru_estrutura = v_sub_ler
                 and mqop_050.item_estrutura   = v_item_ler
                 and mqop_050.numero_alternati = :old.alternativa_item
                 and mqop_050.numero_roteiro   = :old.roteiro_opcional;
               if v_nr_registro = 0
               then
                  v_item_ler := '000000';
                  begin
                     select count(*)
                     into v_nr_registro
                     from mqop_050
                     where mqop_050.nivel_estrutura  = :old.pano_sbg_nivel99
                       and mqop_050.grupo_estrutura  = :old.pano_sbg_grupo
                       and mqop_050.subgru_estrutura = v_sub_ler
                       and mqop_050.item_estrutura   = v_item_ler
                       and mqop_050.numero_alternati = :old.alternativa_item
                       and mqop_050.numero_roteiro   = :old.roteiro_opcional;
                     if v_nr_registro = 0
                     then
                        v_sub_ler  := '000';
                        v_item_ler := :old.pano_sbg_item;
                        begin
                           select count(*)
                           into v_nr_registro
                           from mqop_050
                           where mqop_050.nivel_estrutura  = :old.pano_sbg_nivel99
                             and mqop_050.grupo_estrutura  = :old.pano_sbg_grupo
                             and mqop_050.subgru_estrutura = v_sub_ler
                             and mqop_050.item_estrutura   = v_item_ler
                             and mqop_050.numero_alternati = :old.alternativa_item
                             and mqop_050.numero_roteiro   = :old.roteiro_opcional;
                           if v_nr_registro = 0
                           then
                              v_item_ler := '000000';
                           end if;
                        end;
                     end if;
                  end;
               end if;
            end;

            -- Chama procedure para atualizacao dos estagios e operacoes diminuindo os minutos OB.
            inter_pr_ordem_beneficiamento(:old.pano_sbg_nivel99,
                                          :old.pano_sbg_grupo,
                                          v_sub_ler,
                                          v_item_ler,
                                          :old.alternativa_item,
                                          :old.roteiro_opcional,
                                          :old.ordem_producao,
                                          v_situacao_ordem,
                                          :old.sequencia,
                                          :old.qtde_quilos_prog,
                                          v_tipo_ordem,
                                          'D');
         end if;

         -- Quando for atualizada a quantidade programada da OB
         -- executa o recalculo para o planejamento.
         ---
         -- O estorno/acrescimo dos componentes e feito na alteracao
         -- da quantidade do destino (pcpb_030)
         if :old.qtde_quilos_prog <> :new.qtde_quilos_prog
         then
            v_saldo_rolos_ordem := :old.qtde_quilos_prog - :new.qtde_quilos_prog;
            update tmrp_041
            set tmrp_041.qtde_areceber = tmrp_041.qtde_areceber - v_saldo_rolos_ordem
            where tmrp_041.periodo_producao    = v_periodo_producao
              and tmrp_041.area_producao       = 2
              and tmrp_041.nr_pedido_ordem     = :old.ordem_producao
              and tmrp_041.seq_pedido_ordem    = :old.sequencia
              and tmrp_041.sequencia_estrutura = 0;
         end if;

         -- Quando for atualizada a quantidade produzida da OB
         -- executa o recalculo para o planejamento.
         ---
         -- A alteracao da quantidade produzida nao influencia
         -- nos componentes pela trigger.
         if :old.qtde_quilos_prod <> :new.qtde_quilos_prod
         or :old.qtde_rolos_prod  <> :new.qtde_rolos_prod
         then
            begin
               select empr_002.forma_pesagem_rolo_acabado
               into forma_pesagem_rolo_acabado
               from empr_002;
            end;
            
            if forma_pesagem_rolo_acabado = 1
            then
               if :new.qtde_rolos_prog > 0
               then
                  -- Rolos
                  v_peso_rolo         := (:new.qtde_quilos_prog / :new.qtde_rolos_prog);
                  v_saldo_rolos_ordem := ((:new.qtde_rolos_prod - :old.qtde_rolos_prod) * v_peso_rolo);
               else
                  -- Fios
                  v_saldo_rolos_ordem := :new.qtde_quilos_prod - :old.qtde_quilos_prod;
               end if;
            else
               v_saldo_rolos_ordem := :new.qtde_quilos_prod - :old.qtde_quilos_prod;
            end if;

            if (:old.qtde_rolos_prog  >= :new.qtde_rolos_prod 
            or  :old.qtde_quilos_prog >= :new.qtde_quilos_prod)
            and v_saldo_rolos_ordem <> 0
            then
               update tmrp_041
               set tmrp_041.qtde_areceber  = tmrp_041.qtde_areceber  - v_saldo_rolos_ordem
               where tmrp_041.periodo_producao    = v_periodo_producao
                 and tmrp_041.area_producao       = 2
                 and tmrp_041.nr_pedido_ordem     = :old.ordem_producao
                 and tmrp_041.seq_pedido_ordem    = :old.sequencia
                 and tmrp_041.sequencia_estrutura = 0;

               delete tmrp_041
               where tmrp_041.periodo_producao    = v_periodo_producao
                 and tmrp_041.area_producao       = 2
                 and tmrp_041.nr_pedido_ordem     = :old.ordem_producao
                 and tmrp_041.seq_pedido_ordem    = :old.sequencia
                 and tmrp_041.sequencia_estrutura = 0
                 and tmrp_041.qtde_areceber       <= 0;
            end if;
         end if;
      end if;

      -- Se mudou o roteiro ou alternativa do item da OB a trigger
      -- deleta todas as operacoes que os minutos unitarios fique menor ou igual a 0 (zero)
      -- deletando tambem os estagios relacionados a esta operacao deletada, inserindo novamente
      -- considerando o novo roteiro ou a nova alternativa.
      if :old.alternativa_item <> :new.alternativa_item
      or :old.roteiro_opcional <> :new.roteiro_opcional
      or :old.pano_sbg_item    <> :new.pano_sbg_item
      then
         v_registro := 0;

         select count(*)
         into v_registro
         from pcpb_030
         where pcpb_030.ordem_producao = :new.ordem_producao
           and pcpb_030.sequencia      = :new.sequencia;

         -- Ser o item estiver destinado executa atualizacao com nova
         -- alternativa ou roteiro.
         if v_registro > 0
         then
            begin
               -- Busca a situacao da OB, tipo da OB e Ordem Tingimento.
               select pcpb_010.situacao_ordem,  pcpb_010.tipo_ordem,
                      pcpb_010.ordem_tingimento
               into   v_situacao_ordem,        v_tipo_ordem,
                      v_ordem_tingimento
               from pcpb_010
               where pcpb_010.ordem_producao   = :old.ordem_producao
                 and pcpb_010.cod_cancelamento = 0;
               exception
               when others then
                  v_situacao_ordem   := 0;
                  v_tipo_ordem       := 0;
                  v_ordem_tingimento := 0;
            end;

            -- Diminui operacoes e estagios da alternativa e roteiro antigo.
            -- Sempre antes de chamar a procedure para atualizar estagios e operacoes
            -- a logica abaixo deve ser executada para definicao do produto.
            begin
               v_sub_ler  := :old.pano_sbg_subgrupo;
               v_item_ler := :old.pano_sbg_item;
            end;

            begin
               select count(*)
               into v_nr_registro
               from mqop_050
               where mqop_050.nivel_estrutura  = :old.pano_sbg_nivel99
                 and mqop_050.grupo_estrutura  = :old.pano_sbg_grupo
                 and mqop_050.subgru_estrutura = v_sub_ler
                 and mqop_050.item_estrutura   = v_item_ler
                 and mqop_050.numero_alternati = :old.alternativa_item
                 and mqop_050.numero_roteiro   = :old.roteiro_opcional;
               if v_nr_registro = 0
               then
                  v_item_ler := '000000';
                  begin
                     select count(*)
                     into v_nr_registro
                     from mqop_050
                     where mqop_050.nivel_estrutura  = :old.pano_sbg_nivel99
                       and mqop_050.grupo_estrutura  = :old.pano_sbg_grupo
                       and mqop_050.subgru_estrutura = v_sub_ler
                       and mqop_050.item_estrutura   = v_item_ler
                       and mqop_050.numero_alternati = :old.alternativa_item
                       and mqop_050.numero_roteiro   = :old.roteiro_opcional;
                     if v_nr_registro = 0
                     then
                        v_sub_ler  := '000';
                        v_item_ler := :old.pano_sbg_item;
                        begin
                           select count(*)
                           into v_nr_registro
                           from mqop_050
                           where mqop_050.nivel_estrutura  = :old.pano_sbg_nivel99
                             and mqop_050.grupo_estrutura  = :old.pano_sbg_grupo
                             and mqop_050.subgru_estrutura = v_sub_ler
                             and mqop_050.item_estrutura   = v_item_ler
                             and mqop_050.numero_alternati = :old.alternativa_item
                             and mqop_050.numero_roteiro   = :old.roteiro_opcional;
                           if v_nr_registro = 0
                           then
                              v_item_ler := '000000';

                           end if;
                        end;
                     end if;
                  end;
               end if;
            end;

            -- Chama procedure para geracao dos estagios e operacoes para OB.
            inter_pr_ordem_beneficiamento(:old.pano_sbg_nivel99,
                                          :old.pano_sbg_grupo,
                                          v_sub_ler,
                                          v_item_ler,
                                          :old.alternativa_item,
                                          :old.roteiro_opcional,
                                          :old.ordem_producao,
                                          v_situacao_ordem,
                                          :old.sequencia,
                                          :old.qtde_quilos_prog,
                                          v_tipo_ordem,
                                          'D');

            -- Sempre antes de chamar a procedure para atualizar estagios e operacoes
            -- a logica abaixo deve ser executada para definicao do produto.
            begin
               v_sub_ler  := :old.pano_sbg_subgrupo;
               v_item_ler := :new.pano_sbg_item;
            end;

            if :old.pano_sbg_item = '009999'
            then
               -- Manda como nao emitida, para gerar os demais estagios
               -- necessarios quando a cor antiga era termofixada.
               v_situacao_ordem := 0;
            end if;

            begin
               select count(*)
               into v_nr_registro
               from mqop_050
               where mqop_050.nivel_estrutura  = :old.pano_sbg_nivel99
                 and mqop_050.grupo_estrutura  = :old.pano_sbg_grupo
                 and mqop_050.subgru_estrutura = v_sub_ler
                 and mqop_050.item_estrutura   = v_item_ler
                 and mqop_050.numero_alternati = :new.alternativa_item
                 and mqop_050.numero_roteiro   = :new.roteiro_opcional;
               if v_nr_registro = 0
               then
                  v_item_ler := '000000';
                  begin
                     select count(*)
                     into v_nr_registro
                     from mqop_050
                     where mqop_050.nivel_estrutura  = :old.pano_sbg_nivel99
                       and mqop_050.grupo_estrutura  = :old.pano_sbg_grupo
                       and mqop_050.subgru_estrutura = v_sub_ler
                       and mqop_050.item_estrutura   = v_item_ler
                       and mqop_050.numero_alternati = :new.alternativa_item
                       and mqop_050.numero_roteiro   = :new.roteiro_opcional;
                     if v_nr_registro = 0
                     then
                        v_sub_ler  := '000';
                        v_item_ler := :old.pano_sbg_item;
                        begin
                           select count(*)
                           into v_nr_registro
                           from mqop_050
                           where mqop_050.nivel_estrutura  = :old.pano_sbg_nivel99
                             and mqop_050.grupo_estrutura  = :old.pano_sbg_grupo
                             and mqop_050.subgru_estrutura = v_sub_ler
                             and mqop_050.item_estrutura   = v_item_ler
                             and mqop_050.numero_alternati = :new.alternativa_item
                             and mqop_050.numero_roteiro   = :new.roteiro_opcional;
                           if v_nr_registro = 0
                           then
                              v_item_ler := '000000';
                           end if;
                        end;
                     end if;
                  end;
               end if;
            end;

            -- Chama procedure para geracao dos estagios e operacoes para OB.
            inter_pr_ordem_beneficiamento(:old.pano_sbg_nivel99,
                                          :old.pano_sbg_grupo,
                                          v_sub_ler,
                                          v_item_ler,
                                          :new.alternativa_item,
                                          :new.roteiro_opcional,
                                          :old.ordem_producao,
                                          v_situacao_ordem,
                                          :old.sequencia,
                                          :old.qtde_quilos_prog,
                                          v_tipo_ordem,
                                          'I');

            -- Busca a data de embarque/entrega do pedido de venda destinado a OB
            -- para calcular com o centro de custo os dias para producao.
            v_data_embarque := null;

            begin
               select min(pedi_100.data_entr_venda)
               into   v_data_embarque
               from pedi_100, pcpb_030
               where pedi_100.pedido_venda   = pcpb_030.nr_pedido_ordem
                 and pcpb_030.ordem_producao = :old.ordem_producao
                 and pcpb_030.pedido_corte   = 3;
            end;

            -- Chama procedure para calcular a prioridade das operacoes.
            inter_pr_calcula_seq_ob(:old.ordem_producao,
                                    v_ordem_tingimento,
                                    v_data_embarque,
                                    :old.sequencia,
                                    'PCPB_020',
                                    :old.pano_sbg_nivel99,
                                    :old.pano_sbg_grupo,
                                    v_sub_ler,
                                    v_item_ler,
                                    :new.alternativa_item,
                                    :new.roteiro_opcional);

         end if;
      end if;

      -- Quando muda a alternativa elimina a necessidade da ordem e os quimicos da ordem
      -- executando o recalculo das necessidades para nova alternativa.
      -- O recalculo da necessidade dos quimicos nao e executada, apenas a situacao da ordem
      -- e alterada para emissao e recalculo dos quimicos via programa.
      if :old.alternativa_item <> :new.alternativa_item
      then
         -- Busca o periodo e tipo da ordem de producao da OB.
         begin
            select pcpb_010.periodo_producao, pcpb_010.tipo_ordem,
                   pcpb_010.ordem_tingimento
            into   v_periodo_producao,        v_tipo_ordem,
                   v_ordem_tingimento
            from pcpb_010
            where pcpb_010.ordem_producao = :new.ordem_producao;
            exception
            when others then
               v_periodo_producao := 0;
               v_tipo_ordem       := 0;
               v_ordem_tingimento := 0;
               raise_application_error(-20000,'Capa da Ordem de Beneficiamento nao cadastrada (PCPB_010).');
         end;

         if v_tipo_ordem <> 4 -- Reprocesso.
         then
            -- Elimina do planejamento a quantidade necessaria para ordem de beneficiamento
            -- e seus componentes.
            delete tmrp_041
            where tmrp_041.area_producao    = 2
              and tmrp_041.nr_pedido_ordem  = :old.ordem_producao
              and tmrp_041.seq_pedido_ordem = :old.sequencia;

            -- Insere produto para planejamento da producao.
            insert into tmrp_041
               (periodo_producao,       area_producao,
                nr_pedido_ordem,        seq_pedido_ordem,
                codigo_estagio,         nivel_estrutura,
                grupo_estrutura,        subgru_estrutura,
                item_estrutura,         qtde_reservada,
                qtde_areceber,          consumo)
            values
               (v_periodo_producao,     2,
                :new.ordem_producao,    :new.sequencia,
                0,                      :new.pano_sbg_nivel99,
                :new.pano_sbg_grupo,    :new.pano_sbg_subgrupo,
                :new.pano_sbg_item,     0.00,
                :new.qtde_quilos_prog,  0.00);

            -- Procedure que executar a explosao da estrutura do produto
            -- que sera produzido.
            ---
            -- A explosao de estrutura e realizada no updating do campo
            -- alternativa nos destinos (pcpb_030)
            update pcpb_030
            set pcpb_030.alternativa = :new.alternativa_item
            where pcpb_030.ordem_producao = :new.ordem_producao
              and pcpb_030.pano_nivel99   = :new.pano_sbg_nivel99
              and pcpb_030.pano_grupo     = :new.pano_sbg_grupo
              and pcpb_030.pano_subgrupo  = :new.pano_sbg_subgrupo
              and pcpb_030.pano_item      = :new.pano_sbg_item
              and pcpb_030.sequencia      = :new.sequencia;

         end if;

         -- Deleta os quimicos da ordem(s) de producao e atualiza a situacao
         -- das ordens para recalcular na emissao das ordens.
         if v_ordem_tingimento <> 0
         then
            delete pcpb_080
            where pcpb_080.ordem_producao = v_ordem_tingimento;

            update pcpb_010
            set pcpb_010.situacao_ordem = 1
            where pcpb_010.ordem_tingimento = v_ordem_tingimento
              and pcpb_010.situacao_ordem   = 2;

            update pcpb_100
            set pcpb_100.situacao         = '0',
                pcpb_100.situacao_receita = '0'
            where pcpb_100.tipo_ordem        = '1'
              and pcpb_100.ordem_agrupamento = v_ordem_tingimento
              and pcpb_100.situacao_receita  = '1';
         else
            delete pcpb_080
            where pcpb_080.ordem_producao = :old.ordem_producao;

            update pcpb_010
            set pcpb_010.situacao_ordem = 1
            where pcpb_010.ordem_producao = :old.ordem_producao
              and pcpb_010.situacao_ordem = 2;
         end if;
      end if;
   end if;

   if (updating and (:old.cod_cancelamento = 0 and :old.cod_cancelamento <> :new.cod_cancelamento)) or deleting
   then
      -- Deleta o destino do destino quando for ordem de corte
      select count(1)
      into v_nr_registro_031
      from pcpb_031
      where pcpb_031.ordem_producao = :old.ordem_producao;
      if v_nr_registro_031 > 0
      then
         begin
            delete pcpb_031
            where pcpb_031.ordem_producao = :old.ordem_producao;
         end;
      end if;
   end if;

   if deleting
   then
      -- Busca o periodo e tipo da ordem de producao da OB.
      begin
         select pcpb_010.tipo_ordem
         into   v_tipo_ordem
         from pcpb_010
         where pcpb_010.ordem_producao = :old.ordem_producao;
         exception
         when others then
            v_tipo_ordem       := 0;
            raise_application_error(-20000,'Capa da Ordem de Beneficiamento nao cadastrada (PCPB_010).');
      end;

      if v_tipo_ordem <> 4 -- Reprocesso.
      then
         -- Elimina do planejamento a quantidade necessaria para ordem de beneficiamento
         -- e seus componentes.
         delete tmrp_041
         where tmrp_041.area_producao    = 2
           and tmrp_041.nr_pedido_ordem  = :old.ordem_producao
           and tmrp_041.seq_pedido_ordem = :old.sequencia;

         -- Se nao encontrar nenhum registro no planejamento, verifica se pode
         -- eliminar os estagios.
         begin
            select count(*)
            into v_nr_registro_tmrp41
            from tmrp_041
            where tmrp_041.area_producao    = 2
              and tmrp_041.nr_pedido_ordem  = :old.ordem_producao;

            if v_nr_registro_tmrp41 = 0
            then
               -- Se nao encontrar nenhum estagio com producao iniciada, elimina.
               begin
                  select count(*)
                  into v_nr_registro_pcpb40
                  from pcpb_040
                  where pcpb_040.ordem_producao   = :old.ordem_producao
                    and pcpb_040.data_inicio is not null;

                  if v_nr_registro_pcpb40 = 0
                  then
                     delete pcpb_015
                     where pcpb_015.ordem_producao = :old.ordem_producao;

                     delete pcpb_040
                     where pcpb_040.ordem_producao = :old.ordem_producao;
                  end if;
               end;
            end if;
         end;
      end if;
   end if;
end inter_tr_pcpb_020;

-- ALTER TRIGGER "INTER_TR_PCPB_020" ENABLE
 

/

exec inter_pr_recompile;

