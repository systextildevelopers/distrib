alter table blocok_220 add (
COD_ITEM_NIVEL_ORI                   VARCHAR2(1),
COD_ITEM_GRUPO_ORI                   VARCHAR2(5),
COD_ITEM_SUBGRUPO_ORI                VARCHAR2(3),
COD_ITEM_ITEM_ORI                    VARCHAR2(6),
COD_ITEM_EST_AGRUP_INSU_ORI          NUMBER(2),
COD_ITEM_EST_SIMULT_INSU_ORI         NUMBER(2),
COD_ITEM_NIVEL_DEST                   VARCHAR2(1),
COD_ITEM_GRUPO_DEST                   VARCHAR2(5),
COD_ITEM_SUBGRUPO_DEST                VARCHAR2(3),
COD_ITEM_ITEM_DEST                    VARCHAR2(6),
COD_ITEM_EST_AGRUP_INSU_DEST          NUMBER(2),
COD_ITEM_EST_SIMULT_INSU_DEST         NUMBER(2)
);
exec inter_pr_recompile;
