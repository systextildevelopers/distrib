CREATE OR REPLACE PROCEDURE "INTER_PR_GERA_IMAGEM_PLANEJ"
	(
     p_colecao_1 number,
     p_colecao_2 number,
     p_colecao_3 number,
     p_colecao_4 number,
     p_data_ini date,
     p_data_fim date,
     p_periodo_ini_venda number,
     p_periodo_fim_venda number,
     p_periodo_ini_prog number,
     p_periodo_fim_prog number,
     p_cod_empresa number,
     p_cod_deposito1 number,
     p_cod_deposito2 number,
     p_cod_deposito3 number,
     p_cod_deposito4 number,
     p_codigo_usuario number,
     p_referencia number
   )
is

begin
   declare
      v_vendido_semana   number(9);
      v_vendido          number(9);
      v_saldo            number(9);
      v_estoque          number(9);
      v_estoque_calc     number(9);
      v_areceber         number(9);
      v_conserto         number(9);


   begin
      
      -- Limpa tabela temporária da foto do planejamento...
      begin
         delete from PEDI_994
         where PEDI_994.NR_SOLICITACAO = 1;
      end;

      for reg_planejamento in (

         select basi_010.nivel_estrutura,
            basi_010.grupo_estrutura,
            basi_010.subgru_estrutura,
            basi_010.item_estrutura
         from basi_010,
            basi_030
         where basi_010.nivel_estrutura  = basi_030.nivel_estrutura
         and basi_010.grupo_estrutura  = basi_030.referencia
       
         --pega os grupos (referencias) informados no cadastro temporario
         and (
            (p_referencia = 1
               and exists (select rcnb_060.grupo_estrutura_str from rcnb_060
                  where rcnb_060.tipo_registro = 941
                  and rcnb_060.nr_solicitacao = 994
                  and rcnb_060.subgru_estrutura = p_codigo_usuario
                  and rcnb_060.grupo_estrutura_str = basi_010.grupo_estrutura
               )
            )
            or (p_referencia = 2
                and ( exists (select rcnb_060.grupo_estrutura_str from rcnb_060
                              where rcnb_060.tipo_registro = 941
                              and rcnb_060.nr_solicitacao = 994
                              and rcnb_060.subgru_estrutura = p_codigo_usuario
                              and rcnb_060.grupo_estrutura_str = 'XXXXXX'
                             )
                or not exists (select rcnb_060.grupo_estrutura_str from rcnb_060
                               where rcnb_060.tipo_registro = 941
                               and rcnb_060.nr_solicitacao = 994
                               and rcnb_060.subgru_estrutura = p_codigo_usuario
                               and rcnb_060.grupo_estrutura_str = basi_010.grupo_estrutura
                              )
                )
            )
         )
         and basi_010.nivel_estrutura  = '1'
         and basi_030.colecao          in (p_colecao_1,p_colecao_2,p_colecao_3,p_colecao_4)
         order by basi_010.nivel_estrutura,
                  basi_010.grupo_estrutura,
                  basi_010.subgru_estrutura,
                  basi_010.item_estrutura
      )
      loop

         -- *************************************************************
         -- Quantidade de peças vendidas de saldo, anterior do período...
         -- *************************************************************

         v_vendido := 0;

         begin
         
            select nvl(sum(pedi_110.qtde_pedida),0) - nvl(sum(pedi_110.qtde_faturada),0)
            into v_vendido
            from pedi_110,
                 pedi_100,
                 basi_030
            where pedi_100.pedido_venda         = pedi_110.pedido_venda
            and   pedi_110.cd_it_pe_nivel99     = basi_030.nivel_estrutura
            and   pedi_110.cd_it_pe_grupo       = basi_030.referencia
            and   pedi_100.codigo_empresa       = p_cod_empresa
            and   (pedi_100.status_pedido       = 2  -- Liberado Financeiro...
            or    pedi_100.status_pedido        = 1  -- Financeiro...
            or    pedi_100.status_pedido        = 0) -- Digitado...
            and   basi_030.colecao             in (p_colecao_1,p_colecao_2,p_colecao_3,p_colecao_4) -- Coleção dos produtos dos pedidos...
            --and  pedi_100.num_periodo_prod     < v_semana_demanda -- Todos os períodos anteriores ao atual para carga demanda...
            and   pedi_100.num_periodo_prod    between p_periodo_ini_venda and p_periodo_fim_venda
            and   pedi_110.cd_it_pe_nivel99    = reg_planejamento.nivel_estrutura
            and   pedi_110.cd_it_pe_grupo      = reg_planejamento.grupo_estrutura
            and   pedi_110.cd_it_pe_subgrupo   = reg_planejamento.subgru_estrutura
            and   pedi_110.cd_it_pe_item       = reg_planejamento.item_estrutura
            and   pedi_110.cod_cancelamento    = 0 -- Fixo, não considera itens cancelados...
            and   pedi_100.situacao_venda      <> 10; -- Faturado total...
         end;
        
         -- *************************************************************
         -- Quantidade de peças vendidas no período...
         -- *************************************************************
         
         v_vendido_semana := 0;
     
         begin 
            select nvl(sum(pedi_110.qtde_pedida),0)
            into v_vendido_semana
            from pedi_110, 
                 pedi_100,
                 basi_030
            where pedi_100.pedido_venda         =  pedi_110.pedido_venda
            and   pedi_110.cd_it_pe_nivel99     =  basi_030.nivel_estrutura
            and   pedi_110.cd_it_pe_grupo       =  basi_030.referencia
            and   pedi_100.codigo_empresa       = p_cod_empresa
            and   (pedi_100.status_pedido       = 2  -- Liberado Financeiro...
            or    pedi_100.status_pedido        = 1  -- Financeiro...
            or    pedi_100.status_pedido        = 0) -- Digitado...
            and   basi_030.colecao              in (p_colecao_1,p_colecao_2,p_colecao_3,p_colecao_4) -- Coleção dos produtos dos pedidos...
            --and  pedi_100.num_periodo_prod     = v_semana_demanda  -- Período atual que está sendo planejado...
            and   pedi_100.num_periodo_prod     between p_periodo_ini_prog and p_periodo_fim_prog  -- Período atual que está sendo planejado...
            and   pedi_100.data_entr_venda      between p_data_ini and p_data_fim
            and   pedi_110.cd_it_pe_nivel99     = reg_planejamento.nivel_estrutura
            and   pedi_110.cd_it_pe_grupo       = reg_planejamento.grupo_estrutura
            and   pedi_110.cd_it_pe_subgrupo    = reg_planejamento.subgru_estrutura
            and   pedi_110.cd_it_pe_item        = reg_planejamento.item_estrutura
            and   pedi_110.cod_cancelamento     = 0 -- Fixo, não considera itens cancelados...
            and   pedi_100.situacao_venda       <> 10; -- Faturado total...
         end;

         -- *************************************************************
         -- Carga da quantidade DE ESTOQUE do produto...
         -- *************************************************************
        
         v_estoque := 0;

         begin
            select nvl(sum(estq_040.qtde_estoque_atu),0)
            into v_estoque
            from estq_040
            where estq_040.cditem_nivel99  = '1'
            and   estq_040.cditem_nivel99  = reg_planejamento.nivel_estrutura
            and   estq_040.cditem_grupo    = reg_planejamento.grupo_estrutura
            and   estq_040.cditem_subgrupo = reg_planejamento.subgru_estrutura
            and   estq_040.cditem_item     = reg_planejamento.item_estrutura
            --pega os códigos de depósitos informados na tela
            and   estq_040.deposito        in (p_cod_deposito1, p_cod_deposito2, p_cod_deposito3, p_cod_deposito4);
         end;

        -- *************************************************************
        -- Carga da quantidade A RECEBER do produto...
        -- *************************************************************
        
         v_areceber := 0;
         v_conserto := 0;

         begin
            select nvl(sum(pcpc_040.qtde_a_produzir_pacote),0)
            into v_areceber
            from pcpc_040,
                 pcpc_020
            where pcpc_040.ordem_producao        = pcpc_020.ordem_producao
            and   pcpc_040.codigo_estagio        = pcpc_020.ultimo_estagio
            and   pcpc_040.proconf_nivel99       = reg_planejamento.nivel_estrutura
            and   pcpc_040.proconf_grupo         = reg_planejamento.grupo_estrutura
            and   pcpc_040.proconf_subgrupo      = reg_planejamento.subgru_estrutura
            and   pcpc_040.proconf_item          = reg_planejamento.item_estrutura
            and   pcpc_020.cod_cancelamento      = 0
            --and pcpc_040.periodo_producao not in (9635,1853,2018,2019,2020) -- Fixo, "período lixo"
            and   pcpc_040.situacao_ordem        = 1
            and   pcpc_040.periodo_producao      between p_periodo_ini_venda and (p_periodo_fim_venda+1); -- Conforme alinhamento com PCP, Mari
         end;

         begin
            select nvl(sum(pcpc_040.qtde_conserto),0)
            into v_conserto
            from pcpc_040,
                 pcpc_020
            where pcpc_040.ordem_producao        = pcpc_020.ordem_producao
            and   pcpc_040.proconf_nivel99       = reg_planejamento.nivel_estrutura
            and   pcpc_040.proconf_grupo         = reg_planejamento.grupo_estrutura
            and   pcpc_040.proconf_subgrupo      = reg_planejamento.subgru_estrutura
            and   pcpc_040.proconf_item          = reg_planejamento.item_estrutura
            and   pcpc_020.cod_cancelamento      = 0
            --and pcpc_040.periodo_producao not in (9635,1853,2018,2019,2020) -- Fixo, "período lixo"
            and   pcpc_040.situacao_ordem        = 1
            and   pcpc_040.periodo_producao      between p_periodo_ini_venda and (p_periodo_fim_venda+1); -- Conforme alinhamento com PCP, Mari
         end;

         -- Calcula o "estoque" considerando o a receber menos o vendido em aberto das semanas anteriores.
        
         v_estoque_calc := ((v_estoque + v_areceber + v_conserto) - v_vendido);

         -- O saldo final, é a quantidade vendida da semana menos o saldo em estoque.
        
         v_saldo := 0;

         if v_vendido_semana > 0
         then
            v_saldo := v_estoque_calc - v_vendido_semana;
         else
            v_saldo := v_estoque_calc;
         end if;

         begin
            insert into PEDI_994 (
               NR_SOLICITACAO,
               NIVEL,
               GRUPO,
               SUBGRUPO,
               ITEM,
               QTDE_PEDIDO_SEM,
               QTDE_PEDIDO_SEM_ANT,
               QTDE_ESTOQUE_040,
               QTDE_ESTOQUE_CALC,
               QTDE_RECEBER,
               QTDE_RECEBER_CONS,
               QTDE_SALDO,
               QTDE_RESERVA
            ) values (
               1,
               reg_planejamento.nivel_estrutura,
               reg_planejamento.grupo_estrutura,
               reg_planejamento.subgru_estrutura,
               reg_planejamento.item_estrutura,
               v_vendido_semana,
               v_vendido,
               v_estoque,
               v_estoque_calc,
               v_areceber,
               v_conserto,
               v_saldo,
               0
            );
         end;
         
         commit;
         
      end loop;
   end;
end INTER_PR_GERA_IMAGEM_PLANEJ;

/

exec inter_pr_recompile;
