create or replace trigger inter_tr_pcpc_325_rem
  after delete on pcpc_325
  for each row
declare
  ws_usuario_rede           varchar2(20) ;
begin
  ws_usuario_rede := substr(sys_context('USERENV', 'OS_USER'),1,20);
   
  --Ao deletar TAG do volume libera a mesma para farutamento.
  begin
    update pcpc_330 
    set pcpc_330.sit_faturamento = 0
    where pcpc_330.nr_volume = :old.NUMERO_VOLUME;
    exception
       when others then
            null;
  end;
 
 INSERT INTO PCPC_325_REM (
    USUARIO_REM,
    DATA_REM,
    NUMERO_VOLUME,
    NIVEL,
    GRUPO,
    SUB,
    ITEM,
    PERIODO_ORDEM,
    ORDEM_CONFECCAO,
    QTDE_PECAS_REAL,
    QTDE_PECAS_SUG,
    NUMERO_KIT,
    NIVEL_ESTOQUE,
    GRUPO_ESTOQUE,
    SUB_ESTOQUE,
    ITEM_ESTOQUE,
    QTDE_QUILOS_ESTOQUE,
    ARTIGO_PRODUTO,
    SEQ_ITEM_PEDIDO,
    QTDE_QUILOS_REAL,
    EXECUTA_TRIGGER,
    NOME_PROGRAMA,
    USUARIO_SISTEMA,
    DEPOSITO_ENTRADA_ITEM,
    QTDE_PECAS_ATEND,
    DEPOSITO_SAIDA_ITEM,
    ORDEM_PRODUCAO,
    SEQUENCIA_PACK
  ) VALUES (
    ws_usuario_rede,
    sysdate,
    :old.NUMERO_VOLUME,
    :old.NIVEL,
    :old.GRUPO,
    :old.SUB,
    :old.ITEM,
    :old.PERIODO_ORDEM,
    :old.ORDEM_CONFECCAO,
    :old.QTDE_PECAS_REAL,
    :old.QTDE_PECAS_SUG,
    :old.NUMERO_KIT,
    :old.NIVEL_ESTOQUE,
    :old.GRUPO_ESTOQUE,
    :old.SUB_ESTOQUE,
    :old.ITEM_ESTOQUE,
    :old.QTDE_QUILOS_ESTOQUE,
    :old.ARTIGO_PRODUTO,
    :old.SEQ_ITEM_PEDIDO,
    :old.QTDE_QUILOS_REAL,
    :old.EXECUTA_TRIGGER,
    :old.NOME_PROGRAMA,
    :old.USUARIO_SISTEMA,
    :old.DEPOSITO_ENTRADA_ITEM,
    :old.QTDE_PECAS_ATEND,
    :old.DEPOSITO_SAIDA_ITEM,
    :old.ORDEM_PRODUCAO,
    :old.SEQUENCIA_PACK
  );

end inter_tr_pcpc_325_rem;

/

exec inter_pr_recompile;

exit;
