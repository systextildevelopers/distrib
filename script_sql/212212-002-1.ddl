alter table OBRF_923
  drop constraint UNIQ_OBRF_923 cascade;
 
drop index UNIQ_OBRF_923;


alter table OBRF_923 add constraint UNIQ_OBRF_923 unique (id_921,num_doc,ser,cnpj9,cnpj4,cnpj2,nivel,grupo,subgrupo,item);



alter table OBRF_923 drop column cod_part;
alter table OBRF_923 drop column sub;
alter table OBRF_923 drop column cod_item;
