
  CREATE OR REPLACE TRIGGER "INTER_TR_PLAN_MOV_MQOP_050" 
   after insert or delete or update
       of MINUTOS, CODIGO_ESTAGIO
   on mqop_050
   for each row

declare
   ws_usuario_rede           varchar2(20);
   ws_maquina_rede           varchar2(40);
   ws_aplicativo             varchar2(20);
   ws_sid                    number(9);
   ws_empresa                number(3);
   ws_usuario_systextil      varchar2(250);
   ws_locale_usuario         varchar2(5);

   v_executa_trigger number(1);

   v_nivel_cons      varchar2(1);
   v_grupo_cons      varchar2(5);
   v_subgrupo_cons   varchar2(3);
   v_item_cons       varchar2(6);
   v_alt_cons        number(2);
   v_rot_cons        number(2);

begin
   v_executa_trigger := 1;

   if inserting
   then
      v_nivel_cons    := :new.nivel_estrutura;
      v_grupo_cons    := :new.grupo_estrutura;
      v_subgrupo_cons := :new.subgru_estrutura;
      v_item_cons     := :new.item_estrutura;
      v_alt_cons      := :new.numero_alternati;
      v_rot_cons      := :new.numero_roteiro;
   else
      v_nivel_cons    := :old.nivel_estrutura;
      v_grupo_cons    := :old.grupo_estrutura;
      v_subgrupo_cons := :old.subgru_estrutura;
      v_item_cons     := :old.item_estrutura;
      v_alt_cons      := :old.numero_alternati;
      v_rot_cons      := :old.numero_roteiro;
   end if;

   if v_executa_trigger = 1
   then
      -- Dados do usuario logado
      inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                              ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

      for reg_o_planej in(select tmrp_virtual.ordem_planejamento,
                                 tmrp_virtual.pedido_venda,
                                 tmrp_virtual.nivel_produto_origem,
                                 tmrp_virtual.grupo_produto_origem
                          from (
                          select tmrp_610.ordem_planejamento,
                                 decode(tmrp_625.pedido_venda,0,tmrp_625.pedido_reserva,tmrp_625.pedido_venda) pedido_venda,
                                 '0'     as nivel_produto_origem,
                                 '00000' as grupo_produto_origem
                          from tmrp_625, tmrp_620, tmrp_610
                          where   tmrp_620.ordem_planejamento       = tmrp_625.ordem_planejamento
                            and   tmrp_620.pedido_venda             = tmrp_625.pedido_venda
                            and   tmrp_620.pedido_reserva           = tmrp_625.pedido_reserva
                            and   tmrp_620.nivel_produto            = tmrp_625.nivel_produto
                            and   tmrp_620.grupo_produto            = tmrp_625.grupo_produto
                            and   tmrp_620.subgrupo_produto         = tmrp_625.subgrupo_produto
                            and   tmrp_620.item_produto             = tmrp_625.item_produto
                            and   tmrp_620.alternativa_produto      = v_alt_cons
                            and   tmrp_620.roteiro_produto          = v_rot_cons
                            and   tmrp_625.ordem_planejamento       = tmrp_610.ordem_planejamento
                            and   tmrp_610.situacao_ordem           < 9
                            and   tmrp_625.nivel_produto_origem     = '0'
                            and   tmrp_625.grupo_produto_origem     = '00000'
                            and   tmrp_625.subgrupo_produto_origem  = '000'
                            and   tmrp_625.item_produto_origem      = '000000'
                            and   tmrp_625.nivel_produto            = v_nivel_cons
                            and   tmrp_625.grupo_produto            = v_grupo_cons
                            and  (tmrp_625.subgrupo_produto         = v_subgrupo_cons or v_subgrupo_cons = '000')
                            and  (tmrp_625.item_produto             = v_item_cons     or v_item_cons     = '000000')
                            and ((tmrp_625.qtde_areceber_programada > 0
                            and exists (select 1 from tmrp_041, pcpc_020, tmrp_630
                                        where tmrp_041.periodo_producao   = pcpc_020.periodo_producao
                                          and tmrp_041.area_producao      = 1
                                          and tmrp_041.nr_pedido_ordem    = tmrp_630.ordem_prod_compra
                                          and tmrp_041.codigo_estagio     = pcpc_020.ultimo_estagio
                                          and tmrp_041.nivel_estrutura    = tmrp_625.nivel_produto
                                          and tmrp_041.grupo_estrutura    = tmrp_625.grupo_produto
                                          and tmrp_041.subgru_estrutura   = tmrp_625.subgrupo_produto
                                          and tmrp_041.item_estrutura     = tmrp_625.item_produto
                                          and tmrp_041.qtde_areceber      > 0
                                          and pcpc_020.ordem_producao     = tmrp_630.ordem_prod_compra
                                          and pcpc_020.cod_cancelamento   = 0
                                          and tmrp_630.ordem_planejamento = tmrp_610.ordem_planejamento
                                          and tmrp_630.area_producao      = 1))
                            or tmrp_625.qtde_areceber_programada = 0)
                          group by tmrp_610.ordem_planejamento,
                                   decode(tmrp_625.pedido_venda,0,tmrp_625.pedido_reserva,tmrp_625.pedido_venda),
                                   tmrp_625.nivel_produto_origem,
                                   tmrp_625.grupo_produto_origem
                          UNION ALL
                          select tmrp_610.ordem_planejamento,
                                 decode(tmrp_625.pedido_venda,0,tmrp_625.pedido_reserva,tmrp_625.pedido_venda) pedido_venda,
                                 tmrp_625.nivel_produto_origem,
                                 tmrp_625.grupo_produto_origem
                          from tmrp_625, tmrp_620, tmrp_610
                          where   tmrp_620.ordem_planejamento       = tmrp_625.ordem_planejamento
                            and   tmrp_620.pedido_venda             = tmrp_625.pedido_venda
                            and   tmrp_620.pedido_reserva           = tmrp_625.pedido_reserva
                            and   tmrp_620.nivel_produto            = tmrp_625.nivel_produto_origem
                            and   tmrp_620.grupo_produto            = tmrp_625.grupo_produto_origem
                            and   tmrp_620.subgrupo_produto         = tmrp_625.subgrupo_produto_origem
                            and   tmrp_620.item_produto             = tmrp_625.item_produto_origem
                            and   tmrp_620.alternativa_produto      = v_alt_cons
                            and   tmrp_620.roteiro_produto          = v_rot_cons
                            and   tmrp_625.ordem_planejamento       = tmrp_610.ordem_planejamento
                            and   tmrp_610.situacao_ordem           < 9
                            and   tmrp_625.nivel_produto_origem     = '1'
                            and   tmrp_625.nivel_produto            = v_nivel_cons
                            and   tmrp_625.grupo_produto            = v_grupo_cons
                            and  (tmrp_625.subgrupo_produto         = v_subgrupo_cons or v_subgrupo_cons = '000')
                            and  (tmrp_625.item_produto             = v_item_cons     or v_item_cons     = '000000')
                            and ((tmrp_625.qtde_areceber_programada > 0
                            and exists (select 1 from tmrp_041, pcpc_020, tmrp_630
                                        where tmrp_041.periodo_producao   = pcpc_020.periodo_producao
                                          and tmrp_041.area_producao      = 1
                                          and tmrp_041.nr_pedido_ordem    = tmrp_630.ordem_prod_compra
                                          and tmrp_041.codigo_estagio     = pcpc_020.ultimo_estagio
                                          and tmrp_041.nivel_estrutura    = tmrp_625.nivel_produto
                                          and tmrp_041.grupo_estrutura    = tmrp_625.grupo_produto
                                          and tmrp_041.subgru_estrutura   = tmrp_625.subgrupo_produto
                                          and tmrp_041.item_estrutura     = tmrp_625.item_produto
                                          and tmrp_041.qtde_areceber      > 0
                                          and pcpc_020.ordem_producao     = tmrp_630.ordem_prod_compra
                                          and pcpc_020.cod_cancelamento   = 0
                                          and tmrp_630.ordem_planejamento = tmrp_610.ordem_planejamento
                                          and tmrp_630.area_producao      = 1))
                            or tmrp_625.qtde_areceber_programada = 0)
                          group by tmrp_610.ordem_planejamento,
                                 decode(tmrp_625.pedido_venda,0,tmrp_625.pedido_reserva,tmrp_625.pedido_venda),
                                 tmrp_625.nivel_produto_origem,
                                 tmrp_625.grupo_produto_origem) tmrp_virtual
                          group by tmrp_virtual.ordem_planejamento,
                                   tmrp_virtual.pedido_venda,
                                   tmrp_virtual.nivel_produto_origem,
                                   tmrp_virtual.grupo_produto_origem)
      loop
          -- Alteracao dos minutos ou do estagio.
          if updating
          then
             if :old.minutos <> :new.minutos
             then
                inter_pr_insere_motivo_plan(
                  reg_o_planej.ordem_planejamento,        reg_o_planej.pedido_venda,
                  v_nivel_cons,                           v_grupo_cons,
                  v_subgrupo_cons,                        v_item_cons,
                  reg_o_planej.nivel_produto_origem,      reg_o_planej.grupo_produto_origem,
                  --Pendente                              Roteiro
                  0,                                      3,
                  -- ROTEIRO DE FABRICACAO ALTERADO
                  inter_fn_buscar_tag_composta('lb34586', '','','','','','','','','','', ws_locale_usuario,ws_usuario_systextil),

                  --Os minutos do roteiro {0}, da alternativa {1}, do produto {2}, na sequencia {3} foi alterado.
                  inter_fn_buscar_tag_composta('lb33056', to_char(:old.numero_roteiro,'00'),
                                                          to_char(:old.numero_alternati,'00'),
                                                          :old.nivel_estrutura  || '.' ||
                                                          :old.grupo_estrutura  || '.' ||
                                                          :old.subgru_estrutura || '.' ||
                                                          :old.item_estrutura,
                                                          to_char(:new.seq_operacao),
                                                          '','','','','','', ws_locale_usuario,ws_usuario_systextil));
             end if;

             if :old.codigo_estagio <> :new.codigo_estagio
             then
                inter_pr_insere_motivo_plan(
                  reg_o_planej.ordem_planejamento,        reg_o_planej.pedido_venda,
                  v_nivel_cons,                           v_grupo_cons,
                  v_subgrupo_cons,                        v_item_cons,
                  reg_o_planej.nivel_produto_origem,      reg_o_planej.grupo_produto_origem,
                  --Pendente                              Roteiro
                  0,                                      3,
                  -- ROTEIRO DE FABRICACAO ALTERADO
                  inter_fn_buscar_tag_composta('lb34586', '','','','','','','','','','', ws_locale_usuario,ws_usuario_systextil),

                  --O estagio do roteiro {0}, da alternativa {1}, do produto {2}, na sequencia {3} foi alterado.
                  inter_fn_buscar_tag_composta('lb34230', to_char(:old.numero_roteiro,'00'),
                                                          to_char(:old.numero_alternati,'00'),
                                                          :old.nivel_estrutura  || '.' ||
                                                          :old.grupo_estrutura  || '.' ||
                                                          :old.subgru_estrutura || '.' ||
                                                          :old.item_estrutura,
                                                          to_char(:new.seq_operacao),
                                                          '','','','','','', ws_locale_usuario,ws_usuario_systextil));
             end if;
          end if;

          -- Eliminacao de uma operacao.
          if deleting
          then
             inter_pr_insere_motivo_plan(
                  reg_o_planej.ordem_planejamento,        reg_o_planej.pedido_venda,
                  v_nivel_cons,                           v_grupo_cons,
                  v_subgrupo_cons,                        v_item_cons,
                  reg_o_planej.nivel_produto_origem,      reg_o_planej.grupo_produto_origem,
                  --Pendente                              Roteiro
                  0,                                      3,
                  -- ROTEIRO DE FABRICACAO ALTERADO
                  inter_fn_buscar_tag_composta('lb34586', '','','','','','','','','','', ws_locale_usuario,ws_usuario_systextil),

                  --A sequencia {0} do roteiro {1}, da alternativa {2}, do produto {3} foi eliminada.
                  inter_fn_buscar_tag_composta('lb33064', to_char(:old.seq_operacao),
                                                          to_char(:old.numero_roteiro,'00'),
                                                          to_char(:old.numero_alternati,'00'),
                                                          :old.nivel_estrutura  || '.' ||
                                                          :old.grupo_estrutura  || '.' ||
                                                          :old.subgru_estrutura || '.' ||
                                                          :old.item_estrutura,
                                                          '','','','','','', ws_locale_usuario,ws_usuario_systextil));

          end if;

          -- Inclusao de uma operacao.
          if inserting
          then
             inter_pr_insere_motivo_plan(
                  reg_o_planej.ordem_planejamento,        reg_o_planej.pedido_venda,
                  v_nivel_cons,                           v_grupo_cons,
                  v_subgrupo_cons,                        v_item_cons,
                  reg_o_planej.nivel_produto_origem,      reg_o_planej.grupo_produto_origem,
                  --Pendente                              Roteiro
                  0,                                      3,
                  -- ROTEIRO DE FABRICACAO ALTERADO
                  inter_fn_buscar_tag_composta('lb34586', '','','','','','','','','','', ws_locale_usuario,ws_usuario_systextil),

                  --A sequencia {0} do roteiro {1}, da alternativa {2}, do produto {3} foi incluida.
                  inter_fn_buscar_tag_composta('lb33065', to_char(:new.seq_operacao),
                                                          to_char(:new.numero_roteiro,'00'),
                                                          to_char(:new.numero_alternati,'00'),
                                                          :new.nivel_estrutura  || '.' ||
                                                          :new.grupo_estrutura  || '.' ||
                                                          :new.subgru_estrutura || '.' ||
                                                          :new.item_estrutura,
                                                          '','','','','','', ws_locale_usuario,ws_usuario_systextil));
          end if;
      end loop;
   end if;

end inter_tr_plan_mov_mqop_050;

-- ALTER TRIGGER "INTER_TR_PLAN_MOV_MQOP_050" ENABLE
 

/

exec inter_pr_recompile;

