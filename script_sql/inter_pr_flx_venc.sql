
  CREATE OR REPLACE PROCEDURE "INTER_PR_FLX_VENC" (p_cod_usuario          IN NUMBER,
                                              p_data_ini             IN DATE  ,
                                              p_parm_venc            IN NUMBER,
                                              p_filtro_empresa       IN NUMBER,
                                              p_incl_exce            IN NUMBER,
                                              p_opcao_posic          IN NUMBER,
                                              p_posic01              IN NUMBER,
                                              p_posic02              IN NUMBER,
                                              p_posic03              IN NUMBER,
                                              p_posic04              IN NUMBER,
                                              p_posic05              IN NUMBER,
                                              p_cod_emp1             IN NUMBER,
                                              p_cod_emp2             IN NUMBER,
                                              p_cod_emp3             IN NUMBER,
                                              p_cod_emp4             IN NUMBER,
                                              p_cod_emp5             IN NUMBER,
                                              p_inclui_port_rec      IN NUMBER,
                                              p_moeda_titulo         IN NUMBER,
                                              p_desc_venc_erro       OUT VARCHAR2,
                                              p_desc_erro            OUT VARCHAR2) is


--
-- Finalidade: Verificar titulos e duplicatas de previsao vencidas, para o fluxo de caixa Horizontal
-- Autor.....: Tiago H
-- Data......: 08/11/10


w_valor_pagtos            cpag_010.saldo_titulo%type;
w_valor_duplic            fatu_070.saldo_duplicata%type;
w_desc_erro               varchar2(2000);
w_desc_erro_aux           varchar2(2000);
w_erro                    EXCEPTION;
w_desc_pgtos              varchar2(20);
w_desc_duplic             varchar2(25);
ws_usuario_rede           varchar2(25);
ws_maquina_rede           varchar2(45);
ws_aplicativo             varchar2(25);
ws_sid                    number(12);
ws_empresa                number(13);
ws_usuario_systextil      varchar2(250);
ws_locale_usuario         varchar2(8);


BEGIN

   -- PARAMETROS
   -- 1 = So duplicatas previstas e vencidas
   -- 2 = So titulos previstos e vencidos
   -- 3 = Titulos e duplicatas

   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   -- ATENCAO! Existem titulos / duplicatas de previsao vencidos.
   w_desc_erro_aux := inter_fn_buscar_tag('ds28918',  ws_locale_usuario, ws_usuario_systextil)|| Chr(10)||Chr(10);
   -- TOTAL TITULOS:
   w_desc_pgtos := inter_fn_buscar_tag('lb05505',  ws_locale_usuario, ws_usuario_systextil);
   -- TOTAL DUPLICATAS:
   w_desc_duplic := inter_fn_buscar_tag('lb38693',  ws_locale_usuario, ws_usuario_systextil);

   if p_parm_venc = 1
   then

      w_valor_duplic := 0;
      FOR crec in (
      select fatu_070.saldo_duplicata
     from fatu_070
     where fatu_070.data_prorrogacao between p_data_ini and (SYSDATE - 1)
       and fatu_070.cod_canc_duplic  = 0
       and fatu_070.situacao_duplic <> 2
       and fatu_070.moeda_titulo     = p_moeda_titulo
       and fatu_070.previsao         = 1
       and ((p_inclui_port_rec = 1
             and exists (select 1 from rcnb_060
                         where rcnb_060.tipo_registro    = 139
                           and rcnb_060.nr_solicitacao   = 810
                           and rcnb_060.subgru_estrutura = p_cod_usuario
                           and rcnb_060.item_estrutura   = 2
                           and rcnb_060.grupo_estrutura  = fatu_070.portador_duplic))
         or  (p_inclui_port_rec = 2
              and not exists (select 1 from rcnb_060
                          where rcnb_060.tipo_registro    = 139
                            and rcnb_060.nr_solicitacao   = 810
                            and rcnb_060.subgru_estrutura = p_cod_usuario
                            and rcnb_060.item_estrutura   = 2
                            and (rcnb_060.grupo_estrutura <> fatu_070.portador_duplic
                              or rcnb_060.grupo_estrutura  = 9999))))

       and (p_filtro_empresa = 1
              and (p_incl_exce = 1
                  and fatu_070.codigo_empresa in (p_cod_emp1,p_cod_emp2,p_cod_emp3,p_cod_emp4,p_cod_emp5))
              or  (p_incl_exce = 2
                  and fatu_070.codigo_empresa not in (p_cod_emp1,p_cod_emp2,p_cod_emp3,p_cod_emp4,p_cod_emp5))
          or (p_filtro_empresa = 2
              and (p_incl_exce = 1
                  and fatu_070.responsavel_receb in (p_cod_emp1,p_cod_emp2,p_cod_emp3,p_cod_emp4,p_cod_emp5))
              or  (p_incl_exce = 2 )
                  and fatu_070.responsavel_receb in (p_cod_emp1,p_cod_emp2,p_cod_emp3,p_cod_emp4,p_cod_emp5)))

       and (   (p_opcao_posic = 1 and fatu_070.posicao_duplic in (p_posic01, p_posic02,p_posic03,p_posic04,p_posic05))
            or (p_opcao_posic = 2 and fatu_070.posicao_duplic not in (p_posic01, p_posic02,p_posic03,p_posic04,p_posic05))
            )
     )
     LOOP
        BEGIN
          w_valor_duplic := w_valor_duplic + crec.saldo_duplicata;
        END;
     END LOOP;

     if w_valor_duplic > 0
     then
        w_desc_erro := w_desc_erro||w_desc_duplic||' '||ltrim(to_char(w_valor_duplic, '999g999g999d00'))|| Chr(10);
     end if;

   elsif p_parm_venc = 2
   then
      w_valor_pagtos := 0;
      FOR cpag in (
      select cpag_010.saldo_titulo
     from cpag_010
     where   cpag_010.data_vencimento  between p_data_ini and (SYSDATE - 1)
       and   cpag_010.cod_cancelamento = 0
       and  (cpag_010.situacao         = 0 or cpag_010.situacao = 3 )
       and   cpag_010.previsao         = 1
       and ((p_inclui_port_rec = 1
       and   exists (select 1 from rcnb_060
                     where rcnb_060.tipo_registro    = 139
                      and rcnb_060.nr_solicitacao    = 810
                       and rcnb_060.subgru_estrutura = p_cod_usuario
                       and rcnb_060.item_estrutura   = 2
                       and rcnb_060.grupo_estrutura  = cpag_010.cod_portador))
        or  (p_inclui_port_rec = 2
       and   not exists (select 1 from rcnb_060
                         where rcnb_060.tipo_registro    = 139
                           and rcnb_060.nr_solicitacao   = 810
                           and rcnb_060.subgru_estrutura = p_cod_usuario
                           and rcnb_060.item_estrutura   = 2
                           and rcnb_060.grupo_estrutura  = cpag_010.cod_portador)))
       and (p_filtro_empresa = 1
             and (p_incl_exce = 1
                  and cpag_010.codigo_empresa in (p_cod_emp1,p_cod_emp2,p_cod_emp3,p_cod_emp4,p_cod_emp5))
             or  (p_incl_exce = 2
                  and cpag_010.codigo_empresa not in (p_cod_emp1,p_cod_emp2,p_cod_emp3,p_cod_emp4,p_cod_emp5))
        or (p_filtro_empresa = 2
              and (p_incl_exce = 1
                  and cpag_010.cod_end_cobranca in (p_cod_emp1,p_cod_emp2,p_cod_emp3,p_cod_emp4,p_cod_emp5))
              or  (p_incl_exce = 2 )
                  and cpag_010.cod_end_cobranca in (p_cod_emp1,p_cod_emp2,p_cod_emp3,p_cod_emp4,p_cod_emp5)))

       and (   (p_opcao_posic = 1 and cpag_010.posicao_titulo in (p_posic01, p_posic02,p_posic03,p_posic04,p_posic05))
            or (p_opcao_posic = 2 and cpag_010.posicao_titulo not in (p_posic01, p_posic02,p_posic03,p_posic04,p_posic05))
           )
     )
     LOOP
        BEGIN
           w_valor_pagtos := w_valor_pagtos + cpag.saldo_titulo;
        END;
     END LOOP;

     if w_valor_pagtos > 0
     then
        w_desc_erro := w_desc_pgtos||' '||ltrim(to_char(w_valor_pagtos, '999g999g999d00'))|| Chr(10);
     end if;

   else
      w_valor_duplic := 0;

      FOR crec in (
      select fatu_070.saldo_duplicata
     from fatu_070
     where fatu_070.data_prorrogacao between p_data_ini and (SYSDATE - 1)
       and fatu_070.cod_canc_duplic  = 0
       and fatu_070.situacao_duplic <> 2
       and fatu_070.moeda_titulo     = p_moeda_titulo
       and fatu_070.previsao         = 1
       and ((p_inclui_port_rec = 1
             and exists (select 1 from rcnb_060
                         where rcnb_060.tipo_registro    = 139
                           and rcnb_060.nr_solicitacao   = 810
                           and rcnb_060.subgru_estrutura = p_cod_usuario
                           and rcnb_060.item_estrutura   = 2
                           and rcnb_060.grupo_estrutura  = fatu_070.portador_duplic))
         or  (p_inclui_port_rec = 2
              and not exists (select 1 from rcnb_060
                          where rcnb_060.tipo_registro    = 139
                            and rcnb_060.nr_solicitacao   = 810
                            and rcnb_060.subgru_estrutura = p_cod_usuario
                            and rcnb_060.item_estrutura   = 2
                            and (rcnb_060.grupo_estrutura <> fatu_070.portador_duplic
                              or rcnb_060.grupo_estrutura  = 9999))))

       and (p_filtro_empresa = 1
              and (p_incl_exce = 1
                  and fatu_070.codigo_empresa in (p_cod_emp1,p_cod_emp2,p_cod_emp3,p_cod_emp4,p_cod_emp5))
              or  (p_incl_exce = 2
                  and fatu_070.codigo_empresa not in (p_cod_emp1,p_cod_emp2,p_cod_emp3,p_cod_emp4,p_cod_emp5))
          or (p_filtro_empresa = 2
              and (p_incl_exce = 1
                  and fatu_070.responsavel_receb in (p_cod_emp1,p_cod_emp2,p_cod_emp3,p_cod_emp4,p_cod_emp5))
              or  (p_incl_exce = 2 )
                  and fatu_070.responsavel_receb in (p_cod_emp1,p_cod_emp2,p_cod_emp3,p_cod_emp4,p_cod_emp5)))

       and (   (p_opcao_posic = 1 and fatu_070.posicao_duplic in (p_posic01, p_posic02,p_posic03,p_posic04,p_posic05))
            or (p_opcao_posic = 2 and fatu_070.posicao_duplic not in (p_posic01, p_posic02,p_posic03,p_posic04,p_posic05))
            )
      )
      LOOP
        BEGIN
          p_desc_erro := 1;
          w_valor_duplic := w_valor_duplic + crec.saldo_duplicata;
        END;
      END LOOP;
      if w_valor_duplic > 0
      then
         w_desc_erro := w_desc_duplic||' '||ltrim(to_char(w_valor_duplic, '999g999g999d00'));
      end if;

      w_valor_pagtos := 0;
      FOR cpag in (
      select cpag_010.saldo_titulo
     from cpag_010
     where   cpag_010.data_vencimento  between p_data_ini and (SYSDATE - 1)
       and   cpag_010.cod_cancelamento = 0
       and  (cpag_010.situacao         = 0 or cpag_010.situacao = 3 )
       and   cpag_010.previsao         = 1
       and ((p_inclui_port_rec = 1
       and   exists (select 1 from rcnb_060
                     where rcnb_060.tipo_registro    = 139
                      and rcnb_060.nr_solicitacao    = 810
                       and rcnb_060.subgru_estrutura = p_cod_usuario
                       and rcnb_060.item_estrutura   = 2
                       and rcnb_060.grupo_estrutura  = cpag_010.cod_portador))
        or  (p_inclui_port_rec = 2
       and   not exists (select 1 from rcnb_060
                         where rcnb_060.tipo_registro    = 139
                           and rcnb_060.nr_solicitacao   = 810
                           and rcnb_060.subgru_estrutura = p_cod_usuario
                           and rcnb_060.item_estrutura   = 2
                           and rcnb_060.grupo_estrutura  = cpag_010.cod_portador)))
       and (p_filtro_empresa = 1
             and (p_incl_exce = 1
                  and cpag_010.codigo_empresa in (p_cod_emp1,p_cod_emp2,p_cod_emp3,p_cod_emp4,p_cod_emp5))
             or  (p_incl_exce = 2
                  and cpag_010.codigo_empresa not in (p_cod_emp1,p_cod_emp2,p_cod_emp3,p_cod_emp4,p_cod_emp5))
        or (p_filtro_empresa = 2
              and (p_incl_exce = 1
                  and cpag_010.cod_end_cobranca in (p_cod_emp1,p_cod_emp2,p_cod_emp3,p_cod_emp4,p_cod_emp5))
              or  (p_incl_exce = 2 )
                  and cpag_010.cod_end_cobranca in (p_cod_emp1,p_cod_emp2,p_cod_emp3,p_cod_emp4,p_cod_emp5)))

       and (   (p_opcao_posic = 1 and cpag_010.posicao_titulo in (p_posic01, p_posic02,p_posic03,p_posic04,p_posic05))
            or (p_opcao_posic = 2 and cpag_010.posicao_titulo not in (p_posic01, p_posic02,p_posic03,p_posic04,p_posic05))
           )
     )
     LOOP
        BEGIN
           w_valor_pagtos := w_valor_pagtos + cpag.saldo_titulo;
        END;
     END LOOP;
     if w_valor_pagtos > 0
     then
         w_desc_erro := w_desc_erro|| Chr(10) ||w_desc_pgtos||' '||ltrim(to_char(w_valor_pagtos, '999g999g999d00'));
     end if;

   end if;
   if w_valor_duplic > 0 or w_valor_pagtos > 0
   then
      p_desc_venc_erro := w_desc_erro_aux||w_desc_erro;
   end if;
EXCEPTION
   WHEN OTHERS THEN
      p_desc_erro :=  p_desc_erro||'Erro na procedure inter_pr_flx_venc '|| Chr(10) || sqlerrm;
END inter_pr_flx_venc;
 

/

exec inter_pr_recompile;

