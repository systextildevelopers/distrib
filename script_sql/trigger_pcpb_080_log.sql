
  CREATE OR REPLACE TRIGGER "TRIGGER_PCPB_080_LOG" 
BEFORE UPDATE of ordem_producao, nivel_comp, grupo_comp, subgrupo_comp,
	item_comp, grupo_receita, subgrupo_receita, item_receita,
	peso_previsto, peso_real, peso_programado, peso_enfestado,
	operador, data_emissao, hora_emissao, data_pesagem,
	hora_pesagem, letra, pesar_produto, seq_operacao,
	consumo, tipo_calculo, codigo_operacao, sequencia_estrutura,
	sequencia_nivel5, grupo_receita_principal, subgru_receita_principal, item_receita_principal,
	nivel_receita_principal, alternativa_principal, alternativa_receita, numero_grafico,
	consumo_receita, ordem_reprocesso, cons_un_rec, centro_custo,
	motivo_repesagem, nome_programa, responsavel_repesagem, valor_ml_l,
	fator_conversor, relacao_banho, volume_banho, tipo_ordem
or DELETE
or INSERT
on pcpb_080
for each row

declare
   ws_sequencia_nivel5             number(3);
   ws_sequencia_estrutura          number(3);
   ws_nivel_comp                   varchar2(1);
   ws_grupo_comp                   varchar2(5);
   ws_subgrupo_comp                varchar2(3);
   ws_item_comp                    varchar2(6);
   ws_tipo_calculo                 number(2);
   ws_consumo                      number(13,7);
   ws_peso_previsto                number(16,5);
   ws_letra                        varchar2(2);
   ws_nivel_receita_principal      varchar2(1);
   ws_grupo_receita_principal      varchar2(5);
   ws_subgru_receita_principal     varchar2(3);
   ws_item_receita_principal       varchar2(6);
   ws_alternativa_principal        number(2);
   ws_numero_grafico               number(6);
   ws_numero_grafico_old           number(6);
   ws_tipo_calculo_old             number(2);
   ws_consumo_old                  number(13,7);
   ws_peso_previsto_old            number(16,5);
   ws_letra_old                    varchar2(2);
   ws_tipo_ocorr                   varchar2(1);

   ws_usuario_rede                 varchar2(20);
   ws_maquina_rede                 varchar2(40);
   ws_aplicacao                    varchar2(20);
   ws_sid                          number(9);
   ws_empresa                      number(3);
   ws_usuario_systextil            varchar2(250);
   ws_locale_usuario               varchar2(5);

   ws_ordem_producao               number(9);
   ws_grupo_receita                varchar2(5);
   ws_subgrupo_receita             varchar2(3);
   ws_item_receita                 varchar2(6);
   ws_nome_programa                varchar2(30);
   ws_valor_ml_l_old               number(7,3);
   ws_fator_conversor_old          number(7,3);
   ws_valor_ml_l                   number(7,3);
   ws_fator_conversor              number(7,3);
begin
   if INSERTING  then
      ws_ordem_producao                := :new.ordem_producao;
      ws_nivel_comp                    := :new.nivel_comp;
      ws_grupo_comp                    := :new.grupo_comp;
      ws_subgrupo_comp                 := :new.subgrupo_comp;
      ws_item_comp                     := :new.item_comp;
      ws_grupo_receita                 := :new.grupo_receita;
      ws_subgrupo_receita              := :new.subgrupo_receita;
      ws_item_receita                  := :new.item_receita;
      ws_sequencia_nivel5              := :new.sequencia_nivel5;
      ws_sequencia_estrutura           := :new.sequencia_estrutura;
      ws_tipo_calculo                  := :new.tipo_calculo;
      ws_consumo                       := :new.consumo;
      ws_peso_previsto                 := :new.peso_previsto;
      ws_letra                         := :new.letra;
      ws_nivel_receita_principal       := :new.nivel_receita_principal;
      ws_grupo_receita_principal       := :new.grupo_receita_principal;
      ws_subgru_receita_principal      := :new.subgru_receita_principal;
      ws_item_receita_principal        := :new.item_receita_principal;
      ws_alternativa_principal         := :new.alternativa_principal;
      ws_numero_grafico                := :new.numero_grafico;
      ws_numero_grafico_old            := 0;
      ws_tipo_calculo_old              := 0;
      ws_consumo_old                   := 0.0000000;
      ws_peso_previsto_old             := 0.00000;
      ws_letra_old                     := '';
      ws_tipo_ocorr                    := 'I';
      ws_nome_programa                 := :new.nome_programa;
      ws_valor_ml_l_old                := 0.000;
      ws_fator_conversor_old           := 0.000;
      ws_valor_ml_l                    := :new.valor_ml_l;
      ws_fator_conversor               := :new.fator_conversor;
   elsif UPDATING then
      ws_ordem_producao                := :new.ordem_producao;
      ws_nivel_comp                    := :new.nivel_comp;
      ws_grupo_comp                    := :new.grupo_comp;
      ws_subgrupo_comp                 := :new.subgrupo_comp;
      ws_item_comp                     := :new.item_comp;
      ws_grupo_receita                 := :new.grupo_receita;
      ws_subgrupo_receita              := :new.subgrupo_receita;
      ws_item_receita                  := :new.item_receita;
      ws_sequencia_nivel5              := :new.sequencia_nivel5;
      ws_sequencia_estrutura           := :new.sequencia_estrutura;
      ws_tipo_calculo                  := :new.tipo_calculo;
      ws_consumo                       := :new.consumo;
      ws_peso_previsto                 := :new.peso_previsto;
      ws_letra                         := :new.letra;
      ws_nivel_receita_principal       := :new.nivel_receita_principal;
      ws_grupo_receita_principal       := :new.grupo_receita_principal;
      ws_subgru_receita_principal      := :new.subgru_receita_principal;
      ws_item_receita_principal        := :new.item_receita_principal;
      ws_alternativa_principal         := :new.alternativa_principal;
      ws_numero_grafico                := :new.numero_grafico;
      ws_numero_grafico_old            := :old.numero_grafico;
      ws_tipo_calculo_old              := :old.tipo_calculo;
      ws_consumo_old                   := :old.consumo;
      ws_peso_previsto_old             := :old.peso_previsto;
      ws_letra_old                     := :old.letra;
      ws_tipo_ocorr                    := 'U';
      ws_nome_programa                 := :new.nome_programa;
      ws_valor_ml_l_old                := :old.valor_ml_l;
      ws_fator_conversor_old           := :old.fator_conversor;
      ws_valor_ml_l                    := :new.valor_ml_l;
      ws_fator_conversor               := :new.fator_conversor;
   elsif DELETING then
      ws_ordem_producao                := :old.ordem_producao;
      ws_nivel_comp                    := :old.nivel_comp;
      ws_grupo_comp                    := :old.grupo_comp;
      ws_subgrupo_comp                 := :old.subgrupo_comp;
      ws_item_comp                     := :old.item_comp;
      ws_grupo_receita                 := :old.grupo_receita;
      ws_subgrupo_receita              := :old.subgrupo_receita;
      ws_item_receita                  := :old.item_receita;
      ws_sequencia_nivel5              := :old.sequencia_nivel5;
      ws_sequencia_estrutura           := :old.sequencia_estrutura;
      ws_tipo_calculo                  := 0;
      ws_consumo                       := 0.0000000;
      ws_peso_previsto                 := 0.00000;
      ws_letra                         := '';
      ws_nivel_receita_principal       := :old.nivel_receita_principal;
      ws_grupo_receita_principal       := :old.grupo_receita_principal;
      ws_subgru_receita_principal      := :old.subgru_receita_principal;
      ws_item_receita_principal        := :old.item_receita_principal;
      ws_alternativa_principal         := :old.alternativa_principal;
      ws_numero_grafico                := 0;
      ws_numero_grafico_old            := :old.numero_grafico;
      ws_tipo_calculo_old              := :old.tipo_calculo;
      ws_consumo_old                   := :old.consumo;
      ws_peso_previsto_old             := :old.peso_previsto;
      ws_letra_old                     := :old.letra;
      ws_tipo_ocorr                    := 'D';
      ws_nome_programa                 := :old.nome_programa;
      ws_valor_ml_l_old                := :old.valor_ml_l;
      ws_fator_conversor_old           := :old.fator_conversor;
      ws_valor_ml_l                    := 0.000;
      ws_fator_conversor               := 0.000;
   end if;

    -- Dados do usuario logado
      inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicacao,     ws_sid,
                              ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);

   INSERT INTO pcpb_080_log
     (sequencia_nivel5,            sequencia_estrutura,          nivel_comp,
      grupo_comp,                  subgrupo_comp,                item_comp,
      tipo_calculo,                consumo,                      peso_previsto,
      letra,                       nivel_receita_principal,      grupo_receita_principal,
      subgru_receita_principal,    item_receita_principal,       alternativa_principal,
      numero_grafico,              numero_grafico_old,           tipo_calculo_old,
      consumo_old,                 peso_previsto_old,            letra_old,
      ordem_producao,              grupo_receita,                subgrupo_receita,
      item_receita,                data_ocorr,                   tipo_ocorr,
      usuario_rede,                maquina_rede,                 aplicacao,
      nome_programa,               valor_ml_l,                   fator_conversor,
      valor_ml_l_old,              fator_conversor_old
)
   VALUES
     (ws_sequencia_nivel5,            ws_sequencia_estrutura,          ws_nivel_comp,
      ws_grupo_comp,                  ws_subgrupo_comp,                ws_item_comp,
      ws_tipo_calculo,                ws_consumo,                      ws_peso_previsto,
      ws_letra,                       ws_nivel_receita_principal,      ws_grupo_receita_principal,
      ws_subgru_receita_principal,    ws_item_receita_principal,       ws_alternativa_principal,
      ws_numero_grafico,              ws_numero_grafico_old,           ws_tipo_calculo_old,
      ws_consumo_old,                 ws_peso_previsto_old,            ws_letra_old,
      ws_ordem_producao,              ws_grupo_receita,                ws_subgrupo_receita,
      ws_item_receita,                sysdate,                         ws_tipo_ocorr,
      ws_usuario_rede,                ws_maquina_rede,                 ws_aplicacao,
      ws_nome_programa,               ws_valor_ml_l,                   ws_fator_conversor,
      ws_valor_ml_l_old,              ws_fator_conversor_old
);

end trigger_pcpb_080_log;


-- ALTER TRIGGER "TRIGGER_PCPB_080_LOG" ENABLE
 

/

exec inter_pr_recompile;

