ALTER TABLE FATU_070 ADD LOTE_RECEBIMENTO NUMBER(9);

COMMENT ON COLUMN FATU_070.LOTE_RECEBIMENTO IS 'Lote do título para recebimento.';

exec inter_pr_recompile;
/
