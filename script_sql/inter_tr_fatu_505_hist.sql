CREATE OR REPLACE TRIGGER "INTER_TR_FATU_505_HIST" 
   after insert or delete
   or update of codigo_empresa, serie_nota_fisc, descricao_serie, tipo_titulo,
	tipo_serie_nota, ultima_nf_impres, rpt_nota_saida, rpt_nota_entrada,
	numero_selo, ultima_fat_impres, situacao_bloqueio, origem_bloqueio,
	numero_linhas_itens_nota, tipo_documento, ultimo_formulario, num_coluna_msg_nota,
	nr_folhas_formulario, num_ultimo_form, serie_nfe, codigo_especie,
	tipo_nf, situacao, serie_bcm, bcm_entra_nf,
	emitir_nota_bcm
   on fatu_505
   for each row

-- Finalidade: Registrar altera��es feitas na tabela fatu_505
-- Autor.....: Aline Blanski
-- Data......: 07/06/10
--
-- Hist�ricos de altera��es na trigger
--
-- Data      Autor           SS          Observa��es
-- 12/07/10  Aline Blanski   58329/001   Adicionado campos na verifica��o da trigger.

declare
   ws_tipo_ocorr                      varchar2 (1);
   ws_usuario_rede                    varchar2(20);
   ws_maquina_rede                    varchar2(40);
   ws_aplicativo                      varchar2(20);
   ws_sid                             number(9);
   ws_empresa                         number(3);
   ws_codigo_empresa_old              number(3);
   ws_codigo_empresa_new              number(3);
   ws_usuario_systextil               varchar2(250);
   ws_locale_usuario                  varchar2(5);
   ws_nome_programa                   varchar2(20);
   ws_serie_nota_fisc_old             VARCHAR2(3)  default '';
   ws_serie_nota_fisc_new             VARCHAR2(3)  default '';
   ws_tipo_serie_nota_old             NUMBER(1)    default 0 ;
   ws_tipo_serie_nota_new             NUMBER(1)    default 0 ;
   ws_tipo_titulo_old                 NUMBER(2)    default 0 ;
   ws_tipo_titulo_new                 NUMBER(2)    default 0 ;
   ws_ultima_nf_impres_old            NUMBER(9)    default 0 ;
   ws_ultima_nf_impres_new            NUMBER(9)    default 0 ;
   ws_rpt_nota_saida_old              fatu_505.rpt_nota_saida%type   default '';
   ws_rpt_nota_saida_new              fatu_505.rpt_nota_saida%type   default '';
   ws_rpt_nota_entrada_old            fatu_505.rpt_nota_entrada%type default '';
   ws_rpt_nota_entrada_new            fatu_505.rpt_nota_entrada%type default '';
   ws_situacao_bloqueio_old           NUMBER(1)    default 0 ;
   ws_situacao_bloqueio_new           NUMBER(1)    default 0 ;
   ws_descricao_serie_old             VARCHAR2(30) default '';
   ws_descricao_serie_new             VARCHAR2(30) default '';
   ws_numero_linhas_itens_nota_ol     NUMBER(3)    default 0 ;
   ws_numero_linhas_itens_nota_ne     NUMBER(3)    default 0 ;
   ws_num_ultimo_form_old             NUMBER(9)    default 0 ;
   ws_num_ultimo_form_new             NUMBER(9)    default 0 ;
   ws_serie_nfe_old                   VARCHAR2(1)  default 'N';
   ws_serie_nfe_new                   VARCHAR2(1)  default 'N';
   ws_numero_selo_old                 NUMBER(12,1) default 0.0;
   ws_numero_selo_new                 NUMBER(12,1) default 0.0;
   ws_num_coluna_msg_nota_old         NUMBER(3)    default 0;
   ws_num_coluna_msg_nota_new         NUMBER(3)    default 0;
   ws_nr_folhas_formulario_old        NUMBER(1)    default 0;
   ws_nr_folhas_formulario_new        NUMBER(1)    default 0;
   ws_codigo_especie_old              VARCHAR2(5)  default '';
   ws_codigo_especie_new              VARCHAR2(5)  default '';
   ws_tipo_documento_old              NUMBER(1)    default 0 ;
   ws_tipo_documento_new              NUMBER(1)    default 0 ;
   ws_ultimo_formulario_old           NUMBER(9)    default 0 ;
   ws_ultimo_formulario_new           NUMBER(9)    default 0 ;
   ws_tipo_nf_old                     VARCHAR2(2)  default '00';
   ws_tipo_nf_new                     VARCHAR2(2)  default '00';

begin
   if INSERTING then

      ws_tipo_ocorr                   := 'I';
      ws_serie_nota_fisc_old          := null;
      ws_serie_nota_fisc_new          := :new.serie_nota_fisc;
      ws_tipo_serie_nota_old          := 0;
      ws_tipo_serie_nota_new          := :new.tipo_serie_nota;
      ws_tipo_titulo_old              := 0;
      ws_tipo_titulo_new              := :new.tipo_titulo;
      ws_ultima_nf_impres_old         := 0;
      ws_ultima_nf_impres_new         := :new.ultima_nf_impres;
      ws_rpt_nota_saida_old           := null;
      ws_rpt_nota_saida_new           := :new.rpt_nota_saida;
      ws_rpt_nota_entrada_old         := null;
      ws_rpt_nota_entrada_new         := :new.rpt_nota_entrada;
      ws_situacao_bloqueio_old        := 0;
      ws_situacao_bloqueio_new        := :new.situacao_bloqueio;
      ws_descricao_serie_old          := null;
      ws_descricao_serie_new          := :new.descricao_serie;
      ws_numero_linhas_itens_nota_ol  := 0;
      ws_numero_linhas_itens_nota_ne  := :new.numero_linhas_itens_nota;
      ws_num_ultimo_form_old          := 0;
      ws_num_ultimo_form_new          := :new.num_ultimo_form;
      ws_serie_nfe_old                := null;
      ws_serie_nfe_new                := :new.serie_nfe;
      ws_numero_selo_old              := 0;
      ws_numero_selo_new              := :new.numero_selo;
      ws_num_coluna_msg_nota_old      := 0;
      ws_num_coluna_msg_nota_new      := :new.num_coluna_msg_nota;
      ws_nr_folhas_formulario_old     := 0;
      ws_nr_folhas_formulario_new     := :new.nr_folhas_formulario;
      ws_codigo_especie_old           := null;
      ws_codigo_especie_new           := :new.codigo_especie;
      ws_tipo_documento_old           := 0;
      ws_tipo_documento_new           := :new.tipo_documento;
      ws_ultimo_formulario_old        := 0;
      ws_ultimo_formulario_new        := :new.ultimo_formulario;
      ws_tipo_nf_old                  := null;
      ws_tipo_nf_new                  := :new.tipo_nf;
      ws_codigo_empresa_old           := 0;
      ws_codigo_empresa_new           := :new.codigo_empresa;

   elsif UPDATING then

      ws_tipo_ocorr                   := 'A';
      ws_serie_nota_fisc_old          := :old.serie_nota_fisc;
      ws_serie_nota_fisc_new          := :new.serie_nota_fisc;
      ws_tipo_serie_nota_old          := :old.tipo_serie_nota;
      ws_tipo_serie_nota_new          := :new.tipo_serie_nota;
      ws_tipo_titulo_old              := :old.tipo_titulo;
      ws_tipo_titulo_new              := :new.tipo_titulo;
      ws_ultima_nf_impres_old         := :old.ultima_nf_impres;
      ws_ultima_nf_impres_new         := :new.ultima_nf_impres;
      ws_rpt_nota_saida_old           := :old.rpt_nota_saida;
      ws_rpt_nota_saida_new           := :new.rpt_nota_saida;
      ws_rpt_nota_entrada_old         := :old.rpt_nota_entrada;
      ws_rpt_nota_entrada_new         := :new.rpt_nota_entrada;
      ws_situacao_bloqueio_old        := :old.situacao_bloqueio;
      ws_situacao_bloqueio_new        := :new.situacao_bloqueio;
      ws_descricao_serie_old          := :old.descricao_serie;
      ws_descricao_serie_new          := :new.descricao_serie;
      ws_numero_linhas_itens_nota_ol  := :old.numero_linhas_itens_nota;
      ws_numero_linhas_itens_nota_ne  := :new.numero_linhas_itens_nota;
      ws_num_ultimo_form_old          := :old.num_ultimo_form;
      ws_num_ultimo_form_new          := :new.num_ultimo_form;
      ws_serie_nfe_old                := :old.serie_nfe;
      ws_serie_nfe_new                := :new.serie_nfe;
      ws_numero_selo_old              := :old.numero_selo;
      ws_numero_selo_new              := :new.numero_selo;
      ws_num_coluna_msg_nota_old      := :old.num_coluna_msg_nota;
      ws_num_coluna_msg_nota_new      := :new.num_coluna_msg_nota;
      ws_nr_folhas_formulario_old     := :old.nr_folhas_formulario;
      ws_nr_folhas_formulario_new     := :new.nr_folhas_formulario;
      ws_codigo_especie_old           := :old.codigo_especie;
      ws_codigo_especie_new           := :new.codigo_especie;
      ws_tipo_documento_old           := :old.tipo_documento;
      ws_tipo_documento_new           := :new.tipo_documento;
      ws_ultimo_formulario_old        := :old.ultimo_formulario;
      ws_ultimo_formulario_new        := :new.ultimo_formulario;
      ws_tipo_nf_old                  := :old.tipo_nf;
      ws_tipo_nf_new                  := :new.tipo_nf;
      ws_codigo_empresa_old           := :old.codigo_empresa;
      ws_codigo_empresa_new           := :new.codigo_empresa;

   elsif DELETING then

      ws_tipo_ocorr                   := 'D';
      ws_serie_nota_fisc_old          := :old.serie_nota_fisc;
      ws_serie_nota_fisc_new          := null;
      ws_tipo_serie_nota_old          := :old.tipo_serie_nota;
      ws_tipo_serie_nota_new          := 0;
      ws_tipo_titulo_old              := :old.tipo_titulo;
      ws_tipo_titulo_new              := 0;
      ws_ultima_nf_impres_old         := :old.ultima_nf_impres;
      ws_ultima_nf_impres_new         := 0;
      ws_rpt_nota_saida_old           := :old.rpt_nota_saida;
      ws_rpt_nota_saida_new           := null;
      ws_rpt_nota_entrada_old         := :old.rpt_nota_entrada;
      ws_rpt_nota_entrada_new         := null;
      ws_situacao_bloqueio_old        := :old.situacao_bloqueio;
      ws_situacao_bloqueio_new        := 0;
      ws_descricao_serie_old          := :old.descricao_serie;
      ws_descricao_serie_new          := null;
      ws_numero_linhas_itens_nota_ol  := :old.numero_linhas_itens_nota;
      ws_numero_linhas_itens_nota_ne  := 0;
      ws_num_ultimo_form_old          := :old.num_ultimo_form;
      ws_num_ultimo_form_new          := 0;
      ws_serie_nfe_old                := :old.serie_nfe;
      ws_serie_nfe_new                := null;
      ws_numero_selo_old              := :old.numero_selo;
      ws_numero_selo_new              := 0;
      ws_num_coluna_msg_nota_old      := :old.num_coluna_msg_nota;
      ws_num_coluna_msg_nota_new      := 0;
      ws_nr_folhas_formulario_old     := :old.nr_folhas_formulario;
      ws_nr_folhas_formulario_new     := 0;
      ws_codigo_especie_old           := :old.codigo_especie;
      ws_codigo_especie_new           := null;
      ws_tipo_documento_old           := :old.tipo_documento;
      ws_tipo_documento_new           := 0;
      ws_ultimo_formulario_old        := :old.ultimo_formulario;
      ws_ultimo_formulario_new        := 0;
      ws_tipo_nf_old                  := :old.tipo_nf;
      ws_tipo_nf_new                  := null;
      ws_codigo_empresa_old           := :old.codigo_empresa;
      ws_codigo_empresa_new           := 0;

   end if;

   -- Dados do usu�rio logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);
   ws_nome_programa := inter_fn_nome_programa(ws_sid); 
   
   INSERT INTO fatu_505_hist
       (aplicacao,                       tipo_ocorr,
        data_ocorr,
        usuario_rede,                    maquina_rede,
        nome_programa,                   usuario_systextil,
        serie_nota_fisc_old,             serie_nota_fisc_new,
        tipo_serie_nota_old,             tipo_serie_nota_new,
        tipo_titulo_old,                 tipo_titulo_new,
        ultima_nf_impres_old,            ultima_nf_impres_new,
        rpt_nota_saida_old,              rpt_nota_saida_new,
        rpt_nota_entrada_old,            rpt_nota_entrada_new,
        situacao_bloqueio_old,           situacao_bloqueio_new,
        descricao_serie_old,             descricao_serie_new,
        numero_linhas_itens_nota_old,    numero_linhas_itens_nota_new,
        num_ultimo_form_old,             num_ultimo_form_new,
        serie_nfe_old,                   serie_nfe_new,
        numero_selo_old,                 numero_selo_new,
        num_coluna_msg_nota_old,         num_coluna_msg_nota_new,
        nr_folhas_formulario_old,        nr_folhas_formulario_new,
        codigo_especie_old,              codigo_especie_new,
        tipo_documento_old,              tipo_documento_new,
        ultimo_formulario_old,           ultimo_formulario_new,
        tipo_nf_old,                     tipo_nf_new,
        codigo_empresa_old,           codigo_empresa_new
      )
   VALUES
      ( ws_aplicativo,                   ws_tipo_ocorr,
        sysdate,
        ws_usuario_rede,                 ws_maquina_rede,
        ws_nome_programa,                ws_usuario_systextil,
        ws_serie_nota_fisc_old,          ws_serie_nota_fisc_new,
        ws_tipo_serie_nota_old,          ws_tipo_serie_nota_new,
        ws_tipo_titulo_old,              ws_tipo_titulo_new,
        ws_ultima_nf_impres_old,         ws_ultima_nf_impres_new,
        ws_rpt_nota_saida_old,           ws_rpt_nota_saida_new,
        ws_rpt_nota_entrada_old,         ws_rpt_nota_entrada_new,
        ws_situacao_bloqueio_old,        ws_situacao_bloqueio_new,
        ws_descricao_serie_old,          ws_descricao_serie_new,
        ws_numero_linhas_itens_nota_ol,  ws_numero_linhas_itens_nota_ne,
        ws_num_ultimo_form_old,          ws_num_ultimo_form_new,
        ws_serie_nfe_old,                ws_serie_nfe_new,
        ws_numero_selo_old,              ws_numero_selo_new,
        ws_num_coluna_msg_nota_old,      ws_num_coluna_msg_nota_new,
        ws_nr_folhas_formulario_old,     ws_nr_folhas_formulario_new,
        ws_codigo_especie_old,           ws_codigo_especie_new,
        ws_tipo_documento_old,           ws_tipo_documento_new,
        ws_ultimo_formulario_old,        ws_ultimo_formulario_new,
        ws_tipo_nf_old,                  ws_tipo_nf_new,
        ws_codigo_empresa_old,           ws_codigo_empresa_new
      );

end inter_tr_fatu_505_hist;
-- ALTER TRIGGER "INTER_TR_FATU_505_HIST" ENABLE
 

/

exec inter_pr_recompile;

