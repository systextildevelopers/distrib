CREATE OR REPLACE FUNCTION inter_fn_get_empresa_sub
   (p_empresa1 in number,
    p_mes in number,
    p_ano in number,
    p_nivel in varchar2,
    p_grupo in varchar2,
    p_subgrupo in varchar2,
    p_item in varchar2)
return number is

   v_empresa number;
   v_forma_calculo_cardex number;

begin
   v_forma_calculo_cardex := INTER_FN_GET_PARAM_INT(p_empresa1, 'estq.formaCalcCardex');

   v_empresa := p_empresa1;

   if (v_forma_calculo_cardex = 3)
   then
     begin
        select tab_aux.codigo_empresa
        into   v_empresa
        from (select basi_305.codigo_empresa
              from   basi_305
              where basi_305.mes = p_mes
                and basi_305.ano = p_ano
                and basi_305.nivel_subproduto = p_nivel
                and basi_305.grupo_subproduto = p_grupo
                and basi_305.subgru_subproduto = p_subgrupo
                and basi_305.item_subproduto = p_item
                and exists (select * from basi_300
                            where basi_300.nivel_subproduto  = basi_305.nivel_subproduto
                              and basi_300.grupo_subproduto  = basi_305.grupo_subproduto
                              and basi_300.subgru_subproduto = basi_305.subgru_subproduto
                              and basi_300.item_subproduto   = basi_305.item_subproduto)
              order by basi_305.codigo_empresa asc) tab_aux
        where rownum < 2;
     exception
        when others
        then v_empresa := p_empresa1;
     end;
   end if;

   return(v_empresa);

end inter_fn_get_empresa_sub;
/

exec inter_pr_recompile;
/
