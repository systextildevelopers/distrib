insert into hdoc_035
(codigo_programa, descricao, programa_menu, item_menu_def)
values
('obrf_f113', 'Configuração de Notas para Danfe Simplificada',0,1);

insert into hdoc_033
(usu_prg_cdusu, usu_prg_empr_usu, programa, nome_menu, item_menu, ordem_menu, incluir, modificar, excluir, procurar)
values
('INTERSYS', 1, 'obrf_f113', 'obrf_menu' ,1, 0, 'S', 'S', 'S', 'S');

update hdoc_036
set hdoc_036.descricao       = 'Configuração de Notas para Danfe Simplificada'
where hdoc_036.codigo_programa = 'obrf_f113'
  and hdoc_036.locale          = 'es_ES';

commit;
/



