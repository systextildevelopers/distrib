-------------------------------------------
-- Capa da Sugestão de tecidos tem peças --
-------------------------------------------
create table pcpt_080 (
	id number(9,0) not null,
  id_crm varchar2(32),
  id_pedido_forca_vendas varchar2(128),
  pedido_venda NUMBER(9,0) NOT NULL,
  artigo varchar2(5) NOT NULL,
  situacao number(1,0) default 0 not null enable,
  criado_por varchar2(250),
  criado_em timestamp (6) default current_timestamp,
  atualizado_por varchar2(250),
  atualizado_em timestamp (6)
);
/

alter table pcpt_080 add constraint pcpt_080_pk primary key (id);
/

create sequence pcpt_080_seq;
/

create or replace trigger  "inter_tr_pcpt_080_pk" 
before insert on pcpt_080
for each row
begin
  :new.id := pcpt_080_seq.nextval;
end;
/
