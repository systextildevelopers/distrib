
  CREATE OR REPLACE TRIGGER "INTER_TR_LOJA_007_LOG" 
after insert or delete or update
on loja_007
for each row
declare
   ws_usuario_rede           varchar2(20) ;
   ws_maquina_rede           varchar2(40) ;
   ws_aplicativo             varchar2(20) ;
   ws_sid                    number(9) ;
   ws_empresa                number(3) ;
   ws_usuario_systextil      varchar2(250) ;
   ws_locale_usuario         varchar2(5) ;
   v_nome_programa           varchar2(20) ;


begin
-- Dados do usuario logado
   inter_pr_dados_usuario (ws_usuario_rede,        ws_maquina_rede,   ws_aplicativo,     ws_sid,
                           ws_usuario_systextil,   ws_empresa,        ws_locale_usuario);


   v_nome_programa := inter_fn_nome_programa(ws_sid);  
       

 if inserting
 then
    begin

        insert into loja_007_log (
           TIPO_OCORR,   /*0*/
           DATA_OCORR,   /*1*/
           HORA_OCORR,   /*2*/
           USUARIO_REDE,   /*3*/
           MAQUINA_REDE,   /*4*/
           APLICACAO,   /*5*/
           USUARIO_SISTEMA,   /*6*/
           NOME_PROGRAMA,   /*7*/
           empresa_OLD,   /*8*/
           empresa_NEW,   /*9*/
           sacola_OLD,   /*10*/
           sacola_NEW,   /*11*/
           seq_sacola_OLD,   /*12*/
           seq_sacola_NEW,   /*13*/
           orcamento_OLD,   /*14*/
           orcamento_NEW,   /*15*/
           situacao_OLD,   /*16*/
           situacao_NEW,   /*17*/
           nr_fardo_OLD,   /*18*/
           nr_fardo_NEW    /*19*/
        ) values (
            'I', /*o*/
            sysdate, /*1*/
            sysdate,/*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           0,/*8*/
           :new.empresa, /*9*/
           0,/*10*/
           :new.sacola, /*11*/
           0,/*12*/
           :new.seq_sacola, /*13*/
           0,/*14*/
           :new.orcamento, /*15*/
           0,/*16*/
           :new.situacao, /*17*/
           '',/*18*/
           :new.nr_fardo /*19*/
         );
    end;
 end if;


 if updating
 then
    begin
        insert into loja_007_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           empresa_OLD, /*8*/
           empresa_NEW, /*9*/
           sacola_OLD, /*10*/
           sacola_NEW, /*11*/
           seq_sacola_OLD, /*12*/
           seq_sacola_NEW, /*13*/
           orcamento_OLD, /*14*/
           orcamento_NEW, /*15*/
           situacao_OLD, /*16*/
           situacao_NEW, /*17*/
           nr_fardo_OLD, /*18*/
           nr_fardo_NEW  /*19*/
        ) values (
            'A', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede, /*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.empresa,  /*8*/
           :new.empresa, /*9*/
           :old.sacola,  /*10*/
           :new.sacola, /*11*/
           :old.seq_sacola,  /*12*/
           :new.seq_sacola, /*13*/
           :old.orcamento,  /*14*/
           :new.orcamento, /*15*/
           :old.situacao,  /*16*/
           :new.situacao, /*17*/
           :old.nr_fardo,  /*18*/
           :new.nr_fardo  /*19*/
         );
    end;
 end if;


 if deleting
 then
    begin
        insert into loja_007_log (
           TIPO_OCORR, /*0*/
           DATA_OCORR, /*1*/
           HORA_OCORR, /*2*/
           USUARIO_REDE, /*3*/
           MAQUINA_REDE, /*4*/
           APLICACAO, /*5*/
           USUARIO_SISTEMA, /*6*/
           NOME_PROGRAMA, /*7*/
           empresa_OLD, /*8*/
           empresa_NEW, /*9*/
           sacola_OLD, /*10*/
           sacola_NEW, /*11*/
           seq_sacola_OLD, /*12*/
           seq_sacola_NEW, /*13*/
           orcamento_OLD, /*14*/
           orcamento_NEW, /*15*/
           situacao_OLD, /*16*/
           situacao_NEW, /*17*/
           nr_fardo_OLD, /*18*/
           nr_fardo_NEW /*19*/
        ) values (
            'D', /*0*/
            sysdate, /*1*/
            sysdate, /*2*/
            ws_usuario_rede,/*3*/
            ws_maquina_rede,/*4*/
            ws_aplicativo, /*5*/
            ws_usuario_systextil,/*6*/
            v_nome_programa, /*7*/
           :old.empresa, /*8*/
           0, /*9*/
           :old.sacola, /*10*/
           0, /*11*/
           :old.seq_sacola, /*12*/
           0, /*13*/
           :old.orcamento, /*14*/
           0, /*15*/
           :old.situacao, /*16*/
           0, /*17*/
           :old.nr_fardo, /*18*/
           '' /*19*/
         );
    end;
 end if;
end inter_tr_loja_007_log;


-- ALTER TRIGGER "INTER_TR_LOJA_007_LOG" ENABLE
 

/

exec inter_pr_recompile;

