CREATE OR REPLACE FUNCTION "INTER_FN_DANFE_SIMPLIFICADA" (p_cod_empresa in number, p_num_nota in number, p_serie_nota in varchar2)
return int is
   Result              int;
begin

   Result := 0;
   begin
     select nvl(max(1),0)
     into Result
     from fatu_050, obrf_119
     where fatu_050.codigo_empresa = p_cod_empresa
       and fatu_050.num_nota_fiscal = p_num_nota
       and fatu_050.serie_nota_fisc = p_serie_nota
       and (fatu_050.cod_rep_cliente = obrf_119.cod_rep or obrf_119.cod_rep = 999999)
       and (fatu_050.origem_pedido = obrf_119.origem_ped or obrf_119.origem_ped = 999)
       and (fatu_050.serie_nota_fisc = obrf_119.serie or obrf_119.serie = 'XXXX')
       and (exists (select 1 from fatu_060
                    where fatu_060.ch_it_nf_cd_empr = fatu_050.codigo_empresa
                      and fatu_060.ch_it_nf_num_nfis = fatu_050.num_nota_fiscal
                      and fatu_060.ch_it_nf_ser_nfis = fatu_050.serie_nota_fisc
                      and fatu_060.deposito = obrf_119.cod_deposito) or obrf_119.cod_deposito = 9999)
       and  exists (select 1 from pcpc_320
                    where pcpc_320.nota_fiscal = fatu_050.num_nota_fiscal
                      and pcpc_320.cod_empresa = fatu_050.codigo_empresa
                      and pcpc_320.serie_nota  = fatu_050.serie_nota_fisc
                      and(pcpc_320.cod_tipo_volume = obrf_119.tipo_vol or obrf_119.tipo_vol = 9999999));
   exception
      when no_data_found then
         result := 0;
   end;

   return(Result);

end inter_fn_danfe_simplificada;

 
 
/

exec inter_pr_recompile;

