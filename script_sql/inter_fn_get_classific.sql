create or replace FUNCTION inter_fn_get_classific(p_nivel VARCHAR2, p_grupo VARCHAR2, p_subgrupo VARCHAR2,
                                                   p_item VARCHAR2, p_estagio_agrupador NUMBER, -->cod_estagio_agrupador_insu
                                                   p_estagio_agrupador_simultaneo NUMBER,
                                                   p_alternativa number default 0)
RETURN varchar2
IS
       w_classific_fiscal varchar2(15) := '';

       v_encontrou_classif  boolean := false;
       v_alternativa        number;
       v_origem_ncm         number := 0;

       v_nivel_comp         varchar2(1) := null;
       v_grupo_comp         varchar2(5) := null;
       v_sub_comp           varchar2(3) := null;
       v_item_comp          varchar2(6) := null;
BEGIN

    v_alternativa := p_alternativa;

    
    --Nivel 1 e estagio agrupador > 0
    if p_nivel = '1' and
       p_estagio_agrupador is not null and p_estagio_agrupador > 0 then

        
        --Buscar Origem NCM
        begin
            select origem_ncm
            into v_origem_ncm
            from mqop_005
            where mqop_005.codigo_estagio = p_estagio_agrupador;
        exception when others then
            v_origem_ncm := 0;
        end;

        
        
        --Origem NCM 1-Componente Principal
        if v_origem_ncm = 1 then

            
            --Se nao recebeu alternativa
            if v_alternativa is null or v_alternativa = 0 then
                begin
                  --Buscar alternativa padrao
                  SELECT numero_alternati
                  INTO v_alternativa
                  FROM basi_010
                  WHERE 
                      basi_010.nivel_estrutura = p_nivel
                  AND basi_010.grupo_estrutura = p_grupo
                  AND basi_010.subgru_estrutura = p_subgrupo
                  AND basi_010.item_estrutura = p_item;
                exception when others then
                    v_alternativa := 0;
                end;

            end if; --v_alternativa is null or v_alternativa = 0



            --Tem alternativa
            if v_alternativa is not null and v_alternativa > 0 then

                
                --Pegar Tecido com TP primeiro, se nao encontrar, Pegar o Componente com Maior Consumo
                begin
                    select nivel_comp,   grupo_comp,   sub_comp,   item_comp
                    INTO   v_nivel_comp, v_grupo_comp, v_sub_comp, v_item_comp
                    from (
                        select nivel_comp, grupo_comp, sub_comp, item_comp
                        from inter_vi_estrutura
                        where inter_vi_estrutura.nivel_item = p_nivel   --'1'
                        and   inter_vi_estrutura.grupo_item = p_grupo   --'00004'
                        and   (inter_vi_estrutura.sub_item  = p_subgrupo  or inter_vi_estrutura.sub_item  = '000')
                        and   (inter_vi_estrutura.item_item = p_item      or inter_vi_estrutura.item_item = '000000')

                        and   inter_vi_estrutura.alternativa_item = v_alternativa
                        
                        and   (     inter_vi_estrutura.tecido_principal > 0     --Tecido com TP
                                or  inter_vi_estrutura.nivel_comp in (2,7)      --Componente 2 ou 7
                            )

                        order by inter_vi_estrutura.tecido_principal desc       --Primeiro o Tecido com TP
                                ,inter_vi_estrutura.consumo desc                --Depois o de maior consumo
                    )
                    where rownum <= 1;   --Traz o que encontrar primeiro (Primeiro Tecido com TP OU se nao encontrar, o componente de maior consumo)
                exception when others then
                    v_nivel_comp := null;
                    v_grupo_comp := null;
                    v_sub_comp   := null;
                    v_item_comp  := null;
                end;

                
                --Se encontrou componente
                if v_nivel_comp is not null then


                    --Buscar classificacao fiscal do componente
                    begin
                        SELECT classific_fiscal
                        INTO w_classific_fiscal
                        FROM basi_010
                        WHERE 
                            basi_010.nivel_estrutura = v_nivel_comp
                        AND basi_010.grupo_estrutura = v_grupo_comp
                        AND basi_010.subgru_estrutura = v_sub_comp
                        AND basi_010.item_estrutura = v_item_comp;
                    exception when others then
                        null;
                    end;

                    
                    --Se encontrou o cadastro do componente e tem classificacao fiscal, considerar esta classificacao fiscal
                    if trim(w_classific_fiscal) is not null then
                        v_encontrou_classif := true;
                    end if;

                
                end if;     --v_nivel_comp is not null



            end if;     --alternativa > 0


        end if;     --v_origem_ncm = 1-Componente Principal


    end if;     --nivel = 1 and p_estagio_agrupador > 0



    --Se nao encontrou classificacao fiscal acima
    if v_encontrou_classif = false then
          begin
            SELECT classific_fiscal
            INTO w_classific_fiscal
            FROM basi_010
            WHERE 
                basi_010.nivel_estrutura = p_nivel
            AND basi_010.grupo_estrutura = p_grupo
            AND basi_010.subgru_estrutura = p_subgrupo
            AND basi_010.item_estrutura = p_item;
          exception when others then
              null;
          end;
    end if;
    
    RETURN w_classific_fiscal;

END;
/
