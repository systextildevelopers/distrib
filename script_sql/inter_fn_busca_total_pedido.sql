create or replace FUNCTION "INTER_FN_BUSCA_TOTAL_PEDIDO" ( p_ped_compra in number)
return
  number
is
  v_return number;
begin

    select  nvl( SUM(qtde_pedida_item * (supr_100.preco_item_comp*((percentual_ipi/100) + (NVL(percentual_subs, 0) / 100)+1) )), 0) AS QTDE_PEDIDA
    into v_return
    from supr_100
    where supr_100.num_ped_compra = p_ped_compra;

    return v_return;
end;

/

exec inter_pr_recompile;
