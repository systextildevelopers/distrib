insert into pedi_016
(cnpj9, cnpj4, cnpj2, atributo, conteudo)
select cgc_9, cgc_4, cgc_2, 'cliente.ddd_celular', '0' from pedi_010
where  not exists (select 1 from pedi_016 a
                  where a.cnpj9 = pedi_010.cgc_9
                  and a.cnpj4 = pedi_010.cgc_4
                  and a.cnpj2 = pedi_010.cgc_2
                  and a.atributo = 'cliente.ddd_celular' );
 
/

update pedi_016 a
set a.conteudo = (select b.ddd 
from basi_160 b, pedi_010 c
where ( c.cod_cidade = b.cod_cidade and
       a.cnpj9 = c.cgc_9 and a.cnpj4 = c.cgc_4 and a.cnpj2 = c.cgc_2))
where a.atributo = 'cliente.ddd_celular';

/

commit
/
