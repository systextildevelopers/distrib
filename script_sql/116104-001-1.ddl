ALTER TABLE EMPR_002 
ADD (VARIACAO_DATAS_EMIS_SAIDA NUMBER(3) DEFAULT 0 );

COMMENT ON COLUMN EMPR_002.VARIACAO_DATAS_EMIS_SAIDA IS 'Indica os dias de variação para data de emissão e data de saída de notas fiscais em relação à data corrente';

exec inter_pr_recompile;
