declare tmpInt number;

cursor parametro_c is
  select dep_terc_sai_1, dep_terc_sai_2, dep_terc_sai_4, dep_terc_sai_7, dep_terc_sai_9,
    dep_terc_ent_1, dep_terc_ent_2, dep_terc_ent_4, dep_terc_ent_7, dep_terc_ent_9,
    rpt_comunicado_canc, rpt_volume_previsto, nr_linhas_layout_bcm, venda_loja_por_volume,
    nat_oper_loja_prod_consignado, cod_tipo_volume_monta_pedido, perm_alter_qtde_req_mat,
    origem_preco_custo, codigo_empresa
  from fatu_504;

begin
  for reg_parametro in parametro_c
  loop
    tmpInt := 0;

    select nvl(count(*),0) into tmpInt
    from fatu_500
    where fatu_500.codigo_empresa = reg_parametro.codigo_empresa;

    if(tmpInt > 0)
    then
      begin
        insert into empr_008 (codigo_empresa, param, val_int) values (reg_parametro.codigo_empresa, 'estoque.depTercSaida1', reg_parametro.dep_terc_sai_1);
        insert into empr_008 (codigo_empresa, param, val_int) values (reg_parametro.codigo_empresa, 'estoque.depTercSaida2', reg_parametro.dep_terc_sai_2);
        insert into empr_008 (codigo_empresa, param, val_int) values (reg_parametro.codigo_empresa, 'estoque.depTercSaida4', reg_parametro.dep_terc_sai_4);
        insert into empr_008 (codigo_empresa, param, val_int) values (reg_parametro.codigo_empresa, 'estoque.depTercSaida7', reg_parametro.dep_terc_sai_7);
        insert into empr_008 (codigo_empresa, param, val_int) values (reg_parametro.codigo_empresa, 'estoque.depTercSaida9', reg_parametro.dep_terc_sai_9);
        insert into empr_008 (codigo_empresa, param, val_int) values (reg_parametro.codigo_empresa, 'estoque.depTercEntrada1', reg_parametro.dep_terc_ent_1);
        insert into empr_008 (codigo_empresa, param, val_int) values (reg_parametro.codigo_empresa, 'estoque.depTercEntrada2', reg_parametro.dep_terc_ent_2);
        insert into empr_008 (codigo_empresa, param, val_int) values (reg_parametro.codigo_empresa, 'estoque.depTercEntrada4', reg_parametro.dep_terc_ent_4);
        insert into empr_008 (codigo_empresa, param, val_int) values (reg_parametro.codigo_empresa, 'estoque.depTercEntrada7', reg_parametro.dep_terc_ent_7);
        insert into empr_008 (codigo_empresa, param, val_int) values (reg_parametro.codigo_empresa, 'estoque.depTercEntrada9', reg_parametro.dep_terc_ent_9);
        insert into empr_008 (codigo_empresa, param, val_str) values (reg_parametro.codigo_empresa, 'vendas.rpt.comunicadoCanc', reg_parametro.rpt_comunicado_canc);
        insert into empr_008 (codigo_empresa, param, val_str) values (reg_parametro.codigo_empresa, 'confeccao.rpt.volumePrevisto', reg_parametro.rpt_volume_previsto);
        insert into empr_008 (codigo_empresa, param, val_int) values (reg_parametro.codigo_empresa, 'fiscal.nrLinhasLayoutBCM', reg_parametro.nr_linhas_layout_bcm);
        insert into empr_008 (codigo_empresa, param, val_int) values (reg_parametro.codigo_empresa, 'loja.vendaPorVolume', reg_parametro.venda_loja_por_volume);
        insert into empr_008 (codigo_empresa, param, val_int) values (reg_parametro.codigo_empresa, 'loja.naturezaProdConsignado', reg_parametro.nat_oper_loja_prod_consignado);
        insert into empr_008 (codigo_empresa, param, val_int) values (reg_parametro.codigo_empresa, 'vendas.tipoVolumeMontaPedido', reg_parametro.cod_tipo_volume_monta_pedido);
        insert into empr_008 (codigo_empresa, param, val_int) values (reg_parametro.codigo_empresa, 'compras.permiteAlterarQtdeReqMat', reg_parametro.perm_alter_qtde_req_mat);
        insert into empr_008 (codigo_empresa, param, val_int) values (reg_parametro.codigo_empresa, 'estoque.origemPrecoCusto', reg_parametro.origem_preco_custo);
      end;
    end if;

  end loop;
  commit;
end;
/

exec inter_pr_recompile;
