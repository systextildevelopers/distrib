alter table RCNB_850 add param_margem NUMBER(10,5) default 0.00000;

alter table RCNB_850 add valor_margem NUMBER(10,5) default 0.00000;

alter table RCNB_030 add margem NUMBER(1) default 0;

comment on column RCNB_030.margem
  is '0-Nao/1-Sim - Serve para identificar se e referente a margem de lucro.';
