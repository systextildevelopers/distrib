create table pedi_305_LOG  
( 
  TIPO_OCORR                VARCHAR2(1) default '' null,
  DATA_OCORR                DATE null,
  HORA_OCORR                DATE null,
  USUARIO_REDE              VARCHAR2(20) default '' null,
  MAQUINA_REDE              VARCHAR2(40) default '' null,
  APLICACAO                 VARCHAR2(20) default '' null,
  USUARIO_SISTEMA           VARCHAR2(20) default '' null,
  NOME_PROGRAMA             VARCHAR2(20) default '' null,
  nivel_OLD          VARCHAR2(1) default ' ' ,  
  nivel_NEW          VARCHAR2(1) default ' ' , 
  grupo_OLD          VARCHAR2(5) default ' ' ,  
  grupo_NEW          VARCHAR2(5) default ' ' , 
  subgrupo_OLD       VARCHAR2(3) default ' ' ,  
  subgrupo_NEW       VARCHAR2(3) default ' ' , 
  item_OLD           VARCHAR2(6) default ' ' ,  
  item_NEW           VARCHAR2(6) default ' ' , 
  tipo_deposito_OLD  NUMBER(1) default 0 ,  
  tipo_deposito_NEW  NUMBER(1) default 0 , 
  dataini_OLD        DATE ,  
  dataini_NEW        DATE , 
  datafim_OLD        DATE ,  
  datafim_NEW        DATE , 
  por_grade_OLD      NUMBER(1) default 0,  
  por_grade_NEW      NUMBER(1) default 0, 
  codigo_empresa_OLD NUMBER(3) default 0 , 
  codigo_empresa_NEW NUMBER(3) default 0  
) ;
exec inter_pr_recompile;
